Option Strict Off
Option Explicit On
Imports System.Text
Imports System.Security.Cryptography

Public Class encryp_decrpy
    Private TripleDes As New TripleDESCryptoServiceProvider

    '-------- Encrypt the Data ---------'
    Public Shared Function GetEncryptedData(ByVal Data As String) As String

        Dim shaM As New SHA1Managed
        Convert.ToBase64String(shaM.ComputeHash(Encoding.ASCII.GetBytes(Data)))
        Dim eNC_data() As Byte = ASCIIEncoding.ASCII.GetBytes(Data)
        Dim eNC_str As String = Convert.ToBase64String(eNC_data)
        GetEncryptedData = eNC_str

    End Function

    '-------- Decrypt the Data ---------'

    Public Shared Function GetDecryptedData(ByVal Data As String) As String
        Dim dEC_data() As Byte = Convert.FromBase64String(Data)
        Dim dEC_Str As String = ASCIIEncoding.ASCII.GetString(dEC_data)
        GetDecryptedData = dEC_Str

    End Function

End Class
