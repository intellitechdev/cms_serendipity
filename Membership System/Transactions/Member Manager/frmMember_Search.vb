Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class Employee_Search
    Public xcase As String
    Private Sub btnemp_close_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnemp_close.Click
        gridExistingMember.Columns.Clear()
        Me.Close()
    End Sub

    Private Sub btnfind_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnfind.Click
        Call GetExistingMember(Me.txtid.Text.Trim)
    End Sub

    Private Sub grid_employee_search_CellPainting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles gridExistingMember.CellPainting
        'grid_employee_search
        Dim sf As New StringFormat
        sf.Alignment = StringAlignment.Center
        If e.ColumnIndex < 0 AndAlso e.RowIndex >= 0 AndAlso e.RowIndex < gridExistingMember.Rows.Count Then
            e.PaintBackground(e.ClipBounds, True)
            e.Graphics.DrawString((e.RowIndex + 1).ToString, Me.Font, Brushes.Black, e.CellBounds, sf)
            e.Handled = True
        End If
    End Sub

    Public Sub PlotviewRecord()
        Try
            With frmMember_Master
                Select Case (gridExistingMember.CurrentRow.Cells(0).Value)
                    Case True
                        .chkyes.Checked = True
                    Case False
                        .chkNo.Checked = False
                    Case Else
                End Select

                '.txtMemberID.Text = gridExistingMember.CurrentRow.Cells(2).Value

                'If gridExistingMember.CurrentRow.Cells(5).Value = "" And gridExistingMember.CurrentRow.Cells(6).Value = "" Then
                '    .temp_lname.Clear()
                '    .txtCompanyName.Text = gridExistingMember.CurrentRow.Cells(4).Value
                'Else
                '    .temp_lname.Text = gridExistingMember.CurrentRow.Cells(4).Value
                '    .txtCompanyName.Text = ""
                'End If

                .temp_fname.Text = gridExistingMember.CurrentRow.Cells(5).Value
                .temp_midname.Text = gridExistingMember.CurrentRow.Cells(6).Value
                .cboemp_gender.Text = gridExistingMember.CurrentRow.Cells(7).Value
                .cboemp_civil.Text = gridExistingMember.CurrentRow.Cells(8).Value
                .EMP_DATEbirth.Text = gridExistingMember.CurrentRow.Cells(9).Value
                .temp_placebirth.Text = gridExistingMember.CurrentRow.Cells(10).Value
                '.txtPresBldg.Text = gridExistingMember.CurrentRow.Cells(11).Value
                '.txtPrevAdd.Text = gridExistingMember.CurrentRow.Cells(12).Value
                .txtLandline.Text = gridExistingMember.CurrentRow.Cells(13).Value
                .txtresidencephone.Text = gridExistingMember.CurrentRow.Cells(14).Value
                .txtEmailAddress.Text = gridExistingMember.CurrentRow.Cells(15).Value
                .cbotitledesignation.Text = gridExistingMember.CurrentRow.Cells(16).Value
                .cborate.Text = gridExistingMember.CurrentRow.Cells(17).Value
                .txtofficenumber.Text = gridExistingMember.CurrentRow.Cells(18).Value
                .txtlocalofficenumber.Text = gridExistingMember.CurrentRow.Cells(19).Value
                .emp_Datehired.Text = gridExistingMember.CurrentRow.Cells(20).Value
                .txtbasicpay.Text = gridExistingMember.CurrentRow.Cells(21).Value
                .mem_MemberDate.Text = gridExistingMember.CurrentRow.Cells(22).Value
                .txtwithdrawal.Text = gridExistingMember.CurrentRow.Cells(23).Value

                Select Case (gridExistingMember.CurrentRow.Cells(24).Value)
                    Case True
                        .chkBereaveYes.Checked = True
                    Case False
                        .chkBereaveNo.Checked = False
                    Case Else
                End Select

                '.cboemp_status.Text = gridExistingMember.CurrentRow.Cells(27).Value
                .Cbomem_Status.Text = gridExistingMember.CurrentRow.Cells(27).Value

                Dim x As Double = gridExistingMember.CurrentRow.Cells(29).Value
                Dim y As Double = gridExistingMember.CurrentRow.Cells(30).Value
                .emp_Ytenures.Text = CStr(x) + " yr's" + " and " + CStr(y) + " m"

                .GetNearestRelatives(gridExistingMember.CurrentRow.Cells(1).Value)
                .GetmemberDependents(gridExistingMember.CurrentRow.Cells(1).Value)
                .GetmemberBankInfo(gridExistingMember.CurrentRow.Cells(1).Value)
                .GetsourceofIncomce(gridExistingMember.CurrentRow.Cells(1).Value)
                .GetmemberEducationalInfo(gridExistingMember.CurrentRow.Cells(1).Value)
                .GetmemberJobDescription(gridExistingMember.CurrentRow.Cells(1).Value)
                .GetmemberSkills(gridExistingMember.CurrentRow.Cells(1).Value)
                .GetmemberAwards(gridExistingMember.CurrentRow.Cells(1).Value)
                .GetmemberMedicals(gridExistingMember.CurrentRow.Cells(1).Value)
                '.GetmemberLoanHistory(gridExistingMember.CurrentRow.Cells(1).Value)
                '.GetmemberLeave(gridExistingMember.CurrentRow.Cells(1).Value, .cboLeave.Text)
                .GetmemberTrainings(gridExistingMember.CurrentRow.Cells(1).Value)
                .GetmemberDiscipline(gridExistingMember.CurrentRow.Cells(1).Value)
                .GetmemberPerfEvaluation(gridExistingMember.CurrentRow.Cells(1).Value)

                .txtEmployeeNo.Text = gridExistingMember.CurrentRow.Cells(1).Value
                .cbotaxcode.Text = gridExistingMember.CurrentRow.Cells(31).Value
                .emp_company.Text = gridExistingMember.CurrentRow.Cells(32).Value
                .txtofficeadd.Text = gridExistingMember.CurrentRow.Cells(33).Value
                .txtpayrollcontriamount.Text = gridExistingMember.CurrentRow.Cells(34).Value
                .txtorgchart.Tag = gridExistingMember.CurrentRow.Cells(35).Value
                .cboPaycode.Text = gridExistingMember.CurrentRow.Cells(36).Value
                .cbopayroll.Text = gridExistingMember.CurrentRow.Cells(37).Value
                .mem_PaycontDate.Text = gridExistingMember.CurrentRow.Cells(38).Value
                .cboemp_status.Text = gridExistingMember.CurrentRow.Cells(40).Value
                .cboEmp_type.Text = gridExistingMember.CurrentRow.Cells(41).Value
                .txtContractrate.Text = gridExistingMember.CurrentRow.Cells(42).Value
                .txtNickname.Text = gridExistingMember.CurrentRow.Cells(44).Value
                .txtHeightFt.Text = gridExistingMember.CurrentRow.Cells(45).Value
                .txtHeightInch.Text = gridExistingMember.CurrentRow.Cells(46).Value
                .txtW.Text = gridExistingMember.CurrentRow.Cells(47).Value
                .txtLandline2.Text = gridExistingMember.CurrentRow.Cells(48).Value
                .txtresidencephone2.Text = gridExistingMember.CurrentRow.Cells(49).Value
                .txtemailaddress2.Text = gridExistingMember.CurrentRow.Cells(50).Value
                .txtFacebook1.Text = gridExistingMember.CurrentRow.Cells(51).Value
                .txtFacebook2.Text = gridExistingMember.CurrentRow.Cells(52).Value
                '.txtPermanentAdd.Text = gridExistingMember.CurrentRow.Cells(53).Value
                .txtCommonrefNo.Text = gridExistingMember.CurrentRow.Cells(54).Value
                '.txtZipCode1.Text = gridExistingMember.CurrentRow.Cells(55).Value
                '.txtZipcode2.Text = gridExistingMember.CurrentRow.Cells(56).Value
                '.txtZipCode3.Text = gridExistingMember.CurrentRow.Cells(57).Value
                .cboStay1.Text = gridExistingMember.CurrentRow.Cells(58).Value
                .cboStay2.Text = gridExistingMember.CurrentRow.Cells(59).Value
                .cboStay3.Text = gridExistingMember.CurrentRow.Cells(60).Value
                .txtStay2.Text = gridExistingMember.CurrentRow.Cells(61).Value
                .txtStay3.Text = gridExistingMember.CurrentRow.Cells(62).Value
                .txtStay1.Text = gridExistingMember.CurrentRow.Cells(63).Value
                .txtFatherName.Text = gridExistingMember.CurrentRow.Cells(64).Value
                .txtFAdd.Text = gridExistingMember.CurrentRow.Cells(65).Value
                .txtFContact.Text = gridExistingMember.CurrentRow.Cells(66).Value
                .txtMotherName.Text = gridExistingMember.CurrentRow.Cells(67).Value
                .txtMAdd.Text = gridExistingMember.CurrentRow.Cells(68).Value
                .txtMContact.Text = gridExistingMember.CurrentRow.Cells(69).Value
                .txtSpouseName.Text = gridExistingMember.CurrentRow.Cells(70).Value
                .txtSpouseAdd.Text = gridExistingMember.CurrentRow.Cells(71).Value
                .txtSpouseContact.Text = gridExistingMember.CurrentRow.Cells(72).Value
                .txtOccupation.Text = gridExistingMember.CurrentRow.Cells(73).Value
                .txtSpouseStay.Text = gridExistingMember.CurrentRow.Cells(75).Value

                Select Case (gridExistingMember.CurrentRow.Cells(76).Value)
                    Case "Month"
                        .rbSpouseMonth.Checked = True
                    Case "Year"
                        .rbSpouseYears.Checked = True
                End Select

                .txtChildNo.Text = gridExistingMember.CurrentRow.Cells(77).Value

                Select Case (gridExistingMember.CurrentRow.Cells(78).Value)
                    Case "Owned"
                        .rbOwned.Checked = True
                    Case "Rented"
                        .rbRented.Checked = True
                    Case "Living with Parents"
                        .rbParents.Checked = True
                    Case Else
                        .rbParents.Checked = False
                        .rbRented.Checked = False
                        .rbOwned.Checked = False
                End Select

                Select Case (gridExistingMember.CurrentRow.Cells(79).Value)
                    Case True
                        .rbMYes.Checked = True
                    Case False
                        .rbMNo.Checked = True
                    Case Else
                        .rbMNo.Checked = False
                        .rbMYes.Checked = False
                End Select

                Select Case (gridExistingMember.CurrentRow.Cells(80).Value)
                    Case True
                        .rbCYes.Checked = True
                    Case False
                        .rbCNo.Checked = True
                    Case Else
                        .rbCYes.Checked = False
                        .rbCNo.Checked = False
                End Select

                .txtMotor.Text = gridExistingMember.CurrentRow.Cells(81).Value
                .txtCar.Text = gridExistingMember.CurrentRow.Cells(82).Value
                .cboOutlet.Text = gridExistingMember.CurrentRow.Cells(83).Value

                .txtPresBldg.Text = gridExistingMember.CurrentRow.Cells(84).Value
                .txtPresStreet.Text = gridExistingMember.CurrentRow.Cells(85).Value
                .txtPresSitio.Text = gridExistingMember.CurrentRow.Cells(86).Value
                .txtPresCity.Text = gridExistingMember.CurrentRow.Cells(87).Value
                .txtPresProvince.Text = gridExistingMember.CurrentRow.Cells(88).Value
                .txtPrevBldg.Text = gridExistingMember.CurrentRow.Cells(89).Value
                .txtPrevStreet.Text = gridExistingMember.CurrentRow.Cells(90).Value
                .txtPrevSitio.Text = gridExistingMember.CurrentRow.Cells(91).Value
                .txtPrevCity.Text = gridExistingMember.CurrentRow.Cells(92).Value
                .txtPrevProvince.Text = gridExistingMember.CurrentRow.Cells(93).Value
                .txtPermBldg.Text = gridExistingMember.CurrentRow.Cells(94).Value
                .txtPermStreet.Text = gridExistingMember.CurrentRow.Cells(95).Value
                .txtPermSitio.Text = gridExistingMember.CurrentRow.Cells(96).Value
                .txtPermCity.Text = gridExistingMember.CurrentRow.Cells(97).Value
                .txtPermProvince.Text = gridExistingMember.CurrentRow.Cells(98).Value
                .txtSuffix.Text = gridExistingMember.CurrentRow.Cells(99).Value
                .txtContractPrice.Text = gridExistingMember.CurrentRow.Cells(101).Value
                .txtEcola.Text = gridExistingMember.CurrentRow.Cells(102).Value
                .cboContactPerson.Text = gridExistingMember.CurrentRow.Cells(103).Value
                .txtProject.Text = gridExistingMember.CurrentRow.Cells(104).Value

                .getorgchartvalue(frmMember_Master.txtorgchart.Tag)
                .getorgchartIDtodepartment2(frmMember_Master.txtorgchart.Tag)

                'If frmMember_Master.txtCompanyName.Text <> "" Or .temp_fname.Text = "" And .temp_midname.Text = "" Then
                '    frmMember_Master.lblemployee_name.Text = frmMember_Master.txtCompanyName.Text
                'Else
                '    frmMember_Master.lblemployee_name.Text = My.Forms.frmMember_Master.temp_fname.Text + " " + My.Forms.frmMember_Master.temp_midname.Text + " " + My.Forms.frmMember_Master.temp_lname.Text
                'End If
                .LoadEloadingRegistrationDetails(.txtEmployeeNo.Text)

                If gridExistingMember.CurrentRow.Cells(39).Value = True Then
                    .rdoExempt.Checked = True
                    .rdoNonExempt.Checked = False
                Else
                    .rdoExempt.Checked = False
                    .rdoNonExempt.Checked = True
                End If
                'End If
            End With
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Employee_Search_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        gridExistingMember.Columns.Clear()
        Call GetExistingMember("")
        ActiveControl = txtid
    End Sub

#Region "Get Existing Member"
    Private Sub FormatGridMemberMaster()
        With Me.gridExistingMember
            .Columns(0).Visible = False
            .Columns(1).Width = 130
            .Columns(2).Visible = False
            .Columns(3).Visible = False
            .Columns(4).Width = 200
            .Columns(5).Width = 200
            .Columns(6).Width = 200
            '.Columns(6).Visible = False
            .Columns(7).Visible = False
            .Columns(8).Visible = False
            .Columns(9).Visible = False
            .Columns(10).Visible = False
            .Columns(11).Visible = False
            .Columns(12).Visible = False
            .Columns(13).Visible = False
            .Columns(14).Visible = False
            .Columns(15).Visible = False
            .Columns(16).Visible = False
            .Columns(17).Visible = False
            .Columns(18).Visible = False
            .Columns(19).Visible = False
            .Columns(20).Visible = False
            .Columns(21).Visible = False
            .Columns(22).Visible = False
            .Columns(23).Visible = False
            .Columns(24).Visible = False
            .Columns(25).Visible = False
            .Columns(26).Visible = False
            .Columns(27).Visible = False
            .Columns(28).Visible = False
            .Columns(29).Visible = False
            .Columns(30).Visible = False
            .Columns(31).Visible = False
            .Columns(32).Visible = False
            .Columns(33).Visible = False
            .Columns(34).Visible = False
            .Columns(35).Visible = False
            .Columns(36).Visible = False
            .Columns(37).Visible = False
            .Columns(38).Visible = False
            .Columns(39).Visible = False
            .Columns(40).Visible = False
            .Columns(41).Visible = False
            .Columns(42).Visible = False
            .Columns(43).Visible = False
            .Columns(44).Visible = False
            .Columns(45).Visible = False
            .Columns(46).Visible = False
            .Columns(47).Visible = False
            .Columns(48).Visible = False
            .Columns(49).Visible = False
            .Columns(50).Visible = False
            .Columns(51).Visible = False
            .Columns(52).Visible = False
            .Columns(53).Visible = False
            .Columns(54).Visible = False
            .Columns(55).Visible = False
            .Columns(56).Visible = False
            .Columns(57).Visible = False
            .Columns(58).Visible = False
            .Columns(59).Visible = False
            .Columns(60).Visible = False
            .Columns(61).Visible = False
            .Columns(62).Visible = False
            .Columns(63).Visible = False
            .Columns(64).Visible = False
            .Columns(65).Visible = False
            .Columns(66).Visible = False
            .Columns(67).Visible = False
            .Columns(68).Visible = False
            .Columns(69).Visible = False
            .Columns(70).Visible = False
            .Columns(71).Visible = False
            .Columns(72).Visible = False
            .Columns(73).Visible = False
            .Columns(74).Visible = False
            .Columns(75).Visible = False
            .Columns(76).Visible = False
            .Columns(77).Visible = False
            .Columns(78).Visible = False
            .Columns(79).Visible = False
            .Columns(80).Visible = False
            .Columns(81).Visible = False
            .Columns(82).Visible = False
            .Columns(83).Visible = False
            .Columns(84).Visible = False
            .Columns(85).Visible = False
            .Columns(86).Visible = False
            .Columns(87).Visible = False
            .Columns(88).Visible = False
            .Columns(89).Visible = False
            .Columns(90).Visible = False
            .Columns(91).Visible = False
            .Columns(92).Visible = False
            .Columns(93).Visible = False
            .Columns(94).Visible = False
            .Columns(95).Visible = False
            .Columns(96).Visible = False
            .Columns(97).Visible = False
            .Columns(98).Visible = False
            .Columns(99).Visible = False
            .Columns(100).Visible = False
            .Columns(101).Visible = False
            .Columns(102).Visible = False
            .Columns(103).Visible = False
            .Columns(104).Visible = False
            '.Columns(105).Visible = False
        End With
    End Sub
    Private Sub GetExistingMember(ByVal Empinfo As String)
        Dim gcon As New Clsappconfiguration()
        Dim ds As New DataSet
        Call frmMember_Master.cleartxt()

        Try
            ds = ModifiedSqlHelper.ExecuteDataset(gcon.cnstring, CommandType.StoredProcedure, "MSS_MembersInfo_GetMasterInfo", _
                                New SqlParameter("@employeeNo", Empinfo))
            gridExistingMember.DataSource = ds.Tables(0)
            gridExistingMember.Columns(1).HeaderText = "ID No."
            Call FormatGridMemberMaster()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub
#End Region

    Private Sub btnok_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnok.Click
        If xcase = "Dep and Withd" Then
            Dim gcon As New Clsappconfiguration
            Dim rd As SqlDataReader
            Dim load As String = "select fcEmployeeNo,fcLastName +', '+ fcFirstName +' '+  fcMiddleName [full name], isnull((sum(fdDepositAmount) - sum(fdWithdrawalAmount)),0.00)[Total Contribution] from dbo.CIMS_m_Member a left outer join dbo.CIMS_t_Member_Contributions b on fk_Employee =  pk_employee WHERE fcEmployeeNo =" & "'" & gridExistingMember.CurrentRow.Cells(1).Value & "'  group by fcEmployeeNo,fcLastName,fcFirstName,fcMiddleName"
            rd = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, load)
            Try
                While rd.Read
                    frmCapitalShare_Deposit_and_Withdrawal.txttotalContri.Text = rd.Item(2).ToString
                    frmCapitalShare_Deposit_and_Withdrawal.lblMemberName.Text = rd.Item(1).ToString
                    frmCapitalShare_Deposit_and_Withdrawal.lblMemberName.Tag = rd.Item(0).ToString
                End While
                rd.Close()
            Catch ex As Exception
            Finally
                gcon.sqlconn.Close()
            End Try
            Me.Close()
            frmCapitalShare_Deposit_and_Withdrawal.ShowDialog()
        Else
            Call PlotviewRecord()
            frmMember_Master.chkBereaveNo.Enabled = False
            frmMember_Master.chkBereaveYes.Enabled = False
            frmMember_Master.DisableHRISButton()
            Me.Close()
        End If

        frmMember_Master.LoadPicture_Signature(frmMember_Master.txtEmployeeNo.Text)
        'frmMember_Master.btnPayrollHistory.Text = "Payroll History"
        'frmMember_Master.lvlPayrollHistory.Clear()
        'frmMember_Master.lvlPayrollHistory.GridLines = False
        'gridExistingMember.Columns.Clear()
    End Sub

    Private Sub txtid_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtid.KeyPress
        If e.KeyChar = Chr(13) Then
            Call GetExistingMember(Me.txtid.Text.Trim)
        End If
    End Sub

    Private Sub txtid_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtid.TextChanged
        Call GetExistingMember(Me.txtid.Text.Trim)
    End Sub
End Class

