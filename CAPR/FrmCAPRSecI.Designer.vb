<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCAPRSecI
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TabControl1 = New System.Windows.Forms.TabControl
        Me.TabPage1 = New System.Windows.Forms.TabPage
        Me.txtBusActEngaged = New System.Windows.Forms.TextBox
        Me.txtCoopType = New System.Windows.Forms.TextBox
        Me.txtCoopAdd = New System.Windows.Forms.TextBox
        Me.txtRegConNo = New System.Windows.Forms.TextBox
        Me.txtCoopName = New System.Windows.Forms.TextBox
        Me.txtCIN = New System.Windows.Forms.TextBox
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.txtOthers = New System.Windows.Forms.TextBox
        Me.ChkOthers = New System.Windows.Forms.CheckBox
        Me.ChkWholesaleRetail = New System.Windows.Forms.CheckBox
        Me.ChkHotel = New System.Windows.Forms.CheckBox
        Me.ChkManufacturing = New System.Windows.Forms.CheckBox
        Me.ChkAgriculture = New System.Windows.Forms.CheckBox
        Me.ChkEducation = New System.Windows.Forms.CheckBox
        Me.ChkElectricity = New System.Windows.Forms.CheckBox
        Me.ChkFishing = New System.Windows.Forms.CheckBox
        Me.ChkHealth = New System.Windows.Forms.CheckBox
        Me.ChkRealEstate = New System.Windows.Forms.CheckBox
        Me.ChkTransport = New System.Windows.Forms.CheckBox
        Me.ChkConstruction = New System.Windows.Forms.CheckBox
        Me.ChkMining = New System.Windows.Forms.CheckBox
        Me.ChkFinance = New System.Windows.Forms.CheckBox
        Me.Label17 = New System.Windows.Forms.Label
        Me.Label16 = New System.Windows.Forms.Label
        Me.Label15 = New System.Windows.Forms.Label
        Me.Label14 = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.TabPage2 = New System.Windows.Forms.TabPage
        Me.txtTargetFemale = New System.Windows.Forms.TextBox
        Me.txtTargetMale = New System.Windows.Forms.TextBox
        Me.txtTotalTarget = New System.Windows.Forms.TextBox
        Me.txtTotalMember = New System.Windows.Forms.TextBox
        Me.txtTotalAss = New System.Windows.Forms.TextBox
        Me.txtTotalReg = New System.Windows.Forms.TextBox
        Me.txtTotalFemale = New System.Windows.Forms.TextBox
        Me.txtTotalMale = New System.Windows.Forms.TextBox
        Me.txtAssFemale = New System.Windows.Forms.TextBox
        Me.txtRegFemale = New System.Windows.Forms.TextBox
        Me.txtAssMale = New System.Windows.Forms.TextBox
        Me.txtRegMale = New System.Windows.Forms.TextBox
        Me.Label26 = New System.Windows.Forms.Label
        Me.Label25 = New System.Windows.Forms.Label
        Me.Label24 = New System.Windows.Forms.Label
        Me.Label30 = New System.Windows.Forms.Label
        Me.Label29 = New System.Windows.Forms.Label
        Me.Label28 = New System.Windows.Forms.Label
        Me.Label27 = New System.Windows.Forms.Label
        Me.Label23 = New System.Windows.Forms.Label
        Me.Label22 = New System.Windows.Forms.Label
        Me.txtFAX = New System.Windows.Forms.TextBox
        Me.txtEmail = New System.Windows.Forms.TextBox
        Me.txtPhoneNo = New System.Windows.Forms.TextBox
        Me.txtContactName = New System.Windows.Forms.TextBox
        Me.Label21 = New System.Windows.Forms.Label
        Me.Label20 = New System.Windows.Forms.Label
        Me.Label19 = New System.Windows.Forms.Label
        Me.Label18 = New System.Windows.Forms.Label
        Me.TabPage3 = New System.Windows.Forms.TabPage
        Me.Label1 = New System.Windows.Forms.Label
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.OpCtrl11 = New OptionalControl.OptionalControl
        Me.Label41 = New System.Windows.Forms.Label
        Me.OpCtrl10c = New OptionalControl.OptionalControl
        Me.OpCtrl10b = New OptionalControl.OptionalControl
        Me.OpCtrl10a = New OptionalControl.OptionalControl
        Me.Label40 = New System.Windows.Forms.Label
        Me.Label37 = New System.Windows.Forms.Label
        Me.Label39 = New System.Windows.Forms.Label
        Me.OpCtrl9k = New OptionalControl.OptionalControl
        Me.OpCtrl9j = New OptionalControl.OptionalControl
        Me.OpCtrl9i = New OptionalControl.OptionalControl
        Me.OpCtrl9h = New OptionalControl.OptionalControl
        Me.OpCtrl9g = New OptionalControl.OptionalControl
        Me.OpCtrl9f = New OptionalControl.OptionalControl
        Me.OpCtrl9e = New OptionalControl.OptionalControl
        Me.OpCtrl9d = New OptionalControl.OptionalControl
        Me.OpCtrl9c = New OptionalControl.OptionalControl
        Me.OpCtrl9b = New OptionalControl.OptionalControl
        Me.OpCtrl9a = New OptionalControl.OptionalControl
        Me.Label36 = New System.Windows.Forms.Label
        Me.OpCtrl8fv = New OptionalControl.OptionalControl
        Me.OpCtrl8fiv = New OptionalControl.OptionalControl
        Me.OpCtrl8fiii = New OptionalControl.OptionalControl
        Me.OpCtrl8fii = New OptionalControl.OptionalControl
        Me.OpCtrl8fi = New OptionalControl.OptionalControl
        Me.Label35 = New System.Windows.Forms.Label
        Me.OpCtrl8eiii = New OptionalControl.OptionalControl
        Me.OpCtrl8eii = New OptionalControl.OptionalControl
        Me.OpCtrl8ei = New OptionalControl.OptionalControl
        Me.Label34 = New System.Windows.Forms.Label
        Me.OpCtrl8d = New OptionalControl.OptionalControl
        Me.OpCtrl8c = New OptionalControl.OptionalControl
        Me.OpCtrl8b = New OptionalControl.OptionalControl
        Me.OpCtrl8a = New OptionalControl.OptionalControl
        Me.Label32 = New System.Windows.Forms.Label
        Me.Label33 = New System.Windows.Forms.Label
        Me.Label38 = New System.Windows.Forms.Label
        Me.OpCtrl7e = New OptionalControl.OptionalControl
        Me.OpCtrl7d = New OptionalControl.OptionalControl
        Me.OpCtrl7c = New OptionalControl.OptionalControl
        Me.OpCtrl7b = New OptionalControl.OptionalControl
        Me.OpCtrl7aIf = New OptionalControl.OptionalControl
        Me.OpCtrl7a = New OptionalControl.OptionalControl
        Me.Label31 = New System.Windows.Forms.Label
        Me.OpCtrl6b = New OptionalControl.OptionalControl
        Me.OpCtrl6a = New OptionalControl.OptionalControl
        Me.Label11 = New System.Windows.Forms.Label
        Me.OpCtrl5e = New OptionalControl.OptionalControl
        Me.OpCtrl5d = New OptionalControl.OptionalControl
        Me.OpCtrl5c = New OptionalControl.OptionalControl
        Me.OpCtrl5b = New OptionalControl.OptionalControl
        Me.OpCtrl5a = New OptionalControl.OptionalControl
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.OpCtrl4d = New OptionalControl.OptionalControl
        Me.OpCtrl4c = New OptionalControl.OptionalControl
        Me.OpCtrl4biii = New OptionalControl.OptionalControl
        Me.OpCtrl4bii = New OptionalControl.OptionalControl
        Me.OpCtrl4bi = New OptionalControl.OptionalControl
        Me.Label7 = New System.Windows.Forms.Label
        Me.OpCtrl4a = New OptionalControl.OptionalControl
        Me.Label6 = New System.Windows.Forms.Label
        Me.OpCtrl3a = New OptionalControl.OptionalControl
        Me.Label5 = New System.Windows.Forms.Label
        Me.OpCtrl2d = New OptionalControl.OptionalControl
        Me.OpCtrl2c = New OptionalControl.OptionalControl
        Me.OpCtrl2b = New OptionalControl.OptionalControl
        Me.OpCtrl2a = New OptionalControl.OptionalControl
        Me.Label4 = New System.Windows.Forms.Label
        Me.OpCtrl1d = New OptionalControl.OptionalControl
        Me.OpCtrl1c = New OptionalControl.OptionalControl
        Me.OpCtrl1bIf = New OptionalControl.OptionalControl
        Me.OpCtrl1b = New OptionalControl.OptionalControl
        Me.OpCtrl1a = New OptionalControl.OptionalControl
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.TabPage4 = New System.Windows.Forms.TabPage
        Me.CboAudFinState = New System.Windows.Forms.ComboBox
        Me.txtExtAudName = New System.Windows.Forms.TextBox
        Me.txtPeriodOpCoverd = New System.Windows.Forms.TextBox
        Me.txtIntAudDesign = New System.Windows.Forms.TextBox
        Me.txtIntAudName = New System.Windows.Forms.TextBox
        Me.dtExtAud = New System.Windows.Forms.DateTimePicker
        Me.dtLastIntAudRpt = New System.Windows.Forms.DateTimePicker
        Me.NumFreqIntAudit = New System.Windows.Forms.NumericUpDown
        Me.Label56 = New System.Windows.Forms.Label
        Me.Label55 = New System.Windows.Forms.Label
        Me.Label54 = New System.Windows.Forms.Label
        Me.Label58 = New System.Windows.Forms.Label
        Me.Label57 = New System.Windows.Forms.Label
        Me.Label53 = New System.Windows.Forms.Label
        Me.Label52 = New System.Windows.Forms.Label
        Me.Label51 = New System.Windows.Forms.Label
        Me.Label50 = New System.Windows.Forms.Label
        Me.Label49 = New System.Windows.Forms.Label
        Me.Label48 = New System.Windows.Forms.Label
        Me.BtnPrevious = New System.Windows.Forms.Button
        Me.BtnNextOK = New System.Windows.Forms.Button
        Me.btnCancel = New System.Windows.Forms.Button
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.TabPage4.SuspendLayout()
        CType(Me.NumFreqIntAudit, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Controls.Add(Me.TabPage4)
        Me.TabControl1.Location = New System.Drawing.Point(3, 2)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(806, 358)
        Me.TabControl1.TabIndex = 17
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.White
        Me.TabPage1.Controls.Add(Me.txtBusActEngaged)
        Me.TabPage1.Controls.Add(Me.txtCoopType)
        Me.TabPage1.Controls.Add(Me.txtCoopAdd)
        Me.TabPage1.Controls.Add(Me.txtRegConNo)
        Me.TabPage1.Controls.Add(Me.txtCoopName)
        Me.TabPage1.Controls.Add(Me.txtCIN)
        Me.TabPage1.Controls.Add(Me.GroupBox3)
        Me.TabPage1.Controls.Add(Me.Label17)
        Me.TabPage1.Controls.Add(Me.Label16)
        Me.TabPage1.Controls.Add(Me.Label15)
        Me.TabPage1.Controls.Add(Me.Label14)
        Me.TabPage1.Controls.Add(Me.Label13)
        Me.TabPage1.Controls.Add(Me.Label12)
        Me.TabPage1.Location = New System.Drawing.Point(4, 24)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(798, 330)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "A - F"
        '
        'txtBusActEngaged
        '
        Me.txtBusActEngaged.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtBusActEngaged.Location = New System.Drawing.Point(282, 154)
        Me.txtBusActEngaged.Name = "txtBusActEngaged"
        Me.txtBusActEngaged.Size = New System.Drawing.Size(506, 23)
        Me.txtBusActEngaged.TabIndex = 26
        '
        'txtCoopType
        '
        Me.txtCoopType.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtCoopType.Location = New System.Drawing.Point(282, 125)
        Me.txtCoopType.Name = "txtCoopType"
        Me.txtCoopType.Size = New System.Drawing.Size(506, 23)
        Me.txtCoopType.TabIndex = 25
        '
        'txtCoopAdd
        '
        Me.txtCoopAdd.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtCoopAdd.Location = New System.Drawing.Point(282, 96)
        Me.txtCoopAdd.Name = "txtCoopAdd"
        Me.txtCoopAdd.Size = New System.Drawing.Size(506, 23)
        Me.txtCoopAdd.TabIndex = 24
        '
        'txtRegConNo
        '
        Me.txtRegConNo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtRegConNo.Location = New System.Drawing.Point(282, 67)
        Me.txtRegConNo.Name = "txtRegConNo"
        Me.txtRegConNo.Size = New System.Drawing.Size(506, 23)
        Me.txtRegConNo.TabIndex = 29
        '
        'txtCoopName
        '
        Me.txtCoopName.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtCoopName.Location = New System.Drawing.Point(282, 38)
        Me.txtCoopName.Name = "txtCoopName"
        Me.txtCoopName.Size = New System.Drawing.Size(506, 23)
        Me.txtCoopName.TabIndex = 28
        '
        'txtCIN
        '
        Me.txtCIN.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtCIN.Location = New System.Drawing.Point(282, 9)
        Me.txtCIN.Name = "txtCIN"
        Me.txtCIN.Size = New System.Drawing.Size(506, 23)
        Me.txtCIN.TabIndex = 27
        '
        'GroupBox3
        '
        Me.GroupBox3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox3.Controls.Add(Me.txtOthers)
        Me.GroupBox3.Controls.Add(Me.ChkOthers)
        Me.GroupBox3.Controls.Add(Me.ChkWholesaleRetail)
        Me.GroupBox3.Controls.Add(Me.ChkHotel)
        Me.GroupBox3.Controls.Add(Me.ChkManufacturing)
        Me.GroupBox3.Controls.Add(Me.ChkAgriculture)
        Me.GroupBox3.Controls.Add(Me.ChkEducation)
        Me.GroupBox3.Controls.Add(Me.ChkElectricity)
        Me.GroupBox3.Controls.Add(Me.ChkFishing)
        Me.GroupBox3.Controls.Add(Me.ChkHealth)
        Me.GroupBox3.Controls.Add(Me.ChkRealEstate)
        Me.GroupBox3.Controls.Add(Me.ChkTransport)
        Me.GroupBox3.Controls.Add(Me.ChkConstruction)
        Me.GroupBox3.Controls.Add(Me.ChkMining)
        Me.GroupBox3.Controls.Add(Me.ChkFinance)
        Me.GroupBox3.Location = New System.Drawing.Point(6, 184)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(782, 140)
        Me.GroupBox3.TabIndex = 23
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "BOX 3: To be filled-up by CDS"
        '
        'txtOthers
        '
        Me.txtOthers.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtOthers.Enabled = False
        Me.txtOthers.Location = New System.Drawing.Point(490, 107)
        Me.txtOthers.Name = "txtOthers"
        Me.txtOthers.Size = New System.Drawing.Size(277, 23)
        Me.txtOthers.TabIndex = 1
        '
        'ChkOthers
        '
        Me.ChkOthers.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ChkOthers.AutoSize = True
        Me.ChkOthers.Location = New System.Drawing.Point(382, 109)
        Me.ChkOthers.Name = "ChkOthers"
        Me.ChkOthers.Size = New System.Drawing.Size(108, 19)
        Me.ChkOthers.TabIndex = 0
        Me.ChkOthers.Text = "Others, Specify"
        Me.ChkOthers.UseVisualStyleBackColor = True
        '
        'ChkWholesaleRetail
        '
        Me.ChkWholesaleRetail.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ChkWholesaleRetail.AutoSize = True
        Me.ChkWholesaleRetail.Location = New System.Drawing.Point(382, 74)
        Me.ChkWholesaleRetail.Name = "ChkWholesaleRetail"
        Me.ChkWholesaleRetail.Size = New System.Drawing.Size(394, 34)
        Me.ChkWholesaleRetail.TabIndex = 0
        Me.ChkWholesaleRetail.Text = "Wholesale and Retail Trade; Repairs of Motor Vehicle, Motorcycles" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "and Personal a" & _
            "nd Household Goods"
        Me.ChkWholesaleRetail.UseVisualStyleBackColor = True
        '
        'ChkHotel
        '
        Me.ChkHotel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ChkHotel.AutoSize = True
        Me.ChkHotel.Location = New System.Drawing.Point(382, 59)
        Me.ChkHotel.Name = "ChkHotel"
        Me.ChkHotel.Size = New System.Drawing.Size(148, 19)
        Me.ChkHotel.TabIndex = 0
        Me.ChkHotel.Text = "Hotel and Restaurants"
        Me.ChkHotel.UseVisualStyleBackColor = True
        '
        'ChkManufacturing
        '
        Me.ChkManufacturing.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ChkManufacturing.AutoSize = True
        Me.ChkManufacturing.Location = New System.Drawing.Point(382, 44)
        Me.ChkManufacturing.Name = "ChkManufacturing"
        Me.ChkManufacturing.Size = New System.Drawing.Size(108, 19)
        Me.ChkManufacturing.TabIndex = 0
        Me.ChkManufacturing.Text = "Manufacturing"
        Me.ChkManufacturing.UseVisualStyleBackColor = True
        '
        'ChkAgriculture
        '
        Me.ChkAgriculture.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ChkAgriculture.AutoSize = True
        Me.ChkAgriculture.Location = New System.Drawing.Point(382, 29)
        Me.ChkAgriculture.Name = "ChkAgriculture"
        Me.ChkAgriculture.Size = New System.Drawing.Size(208, 19)
        Me.ChkAgriculture.TabIndex = 0
        Me.ChkAgriculture.Text = "Agriculture, Hunting and Forestry"
        Me.ChkAgriculture.UseVisualStyleBackColor = True
        '
        'ChkEducation
        '
        Me.ChkEducation.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ChkEducation.AutoSize = True
        Me.ChkEducation.Location = New System.Drawing.Point(382, 14)
        Me.ChkEducation.Name = "ChkEducation"
        Me.ChkEducation.Size = New System.Drawing.Size(81, 19)
        Me.ChkEducation.TabIndex = 0
        Me.ChkEducation.Text = "Education"
        Me.ChkEducation.UseVisualStyleBackColor = True
        '
        'ChkElectricity
        '
        Me.ChkElectricity.AutoSize = True
        Me.ChkElectricity.Location = New System.Drawing.Point(8, 119)
        Me.ChkElectricity.Name = "ChkElectricity"
        Me.ChkElectricity.Size = New System.Drawing.Size(219, 19)
        Me.ChkElectricity.TabIndex = 0
        Me.ChkElectricity.Text = "Electricity, Gas and Water supplies"
        Me.ChkElectricity.UseVisualStyleBackColor = True
        '
        'ChkFishing
        '
        Me.ChkFishing.AutoSize = True
        Me.ChkFishing.Location = New System.Drawing.Point(8, 104)
        Me.ChkFishing.Name = "ChkFishing"
        Me.ChkFishing.Size = New System.Drawing.Size(66, 19)
        Me.ChkFishing.TabIndex = 0
        Me.ChkFishing.Text = "Fishing"
        Me.ChkFishing.UseVisualStyleBackColor = True
        '
        'ChkHealth
        '
        Me.ChkHealth.AutoSize = True
        Me.ChkHealth.Location = New System.Drawing.Point(8, 89)
        Me.ChkHealth.Name = "ChkHealth"
        Me.ChkHealth.Size = New System.Drawing.Size(156, 19)
        Me.ChkHealth.TabIndex = 0
        Me.ChkHealth.Text = "Health and Social Work"
        Me.ChkHealth.UseVisualStyleBackColor = True
        '
        'ChkRealEstate
        '
        Me.ChkRealEstate.AutoSize = True
        Me.ChkRealEstate.Location = New System.Drawing.Point(8, 74)
        Me.ChkRealEstate.Name = "ChkRealEstate"
        Me.ChkRealEstate.Size = New System.Drawing.Size(263, 19)
        Me.ChkRealEstate.TabIndex = 0
        Me.ChkRealEstate.Text = "Real Estate, Renting and Business Activities"
        Me.ChkRealEstate.UseVisualStyleBackColor = True
        '
        'ChkTransport
        '
        Me.ChkTransport.AutoSize = True
        Me.ChkTransport.Location = New System.Drawing.Point(8, 59)
        Me.ChkTransport.Name = "ChkTransport"
        Me.ChkTransport.Size = New System.Drawing.Size(247, 19)
        Me.ChkTransport.TabIndex = 0
        Me.ChkTransport.Text = "Transport, Storage and Communications"
        Me.ChkTransport.UseVisualStyleBackColor = True
        '
        'ChkConstruction
        '
        Me.ChkConstruction.AutoSize = True
        Me.ChkConstruction.Location = New System.Drawing.Point(8, 44)
        Me.ChkConstruction.Name = "ChkConstruction"
        Me.ChkConstruction.Size = New System.Drawing.Size(97, 19)
        Me.ChkConstruction.TabIndex = 0
        Me.ChkConstruction.Text = "Construction"
        Me.ChkConstruction.UseVisualStyleBackColor = True
        '
        'ChkMining
        '
        Me.ChkMining.AutoSize = True
        Me.ChkMining.Location = New System.Drawing.Point(8, 29)
        Me.ChkMining.Name = "ChkMining"
        Me.ChkMining.Size = New System.Drawing.Size(148, 19)
        Me.ChkMining.TabIndex = 0
        Me.ChkMining.Text = "Mining and Quarrying"
        Me.ChkMining.UseVisualStyleBackColor = True
        '
        'ChkFinance
        '
        Me.ChkFinance.AutoSize = True
        Me.ChkFinance.Location = New System.Drawing.Point(8, 14)
        Me.ChkFinance.Name = "ChkFinance"
        Me.ChkFinance.Size = New System.Drawing.Size(163, 19)
        Me.ChkFinance.TabIndex = 0
        Me.ChkFinance.Text = "Financial Intermediation"
        Me.ChkFinance.UseVisualStyleBackColor = True
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(7, 157)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(177, 15)
        Me.Label17.TabIndex = 19
        Me.Label17.Text = "F. Business Activity Engaged in:"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(7, 128)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(130, 15)
        Me.Label16.TabIndex = 18
        Me.Label16.Text = "E. Type of Cooperative:"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(7, 99)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(195, 15)
        Me.Label15.TabIndex = 17
        Me.Label15.Text = "D. Present Address of Cooperative:"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(7, 70)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(214, 15)
        Me.Label14.TabIndex = 22
        Me.Label14.Text = "C. Registration/Confirmation Number:"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(7, 41)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(268, 15)
        Me.Label13.TabIndex = 21
        Me.Label13.Text = "B. Name of Cooperative as of latest amendment:"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(7, 12)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(243, 15)
        Me.Label12.TabIndex = 20
        Me.Label12.Text = "A. Cooperative Identification Number (CIN):"
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.Color.White
        Me.TabPage2.Controls.Add(Me.txtTargetFemale)
        Me.TabPage2.Controls.Add(Me.txtTargetMale)
        Me.TabPage2.Controls.Add(Me.txtTotalTarget)
        Me.TabPage2.Controls.Add(Me.txtTotalMember)
        Me.TabPage2.Controls.Add(Me.txtTotalAss)
        Me.TabPage2.Controls.Add(Me.txtTotalReg)
        Me.TabPage2.Controls.Add(Me.txtTotalFemale)
        Me.TabPage2.Controls.Add(Me.txtTotalMale)
        Me.TabPage2.Controls.Add(Me.txtAssFemale)
        Me.TabPage2.Controls.Add(Me.txtRegFemale)
        Me.TabPage2.Controls.Add(Me.txtAssMale)
        Me.TabPage2.Controls.Add(Me.txtRegMale)
        Me.TabPage2.Controls.Add(Me.Label26)
        Me.TabPage2.Controls.Add(Me.Label25)
        Me.TabPage2.Controls.Add(Me.Label24)
        Me.TabPage2.Controls.Add(Me.Label30)
        Me.TabPage2.Controls.Add(Me.Label29)
        Me.TabPage2.Controls.Add(Me.Label28)
        Me.TabPage2.Controls.Add(Me.Label27)
        Me.TabPage2.Controls.Add(Me.Label23)
        Me.TabPage2.Controls.Add(Me.Label22)
        Me.TabPage2.Controls.Add(Me.txtFAX)
        Me.TabPage2.Controls.Add(Me.txtEmail)
        Me.TabPage2.Controls.Add(Me.txtPhoneNo)
        Me.TabPage2.Controls.Add(Me.txtContactName)
        Me.TabPage2.Controls.Add(Me.Label21)
        Me.TabPage2.Controls.Add(Me.Label20)
        Me.TabPage2.Controls.Add(Me.Label19)
        Me.TabPage2.Controls.Add(Me.Label18)
        Me.TabPage2.Location = New System.Drawing.Point(4, 24)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(798, 330)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "G - H"
        '
        'txtTargetFemale
        '
        Me.txtTargetFemale.Location = New System.Drawing.Point(355, 275)
        Me.txtTargetFemale.Name = "txtTargetFemale"
        Me.txtTargetFemale.Size = New System.Drawing.Size(77, 23)
        Me.txtTargetFemale.TabIndex = 33
        Me.txtTargetFemale.Text = "0"
        Me.txtTargetFemale.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTargetMale
        '
        Me.txtTargetMale.Location = New System.Drawing.Point(276, 275)
        Me.txtTargetMale.Name = "txtTargetMale"
        Me.txtTargetMale.Size = New System.Drawing.Size(77, 23)
        Me.txtTargetMale.TabIndex = 37
        Me.txtTargetMale.Text = "0"
        Me.txtTargetMale.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotalTarget
        '
        Me.txtTotalTarget.Location = New System.Drawing.Point(434, 275)
        Me.txtTotalTarget.Name = "txtTotalTarget"
        Me.txtTotalTarget.ReadOnly = True
        Me.txtTotalTarget.Size = New System.Drawing.Size(77, 23)
        Me.txtTotalTarget.TabIndex = 38
        Me.txtTotalTarget.Text = "0"
        Me.txtTotalTarget.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotalMember
        '
        Me.txtTotalMember.Location = New System.Drawing.Point(434, 246)
        Me.txtTotalMember.Name = "txtTotalMember"
        Me.txtTotalMember.ReadOnly = True
        Me.txtTotalMember.Size = New System.Drawing.Size(77, 23)
        Me.txtTotalMember.TabIndex = 36
        Me.txtTotalMember.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotalAss
        '
        Me.txtTotalAss.Location = New System.Drawing.Point(434, 217)
        Me.txtTotalAss.Name = "txtTotalAss"
        Me.txtTotalAss.ReadOnly = True
        Me.txtTotalAss.Size = New System.Drawing.Size(77, 23)
        Me.txtTotalAss.TabIndex = 34
        Me.txtTotalAss.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotalReg
        '
        Me.txtTotalReg.Location = New System.Drawing.Point(434, 188)
        Me.txtTotalReg.Name = "txtTotalReg"
        Me.txtTotalReg.ReadOnly = True
        Me.txtTotalReg.Size = New System.Drawing.Size(77, 23)
        Me.txtTotalReg.TabIndex = 35
        Me.txtTotalReg.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotalFemale
        '
        Me.txtTotalFemale.Location = New System.Drawing.Point(355, 246)
        Me.txtTotalFemale.Name = "txtTotalFemale"
        Me.txtTotalFemale.ReadOnly = True
        Me.txtTotalFemale.Size = New System.Drawing.Size(77, 23)
        Me.txtTotalFemale.TabIndex = 42
        Me.txtTotalFemale.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotalMale
        '
        Me.txtTotalMale.Location = New System.Drawing.Point(276, 246)
        Me.txtTotalMale.Name = "txtTotalMale"
        Me.txtTotalMale.ReadOnly = True
        Me.txtTotalMale.Size = New System.Drawing.Size(77, 23)
        Me.txtTotalMale.TabIndex = 43
        Me.txtTotalMale.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtAssFemale
        '
        Me.txtAssFemale.Location = New System.Drawing.Point(355, 217)
        Me.txtAssFemale.Name = "txtAssFemale"
        Me.txtAssFemale.ReadOnly = True
        Me.txtAssFemale.Size = New System.Drawing.Size(77, 23)
        Me.txtAssFemale.TabIndex = 41
        Me.txtAssFemale.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtRegFemale
        '
        Me.txtRegFemale.Location = New System.Drawing.Point(355, 189)
        Me.txtRegFemale.Name = "txtRegFemale"
        Me.txtRegFemale.ReadOnly = True
        Me.txtRegFemale.Size = New System.Drawing.Size(77, 23)
        Me.txtRegFemale.TabIndex = 39
        Me.txtRegFemale.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtAssMale
        '
        Me.txtAssMale.Location = New System.Drawing.Point(276, 217)
        Me.txtAssMale.Name = "txtAssMale"
        Me.txtAssMale.ReadOnly = True
        Me.txtAssMale.Size = New System.Drawing.Size(77, 23)
        Me.txtAssMale.TabIndex = 40
        Me.txtAssMale.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtRegMale
        '
        Me.txtRegMale.Location = New System.Drawing.Point(276, 189)
        Me.txtRegMale.Name = "txtRegMale"
        Me.txtRegMale.ReadOnly = True
        Me.txtRegMale.Size = New System.Drawing.Size(77, 23)
        Me.txtRegMale.TabIndex = 32
        Me.txtRegMale.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(56, 278)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(168, 15)
        Me.Label26.TabIndex = 26
        Me.Label26.Text = "Target/Potential Membership"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(56, 249)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(124, 15)
        Me.Label25.TabIndex = 27
        Me.Label25.Text = "Total No. of Members"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(56, 220)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(148, 15)
        Me.Label24.TabIndex = 24
        Me.Label24.Text = "No. of Associate members"
        '
        'Label30
        '
        Me.Label30.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label30.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label30.ForeColor = System.Drawing.Color.White
        Me.Label30.Location = New System.Drawing.Point(434, 168)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(77, 18)
        Me.Label30.TabIndex = 25
        Me.Label30.Text = "Total"
        Me.Label30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label29
        '
        Me.Label29.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label29.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label29.ForeColor = System.Drawing.Color.White
        Me.Label29.Location = New System.Drawing.Point(355, 168)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(77, 18)
        Me.Label29.TabIndex = 28
        Me.Label29.Text = "Female"
        Me.Label29.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label28
        '
        Me.Label28.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label28.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label28.ForeColor = System.Drawing.Color.White
        Me.Label28.Location = New System.Drawing.Point(276, 168)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(77, 18)
        Me.Label28.TabIndex = 31
        Me.Label28.Text = "Male"
        Me.Label28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label27
        '
        Me.Label27.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label27.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label27.ForeColor = System.Drawing.Color.White
        Me.Label27.Location = New System.Drawing.Point(45, 168)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(229, 18)
        Me.Label27.TabIndex = 30
        Me.Label27.Text = "Particulars"
        Me.Label27.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(56, 192)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(137, 15)
        Me.Label23.TabIndex = 29
        Me.Label23.Text = "No. of Regular members"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(3, 141)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(241, 15)
        Me.Label22.TabIndex = 23
        Me.Label22.Text = "H. Information on Members of Cooperative"
        '
        'txtFAX
        '
        Me.txtFAX.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtFAX.Location = New System.Drawing.Point(164, 93)
        Me.txtFAX.Name = "txtFAX"
        Me.txtFAX.Size = New System.Drawing.Size(624, 23)
        Me.txtFAX.TabIndex = 19
        '
        'txtEmail
        '
        Me.txtEmail.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtEmail.Location = New System.Drawing.Point(164, 64)
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.Size = New System.Drawing.Size(624, 23)
        Me.txtEmail.TabIndex = 20
        '
        'txtPhoneNo
        '
        Me.txtPhoneNo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtPhoneNo.Location = New System.Drawing.Point(164, 35)
        Me.txtPhoneNo.Name = "txtPhoneNo"
        Me.txtPhoneNo.Size = New System.Drawing.Size(624, 23)
        Me.txtPhoneNo.TabIndex = 21
        '
        'txtContactName
        '
        Me.txtContactName.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtContactName.Location = New System.Drawing.Point(164, 6)
        Me.txtContactName.Name = "txtContactName"
        Me.txtContactName.Size = New System.Drawing.Size(624, 23)
        Me.txtContactName.TabIndex = 22
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(30, 67)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(100, 15)
        Me.Label21.TabIndex = 17
        Me.Label21.Text = "c. Email Address:"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(30, 96)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(85, 15)
        Me.Label20.TabIndex = 16
        Me.Label20.Text = "b. Fax Number"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(30, 38)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(103, 15)
        Me.Label19.TabIndex = 18
        Me.Label19.Text = "a. Phone Number:"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(3, 9)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(155, 15)
        Me.Label18.TabIndex = 15
        Me.Label18.Text = "G. Name of Contact Person:"
        '
        'TabPage3
        '
        Me.TabPage3.BackColor = System.Drawing.Color.White
        Me.TabPage3.Controls.Add(Me.Label1)
        Me.TabPage3.Controls.Add(Me.Panel1)
        Me.TabPage3.Location = New System.Drawing.Point(4, 24)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(798, 330)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "I"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 7)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(260, 15)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "I. Information on Compliance to Requirements"
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel1.AutoScroll = True
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel1.Controls.Add(Me.OpCtrl11)
        Me.Panel1.Controls.Add(Me.Label41)
        Me.Panel1.Controls.Add(Me.OpCtrl10c)
        Me.Panel1.Controls.Add(Me.OpCtrl10b)
        Me.Panel1.Controls.Add(Me.OpCtrl10a)
        Me.Panel1.Controls.Add(Me.Label40)
        Me.Panel1.Controls.Add(Me.Label37)
        Me.Panel1.Controls.Add(Me.Label39)
        Me.Panel1.Controls.Add(Me.OpCtrl9k)
        Me.Panel1.Controls.Add(Me.OpCtrl9j)
        Me.Panel1.Controls.Add(Me.OpCtrl9i)
        Me.Panel1.Controls.Add(Me.OpCtrl9h)
        Me.Panel1.Controls.Add(Me.OpCtrl9g)
        Me.Panel1.Controls.Add(Me.OpCtrl9f)
        Me.Panel1.Controls.Add(Me.OpCtrl9e)
        Me.Panel1.Controls.Add(Me.OpCtrl9d)
        Me.Panel1.Controls.Add(Me.OpCtrl9c)
        Me.Panel1.Controls.Add(Me.OpCtrl9b)
        Me.Panel1.Controls.Add(Me.OpCtrl9a)
        Me.Panel1.Controls.Add(Me.Label36)
        Me.Panel1.Controls.Add(Me.OpCtrl8fv)
        Me.Panel1.Controls.Add(Me.OpCtrl8fiv)
        Me.Panel1.Controls.Add(Me.OpCtrl8fiii)
        Me.Panel1.Controls.Add(Me.OpCtrl8fii)
        Me.Panel1.Controls.Add(Me.OpCtrl8fi)
        Me.Panel1.Controls.Add(Me.Label35)
        Me.Panel1.Controls.Add(Me.OpCtrl8eiii)
        Me.Panel1.Controls.Add(Me.OpCtrl8eii)
        Me.Panel1.Controls.Add(Me.OpCtrl8ei)
        Me.Panel1.Controls.Add(Me.Label34)
        Me.Panel1.Controls.Add(Me.OpCtrl8d)
        Me.Panel1.Controls.Add(Me.OpCtrl8c)
        Me.Panel1.Controls.Add(Me.OpCtrl8b)
        Me.Panel1.Controls.Add(Me.OpCtrl8a)
        Me.Panel1.Controls.Add(Me.Label32)
        Me.Panel1.Controls.Add(Me.Label33)
        Me.Panel1.Controls.Add(Me.Label38)
        Me.Panel1.Controls.Add(Me.OpCtrl7e)
        Me.Panel1.Controls.Add(Me.OpCtrl7d)
        Me.Panel1.Controls.Add(Me.OpCtrl7c)
        Me.Panel1.Controls.Add(Me.OpCtrl7b)
        Me.Panel1.Controls.Add(Me.OpCtrl7aIf)
        Me.Panel1.Controls.Add(Me.OpCtrl7a)
        Me.Panel1.Controls.Add(Me.Label31)
        Me.Panel1.Controls.Add(Me.OpCtrl6b)
        Me.Panel1.Controls.Add(Me.OpCtrl6a)
        Me.Panel1.Controls.Add(Me.Label11)
        Me.Panel1.Controls.Add(Me.OpCtrl5e)
        Me.Panel1.Controls.Add(Me.OpCtrl5d)
        Me.Panel1.Controls.Add(Me.OpCtrl5c)
        Me.Panel1.Controls.Add(Me.OpCtrl5b)
        Me.Panel1.Controls.Add(Me.OpCtrl5a)
        Me.Panel1.Controls.Add(Me.Label10)
        Me.Panel1.Controls.Add(Me.Label9)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.OpCtrl4d)
        Me.Panel1.Controls.Add(Me.OpCtrl4c)
        Me.Panel1.Controls.Add(Me.OpCtrl4biii)
        Me.Panel1.Controls.Add(Me.OpCtrl4bii)
        Me.Panel1.Controls.Add(Me.OpCtrl4bi)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.OpCtrl4a)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.OpCtrl3a)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.OpCtrl2d)
        Me.Panel1.Controls.Add(Me.OpCtrl2c)
        Me.Panel1.Controls.Add(Me.OpCtrl2b)
        Me.Panel1.Controls.Add(Me.OpCtrl2a)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.OpCtrl1d)
        Me.Panel1.Controls.Add(Me.OpCtrl1c)
        Me.Panel1.Controls.Add(Me.OpCtrl1bIf)
        Me.Panel1.Controls.Add(Me.OpCtrl1b)
        Me.Panel1.Controls.Add(Me.OpCtrl1a)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Location = New System.Drawing.Point(6, 25)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(784, 289)
        Me.Panel1.TabIndex = 0
        '
        'OpCtrl11
        '
        Me.OpCtrl11.AllowUserToSizeOptionWidth = False
        Me.OpCtrl11.BackColor = System.Drawing.Color.White
        Me.OpCtrl11.ConditionText = "11. Do the BOD and Management conduct monthly review and assessment of the actual" & _
            " performance in relation to its target?"
        Me.OpCtrl11.Dock = System.Windows.Forms.DockStyle.Top
        Me.OpCtrl11.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OpCtrl11.Location = New System.Drawing.Point(0, 1821)
        Me.OpCtrl11.Name = "OpCtrl11"
        Me.OpCtrl11.Option1Text = "Yes"
        Me.OpCtrl11.Option2Text = "No"
        Me.OpCtrl11.Size = New System.Drawing.Size(763, 41)
        Me.OpCtrl11.TabIndex = 105
        Me.OpCtrl11.Yes = True
        '
        'Label41
        '
        Me.Label41.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label41.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label41.Location = New System.Drawing.Point(0, 1806)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(763, 15)
        Me.Label41.TabIndex = 104
        '
        'OpCtrl10c
        '
        Me.OpCtrl10c.AllowUserToSizeOptionWidth = False
        Me.OpCtrl10c.BackColor = System.Drawing.Color.White
        Me.OpCtrl10c.ConditionText = "    c. Annual plan and budget"
        Me.OpCtrl10c.Dock = System.Windows.Forms.DockStyle.Top
        Me.OpCtrl10c.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OpCtrl10c.Location = New System.Drawing.Point(0, 1784)
        Me.OpCtrl10c.Name = "OpCtrl10c"
        Me.OpCtrl10c.Option1Text = "Yes"
        Me.OpCtrl10c.Option2Text = "No"
        Me.OpCtrl10c.Size = New System.Drawing.Size(763, 22)
        Me.OpCtrl10c.TabIndex = 103
        Me.OpCtrl10c.Yes = True
        '
        'OpCtrl10b
        '
        Me.OpCtrl10b.AllowUserToSizeOptionWidth = False
        Me.OpCtrl10b.BackColor = System.Drawing.Color.White
        Me.OpCtrl10b.ConditionText = "    b. Development or strategic plan"
        Me.OpCtrl10b.Dock = System.Windows.Forms.DockStyle.Top
        Me.OpCtrl10b.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OpCtrl10b.Location = New System.Drawing.Point(0, 1762)
        Me.OpCtrl10b.Name = "OpCtrl10b"
        Me.OpCtrl10b.Option1Text = "Yes"
        Me.OpCtrl10b.Option2Text = "No"
        Me.OpCtrl10b.Size = New System.Drawing.Size(763, 22)
        Me.OpCtrl10b.TabIndex = 102
        Me.OpCtrl10b.Yes = True
        '
        'OpCtrl10a
        '
        Me.OpCtrl10a.AllowUserToSizeOptionWidth = False
        Me.OpCtrl10a.BackColor = System.Drawing.Color.White
        Me.OpCtrl10a.ConditionText = "    a. Vision, statement of mission and goals"
        Me.OpCtrl10a.Dock = System.Windows.Forms.DockStyle.Top
        Me.OpCtrl10a.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OpCtrl10a.Location = New System.Drawing.Point(0, 1740)
        Me.OpCtrl10a.Name = "OpCtrl10a"
        Me.OpCtrl10a.Option1Text = "Yes"
        Me.OpCtrl10a.Option2Text = "No"
        Me.OpCtrl10a.Size = New System.Drawing.Size(763, 22)
        Me.OpCtrl10a.TabIndex = 101
        Me.OpCtrl10a.Yes = True
        '
        'Label40
        '
        Me.Label40.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label40.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label40.Location = New System.Drawing.Point(0, 1722)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(763, 18)
        Me.Label40.TabIndex = 100
        Me.Label40.Text = "10. Does the cooperative have defined objectives, plans and programs particularly" & _
            " on the:"
        '
        'Label37
        '
        Me.Label37.BackColor = System.Drawing.Color.Green
        Me.Label37.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label37.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label37.ForeColor = System.Drawing.Color.White
        Me.Label37.Location = New System.Drawing.Point(0, 1701)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(763, 21)
        Me.Label37.TabIndex = 99
        Me.Label37.Text = "PLANS / PROGRAMS AND PERFORMANCE"
        Me.Label37.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label39
        '
        Me.Label39.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label39.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label39.Location = New System.Drawing.Point(0, 1683)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(763, 18)
        Me.Label39.TabIndex = 98
        '
        'OpCtrl9k
        '
        Me.OpCtrl9k.AllowUserToSizeOptionWidth = False
        Me.OpCtrl9k.BackColor = System.Drawing.Color.White
        Me.OpCtrl9k.ConditionText = "    k. Does the cooperative have an organizational structure?"
        Me.OpCtrl9k.Dock = System.Windows.Forms.DockStyle.Top
        Me.OpCtrl9k.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OpCtrl9k.Location = New System.Drawing.Point(0, 1661)
        Me.OpCtrl9k.Name = "OpCtrl9k"
        Me.OpCtrl9k.Option1Text = "Yes"
        Me.OpCtrl9k.Option2Text = "No"
        Me.OpCtrl9k.Size = New System.Drawing.Size(763, 22)
        Me.OpCtrl9k.TabIndex = 97
        Me.OpCtrl9k.Yes = True
        '
        'OpCtrl9j
        '
        Me.OpCtrl9j.AllowUserToSizeOptionWidth = False
        Me.OpCtrl9j.BackColor = System.Drawing.Color.White
        Me.OpCtrl9j.ConditionText = "    j. Do all employees have individual personnel files?"
        Me.OpCtrl9j.Dock = System.Windows.Forms.DockStyle.Top
        Me.OpCtrl9j.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OpCtrl9j.Location = New System.Drawing.Point(0, 1639)
        Me.OpCtrl9j.Name = "OpCtrl9j"
        Me.OpCtrl9j.Option1Text = "Yes"
        Me.OpCtrl9j.Option2Text = "No"
        Me.OpCtrl9j.Size = New System.Drawing.Size(763, 22)
        Me.OpCtrl9j.TabIndex = 96
        Me.OpCtrl9j.Yes = True
        '
        'OpCtrl9i
        '
        Me.OpCtrl9i.AllowUserToSizeOptionWidth = False
        Me.OpCtrl9i.BackColor = System.Drawing.Color.White
        Me.OpCtrl9i.ConditionText = "    i. Is there a policy on sucession of the manager and other key top positions?" & _
            ""
        Me.OpCtrl9i.Dock = System.Windows.Forms.DockStyle.Top
        Me.OpCtrl9i.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OpCtrl9i.Location = New System.Drawing.Point(0, 1617)
        Me.OpCtrl9i.Name = "OpCtrl9i"
        Me.OpCtrl9i.Option1Text = "Yes"
        Me.OpCtrl9i.Option2Text = "No"
        Me.OpCtrl9i.Size = New System.Drawing.Size(763, 22)
        Me.OpCtrl9i.TabIndex = 95
        Me.OpCtrl9i.Yes = True
        '
        'OpCtrl9h
        '
        Me.OpCtrl9h.AllowUserToSizeOptionWidth = False
        Me.OpCtrl9h.BackColor = System.Drawing.Color.White
        Me.OpCtrl9h.ConditionText = "    h. Is there a full-time and qualified manager in the coop?"
        Me.OpCtrl9h.Dock = System.Windows.Forms.DockStyle.Top
        Me.OpCtrl9h.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OpCtrl9h.Location = New System.Drawing.Point(0, 1595)
        Me.OpCtrl9h.Name = "OpCtrl9h"
        Me.OpCtrl9h.Option1Text = "Yes"
        Me.OpCtrl9h.Option2Text = "No"
        Me.OpCtrl9h.Size = New System.Drawing.Size(763, 22)
        Me.OpCtrl9h.TabIndex = 94
        Me.OpCtrl9h.Yes = True
        '
        'OpCtrl9g
        '
        Me.OpCtrl9g.AllowUserToSizeOptionWidth = False
        Me.OpCtrl9g.BackColor = System.Drawing.Color.White
        Me.OpCtrl9g.ConditionText = "    g. Are all the meetings of the committees properly recorded?"
        Me.OpCtrl9g.Dock = System.Windows.Forms.DockStyle.Top
        Me.OpCtrl9g.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OpCtrl9g.Location = New System.Drawing.Point(0, 1573)
        Me.OpCtrl9g.Name = "OpCtrl9g"
        Me.OpCtrl9g.Option1Text = "Yes"
        Me.OpCtrl9g.Option2Text = "No"
        Me.OpCtrl9g.Size = New System.Drawing.Size(763, 22)
        Me.OpCtrl9g.TabIndex = 93
        Me.OpCtrl9g.Yes = True
        '
        'OpCtrl9f
        '
        Me.OpCtrl9f.AllowUserToSizeOptionWidth = False
        Me.OpCtrl9f.BackColor = System.Drawing.Color.White
        Me.OpCtrl9f.ConditionText = "    f. Are all the meetings of the Board of Directors properly recorded and updat" & _
            "ed?"
        Me.OpCtrl9f.Dock = System.Windows.Forms.DockStyle.Top
        Me.OpCtrl9f.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OpCtrl9f.Location = New System.Drawing.Point(0, 1551)
        Me.OpCtrl9f.Name = "OpCtrl9f"
        Me.OpCtrl9f.Option1Text = "Yes"
        Me.OpCtrl9f.Option2Text = "No"
        Me.OpCtrl9f.Size = New System.Drawing.Size(763, 22)
        Me.OpCtrl9f.TabIndex = 92
        Me.OpCtrl9f.Yes = True
        '
        'OpCtrl9e
        '
        Me.OpCtrl9e.AllowUserToSizeOptionWidth = False
        Me.OpCtrl9e.BackColor = System.Drawing.Color.White
        Me.OpCtrl9e.ConditionText = "    e. Does the Board conduct periodic review of policies?"
        Me.OpCtrl9e.Dock = System.Windows.Forms.DockStyle.Top
        Me.OpCtrl9e.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OpCtrl9e.Location = New System.Drawing.Point(0, 1529)
        Me.OpCtrl9e.Name = "OpCtrl9e"
        Me.OpCtrl9e.Option1Text = "Yes"
        Me.OpCtrl9e.Option2Text = "No"
        Me.OpCtrl9e.Size = New System.Drawing.Size(763, 22)
        Me.OpCtrl9e.TabIndex = 91
        Me.OpCtrl9e.Yes = True
        '
        'OpCtrl9d
        '
        Me.OpCtrl9d.AllowUserToSizeOptionWidth = False
        Me.OpCtrl9d.BackColor = System.Drawing.Color.White
        Me.OpCtrl9d.ConditionText = "    d.  Do all committees meet on a regular basis (at least once a month)?"
        Me.OpCtrl9d.Dock = System.Windows.Forms.DockStyle.Top
        Me.OpCtrl9d.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OpCtrl9d.Location = New System.Drawing.Point(0, 1507)
        Me.OpCtrl9d.Name = "OpCtrl9d"
        Me.OpCtrl9d.Option1Text = "Yes"
        Me.OpCtrl9d.Option2Text = "No"
        Me.OpCtrl9d.Size = New System.Drawing.Size(763, 22)
        Me.OpCtrl9d.TabIndex = 90
        Me.OpCtrl9d.Yes = True
        '
        'OpCtrl9c
        '
        Me.OpCtrl9c.AllowUserToSizeOptionWidth = False
        Me.OpCtrl9c.BackColor = System.Drawing.Color.White
        Me.OpCtrl9c.ConditionText = "    c. Does the Board of Directors meet on a regular basis (at least once a month" & _
            ")?"
        Me.OpCtrl9c.Dock = System.Windows.Forms.DockStyle.Top
        Me.OpCtrl9c.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OpCtrl9c.Location = New System.Drawing.Point(0, 1485)
        Me.OpCtrl9c.Name = "OpCtrl9c"
        Me.OpCtrl9c.Option1Text = "Yes"
        Me.OpCtrl9c.Option2Text = "No"
        Me.OpCtrl9c.Size = New System.Drawing.Size(763, 22)
        Me.OpCtrl9c.TabIndex = 89
        Me.OpCtrl9c.Yes = True
        '
        'OpCtrl9b
        '
        Me.OpCtrl9b.AllowUserToSizeOptionWidth = False
        Me.OpCtrl9b.BackColor = System.Drawing.Color.White
        Me.OpCtrl9b.ConditionText = "    b. Are the BOD and Committee members elected in accordance with the coop by-l" & _
            "aws and election procedures / guidlines?"
        Me.OpCtrl9b.Dock = System.Windows.Forms.DockStyle.Top
        Me.OpCtrl9b.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OpCtrl9b.Location = New System.Drawing.Point(0, 1444)
        Me.OpCtrl9b.Name = "OpCtrl9b"
        Me.OpCtrl9b.Option1Text = "Yes"
        Me.OpCtrl9b.Option2Text = "No"
        Me.OpCtrl9b.Size = New System.Drawing.Size(763, 41)
        Me.OpCtrl9b.TabIndex = 88
        Me.OpCtrl9b.Yes = True
        '
        'OpCtrl9a
        '
        Me.OpCtrl9a.AllowUserToSizeOptionWidth = False
        Me.OpCtrl9a.BackColor = System.Drawing.Color.White
        Me.OpCtrl9a.ConditionText = "    a. Does the cooperative have Code of governance and ethical standards?"
        Me.OpCtrl9a.Dock = System.Windows.Forms.DockStyle.Top
        Me.OpCtrl9a.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OpCtrl9a.Location = New System.Drawing.Point(0, 1422)
        Me.OpCtrl9a.Name = "OpCtrl9a"
        Me.OpCtrl9a.Option1Text = "Yes"
        Me.OpCtrl9a.Option2Text = "No"
        Me.OpCtrl9a.Size = New System.Drawing.Size(763, 22)
        Me.OpCtrl9a.TabIndex = 87
        Me.OpCtrl9a.Yes = True
        '
        'Label36
        '
        Me.Label36.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label36.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label36.Location = New System.Drawing.Point(0, 1392)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(763, 30)
        Me.Label36.TabIndex = 86
        Me.Label36.Text = "9. Governance and Management"
        Me.Label36.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'OpCtrl8fv
        '
        Me.OpCtrl8fv.AllowUserToSizeOptionWidth = False
        Me.OpCtrl8fv.BackColor = System.Drawing.Color.White
        Me.OpCtrl8fv.ConditionText = "        v. Job description"
        Me.OpCtrl8fv.Dock = System.Windows.Forms.DockStyle.Top
        Me.OpCtrl8fv.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OpCtrl8fv.Location = New System.Drawing.Point(0, 1370)
        Me.OpCtrl8fv.Name = "OpCtrl8fv"
        Me.OpCtrl8fv.Option1Text = "Yes"
        Me.OpCtrl8fv.Option2Text = "No"
        Me.OpCtrl8fv.Size = New System.Drawing.Size(763, 22)
        Me.OpCtrl8fv.TabIndex = 85
        Me.OpCtrl8fv.Yes = True
        '
        'OpCtrl8fiv
        '
        Me.OpCtrl8fiv.AllowUserToSizeOptionWidth = False
        Me.OpCtrl8fiv.BackColor = System.Drawing.Color.White
        Me.OpCtrl8fiv.ConditionText = "        iv. Performance appraisal"
        Me.OpCtrl8fiv.Dock = System.Windows.Forms.DockStyle.Top
        Me.OpCtrl8fiv.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OpCtrl8fiv.Location = New System.Drawing.Point(0, 1348)
        Me.OpCtrl8fiv.Name = "OpCtrl8fiv"
        Me.OpCtrl8fiv.Option1Text = "Yes"
        Me.OpCtrl8fiv.Option2Text = "No"
        Me.OpCtrl8fiv.Size = New System.Drawing.Size(763, 22)
        Me.OpCtrl8fiv.TabIndex = 84
        Me.OpCtrl8fiv.Yes = True
        '
        'OpCtrl8fiii
        '
        Me.OpCtrl8fiii.AllowUserToSizeOptionWidth = False
        Me.OpCtrl8fiii.BackColor = System.Drawing.Color.White
        Me.OpCtrl8fiii.ConditionText = "        iii. Compensation  / benefits"
        Me.OpCtrl8fiii.Dock = System.Windows.Forms.DockStyle.Top
        Me.OpCtrl8fiii.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OpCtrl8fiii.Location = New System.Drawing.Point(0, 1326)
        Me.OpCtrl8fiii.Name = "OpCtrl8fiii"
        Me.OpCtrl8fiii.Option1Text = "Yes"
        Me.OpCtrl8fiii.Option2Text = "No"
        Me.OpCtrl8fiii.Size = New System.Drawing.Size(763, 22)
        Me.OpCtrl8fiii.TabIndex = 83
        Me.OpCtrl8fiii.Yes = True
        '
        'OpCtrl8fii
        '
        Me.OpCtrl8fii.AllowUserToSizeOptionWidth = False
        Me.OpCtrl8fii.BackColor = System.Drawing.Color.White
        Me.OpCtrl8fii.ConditionText = "        ii. Staff development"
        Me.OpCtrl8fii.Dock = System.Windows.Forms.DockStyle.Top
        Me.OpCtrl8fii.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OpCtrl8fii.Location = New System.Drawing.Point(0, 1304)
        Me.OpCtrl8fii.Name = "OpCtrl8fii"
        Me.OpCtrl8fii.Option1Text = "Yes"
        Me.OpCtrl8fii.Option2Text = "No"
        Me.OpCtrl8fii.Size = New System.Drawing.Size(763, 22)
        Me.OpCtrl8fii.TabIndex = 82
        Me.OpCtrl8fii.Yes = True
        '
        'OpCtrl8fi
        '
        Me.OpCtrl8fi.AllowUserToSizeOptionWidth = False
        Me.OpCtrl8fi.BackColor = System.Drawing.Color.White
        Me.OpCtrl8fi.ConditionText = "        i. Hiring, promotion and firing"
        Me.OpCtrl8fi.Dock = System.Windows.Forms.DockStyle.Top
        Me.OpCtrl8fi.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OpCtrl8fi.Location = New System.Drawing.Point(0, 1282)
        Me.OpCtrl8fi.Name = "OpCtrl8fi"
        Me.OpCtrl8fi.Option1Text = "Yes"
        Me.OpCtrl8fi.Option2Text = "No"
        Me.OpCtrl8fi.Size = New System.Drawing.Size(763, 22)
        Me.OpCtrl8fi.TabIndex = 81
        Me.OpCtrl8fi.Yes = True
        '
        'Label35
        '
        Me.Label35.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label35.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label35.Location = New System.Drawing.Point(0, 1264)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(763, 18)
        Me.Label35.TabIndex = 80
        Me.Label35.Text = "    f. Human resource development"
        '
        'OpCtrl8eiii
        '
        Me.OpCtrl8eiii.AllowUserToSizeOptionWidth = False
        Me.OpCtrl8eiii.BackColor = System.Drawing.Color.White
        Me.OpCtrl8eiii.ConditionText = "        iii. Internal control and audit"
        Me.OpCtrl8eiii.Dock = System.Windows.Forms.DockStyle.Top
        Me.OpCtrl8eiii.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OpCtrl8eiii.Location = New System.Drawing.Point(0, 1242)
        Me.OpCtrl8eiii.Name = "OpCtrl8eiii"
        Me.OpCtrl8eiii.Option1Text = "Yes"
        Me.OpCtrl8eiii.Option2Text = "No"
        Me.OpCtrl8eiii.Size = New System.Drawing.Size(763, 22)
        Me.OpCtrl8eiii.TabIndex = 79
        Me.OpCtrl8eiii.Yes = True
        '
        'OpCtrl8eii
        '
        Me.OpCtrl8eii.AllowUserToSizeOptionWidth = False
        Me.OpCtrl8eii.BackColor = System.Drawing.Color.White
        Me.OpCtrl8eii.ConditionText = "        ii. Accounting"
        Me.OpCtrl8eii.Dock = System.Windows.Forms.DockStyle.Top
        Me.OpCtrl8eii.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OpCtrl8eii.Location = New System.Drawing.Point(0, 1220)
        Me.OpCtrl8eii.Name = "OpCtrl8eii"
        Me.OpCtrl8eii.Option1Text = "Yes"
        Me.OpCtrl8eii.Option2Text = "No"
        Me.OpCtrl8eii.Size = New System.Drawing.Size(763, 22)
        Me.OpCtrl8eii.TabIndex = 78
        Me.OpCtrl8eii.Yes = True
        '
        'OpCtrl8ei
        '
        Me.OpCtrl8ei.AllowUserToSizeOptionWidth = False
        Me.OpCtrl8ei.BackColor = System.Drawing.Color.White
        Me.OpCtrl8ei.ConditionText = "        i. Assets / liability management"
        Me.OpCtrl8ei.Dock = System.Windows.Forms.DockStyle.Top
        Me.OpCtrl8ei.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OpCtrl8ei.Location = New System.Drawing.Point(0, 1198)
        Me.OpCtrl8ei.Name = "OpCtrl8ei"
        Me.OpCtrl8ei.Option1Text = "Yes"
        Me.OpCtrl8ei.Option2Text = "No"
        Me.OpCtrl8ei.Size = New System.Drawing.Size(763, 22)
        Me.OpCtrl8ei.TabIndex = 77
        Me.OpCtrl8ei.Yes = True
        '
        'Label34
        '
        Me.Label34.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label34.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label34.Location = New System.Drawing.Point(0, 1180)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(763, 18)
        Me.Label34.TabIndex = 76
        Me.Label34.Text = "    e. Financial Management"
        '
        'OpCtrl8d
        '
        Me.OpCtrl8d.AllowUserToSizeOptionWidth = False
        Me.OpCtrl8d.BackColor = System.Drawing.Color.White
        Me.OpCtrl8d.ConditionText = "    d. Time deposits"
        Me.OpCtrl8d.Dock = System.Windows.Forms.DockStyle.Top
        Me.OpCtrl8d.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OpCtrl8d.Location = New System.Drawing.Point(0, 1158)
        Me.OpCtrl8d.Name = "OpCtrl8d"
        Me.OpCtrl8d.Option1Text = "Yes"
        Me.OpCtrl8d.Option2Text = "No"
        Me.OpCtrl8d.Size = New System.Drawing.Size(763, 22)
        Me.OpCtrl8d.TabIndex = 75
        Me.OpCtrl8d.Yes = True
        '
        'OpCtrl8c
        '
        Me.OpCtrl8c.AllowUserToSizeOptionWidth = False
        Me.OpCtrl8c.BackColor = System.Drawing.Color.White
        Me.OpCtrl8c.ConditionText = "    c. Savings deposits"
        Me.OpCtrl8c.Dock = System.Windows.Forms.DockStyle.Top
        Me.OpCtrl8c.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OpCtrl8c.Location = New System.Drawing.Point(0, 1136)
        Me.OpCtrl8c.Name = "OpCtrl8c"
        Me.OpCtrl8c.Option1Text = "Yes"
        Me.OpCtrl8c.Option2Text = "No"
        Me.OpCtrl8c.Size = New System.Drawing.Size(763, 22)
        Me.OpCtrl8c.TabIndex = 74
        Me.OpCtrl8c.Yes = True
        '
        'OpCtrl8b
        '
        Me.OpCtrl8b.AllowUserToSizeOptionWidth = False
        Me.OpCtrl8b.BackColor = System.Drawing.Color.White
        Me.OpCtrl8b.ConditionText = "    b. Loaning"
        Me.OpCtrl8b.Dock = System.Windows.Forms.DockStyle.Top
        Me.OpCtrl8b.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OpCtrl8b.Location = New System.Drawing.Point(0, 1114)
        Me.OpCtrl8b.Name = "OpCtrl8b"
        Me.OpCtrl8b.Option1Text = "Yes"
        Me.OpCtrl8b.Option2Text = "No"
        Me.OpCtrl8b.Size = New System.Drawing.Size(763, 22)
        Me.OpCtrl8b.TabIndex = 73
        Me.OpCtrl8b.Yes = True
        '
        'OpCtrl8a
        '
        Me.OpCtrl8a.AllowUserToSizeOptionWidth = False
        Me.OpCtrl8a.BackColor = System.Drawing.Color.White
        Me.OpCtrl8a.ConditionText = "    a. Membership"
        Me.OpCtrl8a.Dock = System.Windows.Forms.DockStyle.Top
        Me.OpCtrl8a.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OpCtrl8a.Location = New System.Drawing.Point(0, 1092)
        Me.OpCtrl8a.Name = "OpCtrl8a"
        Me.OpCtrl8a.Option1Text = "Yes"
        Me.OpCtrl8a.Option2Text = "No"
        Me.OpCtrl8a.Size = New System.Drawing.Size(763, 22)
        Me.OpCtrl8a.TabIndex = 72
        Me.OpCtrl8a.Yes = True
        '
        'Label32
        '
        Me.Label32.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label32.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label32.Location = New System.Drawing.Point(0, 1074)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(763, 18)
        Me.Label32.TabIndex = 71
        Me.Label32.Text = "8. Does the coop have written Manual of Policies and Procedures with specific pro" & _
            "visions on the following:"
        '
        'Label33
        '
        Me.Label33.BackColor = System.Drawing.Color.Green
        Me.Label33.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label33.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label33.ForeColor = System.Drawing.Color.White
        Me.Label33.Location = New System.Drawing.Point(0, 1053)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(763, 21)
        Me.Label33.TabIndex = 70
        Me.Label33.Text = "OPERATIONS AND GOVERNANACE"
        Me.Label33.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label38
        '
        Me.Label38.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label38.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label38.Location = New System.Drawing.Point(0, 1035)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(763, 18)
        Me.Label38.TabIndex = 69
        '
        'OpCtrl7e
        '
        Me.OpCtrl7e.AllowUserToSizeOptionWidth = False
        Me.OpCtrl7e.BackColor = System.Drawing.Color.White
        Me.OpCtrl7e.ConditionText = "    e. Did the coop have business alliance with other cooperatives?"
        Me.OpCtrl7e.Dock = System.Windows.Forms.DockStyle.Top
        Me.OpCtrl7e.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OpCtrl7e.Location = New System.Drawing.Point(0, 1013)
        Me.OpCtrl7e.Name = "OpCtrl7e"
        Me.OpCtrl7e.Option1Text = "Yes"
        Me.OpCtrl7e.Option2Text = "No"
        Me.OpCtrl7e.Size = New System.Drawing.Size(763, 22)
        Me.OpCtrl7e.TabIndex = 39
        Me.OpCtrl7e.Yes = True
        '
        'OpCtrl7d
        '
        Me.OpCtrl7d.AllowUserToSizeOptionWidth = False
        Me.OpCtrl7d.BackColor = System.Drawing.Color.White
        Me.OpCtrl7d.ConditionText = "    d. Did the coop have business alliance with federations?"
        Me.OpCtrl7d.Dock = System.Windows.Forms.DockStyle.Top
        Me.OpCtrl7d.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OpCtrl7d.Location = New System.Drawing.Point(0, 991)
        Me.OpCtrl7d.Name = "OpCtrl7d"
        Me.OpCtrl7d.Option1Text = "Yes"
        Me.OpCtrl7d.Option2Text = "No"
        Me.OpCtrl7d.Size = New System.Drawing.Size(763, 22)
        Me.OpCtrl7d.TabIndex = 38
        Me.OpCtrl7d.Yes = True
        '
        'OpCtrl7c
        '
        Me.OpCtrl7c.AllowUserToSizeOptionWidth = False
        Me.OpCtrl7c.BackColor = System.Drawing.Color.White
        Me.OpCtrl7c.ConditionText = "    c. Did the coop pay annual dues, CETF, loans and other accounts to the federa" & _
            "tion / union of their choice?"
        Me.OpCtrl7c.Dock = System.Windows.Forms.DockStyle.Top
        Me.OpCtrl7c.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OpCtrl7c.Location = New System.Drawing.Point(0, 954)
        Me.OpCtrl7c.Name = "OpCtrl7c"
        Me.OpCtrl7c.Option1Text = "Yes"
        Me.OpCtrl7c.Option2Text = "No"
        Me.OpCtrl7c.Size = New System.Drawing.Size(763, 37)
        Me.OpCtrl7c.TabIndex = 37
        Me.OpCtrl7c.Yes = True
        '
        'OpCtrl7b
        '
        Me.OpCtrl7b.AllowUserToSizeOptionWidth = False
        Me.OpCtrl7b.BackColor = System.Drawing.Color.White
        Me.OpCtrl7b.ConditionText = "    b. Was the coop involved in community service?"
        Me.OpCtrl7b.Dock = System.Windows.Forms.DockStyle.Top
        Me.OpCtrl7b.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OpCtrl7b.Location = New System.Drawing.Point(0, 932)
        Me.OpCtrl7b.Name = "OpCtrl7b"
        Me.OpCtrl7b.Option1Text = "Yes"
        Me.OpCtrl7b.Option2Text = "No"
        Me.OpCtrl7b.Size = New System.Drawing.Size(763, 22)
        Me.OpCtrl7b.TabIndex = 36
        Me.OpCtrl7b.Yes = True
        '
        'OpCtrl7aIf
        '
        Me.OpCtrl7aIf.AllowUserToSizeOptionWidth = False
        Me.OpCtrl7aIf.BackColor = System.Drawing.Color.White
        Me.OpCtrl7aIf.ConditionText = "        If yes, has the coop established a Laboratory Cooperative?"
        Me.OpCtrl7aIf.Dock = System.Windows.Forms.DockStyle.Top
        Me.OpCtrl7aIf.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OpCtrl7aIf.Location = New System.Drawing.Point(0, 910)
        Me.OpCtrl7aIf.Name = "OpCtrl7aIf"
        Me.OpCtrl7aIf.Option1Text = "Yes"
        Me.OpCtrl7aIf.Option2Text = "No"
        Me.OpCtrl7aIf.Size = New System.Drawing.Size(763, 22)
        Me.OpCtrl7aIf.TabIndex = 35
        Me.OpCtrl7aIf.Yes = True
        '
        'OpCtrl7a
        '
        Me.OpCtrl7a.AllowUserToSizeOptionWidth = False
        Me.OpCtrl7a.BackColor = System.Drawing.Color.White
        Me.OpCtrl7a.ConditionText = "    a. Does the coop have depositors that are below 18 years old?"
        Me.OpCtrl7a.Dock = System.Windows.Forms.DockStyle.Top
        Me.OpCtrl7a.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OpCtrl7a.Location = New System.Drawing.Point(0, 888)
        Me.OpCtrl7a.Name = "OpCtrl7a"
        Me.OpCtrl7a.Option1Text = "Yes"
        Me.OpCtrl7a.Option2Text = "No"
        Me.OpCtrl7a.Size = New System.Drawing.Size(763, 22)
        Me.OpCtrl7a.TabIndex = 34
        Me.OpCtrl7a.Yes = True
        '
        'Label31
        '
        Me.Label31.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label31.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label31.Location = New System.Drawing.Point(0, 858)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(763, 30)
        Me.Label31.TabIndex = 33
        Me.Label31.Text = "7. Affiliation and Linkages"
        Me.Label31.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'OpCtrl6b
        '
        Me.OpCtrl6b.AllowUserToSizeOptionWidth = False
        Me.OpCtrl6b.BackColor = System.Drawing.Color.White
        Me.OpCtrl6b.ConditionText = "    b. Is cooperative education regularly conducted for current members?"
        Me.OpCtrl6b.Dock = System.Windows.Forms.DockStyle.Top
        Me.OpCtrl6b.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OpCtrl6b.Location = New System.Drawing.Point(0, 836)
        Me.OpCtrl6b.Name = "OpCtrl6b"
        Me.OpCtrl6b.Option1Text = "Yes"
        Me.OpCtrl6b.Option2Text = "No"
        Me.OpCtrl6b.Size = New System.Drawing.Size(763, 22)
        Me.OpCtrl6b.TabIndex = 32
        Me.OpCtrl6b.Yes = True
        '
        'OpCtrl6a
        '
        Me.OpCtrl6a.AllowUserToSizeOptionWidth = False
        Me.OpCtrl6a.BackColor = System.Drawing.Color.White
        Me.OpCtrl6a.ConditionText = "    a. Are pre-membership education seminars for new members regularly conducted?" & _
            ""
        Me.OpCtrl6a.Dock = System.Windows.Forms.DockStyle.Top
        Me.OpCtrl6a.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OpCtrl6a.Location = New System.Drawing.Point(0, 814)
        Me.OpCtrl6a.Name = "OpCtrl6a"
        Me.OpCtrl6a.Option1Text = "Yes"
        Me.OpCtrl6a.Option2Text = "No"
        Me.OpCtrl6a.Size = New System.Drawing.Size(763, 22)
        Me.OpCtrl6a.TabIndex = 31
        Me.OpCtrl6a.Yes = True
        '
        'Label11
        '
        Me.Label11.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label11.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(0, 784)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(763, 30)
        Me.Label11.TabIndex = 30
        Me.Label11.Text = "6. Membership Education"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'OpCtrl5e
        '
        Me.OpCtrl5e.AllowUserToSizeOptionWidth = False
        Me.OpCtrl5e.BackColor = System.Drawing.Color.White
        Me.OpCtrl5e.ConditionText = "    e. Does the cooperative have an Annual Program and Development / Strategic Pl" & _
            "an that was approved by General Assembly?"
        Me.OpCtrl5e.Dock = System.Windows.Forms.DockStyle.Top
        Me.OpCtrl5e.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OpCtrl5e.Location = New System.Drawing.Point(0, 741)
        Me.OpCtrl5e.Name = "OpCtrl5e"
        Me.OpCtrl5e.Option1Text = "Yes"
        Me.OpCtrl5e.Option2Text = "No"
        Me.OpCtrl5e.Size = New System.Drawing.Size(763, 43)
        Me.OpCtrl5e.TabIndex = 29
        Me.OpCtrl5e.Yes = True
        '
        'OpCtrl5d
        '
        Me.OpCtrl5d.AllowUserToSizeOptionWidth = False
        Me.OpCtrl5d.BackColor = System.Drawing.Color.White
        Me.OpCtrl5d.ConditionText = "    d. Do 70% of members have saving deposits  with the cooperative?"
        Me.OpCtrl5d.Dock = System.Windows.Forms.DockStyle.Top
        Me.OpCtrl5d.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OpCtrl5d.Location = New System.Drawing.Point(0, 719)
        Me.OpCtrl5d.Name = "OpCtrl5d"
        Me.OpCtrl5d.Option1Text = "Yes"
        Me.OpCtrl5d.Option2Text = "No"
        Me.OpCtrl5d.Size = New System.Drawing.Size(763, 22)
        Me.OpCtrl5d.TabIndex = 28
        Me.OpCtrl5d.Yes = True
        '
        'OpCtrl5c
        '
        Me.OpCtrl5c.AllowUserToSizeOptionWidth = False
        Me.OpCtrl5c.BackColor = System.Drawing.Color.White
        Me.OpCtrl5c.ConditionText = "    c. If the coop's authorized capital is not fully subscribed, does the coop ha" & _
            "ve a Capital Build-up progam where 70% of members (regular and associate) partic" & _
            "ipate?"
        Me.OpCtrl5c.Dock = System.Windows.Forms.DockStyle.Top
        Me.OpCtrl5c.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OpCtrl5c.Location = New System.Drawing.Point(0, 674)
        Me.OpCtrl5c.Name = "OpCtrl5c"
        Me.OpCtrl5c.Option1Text = "Yes"
        Me.OpCtrl5c.Option2Text = "No"
        Me.OpCtrl5c.Size = New System.Drawing.Size(763, 45)
        Me.OpCtrl5c.TabIndex = 27
        Me.OpCtrl5c.Yes = True
        '
        'OpCtrl5b
        '
        Me.OpCtrl5b.AllowUserToSizeOptionWidth = False
        Me.OpCtrl5b.BackColor = System.Drawing.Color.White
        Me.OpCtrl5b.ConditionText = "    b. Is the authorized capital of the coop fully subscribed?"
        Me.OpCtrl5b.Dock = System.Windows.Forms.DockStyle.Top
        Me.OpCtrl5b.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OpCtrl5b.Location = New System.Drawing.Point(0, 652)
        Me.OpCtrl5b.Name = "OpCtrl5b"
        Me.OpCtrl5b.Option1Text = "Yes"
        Me.OpCtrl5b.Option2Text = "No"
        Me.OpCtrl5b.Size = New System.Drawing.Size(763, 22)
        Me.OpCtrl5b.TabIndex = 26
        Me.OpCtrl5b.Yes = True
        '
        'OpCtrl5a
        '
        Me.OpCtrl5a.AllowUserToSizeOptionWidth = False
        Me.OpCtrl5a.BackColor = System.Drawing.Color.White
        Me.OpCtrl5a.ConditionText = "    a. Has the quorum requirement as indicated in the cooperative's by-laws been " & _
            "achieved during the conduct of the recent General Assembly Meeting?"
        Me.OpCtrl5a.Dock = System.Windows.Forms.DockStyle.Top
        Me.OpCtrl5a.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OpCtrl5a.Location = New System.Drawing.Point(0, 608)
        Me.OpCtrl5a.Name = "OpCtrl5a"
        Me.OpCtrl5a.Option1Text = "Yes"
        Me.OpCtrl5a.Option2Text = "No"
        Me.OpCtrl5a.Size = New System.Drawing.Size(763, 44)
        Me.OpCtrl5a.TabIndex = 25
        Me.OpCtrl5a.Yes = True
        '
        'Label10
        '
        Me.Label10.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label10.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(0, 590)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(763, 18)
        Me.Label10.TabIndex = 24
        Me.Label10.Text = "5. Membership participation"
        '
        'Label9
        '
        Me.Label9.BackColor = System.Drawing.Color.Green
        Me.Label9.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label9.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.White
        Me.Label9.Location = New System.Drawing.Point(0, 569)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(763, 21)
        Me.Label9.TabIndex = 23
        Me.Label9.Text = "ORGANIZATION"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label8
        '
        Me.Label8.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label8.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(0, 551)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(763, 18)
        Me.Label8.TabIndex = 22
        '
        'OpCtrl4d
        '
        Me.OpCtrl4d.AllowUserToSizeOptionWidth = False
        Me.OpCtrl4d.BackColor = System.Drawing.Color.White
        Me.OpCtrl4d.ConditionText = "    d. Upon retirement, are employees paid?"
        Me.OpCtrl4d.Dock = System.Windows.Forms.DockStyle.Top
        Me.OpCtrl4d.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OpCtrl4d.Location = New System.Drawing.Point(0, 529)
        Me.OpCtrl4d.Name = "OpCtrl4d"
        Me.OpCtrl4d.Option1Text = "Yes"
        Me.OpCtrl4d.Option2Text = "No"
        Me.OpCtrl4d.Size = New System.Drawing.Size(763, 22)
        Me.OpCtrl4d.TabIndex = 21
        Me.OpCtrl4d.Yes = True
        '
        'OpCtrl4c
        '
        Me.OpCtrl4c.AllowUserToSizeOptionWidth = False
        Me.OpCtrl4c.BackColor = System.Drawing.Color.White
        Me.OpCtrl4c.ConditionText = "    c. Are employees provided additional retirement plans other than those privid" & _
            "ed by law?"
        Me.OpCtrl4c.Dock = System.Windows.Forms.DockStyle.Top
        Me.OpCtrl4c.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OpCtrl4c.Location = New System.Drawing.Point(0, 507)
        Me.OpCtrl4c.Name = "OpCtrl4c"
        Me.OpCtrl4c.Option1Text = "Yes"
        Me.OpCtrl4c.Option2Text = "No"
        Me.OpCtrl4c.Size = New System.Drawing.Size(763, 22)
        Me.OpCtrl4c.TabIndex = 20
        Me.OpCtrl4c.Yes = True
        '
        'OpCtrl4biii
        '
        Me.OpCtrl4biii.AllowUserToSizeOptionWidth = False
        Me.OpCtrl4biii.BackColor = System.Drawing.Color.White
        Me.OpCtrl4biii.ConditionText = "        iii. HDMF (Pag-ibig)"
        Me.OpCtrl4biii.Dock = System.Windows.Forms.DockStyle.Top
        Me.OpCtrl4biii.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OpCtrl4biii.Location = New System.Drawing.Point(0, 485)
        Me.OpCtrl4biii.Name = "OpCtrl4biii"
        Me.OpCtrl4biii.Option1Text = "Yes"
        Me.OpCtrl4biii.Option2Text = "No"
        Me.OpCtrl4biii.Size = New System.Drawing.Size(763, 22)
        Me.OpCtrl4biii.TabIndex = 19
        Me.OpCtrl4biii.Yes = True
        '
        'OpCtrl4bii
        '
        Me.OpCtrl4bii.AllowUserToSizeOptionWidth = False
        Me.OpCtrl4bii.BackColor = System.Drawing.Color.White
        Me.OpCtrl4bii.ConditionText = "        ii. Philhealth"
        Me.OpCtrl4bii.Dock = System.Windows.Forms.DockStyle.Top
        Me.OpCtrl4bii.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OpCtrl4bii.Location = New System.Drawing.Point(0, 463)
        Me.OpCtrl4bii.Name = "OpCtrl4bii"
        Me.OpCtrl4bii.Option1Text = "Yes"
        Me.OpCtrl4bii.Option2Text = "No"
        Me.OpCtrl4bii.Size = New System.Drawing.Size(763, 22)
        Me.OpCtrl4bii.TabIndex = 18
        Me.OpCtrl4bii.Yes = True
        '
        'OpCtrl4bi
        '
        Me.OpCtrl4bi.AllowUserToSizeOptionWidth = False
        Me.OpCtrl4bi.BackColor = System.Drawing.Color.White
        Me.OpCtrl4bi.ConditionText = "        i. SSS"
        Me.OpCtrl4bi.Dock = System.Windows.Forms.DockStyle.Top
        Me.OpCtrl4bi.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OpCtrl4bi.Location = New System.Drawing.Point(0, 441)
        Me.OpCtrl4bi.Name = "OpCtrl4bi"
        Me.OpCtrl4bi.Option1Text = "Yes"
        Me.OpCtrl4bi.Option2Text = "No"
        Me.OpCtrl4bi.Size = New System.Drawing.Size(763, 22)
        Me.OpCtrl4bi.TabIndex = 17
        Me.OpCtrl4bi.Yes = True
        '
        'Label7
        '
        Me.Label7.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label7.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(0, 423)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(763, 18)
        Me.Label7.TabIndex = 16
        Me.Label7.Text = "    b. Have the mandatory insurance premiums to the following agencies been remit" & _
            "ted?"
        '
        'OpCtrl4a
        '
        Me.OpCtrl4a.AllowUserToSizeOptionWidth = False
        Me.OpCtrl4a.BackColor = System.Drawing.Color.White
        Me.OpCtrl4a.ConditionText = "    a. Are the legal miimum wage and 13th Month Pay to employees complied with?"
        Me.OpCtrl4a.Dock = System.Windows.Forms.DockStyle.Top
        Me.OpCtrl4a.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OpCtrl4a.Location = New System.Drawing.Point(0, 401)
        Me.OpCtrl4a.Name = "OpCtrl4a"
        Me.OpCtrl4a.Option1Text = "Yes"
        Me.OpCtrl4a.Option2Text = "No"
        Me.OpCtrl4a.Size = New System.Drawing.Size(763, 22)
        Me.OpCtrl4a.TabIndex = 15
        Me.OpCtrl4a.Yes = True
        '
        'Label6
        '
        Me.Label6.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label6.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(0, 371)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(763, 30)
        Me.Label6.TabIndex = 14
        Me.Label6.Text = "4. Compliance with DOLE requirements"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'OpCtrl3a
        '
        Me.OpCtrl3a.AllowUserToSizeOptionWidth = False
        Me.OpCtrl3a.BackColor = System.Drawing.Color.White
        Me.OpCtrl3a.ConditionText = "    a. Has the local business permit / license been acquired?"
        Me.OpCtrl3a.Dock = System.Windows.Forms.DockStyle.Top
        Me.OpCtrl3a.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OpCtrl3a.Location = New System.Drawing.Point(0, 349)
        Me.OpCtrl3a.Name = "OpCtrl3a"
        Me.OpCtrl3a.Option1Text = "Yes"
        Me.OpCtrl3a.Option2Text = "No"
        Me.OpCtrl3a.Size = New System.Drawing.Size(763, 22)
        Me.OpCtrl3a.TabIndex = 13
        Me.OpCtrl3a.Yes = True
        '
        'Label5
        '
        Me.Label5.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label5.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(0, 319)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(763, 30)
        Me.Label5.TabIndex = 12
        Me.Label5.Text = "3. Compliance with LGU requirements"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'OpCtrl2d
        '
        Me.OpCtrl2d.AllowUserToSizeOptionWidth = False
        Me.OpCtrl2d.BackColor = System.Drawing.Color.White
        Me.OpCtrl2d.ConditionText = "    d. Are the Books of Accounts registered?"
        Me.OpCtrl2d.Dock = System.Windows.Forms.DockStyle.Top
        Me.OpCtrl2d.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OpCtrl2d.Location = New System.Drawing.Point(0, 297)
        Me.OpCtrl2d.Name = "OpCtrl2d"
        Me.OpCtrl2d.Option1Text = "Yes"
        Me.OpCtrl2d.Option2Text = "No"
        Me.OpCtrl2d.Size = New System.Drawing.Size(763, 22)
        Me.OpCtrl2d.TabIndex = 11
        Me.OpCtrl2d.Yes = True
        '
        'OpCtrl2c
        '
        Me.OpCtrl2c.AllowUserToSizeOptionWidth = False
        Me.OpCtrl2c.BackColor = System.Drawing.Color.White
        Me.OpCtrl2c.ConditionText = "    c. is the cooperative knowledgeable and clarified as to its tax exemption pre" & _
            "vileges and tax obligations with BIR?"
        Me.OpCtrl2c.Dock = System.Windows.Forms.DockStyle.Top
        Me.OpCtrl2c.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OpCtrl2c.Location = New System.Drawing.Point(0, 258)
        Me.OpCtrl2c.Name = "OpCtrl2c"
        Me.OpCtrl2c.Option1Text = "Yes"
        Me.OpCtrl2c.Option2Text = "No"
        Me.OpCtrl2c.Size = New System.Drawing.Size(763, 39)
        Me.OpCtrl2c.TabIndex = 10
        Me.OpCtrl2c.Yes = True
        '
        'OpCtrl2b
        '
        Me.OpCtrl2b.AllowUserToSizeOptionWidth = False
        Me.OpCtrl2b.BackColor = System.Drawing.Color.White
        Me.OpCtrl2b.ConditionText = "    b. Are appropriate taxes, if any, withheld and remitted?"
        Me.OpCtrl2b.Dock = System.Windows.Forms.DockStyle.Top
        Me.OpCtrl2b.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OpCtrl2b.Location = New System.Drawing.Point(0, 236)
        Me.OpCtrl2b.Name = "OpCtrl2b"
        Me.OpCtrl2b.Option1Text = "Yes"
        Me.OpCtrl2b.Option2Text = "No"
        Me.OpCtrl2b.Size = New System.Drawing.Size(763, 22)
        Me.OpCtrl2b.TabIndex = 9
        Me.OpCtrl2b.Yes = True
        '
        'OpCtrl2a
        '
        Me.OpCtrl2a.AllowUserToSizeOptionWidth = False
        Me.OpCtrl2a.BackColor = System.Drawing.Color.White
        Me.OpCtrl2a.ConditionText = "    a. Have annual registration requirement of the BIR been submitted?"
        Me.OpCtrl2a.Dock = System.Windows.Forms.DockStyle.Top
        Me.OpCtrl2a.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OpCtrl2a.Location = New System.Drawing.Point(0, 214)
        Me.OpCtrl2a.Name = "OpCtrl2a"
        Me.OpCtrl2a.Option1Text = "Yes"
        Me.OpCtrl2a.Option2Text = "No"
        Me.OpCtrl2a.Size = New System.Drawing.Size(763, 22)
        Me.OpCtrl2a.TabIndex = 8
        Me.OpCtrl2a.Yes = True
        '
        'Label4
        '
        Me.Label4.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label4.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(0, 184)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(763, 30)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "2. Compliance with BIR requirements"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'OpCtrl1d
        '
        Me.OpCtrl1d.AllowUserToSizeOptionWidth = False
        Me.OpCtrl1d.BackColor = System.Drawing.Color.White
        Me.OpCtrl1d.ConditionText = "    d. Are CDA required reports accomplished in the prescribed format and submitt" & _
            "ed within the prescribed period?"
        Me.OpCtrl1d.Dock = System.Windows.Forms.DockStyle.Top
        Me.OpCtrl1d.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OpCtrl1d.Location = New System.Drawing.Point(0, 144)
        Me.OpCtrl1d.Name = "OpCtrl1d"
        Me.OpCtrl1d.Option1Text = "Yes"
        Me.OpCtrl1d.Option2Text = "No"
        Me.OpCtrl1d.Size = New System.Drawing.Size(763, 40)
        Me.OpCtrl1d.TabIndex = 6
        Me.OpCtrl1d.Yes = True
        '
        'OpCtrl1c
        '
        Me.OpCtrl1c.AllowUserToSizeOptionWidth = False
        Me.OpCtrl1c.BackColor = System.Drawing.Color.White
        Me.OpCtrl1c.ConditionText = "    c. Are Share Capital Certificates / Passbooks issued to members and regularly" & _
            " updated whenever necessary?"
        Me.OpCtrl1c.Dock = System.Windows.Forms.DockStyle.Top
        Me.OpCtrl1c.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OpCtrl1c.Location = New System.Drawing.Point(0, 104)
        Me.OpCtrl1c.Name = "OpCtrl1c"
        Me.OpCtrl1c.Option1Text = "Yes"
        Me.OpCtrl1c.Option2Text = "No"
        Me.OpCtrl1c.Size = New System.Drawing.Size(763, 40)
        Me.OpCtrl1c.TabIndex = 5
        Me.OpCtrl1c.Yes = True
        '
        'OpCtrl1bIf
        '
        Me.OpCtrl1bIf.AllowUserToSizeOptionWidth = False
        Me.OpCtrl1bIf.BackColor = System.Drawing.Color.White
        Me.OpCtrl1bIf.ConditionText = "        If yes, was it amended and registered?"
        Me.OpCtrl1bIf.Dock = System.Windows.Forms.DockStyle.Top
        Me.OpCtrl1bIf.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OpCtrl1bIf.Location = New System.Drawing.Point(0, 82)
        Me.OpCtrl1bIf.Name = "OpCtrl1bIf"
        Me.OpCtrl1bIf.Option1Text = "Yes"
        Me.OpCtrl1bIf.Option2Text = "No"
        Me.OpCtrl1bIf.Size = New System.Drawing.Size(763, 22)
        Me.OpCtrl1bIf.TabIndex = 4
        Me.OpCtrl1bIf.Yes = True
        '
        'OpCtrl1b
        '
        Me.OpCtrl1b.AllowUserToSizeOptionWidth = False
        Me.OpCtrl1b.BackColor = System.Drawing.Color.White
        Me.OpCtrl1b.ConditionText = "    b. Are the Article of Cooperative and By-Laws updated?"
        Me.OpCtrl1b.Dock = System.Windows.Forms.DockStyle.Top
        Me.OpCtrl1b.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OpCtrl1b.Location = New System.Drawing.Point(0, 60)
        Me.OpCtrl1b.Name = "OpCtrl1b"
        Me.OpCtrl1b.Option1Text = "Yes"
        Me.OpCtrl1b.Option2Text = "No"
        Me.OpCtrl1b.Size = New System.Drawing.Size(763, 22)
        Me.OpCtrl1b.TabIndex = 3
        Me.OpCtrl1b.Yes = True
        '
        'OpCtrl1a
        '
        Me.OpCtrl1a.AllowUserToSizeOptionWidth = False
        Me.OpCtrl1a.BackColor = System.Drawing.Color.White
        Me.OpCtrl1a.ConditionText = "    a. Are the Bonds of Accountable Officers current?"
        Me.OpCtrl1a.Dock = System.Windows.Forms.DockStyle.Top
        Me.OpCtrl1a.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OpCtrl1a.Location = New System.Drawing.Point(0, 38)
        Me.OpCtrl1a.Name = "OpCtrl1a"
        Me.OpCtrl1a.Option1Text = "Yes"
        Me.OpCtrl1a.Option2Text = "No"
        Me.OpCtrl1a.Size = New System.Drawing.Size(763, 22)
        Me.OpCtrl1a.TabIndex = 2
        Me.OpCtrl1a.Yes = True
        '
        'Label3
        '
        Me.Label3.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label3.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(0, 21)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(763, 17)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "1. Compliance with administrative and legal requirements of CDA"
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.Green
        Me.Label2.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label2.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(0, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(763, 21)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "COMPLIANCE"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TabPage4
        '
        Me.TabPage4.BackColor = System.Drawing.Color.White
        Me.TabPage4.Controls.Add(Me.CboAudFinState)
        Me.TabPage4.Controls.Add(Me.txtExtAudName)
        Me.TabPage4.Controls.Add(Me.txtPeriodOpCoverd)
        Me.TabPage4.Controls.Add(Me.txtIntAudDesign)
        Me.TabPage4.Controls.Add(Me.txtIntAudName)
        Me.TabPage4.Controls.Add(Me.dtExtAud)
        Me.TabPage4.Controls.Add(Me.dtLastIntAudRpt)
        Me.TabPage4.Controls.Add(Me.NumFreqIntAudit)
        Me.TabPage4.Controls.Add(Me.Label56)
        Me.TabPage4.Controls.Add(Me.Label55)
        Me.TabPage4.Controls.Add(Me.Label54)
        Me.TabPage4.Controls.Add(Me.Label58)
        Me.TabPage4.Controls.Add(Me.Label57)
        Me.TabPage4.Controls.Add(Me.Label53)
        Me.TabPage4.Controls.Add(Me.Label52)
        Me.TabPage4.Controls.Add(Me.Label51)
        Me.TabPage4.Controls.Add(Me.Label50)
        Me.TabPage4.Controls.Add(Me.Label49)
        Me.TabPage4.Controls.Add(Me.Label48)
        Me.TabPage4.Location = New System.Drawing.Point(4, 24)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage4.Size = New System.Drawing.Size(798, 330)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "J"
        '
        'CboAudFinState
        '
        Me.CboAudFinState.FormattingEnabled = True
        Me.CboAudFinState.Items.AddRange(New Object() {"None", "Yes"})
        Me.CboAudFinState.Location = New System.Drawing.Point(382, 264)
        Me.CboAudFinState.Name = "CboAudFinState"
        Me.CboAudFinState.Size = New System.Drawing.Size(199, 23)
        Me.CboAudFinState.TabIndex = 24
        Me.CboAudFinState.Text = "None"
        '
        'txtExtAudName
        '
        Me.txtExtAudName.Location = New System.Drawing.Point(382, 235)
        Me.txtExtAudName.Name = "txtExtAudName"
        Me.txtExtAudName.Size = New System.Drawing.Size(199, 23)
        Me.txtExtAudName.TabIndex = 21
        '
        'txtPeriodOpCoverd
        '
        Me.txtPeriodOpCoverd.Location = New System.Drawing.Point(382, 206)
        Me.txtPeriodOpCoverd.Name = "txtPeriodOpCoverd"
        Me.txtPeriodOpCoverd.Size = New System.Drawing.Size(199, 23)
        Me.txtPeriodOpCoverd.TabIndex = 20
        '
        'txtIntAudDesign
        '
        Me.txtIntAudDesign.Location = New System.Drawing.Point(383, 132)
        Me.txtIntAudDesign.Name = "txtIntAudDesign"
        Me.txtIntAudDesign.Size = New System.Drawing.Size(199, 23)
        Me.txtIntAudDesign.TabIndex = 23
        '
        'txtIntAudName
        '
        Me.txtIntAudName.Location = New System.Drawing.Point(383, 103)
        Me.txtIntAudName.Name = "txtIntAudName"
        Me.txtIntAudName.Size = New System.Drawing.Size(199, 23)
        Me.txtIntAudName.TabIndex = 22
        '
        'dtExtAud
        '
        Me.dtExtAud.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtExtAud.Location = New System.Drawing.Point(382, 177)
        Me.dtExtAud.Name = "dtExtAud"
        Me.dtExtAud.Size = New System.Drawing.Size(199, 23)
        Me.dtExtAud.TabIndex = 18
        '
        'dtLastIntAudRpt
        '
        Me.dtLastIntAudRpt.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtLastIntAudRpt.Location = New System.Drawing.Point(383, 74)
        Me.dtLastIntAudRpt.Name = "dtLastIntAudRpt"
        Me.dtLastIntAudRpt.Size = New System.Drawing.Size(199, 23)
        Me.dtLastIntAudRpt.TabIndex = 19
        '
        'NumFreqIntAudit
        '
        Me.NumFreqIntAudit.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumFreqIntAudit.Location = New System.Drawing.Point(383, 42)
        Me.NumFreqIntAudit.Name = "NumFreqIntAudit"
        Me.NumFreqIntAudit.Size = New System.Drawing.Size(199, 26)
        Me.NumFreqIntAudit.TabIndex = 17
        Me.NumFreqIntAudit.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'Label56
        '
        Me.Label56.AutoSize = True
        Me.Label56.Location = New System.Drawing.Point(38, 238)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(157, 15)
        Me.Label56.TabIndex = 16
        Me.Label56.Text = "c. Who conducted the audit"
        '
        'Label55
        '
        Me.Label55.AutoSize = True
        Me.Label55.Location = New System.Drawing.Point(38, 209)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(268, 15)
        Me.Label55.TabIndex = 15
        Me.Label55.Text = "b. Period of Operation Covered by the last audit"
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Location = New System.Drawing.Point(38, 181)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(115, 15)
        Me.Label54.TabIndex = 9
        Me.Label54.Text = "a. Date of last audit"
        '
        'Label58
        '
        Me.Label58.AutoSize = True
        Me.Label58.Location = New System.Drawing.Point(584, 267)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(205, 30)
        Me.Label58.TabIndex = 10
        Me.Label58.Text = "If yes, please process to Section III." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "If none, please proceed to Section II."
        '
        'Label57
        '
        Me.Label57.AutoSize = True
        Me.Label57.Location = New System.Drawing.Point(19, 267)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(287, 15)
        Me.Label57.TabIndex = 7
        Me.Label57.Text = "3. Does the coop have audited financial statement?"
        '
        'Label53
        '
        Me.Label53.AutoSize = True
        Me.Label53.Location = New System.Drawing.Point(16, 166)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(97, 15)
        Me.Label53.TabIndex = 8
        Me.Label53.Text = "2. External Audit"
        '
        'Label52
        '
        Me.Label52.AutoSize = True
        Me.Label52.Location = New System.Drawing.Point(39, 106)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(339, 15)
        Me.Label52.TabIndex = 13
        Me.Label52.Text = "c. Who conducted the internal audit (Name and Designation)"
        '
        'Label51
        '
        Me.Label51.AutoSize = True
        Me.Label51.Location = New System.Drawing.Point(39, 78)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(155, 15)
        Me.Label51.TabIndex = 14
        Me.Label51.Text = "b. Date of Last Audit Report"
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.Location = New System.Drawing.Point(39, 46)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(169, 15)
        Me.Label50.TabIndex = 11
        Me.Label50.Text = "a. Frequency of Internal Audit"
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.Location = New System.Drawing.Point(17, 27)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(96, 15)
        Me.Label49.TabIndex = 12
        Me.Label49.Text = "1. Internal Audit"
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.Location = New System.Drawing.Point(6, 12)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(132, 15)
        Me.Label48.TabIndex = 6
        Me.Label48.Text = "J. Information on Audit"
        '
        'BtnPrevious
        '
        Me.BtnPrevious.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtnPrevious.Location = New System.Drawing.Point(625, 366)
        Me.BtnPrevious.Name = "BtnPrevious"
        Me.BtnPrevious.Size = New System.Drawing.Size(87, 27)
        Me.BtnPrevious.TabIndex = 18
        Me.BtnPrevious.Text = "Previous"
        Me.BtnPrevious.UseVisualStyleBackColor = True
        '
        'BtnNextOK
        '
        Me.BtnNextOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtnNextOK.Location = New System.Drawing.Point(722, 366)
        Me.BtnNextOK.Name = "BtnNextOK"
        Me.BtnNextOK.Size = New System.Drawing.Size(87, 27)
        Me.BtnNextOK.TabIndex = 19
        Me.BtnNextOK.Text = "Next"
        Me.BtnNextOK.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Image = Global.WindowsApplication2.My.Resources.Resources.button_cancel
        Me.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnCancel.Location = New System.Drawing.Point(13, 366)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(84, 27)
        Me.btnCancel.TabIndex = 20
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'FrmCAPRSecI
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(817, 418)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.BtnPrevious)
        Me.Controls.Add(Me.BtnNextOK)
        Me.Controls.Add(Me.TabControl1)
        Me.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MinimumSize = New System.Drawing.Size(819, 420)
        Me.Name = "FrmCAPRSecI"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cooperative Annual Performance Report - [Section I]"
        Me.TopMost = True
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.TabPage4.ResumeLayout(False)
        Me.TabPage4.PerformLayout()
        CType(Me.NumFreqIntAudit, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents txtBusActEngaged As System.Windows.Forms.TextBox
    Friend WithEvents txtCoopType As System.Windows.Forms.TextBox
    Friend WithEvents txtCoopAdd As System.Windows.Forms.TextBox
    Friend WithEvents txtRegConNo As System.Windows.Forms.TextBox
    Friend WithEvents txtCoopName As System.Windows.Forms.TextBox
    Friend WithEvents txtCIN As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents txtFAX As System.Windows.Forms.TextBox
    Friend WithEvents txtEmail As System.Windows.Forms.TextBox
    Friend WithEvents txtPhoneNo As System.Windows.Forms.TextBox
    Friend WithEvents txtContactName As System.Windows.Forms.TextBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents BtnPrevious As System.Windows.Forms.Button
    Friend WithEvents BtnNextOK As System.Windows.Forms.Button
    Friend WithEvents txtTargetFemale As System.Windows.Forms.TextBox
    Friend WithEvents txtTargetMale As System.Windows.Forms.TextBox
    Friend WithEvents txtTotalTarget As System.Windows.Forms.TextBox
    Friend WithEvents txtTotalMember As System.Windows.Forms.TextBox
    Friend WithEvents txtTotalAss As System.Windows.Forms.TextBox
    Friend WithEvents txtTotalReg As System.Windows.Forms.TextBox
    Friend WithEvents txtTotalFemale As System.Windows.Forms.TextBox
    Friend WithEvents txtTotalMale As System.Windows.Forms.TextBox
    Friend WithEvents txtAssFemale As System.Windows.Forms.TextBox
    Friend WithEvents txtRegFemale As System.Windows.Forms.TextBox
    Friend WithEvents txtAssMale As System.Windows.Forms.TextBox
    Friend WithEvents txtRegMale As System.Windows.Forms.TextBox
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents OpCtrl1a As OptionalControl.OptionalControl
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents OpCtrl1c As OptionalControl.OptionalControl
    Friend WithEvents OpCtrl1bIf As OptionalControl.OptionalControl
    Friend WithEvents OpCtrl1b As OptionalControl.OptionalControl
    Friend WithEvents OpCtrl2a As OptionalControl.OptionalControl
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents OpCtrl1d As OptionalControl.OptionalControl
    Friend WithEvents OpCtrl2b As OptionalControl.OptionalControl
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents OpCtrl3a As OptionalControl.OptionalControl
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents OpCtrl2d As OptionalControl.OptionalControl
    Friend WithEvents OpCtrl2c As OptionalControl.OptionalControl
    Friend WithEvents OpCtrl4biii As OptionalControl.OptionalControl
    Friend WithEvents OpCtrl4bii As OptionalControl.OptionalControl
    Friend WithEvents OpCtrl4bi As OptionalControl.OptionalControl
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents OpCtrl4a As OptionalControl.OptionalControl
    Friend WithEvents OpCtrl4c As OptionalControl.OptionalControl
    Friend WithEvents OpCtrl4d As OptionalControl.OptionalControl
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents OpCtrl5b As OptionalControl.OptionalControl
    Friend WithEvents OpCtrl5a As OptionalControl.OptionalControl
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents OpCtrl5e As OptionalControl.OptionalControl
    Friend WithEvents OpCtrl5d As OptionalControl.OptionalControl
    Friend WithEvents OpCtrl5c As OptionalControl.OptionalControl
    Friend WithEvents OpCtrl7a As OptionalControl.OptionalControl
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents OpCtrl6b As OptionalControl.OptionalControl
    Friend WithEvents OpCtrl6a As OptionalControl.OptionalControl
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents OpCtrl7e As OptionalControl.OptionalControl
    Friend WithEvents OpCtrl7d As OptionalControl.OptionalControl
    Friend WithEvents OpCtrl7c As OptionalControl.OptionalControl
    Friend WithEvents OpCtrl7b As OptionalControl.OptionalControl
    Friend WithEvents OpCtrl7aIf As OptionalControl.OptionalControl
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents OpCtrl9k As OptionalControl.OptionalControl
    Friend WithEvents OpCtrl9j As OptionalControl.OptionalControl
    Friend WithEvents OpCtrl9i As OptionalControl.OptionalControl
    Friend WithEvents OpCtrl9h As OptionalControl.OptionalControl
    Friend WithEvents OpCtrl9g As OptionalControl.OptionalControl
    Friend WithEvents OpCtrl9f As OptionalControl.OptionalControl
    Friend WithEvents OpCtrl9e As OptionalControl.OptionalControl
    Friend WithEvents OpCtrl9d As OptionalControl.OptionalControl
    Friend WithEvents OpCtrl9c As OptionalControl.OptionalControl
    Friend WithEvents OpCtrl9b As OptionalControl.OptionalControl
    Friend WithEvents OpCtrl9a As OptionalControl.OptionalControl
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents OpCtrl8fv As OptionalControl.OptionalControl
    Friend WithEvents OpCtrl8fiv As OptionalControl.OptionalControl
    Friend WithEvents OpCtrl8fiii As OptionalControl.OptionalControl
    Friend WithEvents OpCtrl8fii As OptionalControl.OptionalControl
    Friend WithEvents OpCtrl8fi As OptionalControl.OptionalControl
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents OpCtrl8eiii As OptionalControl.OptionalControl
    Friend WithEvents OpCtrl8eii As OptionalControl.OptionalControl
    Friend WithEvents OpCtrl8ei As OptionalControl.OptionalControl
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents OpCtrl8d As OptionalControl.OptionalControl
    Friend WithEvents OpCtrl8c As OptionalControl.OptionalControl
    Friend WithEvents OpCtrl8b As OptionalControl.OptionalControl
    Friend WithEvents OpCtrl8a As OptionalControl.OptionalControl
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents OpCtrl11 As OptionalControl.OptionalControl
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents OpCtrl10c As OptionalControl.OptionalControl
    Friend WithEvents OpCtrl10b As OptionalControl.OptionalControl
    Friend WithEvents OpCtrl10a As OptionalControl.OptionalControl
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents CboAudFinState As System.Windows.Forms.ComboBox
    Friend WithEvents txtExtAudName As System.Windows.Forms.TextBox
    Friend WithEvents txtPeriodOpCoverd As System.Windows.Forms.TextBox
    Friend WithEvents txtIntAudDesign As System.Windows.Forms.TextBox
    Friend WithEvents txtIntAudName As System.Windows.Forms.TextBox
    Friend WithEvents dtExtAud As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtLastIntAudRpt As System.Windows.Forms.DateTimePicker
    Friend WithEvents NumFreqIntAudit As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label56 As System.Windows.Forms.Label
    Friend WithEvents Label55 As System.Windows.Forms.Label
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents Label58 As System.Windows.Forms.Label
    Friend WithEvents Label57 As System.Windows.Forms.Label
    Friend WithEvents Label53 As System.Windows.Forms.Label
    Friend WithEvents Label52 As System.Windows.Forms.Label
    Friend WithEvents Label51 As System.Windows.Forms.Label
    Friend WithEvents Label50 As System.Windows.Forms.Label
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents txtOthers As System.Windows.Forms.TextBox
    Friend WithEvents ChkOthers As System.Windows.Forms.CheckBox
    Friend WithEvents ChkWholesaleRetail As System.Windows.Forms.CheckBox
    Friend WithEvents ChkHotel As System.Windows.Forms.CheckBox
    Friend WithEvents ChkManufacturing As System.Windows.Forms.CheckBox
    Friend WithEvents ChkAgriculture As System.Windows.Forms.CheckBox
    Friend WithEvents ChkEducation As System.Windows.Forms.CheckBox
    Friend WithEvents ChkElectricity As System.Windows.Forms.CheckBox
    Friend WithEvents ChkFishing As System.Windows.Forms.CheckBox
    Friend WithEvents ChkHealth As System.Windows.Forms.CheckBox
    Friend WithEvents ChkRealEstate As System.Windows.Forms.CheckBox
    Friend WithEvents ChkTransport As System.Windows.Forms.CheckBox
    Friend WithEvents ChkConstruction As System.Windows.Forms.CheckBox
    Friend WithEvents ChkMining As System.Windows.Forms.CheckBox
    Friend WithEvents ChkFinance As System.Windows.Forms.CheckBox
    Friend WithEvents btnCancel As System.Windows.Forms.Button
End Class
