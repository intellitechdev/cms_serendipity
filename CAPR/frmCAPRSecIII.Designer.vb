<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCAPRSecIII
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle19 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle20 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle21 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle22 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle23 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle24 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle25 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle26 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle27 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle28 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle29 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle30 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.TabControl1 = New System.Windows.Forms.TabControl
        Me.TabPage1 = New System.Windows.Forms.TabPage
        Me.TabPage2 = New System.Windows.Forms.TabPage
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.grdInvestment = New System.Windows.Forms.DataGridView
        Me.ColType = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Col1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Col2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Col3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ColTotal = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.txtGovernTotal = New System.Windows.Forms.TextBox
        Me.txtGovern3 = New System.Windows.Forms.TextBox
        Me.txtGovern2 = New System.Windows.Forms.TextBox
        Me.txtGovern1 = New System.Windows.Forms.TextBox
        Me.txtInvstTotal = New System.Windows.Forms.TextBox
        Me.txtInvst3 = New System.Windows.Forms.TextBox
        Me.txtInvst2 = New System.Windows.Forms.TextBox
        Me.txtInvst1 = New System.Windows.Forms.TextBox
        Me.txtLongTermTotal = New System.Windows.Forms.TextBox
        Me.txtLongTerm3 = New System.Windows.Forms.TextBox
        Me.txtLongTerm2 = New System.Windows.Forms.TextBox
        Me.txtLongTerm1 = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel
        Me.lblTotalInvestment = New System.Windows.Forms.ToolStripStatusLabel
        Me.TabPage3 = New System.Windows.Forms.TabPage
        Me.StatusStrip2 = New System.Windows.Forms.StatusStrip
        Me.ToolStripStatusLabel3 = New System.Windows.Forms.ToolStripStatusLabel
        Me.tsLblTotalExtBorwng = New System.Windows.Forms.ToolStripStatusLabel
        Me.txtPrivFinInst = New System.Windows.Forms.TextBox
        Me.txtGovernFinInst = New System.Windows.Forms.TextBox
        Me.txtAmntLocCur = New System.Windows.Forms.TextBox
        Me.txtConverRate = New System.Windows.Forms.TextBox
        Me.txtAmntForgnCur = New System.Windows.Forms.TextBox
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.GrdExternalBorrwing = New System.Windows.Forms.DataGridView
        Me.ColSource = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ColAmount = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Label12 = New System.Windows.Forms.Label
        Me.Label19 = New System.Windows.Forms.Label
        Me.Label18 = New System.Windows.Forms.Label
        Me.Label17 = New System.Windows.Forms.Label
        Me.Label16 = New System.Windows.Forms.Label
        Me.Label15 = New System.Windows.Forms.Label
        Me.Label14 = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.TabPage4 = New System.Windows.Forms.TabPage
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.grdSavingDeposits = New System.Windows.Forms.DataGridView
        Me.ColTypes = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ColRegNoMembers = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ColregNoAccounts = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ColRegTotal = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ColAssNoMembers = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ColAssNoAccount = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ColAssTotal = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.txtNonWthdrwl6 = New System.Windows.Forms.TextBox
        Me.txt5yr6 = New System.Windows.Forms.TextBox
        Me.txt15yr6 = New System.Windows.Forms.TextBox
        Me.txt901yr6 = New System.Windows.Forms.TextBox
        Me.txt3090Dys6 = New System.Windows.Forms.TextBox
        Me.txt30Dys6 = New System.Windows.Forms.TextBox
        Me.txtWithdrwl6 = New System.Windows.Forms.TextBox
        Me.txtNonWthdrwl3 = New System.Windows.Forms.TextBox
        Me.txt5yr3 = New System.Windows.Forms.TextBox
        Me.txt15yr3 = New System.Windows.Forms.TextBox
        Me.txt901yr3 = New System.Windows.Forms.TextBox
        Me.txt3090Dys3 = New System.Windows.Forms.TextBox
        Me.txt30Dys3 = New System.Windows.Forms.TextBox
        Me.txtWithdrwl3 = New System.Windows.Forms.TextBox
        Me.txtNonWthdrwl5 = New System.Windows.Forms.TextBox
        Me.txtNonWthdrwl2 = New System.Windows.Forms.TextBox
        Me.txt5yr5 = New System.Windows.Forms.TextBox
        Me.txt5yr2 = New System.Windows.Forms.TextBox
        Me.txt15yr5 = New System.Windows.Forms.TextBox
        Me.txt15yr2 = New System.Windows.Forms.TextBox
        Me.txt901yr5 = New System.Windows.Forms.TextBox
        Me.txt901yr2 = New System.Windows.Forms.TextBox
        Me.txt3090Dys5 = New System.Windows.Forms.TextBox
        Me.txtNonWthdrwl4 = New System.Windows.Forms.TextBox
        Me.txt3090Dys2 = New System.Windows.Forms.TextBox
        Me.txt5yr4 = New System.Windows.Forms.TextBox
        Me.txt30Dys5 = New System.Windows.Forms.TextBox
        Me.txt15yr4 = New System.Windows.Forms.TextBox
        Me.txt30Dys2 = New System.Windows.Forms.TextBox
        Me.txt901yr4 = New System.Windows.Forms.TextBox
        Me.txtNonWthdrwl1 = New System.Windows.Forms.TextBox
        Me.txt3090Dys4 = New System.Windows.Forms.TextBox
        Me.txt5yr1 = New System.Windows.Forms.TextBox
        Me.txt15yr1 = New System.Windows.Forms.TextBox
        Me.txt30Dys4 = New System.Windows.Forms.TextBox
        Me.txt901yr1 = New System.Windows.Forms.TextBox
        Me.txtWithdrwl5 = New System.Windows.Forms.TextBox
        Me.txt3090Dys1 = New System.Windows.Forms.TextBox
        Me.txt30Dys1 = New System.Windows.Forms.TextBox
        Me.txtWithdrwl2 = New System.Windows.Forms.TextBox
        Me.txtWithdrwl4 = New System.Windows.Forms.TextBox
        Me.txtWithdrwl1 = New System.Windows.Forms.TextBox
        Me.Label23 = New System.Windows.Forms.Label
        Me.Label29 = New System.Windows.Forms.Label
        Me.Label26 = New System.Windows.Forms.Label
        Me.Label28 = New System.Windows.Forms.Label
        Me.Label25 = New System.Windows.Forms.Label
        Me.Label27 = New System.Windows.Forms.Label
        Me.Label24 = New System.Windows.Forms.Label
        Me.Label22 = New System.Windows.Forms.Label
        Me.Label37 = New System.Windows.Forms.Label
        Me.Label36 = New System.Windows.Forms.Label
        Me.Label35 = New System.Windows.Forms.Label
        Me.Label34 = New System.Windows.Forms.Label
        Me.Label33 = New System.Windows.Forms.Label
        Me.Label32 = New System.Windows.Forms.Label
        Me.Label31 = New System.Windows.Forms.Label
        Me.Label30 = New System.Windows.Forms.Label
        Me.Label21 = New System.Windows.Forms.Label
        Me.Label20 = New System.Windows.Forms.Label
        Me.TabPage5 = New System.Windows.Forms.TabPage
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer
        Me.grdByLoanType = New System.Windows.Forms.DataGridView
        Me.ColParticular = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ColNoOfLoan = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ColLoanAmount = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Label42 = New System.Windows.Forms.Label
        Me.GrdByMaturity = New System.Windows.Forms.DataGridView
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Label43 = New System.Windows.Forms.Label
        Me.StatusStrip3 = New System.Windows.Forms.StatusStrip
        Me.ToolStripStatusLabel2 = New System.Windows.Forms.ToolStripStatusLabel
        Me.tslblTotalLoanReceivable = New System.Windows.Forms.ToolStripStatusLabel
        Me.Label41 = New System.Windows.Forms.Label
        Me.Label40 = New System.Windows.Forms.Label
        Me.Label39 = New System.Windows.Forms.Label
        Me.Label38 = New System.Windows.Forms.Label
        Me.btnPrevious = New System.Windows.Forms.Button
        Me.btnNext = New System.Windows.Forms.Button
        Me.BgLoanReceivable = New System.ComponentModel.BackgroundWorker
        Me.btnCancel = New System.Windows.Forms.Button
        Me.TabControl1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.grdInvestment, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.StatusStrip2.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.GrdExternalBorrwing, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage4.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.grdSavingDeposits, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage5.SuspendLayout()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        CType(Me.grdByLoanType, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GrdByMaturity, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip3.SuspendLayout()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Controls.Add(Me.TabPage4)
        Me.TabControl1.Controls.Add(Me.TabPage5)
        Me.TabControl1.Location = New System.Drawing.Point(2, -1)
        Me.TabControl1.MinimumSize = New System.Drawing.Size(838, 501)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(838, 501)
        Me.TabControl1.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.White
        Me.TabPage1.Location = New System.Drawing.Point(4, 24)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(830, 473)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "A"
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.Color.White
        Me.TabPage2.Controls.Add(Me.GroupBox1)
        Me.TabPage2.Controls.Add(Me.txtGovernTotal)
        Me.TabPage2.Controls.Add(Me.txtGovern3)
        Me.TabPage2.Controls.Add(Me.txtGovern2)
        Me.TabPage2.Controls.Add(Me.txtGovern1)
        Me.TabPage2.Controls.Add(Me.txtInvstTotal)
        Me.TabPage2.Controls.Add(Me.txtInvst3)
        Me.TabPage2.Controls.Add(Me.txtInvst2)
        Me.TabPage2.Controls.Add(Me.txtInvst1)
        Me.TabPage2.Controls.Add(Me.txtLongTermTotal)
        Me.TabPage2.Controls.Add(Me.txtLongTerm3)
        Me.TabPage2.Controls.Add(Me.txtLongTerm2)
        Me.TabPage2.Controls.Add(Me.txtLongTerm1)
        Me.TabPage2.Controls.Add(Me.Label4)
        Me.TabPage2.Controls.Add(Me.Label3)
        Me.TabPage2.Controls.Add(Me.Label2)
        Me.TabPage2.Controls.Add(Me.Label8)
        Me.TabPage2.Controls.Add(Me.Label7)
        Me.TabPage2.Controls.Add(Me.Label6)
        Me.TabPage2.Controls.Add(Me.Label5)
        Me.TabPage2.Controls.Add(Me.Label9)
        Me.TabPage2.Controls.Add(Me.Label1)
        Me.TabPage2.Controls.Add(Me.StatusStrip1)
        Me.TabPage2.Location = New System.Drawing.Point(4, 24)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(830, 473)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "B"
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.grdInvestment)
        Me.GroupBox1.Location = New System.Drawing.Point(3, 182)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(821, 254)
        Me.GroupBox1.TabIndex = 3
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Others (please specify)"
        '
        'grdInvestment
        '
        Me.grdInvestment.BackgroundColor = System.Drawing.Color.White
        Me.grdInvestment.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdInvestment.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ColType, Me.Col1, Me.Col2, Me.Col3, Me.ColTotal})
        Me.grdInvestment.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grdInvestment.GridColor = System.Drawing.Color.Black
        Me.grdInvestment.Location = New System.Drawing.Point(3, 19)
        Me.grdInvestment.Name = "grdInvestment"
        Me.grdInvestment.Size = New System.Drawing.Size(815, 232)
        Me.grdInvestment.TabIndex = 0
        '
        'ColType
        '
        Me.ColType.HeaderText = "Type of Investment"
        Me.ColType.Name = "ColType"
        Me.ColType.Width = 200
        '
        'Col1
        '
        DataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle16.Format = "N2"
        DataGridViewCellStyle16.NullValue = "0.00"
        Me.Col1.DefaultCellStyle = DataGridViewCellStyle16
        Me.Col1.HeaderText = "Investment will mature within One to 3 years (1)"
        Me.Col1.Name = "Col1"
        Me.Col1.Width = 150
        '
        'Col2
        '
        DataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle17.Format = "N2"
        DataGridViewCellStyle17.NullValue = "0.00"
        Me.Col2.DefaultCellStyle = DataGridViewCellStyle17
        Me.Col2.HeaderText = "Investment has maturity of more than 3 years to 5 years (2)"
        Me.Col2.Name = "Col2"
        Me.Col2.Width = 150
        '
        'Col3
        '
        DataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle18.Format = "N2"
        DataGridViewCellStyle18.NullValue = "0.00"
        Me.Col3.DefaultCellStyle = DataGridViewCellStyle18
        Me.Col3.HeaderText = "Investment with more than 5 years maturity or maturity period not specified (3)"
        Me.Col3.Name = "Col3"
        Me.Col3.Width = 150
        '
        'ColTotal
        '
        DataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle19.NullValue = "0.00"
        Me.ColTotal.DefaultCellStyle = DataGridViewCellStyle19
        Me.ColTotal.HeaderText = "Total Amount (1) + (2) + (3)"
        Me.ColTotal.Name = "ColTotal"
        Me.ColTotal.ReadOnly = True
        Me.ColTotal.Width = 150
        '
        'txtGovernTotal
        '
        Me.txtGovernTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtGovernTotal.BackColor = System.Drawing.Color.White
        Me.txtGovernTotal.Location = New System.Drawing.Point(694, 157)
        Me.txtGovernTotal.Name = "txtGovernTotal"
        Me.txtGovernTotal.ReadOnly = True
        Me.txtGovernTotal.Size = New System.Drawing.Size(130, 23)
        Me.txtGovernTotal.TabIndex = 2
        Me.txtGovernTotal.TabStop = False
        Me.txtGovernTotal.Text = "0.00"
        Me.txtGovernTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtGovern3
        '
        Me.txtGovern3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtGovern3.Location = New System.Drawing.Point(558, 157)
        Me.txtGovern3.Name = "txtGovern3"
        Me.txtGovern3.Size = New System.Drawing.Size(130, 23)
        Me.txtGovern3.TabIndex = 2
        Me.txtGovern3.TabStop = False
        Me.txtGovern3.Text = "0.00"
        Me.txtGovern3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtGovern2
        '
        Me.txtGovern2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtGovern2.Location = New System.Drawing.Point(422, 157)
        Me.txtGovern2.Name = "txtGovern2"
        Me.txtGovern2.Size = New System.Drawing.Size(130, 23)
        Me.txtGovern2.TabIndex = 2
        Me.txtGovern2.TabStop = False
        Me.txtGovern2.Text = "0.00"
        Me.txtGovern2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtGovern1
        '
        Me.txtGovern1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtGovern1.Location = New System.Drawing.Point(286, 157)
        Me.txtGovern1.Name = "txtGovern1"
        Me.txtGovern1.Size = New System.Drawing.Size(130, 23)
        Me.txtGovern1.TabIndex = 2
        Me.txtGovern1.TabStop = False
        Me.txtGovern1.Text = "0.00"
        Me.txtGovern1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtInvstTotal
        '
        Me.txtInvstTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtInvstTotal.BackColor = System.Drawing.Color.White
        Me.txtInvstTotal.Location = New System.Drawing.Point(694, 128)
        Me.txtInvstTotal.Name = "txtInvstTotal"
        Me.txtInvstTotal.ReadOnly = True
        Me.txtInvstTotal.Size = New System.Drawing.Size(130, 23)
        Me.txtInvstTotal.TabIndex = 2
        Me.txtInvstTotal.TabStop = False
        Me.txtInvstTotal.Text = "0.00"
        Me.txtInvstTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtInvst3
        '
        Me.txtInvst3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtInvst3.Location = New System.Drawing.Point(558, 128)
        Me.txtInvst3.Name = "txtInvst3"
        Me.txtInvst3.Size = New System.Drawing.Size(130, 23)
        Me.txtInvst3.TabIndex = 2
        Me.txtInvst3.TabStop = False
        Me.txtInvst3.Text = "0.00"
        Me.txtInvst3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtInvst2
        '
        Me.txtInvst2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtInvst2.Location = New System.Drawing.Point(422, 128)
        Me.txtInvst2.Name = "txtInvst2"
        Me.txtInvst2.Size = New System.Drawing.Size(130, 23)
        Me.txtInvst2.TabIndex = 2
        Me.txtInvst2.TabStop = False
        Me.txtInvst2.Text = "0.00"
        Me.txtInvst2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtInvst1
        '
        Me.txtInvst1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtInvst1.Location = New System.Drawing.Point(286, 128)
        Me.txtInvst1.Name = "txtInvst1"
        Me.txtInvst1.Size = New System.Drawing.Size(130, 23)
        Me.txtInvst1.TabIndex = 2
        Me.txtInvst1.TabStop = False
        Me.txtInvst1.Text = "0.00"
        Me.txtInvst1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtLongTermTotal
        '
        Me.txtLongTermTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtLongTermTotal.BackColor = System.Drawing.Color.White
        Me.txtLongTermTotal.Location = New System.Drawing.Point(694, 99)
        Me.txtLongTermTotal.Name = "txtLongTermTotal"
        Me.txtLongTermTotal.ReadOnly = True
        Me.txtLongTermTotal.Size = New System.Drawing.Size(130, 23)
        Me.txtLongTermTotal.TabIndex = 2
        Me.txtLongTermTotal.TabStop = False
        Me.txtLongTermTotal.Text = "0.00"
        Me.txtLongTermTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtLongTerm3
        '
        Me.txtLongTerm3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtLongTerm3.Location = New System.Drawing.Point(558, 99)
        Me.txtLongTerm3.Name = "txtLongTerm3"
        Me.txtLongTerm3.Size = New System.Drawing.Size(130, 23)
        Me.txtLongTerm3.TabIndex = 2
        Me.txtLongTerm3.TabStop = False
        Me.txtLongTerm3.Text = "0.00"
        Me.txtLongTerm3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtLongTerm2
        '
        Me.txtLongTerm2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtLongTerm2.Location = New System.Drawing.Point(422, 99)
        Me.txtLongTerm2.Name = "txtLongTerm2"
        Me.txtLongTerm2.Size = New System.Drawing.Size(130, 23)
        Me.txtLongTerm2.TabIndex = 2
        Me.txtLongTerm2.TabStop = False
        Me.txtLongTerm2.Text = "0.00"
        Me.txtLongTerm2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtLongTerm1
        '
        Me.txtLongTerm1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtLongTerm1.Location = New System.Drawing.Point(286, 99)
        Me.txtLongTerm1.Name = "txtLongTerm1"
        Me.txtLongTerm1.Size = New System.Drawing.Size(130, 23)
        Me.txtLongTerm1.TabIndex = 2
        Me.txtLongTerm1.TabStop = False
        Me.txtLongTerm1.Text = "0.00"
        Me.txtLongTerm1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(6, 160)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(123, 15)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "Goverment Securities"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(6, 131)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(212, 15)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Investment in Cooperative Federation"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 102)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(163, 15)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Long Term Deposits in Banks"
        '
        'Label8
        '
        Me.Label8.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(714, 38)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(80, 30)
        Me.Label8.TabIndex = 1
        Me.Label8.Text = "Total Amount" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "(1) + (2) + (3)"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label7
        '
        Me.Label7.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(555, 23)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(129, 75)
        Me.Label7.TabIndex = 1
        Me.Label7.Text = "Investment with more" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "than 5 year maturity" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "or maturity period not" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "specified" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "(3" & _
            ")"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label6
        '
        Me.Label6.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(414, 23)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(140, 60)
        Me.Label6.TabIndex = 1
        Me.Label6.Text = "Investment has maturity" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "of more than 3 years to" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "5 years" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "(2)"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label5
        '
        Me.Label5.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(283, 23)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(133, 45)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = "Investment will mature" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "within One to 3 years" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "(1)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label9
        '
        Me.Label9.BackColor = System.Drawing.Color.Gray
        Me.Label9.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label9.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.White
        Me.Label9.Location = New System.Drawing.Point(3, 3)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(824, 20)
        Me.Label9.TabIndex = 1
        Me.Label9.Text = "Information of Investment"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(48, 38)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(109, 15)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Type of Investment"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1, Me.lblTotalInvestment})
        Me.StatusStrip1.Location = New System.Drawing.Point(3, 449)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(829, 22)
        Me.StatusStrip1.TabIndex = 0
        Me.StatusStrip1.Text = "StatusStrip1"
        Me.StatusStrip1.Visible = False
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.BackColor = System.Drawing.Color.Transparent
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(93, 17)
        Me.ToolStripStatusLabel1.Text = "Total Investment:"
        '
        'lblTotalInvestment
        '
        Me.lblTotalInvestment.BackColor = System.Drawing.Color.Transparent
        Me.lblTotalInvestment.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.lblTotalInvestment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalInvestment.Name = "lblTotalInvestment"
        Me.lblTotalInvestment.Size = New System.Drawing.Size(31, 17)
        Me.lblTotalInvestment.Text = "0.00"
        '
        'TabPage3
        '
        Me.TabPage3.BackColor = System.Drawing.Color.White
        Me.TabPage3.Controls.Add(Me.StatusStrip2)
        Me.TabPage3.Controls.Add(Me.txtPrivFinInst)
        Me.TabPage3.Controls.Add(Me.txtGovernFinInst)
        Me.TabPage3.Controls.Add(Me.txtAmntLocCur)
        Me.TabPage3.Controls.Add(Me.txtConverRate)
        Me.TabPage3.Controls.Add(Me.txtAmntForgnCur)
        Me.TabPage3.Controls.Add(Me.GroupBox2)
        Me.TabPage3.Controls.Add(Me.Label12)
        Me.TabPage3.Controls.Add(Me.Label19)
        Me.TabPage3.Controls.Add(Me.Label18)
        Me.TabPage3.Controls.Add(Me.Label17)
        Me.TabPage3.Controls.Add(Me.Label16)
        Me.TabPage3.Controls.Add(Me.Label15)
        Me.TabPage3.Controls.Add(Me.Label14)
        Me.TabPage3.Controls.Add(Me.Label13)
        Me.TabPage3.Controls.Add(Me.Label11)
        Me.TabPage3.Controls.Add(Me.Label10)
        Me.TabPage3.Location = New System.Drawing.Point(4, 24)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(830, 473)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "C"
        '
        'StatusStrip2
        '
        Me.StatusStrip2.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel3, Me.tsLblTotalExtBorwng})
        Me.StatusStrip2.Location = New System.Drawing.Point(3, 448)
        Me.StatusStrip2.Name = "StatusStrip2"
        Me.StatusStrip2.Size = New System.Drawing.Size(829, 23)
        Me.StatusStrip2.TabIndex = 6
        Me.StatusStrip2.Text = "StatusStrip2"
        Me.StatusStrip2.Visible = False
        '
        'ToolStripStatusLabel3
        '
        Me.ToolStripStatusLabel3.BackColor = System.Drawing.Color.Transparent
        Me.ToolStripStatusLabel3.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripStatusLabel3.Name = "ToolStripStatusLabel3"
        Me.ToolStripStatusLabel3.Size = New System.Drawing.Size(211, 18)
        Me.ToolStripStatusLabel3.Text = "Total Amount of External Borrowings:"
        '
        'tsLblTotalExtBorwng
        '
        Me.tsLblTotalExtBorwng.BackColor = System.Drawing.Color.Transparent
        Me.tsLblTotalExtBorwng.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tsLblTotalExtBorwng.Name = "tsLblTotalExtBorwng"
        Me.tsLblTotalExtBorwng.Size = New System.Drawing.Size(33, 18)
        Me.tsLblTotalExtBorwng.Text = "0.00"
        '
        'txtPrivFinInst
        '
        Me.txtPrivFinInst.Location = New System.Drawing.Point(273, 213)
        Me.txtPrivFinInst.Name = "txtPrivFinInst"
        Me.txtPrivFinInst.Size = New System.Drawing.Size(200, 23)
        Me.txtPrivFinInst.TabIndex = 5
        Me.txtPrivFinInst.Text = "0.00"
        Me.txtPrivFinInst.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtGovernFinInst
        '
        Me.txtGovernFinInst.Location = New System.Drawing.Point(273, 184)
        Me.txtGovernFinInst.Name = "txtGovernFinInst"
        Me.txtGovernFinInst.Size = New System.Drawing.Size(200, 23)
        Me.txtGovernFinInst.TabIndex = 5
        Me.txtGovernFinInst.Text = "0.00"
        Me.txtGovernFinInst.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtAmntLocCur
        '
        Me.txtAmntLocCur.BackColor = System.Drawing.Color.White
        Me.txtAmntLocCur.Location = New System.Drawing.Point(273, 139)
        Me.txtAmntLocCur.Name = "txtAmntLocCur"
        Me.txtAmntLocCur.ReadOnly = True
        Me.txtAmntLocCur.Size = New System.Drawing.Size(200, 23)
        Me.txtAmntLocCur.TabIndex = 5
        Me.txtAmntLocCur.Text = "0.00"
        Me.txtAmntLocCur.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtConverRate
        '
        Me.txtConverRate.Location = New System.Drawing.Point(273, 110)
        Me.txtConverRate.Name = "txtConverRate"
        Me.txtConverRate.Size = New System.Drawing.Size(200, 23)
        Me.txtConverRate.TabIndex = 5
        Me.txtConverRate.Text = "0.00"
        Me.txtConverRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtAmntForgnCur
        '
        Me.txtAmntForgnCur.Location = New System.Drawing.Point(273, 81)
        Me.txtAmntForgnCur.Name = "txtAmntForgnCur"
        Me.txtAmntForgnCur.Size = New System.Drawing.Size(200, 23)
        Me.txtAmntForgnCur.TabIndex = 5
        Me.txtAmntForgnCur.Text = "0.00"
        Me.txtAmntForgnCur.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'GroupBox2
        '
        Me.GroupBox2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox2.Controls.Add(Me.GrdExternalBorrwing)
        Me.GroupBox2.Location = New System.Drawing.Point(7, 242)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(817, 193)
        Me.GroupBox2.TabIndex = 4
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Others, please specify"
        '
        'GrdExternalBorrwing
        '
        Me.GrdExternalBorrwing.BackgroundColor = System.Drawing.Color.White
        Me.GrdExternalBorrwing.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.GrdExternalBorrwing.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ColSource, Me.ColAmount})
        Me.GrdExternalBorrwing.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GrdExternalBorrwing.GridColor = System.Drawing.Color.Black
        Me.GrdExternalBorrwing.Location = New System.Drawing.Point(3, 19)
        Me.GrdExternalBorrwing.Name = "GrdExternalBorrwing"
        Me.GrdExternalBorrwing.Size = New System.Drawing.Size(811, 171)
        Me.GrdExternalBorrwing.TabIndex = 0
        '
        'ColSource
        '
        Me.ColSource.HeaderText = "Source of Credit"
        Me.ColSource.Name = "ColSource"
        Me.ColSource.Width = 250
        '
        'ColAmount
        '
        DataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle20.Format = "N2"
        DataGridViewCellStyle20.NullValue = "0.00"
        Me.ColAmount.DefaultCellStyle = DataGridViewCellStyle20
        Me.ColAmount.HeaderText = "Amount"
        Me.ColAmount.Name = "ColAmount"
        Me.ColAmount.Width = 150
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(257, 35)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(248, 30)
        Me.Label12.TabIndex = 3
        Me.Label12.Text = "Amount of Outstanding External Borrowings" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "as of Reporting Period"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(34, 216)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(168, 15)
        Me.Label19.TabIndex = 3
        Me.Label19.Text = "Private Financial Institutions"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(34, 187)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(195, 15)
        Me.Label18.TabIndex = 3
        Me.Label18.Text = "Government Financial Institutions"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(18, 170)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(71, 15)
        Me.Label17.TabIndex = 3
        Me.Label17.Text = "Local Loans"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(34, 142)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(147, 15)
        Me.Label16.TabIndex = 3
        Me.Label16.Text = "Amount in Local Currency"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(34, 113)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(96, 15)
        Me.Label15.TabIndex = 3
        Me.Label15.Text = "Conversion Rate"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(34, 84)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(159, 15)
        Me.Label14.TabIndex = 3
        Me.Label14.Text = "Amount of Foreign Currency"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(18, 67)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(83, 15)
        Me.Label13.TabIndex = 3
        Me.Label13.Text = "Foreign Loans"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(75, 35)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(94, 15)
        Me.Label11.TabIndex = 3
        Me.Label11.Text = "Source of Credit"
        '
        'Label10
        '
        Me.Label10.BackColor = System.Drawing.Color.Gray
        Me.Label10.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label10.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.White
        Me.Label10.Location = New System.Drawing.Point(3, 3)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(824, 20)
        Me.Label10.TabIndex = 2
        Me.Label10.Text = "Information of External Borrowings"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TabPage4
        '
        Me.TabPage4.BackColor = System.Drawing.Color.White
        Me.TabPage4.Controls.Add(Me.GroupBox3)
        Me.TabPage4.Controls.Add(Me.txtNonWthdrwl6)
        Me.TabPage4.Controls.Add(Me.txt5yr6)
        Me.TabPage4.Controls.Add(Me.txt15yr6)
        Me.TabPage4.Controls.Add(Me.txt901yr6)
        Me.TabPage4.Controls.Add(Me.txt3090Dys6)
        Me.TabPage4.Controls.Add(Me.txt30Dys6)
        Me.TabPage4.Controls.Add(Me.txtWithdrwl6)
        Me.TabPage4.Controls.Add(Me.txtNonWthdrwl3)
        Me.TabPage4.Controls.Add(Me.txt5yr3)
        Me.TabPage4.Controls.Add(Me.txt15yr3)
        Me.TabPage4.Controls.Add(Me.txt901yr3)
        Me.TabPage4.Controls.Add(Me.txt3090Dys3)
        Me.TabPage4.Controls.Add(Me.txt30Dys3)
        Me.TabPage4.Controls.Add(Me.txtWithdrwl3)
        Me.TabPage4.Controls.Add(Me.txtNonWthdrwl5)
        Me.TabPage4.Controls.Add(Me.txtNonWthdrwl2)
        Me.TabPage4.Controls.Add(Me.txt5yr5)
        Me.TabPage4.Controls.Add(Me.txt5yr2)
        Me.TabPage4.Controls.Add(Me.txt15yr5)
        Me.TabPage4.Controls.Add(Me.txt15yr2)
        Me.TabPage4.Controls.Add(Me.txt901yr5)
        Me.TabPage4.Controls.Add(Me.txt901yr2)
        Me.TabPage4.Controls.Add(Me.txt3090Dys5)
        Me.TabPage4.Controls.Add(Me.txtNonWthdrwl4)
        Me.TabPage4.Controls.Add(Me.txt3090Dys2)
        Me.TabPage4.Controls.Add(Me.txt5yr4)
        Me.TabPage4.Controls.Add(Me.txt30Dys5)
        Me.TabPage4.Controls.Add(Me.txt15yr4)
        Me.TabPage4.Controls.Add(Me.txt30Dys2)
        Me.TabPage4.Controls.Add(Me.txt901yr4)
        Me.TabPage4.Controls.Add(Me.txtNonWthdrwl1)
        Me.TabPage4.Controls.Add(Me.txt3090Dys4)
        Me.TabPage4.Controls.Add(Me.txt5yr1)
        Me.TabPage4.Controls.Add(Me.txt15yr1)
        Me.TabPage4.Controls.Add(Me.txt30Dys4)
        Me.TabPage4.Controls.Add(Me.txt901yr1)
        Me.TabPage4.Controls.Add(Me.txtWithdrwl5)
        Me.TabPage4.Controls.Add(Me.txt3090Dys1)
        Me.TabPage4.Controls.Add(Me.txt30Dys1)
        Me.TabPage4.Controls.Add(Me.txtWithdrwl2)
        Me.TabPage4.Controls.Add(Me.txtWithdrwl4)
        Me.TabPage4.Controls.Add(Me.txtWithdrwl1)
        Me.TabPage4.Controls.Add(Me.Label23)
        Me.TabPage4.Controls.Add(Me.Label29)
        Me.TabPage4.Controls.Add(Me.Label26)
        Me.TabPage4.Controls.Add(Me.Label28)
        Me.TabPage4.Controls.Add(Me.Label25)
        Me.TabPage4.Controls.Add(Me.Label27)
        Me.TabPage4.Controls.Add(Me.Label24)
        Me.TabPage4.Controls.Add(Me.Label22)
        Me.TabPage4.Controls.Add(Me.Label37)
        Me.TabPage4.Controls.Add(Me.Label36)
        Me.TabPage4.Controls.Add(Me.Label35)
        Me.TabPage4.Controls.Add(Me.Label34)
        Me.TabPage4.Controls.Add(Me.Label33)
        Me.TabPage4.Controls.Add(Me.Label32)
        Me.TabPage4.Controls.Add(Me.Label31)
        Me.TabPage4.Controls.Add(Me.Label30)
        Me.TabPage4.Controls.Add(Me.Label21)
        Me.TabPage4.Controls.Add(Me.Label20)
        Me.TabPage4.Location = New System.Drawing.Point(4, 24)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage4.Size = New System.Drawing.Size(830, 473)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "D"
        '
        'GroupBox3
        '
        Me.GroupBox3.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox3.Controls.Add(Me.grdSavingDeposits)
        Me.GroupBox3.Location = New System.Drawing.Point(7, 285)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(815, 182)
        Me.GroupBox3.TabIndex = 6
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Other Types of Deposits, please specify"
        '
        'grdSavingDeposits
        '
        Me.grdSavingDeposits.BackgroundColor = System.Drawing.Color.White
        Me.grdSavingDeposits.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdSavingDeposits.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ColTypes, Me.ColRegNoMembers, Me.ColregNoAccounts, Me.ColRegTotal, Me.ColAssNoMembers, Me.ColAssNoAccount, Me.ColAssTotal})
        Me.grdSavingDeposits.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grdSavingDeposits.GridColor = System.Drawing.Color.Black
        Me.grdSavingDeposits.Location = New System.Drawing.Point(3, 19)
        Me.grdSavingDeposits.Name = "grdSavingDeposits"
        Me.grdSavingDeposits.Size = New System.Drawing.Size(809, 160)
        Me.grdSavingDeposits.TabIndex = 0
        '
        'ColTypes
        '
        Me.ColTypes.HeaderText = "Types of Deposits"
        Me.ColTypes.Name = "ColTypes"
        Me.ColTypes.Width = 200
        '
        'ColRegNoMembers
        '
        DataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle21.Format = "N0"
        DataGridViewCellStyle21.NullValue = "0"
        Me.ColRegNoMembers.DefaultCellStyle = DataGridViewCellStyle21
        Me.ColRegNoMembers.HeaderText = "Reg. Members: No. of Members with Deposit Accounts"
        Me.ColRegNoMembers.Name = "ColRegNoMembers"
        Me.ColRegNoMembers.Width = 150
        '
        'ColregNoAccounts
        '
        DataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle22.Format = "N0"
        DataGridViewCellStyle22.NullValue = "0"
        Me.ColregNoAccounts.DefaultCellStyle = DataGridViewCellStyle22
        Me.ColregNoAccounts.HeaderText = "Regular Members: No. of Accounts"
        Me.ColregNoAccounts.Name = "ColregNoAccounts"
        Me.ColregNoAccounts.Width = 150
        '
        'ColRegTotal
        '
        DataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle23.Format = "N2"
        DataGridViewCellStyle23.NullValue = "0.00"
        Me.ColRegTotal.DefaultCellStyle = DataGridViewCellStyle23
        Me.ColRegTotal.HeaderText = "Regular Members: Total Amount"
        Me.ColRegTotal.Name = "ColRegTotal"
        Me.ColRegTotal.Width = 150
        '
        'ColAssNoMembers
        '
        DataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle24.Format = "N0"
        DataGridViewCellStyle24.NullValue = "0"
        Me.ColAssNoMembers.DefaultCellStyle = DataGridViewCellStyle24
        Me.ColAssNoMembers.HeaderText = "Associate Members: No. of Members with Deposit Accounts"
        Me.ColAssNoMembers.Name = "ColAssNoMembers"
        Me.ColAssNoMembers.Width = 150
        '
        'ColAssNoAccount
        '
        DataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle25.Format = "N0"
        DataGridViewCellStyle25.NullValue = "0"
        Me.ColAssNoAccount.DefaultCellStyle = DataGridViewCellStyle25
        Me.ColAssNoAccount.HeaderText = "Associate Members: No. of Accounts"
        Me.ColAssNoAccount.Name = "ColAssNoAccount"
        Me.ColAssNoAccount.Width = 150
        '
        'ColAssTotal
        '
        DataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle26.Format = "N0"
        DataGridViewCellStyle26.NullValue = "0.00"
        Me.ColAssTotal.DefaultCellStyle = DataGridViewCellStyle26
        Me.ColAssTotal.HeaderText = "Associate Members: Total Amount"
        Me.ColAssTotal.Name = "ColAssTotal"
        Me.ColAssTotal.Width = 150
        '
        'txtNonWthdrwl6
        '
        Me.txtNonWthdrwl6.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtNonWthdrwl6.Location = New System.Drawing.Point(722, 255)
        Me.txtNonWthdrwl6.Name = "txtNonWthdrwl6"
        Me.txtNonWthdrwl6.Size = New System.Drawing.Size(100, 23)
        Me.txtNonWthdrwl6.TabIndex = 5
        Me.txtNonWthdrwl6.Text = "0.00"
        Me.txtNonWthdrwl6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt5yr6
        '
        Me.txt5yr6.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt5yr6.Location = New System.Drawing.Point(722, 229)
        Me.txt5yr6.Name = "txt5yr6"
        Me.txt5yr6.Size = New System.Drawing.Size(100, 23)
        Me.txt5yr6.TabIndex = 5
        Me.txt5yr6.Text = "0.00"
        Me.txt5yr6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt15yr6
        '
        Me.txt15yr6.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt15yr6.Location = New System.Drawing.Point(722, 203)
        Me.txt15yr6.Name = "txt15yr6"
        Me.txt15yr6.Size = New System.Drawing.Size(100, 23)
        Me.txt15yr6.TabIndex = 5
        Me.txt15yr6.Text = "0.00"
        Me.txt15yr6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt901yr6
        '
        Me.txt901yr6.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt901yr6.Location = New System.Drawing.Point(722, 177)
        Me.txt901yr6.Name = "txt901yr6"
        Me.txt901yr6.Size = New System.Drawing.Size(100, 23)
        Me.txt901yr6.TabIndex = 5
        Me.txt901yr6.Text = "0.00"
        Me.txt901yr6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt3090Dys6
        '
        Me.txt3090Dys6.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt3090Dys6.Location = New System.Drawing.Point(722, 151)
        Me.txt3090Dys6.Name = "txt3090Dys6"
        Me.txt3090Dys6.Size = New System.Drawing.Size(100, 23)
        Me.txt3090Dys6.TabIndex = 5
        Me.txt3090Dys6.Text = "0.00"
        Me.txt3090Dys6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt30Dys6
        '
        Me.txt30Dys6.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt30Dys6.Location = New System.Drawing.Point(722, 125)
        Me.txt30Dys6.Name = "txt30Dys6"
        Me.txt30Dys6.Size = New System.Drawing.Size(100, 23)
        Me.txt30Dys6.TabIndex = 5
        Me.txt30Dys6.Text = "0.00"
        Me.txt30Dys6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtWithdrwl6
        '
        Me.txtWithdrwl6.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtWithdrwl6.Location = New System.Drawing.Point(722, 92)
        Me.txtWithdrwl6.Name = "txtWithdrwl6"
        Me.txtWithdrwl6.Size = New System.Drawing.Size(100, 23)
        Me.txtWithdrwl6.TabIndex = 5
        Me.txtWithdrwl6.Text = "0.00"
        Me.txtWithdrwl6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtNonWthdrwl3
        '
        Me.txtNonWthdrwl3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtNonWthdrwl3.Location = New System.Drawing.Point(413, 255)
        Me.txtNonWthdrwl3.Name = "txtNonWthdrwl3"
        Me.txtNonWthdrwl3.Size = New System.Drawing.Size(100, 23)
        Me.txtNonWthdrwl3.TabIndex = 5
        Me.txtNonWthdrwl3.Text = "0.00"
        Me.txtNonWthdrwl3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt5yr3
        '
        Me.txt5yr3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt5yr3.Location = New System.Drawing.Point(413, 229)
        Me.txt5yr3.Name = "txt5yr3"
        Me.txt5yr3.Size = New System.Drawing.Size(100, 23)
        Me.txt5yr3.TabIndex = 5
        Me.txt5yr3.Text = "0.00"
        Me.txt5yr3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt15yr3
        '
        Me.txt15yr3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt15yr3.Location = New System.Drawing.Point(413, 203)
        Me.txt15yr3.Name = "txt15yr3"
        Me.txt15yr3.Size = New System.Drawing.Size(100, 23)
        Me.txt15yr3.TabIndex = 5
        Me.txt15yr3.Text = "0.00"
        Me.txt15yr3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt901yr3
        '
        Me.txt901yr3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt901yr3.Location = New System.Drawing.Point(413, 177)
        Me.txt901yr3.Name = "txt901yr3"
        Me.txt901yr3.Size = New System.Drawing.Size(100, 23)
        Me.txt901yr3.TabIndex = 5
        Me.txt901yr3.Text = "0.00"
        Me.txt901yr3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt3090Dys3
        '
        Me.txt3090Dys3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt3090Dys3.Location = New System.Drawing.Point(413, 151)
        Me.txt3090Dys3.Name = "txt3090Dys3"
        Me.txt3090Dys3.Size = New System.Drawing.Size(100, 23)
        Me.txt3090Dys3.TabIndex = 5
        Me.txt3090Dys3.Text = "0.00"
        Me.txt3090Dys3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt30Dys3
        '
        Me.txt30Dys3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt30Dys3.Location = New System.Drawing.Point(413, 125)
        Me.txt30Dys3.Name = "txt30Dys3"
        Me.txt30Dys3.Size = New System.Drawing.Size(100, 23)
        Me.txt30Dys3.TabIndex = 5
        Me.txt30Dys3.Text = "0.00"
        Me.txt30Dys3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtWithdrwl3
        '
        Me.txtWithdrwl3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtWithdrwl3.Location = New System.Drawing.Point(413, 92)
        Me.txtWithdrwl3.Name = "txtWithdrwl3"
        Me.txtWithdrwl3.Size = New System.Drawing.Size(100, 23)
        Me.txtWithdrwl3.TabIndex = 5
        Me.txtWithdrwl3.Text = "0.00"
        Me.txtWithdrwl3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtNonWthdrwl5
        '
        Me.txtNonWthdrwl5.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtNonWthdrwl5.Location = New System.Drawing.Point(619, 255)
        Me.txtNonWthdrwl5.Name = "txtNonWthdrwl5"
        Me.txtNonWthdrwl5.Size = New System.Drawing.Size(100, 23)
        Me.txtNonWthdrwl5.TabIndex = 5
        Me.txtNonWthdrwl5.Text = "0"
        Me.txtNonWthdrwl5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtNonWthdrwl2
        '
        Me.txtNonWthdrwl2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtNonWthdrwl2.Location = New System.Drawing.Point(310, 255)
        Me.txtNonWthdrwl2.Name = "txtNonWthdrwl2"
        Me.txtNonWthdrwl2.Size = New System.Drawing.Size(100, 23)
        Me.txtNonWthdrwl2.TabIndex = 5
        Me.txtNonWthdrwl2.Text = "0"
        Me.txtNonWthdrwl2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt5yr5
        '
        Me.txt5yr5.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt5yr5.Location = New System.Drawing.Point(619, 229)
        Me.txt5yr5.Name = "txt5yr5"
        Me.txt5yr5.Size = New System.Drawing.Size(100, 23)
        Me.txt5yr5.TabIndex = 5
        Me.txt5yr5.Text = "0"
        Me.txt5yr5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt5yr2
        '
        Me.txt5yr2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt5yr2.Location = New System.Drawing.Point(310, 229)
        Me.txt5yr2.Name = "txt5yr2"
        Me.txt5yr2.Size = New System.Drawing.Size(100, 23)
        Me.txt5yr2.TabIndex = 5
        Me.txt5yr2.Text = "0"
        Me.txt5yr2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt15yr5
        '
        Me.txt15yr5.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt15yr5.Location = New System.Drawing.Point(619, 203)
        Me.txt15yr5.Name = "txt15yr5"
        Me.txt15yr5.Size = New System.Drawing.Size(100, 23)
        Me.txt15yr5.TabIndex = 5
        Me.txt15yr5.Text = "0"
        Me.txt15yr5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt15yr2
        '
        Me.txt15yr2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt15yr2.Location = New System.Drawing.Point(310, 203)
        Me.txt15yr2.Name = "txt15yr2"
        Me.txt15yr2.Size = New System.Drawing.Size(100, 23)
        Me.txt15yr2.TabIndex = 5
        Me.txt15yr2.Text = "0"
        Me.txt15yr2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt901yr5
        '
        Me.txt901yr5.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt901yr5.Location = New System.Drawing.Point(619, 177)
        Me.txt901yr5.Name = "txt901yr5"
        Me.txt901yr5.Size = New System.Drawing.Size(100, 23)
        Me.txt901yr5.TabIndex = 5
        Me.txt901yr5.Text = "0"
        Me.txt901yr5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt901yr2
        '
        Me.txt901yr2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt901yr2.Location = New System.Drawing.Point(310, 177)
        Me.txt901yr2.Name = "txt901yr2"
        Me.txt901yr2.Size = New System.Drawing.Size(100, 23)
        Me.txt901yr2.TabIndex = 5
        Me.txt901yr2.Text = "0"
        Me.txt901yr2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt3090Dys5
        '
        Me.txt3090Dys5.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt3090Dys5.Location = New System.Drawing.Point(619, 151)
        Me.txt3090Dys5.Name = "txt3090Dys5"
        Me.txt3090Dys5.Size = New System.Drawing.Size(100, 23)
        Me.txt3090Dys5.TabIndex = 5
        Me.txt3090Dys5.Text = "0"
        Me.txt3090Dys5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtNonWthdrwl4
        '
        Me.txtNonWthdrwl4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtNonWthdrwl4.Location = New System.Drawing.Point(516, 255)
        Me.txtNonWthdrwl4.Name = "txtNonWthdrwl4"
        Me.txtNonWthdrwl4.Size = New System.Drawing.Size(100, 23)
        Me.txtNonWthdrwl4.TabIndex = 5
        Me.txtNonWthdrwl4.Text = "0"
        Me.txtNonWthdrwl4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt3090Dys2
        '
        Me.txt3090Dys2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt3090Dys2.Location = New System.Drawing.Point(310, 151)
        Me.txt3090Dys2.Name = "txt3090Dys2"
        Me.txt3090Dys2.Size = New System.Drawing.Size(100, 23)
        Me.txt3090Dys2.TabIndex = 5
        Me.txt3090Dys2.Text = "0"
        Me.txt3090Dys2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt5yr4
        '
        Me.txt5yr4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt5yr4.Location = New System.Drawing.Point(516, 229)
        Me.txt5yr4.Name = "txt5yr4"
        Me.txt5yr4.Size = New System.Drawing.Size(100, 23)
        Me.txt5yr4.TabIndex = 5
        Me.txt5yr4.Text = "0"
        Me.txt5yr4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt30Dys5
        '
        Me.txt30Dys5.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt30Dys5.Location = New System.Drawing.Point(619, 125)
        Me.txt30Dys5.Name = "txt30Dys5"
        Me.txt30Dys5.Size = New System.Drawing.Size(100, 23)
        Me.txt30Dys5.TabIndex = 5
        Me.txt30Dys5.Text = "0"
        Me.txt30Dys5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt15yr4
        '
        Me.txt15yr4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt15yr4.Location = New System.Drawing.Point(516, 203)
        Me.txt15yr4.Name = "txt15yr4"
        Me.txt15yr4.Size = New System.Drawing.Size(100, 23)
        Me.txt15yr4.TabIndex = 5
        Me.txt15yr4.Text = "0"
        Me.txt15yr4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt30Dys2
        '
        Me.txt30Dys2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt30Dys2.Location = New System.Drawing.Point(310, 125)
        Me.txt30Dys2.Name = "txt30Dys2"
        Me.txt30Dys2.Size = New System.Drawing.Size(100, 23)
        Me.txt30Dys2.TabIndex = 5
        Me.txt30Dys2.Text = "0"
        Me.txt30Dys2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt901yr4
        '
        Me.txt901yr4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt901yr4.Location = New System.Drawing.Point(516, 177)
        Me.txt901yr4.Name = "txt901yr4"
        Me.txt901yr4.Size = New System.Drawing.Size(100, 23)
        Me.txt901yr4.TabIndex = 5
        Me.txt901yr4.Text = "0"
        Me.txt901yr4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtNonWthdrwl1
        '
        Me.txtNonWthdrwl1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtNonWthdrwl1.Location = New System.Drawing.Point(207, 255)
        Me.txtNonWthdrwl1.Name = "txtNonWthdrwl1"
        Me.txtNonWthdrwl1.Size = New System.Drawing.Size(100, 23)
        Me.txtNonWthdrwl1.TabIndex = 5
        Me.txtNonWthdrwl1.Text = "0"
        Me.txtNonWthdrwl1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt3090Dys4
        '
        Me.txt3090Dys4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt3090Dys4.Location = New System.Drawing.Point(516, 151)
        Me.txt3090Dys4.Name = "txt3090Dys4"
        Me.txt3090Dys4.Size = New System.Drawing.Size(100, 23)
        Me.txt3090Dys4.TabIndex = 5
        Me.txt3090Dys4.Text = "0"
        Me.txt3090Dys4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt5yr1
        '
        Me.txt5yr1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt5yr1.Location = New System.Drawing.Point(207, 229)
        Me.txt5yr1.Name = "txt5yr1"
        Me.txt5yr1.Size = New System.Drawing.Size(100, 23)
        Me.txt5yr1.TabIndex = 5
        Me.txt5yr1.Text = "0"
        Me.txt5yr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt15yr1
        '
        Me.txt15yr1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt15yr1.Location = New System.Drawing.Point(207, 203)
        Me.txt15yr1.Name = "txt15yr1"
        Me.txt15yr1.Size = New System.Drawing.Size(100, 23)
        Me.txt15yr1.TabIndex = 5
        Me.txt15yr1.Text = "0"
        Me.txt15yr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt30Dys4
        '
        Me.txt30Dys4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt30Dys4.Location = New System.Drawing.Point(516, 125)
        Me.txt30Dys4.Name = "txt30Dys4"
        Me.txt30Dys4.Size = New System.Drawing.Size(100, 23)
        Me.txt30Dys4.TabIndex = 5
        Me.txt30Dys4.Text = "0"
        Me.txt30Dys4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt901yr1
        '
        Me.txt901yr1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt901yr1.Location = New System.Drawing.Point(207, 177)
        Me.txt901yr1.Name = "txt901yr1"
        Me.txt901yr1.Size = New System.Drawing.Size(100, 23)
        Me.txt901yr1.TabIndex = 5
        Me.txt901yr1.Text = "0"
        Me.txt901yr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtWithdrwl5
        '
        Me.txtWithdrwl5.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtWithdrwl5.Location = New System.Drawing.Point(619, 92)
        Me.txtWithdrwl5.Name = "txtWithdrwl5"
        Me.txtWithdrwl5.Size = New System.Drawing.Size(100, 23)
        Me.txtWithdrwl5.TabIndex = 5
        Me.txtWithdrwl5.Text = "0"
        Me.txtWithdrwl5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt3090Dys1
        '
        Me.txt3090Dys1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt3090Dys1.Location = New System.Drawing.Point(207, 151)
        Me.txt3090Dys1.Name = "txt3090Dys1"
        Me.txt3090Dys1.Size = New System.Drawing.Size(100, 23)
        Me.txt3090Dys1.TabIndex = 5
        Me.txt3090Dys1.Text = "0"
        Me.txt3090Dys1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt30Dys1
        '
        Me.txt30Dys1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt30Dys1.Location = New System.Drawing.Point(207, 125)
        Me.txt30Dys1.Name = "txt30Dys1"
        Me.txt30Dys1.Size = New System.Drawing.Size(100, 23)
        Me.txt30Dys1.TabIndex = 5
        Me.txt30Dys1.Text = "0"
        Me.txt30Dys1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtWithdrwl2
        '
        Me.txtWithdrwl2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtWithdrwl2.Location = New System.Drawing.Point(310, 92)
        Me.txtWithdrwl2.Name = "txtWithdrwl2"
        Me.txtWithdrwl2.Size = New System.Drawing.Size(100, 23)
        Me.txtWithdrwl2.TabIndex = 5
        Me.txtWithdrwl2.Text = "0"
        Me.txtWithdrwl2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtWithdrwl4
        '
        Me.txtWithdrwl4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtWithdrwl4.Location = New System.Drawing.Point(516, 92)
        Me.txtWithdrwl4.Name = "txtWithdrwl4"
        Me.txtWithdrwl4.Size = New System.Drawing.Size(100, 23)
        Me.txtWithdrwl4.TabIndex = 5
        Me.txtWithdrwl4.Text = "0"
        Me.txtWithdrwl4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtWithdrwl1
        '
        Me.txtWithdrwl1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtWithdrwl1.Location = New System.Drawing.Point(207, 92)
        Me.txtWithdrwl1.Name = "txtWithdrwl1"
        Me.txtWithdrwl1.Size = New System.Drawing.Size(100, 23)
        Me.txtWithdrwl1.TabIndex = 5
        Me.txtWithdrwl1.Text = "0"
        Me.txtWithdrwl1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label23
        '
        Me.Label23.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(608, 28)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(114, 15)
        Me.Label23.TabIndex = 4
        Me.Label23.Text = "Associate Members"
        '
        'Label29
        '
        Me.Label29.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(734, 59)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(80, 15)
        Me.Label29.TabIndex = 4
        Me.Label29.Text = "Total Amount"
        Me.Label29.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label26
        '
        Me.Label26.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(411, 59)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(80, 15)
        Me.Label26.TabIndex = 4
        Me.Label26.Text = "Total Amount"
        Me.Label26.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label28
        '
        Me.Label28.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(623, 59)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(92, 15)
        Me.Label28.TabIndex = 4
        Me.Label28.Text = "No. of Accounts"
        Me.Label28.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label25
        '
        Me.Label25.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(313, 59)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(92, 15)
        Me.Label25.TabIndex = 4
        Me.Label25.Text = "No. of Accounts"
        Me.Label25.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label27
        '
        Me.Label27.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(516, 44)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(93, 45)
        Me.Label27.TabIndex = 4
        Me.Label27.Text = "No. of Members" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "with Deposit" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Account"
        Me.Label27.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label24
        '
        Me.Label24.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(214, 44)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(93, 45)
        Me.Label24.TabIndex = 4
        Me.Label24.Text = "No. of Members" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "with Deposit" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Account"
        Me.Label24.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label22
        '
        Me.Label22.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(313, 28)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(103, 15)
        Me.Label22.TabIndex = 4
        Me.Label22.Text = "Regular Members"
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Location = New System.Drawing.Point(17, 252)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(186, 30)
        Me.Label37.TabIndex = 4
        Me.Label37.Text = "None-withdrawable Savings" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "(For Share Capital  Subscription)"
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Location = New System.Drawing.Point(26, 232)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(107, 15)
        Me.Label36.TabIndex = 4
        Me.Label36.Text = "More than 5 years"
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Location = New System.Drawing.Point(26, 206)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(158, 15)
        Me.Label35.TabIndex = 4
        Me.Label35.Text = "More than 1 year to 5 years"
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Location = New System.Drawing.Point(26, 180)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(161, 15)
        Me.Label34.TabIndex = 4
        Me.Label34.Text = "More than 90 days to 1 year"
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Location = New System.Drawing.Point(26, 154)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(170, 15)
        Me.Label33.TabIndex = 4
        Me.Label33.Text = "More than 30 days to 90 days"
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Location = New System.Drawing.Point(26, 128)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(50, 15)
        Me.Label32.TabIndex = 4
        Me.Label32.Text = "30 days"
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(11, 113)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(84, 15)
        Me.Label31.TabIndex = 4
        Me.Label31.Text = "Time Deposits"
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(11, 95)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(182, 15)
        Me.Label30.TabIndex = 4
        Me.Label30.Text = "Withdrawable Savings Deposits"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(35, 44)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(97, 15)
        Me.Label21.TabIndex = 4
        Me.Label21.Text = "Type of Deposits"
        '
        'Label20
        '
        Me.Label20.BackColor = System.Drawing.Color.Gray
        Me.Label20.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label20.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.Color.White
        Me.Label20.Location = New System.Drawing.Point(3, 3)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(824, 20)
        Me.Label20.TabIndex = 3
        Me.Label20.Text = "Information on Saving Deposits"
        Me.Label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TabPage5
        '
        Me.TabPage5.AutoScroll = True
        Me.TabPage5.BackColor = System.Drawing.Color.White
        Me.TabPage5.Controls.Add(Me.SplitContainer1)
        Me.TabPage5.Controls.Add(Me.StatusStrip3)
        Me.TabPage5.Controls.Add(Me.Label41)
        Me.TabPage5.Controls.Add(Me.Label40)
        Me.TabPage5.Controls.Add(Me.Label39)
        Me.TabPage5.Controls.Add(Me.Label38)
        Me.TabPage5.Location = New System.Drawing.Point(4, 24)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage5.Size = New System.Drawing.Size(830, 473)
        Me.TabPage5.TabIndex = 4
        Me.TabPage5.Text = "E"
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SplitContainer1.Location = New System.Drawing.Point(9, 78)
        Me.SplitContainer1.Name = "SplitContainer1"
        Me.SplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.grdByLoanType)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label42)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.GrdByMaturity)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label43)
        Me.SplitContainer1.Size = New System.Drawing.Size(815, 366)
        Me.SplitContainer1.SplitterDistance = 180
        Me.SplitContainer1.TabIndex = 10
        '
        'grdByLoanType
        '
        Me.grdByLoanType.AllowUserToAddRows = False
        Me.grdByLoanType.AllowUserToDeleteRows = False
        Me.grdByLoanType.AllowUserToResizeRows = False
        Me.grdByLoanType.BackgroundColor = System.Drawing.Color.White
        Me.grdByLoanType.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdByLoanType.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ColParticular, Me.ColNoOfLoan, Me.ColLoanAmount})
        Me.grdByLoanType.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grdByLoanType.GridColor = System.Drawing.Color.Black
        Me.grdByLoanType.Location = New System.Drawing.Point(0, 20)
        Me.grdByLoanType.Name = "grdByLoanType"
        Me.grdByLoanType.ReadOnly = True
        Me.grdByLoanType.Size = New System.Drawing.Size(815, 160)
        Me.grdByLoanType.TabIndex = 0
        '
        'ColParticular
        '
        Me.ColParticular.HeaderText = "Particular"
        Me.ColParticular.Name = "ColParticular"
        Me.ColParticular.ReadOnly = True
        Me.ColParticular.Width = 350
        '
        'ColNoOfLoan
        '
        DataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle27.Format = "N0"
        DataGridViewCellStyle27.NullValue = "0"
        Me.ColNoOfLoan.DefaultCellStyle = DataGridViewCellStyle27
        Me.ColNoOfLoan.HeaderText = "No of Loans"
        Me.ColNoOfLoan.Name = "ColNoOfLoan"
        Me.ColNoOfLoan.ReadOnly = True
        Me.ColNoOfLoan.Width = 200
        '
        'ColLoanAmount
        '
        DataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle28.Format = "N2"
        DataGridViewCellStyle28.NullValue = "0.00"
        Me.ColLoanAmount.DefaultCellStyle = DataGridViewCellStyle28
        Me.ColLoanAmount.HeaderText = "Amount"
        Me.ColLoanAmount.Name = "ColLoanAmount"
        Me.ColLoanAmount.ReadOnly = True
        Me.ColLoanAmount.Width = 200
        '
        'Label42
        '
        Me.Label42.BackColor = System.Drawing.Color.Silver
        Me.Label42.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label42.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label42.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label42.Location = New System.Drawing.Point(0, 0)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(815, 20)
        Me.Label42.TabIndex = 1
        Me.Label42.Text = "By Type of Loan"
        Me.Label42.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'GrdByMaturity
        '
        Me.GrdByMaturity.AllowUserToAddRows = False
        Me.GrdByMaturity.AllowUserToDeleteRows = False
        Me.GrdByMaturity.AllowUserToResizeRows = False
        Me.GrdByMaturity.BackgroundColor = System.Drawing.Color.White
        Me.GrdByMaturity.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.GrdByMaturity.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3})
        Me.GrdByMaturity.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GrdByMaturity.GridColor = System.Drawing.Color.Black
        Me.GrdByMaturity.Location = New System.Drawing.Point(0, 20)
        Me.GrdByMaturity.Name = "GrdByMaturity"
        Me.GrdByMaturity.ReadOnly = True
        Me.GrdByMaturity.Size = New System.Drawing.Size(815, 162)
        Me.GrdByMaturity.TabIndex = 3
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Particular"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Width = 350
        '
        'DataGridViewTextBoxColumn2
        '
        DataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle29.Format = "N0"
        DataGridViewCellStyle29.NullValue = "0"
        Me.DataGridViewTextBoxColumn2.DefaultCellStyle = DataGridViewCellStyle29
        Me.DataGridViewTextBoxColumn2.HeaderText = "No of Loans"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Width = 200
        '
        'DataGridViewTextBoxColumn3
        '
        DataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle30.Format = "N2"
        DataGridViewCellStyle30.NullValue = "0.00"
        Me.DataGridViewTextBoxColumn3.DefaultCellStyle = DataGridViewCellStyle30
        Me.DataGridViewTextBoxColumn3.HeaderText = "Amount"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Width = 200
        '
        'Label43
        '
        Me.Label43.BackColor = System.Drawing.Color.Silver
        Me.Label43.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label43.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label43.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label43.Location = New System.Drawing.Point(0, 0)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(815, 20)
        Me.Label43.TabIndex = 2
        Me.Label43.Text = "By Maturity"
        Me.Label43.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'StatusStrip3
        '
        Me.StatusStrip3.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel2, Me.tslblTotalLoanReceivable})
        Me.StatusStrip3.Location = New System.Drawing.Point(3, 447)
        Me.StatusStrip3.Name = "StatusStrip3"
        Me.StatusStrip3.Size = New System.Drawing.Size(824, 23)
        Me.StatusStrip3.TabIndex = 9
        Me.StatusStrip3.Text = "StatusStrip3"
        '
        'ToolStripStatusLabel2
        '
        Me.ToolStripStatusLabel2.BackColor = System.Drawing.Color.Transparent
        Me.ToolStripStatusLabel2.Name = "ToolStripStatusLabel2"
        Me.ToolStripStatusLabel2.Size = New System.Drawing.Size(75, 18)
        Me.ToolStripStatusLabel2.Text = "Total Amount:"
        '
        'tslblTotalLoanReceivable
        '
        Me.tslblTotalLoanReceivable.BackColor = System.Drawing.Color.Transparent
        Me.tslblTotalLoanReceivable.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tslblTotalLoanReceivable.Name = "tslblTotalLoanReceivable"
        Me.tslblTotalLoanReceivable.Size = New System.Drawing.Size(33, 18)
        Me.tslblTotalLoanReceivable.Text = "0.00"
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Location = New System.Drawing.Point(6, 60)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(194, 15)
        Me.Label41.TabIndex = 5
        Me.Label41.Text = "3. Information on Loan Receivable"
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Location = New System.Drawing.Point(6, 45)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(418, 15)
        Me.Label40.TabIndex = 5
        Me.Label40.Text = "2. Detailed Information on Operations - Please fill up the attached Form 1B"
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Location = New System.Drawing.Point(6, 30)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(465, 15)
        Me.Label39.TabIndex = 5
        Me.Label39.Text = "1. Detailed Information on Financial Condition - Please fill up the attached Form" & _
            " 1A"
        '
        'Label38
        '
        Me.Label38.BackColor = System.Drawing.Color.Gray
        Me.Label38.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label38.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label38.ForeColor = System.Drawing.Color.White
        Me.Label38.Location = New System.Drawing.Point(3, 3)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(824, 20)
        Me.Label38.TabIndex = 4
        Me.Label38.Text = "Other Financial Information for Cooperatives with Savings and Credit Services"
        Me.Label38.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnPrevious
        '
        Me.btnPrevious.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPrevious.Location = New System.Drawing.Point(678, 506)
        Me.btnPrevious.Name = "btnPrevious"
        Me.btnPrevious.Size = New System.Drawing.Size(78, 29)
        Me.btnPrevious.TabIndex = 1
        Me.btnPrevious.Text = "Previous"
        Me.btnPrevious.UseVisualStyleBackColor = True
        '
        'btnNext
        '
        Me.btnNext.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNext.Location = New System.Drawing.Point(758, 506)
        Me.btnNext.Name = "btnNext"
        Me.btnNext.Size = New System.Drawing.Size(78, 29)
        Me.btnNext.TabIndex = 1
        Me.btnNext.Text = "Next"
        Me.btnNext.UseVisualStyleBackColor = True
        '
        'BgLoanReceivable
        '
        '
        'btnCancel
        '
        Me.btnCancel.Image = Global.WindowsApplication2.My.Resources.Resources.button_cancel
        Me.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnCancel.Location = New System.Drawing.Point(6, 506)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(84, 27)
        Me.btnCancel.TabIndex = 21
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'frmCAPRSecIII
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(842, 536)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnNext)
        Me.Controls.Add(Me.btnPrevious)
        Me.Controls.Add(Me.TabControl1)
        Me.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow
        Me.Name = "frmCAPRSecIII"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cooperative Annual Performance Report - [Section III]"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.grdInvestment, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        Me.StatusStrip2.ResumeLayout(False)
        Me.StatusStrip2.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.GrdExternalBorrwing, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage4.ResumeLayout(False)
        Me.TabPage4.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        CType(Me.grdSavingDeposits, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage5.ResumeLayout(False)
        Me.TabPage5.PerformLayout()
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.ResumeLayout(False)
        CType(Me.grdByLoanType, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GrdByMaturity, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip3.ResumeLayout(False)
        Me.StatusStrip3.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents btnPrevious As System.Windows.Forms.Button
    Friend WithEvents btnNext As System.Windows.Forms.Button
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtGovernTotal As System.Windows.Forms.TextBox
    Friend WithEvents txtGovern3 As System.Windows.Forms.TextBox
    Friend WithEvents txtGovern2 As System.Windows.Forms.TextBox
    Friend WithEvents txtGovern1 As System.Windows.Forms.TextBox
    Friend WithEvents txtInvstTotal As System.Windows.Forms.TextBox
    Friend WithEvents txtInvst3 As System.Windows.Forms.TextBox
    Friend WithEvents txtInvst2 As System.Windows.Forms.TextBox
    Friend WithEvents txtInvst1 As System.Windows.Forms.TextBox
    Friend WithEvents txtLongTermTotal As System.Windows.Forms.TextBox
    Friend WithEvents txtLongTerm3 As System.Windows.Forms.TextBox
    Friend WithEvents txtLongTerm2 As System.Windows.Forms.TextBox
    Friend WithEvents txtLongTerm1 As System.Windows.Forms.TextBox
    Friend WithEvents grdInvestment As System.Windows.Forms.DataGridView
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents lblTotalInvestment As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtAmntForgnCur As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents StatusStrip2 As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStripStatusLabel3 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents tsLblTotalExtBorwng As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents txtPrivFinInst As System.Windows.Forms.TextBox
    Friend WithEvents txtGovernFinInst As System.Windows.Forms.TextBox
    Friend WithEvents txtAmntLocCur As System.Windows.Forms.TextBox
    Friend WithEvents txtConverRate As System.Windows.Forms.TextBox
    Friend WithEvents GrdExternalBorrwing As System.Windows.Forms.DataGridView
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents txt5yr6 As System.Windows.Forms.TextBox
    Friend WithEvents txt15yr6 As System.Windows.Forms.TextBox
    Friend WithEvents txt901yr6 As System.Windows.Forms.TextBox
    Friend WithEvents txt3090Dys6 As System.Windows.Forms.TextBox
    Friend WithEvents txt30Dys6 As System.Windows.Forms.TextBox
    Friend WithEvents txtWithdrwl6 As System.Windows.Forms.TextBox
    Friend WithEvents txt5yr3 As System.Windows.Forms.TextBox
    Friend WithEvents txt15yr3 As System.Windows.Forms.TextBox
    Friend WithEvents txt901yr3 As System.Windows.Forms.TextBox
    Friend WithEvents txt3090Dys3 As System.Windows.Forms.TextBox
    Friend WithEvents txt30Dys3 As System.Windows.Forms.TextBox
    Friend WithEvents txtWithdrwl3 As System.Windows.Forms.TextBox
    Friend WithEvents txt5yr5 As System.Windows.Forms.TextBox
    Friend WithEvents txt5yr2 As System.Windows.Forms.TextBox
    Friend WithEvents txt15yr5 As System.Windows.Forms.TextBox
    Friend WithEvents txt15yr2 As System.Windows.Forms.TextBox
    Friend WithEvents txt901yr5 As System.Windows.Forms.TextBox
    Friend WithEvents txt901yr2 As System.Windows.Forms.TextBox
    Friend WithEvents txt3090Dys5 As System.Windows.Forms.TextBox
    Friend WithEvents txt3090Dys2 As System.Windows.Forms.TextBox
    Friend WithEvents txt5yr4 As System.Windows.Forms.TextBox
    Friend WithEvents txt30Dys5 As System.Windows.Forms.TextBox
    Friend WithEvents txt15yr4 As System.Windows.Forms.TextBox
    Friend WithEvents txt30Dys2 As System.Windows.Forms.TextBox
    Friend WithEvents txt901yr4 As System.Windows.Forms.TextBox
    Friend WithEvents txt3090Dys4 As System.Windows.Forms.TextBox
    Friend WithEvents txt5yr1 As System.Windows.Forms.TextBox
    Friend WithEvents txt15yr1 As System.Windows.Forms.TextBox
    Friend WithEvents txt30Dys4 As System.Windows.Forms.TextBox
    Friend WithEvents txt901yr1 As System.Windows.Forms.TextBox
    Friend WithEvents txtWithdrwl5 As System.Windows.Forms.TextBox
    Friend WithEvents txt3090Dys1 As System.Windows.Forms.TextBox
    Friend WithEvents txt30Dys1 As System.Windows.Forms.TextBox
    Friend WithEvents txtWithdrwl2 As System.Windows.Forms.TextBox
    Friend WithEvents txtWithdrwl4 As System.Windows.Forms.TextBox
    Friend WithEvents txtWithdrwl1 As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents txtNonWthdrwl6 As System.Windows.Forms.TextBox
    Friend WithEvents txtNonWthdrwl3 As System.Windows.Forms.TextBox
    Friend WithEvents txtNonWthdrwl5 As System.Windows.Forms.TextBox
    Friend WithEvents txtNonWthdrwl2 As System.Windows.Forms.TextBox
    Friend WithEvents txtNonWthdrwl4 As System.Windows.Forms.TextBox
    Friend WithEvents txtNonWthdrwl1 As System.Windows.Forms.TextBox
    Friend WithEvents grdSavingDeposits As System.Windows.Forms.DataGridView
    Friend WithEvents ColType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Col1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Col2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Col3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColTotal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColSource As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColTypes As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColRegNoMembers As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColregNoAccounts As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColRegTotal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColAssNoMembers As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColAssNoAccount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColAssTotal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TabPage5 As System.Windows.Forms.TabPage
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents StatusStrip3 As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStripStatusLabel2 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents tslblTotalLoanReceivable As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents grdByLoanType As System.Windows.Forms.DataGridView
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents BgLoanReceivable As System.ComponentModel.BackgroundWorker
    Friend WithEvents GrdByMaturity As System.Windows.Forms.DataGridView
    Friend WithEvents ColParticular As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColNoOfLoan As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColLoanAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnCancel As System.Windows.Forms.Button
End Class
