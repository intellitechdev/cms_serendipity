<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmOfficialReceipt
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmOfficialReceipt))
        Me.crvOfficialReceipt = New CrystalDecisions.Windows.Forms.CrystalReportViewer
        Me.loadingPic = New System.Windows.Forms.PictureBox
        Me.bgwLoadReports = New System.ComponentModel.BackgroundWorker
        CType(Me.loadingPic, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'crvOfficialReceipt
        '
        Me.crvOfficialReceipt.ActiveViewIndex = -1
        Me.crvOfficialReceipt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.crvOfficialReceipt.DisplayGroupTree = False
        Me.crvOfficialReceipt.Dock = System.Windows.Forms.DockStyle.Fill
        Me.crvOfficialReceipt.Location = New System.Drawing.Point(0, 0)
        Me.crvOfficialReceipt.Name = "crvOfficialReceipt"
        Me.crvOfficialReceipt.SelectionFormula = ""
        Me.crvOfficialReceipt.ShowGroupTreeButton = False
        Me.crvOfficialReceipt.ShowRefreshButton = False
        Me.crvOfficialReceipt.Size = New System.Drawing.Size(419, 400)
        Me.crvOfficialReceipt.TabIndex = 0
        Me.crvOfficialReceipt.ViewTimeSelectionFormula = ""
        '
        'loadingPic
        '
        Me.loadingPic.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.loadingPic.Image = Global.WindowsApplication2.My.Resources.Resources.LoadingData
        Me.loadingPic.Location = New System.Drawing.Point(158, 157)
        Me.loadingPic.Name = "loadingPic"
        Me.loadingPic.Size = New System.Drawing.Size(107, 74)
        Me.loadingPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.loadingPic.TabIndex = 1
        Me.loadingPic.TabStop = False
        Me.loadingPic.Visible = False
        '
        'bgwLoadReports
        '
        '
        'frmOfficialReceipt
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(419, 400)
        Me.Controls.Add(Me.loadingPic)
        Me.Controls.Add(Me.crvOfficialReceipt)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmOfficialReceipt"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Official Receipt"
        Me.TopMost = True
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.loadingPic, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents crvOfficialReceipt As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents loadingPic As System.Windows.Forms.PictureBox
    Friend WithEvents bgwLoadReports As System.ComponentModel.BackgroundWorker
End Class
