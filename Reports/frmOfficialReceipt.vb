Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class frmOfficialReceipt

    Private gCon As New Clsappconfiguration
    Private rptsummary As New ReportDocument

#Region "Properties"

    Private user As String
    Public Property GetUser() As String
        Get
            Return user
        End Get
        Set(ByVal value As String)
            user = value
        End Set
    End Property

    Private employeeID As String
    Public Property GetEmployeePkID() As String
        Get
            Return employeeID
        End Get
        Set(ByVal value As String)
            employeeID = value
        End Set
    End Property

    Private orNo As String
    Public Property GetORNo() As String
        Get
            Return orNo
        End Get
        Set(ByVal value As String)
            orNo = value
        End Set
    End Property

    Private paymentInWords As String
    Public Property getPaymentInWords() As String
        Get
            Return paymentInWords
        End Get
        Set(ByVal value As String)
            paymentInWords = value
        End Set
    End Property

#End Region

    Private Sub frmOfficialReceipt_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        rptsummary.Close()
    End Sub

    Private Sub frmOfficialReceipt_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        loadingPic.Visible = True
        bgwLoadReports.RunWorkerAsync()
    End Sub

    Private Sub LoadReport()
        Try
            rptsummary.Load(Application.StartupPath & "\LoanReport\Official_Receipt.rpt")
            Logon(rptsummary, gCon.Server, gCon.Database, gCon.Username, gCon.Password)
            rptsummary.Refresh()
            rptsummary.SetParameterValue("@fullName", GetUser())
            rptsummary.SetParameterValue("@fkEmployee", GetEmployeePkID())
            rptsummary.SetParameterValue("@OrNo", GetORNo())
            rptsummary.SetParameterValue("@paymentAmountInWords", getPaymentInWords())
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub bgwLoadReports_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgwLoadReports.DoWork
        LoadReport()
    End Sub

    Private Sub bgwLoadReports_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwLoadReports.RunWorkerCompleted
        crvOfficialReceipt.Visible = True
        crvOfficialReceipt.ReportSource = rptsummary

        loadingPic.Visible = False
    End Sub

    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If
        Dim obj As ReportObject
        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function

    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            If (tbl.TestConnectivity()) Then
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next
        Return True
    End Function

End Class