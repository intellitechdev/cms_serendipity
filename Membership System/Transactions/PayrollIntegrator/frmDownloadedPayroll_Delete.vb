Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient


Public Class frmDownloadedPayroll_Delete
    Dim gcon As New Clsappconfiguration

    Private Sub LoadPayrollCode()
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.StoredProcedure, "CIMS_Member_LoadPayCode")
                While rd.Read
                    cboPaycode_fk.Items.Add(rd.Item(0))
                    cboPaycode.Items.Add(rd.Item(1))
                End While
            End Using
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Payroll Code")
        End Try
    End Sub

    Private Sub LoadSelectedRows()
        If dgvDownloadedPayroll.RowCount <> 0 Then
            cboPaycode.Text = dgvDownloadedPayroll.SelectedCells.Item(0).Value.ToString()
            dtePaydate.Value = dgvDownloadedPayroll.SelectedCells.Item(1).Value.ToString()
        End If
    End Sub

    Private Sub LoadDownloadedPayrollGrid()
        Try
            Dim ds As DataSet = SqlHelper.ExecuteDataset(gcon.cnstring, CommandType.StoredProcedure, "CIMS_Download_ListPayroll_Load")
            dgvDownloadedPayroll.DataSource = ds.Tables(0)
            With dgvDownloadedPayroll
                .Columns.Item(0).HeaderText = "Paycode"
                .Columns.Item(1).HeaderText = "Payment Date"
            End With
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Payroll Code")
        End Try

        
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub frmDownloadedPayroll_Delete_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Me.Dispose()
    End Sub

    Private Sub frmDownloadedPayroll_Delete_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LoadDownloadedPayrollGrid()
        LoadPayrollCode()
    End Sub

    Private Sub dgvDownloadedPayroll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvDownloadedPayroll.Click
        LoadSelectedRows()
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If cboPaycode.Text = "" Then
            MessageBox.Show("Please click and select a payroll first.", "Delete Downloaded Payroll File.", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Else
            If MessageBox.Show("All payroll data related to the selected will be deleted. Proceed?", "Delete Dowloaded Payroll File", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) = Windows.Forms.DialogResult.OK Then
                Try
                    SqlHelper.ExecuteNonQuery(gcon.sqlconn, CommandType.StoredProcedure, "CIMS_Download_PayrollFile_Delete", _
                        New SqlParameter("@payCode", cboPaycode_fk.Text), _
                        New SqlParameter("@payDate", dtePaydate.Value))

                    MessageBox.Show("Payroll Delete Successful!")
                    LoadDownloadedPayrollGrid()
                Catch ex As Exception
                    MessageBox.Show(ex.Message, "Delete Downloaded Payroll File", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                End Try
            End If
        End If
    End Sub

    Private Sub cboPaycode_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPaycode.SelectedIndexChanged
        cboPaycode_fk.SelectedIndex = cboPaycode.SelectedIndex
    End Sub
End Class