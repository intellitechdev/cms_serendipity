Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmMasterfile_LoanTerms
#Region "Get Loan Type"
    Private Sub GetloanType()
        Dim gcon As New Clsappconfiguration
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.StoredProcedure, "CIMS_getloanType_byProj", _
                                     New SqlParameter("@co_name", txtProject.Text))
        Me.cboLoantype.Items.Clear()
        While rd.Read
            Me.cboLoantype.Items.Add(rd.Item(0))
        End While
        rd.Close()
        gcon.sqlconn.Close()
    End Sub
#End Region
#Region "Get loan Type uniqueidentifier"
    Private Sub GetloantypeUnique(ByVal loanid As String)
        Dim mycon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "CIMS_getloanTypekey", _
                                     New SqlParameter("@fcLoanTypeName", loanid))
        Try
            While rd.Read
                getLoanID() = rd.Item(0)
            End While
            rd.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "GetloantypeUnique")
        End Try
    End Sub
#End Region
#Region "Get company uniqueidentifier"
    Private Sub GetCompanyUnique(ByVal coid As String)
        Dim mycon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "CIMS_getCompanykey", _
                                     New SqlParameter("@co_name", coid))
        Try
            While rd.Read
                getco_id() = rd.Item(0)
            End While
            rd.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "GetCompanyUnique")
        End Try
    End Sub
#End Region
#Region "Function"
    Public Function Numbers(ByVal C As Char) As Boolean
        If Not (Char.IsDigit(C)) And Not (C = ".") And Not (Microsoft.VisualBasic.AscW(C) = 8) And Not (Microsoft.VisualBasic.AscW(C) = 13) Then
            MsgBox("Invalid Input! This field allows 0-9 only.", MsgBoxStyle.Exclamation, "Invalid")
            Return False
        Else
            Return True
        End If
    End Function
#End Region
#Region "Variables"
    Private LoanID As String
    Private pkterms As String
    Private co_id As String
#End Region
#Region "Property"
    Private Property getLoanID() As String
        Get
            Return LoanID
        End Get
        Set(ByVal value As String)
            LoanID = value
        End Set
    End Property
    Private Property getpkterms() As String
        Get
            Return pkterms
        End Get
        Set(ByVal value As String)
            pkterms = value
        End Set
    End Property
    Private Property getco_id() As String
        Get
            Return co_id
        End Get
        Set(ByVal value As String)
            co_id = value
        End Set
    End Property
#End Region
#Region "Insert Update loan Terms"
    Private Sub InsertUpdateLoanTerms(ByVal loanid As String, ByVal terms As Decimal, ByVal pkterms As String)
        Dim mycon As New Clsappconfiguration
        mycon.sqlconn.Open()
        Dim trans As SqlTransaction = mycon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Masterfiles_Terms_AddEdit", _
                                      New SqlParameter("@fk_LoanType", loanid), _
                                      New SqlParameter("@fnTermInMonths", terms), _
                                      New SqlParameter("@pk_Terms", pkterms), _
                                      New SqlParameter("@co_name", txtProject.Text))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "InsertUpdateLoanTerms")
        Finally
            mycon.sqlconn.Close()
        End Try
    End Sub
#End Region
#Region "Delete loan Terms"
    Private Sub DeleteloanTerms(ByVal pkterms As String)
        Dim mycon As New Clsappconfiguration
        mycon.sqlconn.Open()
        Dim trans As SqlTransaction = mycon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Masterfiles_Terms_Delete", _
                                      New SqlParameter("@pk_Terms", pkterms))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "DeleteloanTerms")
        Finally
            mycon.sqlconn.Close()
        End Try
    End Sub
#End Region
#Region "View Loan Terms based on loan type"
    Private Sub ViewLoanTerms(ByVal loanid As String)
        Dim mycon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "CIMS_m_LoanType_Terms_Select", _
                                   New SqlParameter("@fk_LoanType", loanid), _
                                   New SqlParameter("@co_name", txtProject.Text))
        With Me.listLoanTerms
            .Clear()
            .View = View.Details
            .GridLines = True
            .FullRowSelect = True
            .Columns.Add("Terms in Months", 300, HorizontalAlignment.Left)
            Try
                While rd.Read
                    With .Items.Add(rd.Item(0))
                        .SubItems.Add(rd.Item(1))
                    End With
                End While
                rd.Close()
            Catch ex As Exception
                MessageBox.Show(ex.Message, "ViewLoanTerms")
            End Try
        End With
    End Sub
#End Region
    Private Sub btnsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsave.Click
        Try
            If btnsave.Text = "New" Then

                Label1.Text = "Add Loan Terms"
                btndelete.Visible = False
                btnupdate.Visible = False
                btnsave.Text = "Save"
                btnsave.Image = My.Resources.save2
                btnclose.Text = "Cancel"
                btnclose.Location = New System.Drawing.Point(74, 6)

            ElseIf btnsave.Text = "Save" Then
                If Me.cboLoantype.Text <> "" And Me.txtTerms.Text <> "" Then
                    Label1.Text = "Loan Terms"
                    Call InsertUpdateLoanTerms(LoanID, Me.txtTerms.Text, "")
                    Call ViewLoanTerms(LoanID)
                    Me.txtTerms.Text = "0.0"
                    btnsave.Text = "New"
                    btnsave.Image = My.Resources.new3
                    btnclose.Text = "Close"
                    btnclose.Location = New System.Drawing.Point(218, 6)
                    btndelete.Visible = True
                    btnupdate.Visible = True
                Else
                    MessageBox.Show("Empty field found! ", "Save", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub btnupdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupdate.Click
        Try
            If btnupdate.Text = "Edit" Then
                Label1.Text = "Edit Loan Terms"
                Me.txtTerms.Text = Me.listLoanTerms.SelectedItems(0).Text
                getpkterms() = Me.listLoanTerms.SelectedItems(0).SubItems(1).Text
                Me.btnsave.Enabled = False
                btnupdate.Text = "Update"
                btnupdate.Image = My.Resources.save2
                btnclose.Text = "Cancel"
                btnclose.Location = New System.Drawing.Point(141, 6)
                btndelete.Visible = False

            ElseIf btnupdate.Text = "Update" Then
                If Me.cboLoantype.Text <> "" And Me.txtTerms.Text <> "" Then
                    Label1.Text = "Loan Terms"
                    Call InsertUpdateLoanTerms(LoanID, Me.txtTerms.Text, pkterms)
                    Call ViewLoanTerms(LoanID)
                    Me.txtTerms.Text = "0.0"
                    btndelete.Visible = True
                    btnupdate.Text = "Edit"
                    btnupdate.Image = My.Resources.edit1
                    btnclose.Text = "Cancel"
                    btnclose.Location = New System.Drawing.Point(218, 6)
                    btnclose.Text = "Close"
                    Me.btnsave.Enabled = True
                    Me.btndelete.Visible = True

                Else
                    MessageBox.Show("Empty field found! ", "Edit", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            End If

        Catch ex As Exception
        End Try
    End Sub

    Private Sub btndelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndelete.Click
        Try
            getpkterms() = Me.listLoanTerms.SelectedItems(0).SubItems(1).Text
            Dim x As New DialogResult
            x = MessageBox.Show("Are you sure you want to permanently delete this record?", "Confirm Deletion", MessageBoxButtons.OKCancel, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
            If x = System.Windows.Forms.DialogResult.OK Then
                Call DeleteloanTerms(pkterms)
                Call ViewLoanTerms(LoanID)
                Me.txtTerms.Text = "0.0"
            ElseIf x = System.Windows.Forms.DialogResult.Cancel Then
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub frmLoanTerms_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.txtProject.Text = frmMasterfile_LoanType.cboProject.Text
        If Me.txtProject.Text = "" Then
            Call GetloanType()
        Else
            Me.txtProject.Text = frmMasterfile_LoanType.cboProject.Text
            Me.cboLoantype.Text = frmMasterfile_LoanType.txtName.Text
            Call GetloantypeUnique(Me.cboLoantype.Text)
            Call ViewLoanTerms(LoanID)
            Call GetloanType()
        End If

    End Sub

    Private Sub txtTerms_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTerms.Click
        txtTerms.Clear()
    End Sub

    Private Sub txtTerms_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtTerms.KeyDown
        If e.KeyCode = Keys.Enter Then
            If Me.cboLoantype.Text <> "" And Me.txtTerms.Text <> "" Then
                Label1.Text = "Loan Terms"
                Call InsertUpdateLoanTerms(LoanID, Me.txtTerms.Text, "")
                Call ViewLoanTerms(LoanID)
                Me.txtTerms.Text = "0.0"
                btnsave.Text = "New"
                btnsave.Image = My.Resources.new3
                btnclose.Text = "Close"
                btnclose.Location = New System.Drawing.Point(218, 6)
                btndelete.Visible = True
                btnupdate.Visible = True
            Else
                MessageBox.Show("Empty field found! ", "Save", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
        End If
    End Sub

    Private Sub txtTerms_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtTerms.KeyPress
        e.Handled = Not Numbers(e.KeyChar)
    End Sub

    Private Sub txtTerms_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTerms.TextChanged

    End Sub

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        If btnclose.Text = "Cancel" Then
            Label1.Text = "Loan Terms"
            btnsave.Text = "New"
            btnsave.Image = My.Resources.new3
            btnupdate.Text = "Edit"
            btnupdate.Image = My.Resources.edit1
            btnclose.Text = "Close"
            btnclose.Location = New System.Drawing.Point(218, 6)
            btndelete.Visible = True
            btnupdate.Visible = True
            btnsave.Enabled = True
        ElseIf btnclose.Text = "Close" Then
            Me.Close()
        End If
    End Sub
    Private Sub cboLoantype_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLoantype.TextChanged
        'Call GetCompanyUnique(co_id)
        Call GetloantypeUnique(Me.cboLoantype.Text)
        Call ViewLoanTerms(LoanID)
    End Sub
End Class