Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Threading
Module ClientsReports
    '===================================
    ' APPLICANT REPORTS
    ' crystal report login with sub report
    ' author : jolan p. mahinay
    ' create : 03/02/07
    '===================================

    Public Sub load_without_parameter_all_applicant()
        Dim apprdr As New System.Configuration.AppSettingsReader
        Dim s, d, p As New Clsappconfiguration

        Dim report As New ReportDocument()
        Dim connection As IConnectionInfo
        Dim serverName1 As String = s.Server
        Dim userID As String = d.Username
        Dim password As String = p.Password

        report.Load(Application.StartupPath & "\PICC-reports\print_all_applicant.rpt")
        report.Refresh()


        ' Set Database Logon to main report
        For Each connection In report.DataSourceConnections
            Select Case connection.ServerName
                Case serverName1
                    connection.SetLogon(userID, password)
            End Select
        Next

        ' Set Database Logon to subreport
        Dim subreport As ReportDocument
        For Each subreport In report.Subreports
            For Each connection In subreport.DataSourceConnections
                Select Case connection.ServerName
                    Case serverName1
                        connection.SetLogon(userID, password)
                End Select
            Next
        Next
        CooperativeReports.appreports.ReportSource = report
        CooperativeReports.MdiParent = Form1
        CooperativeReports.Show()
    End Sub
    Public Sub load_all_hired_applicant()
        Dim apprdr As New System.Configuration.AppSettingsReader
        Dim s, d, p As New Clsappconfiguration

        Dim report As New ReportDocument()
        Dim connection As IConnectionInfo
        Dim serverName1 As String = s.Server  ' "Server1"
        Dim userID As String = d.Username ' "MyUserID"
        Dim password As String = p.Password ' "MyPassword"

        report.Load(Application.StartupPath & "\PICC-reports\print_all_hired_applicant.rpt")
        report.Refresh()


        ' Set Database Logon to main report
        For Each connection In report.DataSourceConnections
            Select Case connection.ServerName
                Case serverName1
                    connection.SetLogon(userID, password)
            End Select
        Next

        ' Set Database Logon to subreport
        Dim subreport As ReportDocument
        For Each subreport In report.Subreports
            For Each connection In subreport.DataSourceConnections
                Select Case connection.ServerName
                    Case serverName1
                        connection.SetLogon(userID, password)
                End Select
            Next
        Next
        CooperativeReports.appreports.ReportSource = report
        CooperativeReports.MdiParent = Form1
        CooperativeReports.Show()
    End Sub
    Public Sub applicant_according_to_datehired()
        Dim apprdr As New System.Configuration.AppSettingsReader
        Dim s, d, p As New Clsappconfiguration
        Try
            Dim report As New ReportDocument()
            Dim connection As IConnectionInfo
            Dim serverName1 As String = s.Server  ' "Server1"
            Dim userID As String = d.Username ' "MyUserID"
            Dim password As String = p.Password ' "MyPassword"

            report.Load(Application.StartupPath & "\PICC-reports\Applicant_According_to_Datehired_Revised.rpt")
            report.Refresh()
            report.SetParameterValue("@datefrom", Applicant_Selection.datehiredfrom.Text) 'datehiredfrom.TextBox_Text)
            report.SetParameterValue("@dateto", Applicant_Selection.datehireto.Text) 'datehiredto.TextBox_Text)

            ' Set Database Logon to main report
            For Each connection In report.DataSourceConnections
                Select Case connection.ServerName
                    Case serverName1
                        connection.SetLogon(userID, password)
                End Select
            Next

            ' Set Database Logon to subreport
            Dim subreport As ReportDocument
            For Each subreport In report.Subreports
                For Each connection In subreport.DataSourceConnections
                    Select Case connection.ServerName
                        Case serverName1
                            connection.SetLogon(userID, password)
                    End Select
                Next
            Next
            CooperativeReports.appreports.ReportSource = report
            CooperativeReports.MdiParent = Form1
            CooperativeReports.Show()
        Catch ex As Exception
            MessageBox.Show("Supply the other Parameter", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub
    Public Sub applicant_according_to_age()
        '
        Dim apprdr As New System.Configuration.AppSettingsReader
        Dim s, d, p As New Clsappconfiguration
        Try
            Dim report As New ReportDocument()
            Dim connection As IConnectionInfo
            Dim serverName1 As String = s.Server  ' "Server1"
            Dim userID As String = d.Username ' "MyUserID"
            Dim password As String = p.Password ' "MyPassword"

            report.Load(Application.StartupPath & "\PICC-reports\Applicant_According_to_age_Revised.rpt")
            report.Refresh()
            report.SetParameterValue("@agefrom", Applicant_Selection.txtage.Text) 'datehiredfrom.TextBox_Text)
            report.SetParameterValue("@ageto", Applicant_Selection.txtageto.Text) 'datehiredto.TextBox_Text)

            ' Set Database Logon to main report
            For Each connection In report.DataSourceConnections
                Select Case connection.ServerName
                    Case serverName1
                        connection.SetLogon(userID, password)
                End Select
            Next

            ' Set Database Logon to subreport
            Dim subreport As ReportDocument
            For Each subreport In report.Subreports
                For Each connection In subreport.DataSourceConnections
                    Select Case connection.ServerName
                        Case serverName1
                            connection.SetLogon(userID, password)
                    End Select
                Next
            Next
            CooperativeReports.appreports.ReportSource = report
            CooperativeReports.MdiParent = Form1
            CooperativeReports.Show()
        Catch ex As Exception
            MessageBox.Show("Supply the other Parameter", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub
    Public Sub applicant_according_to_Gender()
        'Application.StartupPath & "\PICC-reports\Applicant_According_to_Gender_Revised.rpt"
        'objreport.
        Dim apprdr As New System.Configuration.AppSettingsReader
        Dim s, d, p As New Clsappconfiguration
        Try
            Dim report As New ReportDocument()
            Dim connection As IConnectionInfo
            Dim serverName1 As String = s.Server  ' "Server1"
            Dim userID As String = d.Username ' "MyUserID"
            Dim password As String = p.Password ' "MyPassword"

            report.Load(Application.StartupPath & "\PICC-reports\Applicant_According_to_Gender_Revised.rpt")
            report.Refresh()
            report.SetParameterValue("@Gender", Applicant_Selection.cbogender.Text)

            ' Set Database Logon to main report
            For Each connection In report.DataSourceConnections
                Select Case connection.ServerName
                    Case serverName1
                        connection.SetLogon(userID, password)
                End Select
            Next

            ' Set Database Logon to subreport
            Dim subreport As ReportDocument
            For Each subreport In report.Subreports
                For Each connection In subreport.DataSourceConnections
                    Select Case connection.ServerName
                        Case serverName1
                            connection.SetLogon(userID, password)
                    End Select
                Next
            Next
            CooperativeReports.appreports.ReportSource = report
            CooperativeReports.MdiParent = Form1
            CooperativeReports.Show()
        Catch ex As Exception
            MessageBox.Show("Supply the Parameter", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Public Sub applicant_according_to_civilstatus()
        Dim apprdr As New System.Configuration.AppSettingsReader
        Dim s, d, p As New Clsappconfiguration

        Dim report As New ReportDocument()
        Dim connection As IConnectionInfo
        Dim serverName1 As String = s.Server  ' "Server1"
        Dim userID As String = d.Username ' "MyUserID"
        Dim password As String = p.Password ' "MyPassword"
        Try
            report.Load(Application.StartupPath & "\PICC-reports\Applicant_According_to_Civilstatus.rpt")
            report.Refresh()
            report.SetParameterValue("@Civil_Status", Applicant_Selection.cboListBy.Text)

            ' Set Database Logon to main report
            For Each connection In report.DataSourceConnections
                Select Case connection.ServerName
                    Case serverName1
                        connection.SetLogon(userID, password)
                End Select
            Next

            ' Set Database Logon to subreport
            Dim subreport As ReportDocument
            For Each subreport In report.Subreports
                For Each connection In subreport.DataSourceConnections
                    Select Case connection.ServerName
                        Case serverName1
                            connection.SetLogon(userID, password)
                    End Select
                Next
            Next
            CooperativeReports.appreports.ReportSource = report
            CooperativeReports.MdiParent = Form1
            CooperativeReports.Show()
        Catch ex As Exception
            MessageBox.Show("Supply the Parameter", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Public Sub applicant_according_to_degree()
        Dim apprdr As New System.Configuration.AppSettingsReader
        Dim s, d, p As New Clsappconfiguration

        Dim report As New ReportDocument()
        Dim connection As IConnectionInfo
        Dim serverName1 As String = s.Server  ' "Server1"
        Dim userID As String = d.Username ' "MyUserID"
        Dim password As String = p.Password ' "MyPassword"
        Try
            report.Load(Application.StartupPath & "\PICC-reports\Applicant_According_to_degree.rpt")
            report.Refresh()
            report.SetParameterValue("@degree", Applicant_Selection.cbodegree.Text)

            ' Set Database Logon to main report
            For Each connection In report.DataSourceConnections
                Select Case connection.ServerName
                    Case serverName1
                        connection.SetLogon(userID, password)
                End Select
            Next

            ' Set Database Logon to subreport
            Dim subreport As ReportDocument
            For Each subreport In report.Subreports
                For Each connection In subreport.DataSourceConnections
                    Select Case connection.ServerName
                        Case serverName1
                            connection.SetLogon(userID, password)
                    End Select
                Next
            Next
            CooperativeReports.appreports.ReportSource = report
            CooperativeReports.MdiParent = Form1
            CooperativeReports.Show()
        Catch ex As Exception
            MessageBox.Show("Supply the Parameter", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Public Sub applicant_according_to_course()
        Dim apprdr As New System.Configuration.AppSettingsReader
        Dim s, d, p As New Clsappconfiguration

        Dim report As New ReportDocument()
        Dim connection As IConnectionInfo
        Dim serverName1 As String = s.Server  ' "Server1"
        Dim userID As String = d.Username ' "MyUserID"
        Dim password As String = p.Password ' "MyPassword"
        Try
            report.Load(Application.StartupPath & "\PICC-reports\Applicant_According_to_Course.rpt")
            report.Refresh()
            report.SetParameterValue("@course", Applicant_Selection.txtcourse.Text)

            ' Set Database Logon to main report
            For Each connection In report.DataSourceConnections
                Select Case connection.ServerName
                    Case serverName1
                        connection.SetLogon(userID, password)
                End Select
            Next

            ' Set Database Logon to subreport
            Dim subreport As ReportDocument
            For Each subreport In report.Subreports
                For Each connection In subreport.DataSourceConnections
                    Select Case connection.ServerName
                        Case serverName1
                            connection.SetLogon(userID, password)
                    End Select
                Next
            Next
            CooperativeReports.appreports.ReportSource = report
            CooperativeReports.MdiParent = Form1
            CooperativeReports.Show()
        Catch ex As Exception
            MessageBox.Show("Supply the Parameter", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Public Sub applicant_according_to_school()
        Dim apprdr As New System.Configuration.AppSettingsReader
        Dim s, d, p As New Clsappconfiguration

        Dim report As New ReportDocument()
        Dim connection As IConnectionInfo
        Dim serverName1 As String = s.Server  ' "Server1"
        Dim userID As String = d.Username ' "MyUserID"
        Dim password As String = p.Password ' "MyPassword"
        Try
            report.Load(Application.StartupPath & "\PICC-reports\Applicant_According_to_School.rpt")
            report.Refresh()
            report.SetParameterValue("@school", Applicant_Selection.txtschool.Text)

            ' Set Database Logon to main report
            For Each connection In report.DataSourceConnections
                Select Case connection.ServerName
                    Case serverName1
                        connection.SetLogon(userID, password)
                End Select
            Next

            ' Set Database Logon to subreport
            Dim subreport As ReportDocument
            For Each subreport In report.Subreports
                For Each connection In subreport.DataSourceConnections
                    Select Case connection.ServerName
                        Case serverName1
                            connection.SetLogon(userID, password)
                    End Select
                Next
            Next
            CooperativeReports.appreports.ReportSource = report
            CooperativeReports.MdiParent = Form1
            CooperativeReports.Show()
        Catch ex As Exception
            MessageBox.Show("Supply the Parameter", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Public Sub applicant_according_to_jobpreferred()
        Dim apprdr As New System.Configuration.AppSettingsReader
        Dim s, d, p As New Clsappconfiguration

        Dim report As New ReportDocument()
        Dim connection As IConnectionInfo
        Dim serverName1 As String = s.Server  ' "Server1"
        Dim userID As String = d.Username ' "MyUserID"
        Dim password As String = p.Password ' "MyPassword"
        Try
            report.Load(Application.StartupPath & "\PICC-reports\Applicant_According_to_Jobprefered.rpt")
            report.Refresh()
            report.SetParameterValue("@Firstchoice", Applicant_Selection.cbojobpreferred.Text)

            ' Set Database Logon to main report
            For Each connection In report.DataSourceConnections
                Select Case connection.ServerName
                    Case serverName1
                        connection.SetLogon(userID, password)
                End Select
            Next

            ' Set Database Logon to subreport
            Dim subreport As ReportDocument
            For Each subreport In report.Subreports
                For Each connection In subreport.DataSourceConnections
                    Select Case connection.ServerName
                        Case serverName1
                            connection.SetLogon(userID, password)
                    End Select
                Next
            Next
            CooperativeReports.appreports.ReportSource = report
            CooperativeReports.MdiParent = Form1
            CooperativeReports.Show()
        Catch ex As Exception
            MessageBox.Show("Supply the Parameter", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub
    Public Sub according_to_date_applied()
        Dim apprdr As New System.Configuration.AppSettingsReader
        Dim s, d, p As New Clsappconfiguration

        Dim report As New ReportDocument()
        Dim connection As IConnectionInfo
        Dim serverName1 As String = s.Server  ' "Server1"
        Dim userID As String = d.Username ' "MyUserID"
        Dim password As String = p.Password ' "MyPassword"
        Try
            report.Load(Application.StartupPath & "\PICC-reports\Applicant_According_to_Dateapplied_Revised.rpt")
            report.Refresh()
            report.SetParameterValue("@Dateapplyfrom", Applicant_Selection.dateappliedfrom.Text)
            report.SetParameterValue("@Dateapplyto", Applicant_Selection.dateappliedto.Text)

            ' Set Database Logon to main report
            For Each connection In report.DataSourceConnections
                Select Case connection.ServerName
                    Case serverName1
                        connection.SetLogon(userID, password)
                End Select
            Next

            ' Set Database Logon to subreport
            Dim subreport As ReportDocument
            For Each subreport In report.Subreports
                For Each connection In subreport.DataSourceConnections
                    Select Case connection.ServerName
                        Case serverName1
                            connection.SetLogon(userID, password)
                    End Select
                Next
            Next
            CooperativeReports.appreports.ReportSource = report
            CooperativeReports.MdiParent = Form1
            CooperativeReports.Show()
        Catch ex As Exception
            MessageBox.Show("Supply the other Parameter", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    '===================================
    ' EMPLOYEE REPORTS
    ' crystal report login with sub report
    ' author : jolan p. mahinay
    ' create : 03/02/07
    '===================================


    Public Sub active_employees()
        Dim apprdr As New System.Configuration.AppSettingsReader
        Dim s, d, p As New Clsappconfiguration

        Dim report As New ReportDocument()
        Dim connection As IConnectionInfo
        Dim serverName1 As String = s.Server  ' "Server1"
        Dim userID As String = d.Username ' "MyUserID"
        Dim password As String = p.Password ' "MyPassword"
        Try
            report.Load(Application.StartupPath & "\PICC-reports\Print_list_of_active_employees.rpt")
            report.Refresh()

            ' Set Database Logon to main report
            For Each connection In report.DataSourceConnections
                Select Case connection.ServerName
                    Case serverName1
                        connection.SetLogon(userID, password)
                End Select
            Next

            ' Set Database Logon to subreport
            Dim subreport As ReportDocument
            For Each subreport In report.Subreports
                For Each connection In subreport.DataSourceConnections
                    Select Case connection.ServerName
                        Case serverName1
                            connection.SetLogon(userID, password)
                    End Select
                Next
            Next
            CooperativeReports.appreports.ReportSource = report
            CooperativeReports.MdiParent = Form1
            CooperativeReports.Show()
        Catch ex As Exception
            'MessageBox.Show("Supply the other Parameter", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Public Sub resigned_employees()
        Dim apprdr As New System.Configuration.AppSettingsReader
        Dim s, d, p As New Clsappconfiguration

        Dim report As New ReportDocument()
        Dim connection As IConnectionInfo
        Dim serverName1 As String = s.Server  ' "Server1"
        Dim userID As String = d.Username ' "MyUserID"
        Dim password As String = p.Password ' "MyPassword"
        Try
            report.Load(Application.StartupPath & "\PICC-reports\Print_list_of_resigned_employees.rpt")
            report.Refresh()

            ' Set Database Logon to main report
            For Each connection In report.DataSourceConnections
                Select Case connection.ServerName
                    Case serverName1
                        connection.SetLogon(userID, password)
                End Select
            Next

            ' Set Database Logon to subreport
            Dim subreport As ReportDocument
            For Each subreport In report.Subreports
                For Each connection In subreport.DataSourceConnections
                    Select Case connection.ServerName
                        Case serverName1
                            connection.SetLogon(userID, password)
                    End Select
                Next
            Next
            CooperativeReports.appreports.ReportSource = report
            CooperativeReports.MdiParent = Form1
            CooperativeReports.Show()
        Catch ex As Exception
            'MessageBox.Show("Supply the other Parameter", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Public Sub demography()
        Dim apprdr As New System.Configuration.AppSettingsReader
        Dim s, d, p As New Clsappconfiguration

        Dim report As New ReportDocument()
        Dim connection As IConnectionInfo
        Dim serverName1 As String = s.Server  ' "Server1"
        Dim userID As String = d.Username ' "MyUserID"
        Dim password As String = p.Password ' "MyPassword"
        Try
            report.Load(Application.StartupPath & "\PICC-reports\Employee_According_to_Demography.rpt")
            report.Refresh()

            ' Set Database Logon to main report
            For Each connection In report.DataSourceConnections
                Select Case connection.ServerName
                    Case serverName1
                        connection.SetLogon(userID, password)
                End Select
            Next

            ' Set Database Logon to subreport
            Dim subreport As ReportDocument
            For Each subreport In report.Subreports
                For Each connection In subreport.DataSourceConnections
                    Select Case connection.ServerName
                        Case serverName1
                            connection.SetLogon(userID, password)
                    End Select
                Next
            Next
            CooperativeReports.appreports.ReportSource = report
            CooperativeReports.MdiParent = Form1
            CooperativeReports.Show()
        Catch ex As Exception
            'MessageBox.Show("Supply the other Parameter", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Public Sub birthday()
        Dim apprdr As New System.Configuration.AppSettingsReader
        Dim s, d, p As New Clsappconfiguration

        Dim report As New ReportDocument()
        Dim connection As IConnectionInfo
        Dim serverName1 As String = s.Server  ' "Server1"
        Dim userID As String = d.Username ' "MyUserID"
        Dim password As String = p.Password ' "MyPassword"
        Try
            report.Load(Application.StartupPath & "\PICC-reports\Birthday.rpt")
            report.Refresh()
            report.SetParameterValue("@month", Employee_Selection_Report.cbomonth.Text) '2)
            report.DataDefinition.FormulaFields("sDate").Text = "'" & Employee_Selection_Report.txtMonthName.Text & "'"

            ' Set Database Logon to main report
            For Each connection In report.DataSourceConnections
                Select Case connection.ServerName
                    Case serverName1
                        connection.SetLogon(userID, password)
                End Select
            Next

            ' Set Database Logon to subreport
            Dim subreport As ReportDocument
            For Each subreport In report.Subreports
                For Each connection In subreport.DataSourceConnections
                    Select Case connection.ServerName
                        Case serverName1
                            connection.SetLogon(userID, password)
                    End Select
                Next
            Next
            CooperativeReports.appreports.ReportSource = report
            CooperativeReports.MdiParent = Form1
            CooperativeReports.Show()
        Catch ex As Exception
            MessageBox.Show("Supply the Parameter", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Public Sub employee_according_to_yearsofservice()
        Dim apprdr As New System.Configuration.AppSettingsReader
        Dim s, d, p As New Clsappconfiguration

        Dim report As New ReportDocument()
        Dim connection As IConnectionInfo
        Dim serverName1 As String = s.Server  ' "Server1"
        Dim userID As String = d.Username ' "MyUserID"
        Dim password As String = p.Password ' "MyPassword"
        Try
            report.Load(Application.StartupPath & "\PICC-reports\Employee_According_to_YearsOfService.rpt")
            report.Refresh()
            report.SetParameterValue("@TENUREFROM", Employee_Selection_Report.txttenurefrom.Text)
            report.SetParameterValue("@TENURETO", Employee_Selection_Report.txttenureto.Text)

            ' Set Database Logon to main report
            For Each connection In report.DataSourceConnections
                Select Case connection.ServerName
                    Case serverName1
                        connection.SetLogon(userID, password)
                End Select
            Next

            ' Set Database Logon to subreport
            Dim subreport As ReportDocument
            For Each subreport In report.Subreports
                For Each connection In subreport.DataSourceConnections
                    Select Case connection.ServerName
                        Case serverName1
                            connection.SetLogon(userID, password)
                    End Select
                Next
            Next
            CooperativeReports.appreports.ReportSource = report
            CooperativeReports.MdiParent = Form1
            CooperativeReports.Show()
        Catch ex As Exception
            MessageBox.Show("Supply the other Parameter", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Public Sub certification()
        Dim apprdr As New System.Configuration.AppSettingsReader
        Dim s, d, p As New Clsappconfiguration

        Dim report As New ReportDocument()
        Dim connection As IConnectionInfo
        Dim serverName1 As String = s.Server  ' "Server1"
        Dim userID As String = d.Username ' "MyUserID"
        Dim password As String = p.Password ' "MyPassword"
        Try
            report.Load(Application.StartupPath & "\PICC-reports\employee_certification_with_subreport.rpt")
            report.Refresh()
            report.SetParameterValue("@idnumber", Employee_Selection_Report.cbocertificate.Text)

            ' Set Database Logon to main report
            For Each connection In report.DataSourceConnections
                Select Case connection.ServerName
                    Case serverName1
                        connection.SetLogon(userID, password)
                End Select
            Next

            ' Set Database Logon to subreport
            Dim subreport As ReportDocument
            For Each subreport In report.Subreports
                For Each connection In subreport.DataSourceConnections
                    Select Case connection.ServerName
                        Case serverName1
                            connection.SetLogon(userID, password)
                    End Select
                Next
            Next
            CooperativeReports.appreports.ReportSource = report
            CooperativeReports.MdiParent = Form1
            CooperativeReports.Show()
        Catch ex As Exception
            MessageBox.Show("Supply the Parameter", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub
    Public Sub employee_training()

        Dim apprdr As New System.Configuration.AppSettingsReader
        Dim s, d, p As New Clsappconfiguration

        Dim report As New ReportDocument()
        Dim connection As IConnectionInfo
        Dim serverName1 As String = s.Server  ' "Server1"
        Dim userID As String = d.Username ' "MyUserID"
        Dim password As String = p.Password ' "MyPassword"
        Try
            report.Load(Application.StartupPath & "\PICC-reports\EMPLOYEE_MAIN_TRAINING_WITH_SUBREPORT.rpt")
            report.Refresh()
            report.SetParameterValue("@idnumber", Employee_Selection_Report.cbotraining.Text)

            ' Set Database Logon to main report
            For Each connection In report.DataSourceConnections
                Select Case connection.ServerName
                    Case serverName1
                        connection.SetLogon(userID, password)
                End Select
            Next

            ' Set Database Logon to subreport
            Dim subreport As ReportDocument
            For Each subreport In report.Subreports
                For Each connection In subreport.DataSourceConnections
                    Select Case connection.ServerName
                        Case serverName1
                            connection.SetLogon(userID, password)
                    End Select
                Next
            Next
            CooperativeReports.appreports.ReportSource = report
            CooperativeReports.MdiParent = Form1
            CooperativeReports.Show()
        Catch ex As Exception
            MessageBox.Show("Supply the Parameter", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub
    Public Sub employee_according_to_age()

        Dim apprdr As New System.Configuration.AppSettingsReader
        Dim s, d, p As New Clsappconfiguration

        Dim report As New ReportDocument()
        Dim connection As IConnectionInfo
        Dim serverName1 As String = s.Server  ' "Server1"
        Dim userID As String = d.Username ' "MyUserID"
        Dim password As String = p.Password ' "MyPassword"
        Try
            report.Load(Application.StartupPath & "\PICC-reports\Employee_According_to_Age.rpt")
            report.Refresh()
            report.SetParameterValue("@AgeFrom", Employee_Selection_Report.txtagefrom.Text)
            report.SetParameterValue("@AgeTo", Employee_Selection_Report.txtageto.Text)

            ' Set Database Logon to main report
            For Each connection In report.DataSourceConnections
                Select Case connection.ServerName
                    Case serverName1
                        connection.SetLogon(userID, password)
                End Select
            Next

            ' Set Database Logon to subreport
            Dim subreport As ReportDocument
            For Each subreport In report.Subreports
                For Each connection In subreport.DataSourceConnections
                    Select Case connection.ServerName
                        Case serverName1
                            connection.SetLogon(userID, password)
                    End Select
                Next
            Next
            CooperativeReports.appreports.ReportSource = report
            CooperativeReports.MdiParent = Form1
            CooperativeReports.Show()
        Catch ex As Exception
            MessageBox.Show("Supply the other Parameter", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub
    Public Sub employee_according_to_degree()

        Dim apprdr As New System.Configuration.AppSettingsReader
        Dim s, d, p As New Clsappconfiguration

        Dim report As New ReportDocument()
        Dim connection As IConnectionInfo
        Dim serverName1 As String = s.Server  ' "Server1"
        Dim userID As String = d.Username ' "MyUserID"
        Dim password As String = p.Password ' "MyPassword"
        Try
            report.Load(Application.StartupPath & "\PICC-reports\Employee_According_to_Degree.rpt")
            report.Refresh()
            report.SetParameterValue("@degree", Employee_Selection_Report.cbodegree.Text)

            ' Set Database Logon to main report
            For Each connection In report.DataSourceConnections
                Select Case connection.ServerName
                    Case serverName1
                        connection.SetLogon(userID, password)
                End Select
            Next

            ' Set Database Logon to subreport
            Dim subreport As ReportDocument
            For Each subreport In report.Subreports
                For Each connection In subreport.DataSourceConnections
                    Select Case connection.ServerName
                        Case serverName1
                            connection.SetLogon(userID, password)
                    End Select
                Next
            Next
            CooperativeReports.appreports.ReportSource = report
            CooperativeReports.MdiParent = Form1
            CooperativeReports.Show()
        Catch ex As Exception
            MessageBox.Show("Supply the  Parameter", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Public Sub employee_according_to_school()
        Dim apprdr As New System.Configuration.AppSettingsReader
        Dim s, d, p As New Clsappconfiguration

        Dim report As New ReportDocument()
        Dim connection As IConnectionInfo
        Dim serverName1 As String = s.Server  ' "Server1"
        Dim userID As String = d.Username ' "MyUserID"
        Dim password As String = p.Password ' "MyPassword"
        Try
            report.Load(Application.StartupPath & "\PICC-reports\Employee_According_to_School.rpt")
            report.Refresh()
            report.SetParameterValue("@School", Employee_Selection_Report.cboSchool.Text)

            ' Set Database Logon to main report
            For Each connection In report.DataSourceConnections
                Select Case connection.ServerName
                    Case serverName1
                        connection.SetLogon(userID, password)
                End Select
            Next

            ' Set Database Logon to subreport
            Dim subreport As ReportDocument
            For Each subreport In report.Subreports
                For Each connection In subreport.DataSourceConnections
                    Select Case connection.ServerName
                        Case serverName1
                            connection.SetLogon(userID, password)
                    End Select
                Next
            Next
            CooperativeReports.appreports.ReportSource = report
            CooperativeReports.MdiParent = Form1
            CooperativeReports.Show()
        Catch ex As Exception
            MessageBox.Show("Supply the  Parameter", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub
    Public Sub employee_according_to_Course()

        Dim apprdr As New System.Configuration.AppSettingsReader
        Dim s, d, p As New Clsappconfiguration

        Dim report As New ReportDocument()
        Dim connection As IConnectionInfo
        Dim serverName1 As String = s.Server  ' "Server1"
        Dim userID As String = d.Username ' "MyUserID"
        Dim password As String = p.Password ' "MyPassword"
        Try
            report.Load(Application.StartupPath & "\PICC-reports\Employee_According_to_Course.rpt")
            report.Refresh()
            report.SetParameterValue("@Course", Employee_Selection_Report.cboCourse.Text)

            ' Set Database Logon to main report
            For Each connection In report.DataSourceConnections
                Select Case connection.ServerName
                    Case serverName1
                        connection.SetLogon(userID, password)
                End Select
            Next

            ' Set Database Logon to subreport
            Dim subreport As ReportDocument
            For Each subreport In report.Subreports
                For Each connection In subreport.DataSourceConnections
                    Select Case connection.ServerName
                        Case serverName1
                            connection.SetLogon(userID, password)
                    End Select
                Next
            Next
            CooperativeReports.appreports.ReportSource = report
            CooperativeReports.MdiParent = Form1
            CooperativeReports.Show()
        Catch ex As Exception
            MessageBox.Show("Supply the  Parameter", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub
    Public Sub employee_according_to_position()

        Dim apprdr As New System.Configuration.AppSettingsReader
        Dim s, d, p As New Clsappconfiguration

        Dim report As New ReportDocument()
        Dim connection As IConnectionInfo
        Dim serverName1 As String = s.Server  ' "Server1"
        Dim userID As String = d.Username ' "MyUserID"
        Dim password As String = p.Password ' "MyPassword"
        Try
            report.Load(Application.StartupPath & "\PICC-reports\Employee_According_to_Position.rpt")
            report.Refresh()
            report.SetParameterValue("@Title_Designation", Employee_Selection_Report.cboposition.Text)

            ' Set Database Logon to main report
            For Each connection In report.DataSourceConnections
                Select Case connection.ServerName
                    Case serverName1
                        connection.SetLogon(userID, password)
                End Select
            Next

            ' Set Database Logon to subreport
            Dim subreport As ReportDocument
            For Each subreport In report.Subreports
                For Each connection In subreport.DataSourceConnections
                    Select Case connection.ServerName
                        Case serverName1
                            connection.SetLogon(userID, password)
                    End Select
                Next
            Next
            CooperativeReports.appreports.ReportSource = report
            CooperativeReports.MdiParent = Form1
            CooperativeReports.Show()
        Catch ex As Exception
            MessageBox.Show("Supply the  Parameter", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub
    Public Sub employee_salary_grade()

        Dim apprdr As New System.Configuration.AppSettingsReader
        Dim s, d, p As New Clsappconfiguration

        Dim report As New ReportDocument()
        Dim connection As IConnectionInfo
        Dim serverName1 As String = s.Server  ' "Server1"
        Dim userID As String = d.Username ' "MyUserID"
        Dim password As String = p.Password ' "MyPassword"
        Try
            report.Load(Application.StartupPath & "\PICC-reports\Employee_According_to_SalaryGrade.rpt")
            report.Refresh()
            report.SetParameterValue("@Salary_Grade", Employee_Selection_Report.cbosalarygrade.Text)

            ' Set Database Logon to main report
            For Each connection In report.DataSourceConnections
                Select Case connection.ServerName
                    Case serverName1
                        connection.SetLogon(userID, password)
                End Select
            Next

            ' Set Database Logon to subreport
            Dim subreport As ReportDocument
            For Each subreport In report.Subreports
                For Each connection In subreport.DataSourceConnections
                    Select Case connection.ServerName
                        Case serverName1
                            connection.SetLogon(userID, password)
                    End Select
                Next
            Next
            CooperativeReports.appreports.ReportSource = report
            CooperativeReports.MdiParent = Form1
            CooperativeReports.Show()
        Catch ex As Exception
            MessageBox.Show("Supply the  Parameter", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub
    Public Sub employee_Department()

        Dim apprdr As New System.Configuration.AppSettingsReader
        Dim s, d, p As New Clsappconfiguration

        Dim report As New ReportDocument()
        Dim connection As IConnectionInfo
        Dim serverName1 As String = s.Server  ' "Server1"
        Dim userID As String = d.Username ' "MyUserID"
        Dim password As String = p.Password ' "MyPassword"
        Try
            report.Load(Application.StartupPath & "\PICC-reports\Employee_According_to_Department.rpt")
            report.Refresh()
            report.SetParameterValue("@Department", Employee_Selection_Report.txtdepartment.Text)

            ' Set Database Logon to main report
            For Each connection In report.DataSourceConnections
                Select Case connection.ServerName
                    Case serverName1
                        connection.SetLogon(userID, password)
                End Select
            Next

            ' Set Database Logon to subreport
            Dim subreport As ReportDocument
            For Each subreport In report.Subreports
                For Each connection In subreport.DataSourceConnections
                    Select Case connection.ServerName
                        Case serverName1
                            connection.SetLogon(userID, password)
                    End Select
                Next
            Next
            CooperativeReports.appreports.ReportSource = report
            CooperativeReports.MdiParent = Form1
            CooperativeReports.Show()
        Catch ex As Exception
            MessageBox.Show("Supply the  Parameter", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub
    Public Sub employement_status()

        Dim apprdr As New System.Configuration.AppSettingsReader
        Dim s, d, p As New Clsappconfiguration

        Dim report As New ReportDocument()
        Dim connection As IConnectionInfo
        Dim serverName1 As String = s.Server  ' "Server1"
        Dim userID As String = d.Username ' "MyUserID"
        Dim password As String = p.Password ' "MyPassword"
        Try
            report.Load(Application.StartupPath & "\PICC-reports\Employee_According_to_EmploymentStatus.rpt")
            report.Refresh()
            report.SetParameterValue("@Status", Employee_Selection_Report.cbostatus.Text)

            ' Set Database Logon to main report
            For Each connection In report.DataSourceConnections
                Select Case connection.ServerName
                    Case serverName1
                        connection.SetLogon(userID, password)
                End Select
            Next

            ' Set Database Logon to subreport
            Dim subreport As ReportDocument
            For Each subreport In report.Subreports
                For Each connection In subreport.DataSourceConnections
                    Select Case connection.ServerName
                        Case serverName1
                            connection.SetLogon(userID, password)
                    End Select
                Next
            Next
            CooperativeReports.appreports.ReportSource = report
            CooperativeReports.MdiParent = Form1
            CooperativeReports.Show()
        Catch ex As Exception
            MessageBox.Show("Supply the  Parameter", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub
    Public Sub employement_type()

        Dim apprdr As New System.Configuration.AppSettingsReader
        Dim s, d, p As New Clsappconfiguration

        Dim report As New ReportDocument()
        Dim connection As IConnectionInfo
        Dim serverName1 As String = s.Server  ' "Server1"
        Dim userID As String = d.Username ' "MyUserID"
        Dim password As String = p.Password ' "MyPassword"
        Try
            report.Load(Application.StartupPath & "\PICC-reports\Employee_According_to_EmploymentType.rpt")
            report.Refresh()
            report.SetParameterValue("@Employee_Type", Employee_Selection_Report.cboemployeetype.Text)

            ' Set Database Logon to main report
            For Each connection In report.DataSourceConnections
                Select Case connection.ServerName
                    Case serverName1
                        connection.SetLogon(userID, password)
                End Select
            Next

            ' Set Database Logon to subreport
            Dim subreport As ReportDocument
            For Each subreport In report.Subreports
                For Each connection In subreport.DataSourceConnections
                    Select Case connection.ServerName
                        Case serverName1
                            connection.SetLogon(userID, password)
                    End Select
                Next
            Next
            CooperativeReports.appreports.ReportSource = report
            CooperativeReports.MdiParent = Form1
            CooperativeReports.Show()
        Catch ex As Exception
            MessageBox.Show("Supply the  Parameter", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub
    Public Sub employee_according_to_gender()

        Dim apprdr As New System.Configuration.AppSettingsReader
        Dim s, d, p As New Clsappconfiguration

        Dim report As New ReportDocument()
        Dim connection As IConnectionInfo
        Dim serverName1 As String = s.Server  ' "Server1"
        Dim userID As String = d.Username ' "MyUserID"
        Dim password As String = p.Password ' "MyPassword"
        Try
            report.Load(Application.StartupPath & "\PICC-reports\Employee_According_to_Gender.rpt")
            report.Refresh()
            report.SetParameterValue("@gender", Employee_Selection_Report.cbogender.Text)

            ' Set Database Logon to main report
            For Each connection In report.DataSourceConnections
                Select Case connection.ServerName
                    Case serverName1
                        connection.SetLogon(userID, password)
                End Select
            Next

            ' Set Database Logon to subreport
            Dim subreport As ReportDocument
            For Each subreport In report.Subreports
                For Each connection In subreport.DataSourceConnections
                    Select Case connection.ServerName
                        Case serverName1
                            connection.SetLogon(userID, password)
                    End Select
                Next
            Next
            CooperativeReports.appreports.ReportSource = report
            CooperativeReports.MdiParent = Form1
            CooperativeReports.Show()
        Catch ex As Exception
            MessageBox.Show("Supply the  Parameter", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub
    Public Sub employee_according_to_civilstatus()

        Dim apprdr As New System.Configuration.AppSettingsReader
        Dim s, d, p As New Clsappconfiguration

        Dim report As New ReportDocument()
        Dim connection As IConnectionInfo
        Dim serverName1 As String = s.Server  ' "Server1"
        Dim userID As String = d.Username ' "MyUserID"
        Dim password As String = p.Password ' "MyPassword"
        Try
            report.Load(Application.StartupPath & "\PICC-reports\Employee_According_to_CivilStatus.rpt")
            report.Refresh()
            report.SetParameterValue("@Civil_status", Employee_Selection_Report.cbocivilstatus.Text)

            ' Set Database Logon to main report
            For Each connection In report.DataSourceConnections
                Select Case connection.ServerName
                    Case serverName1
                        connection.SetLogon(userID, password)
                End Select
            Next

            ' Set Database Logon to subreport
            Dim subreport As ReportDocument
            For Each subreport In report.Subreports
                For Each connection In subreport.DataSourceConnections
                    Select Case connection.ServerName
                        Case serverName1
                            connection.SetLogon(userID, password)
                    End Select
                Next
            Next
            CooperativeReports.appreports.ReportSource = report
            CooperativeReports.MdiParent = Form1
            CooperativeReports.Show()
        Catch ex As Exception
            MessageBox.Show("Supply the  Parameter", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub
    Public Sub employee_profile()
        '  objreport.Load(Application.StartupPath & "\PICC-reports\Employee_Profile_with_subreport.rpt")
        ' objreport.SetParameterValue("@idnumber", Me.cboemployeeprofileid.Text)
        Dim apprdr As New System.Configuration.AppSettingsReader
        Dim s, d, p As New Clsappconfiguration

        Dim report As New ReportDocument()
        Dim connection As IConnectionInfo
        Dim serverName1 As String = s.Server  ' "Server1"
        Dim userID As String = d.Username ' "MyUserID"
        Dim password As String = p.Password ' "MyPassword"
        Try
            report.Load(Application.StartupPath & "\PICC-reports\Employee_Profile_with_subreport.rpt")
            report.Refresh()
            report.SetParameterValue("@idnumber", Employee_Selection_Report.cboemployeeprofileid.Text)

            ' Set Database Logon to main report
            For Each connection In report.DataSourceConnections
                Select Case connection.ServerName
                    Case serverName1
                        connection.SetLogon(userID, password)
                End Select
            Next

            ' Set Database Logon to subreport
            Dim subreport As ReportDocument
            For Each subreport In report.Subreports
                For Each connection In subreport.DataSourceConnections
                    Select Case connection.ServerName
                        Case serverName1
                            connection.SetLogon(userID, password)
                    End Select
                Next
            Next
            CooperativeReports.appreports.ReportSource = report
            CooperativeReports.MdiParent = Form1
            CooperativeReports.Show()
        Catch ex As Exception
            MessageBox.Show("Supply the  Parameter", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub
End Module
