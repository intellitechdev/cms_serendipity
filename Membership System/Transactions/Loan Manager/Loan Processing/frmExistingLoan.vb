﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmExistingLoan

    Private con As New Clsappconfiguration
    Public empno As String
    Public mode As String = ""

    Private Sub frmExistingLoan_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If mode = "all" Then
            LoadAllLoans("")
        Else
            ListExistingLoans(empno)
        End If
    End Sub

    Private Sub LoadAllLoans(ByVal search As String)
        Try
            dgvExistingLoans.Columns.Clear()
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(con.cnstring, "_Select_AllExistingLoans_All",
                                           New SqlParameter("@search", search))
            dgvExistingLoans.DataSource = ds.Tables(0)
            dgvExistingLoans.Columns(1).Width = 150
            dgvExistingLoans.Columns(4).Width = 50
            dgvExistingLoans.Columns(7).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        Catch ex As Exception
            'frmMsgBox.txtMessage.Text = "Oops!, Something went wrong - " + ex.ToString
            'frmMsgBox.ShowDialog()
        End Try
    End Sub

    Private Sub ListExistingLoans(ByVal empno As String)
        Try
            dgvExistingLoans.Columns.Clear()
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(con.cnstring, "_Select_AllExistingLoans",
                                           New SqlParameter("@EmployeeNo", empno))
            dgvExistingLoans.DataSource = ds.Tables(0)
            dgvExistingLoans.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        Catch ex As Exception
            'frmMsgBox.txtMessage.Text = "Oops!, Something went wrong - " + ex.ToString
            'frmMsgBox.ShowDialog()
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelect.Click
        GetSelectedVal()
    End Sub

    Private Sub GetExistingLoan_Details()
        With dgvExistingLoans
            frmLoan_ApplicationMain.txtLoanNo.Text = .SelectedRows(0).Cells(0).Value.ToString()
            frmLoan_ApplicationMain.cboLoanType.Text = .SelectedRows(0).Cells(1).Value.ToString()
            frmLoan_ApplicationMain.cboTerms.Text = .SelectedRows(0).Cells(2).Value.ToString()
            frmLoan_ApplicationMain.txtPrincipalAmount.Text = .SelectedRows(0).Cells(5).Value.ToString()
            frmLoan_ApplicationMain.CarryForwardPrincipal = .SelectedRows(0).Cells(5).Value.ToString()
        End With
        frmLoan_ApplicationMain.panelStep1.Enabled = True
        frmLoan_ApplicationMain.panelstep2.Enabled = True
        frmLoan_ApplicationMain.panelStep3.Enabled = True
        Me.Close()
    End Sub

    Private Sub dgvExistingLoans_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvExistingLoans.DoubleClick
        GetSelectedVal()
    End Sub

    Private Sub txtSearch_TextChanged(sender As Object, e As EventArgs) Handles txtSearch.TextChanged
        If mode = "all" Then
            LoadAllLoans(txtSearch.Text)
        Else
            Exit Sub ' other code here
        End If
    End Sub

    Private Sub SelectExistingLoans()
        Dim empid As String
        Dim clientname As String
        Try
            With dgvExistingLoans
                frmLoan_ApplicationMain.GetEmployeeID() = CType(.SelectedRows(0).Cells(0).Value, String)
                empid = .SelectedRows(0).Cells(0).Value.ToString()
                ' frmLoan_ApplicationComputation.getEmpinfo() = empid
                frmLoan_ApplicationMain.Text = "Existing Loan - " + empid + " : " + .SelectedRows(0).Cells(1).Value.ToString()
                frmLoan_ApplicationMain.lblStepOne.Text = "Step 1 - " + .SelectedRows(0).Cells(1).Value.ToString()
                frmLoan_ApplicationMain.txtLoanNo.Text = .SelectedRows(0).Cells(2).Value.ToString()
                frmLoan_ApplicationMain.cboLoanType.Text = .SelectedRows(0).Cells(3).Value.ToString()
                frmLoan_ApplicationMain.cboTerms.Text = .SelectedRows(0).Cells(4).Value.ToString()
                'date granted
                'maturity
                'other details...
                frmLoan_ApplicationMain.txtPrincipalAmount.Text = .SelectedRows(0).Cells(7).Value.ToString()
                frmLoan_ApplicationMain.CarryForwardPrincipal = .SelectedRows(0).Cells(7).Value.ToString()
                frmLoan_ApplicationMain._GEtLoanDetails_ForEditing(.SelectedRows(0).Cells(2).Value.ToString())
                frmLoan_ApplicationMain.loanStatus = .SelectedRows(0).Cells(8).Value.ToString()

                frmLoan_Processing.onprocess = frmLoan_ApplicationMain.GetEmployeeID()
            End With
            frmLoan_ApplicationMain.panelStep1.Enabled = True
            frmLoan_ApplicationMain.panelstep2.Enabled = True
            frmLoan_ApplicationMain.panelStep3.Enabled = True
            Me.Close()
        Catch ex As Exception
            'fix this error 
            'frmMsgBox.txtMessage.Text = "Oops!, Something went wrong - " + ex.ToString
            'frmMsgBox.ShowDialog()
        End Try
    End Sub

    Private Sub GetSelectedVal()
        If mode = "all" Then
            'code here
            frmLoan_ApplicationMain.LoadAll_LoanTypes()
            frmLoan_ApplicationMain.txtRestructure.Text = ""
            frmLoan_ApplicationMain.cboTerms.Items.Clear()
            frmLoan_ApplicationMain.inEditMode = True
            frmLoan_ApplicationMain.LoadExistingLoan_Comakers()
            SelectExistingLoans()
        Else
            GetExistingLoan_Details()
            frmLoan_ApplicationMain.LoadExistingLoan_Comakers()
        End If
    End Sub

End Class