Imports System.Data.Sql
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmRqtsforApproval

#Region "Public Variables"
    Public Empinfo As String
#End Region
#Region "Public Property"
    Public Property getEmpinfo() As String
        Get
            Return Empinfo
        End Get
        Set(ByVal value As String)
            Empinfo = value
        End Set
    End Property
#End Region

#Region "View Selected Requirements Per Member"
    Private Sub ViewSelectedRequirements(ByVal empinfo As String)
        Dim gCon As New Clsappconfiguration()
        Dim ds As DataSet

        Try
            ds = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.StoredProcedure, "CIMS_Applicant_Retrieve_MembersipRequirements_PerMember", _
                        New SqlParameter("@fcEmployeeNo", empinfo))
            With grdRqts
                .DataSource = ds.Tables(0)
                .Columns(0).Visible = False
                .Columns(1).Width = 300
                .Columns(2).Visible = False
            End With

        Catch ex As Exception
            MessageBox.Show(ex.Message, "View Requirements for Approval")
        End Try
    End Sub
#End Region

#Region "Update for if approved"
    Private Sub TaggedCompleted(ByVal guid As String)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Applicants_AddEdit_MembershipRqets1", _
                                      New SqlParameter("@pk_MembershipReqts", guid), _
                                      New SqlParameter("@fbApproved", 1))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "Update Member Requirements")
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub
#End Region

#Region "Update Membership Status to Member"
    Private Sub UpdateMembershipStatus(ByVal empNo As String)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Applicants_ChangeMembershipStatus", _
                                      New SqlParameter("@empNo", empNo), _
                                      New SqlParameter("@status", "Active"))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "UpdateMembershipStatus")
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub
#End Region

    Private Sub frmRqtsforApproval_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        getEmpinfo() = frmNewMembersProcessing.lstApplicants.SelectedItems(0).Text
        Call ViewSelectedRequirements(Empinfo)
    End Sub

    Private Sub cmdCancel_RqtsforApproval_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel_RqtsforApproval.Click
        Me.Close()
    End Sub

    Private Sub cmdUpdate_RqtsforApproval_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUpdate_RqtsforApproval.Click
        '//update status to completed

        getEmpinfo() = frmNewMembersProcessing.GrdApplicantInfo.CurrentRow.Cells(0).Value
        Dim i As Integer
        Dim msg As New DialogResult
        Dim msg1 As New DialogResult

        If Me.grdRqts.RowCount = 0 Then
            msg1 = MessageBox.Show("No membership requirements. Cannot approve this applicant", "Membership", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Call frmNewMembersProcessing.ViewApplicantInfo(Empinfo)
            frmListofRqts.Show()
            Me.Close()
        Else
            msg = MessageBox.Show("Are you sure you want to approved this?", "Approved Membership", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If msg = Windows.Forms.DialogResult.Yes Then
                For i = 0 To Me.grdRqts.Rows.Count - 1
                    Call TaggedCompleted(Me.grdRqts.Item(0, i).Value)
                Next
                Call UpdateMembershipStatus(Empinfo)
                Call frmNewMembersProcessing.ViewApplicantInfo(Empinfo)
                Me.Close()
            Else
               
            End If

        End If

    End Sub
End Class