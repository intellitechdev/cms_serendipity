﻿Imports System.Data.Sql
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmMember_Award

    Public pkAwards As String

#Region "Property"
    Public Property getpkAwards() As String
        Get
            Return pkAwards
        End Get
        Set(ByVal value As String)
            pkAwards = value
        End Set
    End Property
#End Region

    'Private Sub frmMember_Award_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    '    dtGiven.Format = DateTimePickerFormat.Short
    '    'dtGiven.Value = Microsoft.VisualBasic.FormatDateTime(Now, DateFormat.ShortDate)
    'End Sub

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        Me.Close()
    End Sub

    Private Sub AddEditAwards(ByVal EmpNo As String, ByVal Awards As String, ByVal DateGiven As Date, ByVal GivenBy As String, ByVal pk_Awards As String)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Member_Awards_AddEdit", _
                                     New SqlParameter("@employeeNo", EmpNo), _
                                     New SqlParameter("@Awards", Awards), _
                                     New SqlParameter("@DateGiven", DateGiven), _
                                     New SqlParameter("@GivenBy", GivenBy), _
                                     New SqlParameter("@pk_Awards", pk_Awards))
            trans.Commit()
            MessageBox.Show("Record has been Saved", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "Save Awards")
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub


    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If txtAwards.Text = "" Or txtGivenBy.Text = "" Or dtGiven.Text = "" Then
            MessageBox.Show("Complete Infomation is Needed", "Awards Information", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        Else
            Call AddEditAwards(frmMember_Master.txtEmployeeNo.Text, txtAwards.Text, dtGiven.Value, txtGivenBy.Text, "")
            Call frmMember_Master.GetmemberAwards(frmMember_Master.txtEmployeeNo.Text)
            Call ClearText()
            'Me.Close()
        End If
    End Sub

    Private Sub btnupdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupdate.Click
            Call AddEditAwards(frmMember_Master.txtEmployeeNo.Text, txtAwards.Text, dtGiven.Value, txtGivenBy.Text, pkAwards)
            Call frmMember_Master.GetmemberAwards(frmMember_Master.txtEmployeeNo.Text)
            Me.Close()
    End Sub

    Private Sub ClearText()
        txtAwards.Text = ""
        txtGivenBy.Text = ""
    End Sub
End Class