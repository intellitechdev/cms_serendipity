Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared

Public Class Saving_and_Deposit_New
    Private x As Boolean
    Public MemberID As String
#Region "Get EMP idnumber"
    Private Sub getEmpIdnumber(ByVal id As String)
        Dim gcon As New Clsappconfiguration
        Dim searchno As String
        lstNumbers.Items.Clear()
        'lstname.Items.Clear()
        searchno = "select fcEmployeeNo from dbo.CIMS_m_Member_Employment " & _
                    "where fcEmployeeNo LIKE '%" & id & "%'"
        'searchname = "select fcLastName + ',' + fcFirstName from dbo.CIMS_m_Member " & _
        '"where fcLastName LIKE '%" & txtSearch.Text & "%'"

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, searchno)
                While rd.Read
                    lstNumbers.Items.Add(rd.Item(0).ToString)
                End While
            End Using
        Catch ex As Exception

        End Try
    End Sub
#End Region
#Region "Get Emp lastname"
    Private Sub getEmplastname(ByVal lastname As String)
        Dim gcon As New Clsappconfiguration
        lstNames.Items.Clear()
        Try
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.StoredProcedure, "MSS_MembersInfo_SearchName", New SqlParameter("@membername", lastname.Trim))
            While rd.Read
                lstNames.Items.Add(rd.Item(0).ToString)
            End While
            rd.Close()
            gcon.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
        End Try
    End Sub
#End Region
#Region "Search Member"
#Region "Load Member number"
    Private Sub Load_MemberNumber()
        Dim gCon As New Clsappconfiguration
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "CIMS_LoanManager_GetMemberList")
        While rd.Read
            lstNumbers.Items.Add(rd.Item("Employee No")).ToString()
        End While
        rd.Close()
        gCon.sqlconn.Close()

    End Sub
#End Region
    Private Sub Load_MemberName()
        'Dim gCon As New Clsappconfiguration
        'Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "CIMS_LoanManager_GetMemberList_Savings_and_TD")
        'Dim lv As ListViewItem

        'With Me.lstmember
        '    .Clear()
        '    .View = View.Details
        '    .GridLines = True
        '    .FullRowSelect = True
        '    .Columns.Add("IDnumber", 70, HorizontalAlignment.Left)
        '    .Columns.Add("Member's FullName", 200, HorizontalAlignment.Left)
        '    .Columns.Add("pk_Employee", 0, HorizontalAlignment.Left)
        '    .Columns.Add("as of", 0, HorizontalAlignment.Left)
        '    Try
        '        While rd.Read
        '            lv = New ListViewItem(rd.Item(0).ToString()) ' New ListViewItem(rd.Item(1)) '--(rd.Item(0).ToString()) '
        '            With lv
        '                .Tag = rd.Item("pk_Employee").ToString()
        '                .SubItems.Add(rd.Item(1).ToString())
        '                .SubItems.Add(rd.Item(2).ToString())
        '                .SubItems.Add(rd.Item(3).ToString())
        '            End With
        '            .Items.Add(lv)
        '            .TopItem.Focused = True
        '        End While
        '        rd.Close()
        '        gCon.sqlconn.Close()
        '    Catch ex As Exception
        '        MessageBox.Show(ex.Message, "Load Member list")
        '    End Try
        'End With
    End Sub
    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchName.TextChanged
        Search_MemberID(cbosavtd.Text, cboAccountName.Text, Me.txtSearch.Text)
    End Sub

    Private Sub Search_MemberID(ByVal accounttype As String, ByVal accountname As String, ByVal memberid As String)
        Dim gcon As New Clsappconfiguration
        'Dim searchno As String

        '[CIMS_LoanManager_GetMemberList_Savings_and_TD_ByTypeAccountNameMemberID]
        'searchno = "select fcEmployeeNo from dbo.CIMS_m_Member_Employment " & _
        '            "where fcEmployeeNo LIKE '%" & txtSearchName.Text & "%'"
        'searchname = "select fcLastName + ',' + fcFirstName from dbo.CIMS_m_Member " & _
        '"where fcLastName LIKE '%" & txtSearch.Text & "%'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.StoredProcedure, "CIMS_LoanManager_GetMemberList_Savings_and_TD_ByTypeAccountNameMemberID",
                                                                New SqlParameter("@fcAccountType", cbosavtd.Text),
                                                                New SqlParameter("@fcAccountName", cboAccountName.Text),
                                                                New SqlParameter("@MemberID", Me.txtSearch.Text))
                While rd.Read
                    lstNumbers.Items.Clear()
                    lstNumbers.Items.Add(rd.Item(0).ToString)
                End While
            End Using
        Catch ex As Exception

        End Try
    End Sub

    Private Sub txtSearchName_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtSearchName.KeyPress
        If e.KeyChar = Chr(13) Then
            Call SearchMember(Me.txtSearchName.Text)
        End If
    End Sub
    Private Sub SearchMember(ByVal searchname As String)

        Dim lv As ListViewItem
        Dim gcon As New Clsappconfiguration
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.sqlconn, CommandType.StoredProcedure, "CIMS_LoanManager_GetMemberList_SearchName_savingTD", _
                                                         New SqlParameter("@SEARCH", searchname))
        With Me.lstmember
            .Clear()
            .View = View.Details
            .GridLines = True
            .FullRowSelect = True
            .Columns.Add("IDnumber", 70, HorizontalAlignment.Left)
            .Columns.Add("Member's Fullname", 200, HorizontalAlignment.Left)
            .Columns.Add("pk_Employee", 0, HorizontalAlignment.Left)
            Try
                While rd.Read
                    lv = New ListViewItem(rd.Item(0).ToString())
                    With lv
                        .Tag = rd.Item("pk_Employee").ToString()
                        .SubItems.Add(rd.Item(1).ToString())
                    End With
                    .Items.Add(lv)
                    .TopItem.Focused = True
                End While
                rd.Close()
                gcon.sqlconn.Close()
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Load Member list")
            End Try
        End With
    End Sub
    Private Sub btnsrch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsrch.Click
        If Me.txtSearchName.Text = "" Then
            Call Load_MemberName()
        Else
            Call SearchMember(Me.txtSearchName.Text)
        End If

    End Sub
#End Region
    Private Sub btnSettings_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSettings.Click
        frmBankSettings.ShowDialog()
    End Sub
    Private Sub btnDep_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDep.Click
        On Error Resume Next
        If cbosavtd.Text = "Debit" Then
            Deposit.GroupBox2.Text = "Debit"
            Deposit.GroupBox1.Text = "Credit"
        ElseIf cbosavtd.Text = "Credit" Then
            Deposit.GroupBox2.Text = "Credit"
            Deposit.GroupBox1.Text = "Debit"
        End If
        Deposit.GetUniqEmp = Me.lstmember.FocusedItem.Tag.ToString
        Deposit.GetName = Me.Txtname.Text
        Deposit.GetIDNo = Me.lstmember.SelectedItems(0).SubItems(4).Text
        'Deposit.txtDes.Text = Me.cboAccountName.Text
        'MessageBox.Show(Me.cboAccountName.SelectedValue.ToString)
        Deposit.txtDes.Tag = Me.cboAccountName.SelectedValue
        Deposit.GetRefNoForMember = Me.lstmember.SelectedItems(0).SubItems(0).Text
        'Deposit.GetRefNo = Me.grdSavingDeposit.se
        Call ViewSavingsDeposit(Me.lstmember.SelectedItems(0).SubItems(4).Text, Me.lstmember.SelectedItems(0).SubItems(0).Text)
        Call Compute_AvailableBalance_SavingsDeposit(Me.lstmember.SelectedItems(0).SubItems(4).Text, Me.lstmember.SelectedItems(0).SubItems(0).Text)
        Call Compute_CurrentBalance_SavingsDeposit(Me.lstmember.SelectedItems(0).SubItems(4).Text, Me.lstmember.SelectedItems(0).SubItems(0).Text)
        Call LoadTerm(Me.lstmember.SelectedItems(0).SubItems(4).Text)
        LoadMaturityDate(Me.lstmember.SelectedItems(0).SubItems(4).Text)
        Deposit.ShowDialog()
        'Deposit.GetSelectSTD = Me.cboAccountName.SelectedItem
        'Call ViewTimeDeposit(Me.lstmember.SelectedItems(0).Text)
        'Call Compute_CurrentBalance_TImeDeposit(lstmember.SelectedItems(0).Text)
        'Call Compute_AvailableBalance_TimeDeposit(Me.lstmember.SelectedItems(0).Text)
        'If Me.cbosavtd.SelectedIndex = 1 Then
        '    'Me.txtdtmatur.Text = lstmember.SelectedItems(0).SubItems(3).Text
        'Else

        'End If
    End Sub
    Private Sub btnwDraw_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnwDraw.Click
        'frmWithdrawal.GetSelectSTD = cbosavtd.SelectedItem
        If txtcurbal.Text = "0.00" Or txtcurbal.Text = "0" Or txtcurbal.Text = "" Then
            MessageBox.Show("Cannot continue transaction!.....", "Withdraw", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Else
            If cbosavtd.Text = "Debit" Then
                frmWithdrawal.GroupBox2.Text = "Debit"
                frmWithdrawal.GroupBox1.Text = "Credit"
            ElseIf cbosavtd.Text = "Credit" Then
                frmWithdrawal.GroupBox2.Text = "Credit"
                frmWithdrawal.GroupBox1.Text = "Debit"
            End If
            frmWithdrawal.GetIDNo = lstmember.SelectedItems(0).SubItems(4).Text
            frmWithdrawal.GetName = Txtname.Text
            frmWithdrawal.GetRefNoForMember = lstmember.SelectedItems(0).SubItems(0).Text
            'frmWithdrawal.GetUniqEmp = lstmember.SelectedItems(0).SubItems(2).Text
            frmWithdrawal.GetUniqEmp = Me.lstmember.FocusedItem.Tag.ToString
            frmWithdrawal.txtDescription.Tag = cboAccountName.SelectedValue
            'MessageBox.Show(cboAccountName.SelectedValue.ToString)
            'frmWithdrawal.txtDescription.Text = cboAccountName.Text
            frmWithdrawal.txtAmount.Focus()
            frmWithdrawal.ShowDialog()
        End If
    End Sub
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        On Error Resume Next
        'Call Compute_CurrentBalance_TImeDeposit(lstmember.SelectedItems(0).Text)
        Call Compute_AvailableBalance_SavingsDeposit(Me.lstmember.SelectedItems(0).SubItems(4).Text, Me.lstmember.SelectedItems(0).SubItems(0).Text)
        'Call Compute_AvailableBalance_TimeDeposit(Me.lstmember.SelectedItems(0).Text)
        Call Compute_CurrentBalance_SavingsDeposit(Me.lstmember.SelectedItems(0).SubItems(4).Text, Me.lstmember.SelectedItems(0).SubItems(0).Text)
        Call LoadTerm(Me.lstmember.SelectedItems(0).SubItems(4).Text)
        LoadMaturityDate(Me.lstmember.SelectedItems(0).SubItems(4).Text)
        frmCheckforClearing.ShowDialog()
    End Sub
    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        Close()
    End Sub
    Private Sub cbosavtd_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbosavtd.SelectedIndexChanged
        SelectMode()
    End Sub
    Private Sub SelectMode()
        Select Case Me.cbosavtd.Text
            Case "Debit"
                AccountNameListByType("Debit")
                'Me.txtterm.Visible = False
                'Me.txtdtmatur.Visible = False
                'Me.lbldateofmaturity.Visible = False
                'Me.lblterm.Visible = False
                'Me.txtavailbal.Visible = True
                'Me.txtcurbal.Visible = True
                'Me.txttdavailbal.Visible = False
                'Me.txttdcurbal.Visible = False
                'Me.grdTimeDeposit.Visible = False
                Me.grdSavingDeposit.Visible = True
                'Me.GroupBox3.Text = "Debit"
            Case "Credit"
                AccountNameListByType("Credit")
                Me.txtterm.Visible = True
                Me.txtdtmatur.Visible = True
                Me.lbldateofmaturity.Visible = True
                Me.lblterm.Visible = True
                'Me.txtavailbal.Visible = False
                'Me.txtcurbal.Visible = False
                'Me.txttdavailbal.Visible = True
                'Me.txttdcurbal.Visible = True
                'Me.grdTimeDeposit.Visible = True
                'Me.grdSavingDeposit.Visible = False
                'Me.GroupBox3.Text = "Credit"
        End Select
    End Sub

    Private Sub AccountNameListByType(ByVal type As String)
        Dim gcon As New Clsappconfiguration
        Dim dr As DataSet
        dr = SqlHelper.ExecuteDataset(gcon.cnstring, CommandType.StoredProcedure, "CIMS_DebitCreate_ListByType", _
                                        New SqlParameter("@AccountType", type))
        Try
            'dr.Tables(0).Rows.Clear()
            With cboAccountName
                If .DataSource Is Nothing Then
                    .DataSource = dr.Tables(0)
                    .ValueMember = "fxkey_AccountID"
                    .DisplayMember = "AccountName"
                Else
                    .DataSource = Nothing
                    .Items.Clear()
                    .DataSource = dr.Tables(0)
                    .ValueMember = "fxkey_AccountID"
                    .DisplayMember = "AccountName"
                End If
            End With
        Catch ex As Exception
            MessageBox.Show(ex.ToString)
        End Try
    End Sub
    Private Sub PlotRecord()
        Try
            Me.txtID.Text = Me.lstmember.SelectedItems(0).SubItems(0).Text
            Me.Txtname.Text = Me.lstmember.SelectedItems(0).SubItems(1).Text
            Me.dtdate.Text = Me.lstmember.SelectedItems(0).SubItems(3).Text
        Catch ex As Exception

        End Try
    End Sub
    Private Sub Saving_and_Deposit_New_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Load_MemberName()
        loadsavtd()
        cbosavtd.Text = ""
    End Sub
#Region "View Saving Deposit"
    Public Sub ViewSavingsDeposit(ByVal Empinfo As String, ByVal RefNo As String)
        Dim gcon As New Clsappconfiguration
        Dim ds As New DataSet
        Dim ad As New SqlDataAdapter
        Dim cmd As New SqlCommand("MSS_LoanTracking_GetSavingsDeposit", gcon.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        gcon.sqlconn.Open()
        With cmd.Parameters
            .Add("@EmployeeNo", SqlDbType.VarChar, 256).Value = Empinfo
            .Add("@RefNo", SqlDbType.VarChar, 15).Value = RefNo
        End With
        If grdSavingDeposit.DataSource Is Nothing Or grdSavingDeposit.DataSource Is DBNull.Value Then

        Else
            grdSavingDeposit.DataSource = Nothing
            grdSavingDeposit.Refresh()
            'grdSavingDeposit.Rows.Clear()
        End If
        Try
            ad.SelectCommand = cmd
            ad.Fill(ds, "CIMS_m_Member")
            With Me.grdSavingDeposit
                .DataSource = ds
                .DataMember = "CIMS_m_Member"
                .Columns(0).Width = 150
                .Columns(1).Width = 250
                .Columns(2).Width = 150
                .Columns(2).DefaultCellStyle.Format = "#,##0.00"
                .Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
                .Columns(3).Width = 100
                .Columns(3).DefaultCellStyle.Format = "#,##0.00"
                .Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
                '.Columns(4).Visible = False
                .Columns(4).DefaultCellStyle.Format = "#,##0.00"
                .Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
                ' .Columns(5).Visible = False
                .Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
                '.Columns(6).Visible = False
                .Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
                .Columns(6).Visible = False
                .Columns(7).Visible = False
            End With
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, "MSS_LoanTracking_GetSavingsDeposit",
                                                              New SqlParameter("@EmployeeNo", Empinfo),
                                                              New SqlParameter("@RefNo", RefNo))
            While rd.Read
                Deposit.GetRefNo = rd(5).ToString
                frmWithdrawal.GetRefNo = rd(5).ToString
                If rd(5).ToString = "" Then
                Else
                    'Deposit.txtRefNo.Text = rd(5).ToString
                    'Deposit.txtDes.Text = rd(1).ToString
                End If
                frmWithdrawal.GetIDNo = rd(7).ToString
                Deposit.GetIDNo = rd(7).ToString
            End While
            Me.btnInt.Enabled = True
            Me.btnDep.Enabled = True
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            'MessageBox.Show(ex.Message, "View Saving Deposit")
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub
#End Region

#Region "View Time Deposit"
    Private Sub ViewTimeDeposit(ByVal Empinof As String)
        Dim gcon As New Clsappconfiguration
        Dim ds As New DataSet
        Dim ad As New SqlDataAdapter
        Dim cmd As New SqlCommand("MSS_LoanTracking_GetTimeDeposit", gcon.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        gcon.sqlconn.Open()
        With cmd.Parameters
            .Add("@EmployeeNo", SqlDbType.VarChar, 256).Value = Empinof
        End With

        Try
            ad.SelectCommand = cmd
            ad.Fill(ds, "CIMS_m_Member")

            Me.grdTimeDeposit.DataSource = ds
            Me.grdTimeDeposit.DataMember = "CIMS_m_Member"
            Me.grdTimeDeposit.Columns(0).Width = 150
            Me.grdTimeDeposit.Columns(1).Width = 250
            Me.grdTimeDeposit.Columns(3).Width = 150
            Me.grdTimeDeposit.Columns(3).DefaultCellStyle.Format = "#,##0.00"
            Me.grdTimeDeposit.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
            Me.grdTimeDeposit.Columns(4).Width = 150
            Me.grdTimeDeposit.Columns(4).DefaultCellStyle.Format = "#,##0.00"
            Me.grdTimeDeposit.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
            Me.grdTimeDeposit.Columns(5).Visible = False
            grdTimeDeposit.Columns(6).Visible = False

            cmd.ExecuteNonQuery()
            gcon.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "View Time Deposit")
        End Try
    End Sub
#End Region

    Private Sub lstmember_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstmember.Click
        Dim x = Me.cbosavtd.SelectedItem
        ' If x = "Debit" Or x = "Credit" Then
        'SelectMode()
        PlotRecord()
        Call ViewSavingsDeposit(Me.lstmember.SelectedItems(0).SubItems(4).Text, Me.lstmember.SelectedItems(0).SubItems(0).Text)
        'Call ViewTimeDeposit(Me.lstmember.SelectedItems(0).Text)
        Call Compute_CurrentBalance_SavingsDeposit(Me.lstmember.SelectedItems(0).SubItems(4).Text, Me.lstmember.SelectedItems(0).SubItems(0).Text)
        Call Compute_AvailableBalance_SavingsDeposit(Me.lstmember.SelectedItems(0).SubItems(4).Text, Me.lstmember.SelectedItems(0).SubItems(0).Text)
        'Call Compute_AvailableBalance_TimeDeposit(Me.lstmember.SelectedItems(0).Text)
        'Call Compute_CurrentBalance_TImeDeposit(lstmember.SelectedItems(0).Text)
        Call LoadTerm(Me.lstmember.SelectedItems(0).SubItems(4).Text)
        LoadMaturityDate(Me.lstmember.SelectedItems(0).SubItems(4).Text)
        Deposit.GetUniqEmp = Me.lstmember.SelectedItems(0).SubItems(4).Text
        'Else
        '    MessageBox.Show("Select Type First", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
        'End If
    End Sub

    Public Sub LoadTerm(ByVal emp As String)
        Dim gcon As New Clsappconfiguration
        Dim ds As SqlDataReader
        ds = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.StoredProcedure, "CIMS_Deposit_TimeDeposit_LoadTerm", _
                                                        New SqlParameter("@EMPID", emp))
        Me.txtterm.Clear()
        Try
            While ds.Read
                Me.txtterm.Text = ds.Item(0).ToString
            End While
            ds.Close()
        Catch ex As Exception

        End Try
    End Sub
    Public Sub LoadMaturityDate(ByVal Matdate As String)
        Dim gcon As New Clsappconfiguration
        Dim dr As SqlDataReader
        dr = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.StoredProcedure, "CIMS_Deposit_TimeDeposit_LoadMaturedate", _
                                        New SqlParameter("EMPID", Matdate))
        Me.txtdtmatur.Clear()
        Try
            While dr.Read
                Me.txtdtmatur.Text = dr.Item(0).ToString
            End While
            dr.Close()
        Catch ex As Exception
        End Try
    End Sub
    Public Sub Compute_AvailableBalance_SavingsDeposit(ByVal pk_Member As String, ByVal refNo As String)
        Dim gcon As New Clsappconfiguration
        Dim ds As SqlDataReader
        ds = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.StoredProcedure, "usp_t_GetAvailableBalance_Savings_Deposit", _
                                                          New SqlParameter("@fkEmployee", pk_Member),
                                                          New SqlParameter("@fcRefNumber", refNo))
        Try
            'Me.txtavailbal.Clear()
            While ds.Read
                Me.txtavailbal.Text = ds.Item(0).ToStrsaing
            End While
            ds.Close()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub Compute_CurrentBalance_SavingsDeposit(ByVal pk_Member As String, ByVal refNo As String)
        Dim gcon As New Clsappconfiguration
        Dim ds As SqlDataReader
        ds = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.StoredProcedure, "usp_t_GetCurrentBalance_Savings_Deposit", _
                                                          New SqlParameter("@EmpID", pk_Member),
                                                          New SqlParameter("@fcRefNumber", refNo))
        Try
            While ds.Read
                Me.txtcurbal.Text = ds("BALANCE").ToString
            End While
            ds.Close()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub Compute_CurrentBalance_TImeDeposit(ByVal pk_Member As String)
        Dim gcon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.StoredProcedure, "usp_t_GetCurrentBalance_TimeDeposit_Deposit", _
                            New SqlParameter("@EmpID", pk_Member))
        Try
            While rd.Read
                Me.txttdcurbal.Text = rd.Item(0).ToString
            End While
            rd.Close()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub Compute_AvailableBalance_TimeDeposit(ByVal pk_Member As String)
        Dim gcon As New Clsappconfiguration
        Dim ds As SqlDataReader

        ds = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.StoredProcedure, "[usp_t_GetAvailableBalance_TimeDeposit_Deposit]", _
                                                          New SqlParameter("@fkEmployee", pk_Member))
        Try
            While ds.Read
                Me.txttdavailbal.Text = ds.Item(0).ToString
            End While
            ds.Close()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub lstNumbers_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstNumbers.SelectedIndexChanged
        Call ViewSavingsDeposit(Me.lstmember.SelectedItems(0).SubItems(4).Text, Me.lstmember.SelectedItems(0).SubItems(0).Text)
        'Call ViewTimeDeposit(Me.lstNumbers.SelectedItem.ToString)
    End Sub
    Private Sub lstNames_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstNames.SelectedIndexChanged
        Dim x, result As String
        x = lstNames.SelectedItem.ToString
        Dim y As String() = x.Split(New Char() {","})
        result = y(0)
        Call ViewSavingsDeposit(result, Me.lstmember.SelectedItems(0).Text)
        'Call ViewTimeDeposit(result)
    End Sub
    Private Sub loadsavtd()
        On Error Resume Next
        With Me.cbosavtd
            .SelectedIndex = 0
            .Items.Add("Debit")
            .Items.Add("Credit")
        End With
    End Sub
#Region "Filter"
    Private Sub filter_SavingsDeposit(ByVal Empinfo As String, ByVal datestart As Date, ByVal dateend As Date)
        Dim gcon As New Clsappconfiguration
        Dim ds As New DataSet
        Dim ad As New SqlDataAdapter
        Dim cmd As New SqlCommand("filter_Savings_deposit", gcon.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        gcon.sqlconn.Open()
        With cmd.Parameters
            .Add("@dateStart", SqlDbType.SmallDateTime).Value = datestart
            .Add("@dateEnd", SqlDbType.SmallDateTime).Value = dateend
            .Add("@EmployeeNo", SqlDbType.VarChar, 256).Value = Empinfo
        End With
        Try
            ad.SelectCommand = cmd
            ad.Fill(ds, "CIMS_m_Member")
            Me.grdSavingDeposit.DataSource = ds
            Me.grdSavingDeposit.DataMember = "CIMS_m_Member"
            Me.grdSavingDeposit.Columns(0).Width = 150
            Me.grdSavingDeposit.Columns(2).DefaultCellStyle.Format = "#,##0.00"
            Me.grdSavingDeposit.Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
            Me.grdSavingDeposit.Columns(3).DefaultCellStyle.Format = "#,##0.00"
            Me.grdSavingDeposit.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
            Me.grdSavingDeposit.Columns(4).DefaultCellStyle.Format = "#,##0.00"
            Me.grdSavingDeposit.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight



            cmd.ExecuteNonQuery()
            gcon.sqlconn.Close()
            Me.grdSavingDeposit.ClearSelection()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "View Saving Deposit")
        End Try
    End Sub
    Private Sub filter_TimeDeposit(ByVal Empinof As String, ByVal datestart As Date, ByVal dateend As Date)
        Dim gcon As New Clsappconfiguration
        Dim ds As New DataSet
        Dim ad As New SqlDataAdapter
        Dim cmd As New SqlCommand("filter_Time_deposit", gcon.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        gcon.sqlconn.Open()
        With cmd.Parameters
            .Add("@dateStart", SqlDbType.SmallDateTime).Value = datestart
            .Add("@dateEnd", SqlDbType.SmallDateTime).Value = dateend
            .Add("@EmployeeNo", SqlDbType.VarChar, 256).Value = Empinof
        End With

        Try
            ad.SelectCommand = cmd
            ad.Fill(ds, "CIMS_m_Member")

            Me.grdTimeDeposit.DataSource = ds
            Me.grdTimeDeposit.DataMember = "CIMS_m_Member"
            Me.grdTimeDeposit.Columns(0).Width = 150
            Me.grdTimeDeposit.Columns(3).DefaultCellStyle.Format = "#,##0.00"
            Me.grdTimeDeposit.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
            Me.grdTimeDeposit.Columns(4).DefaultCellStyle.Format = "#,##0.00"
            Me.grdTimeDeposit.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
            Me.grdTimeDeposit.Columns(5).DefaultCellStyle.Format = "#,##0.00"
            Me.grdTimeDeposit.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight

            cmd.ExecuteNonQuery()
            gcon.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "View Time Deposit")
        End Try
    End Sub
#End Region
    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        filter_SavingsDeposit(Me.lstmember.SelectedItems(0).SubItems(0).Text, FormatDateTime(dtstart.Value, DateFormat.ShortDate), FormatDateTime(dtend.Value, DateFormat.ShortDate))
        'filter_TimeDeposit(Me.lstmember.SelectedItems(0).Text, FormatDateTime(dtstart.Value, DateFormat.ShortDate), FormatDateTime(dtend.Value, DateFormat.ShortDate))
    End Sub

    'Private Sub lstmember_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles lstmember.KeyDown
    '    On Error Resume Next
    '    lstmember.Refresh()
    '    SelectMode()
    '    PlotRecord()
    '    Call ViewSavingsDeposit(Me.lstmember.SelectedItems(0).SubItems(4).Text, Me.lstmember.SelectedItems(0).SubItems(0).Text)
    '    'Call ViewTimeDeposit(Me.lstmember.SelectedItems(0).Text)
    '    Call Compute_CurrentBalance_TImeDeposit(Me.lstmember.SelectedItems(0).SubItems(4).Text)
    '    Call Compute_AvailableBalance_SavingsDeposit(Me.lstmember.SelectedItems(0).SubItems(4).Text)
    '    Call Compute_AvailableBalance_TimeDeposit(Me.lstmember.SelectedItems(0).Text)
    '    Call Compute_CurrentBalance_SavingsDeposit(lstmember.SelectedItems(0).Text)
    '    Call LoadTerm(lstmember.SelectedItems(0).Text)
    '    LoadMaturityDate(lstmember.SelectedItems(0).Text)
    'End Sub

    'Private Sub lstmember_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles lstmember.KeyUp
    '    On Error Resume Next
    '    lstmember.Refresh()
    '    SelectMode()
    '    PlotRecord()
    '    Call ViewSavingsDeposit(Me.lstmember.SelectedItems(0).SubItems(4).Text, Me.lstmember.SelectedItems(0).SubItems(0).Text)
    '    'Call ViewTimeDeposit(Me.lstmember.SelectedItems(0).Text)
    '    'Call Compute_CurrentBalance_TImeDeposit(lstmember.SelectedItems(0).Text)
    '    Call Compute_AvailableBalance_SavingsDeposit(Me.lstmember.SelectedItems(0).Text)
    '    'Call Compute_AvailableBalance_TimeDeposit(Me.lstmember.SelectedItems(0).Text)
    '    Call Compute_CurrentBalance_SavingsDeposit(lstmember.SelectedItems(0).Text)
    '    Call LoadTerm(lstmember.SelectedItems(0).Text)
    '    LoadMaturityDate(lstmember.SelectedItems(0).Text)
    'End Sub

    Private Sub btnInt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInt.Click
        'If cbosavtd.SelectedItem = "Savings" Then
        If grdSavingDeposit.RowCount = 0 Then
            MessageBox.Show("User Error! You cannot credit any interest. The member does not have any savings account.", "User Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        ElseIf Me.txtavailbal.Text = "0.00" Then
            MessageBox.Show("Information! This member dont a have an available balance. Please check the check's clearing.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            Call OpenInterestCreditWindow()
        End If

        'Else
        '    'For Time Deposit
        '    If grdTimeDeposit.RowCount = 0 Then

        '        MessageBox.Show("User Error! You cannot credit any interest. The member does not have any Time Deposit account.", "User Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        '    ElseIf Me.txttdavailbal.Text = "0.00" Then
        '        MessageBox.Show("Information! This member dont a have an available balance. Please check first the check's clearing.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Error)
        '    Else
        '        Call OpenInterestCreditWindow()

        '    End If
        'End If
    End Sub

    Private Sub OpenInterestCreditWindow()
        frmManual_Interest.GetEMPID = Me.lstmember.SelectedItems(0).SubItems(2).Text
        'frmManual_Interest.GetSelectSTD = Me.cbosavtd.SelectedIndex
        frmManual_Interest.GetName = Me.Txtname.Text
        frmManual_Interest.GetIDNo = Me.txtID.Text
        frmManual_Interest.txtDesc.Text = cboAccountName.Text

        frmManual_Interest.ShowDialog()

        Call ViewSavingsDeposit(Me.lstmember.SelectedItems(0).SubItems(4).Text, Me.lstmember.SelectedItems(0).SubItems(0).Text)
        'Call ViewTimeDeposit(Me.lstmember.SelectedItems(0).Text)

        'Call Compute_CurrentBalance_TImeDeposit(lstmember.SelectedItems(0).Text)
        Call Compute_AvailableBalance_SavingsDeposit(Me.lstmember.SelectedItems(0).Text, Me.lstmember.SelectedItems(0).SubItems(0).Text)
        'Call Compute_AvailableBalance_TimeDeposit(Me.lstmember.SelectedItems(0).Text)
        Call Compute_CurrentBalance_SavingsDeposit(lstmember.SelectedItems(0).Text, Me.lstmember.SelectedItems(0).SubItems(0).Text)
        LoadMaturityDate(lstmember.SelectedItems(0).Text)
        LoadTerm(lstmember.SelectedItems(0).Text)
    End Sub

#Region "REPORT"
    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If

        Dim obj As ReportObject

        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function
    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        ' for each table apply connection info
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            ' check if logon was successful
            ' if TestConnectivity returns false,
            ' check logon credentials
            If (tbl.TestConnectivity()) Then
                'drop fully qualified table location
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next

        Return True
    End Function
    Private Sub btnpreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnpreview.Click
        Try
            'If Me.cbosavtd.SelectedItem = "Savings" Then
            Dim childform As Integer = 0
            Dim childforms(4) As CooperativeReports
            childform = 1
            childforms(childform) = New CooperativeReports()
            childforms(childform).Text = "Savings Account"
            Dim con As New clsPersonnel
            Dim appRdr As New System.Configuration.AppSettingsReader
            Dim myconnection As New Clsappconfiguration
            Dim objreport As New CrystalDecisions.CrystalReports.Engine.ReportDocument
            objreport.Load(Application.StartupPath & "\LoanReport\SavingsReport.rpt")
            Logon(objreport, con.servername, con.databasename, con.username1, con.password1)
            objreport.Refresh()
            objreport.SetParameterValue("@EmpID", lstmember.SelectedItems(0).SubItems(2).Text)
            childforms(childform).appreports.ReportSource = objreport
            'childforms(childform).MdiParent =me
            childforms(childform).Show()
            'Else
            '    Dim childform As Integer = 0
            '    Dim childforms(4) As CooperativeReports
            '    childform = 1
            '    childforms(childform) = New CooperativeReports()
            '    childforms(childform).Text = "Savings Account"
            '    Dim con As New clsPersonnel
            '    Dim appRdr As New System.Configuration.AppSettingsReader
            '    Dim myconnection As New Clsappconfiguration
            '    Dim objreport As New CrystalDecisions.CrystalReports.Engine.ReportDocument
            '    objreport.Load(Application.StartupPath & "\LoanReport\SavingsReportTimeDep.rpt")
            '    Logon(objreport, con.servername, con.databasename, con.username1, con.password1)
            '    objreport.Refresh()
            '    objreport.SetParameterValue("@EmpID", lstmember.SelectedItems(0).SubItems(2).Text)
            '    childforms(childform).appreports.ReportSource = objreport
            '    'childforms(childform).MdiParent =me
            '    childforms(childform).Show()
            'End If
        Catch ex As Exception
            MessageBox.Show("Supply the given Parameter", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
        'MessageBox.Show("Select Member No. on the list below..", "Empty field found", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Try
            'If Me.cbosavtd.SelectedItem = "Savings" Then

            Dim childform As Integer = 0
            Dim childforms(4) As CooperativeReports
            childform = 1
            childforms(childform) = New CooperativeReports()
            childforms(childform).Text = "Savings Account"
            Dim con As New clsPersonnel
            Dim appRdr As New System.Configuration.AppSettingsReader
            Dim myconnection As New Clsappconfiguration
            Dim objreport As New CrystalDecisions.CrystalReports.Engine.ReportDocument
            objreport.Load(Application.StartupPath & "\LoanReport\SavingsReportAll.rpt")
            Logon(objreport, con.servername, con.databasename, con.username1, con.password1)
            objreport.Refresh()
            'objreport.SetParameterValue("@EmpID", lstmember.SelectedItems(0).SubItems(2).Text)

            childforms(childform).appreports.ReportSource = objreport
            'childforms(childform).MdiParent =me
            childforms(childform).Show()

            'Else

            '    Dim childform As Integer = 0
            '    Dim childforms(4) As CooperativeReports
            '    childform = 1
            '    childforms(childform) = New CooperativeReports()
            '    childforms(childform).Text = "Savings Account"
            '    Dim con As New clsPersonnel
            '    Dim appRdr As New System.Configuration.AppSettingsReader
            '    Dim myconnection As New Clsappconfiguration
            '    Dim objreport As New CrystalDecisions.CrystalReports.Engine.ReportDocument
            '    objreport.Load(Application.StartupPath & "\LoanReport\SavingsReportTimeDepAll.rpt")
            '    Logon(objreport, con.servername, con.databasename, con.username1, con.password1)
            '    objreport.Refresh()
            '    'objreport.SetParameterValue("@EmpID", lstmember.SelectedItems(0).SubItems(2).Text)

            '    childforms(childform).appreports.ReportSource = objreport
            '    'childforms(childform).MdiParent =me
            '    childforms(childform).Show()
            'End If
        Catch ex As Exception
            'MessageBox.Show("Supply the given Parameter", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try

        'MessageBox.Show("Select Member No. on the list below..", "Empty field found", MessageBoxButtons.OK, MessageBoxIcon.Information)

    End Sub
#End Region

    Private Sub cboAccountName_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAccountName.SelectedIndexChanged
        On Error Resume Next
        EmployeeListByTypeandName(cbosavtd.Text, cboAccountName.Text)
    End Sub

    Private Sub EmployeeListByTypeandName(ByVal type As String, ByVal Name As String)
        Dim gCon As New Clsappconfiguration
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "CIMS_LoanManager_GetMemberList_Savings_and_TD_ByTypeAndAccountName",
                                                          New SqlParameter("@fcAccountType", type),
                                                          New SqlParameter("@fcAccountName", Name))
        Dim lv As ListViewItem
        With Me.lstmember
            .Clear()
            .View = View.Details
            .GridLines = True
            .FullRowSelect = True
            .Columns.Add("Reference No.", 100, HorizontalAlignment.Left)
            .Columns.Add("Member's FullName", 200, HorizontalAlignment.Left)
            .Columns.Add("pk_Employee", 0, HorizontalAlignment.Left)
            .Columns.Add("as of", 0, HorizontalAlignment.Left)
            .Columns.Add("Employee ID", 0, HorizontalAlignment.Left)
            Try
                While rd.Read
                    lv = New ListViewItem(rd.Item(0).ToString()) ' New ListViewItem(rd.Item(1)) '--(rd.Item(0).ToString()) '
                    With lv
                        .Tag = rd.Item("pk_Employee").ToString()
                        .SubItems.Add(rd.Item(1).ToString())
                        .SubItems.Add(rd.Item(2).ToString())
                        .SubItems.Add(rd.Item(3).ToString())
                        .SubItems.Add(rd.Item(4).ToString())
                    End With
                    .Items.Add(lv)
                    .TopItem.Focused = True
                End While
                rd.Close()
                gCon.sqlconn.Close()
            Catch ex As Exception
                'MessageBox.Show(ex.Message, "Load Member list")
            End Try
        End With
    End Sub

End Class