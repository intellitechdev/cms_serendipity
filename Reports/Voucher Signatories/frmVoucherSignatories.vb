﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Imports System.Threading

Public Class frmVoucherSignatories

    Private gCon As New Clsappconfiguration()

    Private Sub loadSignatories()
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "spu_LoanSignatories_Load",
                                                          New SqlParameter("@Company", frmLoan_ApplicationMain.cboProject.Text))
        While rd.Read = True
            txtName.Text = rd.Item(0).ToString
            txtName2.Text = rd.Item(1).ToString
            txtname3.Text = rd.Item(2).ToString
            txtName4.Text = rd.Item(3).ToString
        End While
    End Sub

    Private Sub frmSignatories_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        loadSignatories()
    End Sub

    Private Sub btnSave_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "spu_LoanSignatories_Insert", _
            New SqlParameter("@fcName", txtName.Text), _
            New SqlParameter("@fcName2", txtName2.Text), _
            New SqlParameter("@fcName3", txtname3.Text), _
            New SqlParameter("@fcName4", txtName4.Text), _
            New SqlParameter("@fcBranch", frmLoan_ApplicationMain.cboProject.Text))
        MsgBox("Saved.", MsgBoxStyle.Information, "Signatories")
        Me.Close()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub
End Class