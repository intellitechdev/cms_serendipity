Imports Microsoft.ApplicationBlocks.Data
Public Class frmCAPRViewer

#Region "Declaration"
    Private gCon As New Clsappconfiguration
    Private rpts1stPage As New ReportDocument
    Private rpts2ndPage As New ReportDocument
    Private rpts3rdPage As New ReportDocument
    Private rpts5thPage As New ReportDocument
#End Region

#Region "Functions and Subs"

    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If

        Dim obj As ReportObject

        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function
    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        ' for each table apply connection info
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            ' check if logon was successful
            ' if TestConnectivity returns false,
            ' check logon credentials
            If (tbl.TestConnectivity()) Then
                'drop fully qualified table location
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next

        Return True
    End Function
    Public Sub ShowError(ByVal FunctionOrSubName As String, ByVal ModuleName As Form, ByVal ErrorMsg As String)
        MsgBox("Error at " & FunctionOrSubName & " in " & ModuleName.Name.ToString & "." & vbCr & vbCr & ErrorMsg, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error occur!")
    End Sub
    Private Sub Load1stPage()
        Try
            rpts1stPage.Load(Application.StartupPath & "\LoanReport\CAPR1stPage.rpt")
            Logon(rpts1stPage, gCon.Server, gCon.Database, gCon.Username, gCon.Password)
            rpts1stPage.Refresh()
            rpts1stPage.SetParameterValue("@ReceivedBy", ReceivedBy)
            rpts1stPage.SetParameterValue("@Validateby", ValidateBy)
            rpts1stPage.SetParameterValue("@Encodedby", EncodedBy)
            rpts1stPage.SetParameterValue("@Verifiedby", Verifiedby)
            rpts1stPage.SetParameterValue("@DateReceive", DateReceived)
            rpts1stPage.SetParameterValue("@DateValidate", DateValidated)
            rpts1stPage.SetParameterValue("@DateEncoded", DateEncoded)
            rpts1stPage.SetParameterValue("@DateVerified", DateVerified)
            rpts1stPage.SetParameterValue("@AsOfYear", AsOfDateYear)
            rpts1stPage.SetParameterValue("@CoopType", CoopType)
            rpts1stPage.SetParameterValue("@BussActEngaged", BussActEngaged)
            rpts1stPage.SetParameterValue("@Phone", PhoneNo)
            rpts1stPage.SetParameterValue("@ContactName", ContactPerson)
            rpts1stPage.SetParameterValue("@Fax", FAX)
            rpts1stPage.SetParameterValue("@Email", Email)
            rpts1stPage.SetParameterValue("@CIN", CINumber)
            rpts1stPage.SetParameterValue("@CompanyName", CoopNameLatestAmendment)
            rpts1stPage.SetParameterValue("@RegConfrmNo", RegConfirmNo)
            rpts1stPage.SetParameterValue("@companyAddress", PresentCoopAddress)
            rpts1stPage.SetParameterValue("@Financial", Box3FinancialInterm)
            rpts1stPage.SetParameterValue("@Mining", Box3Mining)
            rpts1stPage.SetParameterValue("@Storage", Box3TransStorageComm)
            rpts1stPage.SetParameterValue("@Estate", Box3RealEstateRentBussAct)
            rpts1stPage.SetParameterValue("@Construction", Box3Construction)
            rpts1stPage.SetParameterValue("@Health", Box3HealthSocialWork)
            rpts1stPage.SetParameterValue("@Fishing", Box3Fishing)
            rpts1stPage.SetParameterValue("@Electricity", Box3ElecGasWater)
            rpts1stPage.SetParameterValue("@Education", Box3Education)
            rpts1stPage.SetParameterValue("@Agri", Box3AgriHuntForst)
            rpts1stPage.SetParameterValue("@Manufac", Box3Manufctring)
            rpts1stPage.SetParameterValue("@Hotel", Box3HotelResto)
            rpts1stPage.SetParameterValue("@WholeSale", Box3WholeSaleRetail)
            rpts1stPage.SetParameterValue("@Others", Box3Others)
            rpts1stPage.SetParameterValue("@OthersSpecified", Box3OtherSPecified)
        Catch ex As Exception
            ShowError("Load1stPage", Me, ex.Message)
        End Try
    End Sub
    Private Sub Load2ndAnd3rdPage()
        Try
            With rpts2ndPage
                .Load(Application.StartupPath & "\LoanReport\CAPR2ndPage.rpt")
                Logon(rpts2ndPage, gCon.Server, gCon.Database, gCon.Username, gCon.Password)
                .Refresh()
                .SetParameterValue("@TargetMale", IHTargetMale)
                .SetParameterValue("@TargetFemale", IHTargeFemale)
                .SetParameterValue("@I1a", Ii1a)
                .SetParameterValue("@I1b", Ii1b)
                .SetParameterValue("@I1bIf", I1bif)
                .SetParameterValue("@I1c", Ii1c)
                .SetParameterValue("@I1d", Ii1d)
                .SetParameterValue("@I2a", Ii2a)
                .SetParameterValue("@I2b", Ii2b)
                .SetParameterValue("@I2c", Ii2c)
                .SetParameterValue("@I2d", Ii2d)
                .SetParameterValue("@I3a", Ii3a)
                .SetParameterValue("@I4a", Ii4a)
                .SetParameterValue("@I4bi", Ii4bi)
                .SetParameterValue("@I4bii", Ii4bii)
                .SetParameterValue("@I4biii", Ii4biii)
                .SetParameterValue("@I4c", Ii5a)
                .SetParameterValue("@I4d", Ii4d)
                .SetParameterValue("@I5a", Ii5a)
                .SetParameterValue("@I5b", Ii5b)
                .SetParameterValue("@I5c", Ii5c)
                .SetParameterValue("@I5d", Ii5d)
                .SetParameterValue("@I5e", Ii5e)
                .SetParameterValue("@I6a", Ii6a)
                .SetParameterValue("@I6b", Ii6b)
                .SetParameterValue("@I7a", Ii7a)
                .SetParameterValue("@I7aIf", Ii7aIf)
                .SetParameterValue("@I7b", Ii7b)
                .SetParameterValue("@I7c", Ii7c)
                .SetParameterValue("@I7d", Ii7d)
                .SetParameterValue("@I7e", Ii7e)
                .SetParameterValue("@CIN", CINumber)
            End With
        Catch ex As Exception
            ShowError("Load2ndAnd3rdPage", Me, ex.Message)
        End Try

        Try
            With rpts3rdPage
                .Load(Application.StartupPath & "\LoanReport\CAPR2ndPageb.rpt")
                Logon(rpts3rdPage, gCon.Server, gCon.Database, gCon.Username, gCon.Password)
                .Refresh()
                .SetParameterValue("@CIN", CINumber)
                .SetParameterValue("@8a", Ii1a)
                .SetParameterValue("@8b", Ii1b)
                .SetParameterValue("@8c", I1bif)
                .SetParameterValue("@8d", Ii1c)
                .SetParameterValue("@8ei", Ii1d)
                .SetParameterValue("@8eii", Ii2a)
                .SetParameterValue("@8eiii", Ii2b)
                .SetParameterValue("@8fi", Ii2c)
                .SetParameterValue("@8fii", Ii2d)
                .SetParameterValue("@8fiii", Ii3a)
                .SetParameterValue("@8fiv", Ii4a)
                .SetParameterValue("@8fv", Ii4bi)
                .SetParameterValue("@9a", Ii4bii)
                .SetParameterValue("@9b", Ii4biii)
                .SetParameterValue("@9c", Ii5a)
                .SetParameterValue("@9d", Ii4d)
                .SetParameterValue("@9e", Ii5a)
                .SetParameterValue("@9f", Ii5b)
                .SetParameterValue("@9g", Ii5c)
                .SetParameterValue("@9h", Ii5d)
                .SetParameterValue("@9i", Ii5e)
                .SetParameterValue("@9j", Ii6a)
                .SetParameterValue("@9k", Ii6b)
                .SetParameterValue("@10a", Ii7a)
                .SetParameterValue("@10b", Ii7aIf)
                .SetParameterValue("@10c", Ii7b)
                .SetParameterValue("@11", Ii7c)
                .SetParameterValue("@J1a", Ij1a)
                .SetParameterValue("@J1b", Ij1b)
                .SetParameterValue("@J1c", Ij1cName)
                .SetParameterValue("@J1ci", Ij1cDesignation)
                .SetParameterValue("@J2a", Ij2a)
                .SetParameterValue("@J2b", Ij2b)
                .SetParameterValue("@J2c", Ij2c)
                .SetParameterValue("@J3", Ij3)
            End With
        Catch ex As Exception
            ShowError("Load2ndAnd3rdPage", Me, ex.Message)
        End Try
    End Sub
    Private Sub Load5thPage()
        Try
            rpts5thPage.Load(Application.StartupPath & "\LoanReport\CAPR5Page.rpt")
            Logon(rpts5thPage, gCon.Server, gCon.Database, gCon.Username, gCon.Password)
            rpts5thPage.Refresh()

            rpts5thPage.SetParameterValue("@CIN", CINumber)


            With rpts5thPage 'tem("Subreport2")
                .SetParameterValue("@LongTerm1", VLongTerm1, "SubCAPRIIIB.rpt")
                .SetParameterValue("@LongTerm2", VLongTerm2, "SubCAPRIIIB.rpt")
                .SetParameterValue("@LongTerm3", VLongTerm3, "SubCAPRIIIB.rpt")
                .SetParameterValue("@InvestCoopFed1", VInvst1, "SubCAPRIIIB.rpt")
                .SetParameterValue("@InvestCoopFed2", VInvst2, "SubCAPRIIIB.rpt")
                .SetParameterValue("@InvestCoopFed3", VInvst3, "SubCAPRIIIB.rpt")
                .SetParameterValue("@GovernSec1", VGovernSec1, "SubCAPRIIIB.rpt")
                .SetParameterValue("@GovernSec2", VGovernSec2, "SubCAPRIIIB.rpt")
                .SetParameterValue("@GovernSec3", VGovernSec3, "SubCAPRIIIB.rpt")
                .SetParameterValue("@ColType", VBColType, "SubCAPRIIIB.rpt")
                .SetParameterValue("@ColAmoun1", VBAmnt1, "SubCAPRIIIB.rpt")
                .SetParameterValue("@ColAmoun2", VBAmnt2, "SubCAPRIIIB.rpt")
                .SetParameterValue("@ColAmoun3", VBAmnt3, "SubCAPRIIIB.rpt")

                .SetParameterValue("@AmtForCurrcy", VCForeignAmnt, "SubCAPRIIIc.rpt")
                .SetParameterValue("@ConversionRate", VCConvRte, "SubCAPRIIIc.rpt")
                .SetParameterValue("@AmtLocCurrncy", VCLocCurAmt, "SubCAPRIIIc.rpt")
                .SetParameterValue("@GovFinIns", VCGovFinInst, "SubCAPRIIIc.rpt")
                .SetParameterValue("@PrivateFinIns", VCPrivFinIns, "SubCAPRIIIc.rpt")
                .SetParameterValue("@CreditSource", VCCreditSource, "SubCAPRIIIc.rpt")
                .SetParameterValue("@Amount", VCAmnt, "SubCAPRIIIc.rpt")

                .SetParameterValue("@DepositType", VDDepositType, "SubCAPRIIId.rpt")
                .SetParameterValue("@RegNoMemWthDepAcnt", VDRegNoMemWthDepAcnt, "SubCAPRIIId.rpt")
                .SetParameterValue("@RegNoAcnt", VDRegNoAcnt, "SubCAPRIIId.rpt")
                .SetParameterValue("@RegTotal", VDRegTotal, "SubCAPRIIId.rpt")
                .SetParameterValue("@AssNoMemWthDepAcnt", VDAssNoMemWthDepAcnt, "SubCAPRIIId.rpt")
                .SetParameterValue("@AssNoAcnt", VDAssNoAcnt, "SubCAPRIIId.rpt")
                .SetParameterValue("@AssTotal", VDAssTotal, "SubCAPRIIId.rpt")
                .SetParameterValue("@Withdwble1", VDWithdwble1, "SubCAPRIIId.rpt")
                .SetParameterValue("@Withdwble2", VDWithdwble2, "SubCAPRIIId.rpt")
                .SetParameterValue("@Withdwble3", VDWithdwble3, "SubCAPRIIId.rpt")
                .SetParameterValue("@Withdwble4", VDWithdwble4, "SubCAPRIIId.rpt")
                .SetParameterValue("@Withdwble5", VDWithdwble5, "SubCAPRIIId.rpt")
                .SetParameterValue("@Withdwble6", VDWithdwble6, "SubCAPRIIId.rpt")
                .SetParameterValue("@30days1", VD30days1, "SubCAPRIIId.rpt")
                .SetParameterValue("@30days2", VD30days2, "SubCAPRIIId.rpt")
                .SetParameterValue("@30days3", VD30days3, "SubCAPRIIId.rpt")
                .SetParameterValue("@30days4", VD30days4, "SubCAPRIIId.rpt")
                .SetParameterValue("@30days5", VD30days5, "SubCAPRIIId.rpt")
                .SetParameterValue("@30days6", VD30days6, "SubCAPRIIId.rpt")
                .SetParameterValue("@30to90days1", VD30to90days1, "SubCAPRIIId.rpt")
                .SetParameterValue("@30to90days2", VD30to90days2, "SubCAPRIIId.rpt")
                .SetParameterValue("@30to90days3", VD30to90days3, "SubCAPRIIId.rpt")
                .SetParameterValue("@30to90days4", VD30to90days4, "SubCAPRIIId.rpt")
                .SetParameterValue("@30to90days5", VD30to90days5, "SubCAPRIIId.rpt")
                .SetParameterValue("@30to90days6", VD30to90days6, "SubCAPRIIId.rpt")
                .SetParameterValue("@90to1yr1", VD90to1yr1, "SubCAPRIIId.rpt")
                .SetParameterValue("@90to1yr2", VD90to1yr2, "SubCAPRIIId.rpt")
                .SetParameterValue("@90to1yr3", VD90to1yr3, "SubCAPRIIId.rpt")
                .SetParameterValue("@90to1yr4", VD90to1yr4, "SubCAPRIIId.rpt")
                .SetParameterValue("@90to1yr5", VD90to1yr5, "SubCAPRIIId.rpt")
                .SetParameterValue("@90to1yr6", VD90to1yr6, "SubCAPRIIId.rpt")

                .SetParameterValue("@1to5yr1", VD1to5yr1, "SubCAPRIIId.rpt")
                .SetParameterValue("@1to5yr2", VD1to5yr2, "SubCAPRIIId.rpt")
                .SetParameterValue("@1to5yr3", VD1to5yr3, "SubCAPRIIId.rpt")
                .SetParameterValue("@1to5yr4", VD1to5yr4, "SubCAPRIIId.rpt")
                .SetParameterValue("@1to5yr5", VD1to5yr5, "SubCAPRIIId.rpt")
                .SetParameterValue("@1to5yr6", VD1to5yr6, "SubCAPRIIId.rpt")

                .SetParameterValue("@5years1", VD5years1, "SubCAPRIIId.rpt")
                .SetParameterValue("@5years2", VD5years2, "SubCAPRIIId.rpt")
                .SetParameterValue("@5years3", VD5years3, "SubCAPRIIId.rpt")
                .SetParameterValue("@5years4", VD5years4, "SubCAPRIIId.rpt")
                .SetParameterValue("@5years5", VD5years5, "SubCAPRIIId.rpt")
                .SetParameterValue("@5years6", VD5years6, "SubCAPRIIId.rpt")
                .SetParameterValue("@NonWitdrwble1", VDNonWitdrwble1, "SubCAPRIIId.rpt")
                .SetParameterValue("@NonWitdrwble2", VDNonWitdrwble2, "SubCAPRIIId.rpt")
                .SetParameterValue("@NonWitdrwble3", VDNonWitdrwble3, "SubCAPRIIId.rpt")
                .SetParameterValue("@NonWitdrwble4", VDNonWitdrwble4, "SubCAPRIIId.rpt")
                .SetParameterValue("@NonWitdrwble5", VDNonWitdrwble5, "SubCAPRIIId.rpt")
                .SetParameterValue("@NonWitdrwble6", VDNonWitdrwble6, "SubCAPRIIId.rpt")

                .SetParameterValue("@4.1.NoLoan", VI41No)
                .SetParameterValue("@4.2NoLoan", VI42No)
                .SetParameterValue("@4.3NoLoan", VI43No)
                .SetParameterValue("@4.4NoLoan", VI44No)
                .SetParameterValue("@4.5.NoLoan", VI45No)
                .SetParameterValue("@4.1Amnt", VI41Amt)
                .SetParameterValue("@4.2Amnt", VI42Amt)
                .SetParameterValue("@4.3Amnt", VI43Amt)
                .SetParameterValue("@4.4Amnt", VI44Amt)
                .SetParameterValue("@4.5Amnt", VI45Amt)

                .SetParameterValue("@5.1NoLoan", VI51No)
                .SetParameterValue("@5.1Amnt", VI51Amt)
                .SetParameterValue("@5.2NoLoan", VI52No)
                .SetParameterValue("@5.2Amnt", VI52Amt)

                .SetParameterValue("@6.1.1NoLoan", VI611No)
                .SetParameterValue("@6.1.1Amnt", VI611Amt)
                .SetParameterValue("@6.1.2NoLoan", VI612No)
                .SetParameterValue("@6.1.2Amnt", VI612Amt)
                .SetParameterValue("@6.1.3NoLoan", VI613No)
                .SetParameterValue("@6.1.3Amnt", VI613Amt)
                .SetParameterValue("@6.2.1NoLoan", VI621No)
                .SetParameterValue("@6.2.1.Amnt", VI621Amt)
                .SetParameterValue("@6.2.2NoLoan", VI622No)
                .SetParameterValue("@6.2.2Amnt", VI622Amt)

                .SetParameterValue("@7.1Amnt", VII1)
                .SetParameterValue("@7.2Amnt", VII2)
                .SetParameterValue("@7.3Amnt", VII3)
                .SetParameterValue("@7.4Amnt", VII5)
                .SetParameterValue("@7.5Amnt", VII6)

                .SetParameterValue("@8.1.1", VIII1Per)
                .SetParameterValue("@8.2.1", VIII2Per)
                .SetParameterValue("@8.3.1", VIII3Per)
                .SetParameterValue("@8.4.1", VIII4Per)
                .SetParameterValue("@8.5.1", VIII5Per)
                .SetParameterValue("@8.1.2", VIII1Amt)
                .SetParameterValue("@8.2.2", VIII2Amt)
                .SetParameterValue("@8.3.2", VIII3Amt)
                .SetParameterValue("@8.4.2", VIII4Amt)
                .SetParameterValue("@8.5.2", VIII5Amt)
                .SetParameterValue("@8.1.3", VIII1Alloc)
                .SetParameterValue("@8.2.3", VIII2Alloc)
                .SetParameterValue("@8.3.3", VIII3Alloc)
                .SetParameterValue("@8.4.3", VIII4Alloc)
                .SetParameterValue("@8.5.3", VIII5Alloc)
                .SetParameterValue("@8.1.4", VIII1Distrb)
                .SetParameterValue("@8.2.4", VIII2Distrb)
                .SetParameterValue("@8.3.4", VIII3Distrb)
                .SetParameterValue("@8.4.4", VIII4Distrb)
                .SetParameterValue("@8.5.4", VIII5Distrb)
                .SetParameterValue("@8.1.5", VIII1Discrp)
                .SetParameterValue("@8.2.5", VIII2Discrp)
                .SetParameterValue("@8.3.5", VIII3Discrp)
                .SetParameterValue("@8.4.5", VIII4Discrp)
                .SetParameterValue("@8.5.5", VIII5Discrp)

                .SetParameterValue("@IV1.1", IV11)
                .SetParameterValue("@IV1.2", IV12)
                .SetParameterValue("@IV1.3", IV13)
                .SetParameterValue("@IV1.4", IV14)
                .SetParameterValue("@IV1.5", IV15)
                .SetParameterValue("@IV1.6", IV16)
                .SetParameterValue("@IV1.7", IV17)
                .SetParameterValue("@IV1.8", IV18)
                .SetParameterValue("@IV1.9", IV19)
                .SetParameterValue("@IV1.10", IV110)
                .SetParameterValue("@IV1.11", IV111)
                .SetParameterValue("@IV1.12", IV112)
                .SetParameterValue("@IV1.13", IV113)
                .SetParameterValue("@IV1.14", IV114)
                .SetParameterValue("@IV1.15", IV115)
                .SetParameterValue("@IV1.16", IV116)
                .SetParameterValue("@IV1.17", IV117)
                .SetParameterValue("@IV1.18", IV118)
                .SetParameterValue("@IV1.19", IV119)
                .SetParameterValue("@IV1.20", IV120)
                .SetParameterValue("@IV1.21", IV121)

                .SetParameterValue("@IV2.1", IV21)
                .SetParameterValue("@IV2.2", IV22)
                .SetParameterValue("@IV2.3", IV23)
                .SetParameterValue("@IV2.4", IV24)
                .SetParameterValue("@IV2.5", IV25)
                .SetParameterValue("@IV2.6", IV26)
                .SetParameterValue("@IV2.7", IV27)
                .SetParameterValue("@IV2.8", IV28)
                .SetParameterValue("@IV2.9", IV29)
                .SetParameterValue("@IV2.10", IV210)
                .SetParameterValue("@IV2.11", IV211)
                .SetParameterValue("@IV2.12", IV212)
                .SetParameterValue("@IV2.13", IV213)
                .SetParameterValue("@IV2.14", IV214)
                .SetParameterValue("@IV2.15", IV215)
                .SetParameterValue("@IV2.16", IV216)
                .SetParameterValue("@IV2.17", IV217)
                .SetParameterValue("@IV2.18", IV218)
                .SetParameterValue("@IV2.19", IV219)
                .SetParameterValue("@IV2.20", IV220)
                .SetParameterValue("@IV2.21", IV221)

                .SetParameterValue("@IV3.1", IV31)
                .SetParameterValue("@IV3.2", IV32)
                .SetParameterValue("@IV3.3", IV33)
                .SetParameterValue("@IV3.4", IV34)
                .SetParameterValue("@IV3.5", IV35)
                .SetParameterValue("@IV3.6", IV36)
                .SetParameterValue("@IV3.7", IV37)
                .SetParameterValue("@IV3.8", IV38)
                .SetParameterValue("@IV3.9", IV39)
                .SetParameterValue("@IV3.10", IV310)
                .SetParameterValue("@IV3.11", IV311)
                .SetParameterValue("@IV3.12", IV312)
                .SetParameterValue("@IV3.13", IV313)
                .SetParameterValue("@IV3.14", IV314)
                .SetParameterValue("@IV3.15", IV315)
                .SetParameterValue("@IV3.16", IV316)
                .SetParameterValue("@IV3.17", IV317)
                .SetParameterValue("@IV3.18", IV318)
                .SetParameterValue("@IV3.19", IV319)
                .SetParameterValue("@IV3.20", IV320)
                .SetParameterValue("@IV3.21", IV321)

                .SetParameterValue("@Preparedby", PreparedBy)
                .SetParameterValue("@Certified", CertifiedTrue)
                .SetParameterValue("@AccountantCompliance", Accoountant)
                .SetParameterValue("@BODGeneral", BOD)
            End With
        Catch ex As Exception
            ShowError("Load5thPage", Me, ex.Message)
        End Try
    End Sub

#End Region

    Private Sub BGWorker1stPage_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BGWorker1stPage.DoWork
        Load1stPage()
    End Sub

    Private Sub BGWorker1stPage_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BGWorker1stPage.RunWorkerCompleted
        CRVRpt.Visible = True
        CRVRpt.ReportSource = rpts1stPage
        Label1.Visible = False
    End Sub

    Private Sub BGWorker2nd3rdPages_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BGWorker2nd3rdPages.DoWork
        Load2ndAnd3rdPage()
    End Sub

    Private Sub BGWorker2nd3rdPages_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BGWorker2nd3rdPages.RunWorkerCompleted
        CrvRpt2.Visible = True
        CrvRpt2.ReportSource = rpts2ndPage
        CrvRpt3.Visible = True
        CrvRpt3.ReportSource = rpts3rdPage
        Label2.Visible = False
        Label3.Visible = False
    End Sub

    Public Sub BGWorker5thPage_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BGWorker5thPage.DoWork
        Load5thPage()
    End Sub

    Private Sub BGWorker5thPage_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BGWorker5thPage.RunWorkerCompleted
        CrvRpt5.Visible = True
        CrvRpt5.ReportSource = rpts5thPage
        Label5.Visible = False
    End Sub

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        CRVRpt.PrintReport()
        CrvRpt2.PrintReport()
        CrvRpt2.PrintReport()
        CrvRpt3.PrintReport()
        CrvRpt5.PrintReport()
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        If BGWorker1stPage.IsBusy = False And BGWorker2nd3rdPages.IsBusy = False And BGWorker5thPage.IsBusy = False Then
            btnPrint.Enabled = True
        Else
            btnPrint.Enabled = False
        End If
    End Sub

    Private Sub BtnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnBack.Click
        If BGWorker1stPage.IsBusy = True Then
            BGWorker1stPage.CancelAsync()
        End If
        If BGWorker2nd3rdPages.IsBusy = True Then
            BGWorker2nd3rdPages.CancelAsync()
        End If
        If BGWorker5thPage.IsBusy = True Then
            BGWorker5thPage.CancelAsync()
        End If
        Me.Hide()
        xFormSec4.Show()
    End Sub

    Private Sub frmCAPRViewer_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.VisibleChanged
        If Visible = True Then
            Label1.Visible = True
            Label2.Visible = True
            Label3.Visible = True
            Label5.Visible = True

            BGWorker1stPage.RunWorkerAsync()
            BGWorker2nd3rdPages.RunWorkerAsync()
            BGWorker5thPage.RunWorkerAsync()
            Me.WindowState = FormWindowState.Maximized
        End If
    End Sub
End Class