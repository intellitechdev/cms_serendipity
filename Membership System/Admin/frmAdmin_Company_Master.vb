Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmAdmin_Company_Master
#Region "Variables"
    Public compname As String

#End Region
#Region "Get property"
    Public Property getcompname() As String
        Get
            Return compname
        End Get
        Set(ByVal value As String)
            compname = value
        End Set
    End Property
#End Region
    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If btnEdit.Text = "Edit" Then
            Me.txtcompanyname.Focus()
            Call readonlytxtfalse()
            BTnclose.Text = "Cancel"
            btnEdit.Text = "Update"
            Exit Sub
        End If
        If btnEdit.Text = "Update" Then

            Call readonlytxttrue()
            Call checkforid()
            frmMain.lblCompanyTitle.Text = Me.txtcompanyname.Text
            BTnclose.Text = "Close"
            btnEdit.Text = "Edit"
            Me.Close()
        End If
    End Sub
#Region "Chech if recid is exist"
    Private Sub checkforid()
        If Me.txtrecid.Text <> "" Then
            Call updatecompanyinfo()
        Else
            Call companyinfoinsert()
        End If
    End Sub
#End Region
#Region "Readonly txt true"
    Private Sub readonlytxttrue()
        With Me
            .btnaddcompanypicture.Enabled = False
            .txtcompanyname.ReadOnly = True
            .txtcompanyadd1.ReadOnly = True
            .txtcompanyadd2.ReadOnly = True
            .txtcompanyadd3.ReadOnly = True
            .txtcompanyid.ReadOnly = True
            .txtcompanycode.ReadOnly = True
            .txtcompanybranch.ReadOnly = True
            .txtcompanybankcode.ReadOnly = True
            .txtbankbrankcode.ReadOnly = True
            .txtbankname.ReadOnly = True
            .txtcompanyacctno.ReadOnly = True
            .txtphilhealth.ReadOnly = True
            .txtsss.ReadOnly = True
            .txtTIN.ReadOnly = True
            .txtbankacctno.ReadOnly = True
            .txtfaxno.ReadOnly = True
            .txtTelno.ReadOnly = True
            .txtzipcode.ReadOnly = True
        End With
    End Sub
#End Region
#Region "Readonly txt false"
    Private Sub readonlytxtfalse()
        With Me
            .btnaddcompanypicture.Enabled = True
            .txtcompanyname.ReadOnly = False
            .txtcompanyadd1.ReadOnly = False
            .txtcompanyadd2.ReadOnly = False
            .txtcompanyadd3.ReadOnly = False
            .txtcompanyid.ReadOnly = False
            .txtcompanycode.ReadOnly = False
            .txtcompanybranch.ReadOnly = False
            .txtcompanybankcode.ReadOnly = False
            .txtbankbrankcode.ReadOnly = False
            .txtbankname.ReadOnly = False
            .txtcompanyacctno.ReadOnly = False
            .txtphilhealth.ReadOnly = False
            .txtsss.ReadOnly = False
            .txtTIN.ReadOnly = False
            .txtbankacctno.ReadOnly = False
            .txtfaxno.ReadOnly = False
            .txtTelno.ReadOnly = False
            .txtzipcode.ReadOnly = False
        End With
    End Sub
#End Region
#Region "Company Information Insert"
    Private Sub companyinfoinsert()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_company_information_insert", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        Try
            With cmd.Parameters
                .Add("@CompanyName", SqlDbType.VarChar, 100).Value = Me.txtcompanyname.Text
                .Add("@CompanyAdd1", SqlDbType.VarChar, 255).Value = Me.txtcompanyadd1.Text
                .Add("@CompanyAdd2", SqlDbType.VarChar, 255).Value = Me.txtcompanyadd2.Text
                .Add("@CompanyAdd3", SqlDbType.VarChar, 255).Value = Me.txtcompanyadd3.Text
                .Add("@CompanyID", SqlDbType.VarChar, 50).Value = Me.txtcompanyid.Text
                .Add("@CompanyCode", SqlDbType.VarChar, 50).Value = Me.txtcompanycode.Text
                .Add("@CompanyBranch", SqlDbType.VarChar, 255).Value = Me.txtcompanybranch.Text
                .Add("@CompanyBankCode", SqlDbType.VarChar, 50).Value = Me.txtcompanybankcode.Text
                .Add("@BankBranchCode", SqlDbType.VarChar, 50).Value = Me.txtbankbrankcode.Text
                .Add("@BankName", SqlDbType.VarChar, 150).Value = Me.txtbankname.Text
                .Add("@CompanyAcctno", SqlDbType.VarChar, 50).Value = Me.txtcompanyacctno.Text
                .Add("@PhilHealth", SqlDbType.VarChar, 50).Value = Me.txtphilhealth.Text
                .Add("@SSS", SqlDbType.VarChar, 50).Value = Me.txtsss.Text
                .Add("@TIN", SqlDbType.VarChar, 50).Value = Me.txtTIN.Text
                .Add("@Batchnum", SqlDbType.VarChar, 50).Value = Me.txtbankacctno.Text
                .Add("@Faxnum", SqlDbType.Char, 20).Value = Me.txtfaxno.Text
                .Add("@Telnum", SqlDbType.Char, 20).Value = Me.txtTelno.Text
                .Add("@Zipcode", SqlDbType.Char, 20).Value = Me.txtzipcode.Text
                .Add("@PicturePath", SqlDbType.Char, 255).Value = Me.txtpicturepath.Text
            End With
            myconnection.sqlconn.Open()
            cmd.ExecuteNonQuery()
            myconnection.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show("usp_per_company_information_insert", "insert failed", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
#End Region
#Region "Update Company Information"
    Private Sub updatecompanyinfo()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_company_information_update", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        Try
            With cmd.Parameters
                .Add("@recid", SqlDbType.Int).Value = Me.txtrecid.Text
                .Add("@CompanyName", SqlDbType.VarChar, 100).Value = Me.txtcompanyname.Text
                .Add("@CompanyAdd1", SqlDbType.VarChar, 255).Value = Me.txtcompanyadd1.Text
                .Add("@CompanyAdd2", SqlDbType.VarChar, 255).Value = Me.txtcompanyadd2.Text
                .Add("@CompanyAdd3", SqlDbType.VarChar, 255).Value = Me.txtcompanyadd3.Text
                .Add("@CompanyID", SqlDbType.VarChar, 50).Value = Me.txtcompanyid.Text
                .Add("@CompanyCode", SqlDbType.VarChar, 50).Value = Me.txtcompanycode.Text
                .Add("@CompanyBranch", SqlDbType.VarChar, 255).Value = Me.txtcompanybranch.Text
                .Add("@CompanyBankCode", SqlDbType.VarChar, 50).Value = Me.txtcompanybankcode.Text
                .Add("@BankBranchCode", SqlDbType.VarChar, 50).Value = Me.txtbankbrankcode.Text
                .Add("@BankName", SqlDbType.VarChar, 150).Value = Me.txtbankname.Text
                .Add("@CompanyAcctno", SqlDbType.VarChar, 50).Value = Me.txtcompanyacctno.Text
                .Add("@PhilHealth", SqlDbType.VarChar, 50).Value = Me.txtphilhealth.Text
                .Add("@SSS", SqlDbType.VarChar, 50).Value = Me.txtsss.Text
                .Add("@TIN", SqlDbType.VarChar, 50).Value = Me.txtTIN.Text
                .Add("@Batchnum", SqlDbType.VarChar, 50).Value = Me.txtbankacctno.Text
                .Add("@Faxnum", SqlDbType.Char, 20).Value = Me.txtfaxno.Text
                .Add("@Telnum", SqlDbType.Char, 20).Value = Me.txtTelno.Text
                .Add("@Zipcode", SqlDbType.Char, 20).Value = Me.txtzipcode.Text
                .Add("@PicturePath", SqlDbType.Char, 255).Value = Me.txtpicturepath.Text
            End With
            myconnection.sqlconn.Open()
            cmd.ExecuteNonQuery()
            myconnection.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show("usp_per_company_information_update", "Update failed", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
#End Region
    Private Sub frmCompany_Information_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call readonlytxttrue()
        Call loadrecordid()
    End Sub
#Region "Load Record ID"
    Private Sub loadrecordid()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_company_information_readid", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        'cmd.Parameters.Add("@recid", SqlDbType.Int).Value = Integer.Parse(Me.txtrecid.Text)
        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader
        Try
            While myreader.Read
                With Me
                    .txtrecid.Text = myreader.Item(0)
                    .txtcompanyname.Text = myreader.Item(1)
                    .txtcompanyadd1.Text = myreader.Item(2)
                    .txtcompanyadd2.Text = myreader.Item(3)
                    .txtcompanyadd3.Text = myreader.Item(4)
                    .txtcompanyid.Text = myreader.Item(5)
                    .txtcompanycode.Text = myreader.Item(6)
                    .txtcompanybranch.Text = myreader.Item(7)
                    .txtcompanybankcode.Text = myreader.Item(8)
                    .txtbankbrankcode.Text = myreader.Item(9)
                    .txtbankname.Text = myreader.Item(10)
                    .txtcompanyacctno.Text = myreader.Item(11)
                    .txtphilhealth.Text = myreader.Item(12)
                    .txtsss.Text = myreader.Item(13)
                    .txtTIN.Text = myreader.Item(14)
                    .txtbankacctno.Text = myreader.Item(15)
                    .txtfaxno.Text = myreader.Item(16)
                    .txtTelno.Text = myreader.Item(17)
                    .txtzipcode.Text = myreader.Item(18)
                    .txtpicturepath.Text = myreader.Item(19)
                End With
            End While
        Finally
            myreader.Close()
            myconnection.sqlconn.Close()
        End Try
    End Sub
#End Region

    Private Sub BTnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BTnclose.Click
        If BTnclose.Text = "Cancel" Then
            btnEdit.Text = "Edit"
            BTnclose.Text = "Close"
            Call readonlytxttrue()
        ElseIf BTnclose.Text = "Close" Then
            Me.Close()
        End If

    End Sub

    Private Sub btnaddcompanypicture_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnaddcompanypicture.Click
        Me.OpenFileDialog1.FileName = ""
        Me.OpenFileDialog1.Filter = "jpg (*.jpg)|*.* |All Files (*.*)|*.*"
        Me.OpenFileDialog1.FilterIndex = 2
        Try
            If Me.OpenFileDialog1.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
                Try
                    Me.txtpicturepath.Text = Me.OpenFileDialog1.FileName
                    CompanyPicture.Image = New Bitmap(OpenFileDialog1.FileName)
                Catch ex As Exception
                    MessageBox.Show("File Format is not valid", "Invalid Format", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                End Try
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub txtpicturepath_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtpicturepath.TextChanged
        'If FileIO.FileSystem.FileExists(Me.txtpicturepath.Text) = False Then
        '    Me.CompanyPicture.ImageLocation = Application.StartupPath & "\SplashScreen v3.jpg"
        '    Me.CompanyPicture.Load()
        'Else
        '    Me.CompanyPicture.ImageLocation = Me.txtpicturepath.Text
        '    Me.CompanyPicture.Load()
        'End If
        'If Me.txtpicturepath.Text <> "" Then
        '    Try
        '        Me.CompanyPicture.ImageLocation = Me.txtpicturepath.Text
        '        Me.CompanyPicture.Load()
        '    Catch ex As Exception
        '    End Try
        'Else
        '    Me.CompanyPicture.ImageLocation = Application.StartupPath & "\SplashScreen v3.jpg"
        '    Me.CompanyPicture.Load()
        'End If
    End Sub

End Class