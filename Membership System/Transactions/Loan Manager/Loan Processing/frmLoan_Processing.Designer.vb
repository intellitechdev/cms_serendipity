<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLoan_Processing
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLoan_Processing))
        Me.GrdLoanSummary = New System.Windows.Forms.DataGridView()
        Me.btnEditAmort = New System.Windows.Forms.Button()
        Me.txtBalance = New System.Windows.Forms.TextBox()
        Me.txtCapitalShare = New System.Windows.Forms.TextBox()
        Me.txtTerms = New System.Windows.Forms.TextBox()
        Me.txtInterest = New System.Windows.Forms.TextBox()
        Me.txtNetProceeds = New System.Windows.Forms.TextBox()
        Me.txtServiceFee = New System.Windows.Forms.TextBox()
        Me.txtInterestRefund = New System.Windows.Forms.TextBox()
        Me.txtPenalty = New System.Windows.Forms.TextBox()
        Me.txtStatus = New System.Windows.Forms.TextBox()
        Me.lblBalance = New System.Windows.Forms.Label()
        Me.lblstatus = New System.Windows.Forms.Label()
        Me.txtPrincipal = New System.Windows.Forms.TextBox()
        Me.lblTerms = New System.Windows.Forms.Label()
        Me.lblCapitalShare = New System.Windows.Forms.Label()
        Me.lblNetProceeds = New System.Windows.Forms.Label()
        Me.txtLoanAppDate = New System.Windows.Forms.TextBox()
        Me.lblInterestRefund = New System.Windows.Forms.Label()
        Me.lblLoanApplicationDate = New System.Windows.Forms.Label()
        Me.lblServiceFee = New System.Windows.Forms.Label()
        Me.lblPenalty = New System.Windows.Forms.Label()
        Me.lblInterest = New System.Windows.Forms.Label()
        Me.lblLoanNumber = New System.Windows.Forms.Label()
        Me.lblPrincipal = New System.Windows.Forms.Label()
        Me.lblClassification = New System.Windows.Forms.Label()
        Me.txtLoanNumber = New System.Windows.Forms.TextBox()
        Me.txtClassification = New System.Windows.Forms.TextBox()
        Me.lblAmortizationDate = New System.Windows.Forms.Label()
        Me.lblAmortization = New System.Windows.Forms.Label()
        Me.txtAmortization = New System.Windows.Forms.TextBox()
        Me.lblTo = New System.Windows.Forms.Label()
        Me.txtothers = New System.Windows.Forms.TextBox()
        Me.txtcompanyloan = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtphihealthloan = New System.Windows.Forms.TextBox()
        Me.txtpagibigloan = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtSSSloan = New System.Windows.Forms.TextBox()
        Me.txtSalaryMonthly = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.cmdCheck = New System.Windows.Forms.Button()
        Me.txtSearchnumber = New System.Windows.Forms.TextBox()
        Me.cmdSearch = New System.Windows.Forms.Button()
        Me.txtSearchLastname = New System.Windows.Forms.TextBox()
        Me.optName = New System.Windows.Forms.RadioButton()
        Me.optNumber = New System.Windows.Forms.RadioButton()
        Me.lstNumber = New System.Windows.Forms.ListBox()
        Me.lstNames = New System.Windows.Forms.ListBox()
        Me.btnview = New System.Windows.Forms.Button()
        Me.btnclose = New System.Windows.Forms.Button()
        Me.lstExistingLoans = New System.Windows.Forms.ListView()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.laonDTto = New System.Windows.Forms.DateTimePicker()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.DTLoan = New System.Windows.Forms.DateTimePicker()
        Me.CboLoanNumber = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.CboLoanType = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.CboLoanStatus = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabMember = New System.Windows.Forms.TabPage()
        Me.TabTransaction = New System.Windows.Forms.TabPage()
        Me.ListStatus = New System.Windows.Forms.ListView()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Btnprint = New System.Windows.Forms.Button()
        Me.panelLoanProcessing = New System.Windows.Forms.Panel()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.PanePanel5 = New WindowsApplication2.PanePanel()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.SplitContainer2 = New System.Windows.Forms.SplitContainer()
        Me.PanePanel2 = New WindowsApplication2.PanePanel()
        Me.lblExpressLoan = New System.Windows.Forms.Label()
        Me.txtmembername = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.tabLoanDetails = New System.Windows.Forms.TabControl()
        Me.loan = New System.Windows.Forms.TabPage()
        Me.pnlLoanDetails = New System.Windows.Forms.Panel()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.txtDNetProceeds = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.txtDInt = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.txtDterms = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.txtdDate = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txtDPrincipal = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txtDLoanNo = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.dgvDeductions = New System.Windows.Forms.DataGridView()
        Me.btnChangeLoanNo = New System.Windows.Forms.Button()
        Me.SplitContainer3 = New System.Windows.Forms.SplitContainer()
        Me.txtPayment = New System.Windows.Forms.TextBox()
        Me.lblPayment = New System.Windows.Forms.Label()
        Me.amortization = New System.Windows.Forms.TabPage()
        Me.dteAmortEnd = New System.Windows.Forms.DateTimePicker()
        Me.dteAmortStart = New System.Windows.Forms.DateTimePicker()
        Me.btnChangeDateEnd = New System.Windows.Forms.Button()
        Me.btnChangeDateStart = New System.Windows.Forms.Button()
        Me.maxLoanAmt = New System.Windows.Forms.TabPage()
        Me.Comaker = New System.Windows.Forms.TabPage()
        Me.dgvComaker = New System.Windows.Forms.DataGridView()
        Me.btnPrintStatement = New System.Windows.Forms.Button()
        Me.btnValidateLoans = New System.Windows.Forms.Button()
        Me.PanePanel3 = New WindowsApplication2.PanePanel()
        Me.lblWhatdouwant = New System.Windows.Forms.Label()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.ViewRequirementToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RequirementsForApprovalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PrintToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LoanApplicationFormToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LoanAgreementToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SearchToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DeleteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CloseToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.panelLoansFilter = New System.Windows.Forms.Panel()
        Me.PanePanel1 = New WindowsApplication2.PanePanel()
        Me.lblLoanSummary = New System.Windows.Forms.Label()
        CType(Me.GrdLoanSummary, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabMember.SuspendLayout()
        Me.TabTransaction.SuspendLayout()
        Me.panelLoanProcessing.SuspendLayout()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.PanePanel5.SuspendLayout()
        Me.SplitContainer2.Panel1.SuspendLayout()
        Me.SplitContainer2.Panel2.SuspendLayout()
        Me.SplitContainer2.SuspendLayout()
        Me.PanePanel2.SuspendLayout()
        Me.tabLoanDetails.SuspendLayout()
        Me.loan.SuspendLayout()
        Me.pnlLoanDetails.SuspendLayout()
        CType(Me.dgvDeductions, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer3.Panel1.SuspendLayout()
        Me.SplitContainer3.Panel2.SuspendLayout()
        Me.SplitContainer3.SuspendLayout()
        Me.amortization.SuspendLayout()
        Me.maxLoanAmt.SuspendLayout()
        Me.Comaker.SuspendLayout()
        CType(Me.dgvComaker, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanePanel3.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        Me.panelLoansFilter.SuspendLayout()
        Me.PanePanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GrdLoanSummary
        '
        Me.GrdLoanSummary.AllowUserToAddRows = False
        Me.GrdLoanSummary.AllowUserToDeleteRows = False
        Me.GrdLoanSummary.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GrdLoanSummary.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.GrdLoanSummary.BackgroundColor = System.Drawing.SystemColors.Window
        Me.GrdLoanSummary.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.GrdLoanSummary.GridColor = System.Drawing.Color.Black
        Me.GrdLoanSummary.Location = New System.Drawing.Point(5, 3)
        Me.GrdLoanSummary.Name = "GrdLoanSummary"
        Me.GrdLoanSummary.ReadOnly = True
        Me.GrdLoanSummary.RowHeadersVisible = False
        Me.GrdLoanSummary.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.GrdLoanSummary.Size = New System.Drawing.Size(732, 276)
        Me.GrdLoanSummary.TabIndex = 21
        '
        'btnEditAmort
        '
        Me.btnEditAmort.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.btnEditAmort.Location = New System.Drawing.Point(255, 70)
        Me.btnEditAmort.Name = "btnEditAmort"
        Me.btnEditAmort.Size = New System.Drawing.Size(124, 22)
        Me.btnEditAmort.TabIndex = 37
        Me.btnEditAmort.Text = "Change Value"
        Me.btnEditAmort.UseVisualStyleBackColor = True
        '
        'txtBalance
        '
        Me.txtBalance.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtBalance.BackColor = System.Drawing.Color.Gainsboro
        Me.txtBalance.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBalance.Location = New System.Drawing.Point(77, 4)
        Me.txtBalance.Name = "txtBalance"
        Me.txtBalance.ReadOnly = True
        Me.txtBalance.Size = New System.Drawing.Size(200, 23)
        Me.txtBalance.TabIndex = 9
        Me.txtBalance.Text = "0"
        Me.txtBalance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtCapitalShare
        '
        Me.txtCapitalShare.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtCapitalShare.BackColor = System.Drawing.Color.Gainsboro
        Me.txtCapitalShare.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCapitalShare.Location = New System.Drawing.Point(113, 120)
        Me.txtCapitalShare.Name = "txtCapitalShare"
        Me.txtCapitalShare.ReadOnly = True
        Me.txtCapitalShare.Size = New System.Drawing.Size(174, 23)
        Me.txtCapitalShare.TabIndex = 5
        Me.txtCapitalShare.Text = "0"
        Me.txtCapitalShare.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTerms
        '
        Me.txtTerms.BackColor = System.Drawing.Color.Gainsboro
        Me.txtTerms.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTerms.Location = New System.Drawing.Point(52, 35)
        Me.txtTerms.Name = "txtTerms"
        Me.txtTerms.ReadOnly = True
        Me.txtTerms.Size = New System.Drawing.Size(66, 23)
        Me.txtTerms.TabIndex = 8
        Me.txtTerms.Text = "0"
        Me.txtTerms.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtInterest
        '
        Me.txtInterest.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtInterest.BackColor = System.Drawing.Color.Gainsboro
        Me.txtInterest.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtInterest.Location = New System.Drawing.Point(113, 62)
        Me.txtInterest.Name = "txtInterest"
        Me.txtInterest.ReadOnly = True
        Me.txtInterest.Size = New System.Drawing.Size(174, 23)
        Me.txtInterest.TabIndex = 3
        Me.txtInterest.Text = "0"
        Me.txtInterest.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtNetProceeds
        '
        Me.txtNetProceeds.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtNetProceeds.BackColor = System.Drawing.Color.Gainsboro
        Me.txtNetProceeds.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNetProceeds.Location = New System.Drawing.Point(113, 120)
        Me.txtNetProceeds.Name = "txtNetProceeds"
        Me.txtNetProceeds.ReadOnly = True
        Me.txtNetProceeds.Size = New System.Drawing.Size(164, 23)
        Me.txtNetProceeds.TabIndex = 7
        Me.txtNetProceeds.Text = "0"
        Me.txtNetProceeds.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtServiceFee
        '
        Me.txtServiceFee.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtServiceFee.BackColor = System.Drawing.Color.Gainsboro
        Me.txtServiceFee.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtServiceFee.Location = New System.Drawing.Point(113, 91)
        Me.txtServiceFee.Name = "txtServiceFee"
        Me.txtServiceFee.ReadOnly = True
        Me.txtServiceFee.Size = New System.Drawing.Size(174, 23)
        Me.txtServiceFee.TabIndex = 4
        Me.txtServiceFee.Text = "0"
        Me.txtServiceFee.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtInterestRefund
        '
        Me.txtInterestRefund.AcceptsTab = True
        Me.txtInterestRefund.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtInterestRefund.BackColor = System.Drawing.Color.Gainsboro
        Me.txtInterestRefund.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtInterestRefund.Location = New System.Drawing.Point(112, 149)
        Me.txtInterestRefund.Name = "txtInterestRefund"
        Me.txtInterestRefund.ReadOnly = True
        Me.txtInterestRefund.Size = New System.Drawing.Size(165, 23)
        Me.txtInterestRefund.TabIndex = 6
        Me.txtInterestRefund.Text = "0"
        Me.txtInterestRefund.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtPenalty
        '
        Me.txtPenalty.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtPenalty.BackColor = System.Drawing.Color.Gainsboro
        Me.txtPenalty.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPenalty.Location = New System.Drawing.Point(113, 149)
        Me.txtPenalty.Name = "txtPenalty"
        Me.txtPenalty.ReadOnly = True
        Me.txtPenalty.Size = New System.Drawing.Size(174, 23)
        Me.txtPenalty.TabIndex = 5
        Me.txtPenalty.Text = "0"
        Me.txtPenalty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtStatus
        '
        Me.txtStatus.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtStatus.BackColor = System.Drawing.Color.Gainsboro
        Me.txtStatus.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtStatus.Location = New System.Drawing.Point(77, 62)
        Me.txtStatus.Name = "txtStatus"
        Me.txtStatus.ReadOnly = True
        Me.txtStatus.Size = New System.Drawing.Size(200, 23)
        Me.txtStatus.TabIndex = 4
        Me.txtStatus.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblBalance
        '
        Me.lblBalance.AutoSize = True
        Me.lblBalance.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBalance.Location = New System.Drawing.Point(9, 8)
        Me.lblBalance.Name = "lblBalance"
        Me.lblBalance.Size = New System.Drawing.Size(61, 19)
        Me.lblBalance.TabIndex = 4
        Me.lblBalance.Text = "Balance"
        '
        'lblstatus
        '
        Me.lblstatus.AutoSize = True
        Me.lblstatus.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblstatus.Location = New System.Drawing.Point(21, 62)
        Me.lblstatus.Name = "lblstatus"
        Me.lblstatus.Size = New System.Drawing.Size(49, 19)
        Me.lblstatus.TabIndex = 3
        Me.lblstatus.Text = "Status"
        '
        'txtPrincipal
        '
        Me.txtPrincipal.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtPrincipal.BackColor = System.Drawing.Color.Gainsboro
        Me.txtPrincipal.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPrincipal.Location = New System.Drawing.Point(113, 33)
        Me.txtPrincipal.Name = "txtPrincipal"
        Me.txtPrincipal.ReadOnly = True
        Me.txtPrincipal.Size = New System.Drawing.Size(174, 23)
        Me.txtPrincipal.TabIndex = 2
        Me.txtPrincipal.Text = "0"
        Me.txtPrincipal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblTerms
        '
        Me.lblTerms.AutoSize = True
        Me.lblTerms.Location = New System.Drawing.Point(6, 39)
        Me.lblTerms.Name = "lblTerms"
        Me.lblTerms.Size = New System.Drawing.Size(40, 13)
        Me.lblTerms.TabIndex = 3
        Me.lblTerms.Text = "Terms:"
        '
        'lblCapitalShare
        '
        Me.lblCapitalShare.AutoSize = True
        Me.lblCapitalShare.Location = New System.Drawing.Point(33, 130)
        Me.lblCapitalShare.Name = "lblCapitalShare"
        Me.lblCapitalShare.Size = New System.Drawing.Size(74, 13)
        Me.lblCapitalShare.TabIndex = 2
        Me.lblCapitalShare.Text = "Capital Share "
        '
        'lblNetProceeds
        '
        Me.lblNetProceeds.AutoSize = True
        Me.lblNetProceeds.Location = New System.Drawing.Point(36, 124)
        Me.lblNetProceeds.Name = "lblNetProceeds"
        Me.lblNetProceeds.Size = New System.Drawing.Size(71, 13)
        Me.lblNetProceeds.TabIndex = 2
        Me.lblNetProceeds.Text = "Net Proceeds"
        '
        'txtLoanAppDate
        '
        Me.txtLoanAppDate.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtLoanAppDate.BackColor = System.Drawing.Color.Gainsboro
        Me.txtLoanAppDate.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLoanAppDate.Location = New System.Drawing.Point(113, 91)
        Me.txtLoanAppDate.Name = "txtLoanAppDate"
        Me.txtLoanAppDate.ReadOnly = True
        Me.txtLoanAppDate.Size = New System.Drawing.Size(164, 23)
        Me.txtLoanAppDate.TabIndex = 2
        Me.txtLoanAppDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblInterestRefund
        '
        Me.lblInterestRefund.AutoSize = True
        Me.lblInterestRefund.Location = New System.Drawing.Point(23, 153)
        Me.lblInterestRefund.Name = "lblInterestRefund"
        Me.lblInterestRefund.Size = New System.Drawing.Size(84, 13)
        Me.lblInterestRefund.TabIndex = 1
        Me.lblInterestRefund.Text = "Interest Refund"
        '
        'lblLoanApplicationDate
        '
        Me.lblLoanApplicationDate.AutoSize = True
        Me.lblLoanApplicationDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanApplicationDate.Location = New System.Drawing.Point(22, 95)
        Me.lblLoanApplicationDate.Name = "lblLoanApplicationDate"
        Me.lblLoanApplicationDate.Size = New System.Drawing.Size(85, 13)
        Me.lblLoanApplicationDate.TabIndex = 1
        Me.lblLoanApplicationDate.Text = "Application Date"
        '
        'lblServiceFee
        '
        Me.lblServiceFee.AutoSize = True
        Me.lblServiceFee.Location = New System.Drawing.Point(40, 101)
        Me.lblServiceFee.Name = "lblServiceFee"
        Me.lblServiceFee.Size = New System.Drawing.Size(63, 13)
        Me.lblServiceFee.TabIndex = 1
        Me.lblServiceFee.Text = "Service Fee"
        '
        'lblPenalty
        '
        Me.lblPenalty.AutoSize = True
        Me.lblPenalty.Location = New System.Drawing.Point(60, 159)
        Me.lblPenalty.Name = "lblPenalty"
        Me.lblPenalty.Size = New System.Drawing.Size(43, 13)
        Me.lblPenalty.TabIndex = 0
        Me.lblPenalty.Text = "Penalty"
        '
        'lblInterest
        '
        Me.lblInterest.AutoSize = True
        Me.lblInterest.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInterest.Location = New System.Drawing.Point(57, 68)
        Me.lblInterest.Name = "lblInterest"
        Me.lblInterest.Size = New System.Drawing.Size(46, 13)
        Me.lblInterest.TabIndex = 1
        Me.lblInterest.Text = "Interest"
        '
        'lblLoanNumber
        '
        Me.lblLoanNumber.AutoSize = True
        Me.lblLoanNumber.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanNumber.Location = New System.Drawing.Point(2, 10)
        Me.lblLoanNumber.Name = "lblLoanNumber"
        Me.lblLoanNumber.Size = New System.Drawing.Size(50, 13)
        Me.lblLoanNumber.TabIndex = 27
        Me.lblLoanNumber.Text = "Loan No."
        '
        'lblPrincipal
        '
        Me.lblPrincipal.AutoSize = True
        Me.lblPrincipal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPrincipal.Location = New System.Drawing.Point(63, 37)
        Me.lblPrincipal.Name = "lblPrincipal"
        Me.lblPrincipal.Size = New System.Drawing.Size(46, 13)
        Me.lblPrincipal.TabIndex = 0
        Me.lblPrincipal.Text = "Principal"
        '
        'lblClassification
        '
        Me.lblClassification.AutoSize = True
        Me.lblClassification.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClassification.Location = New System.Drawing.Point(40, 8)
        Me.lblClassification.Name = "lblClassification"
        Me.lblClassification.Size = New System.Drawing.Size(69, 13)
        Me.lblClassification.TabIndex = 28
        Me.lblClassification.Text = "Classification"
        '
        'txtLoanNumber
        '
        Me.txtLoanNumber.BackColor = System.Drawing.Color.Gainsboro
        Me.txtLoanNumber.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLoanNumber.Location = New System.Drawing.Point(52, 6)
        Me.txtLoanNumber.Name = "txtLoanNumber"
        Me.txtLoanNumber.ReadOnly = True
        Me.txtLoanNumber.Size = New System.Drawing.Size(66, 23)
        Me.txtLoanNumber.TabIndex = 29
        Me.txtLoanNumber.Text = "0"
        Me.txtLoanNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtClassification
        '
        Me.txtClassification.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtClassification.BackColor = System.Drawing.Color.Gainsboro
        Me.txtClassification.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtClassification.Location = New System.Drawing.Point(113, 4)
        Me.txtClassification.Name = "txtClassification"
        Me.txtClassification.ReadOnly = True
        Me.txtClassification.Size = New System.Drawing.Size(174, 23)
        Me.txtClassification.TabIndex = 30
        Me.txtClassification.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblAmortizationDate
        '
        Me.lblAmortizationDate.AutoSize = True
        Me.lblAmortizationDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAmortizationDate.Location = New System.Drawing.Point(15, 15)
        Me.lblAmortizationDate.Name = "lblAmortizationDate"
        Me.lblAmortizationDate.Size = New System.Drawing.Size(94, 13)
        Me.lblAmortizationDate.TabIndex = 31
        Me.lblAmortizationDate.Text = "Amortization Start"
        '
        'lblAmortization
        '
        Me.lblAmortization.AutoSize = True
        Me.lblAmortization.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAmortization.Location = New System.Drawing.Point(41, 71)
        Me.lblAmortization.Name = "lblAmortization"
        Me.lblAmortization.Size = New System.Drawing.Size(67, 13)
        Me.lblAmortization.TabIndex = 32
        Me.lblAmortization.Text = "Amortization"
        '
        'txtAmortization
        '
        Me.txtAmortization.BackColor = System.Drawing.Color.Gainsboro
        Me.txtAmortization.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAmortization.Location = New System.Drawing.Point(129, 71)
        Me.txtAmortization.Name = "txtAmortization"
        Me.txtAmortization.ReadOnly = True
        Me.txtAmortization.Size = New System.Drawing.Size(120, 23)
        Me.txtAmortization.TabIndex = 34
        Me.txtAmortization.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblTo
        '
        Me.lblTo.AutoSize = True
        Me.lblTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTo.Location = New System.Drawing.Point(15, 46)
        Me.lblTo.Name = "lblTo"
        Me.lblTo.Size = New System.Drawing.Size(88, 13)
        Me.lblTo.TabIndex = 35
        Me.lblTo.Text = "Amortization End"
        '
        'txtothers
        '
        Me.txtothers.BackColor = System.Drawing.Color.Gainsboro
        Me.txtothers.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtothers.Location = New System.Drawing.Point(115, 140)
        Me.txtothers.Name = "txtothers"
        Me.txtothers.ReadOnly = True
        Me.txtothers.Size = New System.Drawing.Size(153, 23)
        Me.txtothers.TabIndex = 64
        Me.txtothers.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtcompanyloan
        '
        Me.txtcompanyloan.BackColor = System.Drawing.Color.Gainsboro
        Me.txtcompanyloan.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtcompanyloan.Location = New System.Drawing.Point(115, 113)
        Me.txtcompanyloan.Name = "txtcompanyloan"
        Me.txtcompanyloan.ReadOnly = True
        Me.txtcompanyloan.Size = New System.Drawing.Size(153, 23)
        Me.txtcompanyloan.TabIndex = 63
        Me.txtcompanyloan.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(62, 143)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(44, 15)
        Me.Label13.TabIndex = 62
        Me.Label13.Text = "Others"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(23, 116)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(87, 15)
        Me.Label12.TabIndex = 61
        Me.Label12.Text = "Company Loan"
        '
        'txtphihealthloan
        '
        Me.txtphihealthloan.BackColor = System.Drawing.Color.Gainsboro
        Me.txtphihealthloan.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtphihealthloan.Location = New System.Drawing.Point(115, 86)
        Me.txtphihealthloan.Name = "txtphihealthloan"
        Me.txtphihealthloan.ReadOnly = True
        Me.txtphihealthloan.Size = New System.Drawing.Size(153, 23)
        Me.txtphihealthloan.TabIndex = 60
        Me.txtphihealthloan.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtpagibigloan
        '
        Me.txtpagibigloan.BackColor = System.Drawing.Color.Gainsboro
        Me.txtpagibigloan.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtpagibigloan.Location = New System.Drawing.Point(115, 59)
        Me.txtpagibigloan.Name = "txtpagibigloan"
        Me.txtpagibigloan.ReadOnly = True
        Me.txtpagibigloan.Size = New System.Drawing.Size(153, 23)
        Me.txtpagibigloan.TabIndex = 41
        Me.txtpagibigloan.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(10, 89)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(100, 15)
        Me.Label11.TabIndex = 40
        Me.Label11.Text = "PhilHealth Loans"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(27, 62)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(83, 15)
        Me.Label10.TabIndex = 39
        Me.Label10.Text = "Pagibig Loans"
        '
        'txtSSSloan
        '
        Me.txtSSSloan.BackColor = System.Drawing.Color.Gainsboro
        Me.txtSSSloan.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSSSloan.Location = New System.Drawing.Point(115, 32)
        Me.txtSSSloan.Name = "txtSSSloan"
        Me.txtSSSloan.ReadOnly = True
        Me.txtSSSloan.Size = New System.Drawing.Size(153, 23)
        Me.txtSSSloan.TabIndex = 38
        Me.txtSSSloan.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtSalaryMonthly
        '
        Me.txtSalaryMonthly.BackColor = System.Drawing.Color.Gainsboro
        Me.txtSalaryMonthly.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSalaryMonthly.Location = New System.Drawing.Point(115, 5)
        Me.txtSalaryMonthly.Name = "txtSalaryMonthly"
        Me.txtSalaryMonthly.ReadOnly = True
        Me.txtSalaryMonthly.Size = New System.Drawing.Size(153, 23)
        Me.txtSalaryMonthly.TabIndex = 37
        Me.txtSalaryMonthly.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSalaryMonthly.Visible = False
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(50, 35)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(60, 15)
        Me.Label9.TabIndex = 1
        Me.Label9.Text = "SSS Loans"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(5, 8)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(105, 15)
        Me.Label8.TabIndex = 0
        Me.Label8.Text = "Salary { Monthly }"
        Me.Label8.Visible = False
        '
        'cmdCheck
        '
        Me.cmdCheck.Enabled = False
        Me.cmdCheck.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdCheck.Location = New System.Drawing.Point(530, 29)
        Me.cmdCheck.Name = "cmdCheck"
        Me.cmdCheck.Size = New System.Drawing.Size(145, 30)
        Me.cmdCheck.TabIndex = 40
        Me.cmdCheck.Text = "View Requirements"
        Me.cmdCheck.UseVisualStyleBackColor = True
        Me.cmdCheck.Visible = False
        '
        'txtSearchnumber
        '
        Me.txtSearchnumber.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSearchnumber.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchnumber.Location = New System.Drawing.Point(4, 40)
        Me.txtSearchnumber.Name = "txtSearchnumber"
        Me.txtSearchnumber.Size = New System.Drawing.Size(219, 20)
        Me.txtSearchnumber.TabIndex = 46
        Me.txtSearchnumber.Visible = False
        '
        'cmdSearch
        '
        Me.cmdSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdSearch.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdSearch.Location = New System.Drawing.Point(164, 4)
        Me.cmdSearch.Name = "cmdSearch"
        Me.cmdSearch.Size = New System.Drawing.Size(55, 23)
        Me.cmdSearch.TabIndex = 42
        Me.cmdSearch.Text = "Search"
        Me.cmdSearch.UseVisualStyleBackColor = True
        '
        'txtSearchLastname
        '
        Me.txtSearchLastname.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSearchLastname.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSearchLastname.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchLastname.Location = New System.Drawing.Point(6, 6)
        Me.txtSearchLastname.Name = "txtSearchLastname"
        Me.txtSearchLastname.Size = New System.Drawing.Size(155, 20)
        Me.txtSearchLastname.TabIndex = 48
        '
        'optName
        '
        Me.optName.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.optName.AutoSize = True
        Me.optName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.optName.Location = New System.Drawing.Point(135, 22)
        Me.optName.Name = "optName"
        Me.optName.Size = New System.Drawing.Size(85, 17)
        Me.optName.TabIndex = 49
        Me.optName.TabStop = True
        Me.optName.Text = "Last Name"
        Me.optName.UseVisualStyleBackColor = True
        Me.optName.Visible = False
        '
        'optNumber
        '
        Me.optNumber.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.optNumber.AutoSize = True
        Me.optNumber.Checked = True
        Me.optNumber.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.optNumber.Location = New System.Drawing.Point(135, 3)
        Me.optNumber.Name = "optNumber"
        Me.optNumber.Size = New System.Drawing.Size(68, 17)
        Me.optNumber.TabIndex = 48
        Me.optNumber.TabStop = True
        Me.optNumber.Text = "Number"
        Me.optNumber.UseVisualStyleBackColor = True
        Me.optNumber.Visible = False
        '
        'lstNumber
        '
        Me.lstNumber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lstNumber.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstNumber.FormattingEnabled = True
        Me.lstNumber.Location = New System.Drawing.Point(12, 351)
        Me.lstNumber.Name = "lstNumber"
        Me.lstNumber.Size = New System.Drawing.Size(180, 28)
        Me.lstNumber.TabIndex = 49
        Me.lstNumber.Visible = False
        '
        'lstNames
        '
        Me.lstNames.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lstNames.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstNames.FormattingEnabled = True
        Me.lstNames.Location = New System.Drawing.Point(12, 318)
        Me.lstNames.Name = "lstNames"
        Me.lstNames.Size = New System.Drawing.Size(181, 28)
        Me.lstNames.TabIndex = 50
        Me.lstNames.Visible = False
        '
        'btnview
        '
        Me.btnview.Enabled = False
        Me.btnview.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnview.Location = New System.Drawing.Point(499, 27)
        Me.btnview.Name = "btnview"
        Me.btnview.Size = New System.Drawing.Size(185, 30)
        Me.btnview.TabIndex = 55
        Me.btnview.Text = "Requirements for Approval"
        Me.btnview.UseVisualStyleBackColor = True
        Me.btnview.Visible = False
        '
        'btnclose
        '
        Me.btnclose.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnclose.Location = New System.Drawing.Point(589, 29)
        Me.btnclose.Name = "btnclose"
        Me.btnclose.Size = New System.Drawing.Size(75, 29)
        Me.btnclose.TabIndex = 56
        Me.btnclose.Text = "Close"
        Me.btnclose.UseVisualStyleBackColor = True
        Me.btnclose.Visible = False
        '
        'lstExistingLoans
        '
        Me.lstExistingLoans.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lstExistingLoans.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstExistingLoans.Location = New System.Drawing.Point(6, 32)
        Me.lstExistingLoans.Name = "lstExistingLoans"
        Me.lstExistingLoans.Size = New System.Drawing.Size(213, 544)
        Me.lstExistingLoans.TabIndex = 0
        Me.lstExistingLoans.UseCompatibleStateImageBehavior = False
        '
        'GroupBox3
        '
        Me.GroupBox3.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox3.Controls.Add(Me.laonDTto)
        Me.GroupBox3.Controls.Add(Me.Label6)
        Me.GroupBox3.Controls.Add(Me.Label5)
        Me.GroupBox3.Controls.Add(Me.DTLoan)
        Me.GroupBox3.Controls.Add(Me.CboLoanNumber)
        Me.GroupBox3.Controls.Add(Me.Label4)
        Me.GroupBox3.Controls.Add(Me.CboLoanType)
        Me.GroupBox3.Controls.Add(Me.Label3)
        Me.GroupBox3.Controls.Add(Me.CboLoanStatus)
        Me.GroupBox3.Controls.Add(Me.Label2)
        Me.GroupBox3.Location = New System.Drawing.Point(4, 28)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(734, 0)
        Me.GroupBox3.TabIndex = 58
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Loans Filtering Option"
        Me.GroupBox3.Visible = False
        '
        'laonDTto
        '
        Me.laonDTto.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.laonDTto.Location = New System.Drawing.Point(386, 41)
        Me.laonDTto.Name = "laonDTto"
        Me.laonDTto.Size = New System.Drawing.Size(92, 21)
        Me.laonDTto.TabIndex = 59
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(335, 45)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(45, 13)
        Me.Label6.TabIndex = 59
        Me.Label6.Text = "Date To"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(323, 18)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(57, 13)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "Date From"
        '
        'DTLoan
        '
        Me.DTLoan.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DTLoan.Location = New System.Drawing.Point(386, 14)
        Me.DTLoan.Name = "DTLoan"
        Me.DTLoan.Size = New System.Drawing.Size(92, 21)
        Me.DTLoan.TabIndex = 6
        '
        'CboLoanNumber
        '
        Me.CboLoanNumber.FormattingEnabled = True
        Me.CboLoanNumber.Location = New System.Drawing.Point(64, 42)
        Me.CboLoanNumber.Name = "CboLoanNumber"
        Me.CboLoanNumber.Size = New System.Drawing.Size(48, 21)
        Me.CboLoanNumber.TabIndex = 5
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(6, 45)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(50, 13)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "Loan No."
        '
        'CboLoanType
        '
        Me.CboLoanType.FormattingEnabled = True
        Me.CboLoanType.Location = New System.Drawing.Point(155, 42)
        Me.CboLoanType.Name = "CboLoanType"
        Me.CboLoanType.Size = New System.Drawing.Size(139, 21)
        Me.CboLoanType.TabIndex = 3
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(118, 45)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(31, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Type"
        '
        'CboLoanStatus
        '
        Me.CboLoanStatus.FormattingEnabled = True
        Me.CboLoanStatus.Location = New System.Drawing.Point(64, 14)
        Me.CboLoanStatus.Name = "CboLoanStatus"
        Me.CboLoanStatus.Size = New System.Drawing.Size(230, 21)
        Me.CboLoanStatus.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(3, 18)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(38, 13)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Status"
        '
        'TabControl1
        '
        Me.TabControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TabControl1.Controls.Add(Me.TabMember)
        Me.TabControl1.Controls.Add(Me.TabTransaction)
        Me.TabControl1.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabControl1.Location = New System.Drawing.Point(3, 33)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(234, 632)
        Me.TabControl1.TabIndex = 59
        '
        'TabMember
        '
        Me.TabMember.Controls.Add(Me.lstExistingLoans)
        Me.TabMember.Controls.Add(Me.cmdSearch)
        Me.TabMember.Controls.Add(Me.txtSearchLastname)
        Me.TabMember.Location = New System.Drawing.Point(4, 24)
        Me.TabMember.Name = "TabMember"
        Me.TabMember.Padding = New System.Windows.Forms.Padding(3)
        Me.TabMember.Size = New System.Drawing.Size(226, 604)
        Me.TabMember.TabIndex = 0
        Me.TabMember.Text = "Member"
        Me.TabMember.UseVisualStyleBackColor = True
        '
        'TabTransaction
        '
        Me.TabTransaction.Controls.Add(Me.lstNumber)
        Me.TabTransaction.Controls.Add(Me.lstNames)
        Me.TabTransaction.Controls.Add(Me.ListStatus)
        Me.TabTransaction.Controls.Add(Me.Label7)
        Me.TabTransaction.Controls.Add(Me.optName)
        Me.TabTransaction.Controls.Add(Me.txtSearchnumber)
        Me.TabTransaction.Controls.Add(Me.optNumber)
        Me.TabTransaction.Location = New System.Drawing.Point(4, 24)
        Me.TabTransaction.Name = "TabTransaction"
        Me.TabTransaction.Padding = New System.Windows.Forms.Padding(3)
        Me.TabTransaction.Size = New System.Drawing.Size(226, 604)
        Me.TabTransaction.TabIndex = 1
        Me.TabTransaction.Text = "Transaction"
        Me.TabTransaction.UseVisualStyleBackColor = True
        '
        'ListStatus
        '
        Me.ListStatus.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ListStatus.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListStatus.Location = New System.Drawing.Point(4, 33)
        Me.ListStatus.Name = "ListStatus"
        Me.ListStatus.Size = New System.Drawing.Size(219, 543)
        Me.ListStatus.TabIndex = 1
        Me.ListStatus.UseCompatibleStateImageBehavior = False
        '
        'Label7
        '
        Me.Label7.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.White
        Me.Label7.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(7, 8)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(89, 19)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "Loan Status"
        '
        'Btnprint
        '
        Me.Btnprint.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Btnprint.Location = New System.Drawing.Point(568, 27)
        Me.Btnprint.Name = "Btnprint"
        Me.Btnprint.Size = New System.Drawing.Size(132, 30)
        Me.Btnprint.TabIndex = 62
        Me.Btnprint.Text = "Print Loan Details"
        Me.Btnprint.UseVisualStyleBackColor = True
        Me.Btnprint.Visible = False
        '
        'panelLoanProcessing
        '
        Me.panelLoanProcessing.Controls.Add(Me.SplitContainer1)
        Me.panelLoanProcessing.Dock = System.Windows.Forms.DockStyle.Fill
        Me.panelLoanProcessing.Location = New System.Drawing.Point(0, 0)
        Me.panelLoanProcessing.Name = "panelLoanProcessing"
        Me.panelLoanProcessing.Size = New System.Drawing.Size(985, 636)
        Me.panelLoanProcessing.TabIndex = 63
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.PanePanel5)
        Me.SplitContainer1.Panel1.Controls.Add(Me.TabControl1)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.SplitContainer2)
        Me.SplitContainer1.Panel2.Controls.Add(Me.panelLoansFilter)
        Me.SplitContainer1.Size = New System.Drawing.Size(985, 636)
        Me.SplitContainer1.SplitterDistance = 240
        Me.SplitContainer1.TabIndex = 65
        '
        'PanePanel5
        '
        Me.PanePanel5.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.PanePanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel5.Controls.Add(Me.Label19)
        Me.PanePanel5.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanePanel5.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.PanePanel5.InactiveGradientHighColor = System.Drawing.Color.Transparent
        Me.PanePanel5.InactiveGradientLowColor = System.Drawing.Color.Transparent
        Me.PanePanel5.Location = New System.Drawing.Point(0, 0)
        Me.PanePanel5.Name = "PanePanel5"
        Me.PanePanel5.Size = New System.Drawing.Size(240, 26)
        Me.PanePanel5.TabIndex = 51
        '
        'Label19
        '
        Me.Label19.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label19.AutoSize = True
        Me.Label19.BackColor = System.Drawing.Color.Transparent
        Me.Label19.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.Black
        Me.Label19.Location = New System.Drawing.Point(3, 0)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(87, 23)
        Me.Label19.TabIndex = 14
        Me.Label19.Text = "Members"
        '
        'SplitContainer2
        '
        Me.SplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel2
        Me.SplitContainer2.Location = New System.Drawing.Point(0, 27)
        Me.SplitContainer2.Name = "SplitContainer2"
        Me.SplitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitContainer2.Panel1
        '
        Me.SplitContainer2.Panel1.Controls.Add(Me.GrdLoanSummary)
        Me.SplitContainer2.Panel1.Controls.Add(Me.PanePanel2)
        Me.SplitContainer2.Panel1.Controls.Add(Me.tabLoanDetails)
        '
        'SplitContainer2.Panel2
        '
        Me.SplitContainer2.Panel2.Controls.Add(Me.btnPrintStatement)
        Me.SplitContainer2.Panel2.Controls.Add(Me.btnValidateLoans)
        Me.SplitContainer2.Panel2.Controls.Add(Me.cmdCheck)
        Me.SplitContainer2.Panel2.Controls.Add(Me.Btnprint)
        Me.SplitContainer2.Panel2.Controls.Add(Me.PanePanel3)
        Me.SplitContainer2.Panel2.Controls.Add(Me.btnclose)
        Me.SplitContainer2.Panel2.Controls.Add(Me.btnview)
        Me.SplitContainer2.Panel2.Controls.Add(Me.MenuStrip1)
        Me.SplitContainer2.Size = New System.Drawing.Size(741, 609)
        Me.SplitContainer2.SplitterDistance = 545
        Me.SplitContainer2.TabIndex = 1
        '
        'PanePanel2
        '
        Me.PanePanel2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanePanel2.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.PanePanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel2.Controls.Add(Me.lblExpressLoan)
        Me.PanePanel2.Controls.Add(Me.txtmembername)
        Me.PanePanel2.Controls.Add(Me.Label1)
        Me.PanePanel2.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.PanePanel2.InactiveGradientHighColor = System.Drawing.Color.Transparent
        Me.PanePanel2.InactiveGradientLowColor = System.Drawing.Color.Transparent
        Me.PanePanel2.Location = New System.Drawing.Point(3, 281)
        Me.PanePanel2.Name = "PanePanel2"
        Me.PanePanel2.Size = New System.Drawing.Size(734, 26)
        Me.PanePanel2.TabIndex = 53
        '
        'lblExpressLoan
        '
        Me.lblExpressLoan.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblExpressLoan.AutoSize = True
        Me.lblExpressLoan.BackColor = System.Drawing.Color.Transparent
        Me.lblExpressLoan.Font = New System.Drawing.Font("Calibri", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblExpressLoan.Location = New System.Drawing.Point(1, -1)
        Me.lblExpressLoan.Name = "lblExpressLoan"
        Me.lblExpressLoan.Size = New System.Drawing.Size(77, 26)
        Me.lblExpressLoan.TabIndex = 0
        Me.lblExpressLoan.Text = "Express"
        '
        'txtmembername
        '
        Me.txtmembername.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtmembername.BackColor = System.Drawing.SystemColors.Window
        Me.txtmembername.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtmembername.Font = New System.Drawing.Font("Calibri", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtmembername.Location = New System.Drawing.Point(263, -2)
        Me.txtmembername.Name = "txtmembername"
        Me.txtmembername.Size = New System.Drawing.Size(469, 26)
        Me.txtmembername.TabIndex = 63
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label1.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(0, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(732, 24)
        Me.Label1.TabIndex = 20
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'tabLoanDetails
        '
        Me.tabLoanDetails.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tabLoanDetails.Controls.Add(Me.loan)
        Me.tabLoanDetails.Controls.Add(Me.amortization)
        Me.tabLoanDetails.Controls.Add(Me.maxLoanAmt)
        Me.tabLoanDetails.Controls.Add(Me.Comaker)
        Me.tabLoanDetails.Location = New System.Drawing.Point(4, 309)
        Me.tabLoanDetails.Name = "tabLoanDetails"
        Me.tabLoanDetails.SelectedIndex = 0
        Me.tabLoanDetails.Size = New System.Drawing.Size(734, 232)
        Me.tabLoanDetails.TabIndex = 64
        '
        'loan
        '
        Me.loan.BackColor = System.Drawing.Color.Transparent
        Me.loan.Controls.Add(Me.pnlLoanDetails)
        Me.loan.Controls.Add(Me.btnChangeLoanNo)
        Me.loan.Controls.Add(Me.txtTerms)
        Me.loan.Controls.Add(Me.txtLoanNumber)
        Me.loan.Controls.Add(Me.SplitContainer3)
        Me.loan.Controls.Add(Me.lblLoanNumber)
        Me.loan.Controls.Add(Me.lblTerms)
        Me.loan.Location = New System.Drawing.Point(4, 22)
        Me.loan.Name = "loan"
        Me.loan.Padding = New System.Windows.Forms.Padding(3)
        Me.loan.Size = New System.Drawing.Size(726, 206)
        Me.loan.TabIndex = 0
        Me.loan.Text = "Loan"
        Me.loan.UseVisualStyleBackColor = True
        '
        'pnlLoanDetails
        '
        Me.pnlLoanDetails.Controls.Add(Me.Label20)
        Me.pnlLoanDetails.Controls.Add(Me.txtDNetProceeds)
        Me.pnlLoanDetails.Controls.Add(Me.Label21)
        Me.pnlLoanDetails.Controls.Add(Me.txtDInt)
        Me.pnlLoanDetails.Controls.Add(Me.Label18)
        Me.pnlLoanDetails.Controls.Add(Me.txtDterms)
        Me.pnlLoanDetails.Controls.Add(Me.Label17)
        Me.pnlLoanDetails.Controls.Add(Me.txtdDate)
        Me.pnlLoanDetails.Controls.Add(Me.Label16)
        Me.pnlLoanDetails.Controls.Add(Me.txtDPrincipal)
        Me.pnlLoanDetails.Controls.Add(Me.Label15)
        Me.pnlLoanDetails.Controls.Add(Me.txtDLoanNo)
        Me.pnlLoanDetails.Controls.Add(Me.Label14)
        Me.pnlLoanDetails.Controls.Add(Me.dgvDeductions)
        Me.pnlLoanDetails.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlLoanDetails.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlLoanDetails.Location = New System.Drawing.Point(3, 3)
        Me.pnlLoanDetails.Name = "pnlLoanDetails"
        Me.pnlLoanDetails.Size = New System.Drawing.Size(720, 200)
        Me.pnlLoanDetails.TabIndex = 65
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(5, 46)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(70, 13)
        Me.Label20.TabIndex = 16
        Me.Label20.Text = "Deductions"
        '
        'txtDNetProceeds
        '
        Me.txtDNetProceeds.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDNetProceeds.Location = New System.Drawing.Point(553, 172)
        Me.txtDNetProceeds.Name = "txtDNetProceeds"
        Me.txtDNetProceeds.ReadOnly = True
        Me.txtDNetProceeds.Size = New System.Drawing.Size(140, 21)
        Me.txtDNetProceeds.TabIndex = 15
        Me.txtDNetProceeds.Text = "0.00"
        Me.txtDNetProceeds.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(460, 180)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(87, 13)
        Me.Label21.TabIndex = 14
        Me.Label21.Text = "Net Proceeds:"
        '
        'txtDInt
        '
        Me.txtDInt.Location = New System.Drawing.Point(311, 19)
        Me.txtDInt.Name = "txtDInt"
        Me.txtDInt.ReadOnly = True
        Me.txtDInt.Size = New System.Drawing.Size(100, 21)
        Me.txtDInt.TabIndex = 10
        Me.txtDInt.Text = "0.00"
        Me.txtDInt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(337, 3)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(74, 13)
        Me.Label18.TabIndex = 9
        Me.Label18.Text = "Interest(%)"
        '
        'txtDterms
        '
        Me.txtDterms.Location = New System.Drawing.Point(418, 19)
        Me.txtDterms.Name = "txtDterms"
        Me.txtDterms.ReadOnly = True
        Me.txtDterms.Size = New System.Drawing.Size(100, 21)
        Me.txtDterms.TabIndex = 8
        Me.txtDterms.Text = "0.00"
        Me.txtDterms.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(466, 3)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(47, 13)
        Me.Label17.TabIndex = 7
        Me.Label17.Text = "Terms "
        '
        'txtdDate
        '
        Me.txtdDate.Location = New System.Drawing.Point(524, 19)
        Me.txtdDate.Name = "txtdDate"
        Me.txtdDate.ReadOnly = True
        Me.txtdDate.Size = New System.Drawing.Size(169, 21)
        Me.txtdDate.TabIndex = 6
        Me.txtdDate.Text = "-/-/-"
        Me.txtdDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(604, 3)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(84, 13)
        Me.Label16.TabIndex = 5
        Me.Label16.Text = "Date Granted"
        '
        'txtDPrincipal
        '
        Me.txtDPrincipal.Location = New System.Drawing.Point(113, 19)
        Me.txtDPrincipal.Name = "txtDPrincipal"
        Me.txtDPrincipal.ReadOnly = True
        Me.txtDPrincipal.Size = New System.Drawing.Size(192, 21)
        Me.txtDPrincipal.TabIndex = 4
        Me.txtDPrincipal.Text = "0.00"
        Me.txtDPrincipal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(234, 3)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(55, 13)
        Me.Label15.TabIndex = 3
        Me.Label15.Text = "Principal"
        '
        'txtDLoanNo
        '
        Me.txtDLoanNo.Location = New System.Drawing.Point(7, 19)
        Me.txtDLoanNo.Name = "txtDLoanNo"
        Me.txtDLoanNo.ReadOnly = True
        Me.txtDLoanNo.Size = New System.Drawing.Size(100, 21)
        Me.txtDLoanNo.TabIndex = 2
        Me.txtDLoanNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(41, 3)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(61, 13)
        Me.Label14.TabIndex = 1
        Me.Label14.Text = "Loan No. "
        '
        'dgvDeductions
        '
        Me.dgvDeductions.AllowUserToAddRows = False
        Me.dgvDeductions.AllowUserToDeleteRows = False
        Me.dgvDeductions.AllowUserToResizeColumns = False
        Me.dgvDeductions.AllowUserToResizeRows = False
        Me.dgvDeductions.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvDeductions.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvDeductions.BackgroundColor = System.Drawing.Color.White
        Me.dgvDeductions.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvDeductions.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.dgvDeductions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDeductions.Location = New System.Drawing.Point(8, 62)
        Me.dgvDeductions.Name = "dgvDeductions"
        Me.dgvDeductions.ReadOnly = True
        Me.dgvDeductions.RowHeadersVisible = False
        Me.dgvDeductions.Size = New System.Drawing.Size(688, 107)
        Me.dgvDeductions.TabIndex = 0
        '
        'btnChangeLoanNo
        '
        Me.btnChangeLoanNo.Enabled = False
        Me.btnChangeLoanNo.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.btnChangeLoanNo.Location = New System.Drawing.Point(118, 6)
        Me.btnChangeLoanNo.Name = "btnChangeLoanNo"
        Me.btnChangeLoanNo.Size = New System.Drawing.Size(24, 22)
        Me.btnChangeLoanNo.TabIndex = 38
        Me.btnChangeLoanNo.Text = "..."
        Me.btnChangeLoanNo.UseVisualStyleBackColor = True
        Me.btnChangeLoanNo.Visible = False
        '
        'SplitContainer3
        '
        Me.SplitContainer3.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SplitContainer3.IsSplitterFixed = True
        Me.SplitContainer3.Location = New System.Drawing.Point(146, 0)
        Me.SplitContainer3.Name = "SplitContainer3"
        '
        'SplitContainer3.Panel1
        '
        Me.SplitContainer3.Panel1.Controls.Add(Me.lblClassification)
        Me.SplitContainer3.Panel1.Controls.Add(Me.txtPrincipal)
        Me.SplitContainer3.Panel1.Controls.Add(Me.lblCapitalShare)
        Me.SplitContainer3.Panel1.Controls.Add(Me.lblServiceFee)
        Me.SplitContainer3.Panel1.Controls.Add(Me.lblInterest)
        Me.SplitContainer3.Panel1.Controls.Add(Me.txtServiceFee)
        Me.SplitContainer3.Panel1.Controls.Add(Me.txtPenalty)
        Me.SplitContainer3.Panel1.Controls.Add(Me.lblPrincipal)
        Me.SplitContainer3.Panel1.Controls.Add(Me.txtInterest)
        Me.SplitContainer3.Panel1.Controls.Add(Me.lblPenalty)
        Me.SplitContainer3.Panel1.Controls.Add(Me.txtCapitalShare)
        Me.SplitContainer3.Panel1.Controls.Add(Me.txtClassification)
        '
        'SplitContainer3.Panel2
        '
        Me.SplitContainer3.Panel2.Controls.Add(Me.txtPayment)
        Me.SplitContainer3.Panel2.Controls.Add(Me.lblBalance)
        Me.SplitContainer3.Panel2.Controls.Add(Me.lblPayment)
        Me.SplitContainer3.Panel2.Controls.Add(Me.lblLoanApplicationDate)
        Me.SplitContainer3.Panel2.Controls.Add(Me.txtBalance)
        Me.SplitContainer3.Panel2.Controls.Add(Me.txtLoanAppDate)
        Me.SplitContainer3.Panel2.Controls.Add(Me.txtInterestRefund)
        Me.SplitContainer3.Panel2.Controls.Add(Me.lblNetProceeds)
        Me.SplitContainer3.Panel2.Controls.Add(Me.lblInterestRefund)
        Me.SplitContainer3.Panel2.Controls.Add(Me.lblstatus)
        Me.SplitContainer3.Panel2.Controls.Add(Me.txtStatus)
        Me.SplitContainer3.Panel2.Controls.Add(Me.txtNetProceeds)
        Me.SplitContainer3.Size = New System.Drawing.Size(577, 204)
        Me.SplitContainer3.SplitterDistance = 293
        Me.SplitContainer3.TabIndex = 34
        '
        'txtPayment
        '
        Me.txtPayment.AcceptsTab = True
        Me.txtPayment.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtPayment.BackColor = System.Drawing.Color.Gainsboro
        Me.txtPayment.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPayment.Location = New System.Drawing.Point(77, 33)
        Me.txtPayment.Name = "txtPayment"
        Me.txtPayment.ReadOnly = True
        Me.txtPayment.Size = New System.Drawing.Size(200, 23)
        Me.txtPayment.TabIndex = 33
        Me.txtPayment.Text = "0"
        Me.txtPayment.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblPayment
        '
        Me.lblPayment.AutoSize = True
        Me.lblPayment.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPayment.Location = New System.Drawing.Point(5, 33)
        Me.lblPayment.Name = "lblPayment"
        Me.lblPayment.Size = New System.Drawing.Size(65, 19)
        Me.lblPayment.TabIndex = 32
        Me.lblPayment.Text = "Payment"
        '
        'amortization
        '
        Me.amortization.Controls.Add(Me.dteAmortEnd)
        Me.amortization.Controls.Add(Me.dteAmortStart)
        Me.amortization.Controls.Add(Me.btnChangeDateEnd)
        Me.amortization.Controls.Add(Me.btnChangeDateStart)
        Me.amortization.Controls.Add(Me.lblAmortizationDate)
        Me.amortization.Controls.Add(Me.btnEditAmort)
        Me.amortization.Controls.Add(Me.lblAmortization)
        Me.amortization.Controls.Add(Me.txtAmortization)
        Me.amortization.Controls.Add(Me.lblTo)
        Me.amortization.Location = New System.Drawing.Point(4, 22)
        Me.amortization.Name = "amortization"
        Me.amortization.Padding = New System.Windows.Forms.Padding(3)
        Me.amortization.Size = New System.Drawing.Size(726, 206)
        Me.amortization.TabIndex = 1
        Me.amortization.Text = "Amortization"
        Me.amortization.UseVisualStyleBackColor = True
        '
        'dteAmortEnd
        '
        Me.dteAmortEnd.Enabled = False
        Me.dteAmortEnd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dteAmortEnd.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dteAmortEnd.Location = New System.Drawing.Point(129, 44)
        Me.dteAmortEnd.Name = "dteAmortEnd"
        Me.dteAmortEnd.Size = New System.Drawing.Size(120, 21)
        Me.dteAmortEnd.TabIndex = 41
        '
        'dteAmortStart
        '
        Me.dteAmortStart.Enabled = False
        Me.dteAmortStart.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dteAmortStart.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dteAmortStart.Location = New System.Drawing.Point(129, 14)
        Me.dteAmortStart.Name = "dteAmortStart"
        Me.dteAmortStart.Size = New System.Drawing.Size(120, 21)
        Me.dteAmortStart.TabIndex = 40
        '
        'btnChangeDateEnd
        '
        Me.btnChangeDateEnd.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.btnChangeDateEnd.Location = New System.Drawing.Point(255, 39)
        Me.btnChangeDateEnd.Name = "btnChangeDateEnd"
        Me.btnChangeDateEnd.Size = New System.Drawing.Size(124, 22)
        Me.btnChangeDateEnd.TabIndex = 39
        Me.btnChangeDateEnd.Text = "Change End Date"
        Me.btnChangeDateEnd.UseVisualStyleBackColor = True
        '
        'btnChangeDateStart
        '
        Me.btnChangeDateStart.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.btnChangeDateStart.Location = New System.Drawing.Point(255, 11)
        Me.btnChangeDateStart.Name = "btnChangeDateStart"
        Me.btnChangeDateStart.Size = New System.Drawing.Size(124, 22)
        Me.btnChangeDateStart.TabIndex = 38
        Me.btnChangeDateStart.Text = "Change Start Date"
        Me.btnChangeDateStart.UseVisualStyleBackColor = True
        '
        'maxLoanAmt
        '
        Me.maxLoanAmt.Controls.Add(Me.txtothers)
        Me.maxLoanAmt.Controls.Add(Me.Label8)
        Me.maxLoanAmt.Controls.Add(Me.txtcompanyloan)
        Me.maxLoanAmt.Controls.Add(Me.Label9)
        Me.maxLoanAmt.Controls.Add(Me.Label13)
        Me.maxLoanAmt.Controls.Add(Me.txtSalaryMonthly)
        Me.maxLoanAmt.Controls.Add(Me.Label12)
        Me.maxLoanAmt.Controls.Add(Me.txtSSSloan)
        Me.maxLoanAmt.Controls.Add(Me.txtphihealthloan)
        Me.maxLoanAmt.Controls.Add(Me.Label10)
        Me.maxLoanAmt.Controls.Add(Me.txtpagibigloan)
        Me.maxLoanAmt.Controls.Add(Me.Label11)
        Me.maxLoanAmt.Location = New System.Drawing.Point(4, 22)
        Me.maxLoanAmt.Name = "maxLoanAmt"
        Me.maxLoanAmt.Size = New System.Drawing.Size(726, 206)
        Me.maxLoanAmt.TabIndex = 2
        Me.maxLoanAmt.Text = "Max Loan Amount"
        Me.maxLoanAmt.UseVisualStyleBackColor = True
        '
        'Comaker
        '
        Me.Comaker.Controls.Add(Me.dgvComaker)
        Me.Comaker.Location = New System.Drawing.Point(4, 22)
        Me.Comaker.Name = "Comaker"
        Me.Comaker.Size = New System.Drawing.Size(726, 206)
        Me.Comaker.TabIndex = 3
        Me.Comaker.Text = "Co-Maker Details"
        Me.Comaker.UseVisualStyleBackColor = True
        '
        'dgvComaker
        '
        Me.dgvComaker.AllowUserToAddRows = False
        Me.dgvComaker.AllowUserToDeleteRows = False
        Me.dgvComaker.AllowUserToResizeColumns = False
        Me.dgvComaker.AllowUserToResizeRows = False
        Me.dgvComaker.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvComaker.BackgroundColor = System.Drawing.Color.White
        Me.dgvComaker.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvComaker.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.dgvComaker.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvComaker.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvComaker.Location = New System.Drawing.Point(0, 0)
        Me.dgvComaker.Name = "dgvComaker"
        Me.dgvComaker.ReadOnly = True
        Me.dgvComaker.RowHeadersVisible = False
        Me.dgvComaker.Size = New System.Drawing.Size(726, 206)
        Me.dgvComaker.TabIndex = 0
        '
        'btnPrintStatement
        '
        Me.btnPrintStatement.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPrintStatement.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPrintStatement.Location = New System.Drawing.Point(543, 28)
        Me.btnPrintStatement.Name = "btnPrintStatement"
        Me.btnPrintStatement.Size = New System.Drawing.Size(151, 30)
        Me.btnPrintStatement.TabIndex = 64
        Me.btnPrintStatement.Text = "Print Statement"
        Me.btnPrintStatement.UseVisualStyleBackColor = True
        Me.btnPrintStatement.Visible = False
        '
        'btnValidateLoans
        '
        Me.btnValidateLoans.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnValidateLoans.Location = New System.Drawing.Point(513, 28)
        Me.btnValidateLoans.Name = "btnValidateLoans"
        Me.btnValidateLoans.Size = New System.Drawing.Size(132, 30)
        Me.btnValidateLoans.TabIndex = 63
        Me.btnValidateLoans.Text = "Validate Loans"
        Me.btnValidateLoans.UseVisualStyleBackColor = True
        Me.btnValidateLoans.Visible = False
        '
        'PanePanel3
        '
        Me.PanePanel3.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.PanePanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel3.Controls.Add(Me.lblWhatdouwant)
        Me.PanePanel3.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanePanel3.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.PanePanel3.InactiveGradientHighColor = System.Drawing.Color.Transparent
        Me.PanePanel3.InactiveGradientLowColor = System.Drawing.Color.Transparent
        Me.PanePanel3.Location = New System.Drawing.Point(0, 0)
        Me.PanePanel3.Name = "PanePanel3"
        Me.PanePanel3.Size = New System.Drawing.Size(741, 26)
        Me.PanePanel3.TabIndex = 54
        '
        'lblWhatdouwant
        '
        Me.lblWhatdouwant.AutoSize = True
        Me.lblWhatdouwant.BackColor = System.Drawing.Color.Transparent
        Me.lblWhatdouwant.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWhatdouwant.ForeColor = System.Drawing.Color.Black
        Me.lblWhatdouwant.Location = New System.Drawing.Point(3, 0)
        Me.lblWhatdouwant.Name = "lblWhatdouwant"
        Me.lblWhatdouwant.Size = New System.Drawing.Size(208, 23)
        Me.lblWhatdouwant.TabIndex = 25
        Me.lblWhatdouwant.Text = "What do you want to do?"
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.MenuStrip1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ViewRequirementToolStripMenuItem, Me.RequirementsForApprovalToolStripMenuItem, Me.PrintToolStripMenuItem, Me.SearchToolStripMenuItem, Me.DeleteToolStripMenuItem, Me.CloseToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 36)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(741, 24)
        Me.MenuStrip1.TabIndex = 65
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'ViewRequirementToolStripMenuItem
        '
        Me.ViewRequirementToolStripMenuItem.Image = Global.WindowsApplication2.My.Resources.Resources.find2
        Me.ViewRequirementToolStripMenuItem.Name = "ViewRequirementToolStripMenuItem"
        Me.ViewRequirementToolStripMenuItem.Size = New System.Drawing.Size(139, 20)
        Me.ViewRequirementToolStripMenuItem.Text = "View Requirement"
        '
        'RequirementsForApprovalToolStripMenuItem
        '
        Me.RequirementsForApprovalToolStripMenuItem.Image = Global.WindowsApplication2.My.Resources.Resources.files
        Me.RequirementsForApprovalToolStripMenuItem.Name = "RequirementsForApprovalToolStripMenuItem"
        Me.RequirementsForApprovalToolStripMenuItem.Size = New System.Drawing.Size(191, 20)
        Me.RequirementsForApprovalToolStripMenuItem.Text = "Requirements For Approval"
        '
        'PrintToolStripMenuItem
        '
        Me.PrintToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.LoanApplicationFormToolStripMenuItem, Me.LoanAgreementToolStripMenuItem})
        Me.PrintToolStripMenuItem.Name = "PrintToolStripMenuItem"
        Me.PrintToolStripMenuItem.Size = New System.Drawing.Size(45, 20)
        Me.PrintToolStripMenuItem.Text = "Print"
        Me.PrintToolStripMenuItem.Visible = False
        '
        'LoanApplicationFormToolStripMenuItem
        '
        Me.LoanApplicationFormToolStripMenuItem.Name = "LoanApplicationFormToolStripMenuItem"
        Me.LoanApplicationFormToolStripMenuItem.Size = New System.Drawing.Size(200, 22)
        Me.LoanApplicationFormToolStripMenuItem.Text = "Loan Application Form"
        Me.LoanApplicationFormToolStripMenuItem.Visible = False
        '
        'LoanAgreementToolStripMenuItem
        '
        Me.LoanAgreementToolStripMenuItem.Name = "LoanAgreementToolStripMenuItem"
        Me.LoanAgreementToolStripMenuItem.Size = New System.Drawing.Size(200, 22)
        Me.LoanAgreementToolStripMenuItem.Text = "Loan Agreement"
        '
        'SearchToolStripMenuItem
        '
        Me.SearchToolStripMenuItem.Name = "SearchToolStripMenuItem"
        Me.SearchToolStripMenuItem.Size = New System.Drawing.Size(59, 20)
        Me.SearchToolStripMenuItem.Text = "Search"
        Me.SearchToolStripMenuItem.Visible = False
        '
        'DeleteToolStripMenuItem
        '
        Me.DeleteToolStripMenuItem.Image = Global.WindowsApplication2.My.Resources.Resources.delete3
        Me.DeleteToolStripMenuItem.Name = "DeleteToolStripMenuItem"
        Me.DeleteToolStripMenuItem.Size = New System.Drawing.Size(72, 20)
        Me.DeleteToolStripMenuItem.Text = "Delete"
        '
        'CloseToolStripMenuItem
        '
        Me.CloseToolStripMenuItem.Image = Global.WindowsApplication2.My.Resources.Resources.eventlogError
        Me.CloseToolStripMenuItem.Name = "CloseToolStripMenuItem"
        Me.CloseToolStripMenuItem.Size = New System.Drawing.Size(67, 20)
        Me.CloseToolStripMenuItem.Text = "Close"
        '
        'panelLoansFilter
        '
        Me.panelLoansFilter.Controls.Add(Me.PanePanel1)
        Me.panelLoansFilter.Controls.Add(Me.GroupBox3)
        Me.panelLoansFilter.Dock = System.Windows.Forms.DockStyle.Top
        Me.panelLoansFilter.Location = New System.Drawing.Point(0, 0)
        Me.panelLoansFilter.Name = "panelLoansFilter"
        Me.panelLoansFilter.Size = New System.Drawing.Size(741, 27)
        Me.panelLoansFilter.TabIndex = 0
        '
        'PanePanel1
        '
        Me.PanePanel1.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.PanePanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel1.Controls.Add(Me.lblLoanSummary)
        Me.PanePanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanePanel1.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.PanePanel1.InactiveGradientHighColor = System.Drawing.Color.Transparent
        Me.PanePanel1.InactiveGradientLowColor = System.Drawing.Color.Transparent
        Me.PanePanel1.Location = New System.Drawing.Point(0, 0)
        Me.PanePanel1.Name = "PanePanel1"
        Me.PanePanel1.Size = New System.Drawing.Size(741, 26)
        Me.PanePanel1.TabIndex = 52
        '
        'lblLoanSummary
        '
        Me.lblLoanSummary.AutoSize = True
        Me.lblLoanSummary.BackColor = System.Drawing.Color.Transparent
        Me.lblLoanSummary.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanSummary.ForeColor = System.Drawing.Color.Black
        Me.lblLoanSummary.Location = New System.Drawing.Point(0, 1)
        Me.lblLoanSummary.Name = "lblLoanSummary"
        Me.lblLoanSummary.Size = New System.Drawing.Size(125, 23)
        Me.lblLoanSummary.TabIndex = 20
        Me.lblLoanSummary.Text = "Loan Summary"
        '
        'frmLoan_Processing
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Window
        Me.ClientSize = New System.Drawing.Size(985, 636)
        Me.Controls.Add(Me.panelLoanProcessing)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.MinimumSize = New System.Drawing.Size(882, 558)
        Me.Name = "frmLoan_Processing"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Loan Processing"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.GrdLoanSummary, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.TabControl1.ResumeLayout(False)
        Me.TabMember.ResumeLayout(False)
        Me.TabMember.PerformLayout()
        Me.TabTransaction.ResumeLayout(False)
        Me.TabTransaction.PerformLayout()
        Me.panelLoanProcessing.ResumeLayout(False)
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.ResumeLayout(False)
        Me.PanePanel5.ResumeLayout(False)
        Me.PanePanel5.PerformLayout()
        Me.SplitContainer2.Panel1.ResumeLayout(False)
        Me.SplitContainer2.Panel2.ResumeLayout(False)
        Me.SplitContainer2.Panel2.PerformLayout()
        Me.SplitContainer2.ResumeLayout(False)
        Me.PanePanel2.ResumeLayout(False)
        Me.PanePanel2.PerformLayout()
        Me.tabLoanDetails.ResumeLayout(False)
        Me.loan.ResumeLayout(False)
        Me.loan.PerformLayout()
        Me.pnlLoanDetails.ResumeLayout(False)
        Me.pnlLoanDetails.PerformLayout()
        CType(Me.dgvDeductions, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer3.Panel1.ResumeLayout(False)
        Me.SplitContainer3.Panel1.PerformLayout()
        Me.SplitContainer3.Panel2.ResumeLayout(False)
        Me.SplitContainer3.Panel2.PerformLayout()
        Me.SplitContainer3.ResumeLayout(False)
        Me.amortization.ResumeLayout(False)
        Me.amortization.PerformLayout()
        Me.maxLoanAmt.ResumeLayout(False)
        Me.maxLoanAmt.PerformLayout()
        Me.Comaker.ResumeLayout(False)
        CType(Me.dgvComaker, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanePanel3.ResumeLayout(False)
        Me.PanePanel3.PerformLayout()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.panelLoansFilter.ResumeLayout(False)
        Me.PanePanel1.ResumeLayout(False)
        Me.PanePanel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblLoanSummary As System.Windows.Forms.Label
    Friend WithEvents GrdLoanSummary As System.Windows.Forms.DataGridView
    Friend WithEvents lblWhatdouwant As System.Windows.Forms.Label
    Friend WithEvents txtStatus As System.Windows.Forms.TextBox
    Friend WithEvents lblstatus As System.Windows.Forms.Label
    Friend WithEvents txtLoanAppDate As System.Windows.Forms.TextBox
    Friend WithEvents lblLoanApplicationDate As System.Windows.Forms.Label
    Friend WithEvents lblExpressLoan As System.Windows.Forms.Label
    Friend WithEvents lblLoanNumber As System.Windows.Forms.Label
    Friend WithEvents lblClassification As System.Windows.Forms.Label
    Friend WithEvents txtLoanNumber As System.Windows.Forms.TextBox
    Friend WithEvents txtClassification As System.Windows.Forms.TextBox
    Friend WithEvents lblAmortizationDate As System.Windows.Forms.Label
    Friend WithEvents lblAmortization As System.Windows.Forms.Label
    Friend WithEvents txtAmortization As System.Windows.Forms.TextBox
    Friend WithEvents lblTo As System.Windows.Forms.Label
    Friend WithEvents lblInterest As System.Windows.Forms.Label
    Friend WithEvents lblPrincipal As System.Windows.Forms.Label
    Friend WithEvents txtInterest As System.Windows.Forms.TextBox
    Friend WithEvents txtPrincipal As System.Windows.Forms.TextBox
    Friend WithEvents txtCapitalShare As System.Windows.Forms.TextBox
    Friend WithEvents txtServiceFee As System.Windows.Forms.TextBox
    Friend WithEvents lblCapitalShare As System.Windows.Forms.Label
    Friend WithEvents lblServiceFee As System.Windows.Forms.Label
    Friend WithEvents txtBalance As System.Windows.Forms.TextBox
    Friend WithEvents txtTerms As System.Windows.Forms.TextBox
    Friend WithEvents txtNetProceeds As System.Windows.Forms.TextBox
    Friend WithEvents txtInterestRefund As System.Windows.Forms.TextBox
    Friend WithEvents txtPenalty As System.Windows.Forms.TextBox
    Friend WithEvents lblBalance As System.Windows.Forms.Label
    Friend WithEvents lblTerms As System.Windows.Forms.Label
    Friend WithEvents lblNetProceeds As System.Windows.Forms.Label
    Friend WithEvents lblInterestRefund As System.Windows.Forms.Label
    Friend WithEvents lblPenalty As System.Windows.Forms.Label
    Friend WithEvents cmdCheck As System.Windows.Forms.Button
    Friend WithEvents txtSearchnumber As System.Windows.Forms.TextBox
    Friend WithEvents cmdSearch As System.Windows.Forms.Button
    Friend WithEvents optName As System.Windows.Forms.RadioButton
    Friend WithEvents optNumber As System.Windows.Forms.RadioButton
    Friend WithEvents txtSearchLastname As System.Windows.Forms.TextBox
    Friend WithEvents lstNumber As System.Windows.Forms.ListBox
    Friend WithEvents lstNames As System.Windows.Forms.ListBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents PanePanel5 As WindowsApplication2.PanePanel
    Friend WithEvents PanePanel1 As WindowsApplication2.PanePanel
    Friend WithEvents PanePanel2 As WindowsApplication2.PanePanel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents PanePanel3 As WindowsApplication2.PanePanel
    Friend WithEvents btnview As System.Windows.Forms.Button
    Friend WithEvents btnclose As System.Windows.Forms.Button
    Friend WithEvents lstExistingLoans As System.Windows.Forms.ListView
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents CboLoanType As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents CboLoanStatus As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents CboLoanNumber As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents DTLoan As System.Windows.Forms.DateTimePicker
    Friend WithEvents laonDTto As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabMember As System.Windows.Forms.TabPage
    Friend WithEvents TabTransaction As System.Windows.Forms.TabPage
    Friend WithEvents ListStatus As System.Windows.Forms.ListView
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtpagibigloan As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtSSSloan As System.Windows.Forms.TextBox
    Friend WithEvents txtSalaryMonthly As System.Windows.Forms.TextBox
    Friend WithEvents txtphihealthloan As System.Windows.Forms.TextBox
    Friend WithEvents txtothers As System.Windows.Forms.TextBox
    Friend WithEvents txtcompanyloan As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Btnprint As System.Windows.Forms.Button
    Friend WithEvents btnEditAmort As System.Windows.Forms.Button
    Friend WithEvents panelLoanProcessing As System.Windows.Forms.Panel
    Friend WithEvents txtmembername As System.Windows.Forms.TextBox
    Friend WithEvents tabLoanDetails As System.Windows.Forms.TabControl
    Friend WithEvents loan As System.Windows.Forms.TabPage
    Friend WithEvents amortization As System.Windows.Forms.TabPage
    Friend WithEvents maxLoanAmt As System.Windows.Forms.TabPage
    Friend WithEvents txtPayment As System.Windows.Forms.TextBox
    Friend WithEvents lblPayment As System.Windows.Forms.Label
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents panelLoansFilter As System.Windows.Forms.Panel
    Friend WithEvents SplitContainer2 As System.Windows.Forms.SplitContainer
    Friend WithEvents SplitContainer3 As System.Windows.Forms.SplitContainer
    Friend WithEvents btnChangeLoanNo As System.Windows.Forms.Button
    Friend WithEvents btnValidateLoans As System.Windows.Forms.Button
    Friend WithEvents btnChangeDateEnd As System.Windows.Forms.Button
    Friend WithEvents btnChangeDateStart As System.Windows.Forms.Button
    Friend WithEvents dteAmortEnd As System.Windows.Forms.DateTimePicker
    Friend WithEvents dteAmortStart As System.Windows.Forms.DateTimePicker
    Friend WithEvents Comaker As System.Windows.Forms.TabPage
    Friend WithEvents dgvComaker As System.Windows.Forms.DataGridView
    Friend WithEvents btnPrintStatement As System.Windows.Forms.Button
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents ViewRequirementToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RequirementsForApprovalToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PrintToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LoanApplicationFormToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LoanAgreementToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CloseToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DeleteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SearchToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents pnlLoanDetails As System.Windows.Forms.Panel
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents txtDNetProceeds As System.Windows.Forms.TextBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents txtDInt As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents txtDterms As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txtdDate As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents txtDPrincipal As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txtDLoanNo As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents dgvDeductions As System.Windows.Forms.DataGridView
End Class
