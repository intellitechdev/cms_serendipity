﻿Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class frmAdmin_UpdateRights
    Private gCon As New Clsappconfiguration

    Private Sub frmAdmin_UpdateRights_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Txtusername.Clear()
        GrdRights.Columns.Clear()
        GrdRights.Enabled = False
        BtnUpdate.Enabled = False
    End Sub

    Private Sub BtnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnUpdate.Click
        Update_Rights()
    End Sub

    Private Sub BtnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Me.Close()
    End Sub

    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Search_User(Me.Txtusername.Text)
    End Sub

    Private Sub Search_User(ByVal Username As String)
        Try
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.StoredProcedure, "CIMS_USERRIGHTS_LISTBYUSER",
                                          New SqlParameter("@UserID", Username))
            If ds.Tables.Item(0).Rows.Count <> 0 Then
                With GrdRights
                    .DataSource = ds.Tables(0)
                    .Columns(0).Visible = False

                    .Columns(1).HeaderText = "Code"
                    .Columns(1).ReadOnly = True

                    .Columns(2).HeaderText = "Description"
                    .Columns(2).Width = "139"
                    .Columns(2).ReadOnly = True

                    .Columns(3).HeaderText = "Allowed"
                    .Columns(3).Width = "62"
                End With
                GrdRights.Enabled = True
                BtnUpdate.Enabled = True
            Else
                GrdRights.Columns.Clear()
                GrdRights.Enabled = False
                BtnUpdate.Enabled = False
                MessageBox.Show("User not found!", "User Rights", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Access to sqlserver Insert Method", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub Update_Rights()
        Dim n As Integer
        Try
            For n = 0 To (GrdRights.RowCount - 1)
                SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "CIMS_USERRIGHTS_UPDATE", _
                    New SqlParameter("@UserID", Me.Txtusername.Text), _
                    New SqlParameter("@Fk_ModuleID", Me.GrdRights.Rows(n).Cells("Fk_ModuleID").Value.ToString), _
                    New SqlParameter("@IsAllowed", Me.GrdRights.Rows(n).Cells("IsAllowed").Value.ToString))
            Next
            MessageBox.Show("User Rights Updated!", "User Rights", MessageBoxButtons.OK)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Access to sqlserver Insert Method", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

End Class