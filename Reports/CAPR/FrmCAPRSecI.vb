Imports system.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class FrmCAPRSecI
    Public xParent As frmCAPRViewer
    Private gCon As New Clsappconfiguration

    Private Sub ShowError(ByVal FunctionOrSubName As String, ByVal ModuleName As Form, ByVal ErrorMsg As String)
        MsgBox("Error at " & FunctionOrSubName & " in " & ModuleName.Name.ToString & "." & vbCr & vbCr & ErrorMsg, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error occur!")
    End Sub
    Private Sub LoadIHData()
        Dim sSqlCmd As String = "dbo.CIMS_Reports_InfoOnMembersOfCoop"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                While rd.Read
                    Select Case rd.Item("Particulars").ToString
                        Case "No. of Regular Members"
                            txtRegMale.Text = rd.Item("Male")
                            txtRegFemale.Text = rd.Item("Female")
                            txtTotalReg.Text = rd.Item("Total")

                        Case "No. of Associate Members"
                            txtAssMale.Text = rd.Item("Male")
                            txtAssFemale.Text = rd.Item("Female")
                            txtTotalAss.Text = rd.Item("Total")
                    End Select
                End While
            End Using
        Catch ex As Exception
            ShowError("LoadIHData", Me, ex.Message)
        End Try

        txtTotalMale.Text = CDec(txtRegMale.Text) + CDec(txtAssMale.Text)
        txtTotalFemale.Text = CDec(txtRegFemale.Text) + CDec(txtAssFemale.Text)
        txtTotalMember.Text = CDec(txtTotalMale.Text) + CDec(txtTotalFemale.Text)
    End Sub
    Private Sub FrmCAPRSecI_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        txtCIN.Text = CINumber
        txtCoopName.Text = CoopNameLatestAmendment
        txtCoopAdd.Text = PresentCoopAddress
        txtRegConNo.Text = RegConfirmNo
        LoadIHData()
    End Sub

    Private Sub BtnNextOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnNextOK.Click
        Box3FinancialInterm = ChkFinance.Checked
        Box3Mining = ChkMining.Checked
        Box3Construction = ChkConstruction.Checked
        Box3TransStorageComm = ChkTransport.Checked
        Box3RealEstateRentBussAct = ChkRealEstate.Checked
        Box3HealthSocialWork = ChkHealth.Checked
        Box3Fishing = ChkFishing.Checked
        Box3ElecGasWater = ChkElectricity.Checked
        Box3Education = ChkEducation.Checked
        Box3AgriHuntForst = ChkAgriculture.Checked
        Box3Manufctring = ChkManufacturing.Checked
        Box3HotelResto = ChkHotel.Checked
        Box3WholeSaleRetail = ChkWholesaleRetail.Checked
        Box3Others = ChkOthers.Checked
        RegConfirmNo = txtRegConNo.Text
        CoopType = txtCoopType.Text
        BussActEngaged = txtBusActEngaged.Text
        If ChkOthers.Checked = True Then
            Box3OtherSPecified = txtOthers.Text
        Else
            Box3OtherSPecified = ""
        End If
        ContactPerson = IIf(txtContactName.Text = "", " ", txtContactName.Text)
        PhoneNo = IIf(txtPhoneNo.Text = "", " ", txtPhoneNo.Text)
        FAX = IIf(txtFAX.Text = "", " ", txtFAX.Text)
        Email = IIf(txtEmail.Text = "", " ", txtEmail.Text)

        'Label1.Visible = True
        'BGWorker1stPage.RunWorkerAsync()

        IHTargetMale = CDec(txtTargetMale.Text)
        IHTargeFemale = CDec(txtTargetFemale.Text)
        Ii1a = IIf(OpCtrl1a.Yes = True, "Yes", "No")
        Ii1b = IIf(OpCtrl1b.Yes = True, "Yes", "No")
        I1bif = IIf(OpCtrl1bIf.Yes = True, "Yes", "No")
        Ii1c = IIf(OpCtrl1c.Yes = True, "Yes", "No")
        Ii1d = IIf(OpCtrl1d.Yes = True, "Yes", "No")
        Ii2a = IIf(OpCtrl2a.Yes = True, "Yes", "No")
        Ii2b = IIf(OpCtrl2b.Yes = True, "Yes", "No")
        Ii2c = IIf(OpCtrl2c.Yes = True, "Yes", "No")
        Ii2d = IIf(OpCtrl2d.Yes = True, "Yes", "No")
        Ii3a = IIf(OpCtrl3a.Yes = True, "Yes", "No")
        Ii4a = IIf(OpCtrl4a.Yes = True, "Yes", "No")
        Ii4bi = IIf(OpCtrl4bi.Yes = True, "Yes", "No")
        Ii4bii = IIf(OpCtrl4bii.Yes = True, "Yes", "No")
        Ii4biii = IIf(OpCtrl4biii.Yes = True, "Yes", "No")
        Ii4c = IIf(OpCtrl4c.Yes = True, "Yes", "No")
        Ii4d = IIf(OpCtrl4d.Yes = True, "Yes", "No")
        Ii5a = IIf(OpCtrl5a.Yes = True, "Yes", "No")
        Ii5b = IIf(OpCtrl5b.Yes = True, "Yes", "No")
        Ii5c = IIf(OpCtrl5c.Yes = True, "Yes", "No")
        Ii5d = IIf(OpCtrl5d.Yes = True, "Yes", "No")
        Ii5e = IIf(OpCtrl5e.Yes = True, "Yes", "No")
        Ii6a = IIf(OpCtrl6a.Yes = True, "Yes", "No")
        Ii6b = IIf(OpCtrl6b.Yes = True, "Yes", "No")
        Ii7a = IIf(OpCtrl7a.Yes = True, "Yes", "No")
        Ii7aIf = IIf(OpCtrl7aIf.Yes = True, "Yes", "No")
        Ii7b = IIf(OpCtrl7b.Yes = True, "Yes", "No")
        Ii7c = IIf(OpCtrl7c.Yes = True, "Yes", "No")
        Ii7d = IIf(OpCtrl7d.Yes = True, "Yes", "No")
        Ii7e = IIf(OpCtrl7e.Yes = True, "Yes", "No")

        Ii8a = IIf(OpCtrl8a.Yes = True, "Yes", "No")
        Ii8b = IIf(OpCtrl8b.Yes = True, "Yes", "No")
        Ii8c = IIf(OpCtrl8c.Yes = True, "Yes", "No")
        Ii8d = IIf(OpCtrl8d.Yes = True, "Yes", "No")
        Ii8ei = IIf(OpCtrl8ei.Yes = True, "Yes", "No")
        Ii8eii = IIf(OpCtrl8eii.Yes = True, "Yes", "No")
        Ii8eiii = IIf(OpCtrl8eiii.Yes = True, "Yes", "No")
        Ii8fi = IIf(OpCtrl8fi.Yes = True, "Yes", "No")
        Ii8fii = IIf(OpCtrl8fii.Yes = True, "Yes", "No")
        Ii8fiii = IIf(OpCtrl8fiii.Yes = True, "Yes", "No")
        Ii8fiv = IIf(OpCtrl8fiv.Yes = True, "Yes", "No")
        Ii8fv = IIf(OpCtrl8fv.Yes = True, "Yes", "No")
        Ii9a = IIf(OpCtrl9a.Yes = True, "Yes", "No")
        Ii9b = IIf(OpCtrl9b.Yes = True, "Yes", "No")
        Ii9c = IIf(OpCtrl9c.Yes = True, "Yes", "No")
        Ii9d = IIf(OpCtrl9d.Yes = True, "Yes", "No")
        Ii9e = IIf(OpCtrl9e.Yes = True, "Yes", "No")
        Ii9f = IIf(OpCtrl9f.Yes = True, "Yes", "No")
        Ii9g = IIf(OpCtrl9g.Yes = True, "Yes", "No")
        Ii9h = IIf(OpCtrl9h.Yes = True, "Yes", "No")
        Ii9i = IIf(OpCtrl9i.Yes = True, "Yes", "No")
        Ii9j = IIf(OpCtrl9j.Yes = True, "Yes", "No")
        Ii9k = IIf(OpCtrl9k.Yes = True, "Yes", "No")
        Ii10a = IIf(OpCtrl10a.Yes = True, "Yes", "No")
        Ii10b = IIf(OpCtrl10b.Yes = True, "Yes", "No")
        Ii10c = IIf(OpCtrl10c.Yes = True, "Yes", "No")
        Ii11 = IIf(OpCtrl11.Yes = True, "Yes", "No")
        Ij1a = NumFreqIntAudit.Value
        Ij1b = dtLastIntAudRpt.Value.Date
        Ij1cName = txtIntAudName.Text
        Ij1cDesignation = txtIntAudDesign.Text
        Ij2a = dtExtAud.Value.Date
        Ij2b = txtPeriodOpCoverd.Text
        Ij2c = txtExtAudName.Text
        Ij3 = CboAudFinState.Text

        '.Label2.Visible = True
        '.Label3.Visible = True
        '.BGWorker2nd3rdPages.RunWorkerAsync()
        Me.Hide()
        If CboAudFinState.Text = "None" Then
            xSectionSwitch = False
        Else
            xSectionSwitch = True
        End If

        If xSectionSwitch = True Then
            xFormSec3.Show()
        Else
            xFormSec2.Show()
        End If
    End Sub

    Private Sub BtnPrevious_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnPrevious.Click
        With xParent
            xFormItro.Show()
            Me.Hide()
        End With
    End Sub

    Private Sub ChkOthers_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ChkOthers.CheckedChanged
        txtOthers.Enabled = ChkOthers.Checked
    End Sub

    Private Sub txtTargetMale_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtTargetMale.KeyPress, txtTargetFemale.KeyPress
        If Not (e.KeyChar.IsDigit(e.KeyChar)) And _
               e.KeyChar <> ChrW(Keys.Back) Then
            e.Handled = True
        End If
    End Sub

    Private Sub txtTargetMale_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTargetMale.LostFocus
        If txtTargetMale.Text = "" Then
            txtTargetMale.Text = 0
        End If
    End Sub

    Private Sub txtTargetFemale_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTargetFemale.LostFocus
        If txtTargetFemale.Text = "" Then
            txtTargetFemale.Text = 0
        End If
    End Sub

    Private Sub txtTargetMale_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTargetMale.TextChanged
        If txtTargetMale.Text = "" Then
            txtTargetMale.Text = "0"
        End If
        txtTotalTarget.Text = (Format(IIf(txtTargetMale.Text = "", "0", CDec(txtTargetMale.Text)) + IIf(txtTargetFemale.Text = "", "0", CDec(txtTargetFemale.Text)), "##,##0"))
    End Sub

    Private Sub txtTargetFemale_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTargetFemale.TextChanged
        If txtTargetFemale.Text = "" Then
            txtTargetFemale.Text = "0"
        End If
        txtTotalTarget.Text = (Format(CDec(IIf(txtTargetMale.Text = "", "0", txtTargetMale.Text)) + _
                                            CDec(IIf(txtTargetFemale.Text = "", "0", txtTargetFemale.Text)), "##,##0"))
    End Sub
End Class