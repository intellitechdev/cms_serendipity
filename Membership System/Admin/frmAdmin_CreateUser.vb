Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class frmAdmin_CreateUser
    Dim userCount As String = ""
    Private gCon As New Clsappconfiguration

    Private Sub BtnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Me.Close()
    End Sub

    Private Sub BtnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnOK.Click
        If Confirm_Password() = False Then
            MessageBox.Show("Password Don't match", "New User", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Else
            Call CreateNewuser()
        End If
    End Sub

#Region "Create New User"
    Private Sub CreateNewuser()
        If Me.Txtusername.Text <> "" Or Me.TxtPassword.Text <> "" Then
            gCon.sqlconn.Open()
            Dim cmd As New SqlCommand("usp_per_Employee_Login_Insert", gCon.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            With cmd.Parameters
                .Add("@Username", SqlDbType.VarChar, 50).Value = Me.Txtusername.Text
                .Add("@Password", SqlDbType.VarChar, 50).Value = Me.TxtPassword.Text
                .Add("@email", SqlDbType.VarChar, 50).Value = "User"
            End With
            cmd.ExecuteNonQuery()
            User_Rights_INSERT()
            gCon.sqlconn.Close()
            Me.Close()
        End If
    End Sub
#End Region

    Private Sub frmAdmin_CreateUser_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        TxtPassword.Clear()
        Txtusername.Clear()
        Check_User()
        User_Count()
        User_Rights_List()
    End Sub
    Private Sub Check_User()
        Dim con As New Clsappconfiguration
        Dim sSqlCmd As String = "SELECT COUNT (Emp_ID) FROM Employee_Login"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(con.sqlconn, CommandType.Text, sSqlCmd)
                While rd.Read
                    userCount = rd.Item(0).ToString
                End While
            End Using
        Catch ex As Exception

        End Try
    End Sub
    Private Sub User_Count()
        If userCount = "4" Then
            MessageBox.Show("User Account is on Maximum Number", "Create Users", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Me.Close()
        Else
            Exit Sub
        End If
    End Sub

    Private Sub User_Rights_List()
        Try
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.StoredProcedure, "CIMS_USERRIGHTS_LIST")
            With GrdRights
                .DataSource = ds.Tables(0)
                .Columns(0).HeaderText = "Code"
                .Columns(0).ReadOnly = True
                .Columns(1).HeaderText = "Description"
                .Columns(1).ReadOnly = True
                .Columns(1).Width = "200"
                .Columns(2).HeaderText = "Allowed"
                .Columns(2).Width = "67"
            End With
        Catch ex As Exception
        End Try
    End Sub

    Private Sub User_Rights_INSERT()
        Dim n As Integer
        Try
            For n = 0 To (GrdRights.RowCount - 1)
                SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "CIMS_USERRIGHTS_INSERT", _
                    New SqlParameter("@Fk_ModuleID", Me.GrdRights.Rows(n).Cells("PK_Module").Value), _
                    New SqlParameter("@UserID", Me.Txtusername.Text), _
                    New SqlParameter("@IsAllowed", Me.GrdRights.Rows(n).Cells("IsAllowed").Value.ToString))
            Next
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Access to sqlserver Insert Method", MessageBoxButtons.OK)
        End Try
    End Sub

    Private Function Confirm_Password() As Boolean
        If TxtPassword.Text = txtConfirmPassword.Text Then
            Confirm_Password = True
            Return Confirm_Password
        Else
            Confirm_Password = False
            Return Confirm_Password
        End If
    End Function
End Class