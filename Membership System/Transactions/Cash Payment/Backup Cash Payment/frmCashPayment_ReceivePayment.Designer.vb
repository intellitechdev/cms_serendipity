<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCashPayment_ReceivePayment
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCashPayment_ReceivePayment))
        Me.lblName = New System.Windows.Forms.Label
        Me.lblEmpNo = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtOR = New System.Windows.Forms.TextBox
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.txtTerm = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtPayment = New System.Windows.Forms.TextBox
        Me.txtChkNo = New System.Windows.Forms.TextBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.MtcboPymntMethd = New MTGCComboBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.lblLoan = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.lblLoanNo = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.txtMemo = New System.Windows.Forms.TextBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.MTcboBackAcnt = New MTGCComboBox
        Me.DtDate = New System.Windows.Forms.DateTimePicker
        Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker
        Me.btnCancel = New System.Windows.Forms.Button
        Me.btnPay = New System.Windows.Forms.Button
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblName
        '
        Me.lblName.Font = New System.Drawing.Font("Calibri", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.Location = New System.Drawing.Point(112, 14)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(279, 28)
        Me.lblName.TabIndex = 1
        Me.lblName.Text = "[Name]"
        '
        'lblEmpNo
        '
        Me.lblEmpNo.AutoSize = True
        Me.lblEmpNo.Location = New System.Drawing.Point(112, 41)
        Me.lblEmpNo.Name = "lblEmpNo"
        Me.lblEmpNo.Size = New System.Drawing.Size(136, 15)
        Me.lblEmpNo.TabIndex = 2
        Me.lblEmpNo.Text = "Employee No.: [000000]"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(391, 28)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(50, 15)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "OR. No.:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(406, 58)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(35, 15)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "Date:"
        '
        'txtOR
        '
        Me.txtOR.Location = New System.Drawing.Point(447, 25)
        Me.txtOR.Name = "txtOR"
        Me.txtOR.Size = New System.Drawing.Size(131, 23)
        Me.txtOR.TabIndex = 5
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel1.Controls.Add(Me.txtTerm)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.txtPayment)
        Me.Panel1.Controls.Add(Me.txtChkNo)
        Me.Panel1.Controls.Add(Me.Label9)
        Me.Panel1.Controls.Add(Me.MtcboPymntMethd)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.lblLoan)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.lblLoanNo)
        Me.Panel1.Location = New System.Drawing.Point(7, 106)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(571, 114)
        Me.Panel1.TabIndex = 6
        '
        'txtTerm
        '
        Me.txtTerm.Location = New System.Drawing.Point(106, 66)
        Me.txtTerm.Name = "txtTerm"
        Me.txtTerm.Size = New System.Drawing.Size(63, 23)
        Me.txtTerm.TabIndex = 11
        Me.txtTerm.Text = "0"
        Me.txtTerm.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTerm.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(69, 69)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(37, 15)
        Me.Label1.TabIndex = 10
        Me.Label1.Text = "Term:"
        Me.Label1.Visible = False
        '
        'txtPayment
        '
        Me.txtPayment.Font = New System.Drawing.Font("Calibri", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPayment.Location = New System.Drawing.Point(411, 69)
        Me.txtPayment.Name = "txtPayment"
        Me.txtPayment.Size = New System.Drawing.Size(151, 33)
        Me.txtPayment.TabIndex = 5
        Me.txtPayment.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtChkNo
        '
        Me.txtChkNo.Location = New System.Drawing.Point(411, 31)
        Me.txtChkNo.Name = "txtChkNo"
        Me.txtChkNo.Size = New System.Drawing.Size(151, 23)
        Me.txtChkNo.TabIndex = 5
        Me.txtChkNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Calibri", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(305, 69)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(106, 29)
        Me.Label9.TabIndex = 4
        Me.Label9.Text = "Payment:"
        '
        'MtcboPymntMethd
        '
        Me.MtcboPymntMethd.ArrowBoxColor = System.Drawing.SystemColors.Control
        Me.MtcboPymntMethd.ArrowColor = System.Drawing.Color.Black
        Me.MtcboPymntMethd.BindedControl = CType(resources.GetObject("MtcboPymntMethd.BindedControl"), MTGCComboBox.ControlloAssociato)
        Me.MtcboPymntMethd.BorderStyle = MTGCComboBox.TipiBordi.Fixed3D
        Me.MtcboPymntMethd.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.MtcboPymntMethd.ColumnNum = 2
        Me.MtcboPymntMethd.ColumnWidth = "100;0"
        Me.MtcboPymntMethd.DisabledArrowBoxColor = System.Drawing.SystemColors.Control
        Me.MtcboPymntMethd.DisabledArrowColor = System.Drawing.Color.LightGray
        Me.MtcboPymntMethd.DisabledBackColor = System.Drawing.SystemColors.Control
        Me.MtcboPymntMethd.DisabledBorderColor = System.Drawing.SystemColors.InactiveBorder
        Me.MtcboPymntMethd.DisabledForeColor = System.Drawing.SystemColors.GrayText
        Me.MtcboPymntMethd.DisplayMember = "Text"
        Me.MtcboPymntMethd.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.MtcboPymntMethd.DropDownBackColor = System.Drawing.Color.FromArgb(CType(CType(193, Byte), Integer), CType(CType(210, Byte), Integer), CType(CType(238, Byte), Integer))
        Me.MtcboPymntMethd.DropDownForeColor = System.Drawing.Color.Black
        Me.MtcboPymntMethd.DropDownStyle = MTGCComboBox.CustomDropDownStyle.DropDown
        Me.MtcboPymntMethd.DropDownWidth = 270
        Me.MtcboPymntMethd.GridLineColor = System.Drawing.Color.LightGray
        Me.MtcboPymntMethd.GridLineHorizontal = False
        Me.MtcboPymntMethd.GridLineVertical = True
        Me.MtcboPymntMethd.LoadingType = MTGCComboBox.CaricamentoCombo.ComboBoxItem
        Me.MtcboPymntMethd.Location = New System.Drawing.Point(106, 31)
        Me.MtcboPymntMethd.ManagingFastMouseMoving = True
        Me.MtcboPymntMethd.ManagingFastMouseMovingInterval = 30
        Me.MtcboPymntMethd.MaxDropDownItems = 20
        Me.MtcboPymntMethd.Name = "MtcboPymntMethd"
        Me.MtcboPymntMethd.SelectedItem = Nothing
        Me.MtcboPymntMethd.SelectedValue = Nothing
        Me.MtcboPymntMethd.Size = New System.Drawing.Size(193, 24)
        Me.MtcboPymntMethd.TabIndex = 9
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(342, 34)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(63, 15)
        Me.Label8.TabIndex = 4
        Me.Label8.Text = "Check No.:"
        '
        'lblLoan
        '
        Me.lblLoan.AutoSize = True
        Me.lblLoan.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoan.Location = New System.Drawing.Point(3, 7)
        Me.lblLoan.Name = "lblLoan"
        Me.lblLoan.Size = New System.Drawing.Size(84, 23)
        Me.lblLoan.TabIndex = 0
        Me.lblLoan.Text = "Loan No.:"
        Me.lblLoan.Visible = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(4, 34)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(102, 15)
        Me.Label7.TabIndex = 2
        Me.Label7.Text = "Payment Method:"
        '
        'lblLoanNo
        '
        Me.lblLoanNo.AutoSize = True
        Me.lblLoanNo.Location = New System.Drawing.Point(93, 13)
        Me.lblLoanNo.Name = "lblLoanNo"
        Me.lblLoanNo.Size = New System.Drawing.Size(59, 15)
        Me.lblLoanNo.TabIndex = 2
        Me.lblLoanNo.Text = "[Loan No]"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(13, 226)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(44, 15)
        Me.Label10.TabIndex = 7
        Me.Label10.Text = "Memo:"
        '
        'txtMemo
        '
        Me.txtMemo.Location = New System.Drawing.Point(103, 223)
        Me.txtMemo.Multiline = True
        Me.txtMemo.Name = "txtMemo"
        Me.txtMemo.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtMemo.Size = New System.Drawing.Size(249, 52)
        Me.txtMemo.TabIndex = 8
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(13, 284)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(84, 15)
        Me.Label11.TabIndex = 7
        Me.Label11.Text = "Bank Account:"
        '
        'MTcboBackAcnt
        '
        Me.MTcboBackAcnt.ArrowBoxColor = System.Drawing.SystemColors.Control
        Me.MTcboBackAcnt.ArrowColor = System.Drawing.Color.Black
        Me.MTcboBackAcnt.BindedControl = CType(resources.GetObject("MTcboBackAcnt.BindedControl"), MTGCComboBox.ControlloAssociato)
        Me.MTcboBackAcnt.BorderStyle = MTGCComboBox.TipiBordi.Fixed3D
        Me.MTcboBackAcnt.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.MTcboBackAcnt.ColumnNum = 2
        Me.MTcboBackAcnt.ColumnWidth = "250;0"
        Me.MTcboBackAcnt.DisabledArrowBoxColor = System.Drawing.SystemColors.Control
        Me.MTcboBackAcnt.DisabledArrowColor = System.Drawing.Color.LightGray
        Me.MTcboBackAcnt.DisabledBackColor = System.Drawing.SystemColors.Control
        Me.MTcboBackAcnt.DisabledBorderColor = System.Drawing.SystemColors.InactiveBorder
        Me.MTcboBackAcnt.DisabledForeColor = System.Drawing.SystemColors.GrayText
        Me.MTcboBackAcnt.DisplayMember = "Text"
        Me.MTcboBackAcnt.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.MTcboBackAcnt.DropDownBackColor = System.Drawing.Color.FromArgb(CType(CType(193, Byte), Integer), CType(CType(210, Byte), Integer), CType(CType(238, Byte), Integer))
        Me.MTcboBackAcnt.DropDownForeColor = System.Drawing.Color.Black
        Me.MTcboBackAcnt.DropDownStyle = MTGCComboBox.CustomDropDownStyle.DropDown
        Me.MTcboBackAcnt.DropDownWidth = 270
        Me.MTcboBackAcnt.GridLineColor = System.Drawing.Color.LightGray
        Me.MTcboBackAcnt.GridLineHorizontal = False
        Me.MTcboBackAcnt.GridLineVertical = True
        Me.MTcboBackAcnt.LoadingType = MTGCComboBox.CaricamentoCombo.ComboBoxItem
        Me.MTcboBackAcnt.Location = New System.Drawing.Point(103, 281)
        Me.MTcboBackAcnt.ManagingFastMouseMoving = True
        Me.MTcboBackAcnt.ManagingFastMouseMovingInterval = 30
        Me.MTcboBackAcnt.MaxDropDownItems = 20
        Me.MTcboBackAcnt.Name = "MTcboBackAcnt"
        Me.MTcboBackAcnt.SelectedItem = Nothing
        Me.MTcboBackAcnt.SelectedValue = Nothing
        Me.MTcboBackAcnt.Size = New System.Drawing.Size(249, 24)
        Me.MTcboBackAcnt.TabIndex = 9
        '
        'DtDate
        '
        Me.DtDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DtDate.Location = New System.Drawing.Point(447, 54)
        Me.DtDate.Name = "DtDate"
        Me.DtDate.Size = New System.Drawing.Size(131, 23)
        Me.DtDate.TabIndex = 11
        '
        'BackgroundWorker1
        '
        '
        'btnCancel
        '
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Image = Global.WindowsApplication2.My.Resources.Resources.button_cancel
        Me.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnCancel.Location = New System.Drawing.Point(495, 278)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(83, 27)
        Me.btnCancel.TabIndex = 13
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnPay
        '
        Me.btnPay.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPay.Image = Global.WindowsApplication2.My.Resources.Resources.apply
        Me.btnPay.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnPay.Location = New System.Drawing.Point(409, 278)
        Me.btnPay.Name = "btnPay"
        Me.btnPay.Size = New System.Drawing.Size(83, 27)
        Me.btnPay.TabIndex = 12
        Me.btnPay.Text = "Post"
        Me.btnPay.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnPay.UseVisualStyleBackColor = True
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.WindowsApplication2.My.Resources.Resources.cash_in_hand
        Me.PictureBox1.Location = New System.Drawing.Point(7, 14)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(99, 86)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'frmCashPayment_ReceivePayment
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Window
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(583, 311)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnPay)
        Me.Controls.Add(Me.DtDate)
        Me.Controls.Add(Me.MTcboBackAcnt)
        Me.Controls.Add(Me.txtMemo)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.txtOR)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.lblEmpNo)
        Me.Controls.Add(Me.lblName)
        Me.Controls.Add(Me.PictureBox1)
        Me.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frmCashPayment_ReceivePayment"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Receive Payments"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents lblEmpNo As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtOR As System.Windows.Forms.TextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lblLoan As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents lblLoanNo As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtPayment As System.Windows.Forms.TextBox
    Friend WithEvents txtChkNo As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtMemo As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents MTcboBackAcnt As MTGCComboBox
    Friend WithEvents DtDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents MtcboPymntMethd As MTGCComboBox
    Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
    Friend WithEvents btnPay As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents txtTerm As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class
