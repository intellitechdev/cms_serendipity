<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLoan_Policy_Master
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLoan_Policy_Master))
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtcode = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.chkCS1 = New System.Windows.Forms.CheckBox()
        Me.chkTenure = New System.Windows.Forms.CheckBox()
        Me.chkMaxTerms = New System.Windows.Forms.CheckBox()
        Me.gridPolicies = New System.Windows.Forms.DataGridView()
        Me.btnViewshare = New System.Windows.Forms.Button()
        Me.btnViewMaxterms = New System.Windows.Forms.Button()
        Me.chkCoopTen = New System.Windows.Forms.CheckBox()
        Me.chkCompTen = New System.Windows.Forms.CheckBox()
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        Me.txtpolicydesc = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txttempcode = New System.Windows.Forms.TextBox()
        Me.chkApplyPositionTypeLimit = New System.Windows.Forms.CheckBox()
        Me.btnViewPositionTypePolicies = New System.Windows.Forms.Button()
        Me.picStep1 = New WindowsApplication2.PanePanel()
        Me.lblStepone = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btnclose = New System.Windows.Forms.Button()
        Me.btndelete = New System.Windows.Forms.Button()
        Me.btnupdate = New System.Windows.Forms.Button()
        Me.btnsave = New System.Windows.Forms.Button()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cboProjectName = New System.Windows.Forms.ComboBox()
        CType(Me.gridPolicies, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.picStep1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(15, 77)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(71, 15)
        Me.Label2.TabIndex = 34
        Me.Label2.Text = "Policy Code"
        '
        'txtcode
        '
        Me.txtcode.Location = New System.Drawing.Point(97, 72)
        Me.txtcode.Name = "txtcode"
        Me.txtcode.ReadOnly = True
        Me.txtcode.Size = New System.Drawing.Size(53, 23)
        Me.txtcode.TabIndex = 35
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(6, 11)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(168, 15)
        Me.Label5.TabIndex = 43
        Me.Label5.Text = "Loanable Amount Limitations"
        '
        'chkCS1
        '
        Me.chkCS1.AutoSize = True
        Me.chkCS1.Location = New System.Drawing.Point(12, 29)
        Me.chkCS1.Name = "chkCS1"
        Me.chkCS1.Size = New System.Drawing.Size(164, 19)
        Me.chkCS1.TabIndex = 44
        Me.chkCS1.Text = "Apply Capital Share Limit"
        Me.chkCS1.UseVisualStyleBackColor = True
        '
        'chkTenure
        '
        Me.chkTenure.AutoSize = True
        Me.chkTenure.Location = New System.Drawing.Point(12, 52)
        Me.chkTenure.Name = "chkTenure"
        Me.chkTenure.Size = New System.Drawing.Size(126, 19)
        Me.chkTenure.TabIndex = 45
        Me.chkTenure.Text = "Apply Tenure Limit"
        Me.chkTenure.UseVisualStyleBackColor = True
        '
        'chkMaxTerms
        '
        Me.chkMaxTerms.AutoSize = True
        Me.chkMaxTerms.Location = New System.Drawing.Point(272, 29)
        Me.chkMaxTerms.Name = "chkMaxTerms"
        Me.chkMaxTerms.Size = New System.Drawing.Size(149, 19)
        Me.chkMaxTerms.TabIndex = 50
        Me.chkMaxTerms.Text = "Apply Max Terms Limit"
        Me.chkMaxTerms.UseVisualStyleBackColor = True
        '
        'gridPolicies
        '
        Me.gridPolicies.AllowUserToAddRows = False
        Me.gridPolicies.AllowUserToDeleteRows = False
        Me.gridPolicies.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gridPolicies.BackgroundColor = System.Drawing.Color.White
        Me.gridPolicies.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gridPolicies.GridColor = System.Drawing.Color.White
        Me.gridPolicies.Location = New System.Drawing.Point(12, 223)
        Me.gridPolicies.Name = "gridPolicies"
        Me.gridPolicies.ReadOnly = True
        Me.gridPolicies.Size = New System.Drawing.Size(491, 188)
        Me.gridPolicies.TabIndex = 0
        '
        'btnViewshare
        '
        Me.btnViewshare.Location = New System.Drawing.Point(180, 27)
        Me.btnViewshare.Name = "btnViewshare"
        Me.btnViewshare.Size = New System.Drawing.Size(24, 20)
        Me.btnViewshare.TabIndex = 52
        Me.btnViewshare.Text = "..."
        Me.btnViewshare.UseVisualStyleBackColor = True
        '
        'btnViewMaxterms
        '
        Me.btnViewMaxterms.Location = New System.Drawing.Point(440, 28)
        Me.btnViewMaxterms.Name = "btnViewMaxterms"
        Me.btnViewMaxterms.Size = New System.Drawing.Size(24, 20)
        Me.btnViewMaxterms.TabIndex = 53
        Me.btnViewMaxterms.Text = "..."
        Me.btnViewMaxterms.UseVisualStyleBackColor = True
        '
        'chkCoopTen
        '
        Me.chkCoopTen.AutoSize = True
        Me.chkCoopTen.Location = New System.Drawing.Point(38, 76)
        Me.chkCoopTen.Name = "chkCoopTen"
        Me.chkCoopTen.Size = New System.Drawing.Size(183, 19)
        Me.chkCoopTen.TabIndex = 56
        Me.chkCoopTen.Text = "Based on Cooperative Tenure"
        Me.chkCoopTen.UseVisualStyleBackColor = True
        '
        'chkCompTen
        '
        Me.chkCompTen.AutoSize = True
        Me.chkCompTen.Location = New System.Drawing.Point(38, 97)
        Me.chkCompTen.Name = "chkCompTen"
        Me.chkCompTen.Size = New System.Drawing.Size(169, 19)
        Me.chkCompTen.TabIndex = 57
        Me.chkCompTen.Text = "Based on Company Tenure"
        Me.chkCompTen.UseVisualStyleBackColor = True
        '
        'txtpolicydesc
        '
        Me.txtpolicydesc.Location = New System.Drawing.Point(295, 72)
        Me.txtpolicydesc.Name = "txtpolicydesc"
        Me.txtpolicydesc.Size = New System.Drawing.Size(206, 23)
        Me.txtpolicydesc.TabIndex = 58
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(189, 75)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(107, 15)
        Me.Label6.TabIndex = 32
        Me.Label6.Text = "Policy Description"
        '
        'txttempcode
        '
        Me.txttempcode.Location = New System.Drawing.Point(153, 72)
        Me.txttempcode.Name = "txttempcode"
        Me.txttempcode.Size = New System.Drawing.Size(38, 23)
        Me.txttempcode.TabIndex = 59
        Me.txttempcode.Visible = False
        '
        'chkApplyPositionTypeLimit
        '
        Me.chkApplyPositionTypeLimit.AutoSize = True
        Me.chkApplyPositionTypeLimit.Location = New System.Drawing.Point(272, 54)
        Me.chkApplyPositionTypeLimit.Name = "chkApplyPositionTypeLimit"
        Me.chkApplyPositionTypeLimit.Size = New System.Drawing.Size(162, 19)
        Me.chkApplyPositionTypeLimit.TabIndex = 60
        Me.chkApplyPositionTypeLimit.Text = "Apply Position Type Limit"
        Me.chkApplyPositionTypeLimit.UseVisualStyleBackColor = True
        '
        'btnViewPositionTypePolicies
        '
        Me.btnViewPositionTypePolicies.Location = New System.Drawing.Point(440, 54)
        Me.btnViewPositionTypePolicies.Name = "btnViewPositionTypePolicies"
        Me.btnViewPositionTypePolicies.Size = New System.Drawing.Size(24, 20)
        Me.btnViewPositionTypePolicies.TabIndex = 61
        Me.btnViewPositionTypePolicies.Text = "..."
        Me.btnViewPositionTypePolicies.UseVisualStyleBackColor = True
        '
        'picStep1
        '
        Me.picStep1.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.picStep1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picStep1.Controls.Add(Me.lblStepone)
        Me.picStep1.Dock = System.Windows.Forms.DockStyle.Top
        Me.picStep1.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.picStep1.InactiveGradientHighColor = System.Drawing.Color.Transparent
        Me.picStep1.InactiveGradientLowColor = System.Drawing.Color.Transparent
        Me.picStep1.Location = New System.Drawing.Point(0, 0)
        Me.picStep1.Name = "picStep1"
        Me.picStep1.Size = New System.Drawing.Size(515, 40)
        Me.picStep1.TabIndex = 30
        '
        'lblStepone
        '
        Me.lblStepone.AutoSize = True
        Me.lblStepone.BackColor = System.Drawing.Color.Transparent
        Me.lblStepone.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStepone.ForeColor = System.Drawing.Color.White
        Me.lblStepone.Location = New System.Drawing.Point(3, 11)
        Me.lblStepone.Name = "lblStepone"
        Me.lblStepone.Size = New System.Drawing.Size(115, 19)
        Me.lblStepone.TabIndex = 18
        Me.lblStepone.Text = "Loan Policies"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btnViewPositionTypePolicies)
        Me.GroupBox2.Controls.Add(Me.chkApplyPositionTypeLimit)
        Me.GroupBox2.Controls.Add(Me.chkCompTen)
        Me.GroupBox2.Controls.Add(Me.chkCoopTen)
        Me.GroupBox2.Controls.Add(Me.btnViewMaxterms)
        Me.GroupBox2.Controls.Add(Me.btnViewshare)
        Me.GroupBox2.Controls.Add(Me.chkMaxTerms)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.chkTenure)
        Me.GroupBox2.Controls.Add(Me.chkCS1)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 96)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(489, 123)
        Me.GroupBox2.TabIndex = 64
        Me.GroupBox2.TabStop = False
        '
        'btnclose
        '
        Me.btnclose.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnclose.Image = CType(resources.GetObject("btnclose.Image"), System.Drawing.Image)
        Me.btnclose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnclose.Location = New System.Drawing.Point(252, 3)
        Me.btnclose.Name = "btnclose"
        Me.btnclose.Size = New System.Drawing.Size(77, 32)
        Me.btnclose.TabIndex = 9
        Me.btnclose.Text = "Close"
        Me.btnclose.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnclose.UseVisualStyleBackColor = True
        '
        'btndelete
        '
        Me.btndelete.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btndelete.Image = CType(resources.GetObject("btndelete.Image"), System.Drawing.Image)
        Me.btndelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btndelete.Location = New System.Drawing.Point(169, 3)
        Me.btndelete.Name = "btndelete"
        Me.btndelete.Size = New System.Drawing.Size(77, 32)
        Me.btndelete.TabIndex = 8
        Me.btndelete.Text = "Delete"
        Me.btndelete.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btndelete.UseVisualStyleBackColor = True
        '
        'btnupdate
        '
        Me.btnupdate.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnupdate.Image = CType(resources.GetObject("btnupdate.Image"), System.Drawing.Image)
        Me.btnupdate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnupdate.Location = New System.Drawing.Point(86, 3)
        Me.btnupdate.Name = "btnupdate"
        Me.btnupdate.Size = New System.Drawing.Size(77, 32)
        Me.btnupdate.TabIndex = 7
        Me.btnupdate.Text = "Edit"
        Me.btnupdate.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnupdate.UseVisualStyleBackColor = True
        '
        'btnsave
        '
        Me.btnsave.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnsave.Image = Global.WindowsApplication2.My.Resources.Resources.new3
        Me.btnsave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnsave.Location = New System.Drawing.Point(3, 3)
        Me.btnsave.Name = "btnsave"
        Me.btnsave.Size = New System.Drawing.Size(77, 32)
        Me.btnsave.TabIndex = 6
        Me.btnsave.Text = "New"
        Me.btnsave.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnsave.UseVisualStyleBackColor = True
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.BackColor = System.Drawing.Color.White
        Me.FlowLayoutPanel1.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.FlowLayoutPanel1.Controls.Add(Me.btnsave)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnupdate)
        Me.FlowLayoutPanel1.Controls.Add(Me.btndelete)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnclose)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 420)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(515, 38)
        Me.FlowLayoutPanel1.TabIndex = 62
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(15, 51)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(80, 15)
        Me.Label1.TabIndex = 65
        Me.Label1.Text = "Project Name"
        '
        'cboProjectName
        '
        Me.cboProjectName.FormattingEnabled = True
        Me.cboProjectName.Location = New System.Drawing.Point(97, 45)
        Me.cboProjectName.Name = "cboProjectName"
        Me.cboProjectName.Size = New System.Drawing.Size(404, 23)
        Me.cboProjectName.TabIndex = 66
        Me.cboProjectName.Text = "Select Project"
        '
        'frmLoan_Policy_Master
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(515, 458)
        Me.Controls.Add(Me.cboProjectName)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.Controls.Add(Me.gridPolicies)
        Me.Controls.Add(Me.txttempcode)
        Me.Controls.Add(Me.txtpolicydesc)
        Me.Controls.Add(Me.txtcode)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.picStep1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(515, 465)
        Me.Name = "frmLoan_Policy_Master"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Loan Policies Master"
        CType(Me.gridPolicies, System.ComponentModel.ISupportInitialize).EndInit()
        Me.picStep1.ResumeLayout(False)
        Me.picStep1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents picStep1 As WindowsApplication2.PanePanel
    Friend WithEvents lblStepone As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtcode As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents chkCS1 As System.Windows.Forms.CheckBox
    Friend WithEvents chkTenure As System.Windows.Forms.CheckBox
    Friend WithEvents chkMaxTerms As System.Windows.Forms.CheckBox
    Friend WithEvents btnViewshare As System.Windows.Forms.Button
    Friend WithEvents btnViewMaxterms As System.Windows.Forms.Button
    Friend WithEvents gridPolicies As System.Windows.Forms.DataGridView
    Friend WithEvents chkCoopTen As System.Windows.Forms.CheckBox
    Friend WithEvents chkCompTen As System.Windows.Forms.CheckBox
    Friend WithEvents FolderBrowserDialog1 As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents txtpolicydesc As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txttempcode As System.Windows.Forms.TextBox
    Friend WithEvents chkApplyPositionTypeLimit As System.Windows.Forms.CheckBox
    Friend WithEvents btnViewPositionTypePolicies As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents btnclose As System.Windows.Forms.Button
    Friend WithEvents btndelete As System.Windows.Forms.Button
    Friend WithEvents btnupdate As System.Windows.Forms.Button
    Friend WithEvents btnsave As System.Windows.Forms.Button
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cboProjectName As System.Windows.Forms.ComboBox
End Class
