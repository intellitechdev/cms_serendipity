Public Class frmRpt_ContributionBalanceMaster

    Private rptsummary As New ReportDocument
    Private gcon As New Clsappconfiguration

    Private Sub frmContributionBalanceMaster_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        rptsummary.Close()
    End Sub

    Private Sub frmContributionBalanceMaster_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        bgwContributionBalance.RunWorkerAsync()
    End Sub

    Private Sub LoadReport()
        Try
            rptsummary.Load(Application.StartupPath & "\LoanReport\ContributionBalanceMaster.rpt")
            Logon(rptsummary, gcon.Server, gcon.Database, gcon.Username, gcon.Password)
            rptsummary.Refresh()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

#Region "Default Report Process"
    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If
        Dim obj As ReportObject
        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function

    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            If (tbl.TestConnectivity()) Then
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next
        Return True
    End Function
#End Region

    Private Sub bgwContributionBalance_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgwContributionBalance.DoWork
        LoadReport()
    End Sub

    Private Sub bgwContributionBalance_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwContributionBalance.RunWorkerCompleted
        Me.crvContributionBalance.Visible = True
        Me.crvContributionBalance.ReportSource = rptsummary

        picLoading.Visible = False
    End Sub
End Class