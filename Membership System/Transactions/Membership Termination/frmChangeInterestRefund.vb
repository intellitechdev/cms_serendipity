﻿Imports System.Data

'##################################################
'### Developer: Charl Magne "Ionflux" San Pedro ###
'### Date:      09.19.2013                      ###
'##################################################

Public Class frmChangeInterestRefund

    Private SQLHelper As New NewSQLHelper
    Public xDate As DateTime
    Public EmpNo As String
    Public ParentForm As frmMembersWithdrawalReport
    Private Modification As String = ""

    Private Function ChangeAmount() As Boolean
        Dim IsChanged As Boolean = False
        Dim NewModification As String = ""
        Dim Rowvalue As Decimal = 0
        'format: <reference>▌<Amount>▄<reference>▌<Amount>

        For Each xRow As DataGridViewRow In grdInterest.Rows
            If IsDBNull(xRow.Cells("ColAmount").Value) = True Then
                Rowvalue = 0
            Else
                Rowvalue = Convert.ToDecimal(xRow.Cells("ColAmount").Value.ToString())
            End If
            If Rowvalue <> Convert.ToDecimal(xRow.Cells("ColOrigAmt").Value.ToString()) Then
                IsChanged = True
                If NewModification = "" Then
                    NewModification = xRow.Cells("ColRefNo").Value.ToString() & "▌" & xRow.Cells("ColAmount").Value.ToString()
                Else
                    NewModification &= "▄" & xRow.Cells("ColRefNo").Value.ToString() & "▌" & xRow.Cells("ColAmount").Value.ToString()
                End If
            End If
        Next

        If IsChanged = True Then
            ParentForm.EditedInterestRefund = NewModification
            Return True
        Else
            Return False
        End If
    End Function
    Private Sub ShowErrorMsg(ByVal ErrorMessage As String)
        MsgBox(ErrorMessage, MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, "Error!")
    End Sub
    Private Sub LoadInterestRefunds()
        Try
            CheckForIllegalCrossThreadCalls = False

            Dim SQLStr As String = "CIMS_sp_GetInterestRefund '" & EmpNo & "', '" & xDate & "', '" & Modification & "'"
            Dim DTSource As New DataTable()

            DTSource = SQLHelper.ExecuteDataset(SQLStr).Tables(0)
            Modification = ParentForm.EditedInterestRefund

            If Modification <> "" Then
                For Each xItem As String In Modification.Split(New Char() {"▄"c})
                    For Each xRow As DataRow In DTSource.Rows
                        If xRow("Reference").ToString() = xItem.Split(New Char() {"▌"c})(0) Then
                            xRow("Amount") = Convert.ToDecimal(xItem.Split(New Char() {"▌"c})(1))
                            Exit For
                        End If
                    Next
                Next
            End If
            grdInterest.DataSource = DTSource.DefaultView
        Catch ex As Exception
            ShowErrorMsg(ex.Message)
        End Try
    End Sub

    Private Sub frmChangeInterestRefund_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        bgLoad.RunWorkerAsync()
    End Sub
    Private Sub btnChange_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnChange.Click
        If ChangeAmount() = True Then
            Me.DialogResult = Windows.Forms.DialogResult.OK
        Else
            Me.Close()
        End If
    End Sub
    Private Sub bgLoad_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgLoad.DoWork
        LoadInterestRefunds()
    End Sub
    Private Sub bgLoad_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgLoad.RunWorkerCompleted
        btnChange.Enabled = True
        lblLoading.Visible = False
        grdInterest.Enabled = True
    End Sub
    Private Sub frmChangeInterestRefund_ResizeBegin(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ResizeBegin
        Me.Opacity = 0.6
    End Sub
    Private Sub frmChangeInterestRefund_ResizeEnd(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ResizeEnd
        Me.Opacity = 1
    End Sub
End Class