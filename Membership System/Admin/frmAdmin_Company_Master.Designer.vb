<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAdmin_Company_Master
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAdmin_Company_Master))
        Me.PanePanel1 = New WindowsApplication2.PanePanel()
        Me.PanePanel3 = New WindowsApplication2.PanePanel()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.BTnclose = New System.Windows.Forms.Button()
        Me.btnEdit = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.txtcompanyname = New System.Windows.Forms.TextBox()
        Me.txtcompanyadd3 = New System.Windows.Forms.TextBox()
        Me.txtcompanyid = New System.Windows.Forms.TextBox()
        Me.txtcompanycode = New System.Windows.Forms.TextBox()
        Me.txtcompanybranch = New System.Windows.Forms.TextBox()
        Me.txtcompanybankcode = New System.Windows.Forms.TextBox()
        Me.txtbankbrankcode = New System.Windows.Forms.TextBox()
        Me.txtbankname = New System.Windows.Forms.TextBox()
        Me.txtcompanyadd1 = New System.Windows.Forms.TextBox()
        Me.txtcompanyadd2 = New System.Windows.Forms.TextBox()
        Me.txtcompanyacctno = New System.Windows.Forms.TextBox()
        Me.txtphilhealth = New System.Windows.Forms.TextBox()
        Me.txtsss = New System.Windows.Forms.TextBox()
        Me.txtTIN = New System.Windows.Forms.TextBox()
        Me.txtbankacctno = New System.Windows.Forms.TextBox()
        Me.CompanyPicture = New System.Windows.Forms.PictureBox()
        Me.txtTelno = New System.Windows.Forms.TextBox()
        Me.txtzipcode = New System.Windows.Forms.TextBox()
        Me.btnaddcompanypicture = New System.Windows.Forms.Button()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.txtfaxno = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtrecid = New System.Windows.Forms.TextBox()
        Me.txtpicturepath = New System.Windows.Forms.TextBox()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.PanePanel4 = New WindowsApplication2.PanePanel()
        Me.PanePanel1.SuspendLayout()
        Me.PanePanel3.SuspendLayout()
        CType(Me.CompanyPicture, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.PanePanel4.SuspendLayout()
        Me.SuspendLayout()
        '
        'PanePanel1
        '
        Me.PanePanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel1.Controls.Add(Me.PanePanel3)
        Me.PanePanel1.Controls.Add(Me.Label1)
        Me.PanePanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanePanel1.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.PanePanel1.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel1.InactiveGradientHighColor = System.Drawing.Color.Green
        Me.PanePanel1.InactiveGradientLowColor = System.Drawing.Color.White
        Me.PanePanel1.Location = New System.Drawing.Point(0, 0)
        Me.PanePanel1.Name = "PanePanel1"
        Me.PanePanel1.Size = New System.Drawing.Size(546, 38)
        Me.PanePanel1.TabIndex = 0
        '
        'PanePanel3
        '
        Me.PanePanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel3.Controls.Add(Me.Label20)
        Me.PanePanel3.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanePanel3.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.PanePanel3.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel3.InactiveGradientHighColor = System.Drawing.Color.Green
        Me.PanePanel3.InactiveGradientLowColor = System.Drawing.Color.GreenYellow
        Me.PanePanel3.Location = New System.Drawing.Point(0, 0)
        Me.PanePanel3.Name = "PanePanel3"
        Me.PanePanel3.Size = New System.Drawing.Size(544, 36)
        Me.PanePanel3.TabIndex = 3
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.BackColor = System.Drawing.Color.Transparent
        Me.Label20.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label20.ForeColor = System.Drawing.Color.White
        Me.Label20.Location = New System.Drawing.Point(5, 11)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(147, 16)
        Me.Label20.TabIndex = 0
        Me.Label20.Text = "Company Information"
        Me.Label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(11, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(174, 19)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Company Information"
        '
        'BTnclose
        '
        Me.BTnclose.Location = New System.Drawing.Point(467, 3)
        Me.BTnclose.Name = "BTnclose"
        Me.BTnclose.Size = New System.Drawing.Size(66, 28)
        Me.BTnclose.TabIndex = 1
        Me.BTnclose.Text = "Close"
        Me.BTnclose.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Location = New System.Drawing.Point(395, 3)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(66, 28)
        Me.btnEdit.TabIndex = 0
        Me.btnEdit.Text = "Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(16, 58)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(88, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Company Name :"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(15, 82)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(104, 13)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Company Address 1:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(15, 108)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(104, 13)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "Company Address 2:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(15, 131)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(104, 13)
        Me.Label5.TabIndex = 5
        Me.Label5.Text = "Company Address 3:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(15, 158)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(71, 13)
        Me.Label6.TabIndex = 6
        Me.Label6.Text = "Company ID :"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(16, 184)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(85, 13)
        Me.Label7.TabIndex = 7
        Me.Label7.Text = "Company Code :"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(15, 206)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(94, 13)
        Me.Label8.TabIndex = 8
        Me.Label8.Text = "Company Branch :"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(16, 231)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(113, 13)
        Me.Label9.TabIndex = 9
        Me.Label9.Text = "Company Bank Code :"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(15, 256)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(103, 13)
        Me.Label10.TabIndex = 10
        Me.Label10.Text = "Bank Branch Code :"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(15, 305)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(103, 13)
        Me.Label11.TabIndex = 11
        Me.Label11.Text = "Company Acct. no. :"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(16, 331)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(79, 13)
        Me.Label12.TabIndex = 12
        Me.Label12.Text = "PhilHealth no. :"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(16, 357)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(52, 13)
        Me.Label13.TabIndex = 13
        Me.Label13.Text = "SSS no. :"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(16, 381)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(37, 13)
        Me.Label14.TabIndex = 14
        Me.Label14.Text = "T I N :"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(15, 407)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(84, 13)
        Me.Label15.TabIndex = 15
        Me.Label15.Text = "Bank Acct. no. :"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(16, 430)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(48, 13)
        Me.Label16.TabIndex = 16
        Me.Label16.Text = "Fax no. :"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(313, 409)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(49, 13)
        Me.Label17.TabIndex = 17
        Me.Label17.Text = "Tel. no. :"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(313, 435)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(56, 13)
        Me.Label18.TabIndex = 18
        Me.Label18.Text = "Zip Code :"
        '
        'txtcompanyname
        '
        Me.txtcompanyname.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtcompanyname.Location = New System.Drawing.Point(164, 55)
        Me.txtcompanyname.Name = "txtcompanyname"
        Me.txtcompanyname.Size = New System.Drawing.Size(360, 20)
        Me.txtcompanyname.TabIndex = 0
        '
        'txtcompanyadd3
        '
        Me.txtcompanyadd3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtcompanyadd3.Location = New System.Drawing.Point(164, 128)
        Me.txtcompanyadd3.Name = "txtcompanyadd3"
        Me.txtcompanyadd3.Size = New System.Drawing.Size(360, 20)
        Me.txtcompanyadd3.TabIndex = 3
        '
        'txtcompanyid
        '
        Me.txtcompanyid.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtcompanyid.Location = New System.Drawing.Point(164, 153)
        Me.txtcompanyid.Name = "txtcompanyid"
        Me.txtcompanyid.Size = New System.Drawing.Size(360, 20)
        Me.txtcompanyid.TabIndex = 4
        '
        'txtcompanycode
        '
        Me.txtcompanycode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtcompanycode.Location = New System.Drawing.Point(164, 178)
        Me.txtcompanycode.Name = "txtcompanycode"
        Me.txtcompanycode.Size = New System.Drawing.Size(360, 20)
        Me.txtcompanycode.TabIndex = 5
        '
        'txtcompanybranch
        '
        Me.txtcompanybranch.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtcompanybranch.Location = New System.Drawing.Point(164, 203)
        Me.txtcompanybranch.Name = "txtcompanybranch"
        Me.txtcompanybranch.Size = New System.Drawing.Size(360, 20)
        Me.txtcompanybranch.TabIndex = 6
        '
        'txtcompanybankcode
        '
        Me.txtcompanybankcode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtcompanybankcode.Location = New System.Drawing.Point(164, 227)
        Me.txtcompanybankcode.Name = "txtcompanybankcode"
        Me.txtcompanybankcode.Size = New System.Drawing.Size(360, 20)
        Me.txtcompanybankcode.TabIndex = 7
        '
        'txtbankbrankcode
        '
        Me.txtbankbrankcode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtbankbrankcode.Location = New System.Drawing.Point(164, 251)
        Me.txtbankbrankcode.Name = "txtbankbrankcode"
        Me.txtbankbrankcode.Size = New System.Drawing.Size(143, 20)
        Me.txtbankbrankcode.TabIndex = 8
        '
        'txtbankname
        '
        Me.txtbankname.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtbankname.Location = New System.Drawing.Point(164, 276)
        Me.txtbankname.Name = "txtbankname"
        Me.txtbankname.Size = New System.Drawing.Size(143, 20)
        Me.txtbankname.TabIndex = 9
        '
        'txtcompanyadd1
        '
        Me.txtcompanyadd1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtcompanyadd1.Location = New System.Drawing.Point(164, 79)
        Me.txtcompanyadd1.Name = "txtcompanyadd1"
        Me.txtcompanyadd1.Size = New System.Drawing.Size(360, 20)
        Me.txtcompanyadd1.TabIndex = 1
        '
        'txtcompanyadd2
        '
        Me.txtcompanyadd2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtcompanyadd2.Location = New System.Drawing.Point(164, 103)
        Me.txtcompanyadd2.Name = "txtcompanyadd2"
        Me.txtcompanyadd2.Size = New System.Drawing.Size(360, 20)
        Me.txtcompanyadd2.TabIndex = 2
        '
        'txtcompanyacctno
        '
        Me.txtcompanyacctno.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtcompanyacctno.Location = New System.Drawing.Point(164, 300)
        Me.txtcompanyacctno.Name = "txtcompanyacctno"
        Me.txtcompanyacctno.Size = New System.Drawing.Size(143, 20)
        Me.txtcompanyacctno.TabIndex = 10
        '
        'txtphilhealth
        '
        Me.txtphilhealth.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtphilhealth.Location = New System.Drawing.Point(164, 325)
        Me.txtphilhealth.Name = "txtphilhealth"
        Me.txtphilhealth.Size = New System.Drawing.Size(143, 20)
        Me.txtphilhealth.TabIndex = 11
        '
        'txtsss
        '
        Me.txtsss.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtsss.Location = New System.Drawing.Point(164, 350)
        Me.txtsss.Name = "txtsss"
        Me.txtsss.Size = New System.Drawing.Size(143, 20)
        Me.txtsss.TabIndex = 12
        '
        'txtTIN
        '
        Me.txtTIN.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTIN.Location = New System.Drawing.Point(164, 375)
        Me.txtTIN.Name = "txtTIN"
        Me.txtTIN.Size = New System.Drawing.Size(143, 20)
        Me.txtTIN.TabIndex = 13
        '
        'txtbankacctno
        '
        Me.txtbankacctno.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtbankacctno.Location = New System.Drawing.Point(164, 401)
        Me.txtbankacctno.Name = "txtbankacctno"
        Me.txtbankacctno.Size = New System.Drawing.Size(143, 20)
        Me.txtbankacctno.TabIndex = 14
        '
        'CompanyPicture
        '
        Me.CompanyPicture.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CompanyPicture.Location = New System.Drawing.Point(313, 254)
        Me.CompanyPicture.Name = "CompanyPicture"
        Me.CompanyPicture.Size = New System.Drawing.Size(211, 114)
        Me.CompanyPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.CompanyPicture.TabIndex = 34
        Me.CompanyPicture.TabStop = False
        '
        'txtTelno
        '
        Me.txtTelno.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTelno.Location = New System.Drawing.Point(381, 406)
        Me.txtTelno.Name = "txtTelno"
        Me.txtTelno.Size = New System.Drawing.Size(143, 20)
        Me.txtTelno.TabIndex = 16
        '
        'txtzipcode
        '
        Me.txtzipcode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtzipcode.Location = New System.Drawing.Point(381, 432)
        Me.txtzipcode.Name = "txtzipcode"
        Me.txtzipcode.Size = New System.Drawing.Size(143, 20)
        Me.txtzipcode.TabIndex = 17
        '
        'btnaddcompanypicture
        '
        Me.btnaddcompanypicture.Location = New System.Drawing.Point(313, 374)
        Me.btnaddcompanypicture.Name = "btnaddcompanypicture"
        Me.btnaddcompanypicture.Size = New System.Drawing.Size(211, 23)
        Me.btnaddcompanypicture.TabIndex = 18
        Me.btnaddcompanypicture.Text = "Add / Change Company Logo"
        Me.btnaddcompanypicture.UseVisualStyleBackColor = True
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(15, 280)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(69, 13)
        Me.Label19.TabIndex = 38
        Me.Label19.Text = "Bank Name :"
        '
        'txtfaxno
        '
        Me.txtfaxno.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtfaxno.Location = New System.Drawing.Point(164, 427)
        Me.txtfaxno.Name = "txtfaxno"
        Me.txtfaxno.Size = New System.Drawing.Size(143, 20)
        Me.txtfaxno.TabIndex = 15
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtrecid)
        Me.GroupBox1.Controls.Add(Me.txtpicturepath)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 44)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(522, 413)
        Me.GroupBox1.TabIndex = 40
        Me.GroupBox1.TabStop = False
        '
        'txtrecid
        '
        Me.txtrecid.Location = New System.Drawing.Point(93, 356)
        Me.txtrecid.Name = "txtrecid"
        Me.txtrecid.Size = New System.Drawing.Size(37, 20)
        Me.txtrecid.TabIndex = 17
        Me.txtrecid.Visible = False
        '
        'txtpicturepath
        '
        Me.txtpicturepath.Location = New System.Drawing.Point(80, 383)
        Me.txtpicturepath.Name = "txtpicturepath"
        Me.txtpicturepath.Size = New System.Drawing.Size(52, 20)
        Me.txtpicturepath.TabIndex = 16
        Me.txtpicturepath.Visible = False
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'PanePanel4
        '
        Me.PanePanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel4.Controls.Add(Me.BTnclose)
        Me.PanePanel4.Controls.Add(Me.btnEdit)
        Me.PanePanel4.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PanePanel4.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PanePanel4.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel4.InactiveGradientHighColor = System.Drawing.Color.Green
        Me.PanePanel4.InactiveGradientLowColor = System.Drawing.Color.GreenYellow
        Me.PanePanel4.Location = New System.Drawing.Point(0, 466)
        Me.PanePanel4.Name = "PanePanel4"
        Me.PanePanel4.Size = New System.Drawing.Size(546, 36)
        Me.PanePanel4.TabIndex = 41
        '
        'frmAdmin_Company_Master
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(546, 502)
        Me.Controls.Add(Me.PanePanel4)
        Me.Controls.Add(Me.txtfaxno)
        Me.Controls.Add(Me.Label19)
        Me.Controls.Add(Me.btnaddcompanypicture)
        Me.Controls.Add(Me.txtzipcode)
        Me.Controls.Add(Me.txtTelno)
        Me.Controls.Add(Me.CompanyPicture)
        Me.Controls.Add(Me.txtbankacctno)
        Me.Controls.Add(Me.txtTIN)
        Me.Controls.Add(Me.txtsss)
        Me.Controls.Add(Me.txtphilhealth)
        Me.Controls.Add(Me.txtcompanyacctno)
        Me.Controls.Add(Me.txtcompanyadd2)
        Me.Controls.Add(Me.txtcompanyadd1)
        Me.Controls.Add(Me.txtbankname)
        Me.Controls.Add(Me.txtbankbrankcode)
        Me.Controls.Add(Me.txtcompanybankcode)
        Me.Controls.Add(Me.txtcompanybranch)
        Me.Controls.Add(Me.txtcompanycode)
        Me.Controls.Add(Me.txtcompanyid)
        Me.Controls.Add(Me.txtcompanyadd3)
        Me.Controls.Add(Me.txtcompanyname)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.PanePanel1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAdmin_Company_Master"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Company Info."
        Me.PanePanel1.ResumeLayout(False)
        Me.PanePanel1.PerformLayout()
        Me.PanePanel3.ResumeLayout(False)
        Me.PanePanel3.PerformLayout()
        CType(Me.CompanyPicture, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.PanePanel4.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PanePanel1 As WindowsApplication2.PanePanel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents txtcompanyname As System.Windows.Forms.TextBox
    Friend WithEvents txtcompanyadd3 As System.Windows.Forms.TextBox
    Friend WithEvents txtcompanyid As System.Windows.Forms.TextBox
    Friend WithEvents txtcompanycode As System.Windows.Forms.TextBox
    Friend WithEvents txtcompanybranch As System.Windows.Forms.TextBox
    Friend WithEvents txtcompanybankcode As System.Windows.Forms.TextBox
    Friend WithEvents txtbankbrankcode As System.Windows.Forms.TextBox
    Friend WithEvents txtbankname As System.Windows.Forms.TextBox
    Friend WithEvents txtcompanyadd1 As System.Windows.Forms.TextBox
    Friend WithEvents txtcompanyadd2 As System.Windows.Forms.TextBox
    Friend WithEvents txtcompanyacctno As System.Windows.Forms.TextBox
    Friend WithEvents txtphilhealth As System.Windows.Forms.TextBox
    Friend WithEvents txtsss As System.Windows.Forms.TextBox
    Friend WithEvents txtTIN As System.Windows.Forms.TextBox
    Friend WithEvents txtbankacctno As System.Windows.Forms.TextBox
    Friend WithEvents CompanyPicture As System.Windows.Forms.PictureBox
    Friend WithEvents txtTelno As System.Windows.Forms.TextBox
    Friend WithEvents txtzipcode As System.Windows.Forms.TextBox
    Friend WithEvents btnaddcompanypicture As System.Windows.Forms.Button
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents txtfaxno As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents BTnclose As System.Windows.Forms.Button
    Friend WithEvents btnEdit As System.Windows.Forms.Button
    Friend WithEvents txtpicturepath As System.Windows.Forms.TextBox
    Friend WithEvents txtrecid As System.Windows.Forms.TextBox
    Friend WithEvents PanePanel3 As WindowsApplication2.PanePanel
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents PanePanel4 As WindowsApplication2.PanePanel
End Class
