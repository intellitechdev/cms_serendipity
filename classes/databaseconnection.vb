Imports System.Data
Imports System.Data.SqlClient

Module databaseconnection
    Public Function SqlDBOpenConnection() As SqlClient.SqlConnection
        Dim myConnection As New SqlConnection(My.Settings.CNString)
        Try
            myConnection.Open()
            Return myConnection
        Catch ex As Exception
            Throw New System.Exception(ex.Message)
        End Try
    End Function

End Module
