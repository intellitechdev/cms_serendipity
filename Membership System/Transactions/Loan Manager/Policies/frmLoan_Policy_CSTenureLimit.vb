Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmLoan_Policy_CSTenureLimit
#Region "Variable"
    Private loanid As String
    Private shareid As String
#End Region
#Region "Property"
    Private Property getloanid() As String
        Get
            Return loanid
        End Get
        Set(ByVal value As String)
            loanid = value
        End Set
    End Property
    Private Property getshareid() As String
        Get
            Return shareid
        End Get
        Set(ByVal value As String)
            shareid = value
        End Set
    End Property
#End Region
#Region "Policy Code without param"
    Private Sub Policycode()
        Dim mycon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "CIMS_m_LoanType_Policies_Select")
        Try
            While rd.Read
                Me.cboloantype.Items.Add(rd.Item(0))
            End While
            rd.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Policycode")
        End Try

    End Sub
#End Region
#Region "Policy Code with param"
    Private Sub PolicyID(ByVal shareid As String)
        Dim mycon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "CIMS_m_LoanType_Policies_SelectParam", _
                                     New SqlParameter("@fnPolicyCode", shareid))
        Try
            While rd.Read
                getloanid() = rd.Item(0)
            End While
            rd.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "PolicyID")
        End Try
    End Sub
#End Region
#Region "Function"
    Public Function Decimals(ByVal C As Char) As Boolean
        If Not (Char.IsDigit(C)) And Not (C = ".") And Not (Microsoft.VisualBasic.AscW(C) = 8) And Not (Microsoft.VisualBasic.AscW(C) = 13) Then
            MsgBox("Invalid Input! This field allows 0-9 and '.' only.", MsgBoxStyle.Exclamation, "Invalid")
            Return False
        Else
            Return True
        End If
    End Function
#End Region
#Region "ClearText"
    Private Sub ClearText()

        Me.cboOperator.Text = ""
        Me.txtTenurefrom.Text = "0.0"
        Me.txtTenureTo.Text = "0.0"
        Me.txtCSfrom.Text = "0.00"
        Me.txtCSTo.Text = "0.00"
        Me.txtRate.Text = "0.00"
    End Sub
#End Region
#Region "Add/Edit Capital Share"
    Private Sub AddEditCapitalShare(ByVal pk_PolicyCapitalTenure As String, ByVal fk_LoanPolicies As String, ByVal tenureFrom As Decimal, _
                                   ByVal tenureTo As Decimal, ByVal csFrom As Decimal, ByVal csTo As Decimal, ByVal operators As String, _
                                   ByVal fdRate As Decimal)
        Dim mycon As New Clsappconfiguration
        mycon.sqlconn.Open()
        Dim trans As SqlTransaction = mycon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Masterfiles_TenureAndCSLimits_AddEdit", _
                                     New SqlParameter("@pk_PolicyCapitalTenure", pk_PolicyCapitalTenure), _
                                     New SqlParameter("@fk_LoanPolicies", fk_LoanPolicies), _
                                     New SqlParameter("@tenureFrom", tenureFrom), _
                                     New SqlParameter("@tenureTo", tenureTo), _
                                     New SqlParameter("@csFrom", csFrom), _
                                     New SqlParameter("@csTo", csTo), _
                                     New SqlParameter("@operator", operators), _
                                     New SqlParameter("@fdRate", fdRate))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "AddEditCapitalShare")
        Finally
            mycon.sqlconn.Close()
        End Try
    End Sub
#End Region
#Region "Delete"
    Private Sub Delete(ByVal capitalid As String)
        Dim mycon As New Clsappconfiguration
        mycon.sqlconn.Open()
        Dim trans As SqlTransaction = mycon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Masterfiles_TenureAndCSLimits_Delete", _
                                      New SqlParameter("@pk_PolicyCapitalTenure", capitalid))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "Delete")
        Finally
            mycon.sqlconn.Close()
        End Try
    End Sub
#End Region
#Region "View Capital Share Limit"
    Private Sub ViewCapitalShare()
        Dim mycon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "CIMS_Masterfiles_TenureAndCSLimits_Select")
        Try
            With Me.lvlCapital
                .Clear()
                .View = View.Details
                .FullRowSelect = True
                .GridLines = True
                .Columns.Add("CS Code", 70, HorizontalAlignment.Left)
                .Columns.Add("Policy Description", 200, HorizontalAlignment.Left)
                .Columns.Add("Tenure From", 100, HorizontalAlignment.Left)
                .Columns.Add("Tenure To", 100, HorizontalAlignment.Left)
                .Columns.Add("CS From", 100, HorizontalAlignment.Right)
                .Columns.Add("CS To", 100, HorizontalAlignment.Right)
                .Columns.Add("Operator", 70, HorizontalAlignment.Left)
                .Columns.Add("Rate", 100, HorizontalAlignment.Right)
                Try
                    While rd.Read
                        With .Items.Add(rd.Item(0))
                            .SubItems.Add(rd.Item(1))
                            .SubItems.Add(rd.Item(2))
                            .SubItems.Add(rd.Item(3))
                            .SubItems.Add(Format(rd.Item(4), "#,##0.00"))
                            .SubItems.Add(Format(rd.Item(5), "#,##0.00"))
                            .SubItems.Add(rd.Item(6))
                            .SubItems.Add(Format(rd.Item(7), "#,##0.00"))
                            .SubItems.Add(rd.Item(8))
                            .SubItems.Add(rd.Item(9))
                            .SubItems.Add(rd.Item(10))
                        End With
                    End While
                    rd.Close()
                    mycon.sqlconn.Close()
                Catch ex As Exception
                    MessageBox.Show(ex.Message, "ViewCapitalShare")
                End Try
            End With
        Catch ex As Exception

        End Try
    End Sub
#End Region
#Region "View share with parameter"
    Private Sub viewsharewithparameter(ByVal code As String)
        Dim mycon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "CIMS_Masterfiles_TenureAndCSLimits_Param", _
                                    New SqlParameter("@fnCS_Code", code))
        Try
            With Me.lvlCapital
                .Clear()
                .View = View.Details
                .FullRowSelect = True
                .GridLines = True
                .Columns.Add("CS Code", 70, HorizontalAlignment.Left)
                .Columns.Add("Policy Description", 200, HorizontalAlignment.Left)
                .Columns.Add("Tenure From", 100, HorizontalAlignment.Left)
                .Columns.Add("Tenure To", 100, HorizontalAlignment.Left)
                .Columns.Add("CS From", 100, HorizontalAlignment.Right)
                .Columns.Add("CS To", 100, HorizontalAlignment.Right)
                .Columns.Add("Operator", 70, HorizontalAlignment.Left)
                .Columns.Add("Rate", 100, HorizontalAlignment.Right)
                Try
                    While rd.Read
                        With .Items.Add(rd.Item(0))
                            .SubItems.Add(rd.Item(1))
                            .SubItems.Add(rd.Item(2))
                            .SubItems.Add(rd.Item(3))
                            .SubItems.Add(Format(rd.Item(4), "#,##0.00"))
                            .SubItems.Add(Format(rd.Item(5), "#,##0.00"))
                            .SubItems.Add(rd.Item(6))
                            .SubItems.Add(Format(rd.Item(7), "#,##0.00"))
                            .SubItems.Add(rd.Item(8))
                            .SubItems.Add(rd.Item(9))
                            .SubItems.Add(rd.Item(10))
                        End With
                    End While
                    rd.Close()
                    mycon.sqlconn.Close()
                Catch ex As Exception
                    MessageBox.Show(ex.Message, "ViewCapitalShare")
                End Try
            End With
        Catch ex As Exception

        End Try
    End Sub
#End Region
    Private Sub frmShareLimitMaster_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.cboloantype.Text = frmLoan_Policy_Master.txtcode.Text
        If Me.cboloantype.Text = "" Then
            Call Policycode()
            Call ViewCapitalShare()
        Else
            Me.cboloantype.Text = frmLoan_Policy_Master.txtcode.Text
            Me.txttemp.Text = frmLoan_Policy_Master.txtcode.Text
            Call viewsharewithparameter(loanid)
            Call Policycode()
        End If
    End Sub

    Private Sub btnsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsave.Click
        Try
            If btnsave.Text = "New" Then
                Call ClearText()
                'Call Policycode()
                btndelete.Visible = False
                btnupdate.Visible = False
                btnsave.Text = "Save"
                btnsave.Image = My.Resources.save2
                btnclose.Text = "Cancel"
                btnclose.Location = New System.Drawing.Point(74, 4)
            ElseIf btnsave.Text = "Save" Then
                If Me.cboloantype.Text <> "" And Me.cboOperator.Text <> "" Then
                    Call AddEditCapitalShare("", loanid, Me.txtTenurefrom.Text, Me.txtTenureTo.Text, Me.txtCSfrom.Text, Me.txtCSTo.Text, Me.cboOperator.Text, _
                                             Me.txtRate.Text)
                    Call viewsharewithparameter(loanid)
                    btnsave.Text = "New"
                    btnsave.Image = My.Resources.new3
                    btnclose.Text = "Close"
                    btnclose.Location = New System.Drawing.Point(218, 4)
                    btndelete.Visible = True
                    btnupdate.Visible = True
                Else
                    MessageBox.Show("Policy and Operator is needed.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                End If
            Else
                MessageBox.Show("Empty field found! ", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If

        Catch ex As Exception
        End Try
    End Sub

    Private Sub btnupdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupdate.Click
        Try
            If btnupdate.Text = "Edit" Then

                Me.txtTenurefrom.Text = Me.lvlCapital.SelectedItems(0).SubItems(2).Text
                Me.txtTenureTo.Text = Me.lvlCapital.SelectedItems(0).SubItems(3).Text
                Me.txtCSfrom.Text = Me.lvlCapital.SelectedItems(0).SubItems(4).Text
                Me.txtCSTo.Text = Me.lvlCapital.SelectedItems(0).SubItems(5).Text
                Me.cboOperator.Text = Me.lvlCapital.SelectedItems(0).SubItems(6).Text
                Me.txtRate.Text = Me.lvlCapital.SelectedItems(0).SubItems(7).Text
                Me.cboloantype.Text = Me.lvlCapital.SelectedItems(0).SubItems(8).Text
                getshareid() = Me.lvlCapital.SelectedItems(0).SubItems(9).Text
                getloanid() = Me.lvlCapital.SelectedItems(0).SubItems(10).Text

                Me.btnsave.Enabled = False
                btnupdate.Text = "Update"
                btnupdate.Image = My.Resources.save2
                btnclose.Text = "Cancel"
                btnclose.Location = New System.Drawing.Point(141, 4)
                btndelete.Visible = False

            ElseIf btnupdate.Text = "Update" Then
                Call AddEditCapitalShare(shareid, loanid, Me.txtTenurefrom.Text, Me.txtTenureTo.Text, Me.txtCSfrom.Text, Me.txtCSTo.Text, Me.cboOperator.Text, _
                                                         Me.txtRate.Text)
                Call viewsharewithparameter(loanid)
                Call ClearText()
                btndelete.Visible = True
                btnupdate.Text = "Edit"
                btnupdate.Image = My.Resources.edit1
                btnclose.Text = "Cancel"
                btnclose.Location = New System.Drawing.Point(218, 4)
                btnclose.Text = "Close"
                Me.btnsave.Enabled = True
                Me.btndelete.Visible = True

            Else
                MessageBox.Show("Empty field found! ", "Edit", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        If btnclose.Text = "Cancel" Then
            btnsave.Text = "New"
            btnsave.Image = My.Resources.new3
            btnupdate.Text = "Edit"
            btnupdate.Image = My.Resources.edit1
            btnclose.Text = "Close"
            btnclose.Location = New System.Drawing.Point(218, 4)
            btndelete.Visible = True
            btnupdate.Visible = True
            btnsave.Enabled = True
        ElseIf btnclose.Text = "Close" Then
            Me.Close()
        End If
    End Sub

    Private Sub txtTenurefrom_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtTenurefrom.KeyPress
        e.Handled = Not Decimals(e.KeyChar)
    End Sub

    Private Sub txtTenurefrom_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTenurefrom.TextChanged
        If Me.txtTenurefrom.Text = "" Then
            Me.txtTenurefrom.Text = "0.0"
        End If
    End Sub

    Private Sub txtTenureTo_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtTenureTo.KeyPress
        e.Handled = Not Decimals(e.KeyChar)
    End Sub

    Private Sub txtTenureTo_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTenureTo.TextChanged
        If Me.txtTenureTo.Text = "" Then
            Me.txtTenureTo.Text = "0.0"
        End If
    End Sub

    Private Sub txtCSfrom_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtCSfrom.KeyPress
        e.Handled = Not Decimals(e.KeyChar)
    End Sub

    Private Sub txtCSfrom_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCSfrom.TextChanged
        If Me.txtCSfrom.Text = "" Then
            Me.txtCSfrom.Text = "0.00"
        End If
    End Sub

    Private Sub txtCSTo_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtCSTo.KeyPress
        e.Handled = Not Decimals(e.KeyChar)
    End Sub

    Private Sub txtCSTo_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCSTo.TextChanged
        If Me.txtCSTo.Text = "" Then
            Me.txtCSTo.Text = "0.00"
        End If
    End Sub

    Private Sub cboloantype_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboloantype.KeyPress
        e.KeyChar = Microsoft.VisualBasic.ChrW(0)
    End Sub

    Private Sub cboOperator_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboOperator.KeyPress
        e.KeyChar = Microsoft.VisualBasic.ChrW(0)
    End Sub

    Private Sub btndelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndelete.Click
        Try
            getshareid() = Me.lvlCapital.SelectedItems(0).SubItems(9).Text
            Dim x As New DialogResult
            x = MessageBox.Show("Are you sure you want to permanently delete this record?", "Confirm Deletion", MessageBoxButtons.OKCancel, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
            If x = System.Windows.Forms.DialogResult.OK Then
                Call Delete(shareid)
                Call viewsharewithparameter(loanid)
                Call ClearText()
            ElseIf x = System.Windows.Forms.DialogResult.Cancel Then
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub lvlCapital_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvlCapital.Click
        Call LvlCapitalkeydownkeypress()
    End Sub
#Region "LvlCapital Keydown/keypress"
    Private Sub LvlCapitalkeydownkeypress()
        Try


            Me.txtTenurefrom.Text = Me.lvlCapital.SelectedItems(0).SubItems(2).Text
            Me.txtTenureTo.Text = Me.lvlCapital.SelectedItems(0).SubItems(3).Text
            Me.txtCSfrom.Text = Me.lvlCapital.SelectedItems(0).SubItems(4).Text
            Me.txtCSTo.Text = Me.lvlCapital.SelectedItems(0).SubItems(5).Text
            Me.cboOperator.Text = Me.lvlCapital.SelectedItems(0).SubItems(6).Text
            Me.txtRate.Text = Me.lvlCapital.SelectedItems(0).SubItems(7).Text
            Me.cboloantype.Text = Me.lvlCapital.SelectedItems(0).SubItems(8).Text
            getshareid() = Me.lvlCapital.SelectedItems(0).SubItems(9).Text
            getloanid() = Me.lvlCapital.SelectedItems(0).SubItems(10).Text

        Catch ex As Exception
            MessageBox.Show(ex.Message, "LvlCapitalkeydownkeypress")
        End Try
    End Sub
#End Region

    Private Sub lvlCapital_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles lvlCapital.KeyDown
        Call LvlCapitalkeydownkeypress()
    End Sub

    Private Sub lvlCapital_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles lvlCapital.KeyUp
        Call LvlCapitalkeydownkeypress()
    End Sub

    Private Sub cboloantype_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboloantype.SelectedIndexChanged

    End Sub

    Private Sub txtRate_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtRate.KeyPress
        e.Handled = Not Decimals(e.KeyChar)
    End Sub

    Private Sub cboloantype_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboloantype.TextChanged
        Call PolicyID(Me.cboloantype.Text)

    End Sub


    Private Sub txttemp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txttemp.TextChanged
        Call viewsharewithparameter(loanid)
    End Sub

    Private Sub lvlCapital_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvlCapital.SelectedIndexChanged

    End Sub
End Class