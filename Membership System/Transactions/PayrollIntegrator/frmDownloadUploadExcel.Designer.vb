<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDownloadUploadExcel
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDownloadUploadExcel))
        Me.tabPayrollInteg = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.chkloans = New System.Windows.Forms.CheckBox()
        Me.gridLoans = New System.Windows.Forms.DataGridView()
        Me.chkContribution = New System.Windows.Forms.CheckBox()
        Me.chkbereavement = New System.Windows.Forms.CheckBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtBrevAmt = New System.Windows.Forms.TextBox()
        Me.gridDownload = New System.Windows.Forms.DataGridView()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.btnviewdetails = New System.Windows.Forms.Button()
        Me.btnCreateFile = New System.Windows.Forms.Button()
        Me.btnprocess_Download = New System.Windows.Forms.Button()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.btnDeletePayroll = New System.Windows.Forms.Button()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.cboPaydate = New System.Windows.Forms.ComboBox()
        Me.lblContribution = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cboPaycode = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.btnReverse = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cbopaycodeupload = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtfile = New System.Windows.Forms.TextBox()
        Me.txtpaydate = New System.Windows.Forms.TextBox()
        Me.btnBrowse = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.btnUpload = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.gridupload = New System.Windows.Forms.DataGridView()
        Me.btnValidate = New System.Windows.Forms.Button()
        Me.btnProcess_Upload = New System.Windows.Forms.Button()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.lblLoading = New System.Windows.Forms.Label()
        Me.lstVwDownloadList = New System.Windows.Forms.ListView()
        Me.ColDesc = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColDate = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColType = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColDownloadedBy = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.bgwValidate = New System.ComponentModel.BackgroundWorker()
        Me.bgwDeductionDetails = New System.ComponentModel.BackgroundWorker()
        Me.bgwDeductionProcessed = New System.ComponentModel.BackgroundWorker()
        Me.bgwDatabaseFunctions = New System.ComponentModel.BackgroundWorker()
        Me.bgwUnsettled = New System.ComponentModel.BackgroundWorker()
        Me.bgDownload = New System.ComponentModel.BackgroundWorker()
        Me.btnclosed = New System.Windows.Forms.Button()
        Me.SavDiag = New System.Windows.Forms.SaveFileDialog()
        Me.tabPayrollInteg.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        CType(Me.gridLoans, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.gridDownload, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.gridupload, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'tabPayrollInteg
        '
        Me.tabPayrollInteg.Controls.Add(Me.TabPage1)
        Me.tabPayrollInteg.Controls.Add(Me.TabPage2)
        Me.tabPayrollInteg.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tabPayrollInteg.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabPayrollInteg.Location = New System.Drawing.Point(0, 0)
        Me.tabPayrollInteg.Name = "tabPayrollInteg"
        Me.tabPayrollInteg.SelectedIndex = 0
        Me.tabPayrollInteg.Size = New System.Drawing.Size(690, 577)
        Me.tabPayrollInteg.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.FlowLayoutPanel1)
        Me.TabPage1.Controls.Add(Me.Panel2)
        Me.TabPage1.Controls.Add(Me.Label8)
        Me.TabPage1.Controls.Add(Me.cboPaydate)
        Me.TabPage1.Controls.Add(Me.lblContribution)
        Me.TabPage1.Controls.Add(Me.Label2)
        Me.TabPage1.Controls.Add(Me.cboPaycode)
        Me.TabPage1.Controls.Add(Me.Label1)
        Me.TabPage1.Location = New System.Drawing.Point(4, 28)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(682, 545)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Download Deductions"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.chkloans)
        Me.FlowLayoutPanel1.Controls.Add(Me.gridLoans)
        Me.FlowLayoutPanel1.Controls.Add(Me.chkContribution)
        Me.FlowLayoutPanel1.Controls.Add(Me.chkbereavement)
        Me.FlowLayoutPanel1.Controls.Add(Me.Panel1)
        Me.FlowLayoutPanel1.Controls.Add(Me.gridDownload)
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(19, 96)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(590, 441)
        Me.FlowLayoutPanel1.TabIndex = 41
        Me.FlowLayoutPanel1.WrapContents = False
        '
        'chkloans
        '
        Me.chkloans.AutoSize = True
        Me.chkloans.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkloans.Location = New System.Drawing.Point(3, 3)
        Me.chkloans.Name = "chkloans"
        Me.chkloans.Size = New System.Drawing.Size(62, 22)
        Me.chkloans.TabIndex = 0
        Me.chkloans.Text = "Loans"
        Me.chkloans.UseVisualStyleBackColor = True
        '
        'gridLoans
        '
        Me.gridLoans.AllowUserToAddRows = False
        Me.gridLoans.AllowUserToDeleteRows = False
        Me.gridLoans.BackgroundColor = System.Drawing.Color.Gainsboro
        Me.gridLoans.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gridLoans.Location = New System.Drawing.Point(3, 31)
        Me.gridLoans.Name = "gridLoans"
        Me.gridLoans.RowHeadersVisible = False
        Me.gridLoans.Size = New System.Drawing.Size(587, 134)
        Me.gridLoans.TabIndex = 0
        Me.gridLoans.Visible = False
        '
        'chkContribution
        '
        Me.chkContribution.AutoSize = True
        Me.chkContribution.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkContribution.Location = New System.Drawing.Point(3, 171)
        Me.chkContribution.Name = "chkContribution"
        Me.chkContribution.Size = New System.Drawing.Size(101, 19)
        Me.chkContribution.TabIndex = 4
        Me.chkContribution.Text = "Contributions"
        Me.chkContribution.UseVisualStyleBackColor = True
        '
        'chkbereavement
        '
        Me.chkbereavement.AutoSize = True
        Me.chkbereavement.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkbereavement.Location = New System.Drawing.Point(3, 196)
        Me.chkbereavement.Name = "chkbereavement"
        Me.chkbereavement.Size = New System.Drawing.Size(15, 14)
        Me.chkbereavement.TabIndex = 3
        Me.chkbereavement.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.chkbereavement.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.txtBrevAmt)
        Me.Panel1.Location = New System.Drawing.Point(3, 216)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(200, 43)
        Me.Panel1.TabIndex = 38
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(19, 11)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(71, 15)
        Me.Label3.TabIndex = 37
        Me.Label3.Text = "* Enter Amt."
        '
        'txtBrevAmt
        '
        Me.txtBrevAmt.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBrevAmt.Location = New System.Drawing.Point(96, 8)
        Me.txtBrevAmt.Name = "txtBrevAmt"
        Me.txtBrevAmt.Size = New System.Drawing.Size(100, 23)
        Me.txtBrevAmt.TabIndex = 3
        Me.txtBrevAmt.Text = "0.00"
        Me.txtBrevAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'gridDownload
        '
        Me.gridDownload.AllowUserToAddRows = False
        Me.gridDownload.AllowUserToDeleteRows = False
        Me.gridDownload.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gridDownload.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gridDownload.Location = New System.Drawing.Point(3, 265)
        Me.gridDownload.Name = "gridDownload"
        Me.gridDownload.ReadOnly = True
        Me.gridDownload.Size = New System.Drawing.Size(587, 158)
        Me.gridDownload.TabIndex = 0
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.White
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Controls.Add(Me.btnviewdetails)
        Me.Panel2.Controls.Add(Me.btnCreateFile)
        Me.Panel2.Controls.Add(Me.btnprocess_Download)
        Me.Panel2.Controls.Add(Me.PictureBox5)
        Me.Panel2.Controls.Add(Me.btnDeletePayroll)
        Me.Panel2.Location = New System.Drawing.Point(615, 3)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(124, 539)
        Me.Panel2.TabIndex = 48
        '
        'btnviewdetails
        '
        Me.btnviewdetails.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnviewdetails.Image = CType(resources.GetObject("btnviewdetails.Image"), System.Drawing.Image)
        Me.btnviewdetails.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnviewdetails.Location = New System.Drawing.Point(13, 85)
        Me.btnviewdetails.Name = "btnviewdetails"
        Me.btnviewdetails.Size = New System.Drawing.Size(100, 39)
        Me.btnviewdetails.TabIndex = 38
        Me.btnviewdetails.Text = "Details"
        Me.btnviewdetails.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnviewdetails.UseVisualStyleBackColor = True
        '
        'btnCreateFile
        '
        Me.btnCreateFile.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCreateFile.Image = CType(resources.GetObject("btnCreateFile.Image"), System.Drawing.Image)
        Me.btnCreateFile.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCreateFile.Location = New System.Drawing.Point(13, 173)
        Me.btnCreateFile.Name = "btnCreateFile"
        Me.btnCreateFile.Size = New System.Drawing.Size(100, 40)
        Me.btnCreateFile.TabIndex = 2
        Me.btnCreateFile.Text = "Export"
        Me.btnCreateFile.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnCreateFile.UseVisualStyleBackColor = True
        '
        'btnprocess_Download
        '
        Me.btnprocess_Download.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnprocess_Download.Image = CType(resources.GetObject("btnprocess_Download.Image"), System.Drawing.Image)
        Me.btnprocess_Download.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnprocess_Download.Location = New System.Drawing.Point(13, 127)
        Me.btnprocess_Download.Name = "btnprocess_Download"
        Me.btnprocess_Download.Size = New System.Drawing.Size(100, 40)
        Me.btnprocess_Download.TabIndex = 36
        Me.btnprocess_Download.Text = "Process"
        Me.btnprocess_Download.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnprocess_Download.UseVisualStyleBackColor = True
        '
        'PictureBox5
        '
        Me.PictureBox5.Image = CType(resources.GetObject("PictureBox5.Image"), System.Drawing.Image)
        Me.PictureBox5.Location = New System.Drawing.Point(9, 405)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(100, 128)
        Me.PictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox5.TabIndex = 47
        Me.PictureBox5.TabStop = False
        '
        'btnDeletePayroll
        '
        Me.btnDeletePayroll.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDeletePayroll.Image = CType(resources.GetObject("btnDeletePayroll.Image"), System.Drawing.Image)
        Me.btnDeletePayroll.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDeletePayroll.Location = New System.Drawing.Point(13, 217)
        Me.btnDeletePayroll.Margin = New System.Windows.Forms.Padding(1)
        Me.btnDeletePayroll.Name = "btnDeletePayroll"
        Me.btnDeletePayroll.Size = New System.Drawing.Size(100, 39)
        Me.btnDeletePayroll.TabIndex = 10
        Me.btnDeletePayroll.Text = "Delete"
        Me.btnDeletePayroll.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnDeletePayroll.UseVisualStyleBackColor = True
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(15, 65)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(341, 19)
        Me.Label8.TabIndex = 42
        Me.Label8.Text = "What do you want to include in the deductions?"
        '
        'cboPaydate
        '
        Me.cboPaydate.FormattingEnabled = True
        Me.cboPaydate.Location = New System.Drawing.Point(436, 14)
        Me.cboPaydate.Name = "cboPaydate"
        Me.cboPaydate.Size = New System.Drawing.Size(132, 27)
        Me.cboPaydate.TabIndex = 40
        '
        'lblContribution
        '
        Me.lblContribution.AutoSize = True
        Me.lblContribution.Location = New System.Drawing.Point(151, 206)
        Me.lblContribution.Name = "lblContribution"
        Me.lblContribution.Size = New System.Drawing.Size(0, 19)
        Me.lblContribution.TabIndex = 39
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(345, 20)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(85, 15)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Payment Date:"
        '
        'cboPaycode
        '
        Me.cboPaycode.FormattingEnabled = True
        Me.cboPaycode.Location = New System.Drawing.Point(82, 14)
        Me.cboPaycode.Name = "cboPaycode"
        Me.cboPaycode.Size = New System.Drawing.Size(195, 27)
        Me.cboPaycode.TabIndex = 3
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(16, 20)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(60, 15)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Pay Code:"
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.btnReverse)
        Me.TabPage2.Controls.Add(Me.btnCancel)
        Me.TabPage2.Controls.Add(Me.GroupBox2)
        Me.TabPage2.Controls.Add(Me.Label7)
        Me.TabPage2.Controls.Add(Me.gridupload)
        Me.TabPage2.Controls.Add(Me.btnValidate)
        Me.TabPage2.Controls.Add(Me.btnProcess_Upload)
        Me.TabPage2.Location = New System.Drawing.Point(4, 28)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(739, 545)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Upload Payroll File"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'btnReverse
        '
        Me.btnReverse.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReverse.Image = CType(resources.GetObject("btnReverse.Image"), System.Drawing.Image)
        Me.btnReverse.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnReverse.Location = New System.Drawing.Point(165, 507)
        Me.btnReverse.Name = "btnReverse"
        Me.btnReverse.Size = New System.Drawing.Size(138, 30)
        Me.btnReverse.TabIndex = 67
        Me.btnReverse.Text = "Reverse"
        Me.btnReverse.UseVisualStyleBackColor = True
        Me.btnReverse.Visible = False
        '
        'btnCancel
        '
        Me.btnCancel.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Image = CType(resources.GetObject("btnCancel.Image"), System.Drawing.Image)
        Me.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCancel.Location = New System.Drawing.Point(596, 507)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(128, 30)
        Me.btnCancel.TabIndex = 66
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cbopaycodeupload)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.txtfile)
        Me.GroupBox2.Controls.Add(Me.txtpaydate)
        Me.GroupBox2.Controls.Add(Me.btnBrowse)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.btnUpload)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(14, 7)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(710, 106)
        Me.GroupBox2.TabIndex = 48
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Upload Details"
        '
        'cbopaycodeupload
        '
        Me.cbopaycodeupload.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbopaycodeupload.FormattingEnabled = True
        Me.cbopaycodeupload.Location = New System.Drawing.Point(93, 25)
        Me.cbopaycodeupload.Name = "cbopaycodeupload"
        Me.cbopaycodeupload.Size = New System.Drawing.Size(340, 23)
        Me.cbopaycodeupload.TabIndex = 43
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(7, 58)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(60, 15)
        Me.Label4.TabIndex = 36
        Me.Label4.Text = "Filename:"
        '
        'txtfile
        '
        Me.txtfile.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtfile.Location = New System.Drawing.Point(93, 58)
        Me.txtfile.Name = "txtfile"
        Me.txtfile.ReadOnly = True
        Me.txtfile.Size = New System.Drawing.Size(416, 23)
        Me.txtfile.TabIndex = 37
        '
        'txtpaydate
        '
        Me.txtpaydate.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtpaydate.Location = New System.Drawing.Point(515, 23)
        Me.txtpaydate.Name = "txtpaydate"
        Me.txtpaydate.Size = New System.Drawing.Size(189, 23)
        Me.txtpaydate.TabIndex = 46
        '
        'btnBrowse
        '
        Me.btnBrowse.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBrowse.Location = New System.Drawing.Point(515, 54)
        Me.btnBrowse.Name = "btnBrowse"
        Me.btnBrowse.Size = New System.Drawing.Size(39, 29)
        Me.btnBrowse.TabIndex = 38
        Me.btnBrowse.Text = "..."
        Me.btnBrowse.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(451, 28)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(58, 15)
        Me.Label5.TabIndex = 45
        Me.Label5.Text = "Pay Date:"
        '
        'btnUpload
        '
        Me.btnUpload.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUpload.Image = CType(resources.GetObject("btnUpload.Image"), System.Drawing.Image)
        Me.btnUpload.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUpload.Location = New System.Drawing.Point(560, 54)
        Me.btnUpload.Name = "btnUpload"
        Me.btnUpload.Size = New System.Drawing.Size(144, 29)
        Me.btnUpload.TabIndex = 39
        Me.btnUpload.Text = "Upload"
        Me.btnUpload.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(6, 28)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(80, 15)
        Me.Label6.TabIndex = 44
        Me.Label6.Text = "Payroll Code:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(10, 116)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(93, 19)
        Me.Label7.TabIndex = 47
        Me.Label7.Text = "Payroll Data"
        '
        'gridupload
        '
        Me.gridupload.AllowUserToAddRows = False
        Me.gridupload.AllowUserToDeleteRows = False
        Me.gridupload.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gridupload.Location = New System.Drawing.Point(13, 138)
        Me.gridupload.Name = "gridupload"
        Me.gridupload.ReadOnly = True
        Me.gridupload.Size = New System.Drawing.Size(711, 363)
        Me.gridupload.TabIndex = 0
        '
        'btnValidate
        '
        Me.btnValidate.Enabled = False
        Me.btnValidate.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnValidate.Image = CType(resources.GetObject("btnValidate.Image"), System.Drawing.Image)
        Me.btnValidate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnValidate.Location = New System.Drawing.Point(309, 507)
        Me.btnValidate.Name = "btnValidate"
        Me.btnValidate.Size = New System.Drawing.Size(138, 30)
        Me.btnValidate.TabIndex = 42
        Me.btnValidate.Text = "Validate"
        Me.btnValidate.UseVisualStyleBackColor = True
        '
        'btnProcess_Upload
        '
        Me.btnProcess_Upload.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnProcess_Upload.Image = CType(resources.GetObject("btnProcess_Upload.Image"), System.Drawing.Image)
        Me.btnProcess_Upload.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnProcess_Upload.Location = New System.Drawing.Point(453, 507)
        Me.btnProcess_Upload.Name = "btnProcess_Upload"
        Me.btnProcess_Upload.Size = New System.Drawing.Size(138, 30)
        Me.btnProcess_Upload.TabIndex = 6
        Me.btnProcess_Upload.Text = "Process"
        Me.btnProcess_Upload.UseVisualStyleBackColor = True
        '
        'Panel3
        '
        Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel3.Controls.Add(Me.lblLoading)
        Me.Panel3.Controls.Add(Me.lstVwDownloadList)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel3.Location = New System.Drawing.Point(690, 0)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(338, 577)
        Me.Panel3.TabIndex = 39
        '
        'lblLoading
        '
        Me.lblLoading.Image = CType(resources.GetObject("lblLoading.Image"), System.Drawing.Image)
        Me.lblLoading.Location = New System.Drawing.Point(128, 223)
        Me.lblLoading.Name = "lblLoading"
        Me.lblLoading.Size = New System.Drawing.Size(52, 49)
        Me.lblLoading.TabIndex = 49
        Me.lblLoading.Visible = False
        '
        'lstVwDownloadList
        '
        Me.lstVwDownloadList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColDesc, Me.ColDate, Me.ColType, Me.ColDownloadedBy})
        Me.lstVwDownloadList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lstVwDownloadList.Enabled = False
        Me.lstVwDownloadList.FullRowSelect = True
        Me.lstVwDownloadList.Location = New System.Drawing.Point(0, 0)
        Me.lstVwDownloadList.Name = "lstVwDownloadList"
        Me.lstVwDownloadList.Size = New System.Drawing.Size(334, 573)
        Me.lstVwDownloadList.TabIndex = 0
        Me.lstVwDownloadList.UseCompatibleStateImageBehavior = False
        Me.lstVwDownloadList.View = System.Windows.Forms.View.Details
        '
        'ColDesc
        '
        Me.ColDesc.Text = "Description"
        Me.ColDesc.Width = 80
        '
        'ColDate
        '
        Me.ColDate.Text = "Date"
        Me.ColDate.Width = 80
        '
        'ColType
        '
        Me.ColType.Text = "Type"
        Me.ColType.Width = 80
        '
        'ColDownloadedBy
        '
        Me.ColDownloadedBy.Text = "Downloaded By"
        Me.ColDownloadedBy.Width = 95
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'Timer1
        '
        Me.Timer1.Interval = 50
        '
        'bgwValidate
        '
        '
        'bgwDeductionDetails
        '
        '
        'bgwDeductionProcessed
        '
        '
        'bgwDatabaseFunctions
        '
        '
        'bgwUnsettled
        '
        '
        'bgDownload
        '
        '
        'btnclosed
        '
        Me.btnclosed.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnclosed.Image = CType(resources.GetObject("btnclosed.Image"), System.Drawing.Image)
        Me.btnclosed.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnclosed.Location = New System.Drawing.Point(569, 5)
        Me.btnclosed.Name = "btnclosed"
        Me.btnclosed.Size = New System.Drawing.Size(66, 28)
        Me.btnclosed.TabIndex = 9
        Me.btnclosed.Text = "Close"
        Me.btnclosed.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnclosed.UseVisualStyleBackColor = True
        '
        'SavDiag
        '
        Me.SavDiag.Filter = "Excel 97-2003 File|*.xls"
        Me.SavDiag.Title = "Save File"
        '
        'frmDownloadUploadExcel
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1028, 577)
        Me.Controls.Add(Me.tabPayrollInteg)
        Me.Controls.Add(Me.Panel3)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmDownloadUploadExcel"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Payroll Integrator"
        Me.tabPayrollInteg.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.PerformLayout()
        CType(Me.gridLoans, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.gridDownload, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.gridupload, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents tabPayrollInteg As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtBrevAmt As System.Windows.Forms.TextBox
    Friend WithEvents chkContribution As System.Windows.Forms.CheckBox
    Friend WithEvents chkbereavement As System.Windows.Forms.CheckBox
    Friend WithEvents cboPaycode As System.Windows.Forms.ComboBox
    Friend WithEvents PanePanel1 As WindowsApplication2.PanePanel
    Friend WithEvents btnclosed As System.Windows.Forms.Button
    Friend WithEvents btnProcess_Upload As System.Windows.Forms.Button
    Friend WithEvents gridDownload As System.Windows.Forms.DataGridView
    Friend WithEvents btnprocess_Download As System.Windows.Forms.Button
    Friend WithEvents btnCreateFile As System.Windows.Forms.Button
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents chkloans As System.Windows.Forms.CheckBox
    Friend WithEvents btnviewdetails As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents gridLoans As System.Windows.Forms.DataGridView
    Friend WithEvents btnUpload As System.Windows.Forms.Button
    Friend WithEvents btnBrowse As System.Windows.Forms.Button
    Friend WithEvents txtfile As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lblContribution As System.Windows.Forms.Label
    Friend WithEvents btnValidate As System.Windows.Forms.Button
    Friend WithEvents cboPaydate As System.Windows.Forms.ComboBox
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cbopaycodeupload As System.Windows.Forms.ComboBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtpaydate As System.Windows.Forms.TextBox
    Friend WithEvents btnDeletePayroll As System.Windows.Forms.Button
    Friend WithEvents gridupload As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents bgwValidate As System.ComponentModel.BackgroundWorker
    Friend WithEvents bgwDeductionDetails As System.ComponentModel.BackgroundWorker
    Friend WithEvents bgwDeductionProcessed As System.ComponentModel.BackgroundWorker
    Friend WithEvents bgwDatabaseFunctions As System.ComponentModel.BackgroundWorker
    Friend WithEvents bgwUnsettled As System.ComponentModel.BackgroundWorker
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents PictureBox5 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents btnReverse As System.Windows.Forms.Button
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents lblLoading As System.Windows.Forms.Label
    Friend WithEvents lstVwDownloadList As System.Windows.Forms.ListView
    Friend WithEvents bgDownload As System.ComponentModel.BackgroundWorker
    Friend WithEvents ColDesc As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents SavDiag As System.Windows.Forms.SaveFileDialog
    Friend WithEvents ColType As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColDownloadedBy As System.Windows.Forms.ColumnHeader
End Class
