<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFinStep
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmFinStep))
        Me.txtContribution = New System.Windows.Forms.TextBox()
        Me.txtInterest2 = New System.Windows.Forms.TextBox()
        Me.txtServiceFee = New System.Windows.Forms.TextBox()
        Me.txtAmortization = New System.Windows.Forms.TextBox()
        Me.txtAmortizationDate = New System.Windows.Forms.TextBox()
        Me.lblNetProceeds = New System.Windows.Forms.Label()
        Me.lblContribution = New System.Windows.Forms.Label()
        Me.lblInterest2 = New System.Windows.Forms.Label()
        Me.lblServiceFee = New System.Windows.Forms.Label()
        Me.lblAmortization = New System.Windows.Forms.Label()
        Me.lblAmortizationDate = New System.Windows.Forms.Label()
        Me.txtInterest = New System.Windows.Forms.TextBox()
        Me.txtterms = New System.Windows.Forms.TextBox()
        Me.txtloantype = New System.Windows.Forms.TextBox()
        Me.grpCapitalContribution = New System.Windows.Forms.GroupBox()
        Me.txtMaxLoanAmount2 = New System.Windows.Forms.TextBox()
        Me.txtPolicyRate = New System.Windows.Forms.TextBox()
        Me.txtCapitalContribution = New System.Windows.Forms.TextBox()
        Me.lblMaxLoanAccount2 = New System.Windows.Forms.Label()
        Me.lblPolicyRate = New System.Windows.Forms.Label()
        Me.lblCapitalContribution = New System.Windows.Forms.Label()
        Me.lblInterest = New System.Windows.Forms.Label()
        Me.lblTerms = New System.Windows.Forms.Label()
        Me.lblLoanType = New System.Windows.Forms.Label()
        Me.grpAmortization = New System.Windows.Forms.GroupBox()
        Me.txtNetProceeds = New System.Windows.Forms.TextBox()
        Me.grpTitle = New System.Windows.Forms.GroupBox()
        Me.lblMonthlytitle = New System.Windows.Forms.Label()
        Me.lblmaximumTitle = New System.Windows.Forms.Label()
        Me.btnclose = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.lblSalary = New System.Windows.Forms.Label()
        Me.lblTotalExistingDeductions = New System.Windows.Forms.Label()
        Me.lblMaxLoanAccount = New System.Windows.Forms.Label()
        Me.txtSalary = New System.Windows.Forms.TextBox()
        Me.txtRemainingNetPay = New System.Windows.Forms.TextBox()
        Me.txtMaxLoanAmount = New System.Windows.Forms.TextBox()
        Me.grpCapacityToPay = New System.Windows.Forms.GroupBox()
        Me.txtCapToPay = New System.Windows.Forms.TextBox()
        Me.Cp = New System.Windows.Forms.Label()
        Me.grpCapitalContribution.SuspendLayout()
        Me.grpAmortization.SuspendLayout()
        Me.grpTitle.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.grpCapacityToPay.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtContribution
        '
        Me.txtContribution.BackColor = System.Drawing.Color.White
        Me.txtContribution.Location = New System.Drawing.Point(142, 128)
        Me.txtContribution.Name = "txtContribution"
        Me.txtContribution.ReadOnly = True
        Me.txtContribution.Size = New System.Drawing.Size(121, 21)
        Me.txtContribution.TabIndex = 10
        Me.txtContribution.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtInterest2
        '
        Me.txtInterest2.BackColor = System.Drawing.Color.White
        Me.txtInterest2.Location = New System.Drawing.Point(142, 101)
        Me.txtInterest2.Name = "txtInterest2"
        Me.txtInterest2.ReadOnly = True
        Me.txtInterest2.Size = New System.Drawing.Size(121, 21)
        Me.txtInterest2.TabIndex = 9
        Me.txtInterest2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtServiceFee
        '
        Me.txtServiceFee.BackColor = System.Drawing.Color.White
        Me.txtServiceFee.Location = New System.Drawing.Point(142, 74)
        Me.txtServiceFee.Name = "txtServiceFee"
        Me.txtServiceFee.ReadOnly = True
        Me.txtServiceFee.Size = New System.Drawing.Size(121, 21)
        Me.txtServiceFee.TabIndex = 8
        Me.txtServiceFee.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtAmortization
        '
        Me.txtAmortization.BackColor = System.Drawing.Color.White
        Me.txtAmortization.Location = New System.Drawing.Point(142, 47)
        Me.txtAmortization.Name = "txtAmortization"
        Me.txtAmortization.ReadOnly = True
        Me.txtAmortization.Size = New System.Drawing.Size(121, 21)
        Me.txtAmortization.TabIndex = 7
        Me.txtAmortization.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtAmortizationDate
        '
        Me.txtAmortizationDate.BackColor = System.Drawing.Color.White
        Me.txtAmortizationDate.Location = New System.Drawing.Point(142, 20)
        Me.txtAmortizationDate.Name = "txtAmortizationDate"
        Me.txtAmortizationDate.ReadOnly = True
        Me.txtAmortizationDate.Size = New System.Drawing.Size(121, 21)
        Me.txtAmortizationDate.TabIndex = 6
        Me.txtAmortizationDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblNetProceeds
        '
        Me.lblNetProceeds.AutoSize = True
        Me.lblNetProceeds.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNetProceeds.Location = New System.Drawing.Point(53, 156)
        Me.lblNetProceeds.Name = "lblNetProceeds"
        Me.lblNetProceeds.Size = New System.Drawing.Size(83, 16)
        Me.lblNetProceeds.TabIndex = 5
        Me.lblNetProceeds.Text = "Net Proceeds"
        '
        'lblContribution
        '
        Me.lblContribution.AutoSize = True
        Me.lblContribution.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblContribution.Location = New System.Drawing.Point(26, 129)
        Me.lblContribution.Name = "lblContribution"
        Me.lblContribution.Size = New System.Drawing.Size(110, 16)
        Me.lblContribution.TabIndex = 4
        Me.lblContribution.Text = "Contribution (0%)"
        '
        'lblInterest2
        '
        Me.lblInterest2.AutoSize = True
        Me.lblInterest2.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInterest2.Location = New System.Drawing.Point(51, 102)
        Me.lblInterest2.Name = "lblInterest2"
        Me.lblInterest2.Size = New System.Drawing.Size(85, 16)
        Me.lblInterest2.TabIndex = 3
        Me.lblInterest2.Text = "Interest (9%)"
        '
        'lblServiceFee
        '
        Me.lblServiceFee.AutoSize = True
        Me.lblServiceFee.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblServiceFee.Location = New System.Drawing.Point(28, 75)
        Me.lblServiceFee.Name = "lblServiceFee"
        Me.lblServiceFee.Size = New System.Drawing.Size(108, 16)
        Me.lblServiceFee.TabIndex = 2
        Me.lblServiceFee.Text = "Service Fee (1%)"
        '
        'lblAmortization
        '
        Me.lblAmortization.AutoSize = True
        Me.lblAmortization.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAmortization.Location = New System.Drawing.Point(56, 48)
        Me.lblAmortization.Name = "lblAmortization"
        Me.lblAmortization.Size = New System.Drawing.Size(80, 16)
        Me.lblAmortization.TabIndex = 1
        Me.lblAmortization.Text = "Amortization"
        '
        'lblAmortizationDate
        '
        Me.lblAmortizationDate.AutoSize = True
        Me.lblAmortizationDate.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAmortizationDate.Location = New System.Drawing.Point(28, 21)
        Me.lblAmortizationDate.Name = "lblAmortizationDate"
        Me.lblAmortizationDate.Size = New System.Drawing.Size(110, 16)
        Me.lblAmortizationDate.TabIndex = 0
        Me.lblAmortizationDate.Text = "Amortization Date"
        '
        'txtInterest
        '
        Me.txtInterest.BackColor = System.Drawing.Color.White
        Me.txtInterest.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtInterest.Location = New System.Drawing.Point(136, 82)
        Me.txtInterest.Name = "txtInterest"
        Me.txtInterest.ReadOnly = True
        Me.txtInterest.Size = New System.Drawing.Size(121, 23)
        Me.txtInterest.TabIndex = 18
        Me.txtInterest.Text = "  "
        Me.txtInterest.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtterms
        '
        Me.txtterms.BackColor = System.Drawing.Color.White
        Me.txtterms.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtterms.Location = New System.Drawing.Point(136, 53)
        Me.txtterms.Name = "txtterms"
        Me.txtterms.ReadOnly = True
        Me.txtterms.Size = New System.Drawing.Size(121, 23)
        Me.txtterms.TabIndex = 17
        Me.txtterms.Text = "  "
        Me.txtterms.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtloantype
        '
        Me.txtloantype.BackColor = System.Drawing.Color.White
        Me.txtloantype.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtloantype.Location = New System.Drawing.Point(136, 24)
        Me.txtloantype.Name = "txtloantype"
        Me.txtloantype.ReadOnly = True
        Me.txtloantype.Size = New System.Drawing.Size(121, 23)
        Me.txtloantype.TabIndex = 16
        Me.txtloantype.Text = "  "
        Me.txtloantype.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'grpCapitalContribution
        '
        Me.grpCapitalContribution.Controls.Add(Me.txtMaxLoanAmount2)
        Me.grpCapitalContribution.Controls.Add(Me.txtPolicyRate)
        Me.grpCapitalContribution.Controls.Add(Me.txtCapitalContribution)
        Me.grpCapitalContribution.Controls.Add(Me.lblMaxLoanAccount2)
        Me.grpCapitalContribution.Controls.Add(Me.lblPolicyRate)
        Me.grpCapitalContribution.Controls.Add(Me.lblCapitalContribution)
        Me.grpCapitalContribution.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpCapitalContribution.ForeColor = System.Drawing.Color.Black
        Me.grpCapitalContribution.Location = New System.Drawing.Point(287, 205)
        Me.grpCapitalContribution.Name = "grpCapitalContribution"
        Me.grpCapitalContribution.Size = New System.Drawing.Size(281, 124)
        Me.grpCapitalContribution.TabIndex = 8
        Me.grpCapitalContribution.TabStop = False
        Me.grpCapitalContribution.Text = "Capital Contribution"
        '
        'txtMaxLoanAmount2
        '
        Me.txtMaxLoanAmount2.BackColor = System.Drawing.Color.White
        Me.txtMaxLoanAmount2.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMaxLoanAmount2.Location = New System.Drawing.Point(142, 78)
        Me.txtMaxLoanAmount2.Name = "txtMaxLoanAmount2"
        Me.txtMaxLoanAmount2.ReadOnly = True
        Me.txtMaxLoanAmount2.Size = New System.Drawing.Size(121, 23)
        Me.txtMaxLoanAmount2.TabIndex = 5
        Me.txtMaxLoanAmount2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtPolicyRate
        '
        Me.txtPolicyRate.BackColor = System.Drawing.Color.White
        Me.txtPolicyRate.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPolicyRate.Location = New System.Drawing.Point(142, 52)
        Me.txtPolicyRate.Name = "txtPolicyRate"
        Me.txtPolicyRate.ReadOnly = True
        Me.txtPolicyRate.Size = New System.Drawing.Size(121, 23)
        Me.txtPolicyRate.TabIndex = 4
        Me.txtPolicyRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtCapitalContribution
        '
        Me.txtCapitalContribution.BackColor = System.Drawing.Color.White
        Me.txtCapitalContribution.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCapitalContribution.Location = New System.Drawing.Point(142, 26)
        Me.txtCapitalContribution.Name = "txtCapitalContribution"
        Me.txtCapitalContribution.ReadOnly = True
        Me.txtCapitalContribution.Size = New System.Drawing.Size(121, 23)
        Me.txtCapitalContribution.TabIndex = 3
        Me.txtCapitalContribution.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblMaxLoanAccount2
        '
        Me.lblMaxLoanAccount2.AutoSize = True
        Me.lblMaxLoanAccount2.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMaxLoanAccount2.Location = New System.Drawing.Point(26, 81)
        Me.lblMaxLoanAccount2.Name = "lblMaxLoanAccount2"
        Me.lblMaxLoanAccount2.Size = New System.Drawing.Size(110, 16)
        Me.lblMaxLoanAccount2.TabIndex = 2
        Me.lblMaxLoanAccount2.Text = "Max Loan Amount"
        '
        'lblPolicyRate
        '
        Me.lblPolicyRate.AutoSize = True
        Me.lblPolicyRate.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPolicyRate.Location = New System.Drawing.Point(66, 55)
        Me.lblPolicyRate.Name = "lblPolicyRate"
        Me.lblPolicyRate.Size = New System.Drawing.Size(70, 16)
        Me.lblPolicyRate.TabIndex = 1
        Me.lblPolicyRate.Text = "Policy Rate"
        '
        'lblCapitalContribution
        '
        Me.lblCapitalContribution.AutoSize = True
        Me.lblCapitalContribution.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCapitalContribution.Location = New System.Drawing.Point(16, 29)
        Me.lblCapitalContribution.Name = "lblCapitalContribution"
        Me.lblCapitalContribution.Size = New System.Drawing.Size(120, 16)
        Me.lblCapitalContribution.TabIndex = 0
        Me.lblCapitalContribution.Text = "Capital Contribution"
        '
        'lblInterest
        '
        Me.lblInterest.AutoSize = True
        Me.lblInterest.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInterest.Location = New System.Drawing.Point(78, 85)
        Me.lblInterest.Name = "lblInterest"
        Me.lblInterest.Size = New System.Drawing.Size(52, 16)
        Me.lblInterest.TabIndex = 2
        Me.lblInterest.Text = "Interest"
        '
        'lblTerms
        '
        Me.lblTerms.AutoSize = True
        Me.lblTerms.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTerms.Location = New System.Drawing.Point(30, 56)
        Me.lblTerms.Name = "lblTerms"
        Me.lblTerms.Size = New System.Drawing.Size(100, 16)
        Me.lblTerms.TabIndex = 1
        Me.lblTerms.Text = "Terms (Months)"
        '
        'lblLoanType
        '
        Me.lblLoanType.AllowDrop = True
        Me.lblLoanType.AutoSize = True
        Me.lblLoanType.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanType.Location = New System.Drawing.Point(63, 27)
        Me.lblLoanType.Name = "lblLoanType"
        Me.lblLoanType.Size = New System.Drawing.Size(67, 16)
        Me.lblLoanType.TabIndex = 0
        Me.lblLoanType.Text = "Loan Type"
        '
        'grpAmortization
        '
        Me.grpAmortization.Controls.Add(Me.txtNetProceeds)
        Me.grpAmortization.Controls.Add(Me.btnclose)
        Me.grpAmortization.Controls.Add(Me.txtContribution)
        Me.grpAmortization.Controls.Add(Me.txtInterest2)
        Me.grpAmortization.Controls.Add(Me.txtServiceFee)
        Me.grpAmortization.Controls.Add(Me.txtAmortization)
        Me.grpAmortization.Controls.Add(Me.txtAmortizationDate)
        Me.grpAmortization.Controls.Add(Me.lblNetProceeds)
        Me.grpAmortization.Controls.Add(Me.lblContribution)
        Me.grpAmortization.Controls.Add(Me.lblInterest2)
        Me.grpAmortization.Controls.Add(Me.lblServiceFee)
        Me.grpAmortization.Controls.Add(Me.lblAmortization)
        Me.grpAmortization.Controls.Add(Me.lblAmortizationDate)
        Me.grpAmortization.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpAmortization.ForeColor = System.Drawing.Color.Black
        Me.grpAmortization.Location = New System.Drawing.Point(287, 335)
        Me.grpAmortization.Name = "grpAmortization"
        Me.grpAmortization.Size = New System.Drawing.Size(281, 194)
        Me.grpAmortization.TabIndex = 9
        Me.grpAmortization.TabStop = False
        Me.grpAmortization.Visible = False
        '
        'txtNetProceeds
        '
        Me.txtNetProceeds.BackColor = System.Drawing.Color.White
        Me.txtNetProceeds.Location = New System.Drawing.Point(142, 155)
        Me.txtNetProceeds.Name = "txtNetProceeds"
        Me.txtNetProceeds.ReadOnly = True
        Me.txtNetProceeds.Size = New System.Drawing.Size(121, 21)
        Me.txtNetProceeds.TabIndex = 11
        Me.txtNetProceeds.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'grpTitle
        '
        Me.grpTitle.Controls.Add(Me.lblMonthlytitle)
        Me.grpTitle.Controls.Add(Me.lblmaximumTitle)
        Me.grpTitle.Location = New System.Drawing.Point(4, 4)
        Me.grpTitle.Name = "grpTitle"
        Me.grpTitle.Size = New System.Drawing.Size(564, 74)
        Me.grpTitle.TabIndex = 10
        Me.grpTitle.TabStop = False
        '
        'lblMonthlytitle
        '
        Me.lblMonthlytitle.AutoSize = True
        Me.lblMonthlytitle.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMonthlytitle.Location = New System.Drawing.Point(214, 41)
        Me.lblMonthlytitle.Name = "lblMonthlytitle"
        Me.lblMonthlytitle.Size = New System.Drawing.Size(117, 19)
        Me.lblMonthlytitle.TabIndex = 1
        Me.lblMonthlytitle.Text = "(Monthly Basis)"
        '
        'lblmaximumTitle
        '
        Me.lblmaximumTitle.AutoSize = True
        Me.lblmaximumTitle.Font = New System.Drawing.Font("Calibri", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblmaximumTitle.Location = New System.Drawing.Point(127, 15)
        Me.lblmaximumTitle.Name = "lblmaximumTitle"
        Me.lblmaximumTitle.Size = New System.Drawing.Size(300, 26)
        Me.lblmaximumTitle.TabIndex = 0
        Me.lblmaximumTitle.Text = "Maximum Loanable Computation"
        '
        'btnclose
        '
        Me.btnclose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnclose.Location = New System.Drawing.Point(198, 0)
        Me.btnclose.Name = "btnclose"
        Me.btnclose.Size = New System.Drawing.Size(75, 23)
        Me.btnclose.TabIndex = 11
        Me.btnclose.Text = "Close"
        Me.btnclose.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lblLoanType)
        Me.GroupBox1.Controls.Add(Me.txtloantype)
        Me.GroupBox1.Controls.Add(Me.lblTerms)
        Me.GroupBox1.Controls.Add(Me.txtterms)
        Me.GroupBox1.Controls.Add(Me.lblInterest)
        Me.GroupBox1.Controls.Add(Me.txtInterest)
        Me.GroupBox1.Location = New System.Drawing.Point(293, 84)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(270, 115)
        Me.GroupBox1.TabIndex = 12
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Loan Information"
        '
        'lblSalary
        '
        Me.lblSalary.AutoSize = True
        Me.lblSalary.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSalary.Location = New System.Drawing.Point(102, 19)
        Me.lblSalary.Name = "lblSalary"
        Me.lblSalary.Size = New System.Drawing.Size(44, 16)
        Me.lblSalary.TabIndex = 3
        Me.lblSalary.Text = "Salary"
        '
        'lblTotalExistingDeductions
        '
        Me.lblTotalExistingDeductions.AutoSize = True
        Me.lblTotalExistingDeductions.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalExistingDeductions.Location = New System.Drawing.Point(31, 53)
        Me.lblTotalExistingDeductions.Name = "lblTotalExistingDeductions"
        Me.lblTotalExistingDeductions.Size = New System.Drawing.Size(115, 16)
        Me.lblTotalExistingDeductions.TabIndex = 14
        Me.lblTotalExistingDeductions.Text = "Remaining Net Pay"
        '
        'lblMaxLoanAccount
        '
        Me.lblMaxLoanAccount.AutoSize = True
        Me.lblMaxLoanAccount.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMaxLoanAccount.Location = New System.Drawing.Point(32, 202)
        Me.lblMaxLoanAccount.Name = "lblMaxLoanAccount"
        Me.lblMaxLoanAccount.Size = New System.Drawing.Size(114, 16)
        Me.lblMaxLoanAccount.TabIndex = 15
        Me.lblMaxLoanAccount.Text = "Max. Loan Amount"
        Me.lblMaxLoanAccount.Visible = False
        '
        'txtSalary
        '
        Me.txtSalary.BackColor = System.Drawing.Color.White
        Me.txtSalary.Location = New System.Drawing.Point(152, 16)
        Me.txtSalary.Name = "txtSalary"
        Me.txtSalary.ReadOnly = True
        Me.txtSalary.Size = New System.Drawing.Size(118, 23)
        Me.txtSalary.TabIndex = 19
        Me.txtSalary.Text = "  "
        Me.txtSalary.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtRemainingNetPay
        '
        Me.txtRemainingNetPay.BackColor = System.Drawing.Color.White
        Me.txtRemainingNetPay.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemainingNetPay.Location = New System.Drawing.Point(151, 50)
        Me.txtRemainingNetPay.Name = "txtRemainingNetPay"
        Me.txtRemainingNetPay.ReadOnly = True
        Me.txtRemainingNetPay.Size = New System.Drawing.Size(117, 23)
        Me.txtRemainingNetPay.TabIndex = 30
        Me.txtRemainingNetPay.Text = "  "
        Me.txtRemainingNetPay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtMaxLoanAmount
        '
        Me.txtMaxLoanAmount.BackColor = System.Drawing.Color.White
        Me.txtMaxLoanAmount.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMaxLoanAmount.Location = New System.Drawing.Point(151, 199)
        Me.txtMaxLoanAmount.Name = "txtMaxLoanAmount"
        Me.txtMaxLoanAmount.ReadOnly = True
        Me.txtMaxLoanAmount.Size = New System.Drawing.Size(118, 23)
        Me.txtMaxLoanAmount.TabIndex = 31
        Me.txtMaxLoanAmount.Text = "  "
        Me.txtMaxLoanAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtMaxLoanAmount.Visible = False
        '
        'grpCapacityToPay
        '
        Me.grpCapacityToPay.Controls.Add(Me.Cp)
        Me.grpCapacityToPay.Controls.Add(Me.txtCapToPay)
        Me.grpCapacityToPay.Controls.Add(Me.txtMaxLoanAmount)
        Me.grpCapacityToPay.Controls.Add(Me.txtRemainingNetPay)
        Me.grpCapacityToPay.Controls.Add(Me.txtSalary)
        Me.grpCapacityToPay.Controls.Add(Me.lblMaxLoanAccount)
        Me.grpCapacityToPay.Controls.Add(Me.lblTotalExistingDeductions)
        Me.grpCapacityToPay.Controls.Add(Me.lblSalary)
        Me.grpCapacityToPay.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpCapacityToPay.ForeColor = System.Drawing.Color.Black
        Me.grpCapacityToPay.Location = New System.Drawing.Point(4, 84)
        Me.grpCapacityToPay.Name = "grpCapacityToPay"
        Me.grpCapacityToPay.Size = New System.Drawing.Size(281, 245)
        Me.grpCapacityToPay.TabIndex = 7
        Me.grpCapacityToPay.TabStop = False
        Me.grpCapacityToPay.Text = "Capacity to Pay"
        '
        'txtCapToPay
        '
        Me.txtCapToPay.BackColor = System.Drawing.Color.White
        Me.txtCapToPay.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCapToPay.Location = New System.Drawing.Point(152, 82)
        Me.txtCapToPay.Name = "txtCapToPay"
        Me.txtCapToPay.ReadOnly = True
        Me.txtCapToPay.Size = New System.Drawing.Size(118, 23)
        Me.txtCapToPay.TabIndex = 32
        Me.txtCapToPay.Text = "  "
        Me.txtCapToPay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Cp
        '
        Me.Cp.AutoSize = True
        Me.Cp.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Cp.Location = New System.Drawing.Point(43, 89)
        Me.Cp.Name = "Cp"
        Me.Cp.Size = New System.Drawing.Size(103, 16)
        Me.Cp.TabIndex = 33
        Me.Cp.Text = "Capacity To Pay "
        '
        'frmFinStep
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BackColor = System.Drawing.Color.White
        Me.CancelButton = Me.btnclose
        Me.ClientSize = New System.Drawing.Size(572, 363)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.grpTitle)
        Me.Controls.Add(Me.grpCapitalContribution)
        Me.Controls.Add(Me.grpCapacityToPay)
        Me.Controls.Add(Me.grpAmortization)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = New System.Drawing.Point(300, 105)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmFinStep"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Maximum Loanable Amount Computation"
        Me.grpCapitalContribution.ResumeLayout(False)
        Me.grpCapitalContribution.PerformLayout()
        Me.grpAmortization.ResumeLayout(False)
        Me.grpAmortization.PerformLayout()
        Me.grpTitle.ResumeLayout(False)
        Me.grpTitle.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.grpCapacityToPay.ResumeLayout(False)
        Me.grpCapacityToPay.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txtContribution As System.Windows.Forms.TextBox
    Friend WithEvents txtInterest2 As System.Windows.Forms.TextBox
    Friend WithEvents txtServiceFee As System.Windows.Forms.TextBox
    Friend WithEvents txtAmortization As System.Windows.Forms.TextBox
    Friend WithEvents txtAmortizationDate As System.Windows.Forms.TextBox
    Friend WithEvents lblNetProceeds As System.Windows.Forms.Label
    Friend WithEvents lblContribution As System.Windows.Forms.Label
    Friend WithEvents lblInterest2 As System.Windows.Forms.Label
    Friend WithEvents lblServiceFee As System.Windows.Forms.Label
    Friend WithEvents lblAmortization As System.Windows.Forms.Label
    Friend WithEvents lblAmortizationDate As System.Windows.Forms.Label
    Friend WithEvents txtInterest As System.Windows.Forms.TextBox
    Friend WithEvents txtterms As System.Windows.Forms.TextBox
    Friend WithEvents txtloantype As System.Windows.Forms.TextBox
    Friend WithEvents grpCapitalContribution As System.Windows.Forms.GroupBox
    Friend WithEvents txtMaxLoanAmount2 As System.Windows.Forms.TextBox
    Friend WithEvents txtPolicyRate As System.Windows.Forms.TextBox
    Friend WithEvents txtCapitalContribution As System.Windows.Forms.TextBox
    Friend WithEvents lblMaxLoanAccount2 As System.Windows.Forms.Label
    Friend WithEvents lblPolicyRate As System.Windows.Forms.Label
    Friend WithEvents lblCapitalContribution As System.Windows.Forms.Label
    Friend WithEvents lblInterest As System.Windows.Forms.Label
    Friend WithEvents lblTerms As System.Windows.Forms.Label
    Friend WithEvents lblLoanType As System.Windows.Forms.Label
    Friend WithEvents grpAmortization As System.Windows.Forms.GroupBox
    Friend WithEvents txtNetProceeds As System.Windows.Forms.TextBox
    Friend WithEvents grpTitle As System.Windows.Forms.GroupBox
    Friend WithEvents lblMonthlytitle As System.Windows.Forms.Label
    Friend WithEvents lblmaximumTitle As System.Windows.Forms.Label
    Friend WithEvents btnclose As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lblSalary As System.Windows.Forms.Label
    Friend WithEvents lblTotalExistingDeductions As System.Windows.Forms.Label
    Friend WithEvents lblMaxLoanAccount As System.Windows.Forms.Label
    Friend WithEvents txtSalary As System.Windows.Forms.TextBox
    Friend WithEvents txtRemainingNetPay As System.Windows.Forms.TextBox
    Friend WithEvents txtMaxLoanAmount As System.Windows.Forms.TextBox
    Friend WithEvents grpCapacityToPay As System.Windows.Forms.GroupBox
    Friend WithEvents Cp As System.Windows.Forms.Label
    Friend WithEvents txtCapToPay As System.Windows.Forms.TextBox
End Class
