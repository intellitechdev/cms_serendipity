Imports System.Data.SqlClient
Public Class frmEmployee_Medical
#Region "variables"
    Public myconnection As New Clsappconfiguration
    Public getmedical_ID As String
#End Region
#Region "Get Property"
    Public Property getmedicalID() As String
        Get
            Return getmedical_ID
        End Get
        Set(ByVal value As String)
            getmedical_ID = value
        End Set
    End Property
#End Region
    Private Sub btnsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsave.Click
   
        Me.Close()
    End Sub

    '#Region "Employee Medical Get/stored proc"
    '    Public Sub Employee_medical_Get()
    '        Dim cmd As New SqlCommand("usp_per_employee_medical_get", myconnection.sqlconn)
    '        cmd.CommandType = CommandType.StoredProcedure
    '        myconnection.sqlconn.Open()
    '        cmd.Parameters.Add("@IDnumber", SqlDbType.VarChar, 50).Value = form6.txtreplicateIDnumber.Text 'tempid_no.Text
    '        With form6.LvlMedical
    '            .Clear()
    '            .View = View.Details
    '            .GridLines = True
    '            .FullRowSelect = True
    '            .Columns.Add("Cases", 150, HorizontalAlignment.Left)
    '            .Columns.Add("Status", 150, HorizontalAlignment.Left)
    '            .Columns.Add("Remarks", 150, HorizontalAlignment.Left)
    '            Dim myreader As SqlDataReader
    '            myreader = cmd.ExecuteReader
    '            Try
    '                While myreader.Read
    '                    With .Items.Add(myreader.Item(0))
    '                        .subitems.add(myreader.Item(1))
    '                        .subitems.add(myreader.Item(2))
    '                        .subitems.add(myreader.Item(3)) 'idnumber
    '                        .subitems.add(myreader.Item(4)) 'key_id 
    '                    End With
    '                End While
    '            Finally
    '                myreader.Close()
    '                myconnection.sqlconn.Close()
    '            End Try
    '        End With
    '    End Sub
    '#End Region

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        Me.Close()
    End Sub

    Private Sub btnupdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupdate.Click
      
    End Sub
End Class