﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmInvestment_Doc

    Private con As New Clsappconfiguration
    Dim ds As DataSet

    Private Sub frmInvestment_Doc_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If isLoaded("") Then
            txtSearch.Text = ""
            ActiveControl = txtSearch
        End If
    End Sub

    Private Function isLoaded(ByVal xkey As String) As Boolean
        Try
            ds = SqlHelper.ExecuteDataset(con.cnstring, "_Investment_Select_OR",
                                          New SqlParameter("@search", xkey))
            dgvList.DataSource = ds.Tables(0)
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Sub txtSearch_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSearch.KeyDown
        If e.KeyCode = Keys.Enter Then
            If isSelected() Then
                Me.Close()
            End If
        End If
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        If isLoaded(txtSearch.Text) Then

        End If
    End Sub

    Private Sub btnSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelect.Click
        If isSelected() Then
            Me.Close()
        End If
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Function isSelected() As Boolean
        Try
            frmInvestment_Entry.cbDocNum.Text = dgvList.SelectedRows(0).Cells(0).Value.ToString
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Sub dgvList_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvList.DoubleClick
        If isSelected() Then
            Me.Close()
        End If
    End Sub
End Class