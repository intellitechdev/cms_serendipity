Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmLoan_Policy_Master
#Region "Variables"
    Private loanid As String
    Public tenurelimit As Boolean
    Private cooptenure As Boolean
    Private comptenure As Boolean
    Public maxterm As Boolean
    Public CSLimit As Boolean
#End Region
#Region "Property"

    Public Property gettenurelimit() As Boolean
        Get
            Return tenurelimit
        End Get
        Set(ByVal value As Boolean)
            tenurelimit = value
        End Set
    End Property
    Public Property getCsLimit() As Boolean
        Get
            Return CSLimit
        End Get
        Set(ByVal value As Boolean)
            CSLimit = value
        End Set
    End Property
    Public Property getcooptenure() As Boolean
        Get
            Return cooptenure
        End Get
        Set(ByVal value As Boolean)
            cooptenure = value
        End Set
    End Property
    Private Property getcomptenure() As Boolean
        Get
            Return comptenure
        End Get
        Set(ByVal value As Boolean)
            comptenure = value
        End Set
    End Property
    Private Property getmaxterm() As Boolean
        Get
            Return maxterm
        End Get
        Set(ByVal value As Boolean)
            maxterm = value
        End Set
    End Property
    Private Property getloadid() As String
        Get
            Return loanid
        End Get
        Set(ByVal value As String)
            loanid = value
        End Set
    End Property
#End Region
#Region "Load Loan type/with param"
    Private Sub loadType()
        Dim mycon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "CIMS_t_Loans_LoanType_Select_01")
        Try
            While rd.Read
                'Me.cboloantype.Items.Add(rd.Item(0))
            End While
            rd.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "loadType")
        End Try

    End Sub
    Private Sub SelectedLoanType(ByVal loantype As String)
        Dim mycon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "CIMS_t_Loans_LoanType_SelectParam", _
                                     New SqlParameter("@fcLoanTypeName", loantype))
        Try
            While rd.Read
                Me.txtcode.Text = rd.Item(1)
            End While
            rd.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "SelectedLoanType")
        End Try
    End Sub
#End Region
#Region "Add/Edit Loan Policies"
    Private Sub AddEditLoanPolicies(ByVal loancode As Integer, ByVal loandesc As String, ByVal Include_CSLimit As Boolean, _
                                    ByVal Include_Tenure As Boolean, ByVal Tenure_OnCooperative As Boolean, ByVal Tenure_OnCompany As Boolean, _
                                    ByVal Include_MaxTerms As Boolean, ByVal ApplyPositionLimit As String)
        Dim mycon As New Clsappconfiguration
        mycon.sqlconn.Open()
        Dim trans As SqlTransaction = mycon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Masterfiles_LoanPolicy_AddEdit", _
                                      New SqlParameter("@loanPolicyCode", loancode), _
                                      New SqlParameter("@description", loandesc), _
                                      New SqlParameter("@Include_CSLimit", Include_CSLimit), _
                                      New SqlParameter("@Include_Tenure", Include_Tenure), _
                                      New SqlParameter("@Tenure_OnCooperative", Tenure_OnCooperative), _
                                      New SqlParameter("@Tenure_OnCompany", Tenure_OnCompany), _
                                      New SqlParameter("@Inlude_MaxTerms", Include_MaxTerms), _
                                      New SqlParameter("@ApplyPositionTypeLimit", ApplyPositionLimit), _
                                      New SqlParameter("@ProjName", cboProjectName.Text))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "AddEditLoanPolicies")
        Finally
            mycon.sqlconn.Close()
        End Try
    End Sub
#End Region
#Region "View Loan Policies with param"
    Private Sub ViewLoanPolicies(ByVal loandesc As String)
        Dim mycon As New Clsappconfiguration
        Dim ad As New SqlDataAdapter
        Dim ds As New DataSet
        Dim cmd As New SqlCommand("CIMS_Masterfiles_Policy_Load", mycon.sqlconn)
        mycon.sqlconn.Open()
        cmd.CommandType = CommandType.StoredProcedure
        With cmd.Parameters
            .Add("@policyCode", SqlDbType.VarChar, 50).Value = Trim(loandesc)
        End With

        ad.SelectCommand = cmd
        ad.Fill(ds, "CIMS_m_LoanType_Policies")

        Me.gridPolicies.DataSource = ds
        Me.gridPolicies.DataMember = "CIMS_m_LoanType_Policies"
        Me.gridPolicies.Columns("PolicyID").Visible = False
        mycon.sqlconn.Close()
    End Sub
#End Region
#Region "View Loan Policies"
    Private Sub LoanPolicyList_byProj()
        Dim mycon As New Clsappconfiguration
        Dim ad As New SqlDataAdapter
        Dim ds As New DataSet
        Dim cmd As New SqlCommand("CIMS_Masterfiles_Policy_Load_All_byProj", mycon.sqlconn)
        cmd.Parameters.Add("@ProjDesc", SqlDbType.VarChar, 300).Value = cboProjectName.Text
        mycon.sqlconn.Open()
        cmd.CommandType = CommandType.StoredProcedure

        ad.SelectCommand = cmd
        ad.Fill(ds, "CIMS_m_LoanType_Policies")

        Me.gridPolicies.DataSource = ds
        Me.gridPolicies.DataMember = "CIMS_m_LoanType_Policies"
        Me.gridPolicies.Columns("PolicyID").Visible = False
        mycon.sqlconn.Close()
    End Sub

    Private Sub LoanPolicyList()
        Dim mycon As New Clsappconfiguration
        Dim ad As New SqlDataAdapter
        Dim ds As New DataSet
        Dim cmd As New SqlCommand("CIMS_Masterfiles_Policy_Load_All", mycon.sqlconn)
        mycon.sqlconn.Open()
        cmd.CommandType = CommandType.StoredProcedure

        ad.SelectCommand = cmd
        ad.Fill(ds, "CIMS_m_LoanType_Policies")

        Me.gridPolicies.DataSource = ds
        Me.gridPolicies.DataMember = "CIMS_m_LoanType_Policies"
        Me.gridPolicies.Columns("PolicyID").Visible = False
        mycon.sqlconn.Close()
    End Sub
#End Region
#Region "Delete Loan Policies"
    Private Sub DeleteLoanPolicies(ByVal Loancode As String)
        Dim mycon As New Clsappconfiguration
        mycon.sqlconn.Open()
        Dim trans As SqlTransaction = mycon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Masterfiles_LoanPolicy_Delete", _
                                      New SqlParameter("@loanPolicyCode", Loancode))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "DeleteLoanPolicies")
        Finally
            mycon.sqlconn.Close()
        End Try
    End Sub
#End Region
#Region "Cleartext"
    Private Sub Cleartext()
        Me.txtpolicydesc.Text = ""
        Me.txtcode.Text = "0"
        Me.chkCS1.Checked = False
        Me.chkTenure.Checked = False
        Me.chkCoopTen.Checked = False
        Me.chkCompTen.Checked = False
        Me.chkMaxTerms.Checked = False
        chkApplyPositionTypeLimit.Checked = False
    End Sub
#End Region
#Region "Load Policy Description"
    Private Sub PolicyDescription(ByVal policycode As String)
        Dim mycon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "CIMS_t_Loans_LoanType_SelectParamcode", _
                                     New SqlParameter("@fcLoancode", policycode))
        Try
            While rd.Read
                Me.txtpolicydesc.Text = rd.Item(0)
            End While
            rd.Close()
            mycon.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "PolicyDescription")
        End Try
    End Sub
#End Region
    Private Sub frmLoanPoliciesMaster_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call _LoadAcctg_Proj()
        cboProjectName.Text = frmMasterfile_LoanType.cboProject.Text
        'Call loadType()
        Call LoanPolicyList_byProj()
        Me.txtcode.Text = frmMasterfile_LoanType.cboPoliciesCode.Text
        Me.txttempcode.Text = frmMasterfile_LoanType.cboPoliciesCode.Text
    End Sub
    Private Sub btnsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsave.Click
        Try
            If btnsave.Text = "New" Then
                Call Cleartext()
                btndelete.Visible = False
                btnupdate.Visible = False
                btnsave.Text = "Save"
                btnsave.Image = My.Resources.save2
                btnclose.Text = "Cancel"
                btnclose.Location = New System.Drawing.Point(88, 5)
                cboProjectName.Enabled = True
            ElseIf btnsave.Text = "Save" Then
                If Me.txtpolicydesc.Text <> "" Then
                    Call AddEditLoanPolicies(Me.txtcode.Text, Me.txtpolicydesc.Text, CSLimit, _
                                             tenurelimit, cooptenure, comptenure, maxterm, chkApplyPositionTypeLimit.Checked)
                    Call LoanPolicyList_byProj()
                    Call Cleartext()
                    btnsave.Text = "New"
                    btnsave.Image = My.Resources.new3
                    btnclose.Text = "Close"
                    btnclose.Location = New System.Drawing.Point(244, 5)
                    btndelete.Visible = True
                    btnupdate.Visible = True
                Else
                    MessageBox.Show("Loan Type is Required.", "Unable to save", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                End If
            Else
                MessageBox.Show("Empty field found! ", "Save", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, "AddEditLoanPolicies")
        End Try
    End Sub

    Private Sub btnupdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupdate.Click
        Try
            If btnupdate.Text = "Edit" Then
                'Me.txtcode.Text = Me.gridPolicies.CurrentRow.Cells("Policy Code").Value
                'Me.txtpolicydesc.Text = Me.gridPolicies.CurrentRow.Cells("Policy Description").Value
                'Me.chkinpercent.Checked = Me.gridPolicies.CurrentRow.Cells(2).Value
                'Me.chkInAmt.Checked = Me.gridPolicies.CurrentRow.Cells(3).Value
                'Me.chkInterest.Checked = Me.gridPolicies.CurrentRow.Cells(4).Value
                'Me.chkService.Checked = Me.gridPolicies.CurrentRow.Cells(5).Value
                'Me.chkCS.Checked = Me.gridPolicies.CurrentRow.Cells(6).Value
                'Me.chkCS1.Checked = Me.gridPolicies.CurrentRow.Cells(7).Value
                'Me.chkTenure.Checked = Me.gridPolicies.CurrentRow.Cells(8).Value
                'Me.chkCoopTen.Checked = Me.gridPolicies.CurrentRow.Cells(9).Value
                'Me.chkCompTen.Checked = Me.gridPolicies.CurrentRow.Cells(10).Value
                'Me.chkMaxTerms.Checked = Me.gridPolicies.CurrentRow.Cells(11).Value
                'chkApplyPositionTypeLimit.Checked = gridPolicies.CurrentRow.Cells(13).Value

                txtcode.Text = gridPolicies.CurrentRow.Cells("Policy Code").Value
                txtpolicydesc.Text = gridPolicies.CurrentRow.Cells("Policy Description").Value
                chkCS1.Checked = gridPolicies.CurrentRow.Cells("Policy CS Limit").Value
                chkTenure.Checked = gridPolicies.CurrentRow.Cells("Policy Tenure Limit").Value
                chkCoopTen.Checked = gridPolicies.CurrentRow.Cells("Tenure Cooperative").Value
                chkCompTen.Checked = gridPolicies.CurrentRow.Cells("Tenure Company").Value
                chkMaxTerms.Checked = gridPolicies.CurrentRow.Cells("Max Terms").Value
                chkApplyPositionTypeLimit.Checked = gridPolicies.CurrentRow.Cells("Position Type").Value

                Me.btnsave.Enabled = False
                btnupdate.Text = "Update"
                btnupdate.Image = My.Resources.save2
                btnclose.Text = "Cancel"
                btnclose.Location = New System.Drawing.Point(164, 5)
                btndelete.Visible = False
                cboProjectName.Enabled = True

            ElseIf btnupdate.Text = "Update" Then
                Call AddEditLoanPolicies(Me.txtcode.Text, Me.txtpolicydesc.Text, CSLimit, _
                                        tenurelimit, cooptenure, comptenure, maxterm, chkApplyPositionTypeLimit.Checked)
                Call Cleartext()
                Call LoanPolicyList_byProj()
                btndelete.Visible = True
                btnupdate.Text = "Edit"
                btnupdate.Image = My.Resources.edit1
                btnclose.Text = "Cancel"
                btnclose.Location = New System.Drawing.Point(218, 4)
                btnclose.Text = "Close"
                Me.btnsave.Enabled = True
                Me.btndelete.Visible = True

            Else
                MessageBox.Show("Empty field found! ", "Edit", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub btndelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndelete.Click
        Try
            Dim x As New DialogResult
            x = MessageBox.Show("Are you sure you want to permanently delete this record?", "Confirm Deletion", MessageBoxButtons.OKCancel, MessageBoxIcon.Information)
            If x = System.Windows.Forms.DialogResult.OK Then
                Call DeleteLoanPolicies(Me.gridPolicies.CurrentRow.Cells(0).Value)
                Call LoanPolicyList_byProj()
            ElseIf x = System.Windows.Forms.DialogResult.Cancel Then
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        If btnclose.Text = "Cancel" Then
            btnsave.Text = "New"
            btnsave.Image = My.Resources.new3
            btnupdate.Text = "Edit"
            btnupdate.Image = My.Resources.edit1
            btnclose.Text = "Close"
            btnclose.Location = New System.Drawing.Point(244, 5)
            btndelete.Visible = True
            btnupdate.Visible = True
            btnsave.Enabled = True
        ElseIf btnclose.Text = "Close" Then
            Me.Close()
        End If
    End Sub

    Private Sub btnViewshare_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnViewshare.Click
        If Me.txtcode.Text = "" Then
            MessageBox.Show("Select one policy code", "Policy", MessageBoxButtons.OK)
        Else
            frmLoan_Policy_CSTenureLimit.Show()
        End If


    End Sub
#Region "view directory"
    Private Sub viewdirectory()
        ' First create a FolderBrowserDialog object
        Dim FolderBrowserDialog1 As New FolderBrowserDialog

        ' Then use the following code to create the Dialog window
        ' Change the .SelectedPath property to the default location
        With FolderBrowserDialog1
            ' Desktop is the root folder in the dialog.
            .RootFolder = Environment.SpecialFolder.Desktop
            ' Select the C:\Windows directory on entry.
            .SelectedPath = "c:\windows"
            ' Prompt the user with a custom message.
            .Description = "Select the source directory"
            If .ShowDialog = DialogResult.OK Then
                ' Display the selected folder if the user clicked on the OK button.
                MessageBox.Show(.SelectedPath)
            End If
        End With
    End Sub
#End Region

    Private Sub cboloantype_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.KeyChar = Microsoft.VisualBasic.ChrW(0)
    End Sub

    Private Sub chkTenure_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkTenure.CheckedChanged
        If Me.chkTenure.Checked = True Then
            gettenurelimit() = True
        Else
            gettenurelimit() = False
        End If
    End Sub

    Private Sub chkCoopTen_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkCoopTen.CheckedChanged
        If Me.chkCoopTen.Checked = True Then
            getcooptenure() = True
            Me.chkCompTen.Checked = False
        Else
            getcooptenure() = False
        End If
    End Sub

    Private Sub chkCompTen_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkCompTen.CheckedChanged
        If Me.chkCompTen.Checked = True Then
            getcomptenure() = True
            Me.chkCoopTen.Checked = False
        Else
            getcomptenure() = False
        End If
    End Sub

    Private Sub chkMaxTerms_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkMaxTerms.CheckedChanged
        If Me.chkMaxTerms.Checked = True Then
            getmaxterm() = True
        Else
            getmaxterm() = False
        End If
    End Sub
#Region "GridPolicies keydown/keypress"
    Private Sub LoadPolicyDetail()
        Try
            txtcode.Text = gridPolicies.CurrentRow.Cells("Policy Code").Value
            txtpolicydesc.Text = gridPolicies.CurrentRow.Cells("Policy Description").Value
            chkCS1.Checked = gridPolicies.CurrentRow.Cells("Policy CS Limit").Value
            chkTenure.Checked = gridPolicies.CurrentRow.Cells("Policy Tenure Limit").Value
            chkCoopTen.Checked = gridPolicies.CurrentRow.Cells("Tenure Cooperative").Value
            chkCompTen.Checked = gridPolicies.CurrentRow.Cells("Tenure Company").Value
            chkMaxTerms.Checked = gridPolicies.CurrentRow.Cells("Max Terms").Value
            'chkApplyPositionTypeLimit.Checked = gridPolicies.CurrentRow.Cells("Position Type").Value
        Catch ex As Exception
            MessageBox.Show(ex.Message, "gridPolicies_KeyDown/Keypress")
        End Try
    End Sub
#End Region

    Private Sub gridPolicies_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles gridPolicies.Click
        Call LoadPolicyDetail()
    End Sub

    Private Sub gridPolicies_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles gridPolicies.KeyDown
        Call LoadPolicyDetail()
    End Sub

    Private Sub gridPolicies_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles gridPolicies.KeyUp
        Call LoadPolicyDetail()
    End Sub

    Private Sub btnViewMaxterms_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnViewMaxterms.Click
        If Me.txtcode.Text = "" Then
            MessageBox.Show("Select one policy code", "Policy", MessageBoxButtons.OK)
        Else
            frmLoan_Policy_MaxTermLimit.Show()
        End If

    End Sub

    Private Sub gridPolicies_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles gridPolicies.CellContentClick
        Call LoadPolicyDetail()
    End Sub

    Private Sub txttempcode_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txttempcode.TextChanged
        Call ViewLoanPolicies(Me.txttempcode.Text)
    End Sub

    Private Sub LoadFirstPolicy()
        If gridPolicies.Rows.Count <> 0 Then
            gridPolicies.Rows(0).Selected = True
        End If
    End Sub

    Private Sub gridPolicies_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles gridPolicies.SelectionChanged
        Call LoadPolicyDetail()
    End Sub

    Private Sub btnViewPositionTypePolicies_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnViewPositionTypePolicies.Click
        frmLoan_Policy_PositionType.MdiParent = frmMain
        frmLoan_Policy_PositionType.Show()
    End Sub

#Region "Load Project"
    Private Sub _LoadAcctg_Proj()
        Dim mycon As New Clsappconfiguration
        Try
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(mycon.cnstring, "_Load_Project")
            cboProjectName.Items.Clear()
            While rd.Read
                With cboProjectName
                    .Items.Add(rd(0).ToString)
                End With
            End While
            cboProjectName.Text = "Select Project"
        Catch ex As Exception
            Throw
        End Try
    End Sub
#End Region

    Private Sub cboProjectName_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboProjectName.SelectedValueChanged
        Call LoanPolicyList_byProj()
    End Sub

    Private Sub chkCS1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkCS1.CheckedChanged
        If Me.chkTenure.Checked = True Then
            getCsLimit() = True
        Else
            getCsLimit() = False
        End If
    End Sub
End Class
