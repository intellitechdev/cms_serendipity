<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAmortizationSettlementReport
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAmortizationSettlementReport))
        Me.loadingPic = New System.Windows.Forms.PictureBox
        Me.bgwLoadReport = New System.ComponentModel.BackgroundWorker
        Me.crvAmortizationSettlement = New CrystalDecisions.Windows.Forms.CrystalReportViewer
        CType(Me.loadingPic, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'loadingPic
        '
        Me.loadingPic.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.loadingPic.Image = Global.WindowsApplication2.My.Resources.Resources.LoadingData
        Me.loadingPic.Location = New System.Drawing.Point(185, 159)
        Me.loadingPic.Name = "loadingPic"
        Me.loadingPic.Size = New System.Drawing.Size(133, 77)
        Me.loadingPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.loadingPic.TabIndex = 0
        Me.loadingPic.TabStop = False
        Me.loadingPic.Visible = False
        '
        'crvAmortizationSettlement
        '
        Me.crvAmortizationSettlement.ActiveViewIndex = -1
        Me.crvAmortizationSettlement.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.crvAmortizationSettlement.DisplayGroupTree = False
        Me.crvAmortizationSettlement.Dock = System.Windows.Forms.DockStyle.Fill
        Me.crvAmortizationSettlement.Location = New System.Drawing.Point(0, 0)
        Me.crvAmortizationSettlement.Name = "crvAmortizationSettlement"
        Me.crvAmortizationSettlement.SelectionFormula = ""
        Me.crvAmortizationSettlement.ShowGroupTreeButton = False
        Me.crvAmortizationSettlement.ShowRefreshButton = False
        Me.crvAmortizationSettlement.Size = New System.Drawing.Size(500, 422)
        Me.crvAmortizationSettlement.TabIndex = 1
        Me.crvAmortizationSettlement.ViewTimeSelectionFormula = ""
        '
        'frmAmortizationSettlementReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(500, 422)
        Me.Controls.Add(Me.loadingPic)
        Me.Controls.Add(Me.crvAmortizationSettlement)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmAmortizationSettlementReport"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Amortization Settlement Report"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.loadingPic, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents loadingPic As System.Windows.Forms.PictureBox
    Friend WithEvents bgwLoadReport As System.ComponentModel.BackgroundWorker
    Friend WithEvents crvAmortizationSettlement As CrystalDecisions.Windows.Forms.CrystalReportViewer
End Class
