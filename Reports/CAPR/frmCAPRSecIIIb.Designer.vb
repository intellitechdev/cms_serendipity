<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCAPRSecIIIb
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TabControl1 = New System.Windows.Forms.TabControl
        Me.TabPage1 = New System.Windows.Forms.TabPage
        Me.txt46Amt = New System.Windows.Forms.TextBox
        Me.txt46No = New System.Windows.Forms.TextBox
        Me.txt45Amt = New System.Windows.Forms.TextBox
        Me.txt45No = New System.Windows.Forms.TextBox
        Me.txt44Amt = New System.Windows.Forms.TextBox
        Me.txt44No = New System.Windows.Forms.TextBox
        Me.txt43Amt = New System.Windows.Forms.TextBox
        Me.txt43No = New System.Windows.Forms.TextBox
        Me.txt52Amt = New System.Windows.Forms.TextBox
        Me.txt42Amt = New System.Windows.Forms.TextBox
        Me.txt52No = New System.Windows.Forms.TextBox
        Me.txt42No = New System.Windows.Forms.TextBox
        Me.txt51Amt = New System.Windows.Forms.TextBox
        Me.txt41Amt = New System.Windows.Forms.TextBox
        Me.txt51No = New System.Windows.Forms.TextBox
        Me.txt41No = New System.Windows.Forms.TextBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label16 = New System.Windows.Forms.Label
        Me.Label15 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label14 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.TabPage2 = New System.Windows.Forms.TabPage
        Me.txt622Amt = New System.Windows.Forms.TextBox
        Me.txt621Amt = New System.Windows.Forms.TextBox
        Me.txt613Amt = New System.Windows.Forms.TextBox
        Me.txt612Amt = New System.Windows.Forms.TextBox
        Me.txt611Amt = New System.Windows.Forms.TextBox
        Me.txt622No = New System.Windows.Forms.TextBox
        Me.txt621No = New System.Windows.Forms.TextBox
        Me.txt613No = New System.Windows.Forms.TextBox
        Me.txt612No = New System.Windows.Forms.TextBox
        Me.txt611No = New System.Windows.Forms.TextBox
        Me.Label23 = New System.Windows.Forms.Label
        Me.Label22 = New System.Windows.Forms.Label
        Me.Label21 = New System.Windows.Forms.Label
        Me.Label20 = New System.Windows.Forms.Label
        Me.Label19 = New System.Windows.Forms.Label
        Me.Label26 = New System.Windows.Forms.Label
        Me.Label25 = New System.Windows.Forms.Label
        Me.Label24 = New System.Windows.Forms.Label
        Me.Label29 = New System.Windows.Forms.Label
        Me.Label28 = New System.Windows.Forms.Label
        Me.Label27 = New System.Windows.Forms.Label
        Me.Label18 = New System.Windows.Forms.Label
        Me.Label17 = New System.Windows.Forms.Label
        Me.TabPage3 = New System.Windows.Forms.TabPage
        Me.txt77Amt = New System.Windows.Forms.TextBox
        Me.txt76Amt = New System.Windows.Forms.TextBox
        Me.txt75Amt = New System.Windows.Forms.TextBox
        Me.txt74Amt = New System.Windows.Forms.TextBox
        Me.txt73Amt = New System.Windows.Forms.TextBox
        Me.txt72Amt = New System.Windows.Forms.TextBox
        Me.txt71Amt = New System.Windows.Forms.TextBox
        Me.Label31 = New System.Windows.Forms.Label
        Me.Label41 = New System.Windows.Forms.Label
        Me.Label40 = New System.Windows.Forms.Label
        Me.Label39 = New System.Windows.Forms.Label
        Me.Label38 = New System.Windows.Forms.Label
        Me.Label37 = New System.Windows.Forms.Label
        Me.Label36 = New System.Windows.Forms.Label
        Me.Label35 = New System.Windows.Forms.Label
        Me.Label34 = New System.Windows.Forms.Label
        Me.Label33 = New System.Windows.Forms.Label
        Me.Label30 = New System.Windows.Forms.Label
        Me.TabPage4 = New System.Windows.Forms.TabPage
        Me.txt86Discrp = New System.Windows.Forms.TextBox
        Me.txt85Discrp = New System.Windows.Forms.TextBox
        Me.txt84Discrp = New System.Windows.Forms.TextBox
        Me.txt83Discrp = New System.Windows.Forms.TextBox
        Me.txt82Discrp = New System.Windows.Forms.TextBox
        Me.txt81Discrp = New System.Windows.Forms.TextBox
        Me.txt86Distrb = New System.Windows.Forms.TextBox
        Me.txt85Distrb = New System.Windows.Forms.TextBox
        Me.txt84Distrb = New System.Windows.Forms.TextBox
        Me.txt83Distrb = New System.Windows.Forms.TextBox
        Me.txt82Distrb = New System.Windows.Forms.TextBox
        Me.txt86Aloc = New System.Windows.Forms.TextBox
        Me.txt85Aloc = New System.Windows.Forms.TextBox
        Me.txt84Aloc = New System.Windows.Forms.TextBox
        Me.txt83Aloc = New System.Windows.Forms.TextBox
        Me.txt82Aloc = New System.Windows.Forms.TextBox
        Me.txt81Distrb = New System.Windows.Forms.TextBox
        Me.txt86Amt = New System.Windows.Forms.TextBox
        Me.txt85Amt = New System.Windows.Forms.TextBox
        Me.txt84Amt = New System.Windows.Forms.TextBox
        Me.txt83Amt = New System.Windows.Forms.TextBox
        Me.txt82Amt = New System.Windows.Forms.TextBox
        Me.txt81Aloc = New System.Windows.Forms.TextBox
        Me.txt86Percentage = New System.Windows.Forms.TextBox
        Me.txt85Percentage = New System.Windows.Forms.TextBox
        Me.txt84Percentage = New System.Windows.Forms.TextBox
        Me.txt83Percentage = New System.Windows.Forms.TextBox
        Me.txt82Percentage = New System.Windows.Forms.TextBox
        Me.txt81Amt = New System.Windows.Forms.TextBox
        Me.txt81Percentage = New System.Windows.Forms.TextBox
        Me.Label57 = New System.Windows.Forms.Label
        Me.Label56 = New System.Windows.Forms.Label
        Me.Label55 = New System.Windows.Forms.Label
        Me.Label54 = New System.Windows.Forms.Label
        Me.Label53 = New System.Windows.Forms.Label
        Me.Label52 = New System.Windows.Forms.Label
        Me.Label51 = New System.Windows.Forms.Label
        Me.Label50 = New System.Windows.Forms.Label
        Me.Label49 = New System.Windows.Forms.Label
        Me.Label48 = New System.Windows.Forms.Label
        Me.Label47 = New System.Windows.Forms.Label
        Me.Label46 = New System.Windows.Forms.Label
        Me.Label45 = New System.Windows.Forms.Label
        Me.Label44 = New System.Windows.Forms.Label
        Me.Label43 = New System.Windows.Forms.Label
        Me.Label42 = New System.Windows.Forms.Label
        Me.Label32 = New System.Windows.Forms.Label
        Me.btnNext = New System.Windows.Forms.Button
        Me.btnPrevious = New System.Windows.Forms.Button
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.TabPage4.SuspendLayout()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Controls.Add(Me.TabPage4)
        Me.TabControl1.Location = New System.Drawing.Point(0, 0)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(719, 362)
        Me.TabControl1.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.White
        Me.TabPage1.Controls.Add(Me.txt46Amt)
        Me.TabPage1.Controls.Add(Me.txt46No)
        Me.TabPage1.Controls.Add(Me.txt45Amt)
        Me.TabPage1.Controls.Add(Me.txt45No)
        Me.TabPage1.Controls.Add(Me.txt44Amt)
        Me.TabPage1.Controls.Add(Me.txt44No)
        Me.TabPage1.Controls.Add(Me.txt43Amt)
        Me.TabPage1.Controls.Add(Me.txt43No)
        Me.TabPage1.Controls.Add(Me.txt52Amt)
        Me.TabPage1.Controls.Add(Me.txt42Amt)
        Me.TabPage1.Controls.Add(Me.txt52No)
        Me.TabPage1.Controls.Add(Me.txt42No)
        Me.TabPage1.Controls.Add(Me.txt51Amt)
        Me.TabPage1.Controls.Add(Me.txt41Amt)
        Me.TabPage1.Controls.Add(Me.txt51No)
        Me.TabPage1.Controls.Add(Me.txt41No)
        Me.TabPage1.Controls.Add(Me.Label9)
        Me.TabPage1.Controls.Add(Me.Label11)
        Me.TabPage1.Controls.Add(Me.Label10)
        Me.TabPage1.Controls.Add(Me.Label16)
        Me.TabPage1.Controls.Add(Me.Label15)
        Me.TabPage1.Controls.Add(Me.Label8)
        Me.TabPage1.Controls.Add(Me.Label7)
        Me.TabPage1.Controls.Add(Me.Label6)
        Me.TabPage1.Controls.Add(Me.Label5)
        Me.TabPage1.Controls.Add(Me.Label4)
        Me.TabPage1.Controls.Add(Me.Label14)
        Me.TabPage1.Controls.Add(Me.Label3)
        Me.TabPage1.Controls.Add(Me.Label13)
        Me.TabPage1.Controls.Add(Me.Label2)
        Me.TabPage1.Controls.Add(Me.Label12)
        Me.TabPage1.Controls.Add(Me.Label1)
        Me.TabPage1.Location = New System.Drawing.Point(4, 24)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(711, 334)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "4 - 5"
        '
        'txt46Amt
        '
        Me.txt46Amt.BackColor = System.Drawing.Color.White
        Me.txt46Amt.Location = New System.Drawing.Point(563, 195)
        Me.txt46Amt.Name = "txt46Amt"
        Me.txt46Amt.ReadOnly = True
        Me.txt46Amt.Size = New System.Drawing.Size(140, 23)
        Me.txt46Amt.TabIndex = 3
        Me.txt46Amt.Text = "0.00"
        Me.txt46Amt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt46No
        '
        Me.txt46No.BackColor = System.Drawing.Color.White
        Me.txt46No.Location = New System.Drawing.Point(417, 195)
        Me.txt46No.Name = "txt46No"
        Me.txt46No.ReadOnly = True
        Me.txt46No.Size = New System.Drawing.Size(140, 23)
        Me.txt46No.TabIndex = 3
        Me.txt46No.Text = "0"
        Me.txt46No.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt45Amt
        '
        Me.txt45Amt.Location = New System.Drawing.Point(563, 166)
        Me.txt45Amt.Name = "txt45Amt"
        Me.txt45Amt.Size = New System.Drawing.Size(140, 23)
        Me.txt45Amt.TabIndex = 3
        Me.txt45Amt.Text = "0.00"
        Me.txt45Amt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt45No
        '
        Me.txt45No.AcceptsReturn = True
        Me.txt45No.Location = New System.Drawing.Point(417, 166)
        Me.txt45No.Name = "txt45No"
        Me.txt45No.Size = New System.Drawing.Size(140, 23)
        Me.txt45No.TabIndex = 3
        Me.txt45No.Text = "0"
        Me.txt45No.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt44Amt
        '
        Me.txt44Amt.Location = New System.Drawing.Point(563, 137)
        Me.txt44Amt.Name = "txt44Amt"
        Me.txt44Amt.Size = New System.Drawing.Size(140, 23)
        Me.txt44Amt.TabIndex = 3
        Me.txt44Amt.Text = "0.00"
        Me.txt44Amt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt44No
        '
        Me.txt44No.Location = New System.Drawing.Point(417, 137)
        Me.txt44No.Name = "txt44No"
        Me.txt44No.Size = New System.Drawing.Size(140, 23)
        Me.txt44No.TabIndex = 3
        Me.txt44No.Text = "0"
        Me.txt44No.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt43Amt
        '
        Me.txt43Amt.Location = New System.Drawing.Point(563, 108)
        Me.txt43Amt.Name = "txt43Amt"
        Me.txt43Amt.Size = New System.Drawing.Size(140, 23)
        Me.txt43Amt.TabIndex = 3
        Me.txt43Amt.Text = "0.00"
        Me.txt43Amt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt43No
        '
        Me.txt43No.Location = New System.Drawing.Point(417, 108)
        Me.txt43No.Name = "txt43No"
        Me.txt43No.Size = New System.Drawing.Size(140, 23)
        Me.txt43No.TabIndex = 3
        Me.txt43No.Text = "0"
        Me.txt43No.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt52Amt
        '
        Me.txt52Amt.Location = New System.Drawing.Point(563, 304)
        Me.txt52Amt.Name = "txt52Amt"
        Me.txt52Amt.Size = New System.Drawing.Size(140, 23)
        Me.txt52Amt.TabIndex = 3
        Me.txt52Amt.Text = "0.00"
        Me.txt52Amt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt42Amt
        '
        Me.txt42Amt.Location = New System.Drawing.Point(563, 79)
        Me.txt42Amt.Name = "txt42Amt"
        Me.txt42Amt.Size = New System.Drawing.Size(140, 23)
        Me.txt42Amt.TabIndex = 3
        Me.txt42Amt.Text = "0.00"
        Me.txt42Amt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt52No
        '
        Me.txt52No.Location = New System.Drawing.Point(417, 304)
        Me.txt52No.Name = "txt52No"
        Me.txt52No.Size = New System.Drawing.Size(140, 23)
        Me.txt52No.TabIndex = 3
        Me.txt52No.Text = "0"
        Me.txt52No.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt42No
        '
        Me.txt42No.Location = New System.Drawing.Point(417, 79)
        Me.txt42No.Name = "txt42No"
        Me.txt42No.Size = New System.Drawing.Size(140, 23)
        Me.txt42No.TabIndex = 3
        Me.txt42No.Text = "0"
        Me.txt42No.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt51Amt
        '
        Me.txt51Amt.Location = New System.Drawing.Point(563, 275)
        Me.txt51Amt.Name = "txt51Amt"
        Me.txt51Amt.Size = New System.Drawing.Size(140, 23)
        Me.txt51Amt.TabIndex = 3
        Me.txt51Amt.Text = "0.00"
        Me.txt51Amt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt41Amt
        '
        Me.txt41Amt.Location = New System.Drawing.Point(563, 50)
        Me.txt41Amt.Name = "txt41Amt"
        Me.txt41Amt.Size = New System.Drawing.Size(140, 23)
        Me.txt41Amt.TabIndex = 3
        Me.txt41Amt.Text = "0.00"
        Me.txt41Amt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt51No
        '
        Me.txt51No.Location = New System.Drawing.Point(417, 275)
        Me.txt51No.Name = "txt51No"
        Me.txt51No.Size = New System.Drawing.Size(140, 23)
        Me.txt51No.TabIndex = 3
        Me.txt51No.Text = "0"
        Me.txt51No.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt41No
        '
        Me.txt41No.Location = New System.Drawing.Point(417, 50)
        Me.txt41No.Name = "txt41No"
        Me.txt41No.Size = New System.Drawing.Size(140, 23)
        Me.txt41No.TabIndex = 3
        Me.txt41No.Text = "0"
        Me.txt41No.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(180, 203)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(35, 15)
        Me.Label9.TabIndex = 2
        Me.Label9.Text = "Total"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(8, 242)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(226, 18)
        Me.Label11.TabIndex = 1
        Me.Label11.Text = "5. Loan Loss / Recovery Information"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(6, 14)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(370, 18)
        Me.Label10.TabIndex = 1
        Me.Label10.Text = "4.  Aging of Loan Receivables - Using Portfolio at Risk (PAR)"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(20, 307)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(183, 15)
        Me.Label16.TabIndex = 1
        Me.Label16.Text = "Loans Recovered within the year"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(20, 278)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(185, 15)
        Me.Label15.TabIndex = 1
        Me.Label15.Text = "Loans written off during the year"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(20, 169)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(342, 15)
        Me.Label8.TabIndex = 1
        Me.Label8.Text = "Loans outstanding with over twelve months missed payments"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(20, 140)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(385, 15)
        Me.Label7.TabIndex = 1
        Me.Label7.Text = "Loans outstanding with missed payments of 31 days up to 12 months"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(20, 111)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(351, 15)
        Me.Label6.TabIndex = 1
        Me.Label6.Text = "Loans outstanding with one-day missed payment up to 30 days"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(20, 82)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(102, 15)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = "Delinquent Loans"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(20, 53)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(85, 15)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "Cuurent Loans"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(610, 257)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(49, 15)
        Me.Label14.TabIndex = 0
        Me.Label14.Text = "Amount"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(610, 32)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(49, 15)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Amount"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(426, 257)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(121, 15)
        Me.Label13.TabIndex = 0
        Me.Label13.Text = "No. of Loan Accounts"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(426, 32)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(121, 15)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "No. of Loan Accounts"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(164, 257)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(69, 15)
        Me.Label12.TabIndex = 0
        Me.Label12.Text = "Particulars"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(164, 32)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(69, 15)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Particulars"
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.Color.White
        Me.TabPage2.Controls.Add(Me.txt622Amt)
        Me.TabPage2.Controls.Add(Me.txt621Amt)
        Me.TabPage2.Controls.Add(Me.txt613Amt)
        Me.TabPage2.Controls.Add(Me.txt612Amt)
        Me.TabPage2.Controls.Add(Me.txt611Amt)
        Me.TabPage2.Controls.Add(Me.txt622No)
        Me.TabPage2.Controls.Add(Me.txt621No)
        Me.TabPage2.Controls.Add(Me.txt613No)
        Me.TabPage2.Controls.Add(Me.txt612No)
        Me.TabPage2.Controls.Add(Me.txt611No)
        Me.TabPage2.Controls.Add(Me.Label23)
        Me.TabPage2.Controls.Add(Me.Label22)
        Me.TabPage2.Controls.Add(Me.Label21)
        Me.TabPage2.Controls.Add(Me.Label20)
        Me.TabPage2.Controls.Add(Me.Label19)
        Me.TabPage2.Controls.Add(Me.Label26)
        Me.TabPage2.Controls.Add(Me.Label25)
        Me.TabPage2.Controls.Add(Me.Label24)
        Me.TabPage2.Controls.Add(Me.Label29)
        Me.TabPage2.Controls.Add(Me.Label28)
        Me.TabPage2.Controls.Add(Me.Label27)
        Me.TabPage2.Controls.Add(Me.Label18)
        Me.TabPage2.Controls.Add(Me.Label17)
        Me.TabPage2.Location = New System.Drawing.Point(4, 24)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(711, 334)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "6"
        '
        'txt622Amt
        '
        Me.txt622Amt.Location = New System.Drawing.Point(424, 250)
        Me.txt622Amt.Name = "txt622Amt"
        Me.txt622Amt.Size = New System.Drawing.Size(159, 23)
        Me.txt622Amt.TabIndex = 2
        Me.txt622Amt.Text = "0.00"
        Me.txt622Amt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt621Amt
        '
        Me.txt621Amt.Location = New System.Drawing.Point(424, 221)
        Me.txt621Amt.Name = "txt621Amt"
        Me.txt621Amt.Size = New System.Drawing.Size(159, 23)
        Me.txt621Amt.TabIndex = 2
        Me.txt621Amt.Text = "0.00"
        Me.txt621Amt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt613Amt
        '
        Me.txt613Amt.Location = New System.Drawing.Point(424, 174)
        Me.txt613Amt.Name = "txt613Amt"
        Me.txt613Amt.Size = New System.Drawing.Size(159, 23)
        Me.txt613Amt.TabIndex = 2
        Me.txt613Amt.Text = "0.00"
        Me.txt613Amt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt612Amt
        '
        Me.txt612Amt.Location = New System.Drawing.Point(424, 145)
        Me.txt612Amt.Name = "txt612Amt"
        Me.txt612Amt.Size = New System.Drawing.Size(159, 23)
        Me.txt612Amt.TabIndex = 2
        Me.txt612Amt.Text = "0.00"
        Me.txt612Amt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt611Amt
        '
        Me.txt611Amt.Location = New System.Drawing.Point(424, 92)
        Me.txt611Amt.Name = "txt611Amt"
        Me.txt611Amt.Size = New System.Drawing.Size(159, 23)
        Me.txt611Amt.TabIndex = 2
        Me.txt611Amt.Text = "0.00"
        Me.txt611Amt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt622No
        '
        Me.txt622No.Location = New System.Drawing.Point(259, 250)
        Me.txt622No.Name = "txt622No"
        Me.txt622No.Size = New System.Drawing.Size(159, 23)
        Me.txt622No.TabIndex = 2
        Me.txt622No.Text = "0"
        Me.txt622No.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt621No
        '
        Me.txt621No.Location = New System.Drawing.Point(259, 221)
        Me.txt621No.Name = "txt621No"
        Me.txt621No.Size = New System.Drawing.Size(159, 23)
        Me.txt621No.TabIndex = 2
        Me.txt621No.Text = "0"
        Me.txt621No.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt613No
        '
        Me.txt613No.Location = New System.Drawing.Point(259, 174)
        Me.txt613No.Name = "txt613No"
        Me.txt613No.Size = New System.Drawing.Size(159, 23)
        Me.txt613No.TabIndex = 2
        Me.txt613No.Text = "0"
        Me.txt613No.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt612No
        '
        Me.txt612No.Location = New System.Drawing.Point(259, 145)
        Me.txt612No.Name = "txt612No"
        Me.txt612No.Size = New System.Drawing.Size(159, 23)
        Me.txt612No.TabIndex = 2
        Me.txt612No.Text = "0"
        Me.txt612No.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt611No
        '
        Me.txt611No.Location = New System.Drawing.Point(259, 92)
        Me.txt611No.Name = "txt611No"
        Me.txt611No.Size = New System.Drawing.Size(159, 23)
        Me.txt611No.TabIndex = 2
        Me.txt611No.Text = "0"
        Me.txt611No.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(98, 177)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(101, 15)
        Me.Label23.TabIndex = 1
        Me.Label23.Text = "- Over 12 months"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(98, 148)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(126, 15)
        Me.Label22.TabIndex = 1
        Me.Label22.Text = "- Less than 12 months"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(77, 124)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(90, 15)
        Me.Label21.TabIndex = 1
        Me.Label21.Text = "Loans with PAR"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(77, 95)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(48, 15)
        Me.Label20.TabIndex = 1
        Me.Label20.Text = "Current"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(47, 75)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(147, 15)
        Me.Label19.TabIndex = 1
        Me.Label19.Text = "DOSRI Loans Outstanding"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(47, 253)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(181, 15)
        Me.Label26.TabIndex = 1
        Me.Label26.Text = "Loans recovered within the year"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(47, 224)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(187, 15)
        Me.Label25.TabIndex = 1
        Me.Label25.Text = "Loans written Off during the year"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(33, 209)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(142, 15)
        Me.Label24.TabIndex = 1
        Me.Label24.Text = "2. Loan Loss Information"
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(480, 35)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(49, 15)
        Me.Label29.TabIndex = 1
        Me.Label29.Text = "Amount"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(285, 35)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(121, 15)
        Me.Label28.TabIndex = 1
        Me.Label28.Text = "No. of Loan Accounts"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(109, 35)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(69, 15)
        Me.Label27.TabIndex = 1
        Me.Label27.Text = "Particulars"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(33, 56)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(115, 15)
        Me.Label18.TabIndex = 1
        Me.Label18.Text = "1. Loan Information"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(8, 13)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(246, 18)
        Me.Label17.TabIndex = 0
        Me.Label17.Text = "6. Loans to Directors, Officers and Staff"
        '
        'TabPage3
        '
        Me.TabPage3.BackColor = System.Drawing.Color.White
        Me.TabPage3.Controls.Add(Me.txt77Amt)
        Me.TabPage3.Controls.Add(Me.txt76Amt)
        Me.TabPage3.Controls.Add(Me.txt75Amt)
        Me.TabPage3.Controls.Add(Me.txt74Amt)
        Me.TabPage3.Controls.Add(Me.txt73Amt)
        Me.TabPage3.Controls.Add(Me.txt72Amt)
        Me.TabPage3.Controls.Add(Me.txt71Amt)
        Me.TabPage3.Controls.Add(Me.Label31)
        Me.TabPage3.Controls.Add(Me.Label41)
        Me.TabPage3.Controls.Add(Me.Label40)
        Me.TabPage3.Controls.Add(Me.Label39)
        Me.TabPage3.Controls.Add(Me.Label38)
        Me.TabPage3.Controls.Add(Me.Label37)
        Me.TabPage3.Controls.Add(Me.Label36)
        Me.TabPage3.Controls.Add(Me.Label35)
        Me.TabPage3.Controls.Add(Me.Label34)
        Me.TabPage3.Controls.Add(Me.Label33)
        Me.TabPage3.Controls.Add(Me.Label30)
        Me.TabPage3.Location = New System.Drawing.Point(4, 24)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(711, 334)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "7"
        '
        'txt77Amt
        '
        Me.txt77Amt.BackColor = System.Drawing.Color.White
        Me.txt77Amt.Location = New System.Drawing.Point(240, 264)
        Me.txt77Amt.Name = "txt77Amt"
        Me.txt77Amt.ReadOnly = True
        Me.txt77Amt.Size = New System.Drawing.Size(170, 23)
        Me.txt77Amt.TabIndex = 5
        Me.txt77Amt.Text = "0.00"
        Me.txt77Amt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt76Amt
        '
        Me.txt76Amt.Location = New System.Drawing.Point(240, 235)
        Me.txt76Amt.Name = "txt76Amt"
        Me.txt76Amt.Size = New System.Drawing.Size(170, 23)
        Me.txt76Amt.TabIndex = 5
        Me.txt76Amt.Text = "0.00"
        Me.txt76Amt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt75Amt
        '
        Me.txt75Amt.Location = New System.Drawing.Point(240, 206)
        Me.txt75Amt.Name = "txt75Amt"
        Me.txt75Amt.Size = New System.Drawing.Size(170, 23)
        Me.txt75Amt.TabIndex = 5
        Me.txt75Amt.Text = "0.00"
        Me.txt75Amt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt74Amt
        '
        Me.txt74Amt.BackColor = System.Drawing.Color.White
        Me.txt74Amt.Location = New System.Drawing.Point(240, 169)
        Me.txt74Amt.Name = "txt74Amt"
        Me.txt74Amt.ReadOnly = True
        Me.txt74Amt.Size = New System.Drawing.Size(170, 23)
        Me.txt74Amt.TabIndex = 5
        Me.txt74Amt.Text = "0.00"
        Me.txt74Amt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt73Amt
        '
        Me.txt73Amt.Location = New System.Drawing.Point(240, 140)
        Me.txt73Amt.Name = "txt73Amt"
        Me.txt73Amt.Size = New System.Drawing.Size(170, 23)
        Me.txt73Amt.TabIndex = 5
        Me.txt73Amt.Text = "0.00"
        Me.txt73Amt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt72Amt
        '
        Me.txt72Amt.Location = New System.Drawing.Point(240, 111)
        Me.txt72Amt.Name = "txt72Amt"
        Me.txt72Amt.Size = New System.Drawing.Size(170, 23)
        Me.txt72Amt.TabIndex = 5
        Me.txt72Amt.Text = "0.00"
        Me.txt72Amt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt71Amt
        '
        Me.txt71Amt.Location = New System.Drawing.Point(240, 82)
        Me.txt71Amt.Name = "txt71Amt"
        Me.txt71Amt.Size = New System.Drawing.Size(170, 23)
        Me.txt71Amt.TabIndex = 5
        Me.txt71Amt.Text = "0.00"
        Me.txt71Amt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(302, 43)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(49, 15)
        Me.Label31.TabIndex = 4
        Me.Label31.Text = "Amount"
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Location = New System.Drawing.Point(20, 267)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(99, 15)
        Me.Label41.TabIndex = 2
        Me.Label41.Text = "Total Risk Assets"
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Location = New System.Drawing.Point(20, 238)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(121, 15)
        Me.Label40.TabIndex = 2
        Me.Label40.Text = "Less: Non-risk Assets"
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Location = New System.Drawing.Point(20, 209)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(73, 15)
        Me.Label39.TabIndex = 2
        Me.Label39.Text = "Total Assets"
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Location = New System.Drawing.Point(20, 172)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(94, 15)
        Me.Label38.TabIndex = 2
        Me.Label38.Text = "Total Net Worth"
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Location = New System.Drawing.Point(20, 143)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(161, 15)
        Me.Label37.TabIndex = 2
        Me.Label37.Text = "Less: Un-booked Allowances"
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Location = New System.Drawing.Point(33, 114)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(96, 15)
        Me.Label36.TabIndex = 2
        Me.Label36.Text = "Donated Capital"
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Location = New System.Drawing.Point(33, 85)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(181, 15)
        Me.Label35.TabIndex = 2
        Me.Label35.Text = "Paid-in Member's Share Capital"
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Location = New System.Drawing.Point(20, 67)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(78, 15)
        Me.Label34.TabIndex = 2
        Me.Label34.Text = "Total Capital"
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Location = New System.Drawing.Point(91, 43)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(69, 15)
        Me.Label33.TabIndex = 2
        Me.Label33.Text = "Particulars"
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label30.Location = New System.Drawing.Point(6, 13)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(446, 18)
        Me.Label30.TabIndex = 1
        Me.Label30.Text = "7. Compliance to Capital Adequacy Ration (net worth to risk asset ratio)"
        '
        'TabPage4
        '
        Me.TabPage4.BackColor = System.Drawing.Color.White
        Me.TabPage4.Controls.Add(Me.txt86Discrp)
        Me.TabPage4.Controls.Add(Me.txt85Discrp)
        Me.TabPage4.Controls.Add(Me.txt84Discrp)
        Me.TabPage4.Controls.Add(Me.txt83Discrp)
        Me.TabPage4.Controls.Add(Me.txt82Discrp)
        Me.TabPage4.Controls.Add(Me.txt81Discrp)
        Me.TabPage4.Controls.Add(Me.txt86Distrb)
        Me.TabPage4.Controls.Add(Me.txt85Distrb)
        Me.TabPage4.Controls.Add(Me.txt84Distrb)
        Me.TabPage4.Controls.Add(Me.txt83Distrb)
        Me.TabPage4.Controls.Add(Me.txt82Distrb)
        Me.TabPage4.Controls.Add(Me.txt86Aloc)
        Me.TabPage4.Controls.Add(Me.txt85Aloc)
        Me.TabPage4.Controls.Add(Me.txt84Aloc)
        Me.TabPage4.Controls.Add(Me.txt83Aloc)
        Me.TabPage4.Controls.Add(Me.txt82Aloc)
        Me.TabPage4.Controls.Add(Me.txt81Distrb)
        Me.TabPage4.Controls.Add(Me.txt86Amt)
        Me.TabPage4.Controls.Add(Me.txt85Amt)
        Me.TabPage4.Controls.Add(Me.txt84Amt)
        Me.TabPage4.Controls.Add(Me.txt83Amt)
        Me.TabPage4.Controls.Add(Me.txt82Amt)
        Me.TabPage4.Controls.Add(Me.txt81Aloc)
        Me.TabPage4.Controls.Add(Me.txt86Percentage)
        Me.TabPage4.Controls.Add(Me.txt85Percentage)
        Me.TabPage4.Controls.Add(Me.txt84Percentage)
        Me.TabPage4.Controls.Add(Me.txt83Percentage)
        Me.TabPage4.Controls.Add(Me.txt82Percentage)
        Me.TabPage4.Controls.Add(Me.txt81Amt)
        Me.TabPage4.Controls.Add(Me.txt81Percentage)
        Me.TabPage4.Controls.Add(Me.Label57)
        Me.TabPage4.Controls.Add(Me.Label56)
        Me.TabPage4.Controls.Add(Me.Label55)
        Me.TabPage4.Controls.Add(Me.Label54)
        Me.TabPage4.Controls.Add(Me.Label53)
        Me.TabPage4.Controls.Add(Me.Label52)
        Me.TabPage4.Controls.Add(Me.Label51)
        Me.TabPage4.Controls.Add(Me.Label50)
        Me.TabPage4.Controls.Add(Me.Label49)
        Me.TabPage4.Controls.Add(Me.Label48)
        Me.TabPage4.Controls.Add(Me.Label47)
        Me.TabPage4.Controls.Add(Me.Label46)
        Me.TabPage4.Controls.Add(Me.Label45)
        Me.TabPage4.Controls.Add(Me.Label44)
        Me.TabPage4.Controls.Add(Me.Label43)
        Me.TabPage4.Controls.Add(Me.Label42)
        Me.TabPage4.Controls.Add(Me.Label32)
        Me.TabPage4.Location = New System.Drawing.Point(4, 24)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage4.Size = New System.Drawing.Size(711, 334)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "8"
        '
        'txt86Discrp
        '
        Me.txt86Discrp.BackColor = System.Drawing.Color.White
        Me.txt86Discrp.Location = New System.Drawing.Point(625, 253)
        Me.txt86Discrp.Name = "txt86Discrp"
        Me.txt86Discrp.ReadOnly = True
        Me.txt86Discrp.Size = New System.Drawing.Size(82, 23)
        Me.txt86Discrp.TabIndex = 4
        Me.txt86Discrp.Text = "0.00"
        Me.txt86Discrp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt85Discrp
        '
        Me.txt85Discrp.Location = New System.Drawing.Point(625, 224)
        Me.txt85Discrp.Name = "txt85Discrp"
        Me.txt85Discrp.Size = New System.Drawing.Size(82, 23)
        Me.txt85Discrp.TabIndex = 4
        Me.txt85Discrp.Text = "0.00"
        Me.txt85Discrp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt84Discrp
        '
        Me.txt84Discrp.Location = New System.Drawing.Point(625, 195)
        Me.txt84Discrp.Name = "txt84Discrp"
        Me.txt84Discrp.Size = New System.Drawing.Size(82, 23)
        Me.txt84Discrp.TabIndex = 4
        Me.txt84Discrp.Text = "0.00"
        Me.txt84Discrp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt83Discrp
        '
        Me.txt83Discrp.Location = New System.Drawing.Point(625, 144)
        Me.txt83Discrp.Name = "txt83Discrp"
        Me.txt83Discrp.Size = New System.Drawing.Size(82, 23)
        Me.txt83Discrp.TabIndex = 4
        Me.txt83Discrp.Text = "0.00"
        Me.txt83Discrp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt82Discrp
        '
        Me.txt82Discrp.Location = New System.Drawing.Point(625, 115)
        Me.txt82Discrp.Name = "txt82Discrp"
        Me.txt82Discrp.Size = New System.Drawing.Size(82, 23)
        Me.txt82Discrp.TabIndex = 4
        Me.txt82Discrp.Text = "0.00"
        Me.txt82Discrp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt81Discrp
        '
        Me.txt81Discrp.Location = New System.Drawing.Point(625, 86)
        Me.txt81Discrp.Name = "txt81Discrp"
        Me.txt81Discrp.Size = New System.Drawing.Size(82, 23)
        Me.txt81Discrp.TabIndex = 4
        Me.txt81Discrp.Text = "0.00"
        Me.txt81Discrp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt86Distrb
        '
        Me.txt86Distrb.BackColor = System.Drawing.Color.White
        Me.txt86Distrb.Location = New System.Drawing.Point(537, 253)
        Me.txt86Distrb.Name = "txt86Distrb"
        Me.txt86Distrb.ReadOnly = True
        Me.txt86Distrb.Size = New System.Drawing.Size(82, 23)
        Me.txt86Distrb.TabIndex = 4
        Me.txt86Distrb.Text = "0.00"
        Me.txt86Distrb.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt85Distrb
        '
        Me.txt85Distrb.Location = New System.Drawing.Point(537, 224)
        Me.txt85Distrb.Name = "txt85Distrb"
        Me.txt85Distrb.Size = New System.Drawing.Size(82, 23)
        Me.txt85Distrb.TabIndex = 4
        Me.txt85Distrb.Text = "0.00"
        Me.txt85Distrb.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt84Distrb
        '
        Me.txt84Distrb.Location = New System.Drawing.Point(537, 195)
        Me.txt84Distrb.Name = "txt84Distrb"
        Me.txt84Distrb.Size = New System.Drawing.Size(82, 23)
        Me.txt84Distrb.TabIndex = 4
        Me.txt84Distrb.Text = "0.00"
        Me.txt84Distrb.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt83Distrb
        '
        Me.txt83Distrb.AcceptsTab = True
        Me.txt83Distrb.Location = New System.Drawing.Point(537, 144)
        Me.txt83Distrb.Name = "txt83Distrb"
        Me.txt83Distrb.Size = New System.Drawing.Size(82, 23)
        Me.txt83Distrb.TabIndex = 4
        Me.txt83Distrb.Text = "0.00"
        Me.txt83Distrb.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt82Distrb
        '
        Me.txt82Distrb.Location = New System.Drawing.Point(537, 115)
        Me.txt82Distrb.Name = "txt82Distrb"
        Me.txt82Distrb.Size = New System.Drawing.Size(82, 23)
        Me.txt82Distrb.TabIndex = 4
        Me.txt82Distrb.Text = "0.00"
        Me.txt82Distrb.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt86Aloc
        '
        Me.txt86Aloc.BackColor = System.Drawing.Color.White
        Me.txt86Aloc.Location = New System.Drawing.Point(449, 253)
        Me.txt86Aloc.Name = "txt86Aloc"
        Me.txt86Aloc.ReadOnly = True
        Me.txt86Aloc.Size = New System.Drawing.Size(82, 23)
        Me.txt86Aloc.TabIndex = 4
        Me.txt86Aloc.Text = "0.00"
        Me.txt86Aloc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt85Aloc
        '
        Me.txt85Aloc.Location = New System.Drawing.Point(449, 224)
        Me.txt85Aloc.Name = "txt85Aloc"
        Me.txt85Aloc.Size = New System.Drawing.Size(82, 23)
        Me.txt85Aloc.TabIndex = 4
        Me.txt85Aloc.Text = "0.00"
        Me.txt85Aloc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt84Aloc
        '
        Me.txt84Aloc.Location = New System.Drawing.Point(449, 195)
        Me.txt84Aloc.Name = "txt84Aloc"
        Me.txt84Aloc.Size = New System.Drawing.Size(82, 23)
        Me.txt84Aloc.TabIndex = 4
        Me.txt84Aloc.Text = "0.00"
        Me.txt84Aloc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt83Aloc
        '
        Me.txt83Aloc.Location = New System.Drawing.Point(449, 144)
        Me.txt83Aloc.Name = "txt83Aloc"
        Me.txt83Aloc.Size = New System.Drawing.Size(82, 23)
        Me.txt83Aloc.TabIndex = 4
        Me.txt83Aloc.Text = "0.00"
        Me.txt83Aloc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt82Aloc
        '
        Me.txt82Aloc.Location = New System.Drawing.Point(449, 115)
        Me.txt82Aloc.Name = "txt82Aloc"
        Me.txt82Aloc.Size = New System.Drawing.Size(82, 23)
        Me.txt82Aloc.TabIndex = 4
        Me.txt82Aloc.Text = "0.00"
        Me.txt82Aloc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt81Distrb
        '
        Me.txt81Distrb.Location = New System.Drawing.Point(537, 86)
        Me.txt81Distrb.Name = "txt81Distrb"
        Me.txt81Distrb.Size = New System.Drawing.Size(82, 23)
        Me.txt81Distrb.TabIndex = 4
        Me.txt81Distrb.Text = "0.00"
        Me.txt81Distrb.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt86Amt
        '
        Me.txt86Amt.BackColor = System.Drawing.Color.White
        Me.txt86Amt.Location = New System.Drawing.Point(361, 253)
        Me.txt86Amt.Name = "txt86Amt"
        Me.txt86Amt.ReadOnly = True
        Me.txt86Amt.Size = New System.Drawing.Size(82, 23)
        Me.txt86Amt.TabIndex = 4
        Me.txt86Amt.Text = "0.00"
        Me.txt86Amt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt85Amt
        '
        Me.txt85Amt.Location = New System.Drawing.Point(361, 224)
        Me.txt85Amt.Name = "txt85Amt"
        Me.txt85Amt.Size = New System.Drawing.Size(82, 23)
        Me.txt85Amt.TabIndex = 4
        Me.txt85Amt.Text = "0.00"
        Me.txt85Amt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt84Amt
        '
        Me.txt84Amt.Location = New System.Drawing.Point(361, 195)
        Me.txt84Amt.Name = "txt84Amt"
        Me.txt84Amt.Size = New System.Drawing.Size(82, 23)
        Me.txt84Amt.TabIndex = 4
        Me.txt84Amt.Text = "0.00"
        Me.txt84Amt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt83Amt
        '
        Me.txt83Amt.Location = New System.Drawing.Point(361, 144)
        Me.txt83Amt.Name = "txt83Amt"
        Me.txt83Amt.Size = New System.Drawing.Size(82, 23)
        Me.txt83Amt.TabIndex = 4
        Me.txt83Amt.Text = "0.00"
        Me.txt83Amt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt82Amt
        '
        Me.txt82Amt.Location = New System.Drawing.Point(361, 115)
        Me.txt82Amt.Name = "txt82Amt"
        Me.txt82Amt.Size = New System.Drawing.Size(82, 23)
        Me.txt82Amt.TabIndex = 4
        Me.txt82Amt.Text = "0.00"
        Me.txt82Amt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt81Aloc
        '
        Me.txt81Aloc.Location = New System.Drawing.Point(449, 86)
        Me.txt81Aloc.Name = "txt81Aloc"
        Me.txt81Aloc.Size = New System.Drawing.Size(82, 23)
        Me.txt81Aloc.TabIndex = 4
        Me.txt81Aloc.Text = "0.00"
        Me.txt81Aloc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt86Percentage
        '
        Me.txt86Percentage.BackColor = System.Drawing.Color.White
        Me.txt86Percentage.Location = New System.Drawing.Point(273, 253)
        Me.txt86Percentage.Name = "txt86Percentage"
        Me.txt86Percentage.ReadOnly = True
        Me.txt86Percentage.Size = New System.Drawing.Size(82, 23)
        Me.txt86Percentage.TabIndex = 4
        Me.txt86Percentage.Text = "0.00"
        Me.txt86Percentage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt85Percentage
        '
        Me.txt85Percentage.Location = New System.Drawing.Point(273, 224)
        Me.txt85Percentage.Name = "txt85Percentage"
        Me.txt85Percentage.Size = New System.Drawing.Size(82, 23)
        Me.txt85Percentage.TabIndex = 4
        Me.txt85Percentage.Text = "0.00"
        Me.txt85Percentage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt84Percentage
        '
        Me.txt84Percentage.Location = New System.Drawing.Point(273, 195)
        Me.txt84Percentage.Name = "txt84Percentage"
        Me.txt84Percentage.Size = New System.Drawing.Size(82, 23)
        Me.txt84Percentage.TabIndex = 4
        Me.txt84Percentage.Text = "0.00"
        Me.txt84Percentage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt83Percentage
        '
        Me.txt83Percentage.Location = New System.Drawing.Point(273, 144)
        Me.txt83Percentage.Name = "txt83Percentage"
        Me.txt83Percentage.Size = New System.Drawing.Size(82, 23)
        Me.txt83Percentage.TabIndex = 4
        Me.txt83Percentage.Text = "0.00"
        Me.txt83Percentage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt82Percentage
        '
        Me.txt82Percentage.Location = New System.Drawing.Point(273, 115)
        Me.txt82Percentage.Name = "txt82Percentage"
        Me.txt82Percentage.Size = New System.Drawing.Size(82, 23)
        Me.txt82Percentage.TabIndex = 4
        Me.txt82Percentage.Text = "0.00"
        Me.txt82Percentage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt81Amt
        '
        Me.txt81Amt.Location = New System.Drawing.Point(361, 86)
        Me.txt81Amt.Name = "txt81Amt"
        Me.txt81Amt.Size = New System.Drawing.Size(82, 23)
        Me.txt81Amt.TabIndex = 4
        Me.txt81Amt.Text = "0.00"
        Me.txt81Amt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt81Percentage
        '
        Me.txt81Percentage.Location = New System.Drawing.Point(273, 86)
        Me.txt81Percentage.Name = "txt81Percentage"
        Me.txt81Percentage.Size = New System.Drawing.Size(82, 23)
        Me.txt81Percentage.TabIndex = 4
        Me.txt81Percentage.Text = "0.00"
        Me.txt81Percentage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label57
        '
        Me.Label57.AutoSize = True
        Me.Label57.Location = New System.Drawing.Point(623, 42)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(85, 15)
        Me.Label57.TabIndex = 3
        Me.Label57.Text = "Discrepancies"
        '
        'Label56
        '
        Me.Label56.AutoSize = True
        Me.Label56.Location = New System.Drawing.Point(542, 51)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(69, 15)
        Me.Label56.TabIndex = 3
        Me.Label56.Text = "Distributed"
        '
        'Label55
        '
        Me.Label55.AutoSize = True
        Me.Label55.Location = New System.Drawing.Point(458, 51)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(59, 15)
        Me.Label55.TabIndex = 3
        Me.Label55.Text = "Allocated"
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Location = New System.Drawing.Point(491, 36)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(87, 15)
        Me.Label54.TabIndex = 3
        Me.Label54.Text = "Actual amount"
        '
        'Label53
        '
        Me.Label53.AutoSize = True
        Me.Label53.Location = New System.Drawing.Point(376, 51)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(49, 15)
        Me.Label53.TabIndex = 3
        Me.Label53.Text = "Amount"
        '
        'Label52
        '
        Me.Label52.AutoSize = True
        Me.Label52.Location = New System.Drawing.Point(279, 51)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(67, 15)
        Me.Label52.TabIndex = 3
        Me.Label52.Text = "Percentage"
        '
        'Label51
        '
        Me.Label51.AutoSize = True
        Me.Label51.Location = New System.Drawing.Point(290, 36)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(128, 15)
        Me.Label51.TabIndex = 3
        Me.Label51.Text = "Mandatory Allocation"
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.Location = New System.Drawing.Point(23, 256)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(219, 15)
        Me.Label50.TabIndex = 3
        Me.Label50.Text = "Total Amount of Undivided Net Surplus"
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.Location = New System.Drawing.Point(23, 227)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(104, 15)
        Me.Label49.TabIndex = 3
        Me.Label49.Text = "Patronage Refund"
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.Location = New System.Drawing.Point(23, 198)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(143, 15)
        Me.Label48.TabIndex = 3
        Me.Label48.Text = "Interest on Share Capital"
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.Location = New System.Drawing.Point(6, 174)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(177, 15)
        Me.Label47.TabIndex = 3
        Me.Label47.Text = "Amount Available for Members"
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.Location = New System.Drawing.Point(23, 147)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(234, 15)
        Me.Label46.TabIndex = 3
        Me.Label46.Text = "Cooperative Education and Training Fund"
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.Location = New System.Drawing.Point(23, 118)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(86, 15)
        Me.Label45.TabIndex = 3
        Me.Label45.Text = "Optional Fund"
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Location = New System.Drawing.Point(23, 89)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(79, 15)
        Me.Label44.TabIndex = 3
        Me.Label44.Text = "Reserve Fund"
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Location = New System.Drawing.Point(4, 71)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(179, 15)
        Me.Label43.TabIndex = 3
        Me.Label43.Text = "Mandatory Reserves Allocation"
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Location = New System.Drawing.Point(86, 42)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(69, 15)
        Me.Label42.TabIndex = 3
        Me.Label42.Text = "Particulars"
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label32.Location = New System.Drawing.Point(6, 13)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(176, 18)
        Me.Label32.TabIndex = 2
        Me.Label32.Text = "8. Allocation of Net Surplus"
        '
        'btnNext
        '
        Me.btnNext.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNext.Location = New System.Drawing.Point(629, 368)
        Me.btnNext.Name = "btnNext"
        Me.btnNext.Size = New System.Drawing.Size(78, 29)
        Me.btnNext.TabIndex = 3
        Me.btnNext.Text = "Next"
        Me.btnNext.UseVisualStyleBackColor = True
        '
        'btnPrevious
        '
        Me.btnPrevious.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPrevious.Location = New System.Drawing.Point(549, 368)
        Me.btnPrevious.Name = "btnPrevious"
        Me.btnPrevious.Size = New System.Drawing.Size(78, 29)
        Me.btnPrevious.TabIndex = 2
        Me.btnPrevious.Text = "Previous"
        Me.btnPrevious.UseVisualStyleBackColor = True
        '
        'frmCAPRSecIIIb
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(719, 402)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnNext)
        Me.Controls.Add(Me.btnPrevious)
        Me.Controls.Add(Me.TabControl1)
        Me.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frmCAPRSecIIIb"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cooperative Annual Performance Report - [Section III continuation]"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        Me.TabPage4.ResumeLayout(False)
        Me.TabPage4.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txt46Amt As System.Windows.Forms.TextBox
    Friend WithEvents txt46No As System.Windows.Forms.TextBox
    Friend WithEvents txt45Amt As System.Windows.Forms.TextBox
    Friend WithEvents txt45No As System.Windows.Forms.TextBox
    Friend WithEvents txt44Amt As System.Windows.Forms.TextBox
    Friend WithEvents txt44No As System.Windows.Forms.TextBox
    Friend WithEvents txt43Amt As System.Windows.Forms.TextBox
    Friend WithEvents txt43No As System.Windows.Forms.TextBox
    Friend WithEvents txt42Amt As System.Windows.Forms.TextBox
    Friend WithEvents txt42No As System.Windows.Forms.TextBox
    Friend WithEvents txt41Amt As System.Windows.Forms.TextBox
    Friend WithEvents txt41No As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txt52Amt As System.Windows.Forms.TextBox
    Friend WithEvents txt52No As System.Windows.Forms.TextBox
    Friend WithEvents txt51Amt As System.Windows.Forms.TextBox
    Friend WithEvents txt51No As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents btnNext As System.Windows.Forms.Button
    Friend WithEvents btnPrevious As System.Windows.Forms.Button
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents txt612Amt As System.Windows.Forms.TextBox
    Friend WithEvents txt611Amt As System.Windows.Forms.TextBox
    Friend WithEvents txt612No As System.Windows.Forms.TextBox
    Friend WithEvents txt611No As System.Windows.Forms.TextBox
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents txt613Amt As System.Windows.Forms.TextBox
    Friend WithEvents txt613No As System.Windows.Forms.TextBox
    Friend WithEvents txt622Amt As System.Windows.Forms.TextBox
    Friend WithEvents txt621Amt As System.Windows.Forms.TextBox
    Friend WithEvents txt622No As System.Windows.Forms.TextBox
    Friend WithEvents txt621No As System.Windows.Forms.TextBox
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents txt74Amt As System.Windows.Forms.TextBox
    Friend WithEvents txt73Amt As System.Windows.Forms.TextBox
    Friend WithEvents txt72Amt As System.Windows.Forms.TextBox
    Friend WithEvents txt71Amt As System.Windows.Forms.TextBox
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents txt77Amt As System.Windows.Forms.TextBox
    Friend WithEvents txt76Amt As System.Windows.Forms.TextBox
    Friend WithEvents txt75Amt As System.Windows.Forms.TextBox
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents Label52 As System.Windows.Forms.Label
    Friend WithEvents Label51 As System.Windows.Forms.Label
    Friend WithEvents Label50 As System.Windows.Forms.Label
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents Label56 As System.Windows.Forms.Label
    Friend WithEvents Label55 As System.Windows.Forms.Label
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents Label53 As System.Windows.Forms.Label
    Friend WithEvents Label57 As System.Windows.Forms.Label
    Friend WithEvents txt84Discrp As System.Windows.Forms.TextBox
    Friend WithEvents txt83Discrp As System.Windows.Forms.TextBox
    Friend WithEvents txt82Discrp As System.Windows.Forms.TextBox
    Friend WithEvents txt81Discrp As System.Windows.Forms.TextBox
    Friend WithEvents txt84Distrb As System.Windows.Forms.TextBox
    Friend WithEvents txt83Distrb As System.Windows.Forms.TextBox
    Friend WithEvents txt82Distrb As System.Windows.Forms.TextBox
    Friend WithEvents txt84Aloc As System.Windows.Forms.TextBox
    Friend WithEvents txt83Aloc As System.Windows.Forms.TextBox
    Friend WithEvents txt82Aloc As System.Windows.Forms.TextBox
    Friend WithEvents txt81Distrb As System.Windows.Forms.TextBox
    Friend WithEvents txt84Amt As System.Windows.Forms.TextBox
    Friend WithEvents txt83Amt As System.Windows.Forms.TextBox
    Friend WithEvents txt82Amt As System.Windows.Forms.TextBox
    Friend WithEvents txt81Aloc As System.Windows.Forms.TextBox
    Friend WithEvents txt84Percentage As System.Windows.Forms.TextBox
    Friend WithEvents txt83Percentage As System.Windows.Forms.TextBox
    Friend WithEvents txt82Percentage As System.Windows.Forms.TextBox
    Friend WithEvents txt81Amt As System.Windows.Forms.TextBox
    Friend WithEvents txt81Percentage As System.Windows.Forms.TextBox
    Friend WithEvents txt86Discrp As System.Windows.Forms.TextBox
    Friend WithEvents txt85Discrp As System.Windows.Forms.TextBox
    Friend WithEvents txt86Distrb As System.Windows.Forms.TextBox
    Friend WithEvents txt85Distrb As System.Windows.Forms.TextBox
    Friend WithEvents txt86Aloc As System.Windows.Forms.TextBox
    Friend WithEvents txt85Aloc As System.Windows.Forms.TextBox
    Friend WithEvents txt86Amt As System.Windows.Forms.TextBox
    Friend WithEvents txt85Amt As System.Windows.Forms.TextBox
    Friend WithEvents txt86Percentage As System.Windows.Forms.TextBox
    Friend WithEvents txt85Percentage As System.Windows.Forms.TextBox
End Class
