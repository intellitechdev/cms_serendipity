﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmChkVoucherEntry
    'Coded by : Vincent Nacar 
    Private gcon As New Clsappconfiguration
    Public LoanNo As String = ""
    Public empno As String
    Public fxkey As String
    Public xCtr As Integer
    Public totDebit As Double = 0
    Public totcredit As Double = 0
    Public keyresult As String = ""

    Dim i As Integer = 0

    Private Sub frmChkVoucherEntry_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        cbDocNum.Text = ""
        txtParticulars.Text = ""
        totDebit = 0
        totcredit = 0
        txtDate.Text = Format(Date.Now, "MM/dd/yyyy")
        LoadUser()
        'LoadCheck()
        GenerateLoanPaymentEntry()
        LoadDetails()
        _LoadAcctg_Proj()
    End Sub

    Private Sub GenerateLoanPaymentEntry()
        Dim btnselect As New DataGridViewCheckBoxColumn
        Dim IDNo As New DataGridViewTextBoxColumn
        Dim ClientName As New DataGridViewTextBoxColumn
        Dim LoanNo As New DataGridViewTextBoxColumn
        Dim AcctRef As New DataGridViewTextBoxColumn
        Dim Code As New DataGridViewTextBoxColumn
        Dim AccountTitle As New DataGridViewTextBoxColumn
        Dim Debit As New DataGridViewTextBoxColumn
        Dim Credit As New DataGridViewTextBoxColumn

        With btnselect
            .Name = "btnselect"
            .HeaderText = "Select"
            .Width = 60
        End With
        With IDNo
            .HeaderText = "ID No"
            .Name = "IDNo"
            .DataPropertyName = "IDNo"
            .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .ReadOnly = True
        End With
        With ClientName
            .HeaderText = "Client Name"
            .Name = "ClientName"
            .DataPropertyName = "ClientName"
            .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .ReadOnly = True
        End With
        With LoanNo
            .HeaderText = "Loan No."
            .Name = "LoanNo"
            .DataPropertyName = "LoanNo"
            .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .ReadOnly = True
        End With
        With AcctRef
            .HeaderText = "Acct Ref."
            .Name = "AcctRef"
            .DataPropertyName = "AcctRef"
            .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .ReadOnly = True
        End With
        With Code
            .HeaderText = "Code"
            .Name = "Code"
            .DataPropertyName = "Code"
            .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .ReadOnly = True
        End With
        With AccountTitle
            .HeaderText = "Account Title"
            .Name = "AccountTitle"
            .DataPropertyName = "AccountTitle"
            .ReadOnly = True
        End With
        With Debit
            .HeaderText = "Debit"
            .Name = "Debit"
            .DataPropertyName = "Debit"
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            '.ReadOnly = True
        End With
        With Credit
            .HeaderText = "Credit"
            .Name = "Credit"
            .DataPropertyName = "Credit"
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            ' .ReadOnly = True
        End With
        With dgvEntry
            .Columns.Clear()
            .Columns.Add(btnselect)
            .Columns.Add(IDNo)
            .Columns.Add(ClientName)
            .Columns.Add(LoanNo)
            .Columns.Add(AcctRef)
            .Columns.Add(Code)
            .Columns.Add(AccountTitle)
            .Columns.Add(Debit)
            .Columns.Add(Credit)
            .Columns("Debit").DefaultCellStyle.Format = "##,##0.00"
            .Columns("Credit").DefaultCellStyle.Format = "##,##0.00"
        End With

    End Sub

    Private Sub LoadDetails()
        Dim rd1 As SqlDataReader
        Dim rd2 As SqlDataReader

        Dim idno As String = ""
        Dim clientname As String = ""
        Dim xloanno As String = ""

        If frmLoan_Approval.txtEmployeeNumber.Text = "" Then
            rd1 = SqlHelper.ExecuteReader(gcon.cnstring, "_Select_Accounts_Detail_ForApproval",
                   New SqlParameter("@empNo", empno),
                   New SqlParameter("@LoanNo", LoanNo),
                   New SqlParameter("@CompanyName", frmLoan_ApproverList.cboProject.Text))
        Else
            rd1 = SqlHelper.ExecuteReader(gcon.cnstring, "_Select_Accounts_Detail_ForApproval",
                   New SqlParameter("@empNo", frmLoan_Approval.txtEmployeeNumber.Text),
                   New SqlParameter("@LoanNo", LoanNo),
                   New SqlParameter("@CompanyName", frmLoan_ApproverList.cboProject.Text))
        End If

        'rd2 = SqlHelper.ExecuteReader(gcon.cnstring, "_Select_Accounts_Detail_ForApproval_Details",
        '                               New SqlParameter("@LoanNo", LoanNo))
        rd2 = SqlHelper.ExecuteReader(gcon.cnstring, "_Generate_Loantype_Charges",
                               New SqlParameter("@LoanNo", LoanNo))
        With dgvEntry
            While rd1.Read
                .Rows.Add()
                'added 8/15/2014 Vince
                idno = rd1("ID No.").ToString
                clientname = rd1("Name").ToString
                xloanno = rd1("Loan Ref.").ToString

                .Item("IDNo", i).Value = idno
                .Item("ClientName", i).Value = clientname
                .Item("LoanNo", i).Value = xloanno
                .Item("Code", i).Value = rd1("Code").ToString
                .Item("AccountTitle", i).Value = rd1("Account Title").ToString
                .Item("Debit", i).Value = rd1("Debit").ToString
                .Item("Credit", i).Value = "0.00"
                totDebit = Val(.Item("Debit", i).Value)
                txtDebit.Text = Format(Val(txtDebit.Text) + totDebit, "##,##0.00")
                i += 1
            End While
            While rd2.Read
                .Rows.Add()
                .Item("IDNo", i).Value = idno
                .Item("ClientName", i).Value = clientname
                .Item("LoanNo", i).Value = xloanno
                .Item("AcctRef", i).Value = rd2("Acct. Ref").ToString
                .Item("Code", i).Value = rd2("Code").ToString
                .Item("AccountTitle", i).Value = rd2("Description").ToString
                .Item("Debit", i).Value = "0.00"
                .Item("Credit", i).Value = rd2("Credit").ToString
                totcredit = totcredit + Val(.Item("Credit", i).Value)
                txtCredit.Text = Format(Val(totcredit), "##,##0.00")
                i += 1
            End While
        End With

        txtNetproceeds.Text = Format((totDebit - totcredit), "##,##0.00")
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        txtDebit.Text = ""
        txtCredit.Text = ""
        GetJVKeyID = ""
        keyresult = "cancelled"
        i = 0
        dgvEntry.Columns.Clear()
        Me.Close()
    End Sub

    Private Sub LoadUser()
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gcon.cnstring, "_Select_Cashier",
                                      New SqlParameter("@username", mod_UsersAccessibility.xUser))
        While rd.Read
            txtCreatedby.Text = rd("email").ToString
            'txtCashier.Text = rd("Fullname")
        End While
    End Sub

    Private Sub SaveEntry()

        If GetJVKeyID() = "" Then
            GetJVKeyID() = System.Guid.NewGuid.ToString()
        End If

        Dim MyGuid As Guid = New Guid(GetJVKeyID)
        fxkey = MyGuid.ToString

        '### Added by Vince 10/7/14
        Dim maxSerialNo As Integer = 0
        Dim dctype As String = ""
        If cboDoctype.Text = "CHECK VOUCHER" Then
            maxSerialNo = Generate_JournalNo_Serial(Date.Now, "CHV")
            dctype = "CHV"
        End If
        If cboDoctype.Text = "CASH VOUCHER" Then
            maxSerialNo = Generate_JournalNo_Serial(Date.Now, "CV")
            dctype = "CV"
        End If
        If cboDoctype.Text = "JOURNAL VOUCHER" Then
            maxSerialNo = Generate_JournalNo_Serial(Date.Now, "JV")
            dctype = "JV"
        End If

        'Dim empno = dgvPaymentEntry.SelectedRows(0).Cells(0).Value.ToString

        SqlHelper.ExecuteNonQuery(gcon.cnstring, "CAS_t_GeneralJournal_Header_InsertUpdate",
                              New SqlParameter("@fxKeyJVNo", MyGuid),
                              New SqlParameter("@fdTransDate", txtDate.Text),
                              New SqlParameter("@fbAdjust", False),
                              New SqlParameter("@fcEmployeeNo", " "),
                              New SqlParameter("@fcMemo", " "),
                              New SqlParameter("@fdTotAmt", txtDebit.Text),
                              New SqlParameter("@user", txtCreatedby.Text),
                              New SqlParameter("@doctypeInitial", dctype),
                              New SqlParameter("@maxSerialNo", maxSerialNo),
                              New SqlParameter("@fiEntryNo", cbDocNum.Text),
                              New SqlParameter("@fbIsCancelled", False),
                              New SqlParameter("@nameOfCompany", cboProject.Text))

    End Sub

    Private jvKeyID As String

    Public Property GetJVKeyID() As String
        Get
            Return jvKeyID
        End Get
        Set(ByVal value As String)
            jvKeyID = value
        End Set
    End Property

    Private Function Generate_JournalNo_Serial(ByVal transDate As Date, ByVal docTypeInitial As String) As Integer
        Dim serialno As Integer
        ' Dim gCon As New Clsappconfiguration
        Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "usp_m_JournalNo_GenerateSerialPerMonthPerYear", _
                                                            New SqlParameter("@date", transDate), _
                                                            New SqlParameter("@doctypeInitial", docTypeInitial))
            If rd.Read() Then
                serialno = rd("maxSerialNo").ToString
            Else
                serialno = ""
            End If
            Return serialno
        End Using
    End Function

    Private Sub SaveDetails()
        Try
            Dim MyGuid As Guid = New Guid(fxkey)

            For xCtr = 0 To dgvEntry.RowCount - 1

                With dgvEntry

                    Dim accounttitle As String = IIf(IsDBNull(.Item("Code", xCtr).Value), Nothing, .Item("Code", xCtr).Value)
                    Dim empno As String = .Item("IDNo", xCtr).Value
                    Dim loan As String = .Item("LoanNo", xCtr).Value
                    Dim acctref As String = .Item("AcctRef", xCtr).Value
                    Dim debit As Double = .Item("Debit", xCtr).Value
                    Dim credit As Double = .Item("Credit", xCtr).Value

                    SqlHelper.ExecuteNonQuery(gcon.cnstring, "CIMS_t_tJVEntry_details_save",
                             New SqlParameter("@fiEntryNo", cbDocNum.Text),
                             New SqlParameter("@AccountCode", accounttitle),
                             New SqlParameter("@fdDebit", debit),
                             New SqlParameter("@fdCredit", credit),
                             New SqlParameter("@fcMemo", txtParticulars.Text),
                             New SqlParameter("@EmployeeNo", empno),
                             New SqlParameter("@fbBillable", False),
                             New SqlParameter("@fxKey", System.Guid.NewGuid),
                             New SqlParameter("@fk_JVHeader", MyGuid),
                             New SqlParameter("@transDate", txtDate.Text),
                             New SqlParameter("@fcLoanRef", loan),
                             New SqlParameter("@fcAccRef", acctref),
                             New SqlParameter("@nameOfCompany", cboProject.Text))

                End With

            Next
        Catch ex As Exception
            ' MessageBox.Show(ex.ToString)
            MsgBox(ex.ToString)
            'MessageBox.Show("Error! Duplicate ID of Accounts. Please ask Administrator.", "Oops")
            Exit Sub
        End Try

        xCtr = 0
    End Sub

    Private Sub UpdateDocNumber()
        SqlHelper.ExecuteNonQuery(gcon.cnstring, "_Update_DocNumber_Used",
                                   New SqlParameter("@DocNumber", cbDocNum.Text),
                                   New SqlParameter("@DateUsed", txtDate.Text))
    End Sub

    'Private Sub LoadCheck()
    '    cbDocNum.Items.Clear()
    '    Dim rd As SqlDataReader
    '    rd = SqlHelper.ExecuteReader(gcon.cnstring, "_LoadCHV")
    '    While rd.Read
    '        With cbDocNum
    '            .Items.Add(rd("fcDocNumber"))
    '        End With
    '    End While
    'End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If cboProject.Text = "Select Project" Then
                MsgBox("Please Select Project!", vbInformation, "Oops!")
                Exit Sub
            ElseIf cbDocNum.Text = "" Then
                MsgBox("Please Select Document No.!", vbInformation, "Oops!")
                Exit Sub
            Else
                SaveEntry()
                SaveDetails()
                UpdateDocNumber()

                frmMsgBox.txtMessage.Text = "Data Successfully Saved!"
                frmMsgBox.ShowDialog()

                GetJVKeyID = ""
                GenerateLoanPaymentEntry()
                txtDebit.Text = ""
                txtCredit.Text = ""

                keyresult = "Ok"
                frmLoan_Approval.btnApprove.Enabled = False
                frmLoan_Approval.btnDisapprove.Enabled = False
                i = 0
                dgvEntry.Columns.Clear()
                Me.Close()
            End If
        Catch ex As Exception
            frmMsgBox.txtMessage.Text = "Error :" + ex.ToString
            frmMsgBox.ShowDialog()
        End Try
    End Sub

    Private Sub btnOutstandingLoan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOutstandingLoan.Click
        frmOutstandingLoan.SelectLoan_byID(dgvEntry.Item("IDNo", 0).Value)
        frmOutstandingLoan.StartPosition = FormStartPosition.CenterScreen
        frmOutstandingLoan.ShowDialog()
    End Sub

    Public Sub AddOutstandingLoans(ByVal id As String, ByVal clientname As String, ByVal loanref As String,
                                   ByVal code As String, ByVal accttitle As String, ByVal balance As String)
        Try
            With dgvEntry
                .Rows.Add()
                .Item("IDNo", i).Value = id
                .Item("ClientName", i).Value = clientname
                .Item("LoanNo", i).Value = loanref
                .Item("Code", i).Value = code
                .Item("AccountTitle", i).Value = accttitle
                .Item("Debit", i).Value = "0.00"
                .Item("Credit", i).Value = balance
                totcredit = totcredit + Val(.Item("Credit", i).Value)
                txtCredit.Text = Format(Val(totcredit), "##,##0.00")
                i += 1
            End With
            RecomputeNetproceed()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub RecomputeNetproceed()
        Dim subtotcredit As Decimal
        Dim subtotdebit As Decimal

        For x As Integer = 0 To dgvEntry.RowCount - 1
            With dgvEntry
                subtotcredit = subtotcredit + Val(.Item("Credit", x).Value)
                subtotdebit = subtotdebit + Val(.Item("Debit", x).Value)
            End With
        Next
        txtDebit.Text = Format(subtotdebit, "##,##0.00")
        txtCredit.Text = Format(subtotcredit, "##,##0.00")
        txtNetproceeds.Text = Format(CDec(txtDebit.Text) - CDec(txtCredit.Text), "##,##0.00")
    End Sub

    Private Sub dgvEntry_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvEntry.CellValueChanged
        RecomputeNetproceed()
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        For Each row As DataGridViewRow In dgvEntry.Rows
            With dgvEntry
                If .Item("btnselect", row.Index).Value = True Then
                    .Rows.Remove(row)
                    i -= 1
                End If
            End With
        Next
    End Sub

    Private Sub btnCheckNo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCheckNo.Click
        If cboDoctype.Text = "" Then
            MsgBox("Please Select Document Type to continue.", vbInformation, "Message")
            Exit Sub
        Else
            If cboDoctype.Text = "CHECK VOUCHER" Then
                frmBrowseCheck.doctype = "CHV"
            End If
            If cboDoctype.Text = "CASH VOUCHER" Then
                frmBrowseCheck.doctype = "CV"
            End If
            If cboDoctype.Text = "JOURNAL VOUCHER" Then
                frmBrowseCheck.doctype = "JV"
            End If
            frmBrowseCheck.StartPosition = FormStartPosition.CenterScreen
            frmBrowseCheck.ShowDialog()
        End If
    End Sub

    Private Sub btnAddItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddItem.Click
        frmAddItem.StartPosition = FormStartPosition.CenterScreen
        frmAddItem.ShowDialog()
    End Sub

    Public Sub AddItems(ByVal id As String, ByVal clientname As String, ByVal loanref As String, ByVal acctref As String,
                               ByVal code As String, ByVal accttitle As String, ByVal balance As String)
        Try
            With dgvEntry
                .Rows.Add()
                .Item("IDNo", i).Value = id
                .Item("ClientName", i).Value = clientname
                .Item("LoanNo", i).Value = loanref
                .Item("AcctRef", i).Value = acctref
                .Item("Code", i).Value = code
                .Item("AccountTitle", i).Value = accttitle
                .Item("Debit", i).Value = "0.00"
                .Item("Credit", i).Value = balance
                totcredit = totcredit + Val(.Item("Credit", i).Value)
                txtCredit.Text = Format(Val(totcredit), "##,##0.00")
                i += 1
            End With
            'RecomputeNetproceed()
        Catch ex As Exception
            Throw
        End Try
    End Sub

#Region "Filter Project"

    Private Sub _LoadAcctg_Proj()
        Dim mycon As New Clsappconfiguration
        Try
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(mycon.cnstring, "_Load_Project")
            cboProject.Items.Clear()
            While rd.Read
                With cboProject
                    .Items.Add(rd(0).ToString)
                End With
            End While
            cboProject.Text = "Select Project"
        Catch ex As Exception
            Throw
        End Try
    End Sub
#End Region
End Class