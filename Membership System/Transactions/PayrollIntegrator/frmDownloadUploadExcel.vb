Imports System.Data.SqlClient
'Imports Microsoft.Office.Interop.Excel 'Excel =
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.OleDb, System.IO

Public Class frmDownloadUploadExcel
    
#Region "Variables"
    Public fkpaycode As String
    Public isloan As Boolean
    Public iscontribution As Boolean
    Public isbereavement As Boolean
    Public isdecimal As Decimal
    Public ispaydate As Date
    Public payrollcode As String

    Private gStrpath, gCnstring As String
    Private gOleConn As New OleDbConnection
    Private gOleCmd As New OleDbCommand
    Private gDT As New DataTable
    Private gErrmsg As String

    Private gIsPayrollCodeValid As Boolean
    Private gIsTranscodeValid As Boolean = False
    Private gTranscode As String
    Private gIsPaydateValid As Boolean
    Private gAreMembersExisting As Boolean

    Private gDeductionDetails As New DataSet
    Private gDeductionProcessed As New DataSet

    Private gFilePath As String

    Private gCon As New Clsappconfiguration
    Private xSQLHelper As New NewSQLHelper
    Public username As String
#End Region

#Region "Get property"
    Public Property GetUserName() As String
        Get
            Return username
        End Get
        Set(ByVal value As String)
            username = value
        End Set
    End Property
#End Region

#Region "Properties"
    Private hasUnsettled As Boolean
    Public Property HasUnsettledPayments() As Boolean
        Get
            Return hasUnsettled
        End Get
        Set(ByVal value As Boolean)
            hasUnsettled = value
        End Set
    End Property

    Public Property getisloan() As Boolean
        Get
            Return isloan
        End Get
        Set(ByVal value As Boolean)
            isloan = value
        End Set
    End Property
    Public Property getiscontribution() As Boolean
        Get
            Return iscontribution
        End Get
        Set(ByVal value As Boolean)
            iscontribution = value
        End Set
    End Property
    Public Property getisbereavement() As Boolean
        Get
            Return isbereavement
        End Get
        Set(ByVal value As Boolean)
            isbereavement = value
        End Set
    End Property
    Public Property getsfkpaycode() As String
        Get
            Return fkpaycode
        End Get
        Set(ByVal value As String)
            fkpaycode = value
        End Set
    End Property
    Public Property getIsDecimal() As Decimal
        Get
            Return isdecimal
        End Get
        Set(ByVal value As Decimal)
            isdecimal = value
        End Set
    End Property
    Public Property getIsPaydate() As Date
        Get
            Return ispaydate
        End Get
        Set(ByVal value As Date)
            ispaydate = value
        End Set
    End Property
    Public Property getpayrollcode() As String
        Get
            Return payrollcode
        End Get
        Set(ByVal value As String)
            payrollcode = value
        End Set
    End Property

#End Region

#Region "Function/SubRoutines"
    Private Sub LoadDownloadList()
        CheckForIllegalCrossThreadCalls = False
        Try
            Dim SQLStr As String = "SELECT TOP 30 t2.PayDesc AS Paycode, t1.fdPaydate, t1.fcType, t1.fcDownloadedBy " & _
                                    "FROM dbo.CIMS_t_PayrollIntegration_downloadHistory AS t1 " & _
                                    "INNER JOIN dbo.CIMS_m_Member_PayCode AS t2 " & _
                                    "ON t2.pk_PayCode = t1.fk_paycode " & _
                                    "GROUP BY t2.PayDesc, t1.fdPaydate, " & _
                                    "t1.fcType, t1.fcDownloadedBy " & _
                                    "ORDER BY t1.fdPaydate DESC,  t2.PayDesc"
            Dim Item As ListViewItem
            Dim DSSource As DataSet = xSQLHelper.ExecuteDataset(SQLStr)

            lstVwDownloadList.Items.Clear()
            For Each xRow As DataRow In DSSource.Tables(0).Rows
                Item = New ListViewItem
                Item.Text = xRow("Paycode").ToString()
                Item.SubItems.Add(Convert.ToDateTime(xRow("fdPaydate")).ToString("MM/dd/yyyy"))
                Item.SubItems.Add(xRow("fcType").ToString())
                Item.SubItems.Add(xRow("fcDownloadedBy").ToString())
                lstVwDownloadList.Items.Add(Item)
            Next
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error!")
        End Try

        '', t1.fcPostedBy, t1.fcSettledBy -'t1.fcPostedBy, t1.fcSettledBy, -'  'Item.SubItems.Add(xRow("fcPostedBy").ToString())
        'Item.SubItems.Add(xRow("fcSettledBy").ToString()) --where t1.fbIsIncludedInPayroll = 1 


    End Sub
    Private Shared Sub DisplayUnsettledPayments()
        frmUnsettledLoanPayments.MdiParent = frmMain
        frmUnsettledLoanPayments.Show()
    End Sub

    Public Sub DisplayDeductionDetails()
        Call DisplayMarqueeProgressBar("Downloading...")
        If bgwDeductionDetails.IsBusy = False Then
            bgwDeductionDetails.RunWorkerAsync()
        Else
            MessageBox.Show("Server is busy. Please try again.")
        End If
    End Sub

    Private Sub LoadPayrollCode()
        Dim gcon As New Clsappconfiguration
        Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.sqlconn, CommandType.StoredProcedure, "CIMS_Member_LoadPayCode")
            Try
                While rd.Read
                    cboPaycode.Items.Add(rd.Item(1))
                End While
                rd.Close()
            Catch ex As Exception
                MessageBox.Show(ex.Message, "PayrollCodeParam")
            End Try
        End Using
    End Sub


    Private Sub PayrollCodeParam(ByVal desc As String)
        Dim gcon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gcon.sqlconn, CommandType.StoredProcedure, "CIMS_Member_LoadPayCodeParam", _
                                    New SqlParameter("@paydesc", desc))
        Try
            While rd.Read
                getsfkpaycode() = rd.Item(0)
            End While
            rd.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "PayrollCodeParam")
        End Try
    End Sub

    Private Sub PaydateSchedule(ByVal paycode As String)
        Try
            cboPaydate.Items.Clear()
            Using rd As SqlDataReader = xSQLHelper.ExecuteReader("CIMS_PaydateSchedule_viewdate", New SqlParameter("@fk_PayCode", paycode))
                While rd.Read()
                    'Me.dtPaydate.Value = rd.Item(0)
                    cboPaydate.Items.Add(rd.Item(0))
                End While
            End Using
        Catch ex As Exception
            MessageBox.Show(ex.Message, "PaydateSchedule")
        End Try
    End Sub

    Private Sub MappingLoad()
        Dim mycon As New Clsappconfiguration
        Dim ad As New SqlDataAdapter
        Dim ds As New DataSet
        Dim cmd As New SqlCommand("CIMS_Download_Mapping_Load", mycon.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure

        ad.SelectCommand = cmd
        ad.Fill(ds, "CIMS_m_PayrollMapping")

        gridLoans.DataSource = ds
        gridLoans.DataMember = "CIMS_m_PayrollMapping"

        gridLoans.Columns("pk_PayrollMappingID").Visible = False

        gridLoans.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
        gridLoans.Columns("External Code").ReadOnly = True
        gridLoans.Columns("Category").ReadOnly = True

        gridLoans.Columns("Select").HeaderText = ""
        gridLoans.Columns("External Code").HeaderText = "Code"
        gridLoans.Columns("Category").HeaderText = "Loan Type"

        mycon.sqlconn.Close()
    End Sub

    Public Sub LoadDeductionDetails(ByVal paycode As String, ByVal isloanincluded As Boolean, ByVal isContributionIncluded As Boolean, _
                                   ByVal isBereavementIncluded As Boolean, ByVal bereavementAmount As Decimal, ByVal payDate As Date)

        Dim mycon As New Clsappconfiguration

        Try
            gDeductionDetails = xSQLHelper.ExecuteDataset("CIMS_Download_Details_FinalLoad", _
                    New SqlParameter("@paycode", paycode), _
                    New SqlParameter("@isLoanIncluded", isloanincluded), _
                    New SqlParameter("@isContributionIncluded", isContributionIncluded), _
                    New SqlParameter("@isBereavementIncluded", isBereavementIncluded), _
                    New SqlParameter("@bereavementAmount", bereavementAmount), _
                    New SqlParameter("@payDate", payDate))
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub

    Public Sub ProcessDownload(ByVal paycode As String, ByVal isloanincluded As Boolean, ByVal isContributionIncluded As Boolean, _
                                   ByVal isBereavementIncluded As Boolean, ByVal bereavementAmount As Decimal, ByVal payDate As Date)
        Try
            gDeductionProcessed = xSQLHelper.ExecuteDataset("CIMS_Download_Process_Load_Analog", _
                                    New SqlParameter("@paycode", paycode), _
                                    New SqlParameter("@isLoanIncluded", isloanincluded), _
                                    New SqlParameter("@isContributionIncluded", isContributionIncluded), _
                                    New SqlParameter("@isBereavementIncluded", isBereavementIncluded), _
                                    New SqlParameter("@bereavementAmount", bereavementAmount), _
                                    New SqlParameter("@payDate", payDate))
        Catch ex As Exception
            Throw ex
        End Try


    End Sub

    Private Function PostDownloadDetails(ByVal paycode As String, ByVal isloanincluded As Boolean, ByVal isContributionIncluded As Boolean, _
                                    ByVal isBereavementIncluded As Boolean, ByVal bereavementAmount As Decimal, ByVal payDate As Date) As Boolean
        Try
            'Dim user As String = frmMain.StLoginName.Text
            'Dim user2 As String = frmMain.getusername()

            xSQLHelper.ExecuteNonQuery("CIMS_Download_Details_PostToHistory_Analog", _
                                      New SqlParameter("@paycode", paycode), _
                                      New SqlParameter("@isLoanIncluded", isloanincluded), _
                                      New SqlParameter("@isContributionIncluded", isContributionIncluded), _
                                      New SqlParameter("@isBereavementIncluded", isBereavementIncluded), _
                                      New SqlParameter("@bereavementAmount", bereavementAmount), _
                                      New SqlParameter("@payDate", payDate), _
                                      New SqlParameter("@user", Me.GetUserName()))
            Return True
        Catch ex As Exception
            MessageBox.Show("CIMS_Download_Details_PostToHistory_Analog" & vbCr & ex.Message, "Insert failed", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Return False
        End Try

    End Function

    Private Sub InsertMappingTemptable(ByVal payrollid As String, ByVal payrollname As String)
        Dim mycon As New Clsappconfiguration
        mycon.sqlconn.Open()
        Dim trans As SqlTransaction = mycon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Download_TempTable_InsertOptions", _
                                     New SqlParameter("@payrollID", payrollid), _
                                     New SqlParameter("@payrollName", payrollname))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show("InsertMappingTemptable", "Insert Failed", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Finally
            mycon.sqlconn.Close()
        End Try

    End Sub

    Private Sub CSCodelabel()
        Dim mycon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "CIMS_Download_CSCode_Load")
        Try
            While rd.Read
                Me.chkContribution.Text = "Contribution" + "  ( " + rd.Item(0) + " )"
            End While
            rd.Read()
            mycon.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show("CSCodelabel", "CSCodelabel", MessageBoxButtons.OK)
        End Try
    End Sub

    Private Sub BCodelabel()
        Dim mycon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "CIMS_Download_BereavementCode_Load")
        Try
            While rd.Read
                Me.chkbereavement.Text = "Bereavement" + "  ( " + rd.Item(0) + " )"
            End While
            rd.Read()
            mycon.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show("BCodelabel", "BCodelabel", MessageBoxButtons.OK)
        End Try
    End Sub

    Public Function Numeric(ByVal C As Char) As Boolean
        If Not (Char.IsDigit(C)) And Not (C = ",") And Not (C = ".") And Not (Microsoft.VisualBasic.AscW(C) = 8) And Not (Microsoft.VisualBasic.AscW(C) = 13) Then
            MsgBox("Invalid Input! This field allows 0-9 and '.' only.", MsgBoxStyle.Exclamation, "Invalid")
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub getGridDate()
        Dim rows As Integer = Me.gridupload.Rows.Count
        If rows > 0 Then
            Me.txtpaydate.Text = Me.gridupload.Item(5, 0).Value.ToString
        End If
    End Sub

    Private Sub UploadExcelFile()
        If Me.txtfile.Text <> "" Then
            Me.Cursor = Cursors.WaitCursor
            Me.GetConnection(Me.txtfile.Text)
            Dim FileName As String = Path.GetFileName(Me.txtfile.Text)

            Dim oleDA As New OleDbDataAdapter("SELECT [PersNo],[LastnameFirstname],[PYAreaFP],[Payrollareatext],[Forperiod],[Pmtdate],[WT]," & _
                                             "[WageTypeLongText],[Numberof],[Amount],[Crcy] FROM [Sheet1$]", gOleConn)
            Try
                'get data from excel
                oleDA.Fill(gDT)
                Me.Cursor = Cursors.Arrow
                Dim rows As Integer = gDT.Rows.Count

                If gDT.Rows.Count <> 0 Then
                    Me.gridupload.DataSource = gDT
                End If

            Catch ex As Exception
                Me.Cursor = Cursors.Arrow
                MessageBox.Show(ex.Message, "Upload", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Finally
                gOleConn.Close()
            End Try
        End If


    End Sub

    Private Sub LoopInsertUploadedFile()
        Try
            ValidateExcelData()

            Dim i As Integer
            With frmStartup

                .MdiParent = frmMain
                .Show()
                .processWait.Minimum = 0
                .processWait.Maximum = Me.gridupload.Rows.Count - 1
                .lblTitle.Text = "Uploading. Please wait..."

                For i = 0 To Me.gridupload.Rows.Count - 1
                    .lblProgress.Text = Me.gridupload.Item(1, i).Value.ToString
                    .lblProcessStatus.Text = ""
                    .processWait.Increment(1)
                    .Refresh()

                    Call InsertUploadedFile(Me.gridupload.Item(0, i).Value, Me.gridupload.Item(1, i).Value, Me.gridupload.Item(2, i).Value, _
                               Me.gridupload.Item(3, i).Value, Me.gridupload.Item(4, i).Value, Me.gridupload.Item(5, i).Value, _
                               Me.gridupload.Item(6, i).Value, Me.gridupload.Item(7, i).Value, Me.gridupload.Item(8, i).Value, _
                               Me.gridupload.Item(9, i).Value, Me.gridupload.Item(10, i).Value)
                Next

                .Close()
            End With
        Catch ex As Exception
            frmStartup.Close()
            MessageBox.Show(ex.Message)
            MessageBox.Show("Please check your excel file data." + _
            vbNewLine + "Possibly some data are missing or improper template used." + _
            vbNewLine + "Cannot proceed. Please validate and re-upload again.", "Upload Payroll", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub ValidateExcelData()
        Dim i As Integer
        For i = 0 To Me.gridupload.Rows.Count - 1
            Try
                Call TryExcelUpload(Me.gridupload.Item(0, i).Value, Me.gridupload.Item(1, i).Value, Me.gridupload.Item(2, i).Value, _
                                    Me.gridupload.Item(3, i).Value, Me.gridupload.Item(4, i).Value, Me.gridupload.Item(5, i).Value, _
                                    Me.gridupload.Item(6, i).Value, Me.gridupload.Item(7, i).Value, Me.gridupload.Item(8, i).Value, _
                                    Me.gridupload.Item(9, i).Value, Me.gridupload.Item(10, i).Value)
            Catch ex As Exception
                Throw ex
                Exit For
            End Try

        Next
    End Sub

    Private Sub TryExcelUpload(ByVal empno As String, ByVal fullname As String, ByVal PYAreaCode As String, _
                                     ByVal PYArea As String, ByVal forPeriod1 As String, ByVal PaymentDate As Date, _
                                     ByVal WT As String, ByVal WageTypeLongText As String, ByVal Numberof As String, _
                                     ByVal Amount As Decimal, ByVal Currency As String)

    End Sub

    Private Sub InsertUploadedFile(ByVal empno As String, ByVal fullname As String, ByVal PYAreaCode As String, _
                                    ByVal PYArea As String, ByVal forPeriod1 As String, ByVal PaymentDate As Date, _
                                    ByVal WT As String, ByVal WageTypeLongText As String, ByVal Numberof As String, _
                                    ByVal Amount As Decimal, ByVal Currency As String)
        Dim mycon As New Clsappconfiguration

        PayrollCodeUploadtab(cbopaycodeupload.Text)

        Try
            SqlHelper.ExecuteNonQuery(mycon.cnstring, CommandType.StoredProcedure, "CIMS_Upload_Payroll", _
                                     New SqlParameter("@employeeNo", empno), _
                                     New SqlParameter("@fcName", fullname), _
                                     New SqlParameter("@fcPYAreaCode", PYAreaCode), _
                                     New SqlParameter("@fcPYArea", PYArea), _
                                     New SqlParameter("@fcforPeriod1", forPeriod1), _
                                     New SqlParameter("@fdPaymentDate", PaymentDate), _
                                     New SqlParameter("@fcReturnCode", WT), _
                                     New SqlParameter("@fcReturnDesc", WageTypeLongText), _
                                     New SqlParameter("@fcNumber", Numberof), _
                                     New SqlParameter("@fdAmount", Amount), _
                                     New SqlParameter("@fcCurrency", Currency), _
                                     New SqlParameter("@fk_PayrollCode", payrollcode))
            btnValidate.Enabled = True
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Add File", MessageBoxButtons.OK)
        End Try
    End Sub

    Private Sub RefreshUploadForm()
        btnProcess_Upload.Enabled = False
        btnValidate.Enabled = False

        Call RemovedataonGrid()
        Call DeleteUploadTemporaryTable()
    End Sub

    Private Sub ValidateUploadedPayroll(ByVal payrollcode As String, ByVal paydate As Date)
        Try
            If AreMembersUploadedExistInDatabase(payrollcode, paydate) Then
                ' 1. Checks whether member in payroll file is grouped in the proper payroll code
                Call ValidateMemberIfInPayrollGroup(payrollcode, paydate)

                '2. Checks whether the payroll date in the payroll file is present in the downloaded deduction details
                Call ValidatePaydate(paydate)

                '3. Checks whether the payroll file contains transcode not present in the downloaded deduction details
                Call ValidateTranscode(payrollcode, paydate)
            End If
        Catch ex As Exception
            Call DeleteUploadTemporaryTable()
            gIsPayrollCodeValid = False

            MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub ValidatePaydate(ByVal paydate As Date)
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "CIMS_Upload_Validation_PayDate", _
                New SqlParameter("@payDate", paydate))

                If rd.Read Then
                    gIsPaydateValid = False
                Else
                    gIsPaydateValid = True
                End If

            End Using
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ValidateTranscode(ByVal payrollCode As String, ByVal paydate As Date)
        Try
            gTranscode = GetTranscodeNotInDownloadDetails(payrollCode, paydate)
            If gTranscode = "" Then
                gIsTranscodeValid = True
            Else
                gIsTranscodeValid = False
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ValidateMemberIfInPayrollGroup(ByVal payrollcode As String, ByVal paydate As Date)
        Dim gCon As New Clsappconfiguration
        Dim ds As New DataSet("ExcelImport")

        Try
            ds = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.StoredProcedure, "CIMS_Upload_PayrollCode_Validate", _
                                         New SqlParameter("@payrollCode", payrollcode), _
                                         New SqlParameter("@payDate", paydate))

            If ds.Tables(0).Rows.Count = 0 Then
                gIsPayrollCodeValid = True

            Else
                gIsPayrollCodeValid = False
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function GetTranscodeNotInDownloadDetails(ByVal payroll As String, ByVal paydate As Date) As String
        Dim transcode As String = ""
        Dim mycon As New Clsappconfiguration

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(mycon.cnstring, CommandType.StoredProcedure, "CIMS_Upload_Validation_Transcode", _
                                            New SqlParameter("@fk_PRollCode", payroll), _
                                            New SqlParameter("@payDate", paydate))

                Dim count As Int16 = 1
                While rd.Read
                    If count = 1 Then
                        transcode = rd.Item(0).ToString
                    Else
                        transcode += ", " + rd.Item(0).ToString
                    End If
                End While
            End Using
        Catch ex As Exception
            Throw ex
        End Try

        Return transcode
    End Function

    Private Shared Sub DisplayMembersNotInPayrollCode()
        frmPayrollCodeValidation.ShowDialog()
    End Sub

    Private Shared Sub ExportNonExistingMembersToExcel(ByVal ds As DataSet)
        Dim excelImport As New clsDatasetToExcel()
        clsDatasetToExcel.SaveXmlSchema(ds)
        excelImport.BuildXMLMap(ds)
    End Sub

    Private Sub CreateExcelOutputFile()
        Try
            Dim rowcount As Int32 = gridDownload.Rows.Count - 1
            If rowcount > -1 Then
                Dim fieldname() As String = {"Employee ID", "Members", "TranCode", "Total"}
                Dim dataarray(rowcount, 4) As Object
                Dim colcount As Int32 = 0

                For rowcounter As Int32 = 0 To rowcount
                    For Each cell As DataGridViewCell In gridDownload.Rows(rowcount - rowcounter).Cells
                        dataarray(rowcounter, colcount) = cell.Value
                        colcount = colcount + 1
                    Next
                    colcount = 0
                Next    

                'var for excel
                Dim xlapp As New Excel.Application
                Dim workbook As Excel.Workbook = xlapp.Workbooks.Add(Excel.XlWBATemplate.xlWBATWorksheet)
                Dim worksheet As Excel.Worksheet = CType(workbook.Worksheets(1), Excel.Worksheet)
                Dim xlcalc As Excel.XlCalculation
                'save setting
                With xlapp
                    xlcalc = .Calculation
                    .Calculation = Excel.XlCalculation.xlCalculationManual
                End With
                'write the field names and data to the targeting worksheet
                With worksheet
                    .Cells(1, 1).Value = "Date:"
                    .Range(.Cells(1, 2), .Cells(1, 4)).Merge()

                    .Cells(1, 2).Value = getIsPaydate().ToString("MMddyy")

                    .Range(.Cells(3, 1), .Cells(3, 4)).Value = fieldname
                    .Range(.Cells(4, 1), .Cells(rowcount + 4, 4)).Value = dataarray
                End With

                workbook.SaveAs(SavDiag.FileName)
                With xlapp
                    .Visible = True
                    .UserControl = True
                    .Calculation = xlcalc

                End With
                'tanggal to memory
                worksheet = Nothing
                workbook = Nothing
                xlapp = Nothing
                GC.Collect()

                lblLoading.Visible = True
                lstVwDownloadList.Enabled = False
                bgDownload.RunWorkerAsync()
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error!")
        End Try
    End Sub

    Private Function AreMembersUploadedExistInDatabase(ByVal payrollcode As String, ByVal paydate As Date) As Boolean
        Dim gcon As New Clsappconfiguration
        Dim nonExistingMembersData As New DataSet
        Dim IsValid As Boolean = True

        Try
            nonExistingMembersData = SqlHelper.ExecuteDataset(gcon.cnstring, CommandType.StoredProcedure, "CIMS_Upload_ValidateIfMemberExist", _
                    New SqlParameter("@payrollCode", payrollcode), _
                    New SqlParameter("@payDate", paydate))

            If nonExistingMembersData.Tables.Count <> 0 Then

                Call ExportNonExistingMembersToExcel(nonExistingMembersData)

                gAreMembersExisting = False
                Return False
            Else
                gAreMembersExisting = True
                Return True
            End If

        Catch ex As Exception
            Throw ex

            gAreMembersExisting = False
            Return False
        End Try

    End Function

    Private Shared Sub PayrollintegUploadHistory()
        Dim mycon As New Clsappconfiguration
        Try
            SqlHelper.ExecuteNonQuery(mycon.cnstring, CommandType.StoredProcedure, "CIMS_Upload_PrepareValidatedUpload", _
                New SqlParameter("@user", frmLogin.Username.Text))

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Payroll Integrator Upload History", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub PostToHistory(ByVal paycode As String, ByVal isloanincluded As Boolean, ByVal isContributionIncluded As Boolean, _
                                ByVal isBereavementIncluded As Boolean, ByVal bereavementAmount As Decimal, ByVal payDate As Date)
        Dim mycon As New Clsappconfiguration

        Try
            Dim user As String = frmLogin.Username.ToString()
            ModifiedSqlHelper.ExecuteNonQuery(mycon.cnstring, CommandType.StoredProcedure, "CIMS_Download_Details_PostToHistory_Analog", _
                                      New SqlParameter("@paycode", paycode), _
                                      New SqlParameter("@isLoanIncluded", isloanincluded), _
                                      New SqlParameter("@isContributionIncluded", isContributionIncluded), _
                                      New SqlParameter("@isBereavementIncluded", isBereavementIncluded), _
                                      New SqlParameter("@bereavementAmount", bereavementAmount), _
                                      New SqlParameter("@payDate", payDate), _
                                      New SqlParameter("@user", user))
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Insert failed", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try

    End Sub

    Private Sub DeleteUploadTempTable()
        Dim mycon As New Clsappconfiguration

        Try
            SqlHelper.ExecuteNonQuery(mycon.cnstring, CommandType.StoredProcedure, "CIMS_Upload_TemporaryTable_Delete")
        Catch ex As Exception
            MessageBox.Show("DeleteTempTable", "frmPayrollCodeValidation", MessageBoxButtons.OK)
        End Try
    End Sub

    Private Sub ClearUploadGrid()
        Dim rows As Integer = gridupload.Rows.Count
        Dim i As Integer
        For i = 0 To rows - 1
            Me.gridupload.Rows.Remove(gridupload.CurrentRow)
        Next
    End Sub

    Private Sub InitializedForm()
        Call CSCodelabel()
        Call BCodelabel()
        Call LoadPayrollCode()
        Call PayrollUploadtab()
        Call MappingLoad()
    End Sub

    Private Sub DisableButtons()
        btnCreateFile.Enabled = False
        txtBrevAmt.Enabled = False
        btnProcess_Upload.Enabled = False
        btnprocess_Download.Enabled = False
    End Sub

    Private Sub CloseIt()
        Me.Close()
    End Sub

    Private Shared Sub ShowProgress()
        Call DisplayMarqueeProgressBar("Backing up Database... Please wait, this may take several minutes.")
    End Sub

    Private Shared Function GetFilePath() As String
        Dim directory As String = ""
        Using FolderBrowserDialog As New FolderBrowserDialog()
            With FolderBrowserDialog
                .RootFolder = Environment.SpecialFolder.Desktop
                .Description = "Select the folder where you will store your backup."
                If .ShowDialog() = Windows.Forms.DialogResult.OK Then
                    directory = .SelectedPath() + "/"
                End If
            End With
        End Using

        Return directory
    End Function

    Private Sub RemovedataonGrid()

        Dim rows As Integer = Me.gridupload.Rows.Count
        Dim i As Integer
        If rows > 0 Then
            For i = 0 To Me.gridupload.Rows.Count - 1
                Me.gridupload.Rows.Remove(Me.gridupload.CurrentRow)
            Next
        End If
    End Sub

    Private Sub DeleteUploadTemporaryTable()
        Dim mycon As New Clsappconfiguration

        Try
            SqlHelper.ExecuteNonQuery(mycon.cnstring, CommandType.StoredProcedure, "CIMS_Upload_TemporaryTable_Delete")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region

#Region "Events"
    Private Sub PayrollUploadtab()
        Dim mycon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "CIMS_m_Member_PayrollCode_Select")
        Try
            While rd.Read
                Me.cbopaycodeupload.Items.Add(rd.Item("PRollDesc"))
                'getpayrollcode()
            End While
            rd.Close()
            mycon.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show("PayrollUploadtab", "Payroll Upload", MessageBoxButtons.OK)
        End Try
    End Sub

    Private Sub PayrollCodeUploadtab(ByVal payrolldesc As String)
        Dim mycon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "CIMS_m_Member_PayrollCode_Param", _
                                     New SqlParameter("PRollDesc", payrolldesc))
        Try
            While rd.Read
                getpayrollcode() = rd.Item("pk_PRollCode")
            End While
            rd.Close()
            mycon.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show("PayrollCodeUploadtab", "Payroll Upload", MessageBoxButtons.OK)
        End Try
    End Sub

    Private Sub SelectSpecificLoanType()
        Dim i As Integer
        Dim isLoanTypeSelected As Boolean
        Dim payrollID As String
        Dim payrollName As String

        For i = 0 To Me.gridLoans.Rows.Count - 1
            isLoanTypeSelected = Me.gridLoans.Item(1, i).Value
            payrollID = Me.gridLoans.Item(2, i).Value
            payrollName = Me.gridLoans.Item(3, i).Value

            If isLoanTypeSelected = True Then
                Call InsertMappingTemptable(payrollID, payrollName)
            Else
                Call DeleteLoanTypeSelected(payrollID, payrollName)
            End If
        Next
    End Sub

    Private Sub DeleteLoanTypeSelected(ByVal payrollId As String, ByVal payrollName As String)
        Dim mycon As New Clsappconfiguration
        Try
            SqlHelper.ExecuteNonQuery(mycon.cnstring, CommandType.StoredProcedure, "CIMS_Download_TempTable_Delete", _
                New SqlParameter("@fcPayrollId", payrollId), _
                New SqlParameter("@fcPaytrollName", payrollName))
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Delete failed", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try

    End Sub

    Private Sub sendtoexcelselectedrows()
        Try
            'Dim rowcount As Int32 = Me.gridDownload.SelectedRows.Count - 1
            Dim rowcount As Int32 = Me.gridDownload.Rows.Count - 1
            If rowcount > -1 Then
                Dim fieldname() As String = {"Employee ID", "Members", "TranCode", "Total"}
                Dim dataarray(rowcount, 4) As Object
                Dim colcount As Int32 = 0

                For rowcounter As Int32 = 0 To rowcount
                    For Each cell As DataGridViewCell In Me.gridDownload.Rows(rowcount - rowcounter).Cells
                        dataarray(rowcounter, colcount) = cell.Value
                        colcount = colcount + 1
                    Next
                    colcount = 0
                Next

                'var for excel
                Dim xlapp As New Excel.Application
                Dim workbook As Excel.Workbook = xlapp.Workbooks.Add(Excel.XlWBATemplate.xlWBATWorksheet)
                Dim worksheet As Excel.Worksheet = CType(workbook.Worksheets(1), Excel.Worksheet)
                Dim xlcalc As Excel.XlCalculation
                'save setting
                With xlapp
                    xlcalc = .Calculation
                    .Calculation = Excel.XlCalculation.xlCalculationManual
                End With
                'write the field names and data to the targeting worksheet
                With worksheet
                    .Range(.Cells(1, 1), .Cells(1, 4)).Value = fieldname
                    .Range(.Cells(2, 1), .Cells(rowcount + 2, 4)).Value = dataarray

                End With

                With xlapp
                    .Visible = True
                    .UserControl = True
                    .Calculation = xlcalc
                End With
                'tanggal to memory
                worksheet = Nothing
                workbook = Nothing
                xlapp = Nothing
                GC.Collect()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.ToString)
        End Try
    End Sub

    Private Sub frmDownloadUploadExcel_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim messageboxResult As DialogResult = MessageBox.Show("""An ounce of prevention is better than a pound of cure.""" _
                                    + vbNewLine + "System recommends that you backup your database" _
                                    + vbNewLine + "before proceeding. Do you want to backup your database?", "Friendly Reminder", MessageBoxButtons.YesNoCancel, _
                                    MessageBoxIcon.Question)

        If messageboxResult = Windows.Forms.DialogResult.Yes Then
            Try
                gFilePath = GetFilePath()
                If gFilePath <> "" Then
                    'Shows Marquee Progress bar
                    Me.BeginInvoke(New MethodInvoker(AddressOf ShowProgress))

                    'backup database (Background Task)
                    bgwDatabaseFunctions.RunWorkerAsync()
                Else
                    InitializedForm()
                    DisableButtons()
                End If

            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try

        ElseIf messageboxResult = Windows.Forms.DialogResult.No Then
            InitializedForm()
            DisableButtons()
            lblLoading.Visible = True
            bgDownload.RunWorkerAsync()
        Else
            Me.BeginInvoke(New MethodInvoker(AddressOf CloseIt))
        End If

    End Sub

    Private Sub chkbereavement_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkbereavement.CheckedChanged
        If Me.chkbereavement.Checked = True Then
            Me.txtBrevAmt.Enabled = True
            getisbereavement() = True
        Else
            Me.txtBrevAmt.Enabled = False
            getisbereavement() = False
        End If
    End Sub

    Private Sub txtBrevAmt_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtBrevAmt.KeyPress
        e.Handled = Not Numeric(e.KeyChar)
    End Sub

    Private Sub txtAmount_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.Handled = Not Numeric(e.KeyChar)
    End Sub

    Private Sub txtBrevAmt_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtBrevAmt.TextChanged
        If Me.txtBrevAmt.Text = "" Then
            Me.txtBrevAmt.Text = "0.00"

        End If
    End Sub

    Private Sub chkloans_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkloans.CheckedChanged
        If Me.chkloans.Checked = True Then
            getisloan() = True
            gridLoans.Visible = True
        Else
            getisloan() = False
            gridLoans.Visible = False
        End If
    End Sub

    Private Sub chkContribution_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkContribution.CheckedChanged
        If Me.chkContribution.Checked = True Then
            getiscontribution() = True
        Else
            getiscontribution() = False
        End If
    End Sub

    Private Sub btnviewdetails_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnviewdetails.Click
        If Me.cboPaycode.Text <> "" Then
            If Date.TryParse(Me.cboPaydate.Text, Date.Now()) Then
                getIsDecimal() = Me.txtBrevAmt.Text
                getIsPaydate() = Me.cboPaydate.Text

                Call DisplayUnsettledPayments()

                If HasUnsettledPayments() = False Then
                    Call DisplayDeductionDetails()
                    frmUnsettledLoanPayments.Close()
                End If
            Else
                MessageBox.Show("The date you entered is invalid.", "User Error: Date Invalid", MessageBoxButtons.OK, MessageBoxIcon.Error)
                cboPaycode.Focus()
            End If
        Else

            MessageBox.Show("Select Pay Code and Pay Date", "Select", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cboPaydate.Focus()
        End If
    End Sub

    Private Sub btnprocess_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnprocess_Download.Click
        If Date.TryParse(Me.cboPaydate.Text, Date.Now()) Then
            If Me.cboPaycode.Text <> "" And cboPaydate.Text <> "" Then
                getIsDecimal() = Me.txtBrevAmt.Text
                getIsPaydate() = Me.cboPaydate.Text

                Call DisplayMarqueeProgressBar("Processing...")

                If bgwDeductionProcessed.IsBusy = False Then
                    bgwDeductionProcessed.RunWorkerAsync()
                Else
                    MessageBox.Show("Server is Busy. Please Try again.", "Server Busy", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If

            Else
                MessageBox.Show("Select Pay Code and Pay Date", "Select", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Else
            MessageBox.Show("The date you entered is invalid.", "User Error: Date Invalid", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub

    Private Sub btnCreateFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCreateFile.Click

        If MessageBox.Show("Create payroll file?", "Payroll Integrator", MessageBoxButtons.YesNo, MessageBoxIcon.Information) = Windows.Forms.DialogResult.Yes Then
            Try
                Dim paydate As Date = cboPaydate.Text
                SavDiag.FileName = "PayrollDownload_" & paydate.ToString("MMddyy")
                SavDiag.ShowDialog()
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub

    Private Sub btnclosed_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclosed.Click
        Me.Close()
    End Sub

    Private Sub btnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse.Click
        With OpenFileDialog1
            Using openFileDialog1 As New OpenFileDialog()
                .InitialDirectory = "c:\"
                .Filter = "Excel Files Only (*.xls)|*.xls"
                .FilterIndex = 2
                .RestoreDirectory = True
                If .ShowDialog = Windows.Forms.DialogResult.OK Then
                    gStrpath = .FileName
                    filename = IO.Path.GetFileName(gStrpath)
                    txtfile.Text = LTrim(gStrpath)
                End If
            End Using
        End With
    End Sub

    Public Sub GetConnection(ByVal FilePath As String)
        'check if the file exists
        Try
            If Not File.Exists(FilePath) Then
                MessageBox.Show("File does not exist or access is denied!", "Upload", MessageBoxButtons.OK)
            Else
                'connection string
                gCnstring = String.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=""Excel 8.0;HDR=Yes;IMEX=1""", FilePath)
                gOleConn = New OleDbConnection(gCnstring)
                gOleCmd = gOleConn.CreateCommand
                gOleConn.Open()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnUpload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        If payrollcode <> "" Then
            Call RemovedataonGrid()
            Call DeleteUploadTemporaryTable()
            Call UploadExcelFile()
            Call getGridDate()
            Call LoopInsertUploadedFile()
        Else
            MessageBox.Show("Payroll Code or Paydate is empty.", "Select Payroll Code and Paydate", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If
    End Sub

    Private Sub btnpreprocess_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnValidate.Click
        If payrollcode <> "" And Me.txtpaydate.Text <> "" Then
            DisplayMarqueeProgressBar("Validating...")
            bgwValidate.RunWorkerAsync()
        Else
            MessageBox.Show("Payroll Code or Paydate is empty.", "Select Payroll Code and Paydate", MessageBoxButtons.OK)
        End If
    End Sub

    Private Sub cbopaycodeupload_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbopaycodeupload.TextChanged
        Call PayrollCodeUploadtab(cbopaycodeupload.Text.Trim)
    End Sub

    Private Sub btnProcessUpload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProcess_Upload.Click
        Call PayrollintegUploadHistory()
        Call RefreshUploadForm()

        frmPayrollIntegrationResult.MdiParent = frmMain
        frmPayrollIntegrationResult.Show()
    End Sub

    Private Sub btnDeletePayroll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeletePayroll.Click
        frmDownloadedPayroll_Delete.ShowDialog()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Call RemovedataonGrid()
        Call DeleteUploadTemporaryTable()

        Dispose()
        Close()
    End Sub

    Private Sub frmDownloadUploadExcel_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        Call RemovedataonGrid()
        Call DeleteUploadTemporaryTable()

        Dispose()
    End Sub

    Private Shared Sub DisplayMarqueeProgressBar(ByVal title As String)
        With frmStartup
            .MdiParent = frmMain
            .Show()
            .processWait.Style = ProgressBarStyle.Marquee
            .processWait.MarqueeAnimationSpeed = 5
            .lblTitle.Text = title
            .lblProcessStatus.Text = ""
            .lblProgress.Text = ""
        End With
    End Sub

    Private Sub bgwUnsettled_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgwUnsettled.DoWork
        If PostDownloadDetails(getsfkpaycode(), getisloan(), getiscontribution(), getisbereavement(), getIsDecimal(), getIsPaydate()) = True Then
            Call CreateExcelOutputFile()
        End If
    End Sub

    Private Sub bgwUnsettled_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwUnsettled.RunWorkerCompleted
        frmStartup.Close()
    End Sub
#End Region

#Region "Background Tasks"
    Private Sub bgwValidate_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgwValidate.DoWork
        Call ValidateUploadedPayroll(payrollcode, Me.txtpaydate.Text)
    End Sub
    Private Sub bgwValidate_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwValidate.RunWorkerCompleted
        frmStartup.Close()

        If gAreMembersExisting Then
            If gIsPayrollCodeValid Then
                If gIsPaydateValid Then
                    If gIsTranscodeValid Then
                        MessageBox.Show(String.Format("    Validation Successful!{0}   You can now proceed.", vbNewLine), "Validation", _
                                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Me.btnProcess_Upload.Enabled = True
                    Else
                        MessageBox.Show(String.Format("User Error! {0} transcode is not included{1}on download Details or Remove {0} details on Payroll file.", gTranscode, vbNewLine), _
                                                "User Error! Transcode Validation", MessageBoxButtons.OK, MessageBoxIcon.Warning)

                        btnProcess_Upload.Enabled = False
                        Call DeleteUploadTemporaryTable()
                        Call ClearUploadGrid()
                    End If
                Else
                    MessageBox.Show("User Error! Paydate in payroll file not included in the deduction details. Please check your payroll file.", "User Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)

                    btnProcess_Upload.Enabled = False
                    Call DeleteUploadTemporaryTable()
                    Call ClearUploadGrid()
                End If
            Else
                MessageBox.Show("Some members in the payroll file does not belong to this payroll code.", "Payroll Code Validation", MessageBoxButtons.OK, MessageBoxIcon.Information)
                ' This will display Members present in the payroll file that does not belong the payroll code in the coop system
                Call DisplayMembersNotInPayrollCode()

                btnValidate.Enabled = False
                Call DeleteUploadTemporaryTable()
                Call ClearUploadGrid()
            End If
        Else
            MessageBox.Show("Some members do not exist in the database.", "Member Validation", MessageBoxButtons.OK, MessageBoxIcon.Information)

            btnValidate.Enabled = False
            Call DeleteUploadTemporaryTable()
            Call ClearUploadGrid()
        End If

    End Sub

    Private Sub bgwDeductionDetails_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgwDeductionDetails.DoWork
        Call LoadDeductionDetails(fkpaycode, isloan, iscontribution, isbereavement, getIsDecimal(), getIsPaydate())
    End Sub
    Private Sub bgwDeductionDetails_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwDeductionDetails.RunWorkerCompleted
        With gridDownload
            .DataSource = gDeductionDetails.Tables(0)

            .Columns("members").Width = 150
            .Columns("trancode").Width = 130

            .Columns("balance").Visible = False
            .Columns("fcStatus").Visible = False
            .Columns("paydate").Visible = False
            .Columns("status").Visible = False
            .Columns("fbPayrollInclusion").Visible = False


            .Columns("total").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .Columns("loanNo").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns("trancode").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns("empNo").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

            .Columns("empNo").HeaderText = "Emp No."
            .Columns("trancode").HeaderText = "Code"
            .Columns("loanNo").HeaderText = "Loan No."
            .Columns("total").HeaderText = "Total"
            .Columns("fcType").HeaderText = "Type"
            .Columns("members").HeaderText = "Members"

            .Columns("total").DefaultCellStyle.Format = "n2"
        End With

        btnCreateFile.Enabled = False
        btnprocess_Download.Enabled = True
        frmStartup.Close()
    End Sub

    Private Sub bgwDeductionProcessed_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgwDeductionProcessed.DoWork
        Try
            ProcessDownload(fkpaycode, isloan, iscontribution, isbereavement, getIsDecimal(), getIsPaydate())
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub bgwDeductionProcessed_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwDeductionProcessed.RunWorkerCompleted
        BindProcessedPayrollToGrid()

        btnCreateFile.Enabled = True
        frmStartup.Close()
    End Sub

    Private Sub bgwDatabaseFunctions_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgwDatabaseFunctions.DoWork
        Dim database As New clsDatabaseFunctions()
        database.BackupDatabase("Coop_Membership_Test", gFilePath, frmMain.username)
        database.BackupDatabase("Coop_Accounting", gFilePath, frmMain.username)
    End Sub
    Private Sub bgwDatabaseFunctions_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwDatabaseFunctions.RunWorkerCompleted
        InitializedForm()
        DisableButtons()

        'Close progress bar
        frmStartup.Close()
    End Sub
    Public Sub BindProcessedPayrollToGrid()
        With gridDownload
            .DataSource = gDeductionProcessed.Tables(0)

            .Columns(1).Width = 150
            .Columns(2).Width = 130
        End With
    End Sub
#End Region

    Private Sub gridloans_CurrentCellDirtyStateChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles gridLoans.CurrentCellDirtyStateChanged
        If gridLoans.IsCurrentCellDirty Then
            gridLoans.CommitEdit(DataGridViewDataErrorContexts.Commit)
        End If
    End Sub

    Private Sub gridloans_CellValueChanged(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles gridLoans.CellValueChanged
        Call SelectSpecificLoanType()
    End Sub

    Private Sub btnReverse_Click(sender As System.Object, e As System.EventArgs) Handles btnReverse.Click
        'use CIMS_Upload_ReverseProcessedPayroll
    End Sub

    Private Sub bgDownload_DoWork(sender As System.Object, e As System.ComponentModel.DoWorkEventArgs) Handles bgDownload.DoWork
        LoadDownloadList()
    End Sub

    Private Sub bgDownload_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgDownload.RunWorkerCompleted
        lblLoading.Visible = False
        lstVwDownloadList.Enabled = True
    End Sub

    Private Sub cboPaycode_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cboPaycode.SelectedIndexChanged
        Call PayrollCodeParam(Me.cboPaycode.Text)
        Call PaydateSchedule(fkpaycode)
    End Sub

    Private Sub SavDiag_FileOk(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles SavDiag.FileOk
        Call DisplayMarqueeProgressBar("Transferring to Datasheet...")
        Call bgwUnsettled.RunWorkerAsync()
    End Sub

    Private Sub TabPage1_Click(sender As System.Object, e As System.EventArgs) Handles TabPage1.Click

    End Sub

    Private Sub lstVwDownloadList_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles lstVwDownloadList.SelectedIndexChanged

    End Sub
End Class