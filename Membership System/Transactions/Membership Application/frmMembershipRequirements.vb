Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmMembershipRequirements

#Region "Variables"
    Private pkRequire As String
#End Region

#Region "Property"
    Private Property getpkRequire() As String
        Get
            Return pkRequire
        End Get
        Set(ByVal value As String)
            pkRequire = value
        End Set
    End Property
#End Region

#Region "View Membership Requirements"
    Private Sub ViewMembershipRequirement()
        Dim mycon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "CIMS_CIMS_m_Applicant_Requirements_LoadAll")
        With Me.listMembershipRequirements_Rqts
            .Clear()
            .View = View.Details
            .GridLines = True
            .FullRowSelect = True
            .Columns.Add("Membership Requirements", 300, HorizontalAlignment.Left)
            Try
                While rd.Read
                    With .Items.Add(rd.Item(0))
                        .SubItems.Add(rd.Item(1))
                    End With
                End While
                rd.Close()
            Catch ex As Exception
                MessageBox.Show(ex.Message, "ViewMembershipRequirement")
            End Try
        End With
    End Sub
#End Region

    Private Sub frmMembershipRequirements_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Call ViewMembershipRequirement()

    End Sub

#Region "Insert Update Membership Requirements"
    Private Sub InsertUpdateMembershipRequirements(ByVal requirement As String, ByVal pkRqts As String)
        Dim mycon As New Clsappconfiguration
        mycon.sqlconn.Open()
        Dim trans As SqlTransaction = mycon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Masterfiles_Applicants_Reqts_AddEdit", _
                                      New SqlParameter("@fcMembershipReqtsDesc", requirement), _
                                      New SqlParameter("@pk_MembershipRqts", pkRqts))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "InsertUpdateMembershipRequirements")
        Finally
            mycon.sqlconn.Close()
        End Try
    End Sub
#End Region

#Region "Delete Membership Requirements"
    Private Sub DeleteMembershipRequirements(ByVal pkrequire As String)
        Dim mycon As New Clsappconfiguration
        mycon.sqlconn.Open()
        Dim trans As SqlTransaction = mycon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Masterfiles_Applicant_Requirements_Delete", _
                                      New SqlParameter("@pk_MembershipReqts", pkrequire))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show("You cannot delete this requirement that is already tag on the member requirements", "DeleteMembershipRequirements")
        Finally
            mycon.sqlconn.Close()
        End Try
    End Sub
#End Region

    Private Sub btnSave_rqts_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave_rqts.Click
        Try
            If btnSave_rqts.Text = "New" Then

                Label1.Text = "Add Membership Requirements"
                btnDelete_rqts.Visible = False
                btnUpdate_rqts.Visible = False
                btnSave_rqts.Text = "Save"
                btnSave_rqts.Image = My.Resources.save2
                btnClose_rqts.Text = "Cancel"
                btnClose_rqts.Location = New System.Drawing.Point(74, 15)

            ElseIf btnSave_rqts.Text = "Save" Then
                If Me.txtdescription_Rqts.Text <> "" Then
                    Label1.Text = "Membership Requirements"
                    Call InsertUpdateMembershipRequirements(Me.txtdescription_Rqts.Text, "")
                    Call ViewMembershipRequirement()
                    btnSave_rqts.Text = "New"
                    btnSave_rqts.Image = My.Resources.new3
                    btnClose_rqts.Text = "Close"
                    btnClose_rqts.Location = New System.Drawing.Point(218, 15)
                    btnDelete_rqts.Visible = True
                    btnUpdate_rqts.Visible = True
                Else
                    MessageBox.Show("Empty field found! ", "Save", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            End If

            Call ViewMembershipRequirement()
        Catch ex As Exception
        End Try
    End Sub
    Private Sub btnClose_rqts_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose_rqts.Click
        If btnClose_rqts.Text = "Cancel" Then
            Label1.Text = "Membership Requirements"
            btnSave_rqts.Text = "New"
            btnSave_rqts.Image = My.Resources.new3
            btnUpdate_rqts.Text = "Edit"
            btnUpdate_rqts.Image = My.Resources.edit1
            btnClose_rqts.Text = "Close"
            btnClose_rqts.Location = New System.Drawing.Point(218, 15)
            btnDelete_rqts.Visible = True
            btnUpdate_rqts.Visible = True
            btnSave_rqts.Enabled = True
        ElseIf btnClose_rqts.Text = "Close" Then
            Me.Close()
        End If
    End Sub
    Private Sub btnDelete_rqts_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete_rqts.Click
        Try
            getpkRequire() = Me.listMembershipRequirements_Rqts.SelectedItems(0).SubItems(1).Text
            Dim x As New DialogResult
            x = MessageBox.Show("Are you sure you want to permanently delete this record?", "Confirm Deletion", MessageBoxButtons.OKCancel, MessageBoxIcon.Information)
            If x = System.Windows.Forms.DialogResult.OK Then
                Call DeleteMembershipRequirements(pkRequire)
            ElseIf x = System.Windows.Forms.DialogResult.Cancel Then
            End If

            Call ViewMembershipRequirement()
        Catch ex As Exception
        End Try

    End Sub
    Private Sub btnUpdate_rqts_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate_rqts.Click
        Try
            If btnUpdate_rqts.Text = "Edit" Then
                Label1.Text = "Edit Membership Requirements"
                Me.txtdescription_Rqts.Text = Me.listMembershipRequirements_Rqts.SelectedItems(0).Text
                getpkRequire() = Me.listMembershipRequirements_Rqts.SelectedItems(0).SubItems(1).Text
                Me.btnSave_rqts.Enabled = False
                btnUpdate_rqts.Text = "Update"
                btnUpdate_rqts.Image = My.Resources.save2
                btnClose_rqts.Text = "Cancel"
                btnClose_rqts.Location = New System.Drawing.Point(141, 15)
                btnDelete_rqts.Visible = False
                btnClose_rqts.Visible = True

            ElseIf btnUpdate_rqts.Text = "Update" Then
                If Me.txtdescription_Rqts.Text <> "" Then
                    Label1.Text = "Membership Requirements"
                    Call InsertUpdateMembershipRequirements(Me.txtdescription_Rqts.Text, pkRequire)
                    Call ViewMembershipRequirement()
                    btnDelete_rqts.Visible = True
                    btnUpdate_rqts.Text = "Edit"
                    btnUpdate_rqts.Image = My.Resources.edit1
                    btnClose_rqts.Text = "Cancel"
                    btnClose_rqts.Location = New System.Drawing.Point(218, 15)
                    btnClose_rqts.Text = "Close"
                    btnClose_rqts.Visible = True
                    Me.btnSave_rqts.Enabled = True
                    Me.btnDelete_rqts.Visible = True

                Else
                    MessageBox.Show("Empty field found! ", "Edit", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            End If

            Call ViewMembershipRequirement()
        Catch ex As Exception
        End Try
    End Sub
End Class