<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmLoanMasterUploaderProcess
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ProgressBar = New System.Windows.Forms.ProgressBar()
        Me.LblProcess = New System.Windows.Forms.Label()
        Me.LblCounter = New System.Windows.Forms.Label()
        Me.Timer = New System.Windows.Forms.Timer(Me.components)
        Me.BGWorker = New System.ComponentModel.BackgroundWorker()
        Me.SuspendLayout()
        '
        'ProgressBar
        '
        Me.ProgressBar.Location = New System.Drawing.Point(5, 5)
        Me.ProgressBar.Name = "ProgressBar"
        Me.ProgressBar.Size = New System.Drawing.Size(446, 13)
        Me.ProgressBar.TabIndex = 0
        '
        'LblProcess
        '
        Me.LblProcess.Location = New System.Drawing.Point(2, 21)
        Me.LblProcess.Name = "LblProcess"
        Me.LblProcess.Size = New System.Drawing.Size(208, 13)
        Me.LblProcess.TabIndex = 1
        Me.LblProcess.Text = "[Process]"
        '
        'LblCounter
        '
        Me.LblCounter.Location = New System.Drawing.Point(248, 20)
        Me.LblCounter.Name = "LblCounter"
        Me.LblCounter.Size = New System.Drawing.Size(203, 17)
        Me.LblCounter.TabIndex = 2
        Me.LblCounter.Text = "[Current No] of [Total No]"
        Me.LblCounter.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Timer
        '
        Me.Timer.Interval = 1
        '
        'BGWorker
        '
        '
        'FrmLoanMasterUploaderProcess
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(456, 39)
        Me.ControlBox = False
        Me.Controls.Add(Me.LblCounter)
        Me.Controls.Add(Me.LblProcess)
        Me.Controls.Add(Me.ProgressBar)
        Me.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "FrmLoanMasterUploaderProcess"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Processing . . ."
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ProgressBar As System.Windows.Forms.ProgressBar
    Friend WithEvents LblProcess As System.Windows.Forms.Label
    Friend WithEvents LblCounter As System.Windows.Forms.Label
    Friend WithEvents Timer As System.Windows.Forms.Timer
    Friend WithEvents BGWorker As System.ComponentModel.BackgroundWorker
End Class
