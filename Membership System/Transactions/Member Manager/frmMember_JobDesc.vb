﻿Imports System.Data.Sql
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmMember_JobDesc

    Public pkJobDesc As String

#Region "Property"
    Public Property getpkJobDesc() As String
        Get
            Return pkJobDesc
        End Get
        Set(ByVal value As String)
            pkJobDesc = value
        End Set
    End Property
#End Region

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        Me.Close()
    End Sub

    Private Sub AddEditJobDesc(ByVal EmpNo As String, ByVal ReportingTo As String, ByVal Location As String, ByVal JobDesc As String, _
                              ByVal pk_Job As String)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Member_JobDesc_AddEdit", _
                                     New SqlParameter("@employeeNo", EmpNo), _
                                     New SqlParameter("@ReportingTo", ReportingTo), _
                                     New SqlParameter("@Location", Location), _
                                     New SqlParameter("@JobDesc", JobDesc), _
                                     New SqlParameter("@pk_JobDesc", pk_Job))
            trans.Commit()
            MessageBox.Show("Record has been Saves", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "Save Job Description")
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If txtJobDesc.Text = "" Or txtLocation.Text = "" Or txtReportTo.Text = "" Then
            MessageBox.Show("Complete Infomation is Needed", "Job Description Information", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        Else
            Call AddEditJobDesc(frmMember_Master.txtEmployeeNo.Text.Trim, txtReportTo.Text.Trim, txtLocation.Text.Trim, txtJobDesc.Text, "")
            Call frmMember_Master.GetmemberJobDescription(frmMember_Master.txtEmployeeNo.Text)
            Call ClearText()
            'Me.Close()
        End If
    End Sub

    Private Sub btnupdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupdate.Click
            Call AddEditJobDesc(frmMember_Master.txtEmployeeNo.Text.Trim, txtReportTo.Text.Trim, txtLocation.Text.Trim, txtJobDesc.Text, pkJobDesc)
            Call frmMember_Master.GetmemberJobDescription(frmMember_Master.txtEmployeeNo.Text)
        Me.Close()
    End Sub

    Private Sub ClearText()
        txtJobDesc.Text = ""
        txtLocation.Text = ""
        txtReportTo.Text = ""
    End Sub
End Class