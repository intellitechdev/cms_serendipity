Imports System.Data.SqlClient
Imports System.Data.OleDb, System.IO

Public Class FrmAdmin_FileUploader
    Dim strpath, cnstring As String
    Dim oleConn As New OleDbConnection
    Dim oleCmd As New OleDbCommand
    Dim DT As New DataTable
    Dim errmsg As String
    Private Sub btngetfile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btngetfile.Click
        Dim openFileDialog1 As New OpenFileDialog()

        openFileDialog1.InitialDirectory = "c:\"
        openFileDialog1.Filter = "Comma Seperate Values (*.xls)|*.xls"
        openFileDialog1.FilterIndex = 2
        openFileDialog1.RestoreDirectory = True

        If openFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            strpath = openFileDialog1.FileName
            Me.txtfile.Text = LTrim(strpath)
        End If
    End Sub
    Public Sub GetConnection(ByVal FilePath As String)
        'check if the file exists
        Try
            If Not File.Exists(FilePath) Then
                MessageBox.Show("File does not exist or access is denied!", "Upload", MessageBoxButtons.OK)
            Else
                'connection string

                cnstring = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & FilePath & ";Extended Properties=""Excel 8.0;HDR=Yes;IMEX=1"""

                oleConn = New OleDbConnection(cnstring)
                oleCmd = oleConn.CreateCommand
                oleConn.Open()
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Me.txtfile.Text <> "" Then
            Me.Cursor = Cursors.WaitCursor
            Me.GetConnection(Me.txtfile.Text)
            Dim FileName As String = Path.GetFileName(Me.txtfile.Text)


            'Dim oleDA As New OleDbDataAdapter("SELECT [EmployeeID],[LastName],[FirstName],[MiddleName],[Designation],[Department]," & _
            '       "[DateHired],[DateBirth],[Gender],[DateRegular],[SG],[Employee Type],[Civil Status],[Status],[Basic Salary]," & _
            '       "[Philhealth],[SSS],[TIN],[PAGIBIG],[ADDRESS],[ADDRESS2],[ADDRESS3],[Home Phone],[Mobile No],[MachineID],[Company],[Division]," & _
            '       "[Department1],[Section],[Unit],[Position Code],[OrgDepartment],[Tax Code],[Rate Type],[ATM],[Basic Days],[Hours Per Day],[PAGIBIG AMOUNT],[Schedule] FROM [sheet1$]", oleConn)
            Dim oleda As New OleDbDataAdapter("SELECT  [EmployeeID],[Basic Salary] FROM [SHEET1$]", oleConn)
            Try
                'get data from excel

                oleda.Fill(DT)
                Me.Cursor = Cursors.Arrow
                Dim rows As Integer = DT.Rows.Count


                If DT.Rows.Count <> 0 Then
                    Me.DataGridView1.DataSource = DT
                    MessageBox.Show(rows & " Employee Basic Salary File Successfully Uploaded", "Upload", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If

            Catch ex As Exception
                Me.Cursor = Cursors.Arrow
                MessageBox.Show("PLease check your Excel Format!", "Upload", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Try
        End If
    End Sub

    Private Sub Btnupdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btnupdate.Click
        If Me.chkbasicpay.Checked = True Then
            Call UpdateBasicPay()
            Exit Sub
        End If
        If Me.chkATM.Checked = True Then
            Call UpdateATM()
            Exit Sub
        End If
        If Me.chktaxcode.Checked = True Then
            Call UpdateTaxcode()
            Exit Sub
        End If
        If Me.ChkPosition.Checked = True Then
            Call UpdatePosition()
            Exit Sub
        End If
        If Me.Chkdepartment.Checked = True Then
            Call UpdateDepartment()
            Exit Sub
        End If
        If Me.chk7column.Checked = True Then
            Call JHRPRG()
            Exit Sub
        End If
        If Me.chkTKClientID.Checked = True Then
            Call TKSIDandClientID()
            Exit Sub
        End If
        If Me.chkResignedActive.Checked = True Then
            Call UpdateResignedtoActive()
            Exit Sub
        End If
    End Sub
#Region "Update Basic Pay"
    Private Sub UpdateBasicPay()
        Dim x As New DialogResult
        If MessageBox.Show("Are You sure you want to save this Excel file in Employee Master File?", "Save", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            Dim rows As Integer = Me.DataGridView1.Rows.Count - 1
            Me.ProgressBar1.Maximum = rows - 1

            Dim i As Integer = 0
            While (i < rows)
                Me.ProgressBar1.Value = i
                Me.Cursor = Cursors.WaitCursor

                Dim employeeid As String = IIf(IsDBNull(Me.DataGridView1.Rows(i).Cells("EmployeeID").Value), "", Me.DataGridView1.Rows(i).Cells("EmployeeID").Value)
                If employeeid <> "" Then
                    Dim basicsalary As String = IIf(IsDBNull(Me.DataGridView1.Rows(i).Cells("Basic Salary").Value), "", Me.DataGridView1.Rows(i).Cells("Basic Salary").Value)
                    Dim saveemployee, savepayroll As New Clsappconfiguration

                    saveemployee.UploadEmployeeMaster(employeeid, basicsalary, errmsg)
                    'saveemployee.UploadPayrollMaster(employeeid, basicsalary, errmsg)

                    If errmsg <> "" Then
                        MessageBox.Show(errmsg, "Upload", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    End If
                End If

                i = i + 1

                If Me.ProgressBar1.Value = rows - 1 Then
                    Me.Cursor = Cursors.Arrow
                    x = MessageBox.Show(rows & " Employee Basic Pay Files Successfully Save!", "Upload", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    If x = Windows.Forms.DialogResult.OK Then
                        Me.Close()
                    End If
                End If
            End While
        End If
    End Sub
#End Region
#Region "Update ATM"
    Private Sub UpdateATM()
        Dim x As New DialogResult
        If MessageBox.Show("Are You sure you want to save this Excel file in Employee Master File?", "Save", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            Dim rows As Integer = Me.DataGridView1.Rows.Count - 1
            Me.ProgressBar1.Maximum = rows - 1

            Dim i As Integer = 0
            While (i < rows)
                Me.ProgressBar1.Value = i
                Me.Cursor = Cursors.WaitCursor

                Dim employeeid As String = IIf(IsDBNull(Me.DataGridView1.Rows(i).Cells("EmployeeID").Value), "", Me.DataGridView1.Rows(i).Cells("EmployeeID").Value)
                If employeeid <> "" Then
                    Dim ATM As String = IIf(IsDBNull(Me.DataGridView1.Rows(i).Cells("ATM").Value), "", Me.DataGridView1.Rows(i).Cells("ATM").Value)
                    Dim saveemployee, savepayroll As New Clsappconfiguration

                    saveemployee.UploadATM(employeeid, ATM, errmsg)
                    'saveemployee.UploadPayrollATM(employeeid, ATM, errmsg)

                    If errmsg <> "" Then
                        MessageBox.Show(errmsg, "Upload", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    End If
                End If

                i = i + 1

                If Me.ProgressBar1.Value = rows - 1 Then
                    Me.Cursor = Cursors.Arrow
                    x = MessageBox.Show(rows & " Employee ATM Files Successfully Save!", "Upload", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    If x = Windows.Forms.DialogResult.OK Then
                        Me.Close()
                    End If
                End If
            End While
        End If
    End Sub
#End Region
#Region "TAX CODE UPDATE RECORD"
    Private Sub UpdateTaxcode()
        Dim x As New DialogResult
        If MessageBox.Show("Are You sure you want to save this Excel file in Employee Master File?", "Save", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            Dim rows As Integer = Me.DataGridView1.Rows.Count - 1
            Me.ProgressBar1.Maximum = rows - 1

            Dim i As Integer = 0
            While (i < rows)
                Me.ProgressBar1.Value = i
                Me.Cursor = Cursors.WaitCursor

                Dim employeeid As String = IIf(IsDBNull(Me.DataGridView1.Rows(i).Cells("EmployeeID").Value), "", Me.DataGridView1.Rows(i).Cells("EmployeeID").Value)
                If employeeid <> "" Then
                    Dim taxcode As String = IIf(IsDBNull(Me.DataGridView1.Rows(i).Cells("Tax Code").Value), "", Me.DataGridView1.Rows(i).Cells("Tax Code").Value)
                    Dim saveemployee, savepayroll As New Clsappconfiguration

                    saveemployee.UpdateTaxcode(employeeid, taxcode, errmsg)
                    'saveemployee.updateTaxCodePayroll(employeeid, taxcode, errmsg)

                    If errmsg <> "" Then
                        MessageBox.Show(errmsg, "Upload", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    End If
                End If

                i = i + 1

                If Me.ProgressBar1.Value = rows - 1 Then
                    Me.Cursor = Cursors.Arrow
                    x = MessageBox.Show(rows & " Employee Tax Code Files Successfully Save!", "Upload", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    If x = Windows.Forms.DialogResult.OK Then
                        Me.Close()
                    End If
                End If
            End While
        End If
    End Sub

#End Region
#Region "Upate Position to Personnel System"
    Private Sub UpdatePosition()
        Dim x As New DialogResult
        If MessageBox.Show("Are You sure you want to save this Excel file in Employee Master File?", "Save", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            Dim rows As Integer = Me.DataGridView1.Rows.Count - 1
            Me.ProgressBar1.Maximum = rows - 1

            Dim i As Integer = 0
            While (i < rows)
                Me.ProgressBar1.Value = i
                Me.Cursor = Cursors.WaitCursor

                Dim employeeid As String = IIf(IsDBNull(Me.DataGridView1.Rows(i).Cells("EmployeeID").Value), "", Me.DataGridView1.Rows(i).Cells("EmployeeID").Value)
                If employeeid <> "" Then
                    Dim Position As String = IIf(IsDBNull(Me.DataGridView1.Rows(i).Cells("Position").Value), "", Me.DataGridView1.Rows(i).Cells("Position").Value)
                    Dim saveemployee As New Clsappconfiguration

                    saveemployee.UpdatePositionPersonnel(employeeid, Position, errmsg)
                    'saveemployee.updateTaxCodePayroll(employeeid, taxcode, errmsg)

                    If errmsg <> "" Then
                        MessageBox.Show(errmsg, "Upload", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    End If
                End If

                i = i + 1

                If Me.ProgressBar1.Value = rows - 1 Then
                    Me.Cursor = Cursors.Arrow
                    x = MessageBox.Show(rows & " Employee Position Files Successfully Save!", "Upload", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    If x = Windows.Forms.DialogResult.OK Then
                        Me.Close()
                    End If
                End If
            End While
        End If
    End Sub
#End Region
#Region "Update Personnel Department"
    Private Sub UpdateDepartment()
        Dim x As New DialogResult
        If MessageBox.Show("Are You sure you want to save this Excel file in Employee Master File?", "Save", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            Dim rows As Integer = Me.DataGridView1.Rows.Count - 1
            Me.ProgressBar1.Maximum = rows - 1

            Dim i As Integer = 0
            While (i < rows)
                Me.ProgressBar1.Value = i
                Me.Cursor = Cursors.WaitCursor

                Dim employeeid As String = IIf(IsDBNull(Me.DataGridView1.Rows(i).Cells("EmployeeID").Value), "", Me.DataGridView1.Rows(i).Cells("EmployeeID").Value)
                If employeeid <> "" Then
                    Dim department As String = IIf(IsDBNull(Me.DataGridView1.Rows(i).Cells("Department").Value), "", Me.DataGridView1.Rows(i).Cells("Department").Value)
                    Dim saveemployee As New Clsappconfiguration

                    saveemployee.UpdatePersonnelDepartment(employeeid, department, errmsg)
                    'saveemployee.updatepayrolldepartment(employeeid, department, errmsg)

                    If errmsg <> "" Then
                        MessageBox.Show(errmsg, "Upload", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    End If
                End If

                i = i + 1

                If Me.ProgressBar1.Value = rows - 1 Then
                    Me.Cursor = Cursors.Arrow
                    x = MessageBox.Show(rows & " Employee Department File Successfully Save!", "Upload", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    If x = Windows.Forms.DialogResult.OK Then
                        Me.Close()
                    End If
                End If
            End While
        End If
    End Sub
#End Region
#Region "JobGrad,Hiring Date, Regularization Date, Position, Resignation Date, Gender"
    Public Sub JHRPRG()
        Dim x As New DialogResult
        If MessageBox.Show("Are You sure you want to save this Excel file in Employee Master File?", "Save", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            Dim rows As Integer = Me.DataGridView1.Rows.Count - 1
            Me.ProgressBar1.Maximum = rows - 1

            Dim i As Integer = 0
            While (i < rows)
                Me.ProgressBar1.Value = i
                Me.Cursor = Cursors.WaitCursor

                Dim employeeid As String = IIf(IsDBNull(Me.DataGridView1.Rows(i).Cells("EmployeeID").Value), "", Me.DataGridView1.Rows(i).Cells("EmployeeID").Value)
                If employeeid <> "" Then
                    Dim jobgrade As String = IIf(IsDBNull(Me.DataGridView1.Rows(i).Cells("Job Grade").Value), "", Me.DataGridView1.Rows(i).Cells("Job Grade").Value)
                    Dim hireddate As String = IIf(IsDBNull(Me.DataGridView1.Rows(i).Cells("Hiring Date").Value), "", Me.DataGridView1.Rows(i).Cells("Hiring Date").Value)
                    Dim regdate As String = IIf(IsDBNull(Me.DataGridView1.Rows(i).Cells("Regularization Date").Value), "", Me.DataGridView1.Rows(i).Cells("Regularization Date").Value)
                    Dim Position As String = IIf(IsDBNull(Me.DataGridView1.Rows(i).Cells("Position").Value), "", Me.DataGridView1.Rows(i).Cells("Position").Value)
                    Dim resignationdate As String = IIf(IsDBNull(Me.DataGridView1.Rows(i).Cells("Resignation Date").Value), "", Me.DataGridView1.Rows(i).Cells("Resignation Date").Value)
                    Dim gender As String = IIf(IsDBNull(Me.DataGridView1.Rows(i).Cells("Gender").Value), "", Me.DataGridView1.Rows(i).Cells("Gender").Value)

                    Dim saveemployee As New Clsappconfiguration

                    saveemployee.updatePersonnelJHRPRG(employeeid, jobgrade, hireddate, regdate, Position, resignationdate, gender, errmsg)
                    'saveemployee.updatepayrolldepartment(employeeid, department, errmsg)

                    If errmsg <> "" Then
                        MessageBox.Show(errmsg, "Upload", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    End If
                End If

                i = i + 1

                If Me.ProgressBar1.Value = rows - 1 Then
                    Me.Cursor = Cursors.Arrow
                    x = MessageBox.Show(rows & " Employee Department File Successfully Save!", "Upload", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    If x = Windows.Forms.DialogResult.OK Then
                        Me.Close()
                    End If
                End If
            End While
        End If
    End Sub
#End Region
#Region "TKS ID and Client ID"
    Public Sub TKSIDandClientID()
        Dim x As New DialogResult
        If MessageBox.Show("Are You sure you want to save this Excel file in Employee Master File?", "Save", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            Dim rows As Integer = Me.DataGridView1.Rows.Count - 1
            Me.ProgressBar1.Maximum = rows - 1

            Dim i As Integer = 0
            While (i < rows)
                Me.ProgressBar1.Value = i
                Me.Cursor = Cursors.WaitCursor

                Dim employeeid As String = IIf(IsDBNull(Me.DataGridView1.Rows(i).Cells("IDnumber").Value), "", Me.DataGridView1.Rows(i).Cells("IDnumber").Value)
                If employeeid <> "" Then
                    Dim TKSID As String = IIf(IsDBNull(Me.DataGridView1.Rows(i).Cells("TKSID").Value), "", Me.DataGridView1.Rows(i).Cells("TKSID").Value)
                    Dim clientID As String = IIf(IsDBNull(Me.DataGridView1.Rows(i).Cells("ClientID").Value), "", Me.DataGridView1.Rows(i).Cells("ClientID").Value)

                    Dim saveemployee As New Clsappconfiguration
                    saveemployee.UpdateTKClientID(employeeid, TKSID, clientID)
                    If errmsg <> "" Then
                        MessageBox.Show(errmsg, "Upload", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    End If
                End If

                i = i + 1

                If Me.ProgressBar1.Value = rows - 1 Then
                    Me.Cursor = Cursors.Arrow
                    x = MessageBox.Show(rows & " Employee TKS ID and Client ID File Successfully Save!", "Upload", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    If x = Windows.Forms.DialogResult.OK Then
                        Me.Close()
                    End If
                End If
            End While
        End If
    End Sub
#End Region
#Region "Update RESIGNED TO ACTIVE EMPLOYEE"
    Public Sub UpdateResignedtoActive()
        Dim x As New DialogResult
        If MessageBox.Show("Are You sure you want to save this Excel file in Employee Master File?", "Save", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            Dim rows As Integer = Me.DataGridView1.Rows.Count - 1
            Me.ProgressBar1.Maximum = rows - 1

            Dim i As Integer = 0
            While (i < rows)
                Me.ProgressBar1.Value = i
                Me.Cursor = Cursors.WaitCursor

                Dim employeeid As String = IIf(IsDBNull(Me.DataGridView1.Rows(i).Cells("IDnumber").Value), "", Me.DataGridView1.Rows(i).Cells("IDnumber").Value)
                If employeeid <> "" Then
                    Dim PerActive As String = IIf(IsDBNull(Me.DataGridView1.Rows(i).Cells("PerActive").Value), "", Me.DataGridView1.Rows(i).Cells("PerActive").Value)
                    Dim PayActive As String = IIf(IsDBNull(Me.DataGridView1.Rows(i).Cells("PayActive").Value), "", Me.DataGridView1.Rows(i).Cells("PayActive").Value)

                    Dim saveemployee As New Clsappconfiguration
                    saveemployee.updateResignedActive(employeeid, PerActive, PayActive)
                    If errmsg <> "" Then
                        MessageBox.Show(errmsg, "Upload", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    End If
                End If

                i = i + 1

                If Me.ProgressBar1.Value = rows - 1 Then
                    Me.Cursor = Cursors.Arrow
                    x = MessageBox.Show(rows & " Resigned to Active File Successfully Save!", "Upload", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    If x = Windows.Forms.DialogResult.OK Then
                        Me.Close()
                    End If
                End If
            End While
        End If
    End Sub
#End Region
    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        Me.Close()
    End Sub

    Private Sub FrmFileUploader_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call ControlDisabled()
    End Sub
#Region "Control Disabled"
    Private Sub ControlDisabled()
        'Basic Pay
        Me.txtfile.ReadOnly = True
        Me.btngetfile.Enabled = False
        Me.Button1.Enabled = False

        'ATM
        Me.txtfileATM.ReadOnly = True
        Me.btngetfileATM.Enabled = False
        Me.BtnUploadATM.Enabled = False
        'tax code
        Me.txtfiletaxcode.ReadOnly = True
        Me.btngetfiletaxcode.Enabled = False
        Me.btnuploadtaxcode.Enabled = False
        'Position
        Me.txtfilePositionUpload.ReadOnly = True
        Me.btnUploadPosition.Enabled = False
        Me.btngetPositionfile.Enabled = False
        'Department
        Me.txtfiledepartmentUpload.ReadOnly = True
        Me.btnUploadDepartment.Enabled = False
        Me.btngetDepartment.Enabled = False
        'JHRPRG
        Me.txtpath7column.ReadOnly = True
        Me.btn7column.Enabled = False
        Me.btnUpload7Column.Enabled = False
        'TKSID AND CLIENTID
        Me.txtTKClientID.ReadOnly = True
        Me.btngetTKClientID.Enabled = False
        Me.BtnUploadTKClientID.Enabled = False
        'Resigned to active
        Me.txtResignedActive.ReadOnly = True
        Me.btngetResignedActive.Enabled = False
        Me.btnUploadResignedActive.Enabled = False
    End Sub
#End Region

    Private Sub btngetfileATM_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btngetfileATM.Click
        Dim openFileDialog1 As New OpenFileDialog()

        openFileDialog1.InitialDirectory = "c:\"
        openFileDialog1.Filter = "Comma Seperate Values (*.xls)|*.xls"
        openFileDialog1.FilterIndex = 2
        openFileDialog1.RestoreDirectory = True

        If openFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            strpath = openFileDialog1.FileName
            Me.txtfileATM.Text = LTrim(strpath)
        End If
    End Sub

    Private Sub BtnUploadATM_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnUploadATM.Click
        If Me.txtfileATM.Text <> "" Then
            Me.Cursor = Cursors.WaitCursor
            Me.GetConnection(Me.txtfileATM.Text)
            Dim FileName As String = Path.GetFileName(Me.txtfileATM.Text)


            'Dim oleDA As New OleDbDataAdapter("SELECT [EmployeeID],[LastName],[FirstName],[MiddleName],[Designation],[Department]," & _
            '       "[DateHired],[DateBirth],[Gender],[DateRegular],[SG],[Employee Type],[Civil Status],[Status],[Basic Salary]," & _
            '       "[Philhealth],[SSS],[TIN],[PAGIBIG],[ADDRESS],[ADDRESS2],[ADDRESS3],[Home Phone],[Mobile No],[MachineID],[Company],[Division]," & _
            '       "[Department1],[Section],[Unit],[Position Code],[OrgDepartment],[Tax Code],[Rate Type],[ATM],[Basic Days],[Hours Per Day],[PAGIBIG AMOUNT],[Schedule] FROM [sheet1$]", oleConn)
            Dim oleda As New OleDbDataAdapter("SELECT  [EmployeeID],[ATM] FROM [SHEET1$]", oleConn)
            Try
                'get data from excel

                oleda.Fill(DT)
                Me.Cursor = Cursors.Arrow
                Dim rows As Integer = DT.Rows.Count


                If DT.Rows.Count <> 0 Then
                    Me.DataGridView1.DataSource = DT
                    MessageBox.Show(rows & " Employee ATM File Successfully Uploaded", "Upload", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If

            Catch ex As Exception
                Me.Cursor = Cursors.Arrow
                MessageBox.Show("PLease check your Excel Format!", "Upload", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Try
        End If
    End Sub

    Private Sub chkbasicpay_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkbasicpay.CheckedChanged
        If Me.chkbasicpay.Checked = True Then
            Me.Chkdepartment.Enabled = False
            Me.chk7column.Enabled = False
            Me.txtfile.ReadOnly = False
            Me.btngetfile.Enabled = True
            Me.Button1.Enabled = True
            Me.chkATM.Enabled = False
            Me.chktaxcode.Enabled = False
            Me.ChkPosition.Enabled = False
            Me.chkTKClientID.Enabled = False
            Me.chkResignedActive.Enabled = False
        ElseIf Me.chkbasicpay.Checked = False Then
            Me.Chkdepartment.Enabled = True
            Me.chk7column.Enabled = True
            Me.txtfile.ReadOnly = True
            Me.btngetfile.Enabled = False
            Me.Button1.Enabled = False
            Me.chkATM.Enabled = True
            Me.txtfile.Text = ""
            Me.DataGridView1.DataSource = ""
            Me.DataGridView1.DataMember = ""
            Me.chktaxcode.Enabled = True
            Me.ChkPosition.Enabled = True
            Me.chkTKClientID.Enabled = True
            Me.chkResignedActive.Enabled = True
        End If
    End Sub

    Private Sub chkATM_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkATM.CheckedChanged
        If Me.chkATM.Checked = True Then
            Me.Chkdepartment.Enabled = False
            Me.chk7column.Enabled = False
            Me.txtfileATM.ReadOnly = True
            Me.btngetfileATM.Enabled = True
            Me.BtnUploadATM.Enabled = True
            Me.chkbasicpay.Enabled = False
            Me.txtfiletaxcode.Enabled = False
            Me.btngetfiletaxcode.Enabled = False
            Me.btnuploadtaxcode.Enabled = False
            Me.chktaxcode.Enabled = False
            Me.ChkPosition.Enabled = False
            Me.chkTKClientID.Enabled = False
            Me.chkResignedActive.Enabled = False
        ElseIf Me.chkATM.Checked = False Then
            Me.Chkdepartment.Enabled = True
            Me.chk7column.Enabled = True
            Me.txtfileATM.ReadOnly = True
            Me.btngetfileATM.Enabled = False
            Me.BtnUploadATM.Enabled = False
            Me.chkbasicpay.Enabled = True
            Me.txtfileATM.Text = ""
            Me.DataGridView1.DataSource = ""
            Me.DataGridView1.DataMember = ""
            Me.chktaxcode.Enabled = True
            Me.ChkPosition.Enabled = True
            Me.chkTKClientID.Enabled = True
            Me.chkResignedActive.Enabled = True
        End If
    End Sub


    Private Sub btngetfiletaxcode_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btngetfiletaxcode.Click
        Dim openFileDialog1 As New OpenFileDialog()

        openFileDialog1.InitialDirectory = "c:\"
        openFileDialog1.Filter = "Comma Seperate Values (*.xls)|*.xls"
        openFileDialog1.FilterIndex = 2
        openFileDialog1.RestoreDirectory = True

        If openFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            strpath = openFileDialog1.FileName
            Me.txtfiletaxcode.Text = LTrim(strpath)
        End If
    End Sub

    Private Sub btnuploadtaxcode_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnuploadtaxcode.Click
        If Me.txtfiletaxcode.Text <> "" Then
            Me.Cursor = Cursors.WaitCursor
            Me.GetConnection(Me.txtfiletaxcode.Text)
            Dim FileName As String = Path.GetFileName(Me.txtfiletaxcode.Text)

            'Dim oleDA As New OleDbDataAdapter("SELECT [EmployeeID],[LastName],[FirstName],[MiddleName],[Designation],[Department]," & _
            '       "[DateHired],[DateBirth],[Gender],[DateRegular],[SG],[Employee Type],[Civil Status],[Status],[Basic Salary]," & _
            '       "[Philhealth],[SSS],[TIN],[PAGIBIG],[ADDRESS],[ADDRESS2],[ADDRESS3],[Home Phone],[Mobile No],[MachineID],[Company],[Division]," & _
            '       "[Department1],[Section],[Unit],[Position Code],[OrgDepartment],[Tax Code],[Rate Type],[ATM],[Basic Days],[Hours Per Day],[PAGIBIG AMOUNT],[Schedule] FROM [sheet1$]", oleConn)
            Dim oleda As New OleDbDataAdapter("SELECT  [EmployeeID],[Tax Code] FROM [SHEET1$]", oleConn)
            Try
                'get data from excel
                oleda.Fill(DT)
                Me.Cursor = Cursors.Arrow
                Dim rows As Integer = DT.Rows.Count

                If DT.Rows.Count <> 0 Then
                    Me.DataGridView1.DataSource = DT
                    MessageBox.Show(rows & " Employee Tax Code File Successfully Uploaded", "Upload", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If

            Catch ex As Exception
                Me.Cursor = Cursors.Arrow
                MessageBox.Show("PLease check your Excel Format!", "Upload", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Try
        End If
    End Sub

    Private Sub chktaxcode_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chktaxcode.CheckedChanged
        If Me.chktaxcode.Checked = True Then
            Me.Chkdepartment.Enabled = False
            Me.chk7column.Enabled = False
            Me.txtfile.Enabled = False
            Me.txtfileATM.Enabled = False
            Me.chkbasicpay.Enabled = False
            Me.chkATM.Enabled = False
            Me.btngetfileATM.Enabled = False
            Me.BtnUploadATM.Enabled = False
            Me.btngetfiletaxcode.Enabled = True
            Me.btnuploadtaxcode.Enabled = True
            Me.txtfiletaxcode.ReadOnly = True
            Me.ChkPosition.Enabled = False
            Me.chkTKClientID.Enabled = False
            Me.chkResignedActive.Enabled = False
        ElseIf Me.chktaxcode.Checked = False Then
            Me.Chkdepartment.Enabled = True
            Me.chk7column.Enabled = True
            Me.txtfileATM.ReadOnly = True
            Me.chkbasicpay.Enabled = True
            Me.chkATM.Enabled = True
            Me.txtfileATM.Text = ""
            Me.DataGridView1.DataSource = ""
            Me.DataGridView1.DataMember = ""
            Me.btngetfiletaxcode.Enabled = False
            Me.btnuploadtaxcode.Enabled = False
            Me.txtfiletaxcode.ReadOnly = True
            Me.ChkPosition.Enabled = True
            Me.chkTKClientID.Enabled = True
            Me.chkResignedActive.Enabled = True
        End If
    End Sub

    Private Sub btngetPositionfile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btngetPositionfile.Click
        Dim openFileDialog1 As New OpenFileDialog()

        openFileDialog1.InitialDirectory = "c:\"
        openFileDialog1.Filter = "Comma Seperate Values (*.xls)|*.xls"
        openFileDialog1.FilterIndex = 2
        openFileDialog1.RestoreDirectory = True

        If openFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            strpath = openFileDialog1.FileName
            Me.txtfilePositionUpload.Text = LTrim(strpath)
        End If
    End Sub

    Private Sub btnUploadPosition_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUploadPosition.Click
        If Me.txtfilePositionUpload.Text <> "" Then
            Me.Cursor = Cursors.WaitCursor
            Me.GetConnection(Me.txtfilePositionUpload.Text)
            Dim FileName As String = Path.GetFileName(Me.txtfilePositionUpload.Text)
            Dim oleda As New OleDbDataAdapter("SELECT  [EmployeeID],[Position] FROM [SHEET1$]", oleConn)
            Try
                'get data from excel
                oleda.Fill(DT)
                Me.Cursor = Cursors.Arrow
                Dim rows As Integer = DT.Rows.Count

                If DT.Rows.Count <> 0 Then
                    Me.DataGridView1.DataSource = DT
                    MessageBox.Show(rows & " Employee Position File Successfully Uploaded", "Upload", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If

            Catch ex As Exception
                Me.Cursor = Cursors.Arrow
                MessageBox.Show("PLease check your Excel Format!", "Upload", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Try
        End If
    End Sub

    Private Sub ChkPosition_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ChkPosition.CheckedChanged
        If Me.ChkPosition.Checked = True Then
            Me.chktaxcode.Enabled = False
            Me.Chkdepartment.Enabled = False
            Me.chk7column.Enabled = False
            Me.txtfile.Enabled = False
            Me.txtfileATM.Enabled = False
            Me.chkbasicpay.Enabled = False
            Me.chkATM.Enabled = False
            Me.btngetfileATM.Enabled = False
            Me.BtnUploadATM.Enabled = False
            Me.btngetPositionfile.Enabled = True
            Me.btnUploadPosition.Enabled = True
            Me.txtfilePositionUpload.ReadOnly = True
            Me.ChkPosition.Enabled = True
            Me.chkTKClientID.Enabled = False
            Me.chkResignedActive.Enabled = False
        ElseIf Me.ChkPosition.Checked = False Then
            Me.chktaxcode.Enabled = True
            Me.Chkdepartment.Enabled = True
            Me.chk7column.Enabled = True
            Me.txtfileATM.ReadOnly = True
            Me.chkbasicpay.Enabled = True
            Me.chkATM.Enabled = True
            Me.txtfileATM.Text = ""
            Me.DataGridView1.DataSource = ""
            Me.DataGridView1.DataMember = ""
            Me.btngetPositionfile.Enabled = False
            Me.btnUploadPosition.Enabled = False
            Me.txtfilePositionUpload.ReadOnly = True
            Me.ChkPosition.Enabled = True
            Me.chkTKClientID.Enabled = True
            Me.chkResignedActive.Enabled = True
        End If
    End Sub

    Private Sub btngetDepartment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btngetDepartment.Click
        Dim openFileDialog1 As New OpenFileDialog()

        openFileDialog1.InitialDirectory = "c:\"
        openFileDialog1.Filter = "Comma Seperate Values (*.xls)|*.xls"
        openFileDialog1.FilterIndex = 2
        openFileDialog1.RestoreDirectory = True

        If openFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            strpath = openFileDialog1.FileName
            Me.txtfiledepartmentUpload.Text = LTrim(strpath)
        End If
    End Sub

    Private Sub btnUploadDepartment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUploadDepartment.Click
        If Me.txtfiledepartmentUpload.Text <> "" Then
            Me.Cursor = Cursors.WaitCursor
            Me.GetConnection(Me.txtfiledepartmentUpload.Text)
            Dim FileName As String = Path.GetFileName(Me.txtfiledepartmentUpload.Text)
            Dim oleda As New OleDbDataAdapter("SELECT  [EmployeeID],[Department] FROM [SHEET1$]", oleConn)
            Try
                'get data from excel
                oleda.Fill(DT)
                Me.Cursor = Cursors.Arrow
                Dim rows As Integer = DT.Rows.Count

                If DT.Rows.Count <> 0 Then
                    Me.DataGridView1.DataSource = DT
                    MessageBox.Show(rows & " Employee Department File Successfully Uploaded", "Upload", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If

            Catch ex As Exception
                Me.Cursor = Cursors.Arrow
                MessageBox.Show("PLease check your Excel Format!", "Upload", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Try
        End If
    End Sub

    Private Sub Chkdepartment_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Chkdepartment.CheckedChanged
        If Me.Chkdepartment.Checked = True Then
            Me.chktaxcode.Enabled = False
            Me.ChkPosition.Enabled = False
            Me.chk7column.Enabled = False
            Me.txtfile.Enabled = False
            Me.txtfileATM.Enabled = False
            Me.chkbasicpay.Enabled = False
            Me.chkATM.Enabled = False
            Me.btngetfileATM.Enabled = False
            Me.BtnUploadATM.Enabled = False
            Me.btngetDepartment.Enabled = True
            Me.btnUploadDepartment.Enabled = True
            Me.txtfiledepartmentUpload.ReadOnly = True
            Me.Chkdepartment.Enabled = True
            Me.chkTKClientID.Enabled = False
            Me.chkResignedActive.Enabled = False
        ElseIf Me.Chkdepartment.Checked = False Then
            Me.chktaxcode.Enabled = True
            Me.ChkPosition.Enabled = True
            Me.chk7column.Enabled = True
            Me.txtfileATM.ReadOnly = True
            Me.chkbasicpay.Enabled = True
            Me.chkATM.Enabled = True
            Me.txtfileATM.Text = ""
            Me.DataGridView1.DataSource = ""
            Me.DataGridView1.DataMember = ""
            Me.btngetDepartment.Enabled = False
            Me.btnUploadDepartment.Enabled = False
            Me.txtfiledepartmentUpload.ReadOnly = True
            Me.Chkdepartment.Enabled = True
            Me.chkTKClientID.Enabled = True
            Me.chkResignedActive.Enabled = True
        End If
    End Sub

    Private Sub btn7column_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn7column.Click
        Dim openFileDialog1 As New OpenFileDialog()

        openFileDialog1.InitialDirectory = "c:\"
        openFileDialog1.Filter = "Comma Seperate Values (*.xls)|*.xls"
        openFileDialog1.FilterIndex = 2
        openFileDialog1.RestoreDirectory = True

        If openFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            strpath = openFileDialog1.FileName
            Me.txtpath7column.Text = LTrim(strpath)
        End If
    End Sub

    Private Sub btnUpload7Column_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpload7Column.Click
        If Me.txtpath7column.Text <> "" Then
            Me.Cursor = Cursors.WaitCursor
            Me.GetConnection(Me.txtpath7column.Text)
            Dim FileName As String = Path.GetFileName(Me.txtpath7column.Text)
            Dim oleda As New OleDbDataAdapter("SELECT  [EmployeeID],[Job Grade],[Hiring Date],[Regularization Date],[Position],[Resignation Date],[Gender] FROM [SHEET1$]", oleConn)
            Try
                'get data from excel
                oleda.Fill(DT)
                Me.Cursor = Cursors.Arrow
                Dim rows As Integer = DT.Rows.Count

                If DT.Rows.Count <> 0 Then
                    Me.DataGridView1.DataSource = DT
                    MessageBox.Show(rows & " Employee  File Successfully Uploaded", "Upload", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If

            Catch ex As Exception
                Me.Cursor = Cursors.Arrow
                MessageBox.Show("PLease check your Excel Format!", "Upload", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Try
        End If
    End Sub

    Private Sub chk7column_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk7column.CheckedChanged
        If Me.chk7column.Checked = True Then
            Me.ChkPosition.Enabled = False
            Me.chktaxcode.Enabled = False
            Me.txtfile.Enabled = False
            Me.txtfileATM.Enabled = False
            Me.chkbasicpay.Enabled = False
            Me.chkATM.Enabled = False
            Me.btngetfileATM.Enabled = False
            Me.BtnUploadATM.Enabled = False
            Me.btn7column.Enabled = True
            Me.btnUpload7Column.Enabled = True
            Me.txtpath7column.ReadOnly = True
            Me.Chkdepartment.Enabled = False
            Me.chk7column.Checked = True
            Me.chkTKClientID.Enabled = False
            Me.chkResignedActive.Enabled = False
        ElseIf Me.chk7column.Checked = False Then
            Me.ChkPosition.Enabled = True
            Me.chktaxcode.Enabled = True
            Me.txtfileATM.ReadOnly = True
            Me.chkbasicpay.Enabled = True
            Me.chkATM.Enabled = True
            Me.txtfileATM.Text = ""
            Me.DataGridView1.DataSource = ""
            Me.DataGridView1.DataMember = ""
            Me.btn7column.Enabled = False
            Me.btnUpload7Column.Enabled = False
            Me.txtpath7column.ReadOnly = True
            Me.Chkdepartment.Enabled = True
            Me.chkTKClientID.Enabled = True
            Me.chkResignedActive.Enabled = True
        End If
    End Sub

    Private Sub btngetTKClientID_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btngetTKClientID.Click
        Dim openFileDialog1 As New OpenFileDialog()

        openFileDialog1.InitialDirectory = "c:\"
        openFileDialog1.Filter = "Comma Seperate Values (*.xls)|*.xls"
        openFileDialog1.FilterIndex = 2
        openFileDialog1.RestoreDirectory = True

        If openFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            strpath = openFileDialog1.FileName
            Me.txtTKClientID.Text = LTrim(strpath)
        End If
    End Sub

    Private Sub BtnUploadTKClientID_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnUploadTKClientID.Click
        If Me.txtTKClientID.Text <> "" Then
            Me.Cursor = Cursors.WaitCursor
            Me.GetConnection(Me.txtTKClientID.Text)
            Dim FileName As String = Path.GetFileName(Me.txtTKClientID.Text)
            Dim oleda As New OleDbDataAdapter("SELECT  [IDnumber],[TKSID],[ClientID] FROM [SHEET1$]", oleConn)
            Try
                'get data from excel
                oleda.Fill(DT)
                Me.Cursor = Cursors.Arrow
                Dim rows As Integer = DT.Rows.Count

                If DT.Rows.Count <> 0 Then
                    Me.DataGridView1.DataSource = DT
                    MessageBox.Show(rows & " TKS ID and Client ID Successfully Uploaded", "Upload", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If

            Catch ex As Exception
                Me.Cursor = Cursors.Arrow
                MessageBox.Show("PLease check your Excel Format!", "Upload", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Try
        End If
    End Sub

    Private Sub chkTKClientID_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkTKClientID.CheckedChanged
        If Me.chkTKClientID.Checked = True Then
            Me.ChkPosition.Enabled = False
            Me.chktaxcode.Enabled = False
            Me.txtfile.Enabled = False
            Me.txtfileATM.Enabled = False
            Me.chkbasicpay.Enabled = False
            Me.chkATM.Enabled = False
            Me.Chkdepartment.Enabled = False
            Me.chk7column.Enabled = False
            Me.btngetTKClientID.Enabled = True
            Me.BtnUploadTKClientID.Enabled = True
            Me.chkResignedActive.Enabled = False
        Else
            Me.ChkPosition.Enabled = True
            Me.chktaxcode.Enabled = True
            Me.txtfile.Enabled = True
            Me.txtfileATM.Enabled = True
            Me.chkbasicpay.Enabled = True
            Me.chkATM.Enabled = True
            Me.Chkdepartment.Enabled = True
            Me.chk7column.Enabled = True
            Me.btngetTKClientID.Enabled = False
            Me.BtnUploadTKClientID.Enabled = False
            Me.chkResignedActive.Enabled = True
        End If
    End Sub

    Private Sub btngetResignedActive_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btngetResignedActive.Click
        Dim openFileDialog1 As New OpenFileDialog()

        openFileDialog1.InitialDirectory = "c:\"
        openFileDialog1.Filter = "Comma Seperate Values (*.xls)|*.xls"
        openFileDialog1.FilterIndex = 2
        openFileDialog1.RestoreDirectory = True

        If openFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            strpath = openFileDialog1.FileName
            Me.txtResignedActive.Text = LTrim(strpath)
        End If
    End Sub

    Private Sub btnUploadResignedActive_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUploadResignedActive.Click
        If Me.txtResignedActive.Text <> "" Then
            Me.Cursor = Cursors.WaitCursor
            Me.GetConnection(Me.txtResignedActive.Text)
            Dim FileName As String = Path.GetFileName(Me.txtResignedActive.Text)
            Dim oleda As New OleDbDataAdapter("SELECT  [IDnumber],[PerActive],[PayActive] FROM [SHEET1$]", oleConn)
            Try
                'get data from excel
                oleda.Fill(DT)
                Me.Cursor = Cursors.Arrow
                Dim rows As Integer = DT.Rows.Count

                If DT.Rows.Count <> 0 Then
                    Me.DataGridView1.DataSource = DT
                    MessageBox.Show(rows & " Resigned to Active Employee Successfully Uploaded", "Upload", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If

            Catch ex As Exception
                Me.Cursor = Cursors.Arrow
                MessageBox.Show("PLease check your Excel Format!", "Upload", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Try
        End If
    End Sub

    Private Sub chkResignedActive_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkResignedActive.CheckedChanged
        If Me.chkResignedActive.Checked = True Then
            Me.chkbasicpay.Enabled = False
            Me.chkATM.Enabled = False
            Me.chktaxcode.Enabled = False
            Me.ChkPosition.Enabled = False
            Me.Chkdepartment.Enabled = False
            Me.chk7column.Enabled = False
            Me.chkTKClientID.Enabled = False
            Me.btngetResignedActive.Enabled = True
            Me.btnUploadResignedActive.Enabled = True
        Else
            Me.chkbasicpay.Enabled = True
            Me.chkATM.Enabled = True
            Me.chktaxcode.Enabled = True
            Me.ChkPosition.Enabled = True
            Me.Chkdepartment.Enabled = True
            Me.chk7column.Enabled = True
            Me.chkTKClientID.Enabled = True
            Me.btngetResignedActive.Enabled = False
            Me.btnUploadResignedActive.Enabled = False
        End If
    End Sub
End Class
