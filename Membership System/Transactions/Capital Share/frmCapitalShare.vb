Imports System.Data.Sql
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmCapitalShare

    Private gCon As New Clsappconfiguration
    Private rptsummary As New ReportDocument
    Private empNo As String


    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If

        Dim obj As ReportObject

        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function
    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        ' for each table apply connection info
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            ' check if logon was successful
            ' if TestConnectivity returns false,
            ' check logon credentials
            If (tbl.TestConnectivity()) Then
                'drop fully qualified table location
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next

        Return True
    End Function

    Private Sub FrmContributions_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Call Load_MemberNumber()
        Call Load_MemberName()
    End Sub
#Region "Load Member number"
    Private Sub Load_MemberNumber()
        Dim gCon As New Clsappconfiguration
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "CIMS_LoanManager_GetMemberList")
        While rd.Read
            lstNumber.Items.Add(rd.Item("Employee No")).ToString()
        End While
        rd.Close()
        gCon.sqlconn.Close()

    End Sub
#End Region
#Region "Load Member fullname"
    Private Sub Load_MemberName()
        Dim gCon As New Clsappconfiguration
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "CIMS_LoanManager_GetMemberList")
        With Me.lstMembers
            .Clear()
            .View = View.Details
            .GridLines = True
            .FullRowSelect = True
            .Columns.Add("IDnumber", 70, HorizontalAlignment.Left)
            .Columns.Add("Member's Fullname", 200, HorizontalAlignment.Left)
            Try
                While rd.Read
                    With .Items.Add(rd.Item(0))
                        .SubItems.Add(rd.Item(1))
                    End With
                End While
                rd.Close()
                gCon.sqlconn.Close()
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Load Member list")
            End Try
        End With
        'Dim gCon As New Clsappconfiguration
        'Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "CIMS_LoanManager_GetMemberList")
        'While rd.Read
        '    lstNames.Items.Add(rd.Item("Full Name")).ToString()
        'End While
        'rd.Close()
        'gCon.sqlconn.Close()
    End Sub
#End Region

    Private Sub txtSearchnumber_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchnumber.TextChanged
        Dim gcon As New Clsappconfiguration
        Dim searchno As String
        lstNumber.Items.Clear()

        searchno = "select fcEmployeeNo from dbo.CIMS_m_Member_Employment " & _
                    "where fcEmployeeNo LIKE '%" & txtSearchnumber.Text & "%'"


        Try
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.sqlconn, CommandType.Text, searchno)
            While rd.Read
                lstNumber.Items.Add(rd.Item(0).ToString)
            End While
            rd.Close()
            gcon.sqlconn.Close()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub txttiSearchLastname_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtSearchLastname.KeyPress
        If e.KeyChar = Chr(13) Then
            Call SearchMember(Me.txtSearchLastname.Text)
        End If
    End Sub

    Private Sub txtSearchLastname_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchLastname.TextChanged

    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub lstNumber_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstNumber.SelectedIndexChanged
        Call ViewContributionPerMember(Me.lstNumber.SelectedItem.ToString)
    End Sub

    Private Sub lstNames_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstNames.SelectedIndexChanged
        Dim x, result As String
        x = lstNames.SelectedItem.ToString
        Dim y As String() = x.Split(New Char() {","})
        result = y(0)
        'Call ViewContributionPerMember(result)
        Call ViewContributionPerMemberReport(result)
    End Sub
#Region "View Contribution Per member"
    Private Sub ViewContributionPerMemberReport(ByVal employeeNo As String)
        Try
            rptsummary.Load(Application.StartupPath & "\LoanReport\MembersContribution_v2.rpt")
            Logon(rptsummary, gCon.Server, gCon.Database, gCon.Username, gCon.Password)
            rptsummary.Refresh()
            rptsummary.SetParameterValue("@employeeNo", employeeNo)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ViewContributionPerMember(ByVal Empinfo As String)
        Dim gcon As New Clsappconfiguration
        Dim ds As New DataSet
        Dim ad As New SqlDataAdapter
        Dim cmd As New SqlCommand("MSS_LoanTracking_GetContributions", gcon.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        gcon.sqlconn.Open()
        With cmd.Parameters
            .Add("@EmployeeNo", SqlDbType.VarChar, 256).Value = Empinfo
        End With
        Try
            ad.SelectCommand = cmd
            ad.Fill(ds, "CIMS_m_Member")

            Me.grdContribution.DataSource = ds
            Me.grdContribution.DataMember = "CIMS_m_Member"
            Me.grdContribution.Columns(0).Width = 150
            Me.grdContribution.Columns(2).DefaultCellStyle.Format = "#,##0.00"
            Me.grdContribution.Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
            Me.grdContribution.Columns(3).DefaultCellStyle.Format = "#,##0.00"
            Me.grdContribution.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
            Me.grdContribution.Columns(4).DefaultCellStyle.Format = "#,##0.00"
            Me.grdContribution.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight

            'Dim i As Integer
            'For i = 0 To Me.grdContribution.Rows.Count - 1
            '    Dim dec As String
            '    dec = Format(Me.grdContribution.Item(4, i).Value, "#,##0.00")
            '    Me.grdContribution.Item(4, i).Value = dec
            'Next

            cmd.ExecuteNonQuery()
            gcon.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "View Contribution")
        End Try
    End Sub
#End Region
#Region "Search Member"
    Private Sub SearchMember(ByVal searchname As String)
        Dim gcon As New Clsappconfiguration
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.sqlconn, CommandType.StoredProcedure, "CIMS_LoanManager_GetMemberList_SearchName", _
                                                          New SqlParameter("@SEARCH", searchname))
        With Me.lstMembers
            .Clear()
            .View = View.Details
            .GridLines = True
            .FullRowSelect = True
            .Columns.Add("IDnumber", 70, HorizontalAlignment.Left)
            .Columns.Add("Member's Fullname", 200, HorizontalAlignment.Left)
            Try
                While rd.Read
                    With .Items.Add(rd.Item(0))
                        .SubItems.Add(rd.Item(1))
                    End With
                End While
                rd.Close()
                gcon.sqlconn.Close()
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Load Member list")
            End Try
        End With
    End Sub
#End Region

    Private Sub cmdSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSearch.Click
        Call SearchMember(Me.txtSearchLastname.Text)
    End Sub

    Private Sub lstMembers_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstMembers.Click

            empNo = Me.lstMembers.SelectedItems(0).Text
            loadingPic.Visible = True

        If bgwCapitalShare.IsBusy = False Then
            bgwCapitalShare.RunWorkerAsync()
        Else
            MessageBox.Show("Still Loading.. Please wait.", "User Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If


            Me.Text = "Contribution" + " ( " + Me.lstMembers.SelectedItems(0).Text.ToUpper + " : " + Me.lstMembers.SelectedItems(0).SubItems(1).Text.ToUpper + " ) "
    End Sub


    Private Sub bgwCapitalShare_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgwCapitalShare.DoWork
        Call ViewContributionPerMemberReport(empNo)
    End Sub

    Private Sub bgwCapitalShare_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwCapitalShare.RunWorkerCompleted
        crvRpt.Visible = True
        crvRpt.ReportSource = rptsummary

        loadingPic.Visible = False
    End Sub
End Class