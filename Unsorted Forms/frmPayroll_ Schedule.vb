Imports System.Data.SqlClient
Public Class Payroll_Schedule
   
    
#Region "Variables"
    Private getpayid As String
    Private getfxkeycat As String
#End Region
#Region "Get Property"
    Private Property getpaycategoryid() As String
        Get
            Return getpayid
        End Get
        Set(ByVal value As String)
            getpayid = value
        End Set
    End Property
    Private Property getfxpaycategory() As String
        Get
            Return getfxkeycat
        End Get
        Set(ByVal value As String)
            getfxkeycat = value
        End Set
    End Property
#End Region
    Private Sub btnnew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnnew.Click
        Try
            If btnnew.Text = "New" Then
                Me.txtpayrollcode.Focus()
                Label1.Text = "Add Pay Category"
                Call cleartxt()
                btnnew.Text = "Save"
                btnnew.Image = My.Resources.save2
                btnclose.Text = "Cancel"
                btnclose.Location = New System.Drawing.Point(75, 6)

                btndelete.Visible = False
                btnupdate.Visible = False
            ElseIf btnnew.Text = "Save" Then
                If Me.txtpayrollcode.Text <> "" And Me.txtpayrolldescription.Text <> "" Then
                    Label1.Text = "Pay Category"
                    Call check_insertpaysched_validation()
                    Call Payroll_category_insert()
                    Call cleartxt()
                    btnnew.Text = "New"
                    btnnew.Image = My.Resources.new3
                    btnclose.Text = "Close"
                    btnclose.Location = New System.Drawing.Point(227, 6)
                    btnupdate.Visible = True
                    btndelete.Visible = True
                Else
                    MessageBox.Show("Empty field found! ", "Save", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub
#Region "Payroll Category Validation/stored proc"
    Public Sub check_insertpaysched_validation()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_payroll_category_selectparameter", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("@Paycategorycode", SqlDbType.VarChar, 10).Value = Me.txtpayrollcode.Text
        cmd.Parameters.Add("@Paydescription", SqlDbType.VarChar, 50).Value = Me.txtpayrolldescription.Text

        myconnection.sqlconn.Open()
        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader
        Try
            If myreader.HasRows Then
                MessageBox.Show("Data already exist, please try another one.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
            Else
                Call add_payrollschedule()
                Call load_payrollschedule()
            End If
        Catch ex As Exception

        End Try

    End Sub
#End Region
#Region "Payroll Category Code Validation"
    Public Sub check_updatepaysched_validation()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_payroll_categorycode_valid_select", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@Paycategorycode", SqlDbType.Char, 10).Value = Me.txtpayrollcode.Text
        myconnection.sqlconn.Open()
        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader
        Try
            If myreader.HasRows Then
                MessageBox.Show("That Payroll code" + " ' " + Me.txtpayrollcode.Text + " ' " + "is already used , Please try another one", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)

            Else
                Call update_payrollschedule()
                Call load_payrollschedule()
            End If


        Catch ex As Exception
        End Try
    End Sub
#End Region
#Region "Payroll Category Description Validation"
    Public Sub check_updatepaydescription()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_payroll_categorydesc_validation", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("@Paydescription", SqlDbType.VarChar, 50).Value = Me.txtpayrolldescription.Text

        myconnection.sqlconn.Open()
        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader
        Try
            If myreader.HasRows Then
                MessageBox.Show("That Payroll Description " + " ' " + Me.txtpayrolldescription.Text + " ' " + "is already used , Please try another one", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
            Else
                Call update_payrollschedule()
                Call load_payrollschedule()
            End If
        Catch ex As Exception
        End Try
    End Sub
#End Region
#Region "Payroll Category Insert/stored proc"
    Public Sub add_payrollschedule()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_payroll_category_insert", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("@fxkeycategory", SqlDbType.Char, 2, ParameterDirection.Output).Value = ""
        cmd.Parameters.Add("@Paycategorycode", SqlDbType.VarChar, 10).Value = Me.txtpayrollcode.Text
        cmd.Parameters.Add("@Paydescription", SqlDbType.VarChar, 50).Value = Me.txtpayrolldescription.Text
        cmd.Parameters.Add("@PayType", SqlDbType.Char, 2).Value = Me.txtPayType.Text

        myconnection.sqlconn.Open()
        cmd.ExecuteNonQuery()
        myconnection.sqlconn.Close()
    End Sub
#End Region
#Region "Payroll $ Tk Category Insert/stored proc"
    Private Sub Payroll_category_insert()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_payroll_tk_category_insert", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("@fxKeyPRCategory", SqlDbType.Char, 2, ParameterDirection.Output).Value = ""
        cmd.Parameters.Add("@fcCategoryCode", SqlDbType.VarChar, 10).Value = Me.txtpayrollcode.Text
        cmd.Parameters.Add("@fcCategoryDesc", SqlDbType.VarChar, 50).Value = Me.txtpayrolldescription.Text
        cmd.Parameters.Add("@fcCategoryType", SqlDbType.Char, 2).Value = Me.txtPayType.Text

        myconnection.sqlconn.Open()
        cmd.ExecuteNonQuery()
        myconnection.sqlconn.Close()
    End Sub
#End Region
#Region "Payroll Category get/stored proc"
    Public Sub load_payrollschedule()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_payroll_category_get", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        With Me.lvlPayCategory
            .Clear()
            .View = View.Details
            .GridLines = True
            .FullRowSelect = True
            .Columns.Add("Category Code", 100, HorizontalAlignment.Left)
            .Columns.Add("Category Description", 160, HorizontalAlignment.Left)
            .Columns.Add("Category Type", 100, HorizontalAlignment.Left)
            Dim myreader As SqlDataReader
            myreader = cmd.ExecuteReader
            Try
                While myreader.Read
                    With .Items.Add(myreader.Item(0))
                        .subitems.add(myreader.Item(1))
                        .subitems.add(myreader.Item(2))
                        .subitems.add(myreader.Item(3)) 'payid
                        .subitems.add(myreader.Item(4)) 'fxkey
                    End With
                End While
            Finally
                myreader.Close()
                myconnection.sqlconn.Close()
            End Try
        End With
    End Sub
#End Region

    Private Sub btnupdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupdate.Click
        Try

            If btnupdate.Text = "Edit" Then
                Me.txtpayrollcode.Focus()
                Label1.Text = "Edit Pay Category"
                Me.txtpayrollcode.Text = Me.lvlPayCategory.SelectedItems(0).Text
                Me.txtpayrollcode.Tag = Me.lvlPayCategory.SelectedItems(0).Text
                Me.txtpayrolldescription.Text = Me.lvlPayCategory.SelectedItems(0).SubItems(1).Text
                Me.txtpayrolldescription.Tag = Me.lvlPayCategory.SelectedItems(0).SubItems(1).Text
                Me.txtPayType.Text = Me.lvlPayCategory.SelectedItems(0).SubItems(2).Text

                getpaycategoryid = Me.lvlPayCategory.SelectedItems(0).SubItems(3).Text
                getfxpaycategory = Me.lvlPayCategory.SelectedItems(0).SubItems(4).Text

                btnupdate.Text = "Update"
                btnupdate.Image = My.Resources.save2
                btnclose.Text = "Cancel"
                btnclose.Location = New System.Drawing.Point(144, 6)

                btndelete.Visible = False
                btnnew.Enabled = False
            ElseIf btnupdate.Text = "Update" Then
                If Me.txtpayrollcode.Text <> "" And Me.txtpayrolldescription.Text <> "" Then
                    If Me.txtpayrollcode.Text = Me.txtpayrollcode.Tag And Me.txtpayrolldescription.Text = Me.txtpayrolldescription.Tag Then
                        Label1.Text = "Pay Category"
                        Call update_payrollschedule()
                        Call Payroll_category_update()
                        Call load_payrollschedule()
                        Call cleartxt()
                        btnupdate.Text = "Edit"
                        btnupdate.Image = My.Resources.edit1
                        btnclose.Text = "Cancel"
                        btnclose.Location = New System.Drawing.Point(227, 6)
                        btnclose.Text = "Close"
                        btndelete.Visible = True
                        btnnew.Enabled = True
                    ElseIf Me.txtpayrollcode.Text <> Me.txtpayrollcode.Tag Then
                        Label1.Text = "Pay Category"
                        Call check_updatepaysched_validation()
                        Call Payroll_category_update()
                        Call cleartxt()
                        btnupdate.Text = "Edit"
                        btnupdate.Image = My.Resources.edit1
                        btnclose.Text = "Cancel"
                        btnclose.Location = New System.Drawing.Point(227, 6)
                        btnclose.Text = "Close"
                        btndelete.Visible = True
                        btnnew.Enabled = True
                    ElseIf Me.txtpayrolldescription.Text <> Me.txtpayrolldescription.Tag Then
                        Label1.Text = "Pay Category"
                        Call check_updatepaydescription()
                        Call Payroll_category_update()
                        Call cleartxt()
                        btnupdate.Text = "Edit"
                        btnupdate.Image = My.Resources.edit1
                        btnclose.Text = "Cancel"
                        btnclose.Location = New System.Drawing.Point(227, 6)
                        btnclose.Text = "Close"
                        btndelete.Visible = True
                        btnnew.Enabled = True
                    End If

                Else
                    MessageBox.Show("Empty field found! ", "Edit", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub
#Region "Payroll Category Update/stored proc"
    Public Sub update_payrollschedule()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_payroll_category_update", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("@PayschedID", SqlDbType.Int).Value = getpayid
        cmd.Parameters.Add("@fxkeycategory", SqlDbType.Char, 2).Value = getfxkeycat
        cmd.Parameters.Add("@Paycategorycode", SqlDbType.VarChar, 10).Value = Me.txtpayrollcode.Text
        cmd.Parameters.Add("@Paydescription", SqlDbType.VarChar, 50).Value = Me.txtpayrolldescription.Text
        cmd.Parameters.Add("@PayType", SqlDbType.Char, 2).Value = Me.txtPayType.Text

        myconnection.sqlconn.Open()
        cmd.ExecuteNonQuery()
        myconnection.sqlconn.Close()
    End Sub
#End Region
#Region "Payroll $ TK Category Update/stored proc"
    Private Sub Payroll_category_update()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_payroll_category_update1", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure

        'cmd.Parameters.Add("@PayschedID", SqlDbType.Int).Value = getpayid
        cmd.Parameters.Add("@fxKeyPRCategory", SqlDbType.Char, 2).Value = getfxkeycat
        cmd.Parameters.Add("@fcCategoryCode", SqlDbType.VarChar, 10).Value = Me.txtpayrollcode.Text
        cmd.Parameters.Add("@fcCategoryDesc", SqlDbType.VarChar, 50).Value = Me.txtpayrolldescription.Text
        cmd.Parameters.Add("@fcCategoryType", SqlDbType.Char, 2).Value = Me.txtPayType.Text

        myconnection.sqlconn.Open()
        cmd.ExecuteNonQuery()
        myconnection.sqlconn.Close()
    End Sub
#End Region
    Private Sub btndelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndelete.Click
        getpaycategoryid() = Me.lvlPayCategory.SelectedItems(0).SubItems(3).Text
        Dim x As New DialogResult
        x = MessageBox.Show("Are you sure you want to permanently delete this record?", "Confirm Deletion", MessageBoxButtons.OKCancel, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
        If x = System.Windows.Forms.DialogResult.OK Then
            'If Me.grdpayroll.SelectedRows.Count > 0 AndAlso _
            '          Not Me.grdpayroll.SelectedRows(0).Index = _
            '          Me.grdpayroll.Rows.Count - 1 Then

            Call delete_payrollschedule()
            Call load_payrollschedule()
            '    Me.grdpayroll.Rows.RemoveAt( _
            '        Me.grdpayroll.SelectedRows(0).Index)

            'End If
        ElseIf x = System.Windows.Forms.DialogResult.Cancel Then
        End If
    End Sub
#Region "Payroll Category Delete/stored proc"
    Public Sub delete_payrollschedule()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_payroll_category_delete", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@PayschedID", SqlDbType.Int).Value = getpayid
        myconnection.sqlconn.Open()
        cmd.ExecuteNonQuery()
        myconnection.sqlconn.Close()
    End Sub
#End Region

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        If btnclose.Text = "Cancel" Then
            Label1.Text = "Pay Category"
            btnnew.Text = "New"
            btnnew.Image = My.Resources.new3
            btnupdate.Text = "Edit"
            btnupdate.Image = My.Resources.edit1
            btnupdate.Visible = True
            btndelete.Visible = True
            btnnew.Enabled = True
            btnclose.Text = "Close"
            btnclose.Location = New System.Drawing.Point(227, 6)
        ElseIf btnclose.Text = "Close" Then
            Me.Close()
        End If
    End Sub

    Private Sub Payroll_Schedule_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call load_payrollschedule()
        Call disabledbutton()
    End Sub
    Private Sub disabledbutton()
        Me.btnnew.Enabled = False
        Me.btndelete.Enabled = False
        Me.btnupdate.Enabled = False
    End Sub
 
    Public Sub cleartxt()
        Me.txtpayrollcode.Text = ""
        Me.txtpayrolldescription.Text = ""
        Me.txtPayType.Text = ""
    End Sub

    Private Sub txtpayrollcode_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtpayrollcode.KeyPress
        e.Handled = Not alphanumeric_Validate(e.KeyChar)
    End Sub

    Private Sub txtpayrolldescription_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtpayrolldescription.KeyPress
        e.Handled = Not alphanumeric_Validate(e.KeyChar)
    End Sub



    Private Sub Label2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label2.Click

    End Sub
End Class