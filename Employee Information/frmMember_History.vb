﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmMember_History

    Private rptsummary As New ReportDocument
    Private gcon As New Clsappconfiguration
    Public ReportMode As String = ""
    Dim History As String
    Dim client As String

#Region "Functions"
    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If

        Dim obj As ReportObject

        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function

    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            If (tbl.TestConnectivity()) Then
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next

        Return True
    End Function

    Private Sub LoadReport()
        Try
            Select Case History
                Case "Payroll History"
                    rptsummary.Load(Application.StartupPath & "\LoanReport\MemberHistory_Payroll.rpt")
                    Logon(rptsummary, gcon.Server, gcon.Database, gcon.Username, gcon.Password)
                    rptsummary.Refresh()
                    rptsummary.SetParameterValue("@EmployeeNo", txtEmployeeNo.Text)

                Case "Contract History"
                    rptsummary.Load(Application.StartupPath & "\LoanReport\MemberHistory_ContractHistroy.rpt")
                    Logon(rptsummary, gcon.Server, gcon.Database, gcon.Username, gcon.Password)
                    rptsummary.Refresh()
                    rptsummary.SetParameterValue("@EmployeeNo", txtEmployeeNo.Text)
                    rptsummary.SetParameterValue("@Client", client)

                Case "Information History"
                    rptsummary.Load(Application.StartupPath & "\LoanReport\MemberHistory_Information.rpt")
                    Logon(rptsummary, gcon.Server, gcon.Database, gcon.Username, gcon.Password)
                    rptsummary.Refresh()
                    rptsummary.SetParameterValue("@EmployeeNo", txtEmployeeNo.Text)
            End Select
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
#End Region

    Private Sub bgwProcessReport_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgwProcessReport.DoWork
        LoadReport()
    End Sub

    Private Sub bgwProcessReport_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwProcessReport.RunWorkerCompleted
        Me.CrvRpt.Visible = True
        Me.CrvRpt.ReportSource = rptsummary
        picLoading.Visible = False
    End Sub

    Private Sub btnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        picLoading.Visible = True
        History = cboHistory.Text
        client = cboClient.Text
        If bgwProcessReport.IsBusy = False Then
            bgwProcessReport.RunWorkerAsync()
        End If
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub frmMember_History_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        cboClient.Visible = False
        Label3.Visible = False
        btnPreview.Enabled = False
    End Sub

    Private Sub cboHistory_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboHistory.SelectedValueChanged
        If cboHistory.Text = "Contract History" Then
            Label3.Visible = True
            cboClient.Visible = True
            btnPreview.Enabled = False
        Else
            btnPreview.Enabled = True
            Label3.Visible = False
            cboClient.Visible = False
        End If
    End Sub

    Private Sub cboClient_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboClient.SelectedIndexChanged
        btnPreview.Enabled = True
    End Sub
End Class