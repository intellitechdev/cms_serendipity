Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
Public Class frmManual_Interest
    Private gcon As New Clsappconfiguration
#Region "Property"
    Private EMPID As String
    Public Property GetEMPID() As String
        Get
            Return EMPID
        End Get
        Set(ByVal value As String)
            EMPID = value
        End Set
    End Property
    Private SelectSTD As String
    Public Property GetSelectSTD() As String
        Get
            Return SelectSTD
        End Get
        Set(ByVal value As String)
            SelectSTD = value
        End Set
    End Property
    Private idNo As String
    Public Property GetIDNo() As String
        Get
            Return idNo
        End Get
        Set(ByVal value As String)
            idNo = value
        End Set
    End Property
    Private Name As String
    Public Property GetName() As String
        Get
            Return Name
        End Get
        Set(ByVal value As String)
            Name = value
        End Set
    End Property
#End Region
    Private Sub frmManual_Interst_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.rdoIndividual.Checked = True
        Try
            If GetSelectSTD = 0 Then
                If rdoIndividual.Checked = True Then
                    LOAD_INTEREST_perEMPID(GetEMPID())
                Else
                    LOAD_INTEREST()
                End If
            ElseIf GetSelectSTD = 1 Then
                If rdoIndividual.Checked = True Then
                    Load_Interest_TimeDeposit_PerEmpID(GetEMPID())
                Else
                    Load_Interest_TimeDeposit()
                End If
            End If

            checkinfo()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Close()
    End Sub
    Private Sub LOAD_INTEREST()
        Dim ds As DataSet
        grdInterest.Columns.Clear()
        Try
            ds = SqlHelper.ExecuteDataset(gcon.cnstring, CommandType.StoredProcedure, "CIMS_LOAD_INTEREST4EACH_MEMBER")
            With Me.grdInterest
                .DataSource = ds.Tables(0)
                .ReadOnly = True
                .Columns(0).Width = 100
                .Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns(1).Width = 150
                .Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns(2).DefaultCellStyle.Format = "#,##0.00"
                .Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                .Columns(2).Width = 150
                .Columns(3).Width = 150
                .Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                .Columns(3).DefaultCellStyle.Format = "#,##0.00"
                .Columns(4).Width = 150
                .Columns(4).DefaultCellStyle.Format = "#,##0.00"
                .Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                .Columns(5).Width = 125
                .Columns(5).DefaultCellStyle.Format = "#,##0.00"
                .Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                .Columns(6).Visible = False
            End With
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub LOAD_INTEREST_perEMPID(ByVal EmpIDs As String)
        Dim ds As New DataSet
        Dim ad As New SqlDataAdapter
        Dim cmd As New SqlCommand("CIMS_LOAD_INTEREST4EACH_MEMBER_perEMPID", gcon.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        gcon.sqlconn.Open()
        With cmd.Parameters
            .Add("@EMPID", SqlDbType.VarChar, 256).Value = EmpIDs
        End With
        grdInterest.Columns.Clear()
        Try
            ad.SelectCommand = cmd
            ad.Fill(ds, "CIMS_m_Member")
            Me.grdInterest.DataSource = ds
            Me.grdInterest.DataMember = "CIMS_m_Member"
            grdInterest.ReadOnly = True
            grdInterest.Columns(0).Width = 100
            grdInterest.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            grdInterest.Columns(1).Width = 150
            grdInterest.Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            grdInterest.Columns(2).DefaultCellStyle.Format = "#,##0.00"
            grdInterest.Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            grdInterest.Columns(2).Width = 150
            grdInterest.Columns(3).Width = 150
            grdInterest.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            grdInterest.Columns(3).DefaultCellStyle.Format = "#,##0.00"
            grdInterest.Columns(4).Width = 150
            grdInterest.Columns(4).DefaultCellStyle.Format = "#,##0.00"
            grdInterest.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            grdInterest.Columns(5).Width = 150
            grdInterest.Columns(5).DefaultCellStyle.Format = "#,##0.00"
            grdInterest.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            grdInterest.Columns(6).Visible = False
            txtamount.Text = ds.Tables(0).Rows(0).Item(5).ToString()
            cmd.ExecuteNonQuery()

        Catch ex As Exception
            Throw ex
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub
    Private Sub Load_Interest_TimeDeposit()
        Dim ds As DataSet
        grdInterest.Columns.Clear()
        Try
            ds = SqlHelper.ExecuteDataset(gcon.cnstring, CommandType.StoredProcedure, "[CIMS_LOAD_INTEREST4EACH_MEMBER_TimeDeosit]")
            With Me.grdInterest
                .DataSource = ds.Tables(0)
                .ReadOnly = True
                .Columns(0).Width = 100
                .Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns(1).Width = 150
                .Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns(2).DefaultCellStyle.Format = "#,##0.00"
                .Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                .Columns(2).Width = 150
                .Columns(3).Width = 150
                .Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                .Columns(3).DefaultCellStyle.Format = "#,##0.00"
                .Columns(4).Width = 150
                .Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns(5).Width = 125
                .Columns(5).DefaultCellStyle.Format = "#,##0.00"
                .Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                .Columns(6).Visible = False
            End With
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub Load_Interest_TimeDeposit_PerEmpID(ByVal EmpIDt As String)
        Dim ds As New DataSet
        Dim ad As New SqlDataAdapter
        Dim cmd As New SqlCommand("[CIMS_LOAD_INTEREST4EACH_MEMBER_PerEmpID_TimeDeosit]", gcon.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        gcon.sqlconn.Open()
        With cmd.Parameters
            .Add("@EMPID", SqlDbType.VarChar, 256).Value = EmpIDt
        End With
        grdInterest.Columns.Clear()
        Try
            ad.SelectCommand = cmd
            ad.Fill(ds, "CIMS_m_Member")
            Me.grdInterest.DataSource = ds
            Me.grdInterest.DataMember = "CIMS_m_Member"
            grdInterest.ReadOnly = True
            grdInterest.Columns(0).Width = 100
            grdInterest.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            grdInterest.Columns(1).Width = 150
            grdInterest.Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            grdInterest.Columns(2).DefaultCellStyle.Format = "#,##0.00"
            grdInterest.Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            grdInterest.Columns(2).Width = 150
            grdInterest.Columns(3).Width = 150
            grdInterest.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            grdInterest.Columns(3).DefaultCellStyle.Format = "#,##0.00"
            grdInterest.Columns(4).Width = 150
            grdInterest.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            grdInterest.Columns(5).Width = 150
            grdInterest.Columns(5).DefaultCellStyle.Format = "#,##0.00"
            grdInterest.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            grdInterest.Columns(6).Visible = False
            txtamount.Text = ds.Tables(0).Rows(0).Item(5).ToString()
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub
    Private Sub checkinfo()
        Try
            Me.txtID.Text = GetIDNo()
            Me.txtname.Text = GetName()
            txtname.Visible = True
            txtamount.Visible = True
            txtID.Visible = True
            Label2.Visible = True
            Label3.Visible = True
            Label5.Visible = True

            If rdoIndividual.Checked = True Then

                If SelectSTD = 0 Then
                    Me.txtDesc.Text = "Savings - Interest Credited"
                Else
                    Me.txtDesc.Text = "Time Deposit - Interest Credited"
                End If

            Else
                Me.txtID.Visible = False
                txtname.Visible = False
                txtamount.Visible = False
                Label2.Visible = False
                Label3.Visible = False
                Label5.Visible = False
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub RadioButton2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdoAll.CheckedChanged
        Try
            checkinfo()
            If GetSelectSTD = 0 Then
                If rdoAll.Checked = True Then
                    LOAD_INTEREST()
                Else
                    LOAD_INTEREST_perEMPID(GetEMPID())
                End If
            ElseIf GetSelectSTD = 1 Then
                If rdoAll.Checked = True Then
                    Load_Interest_TimeDeposit()
                Else
                    Load_Interest_TimeDeposit_PerEmpID(GetEMPID())
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub ApplyTimeDeposit_Interest(ByVal Dpdate As Date, ByVal descr As String, ByVal eID As String, ByVal intamount As Decimal, ByVal clear As Boolean, ByVal fnTerms As Integer)

        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_ADD_INTEREST_IN_TIMEDEPOSIT", _
                                    New SqlParameter("@dtDepdate", Dpdate), _
                                    New SqlParameter("@fcDes", descr), _
                                    New SqlParameter("@Emp", eID), _
                                    New SqlParameter("@fdDepAmnt", intamount), _
                                    New SqlParameter("@tdClear", clear), _
                                    New SqlParameter("@fnTerms", fnTerms))
            trans.Commit()



        Catch ex As Exception
            trans.Rollback()
            Throw ex
        Finally
            gcon.sqlconn.Close()
            Me.Close()
        End Try
    End Sub
    Private Sub Applyinterest_PerMember(ByVal intdate As Date, ByVal desc As String, _
                     ByVal emp As String, ByVal intamount As Decimal, ByVal clear As Boolean)
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_ADD_INTEREST_IN_SAVINGS", _
                                        New SqlParameter("@dtDepdate", intdate), _
                                        New SqlParameter("@fcDes", desc), _
                                        New SqlParameter("@Emp", emp), _
                                        New SqlParameter("@fdDepAmnt", intamount), _
                                        New SqlParameter("@tdClear", clear))

            trans.Commit()

        Catch ex As Exception
            trans.Rollback()
            Throw ex
        Finally
            gcon.sqlconn.Close()
        End Try
        Me.Close()
    End Sub
    Private Sub btnApply_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnApply.Click
        Try
            If GetSelectSTD = 0 Then
                'Apply Interest Per Member-----------------Savings-------------------------
                If rdoIndividual.Checked = True Then
                    Dim x As New DialogResult
                    x = MessageBox.Show("Are you sure you want to Apply Interest?", "Confirm Update", MessageBoxButtons.OKCancel, MessageBoxIcon.Information)

                    If x = Windows.Forms.DialogResult.OK Then
                        Call Applyinterest_PerMember(dateint.Value, txtDesc.Text, grdInterest.CurrentRow.Cells(6).Value.ToString, grdInterest.CurrentRow.Cells(5).Value.ToString, 1)
                    ElseIf x = Windows.Forms.DialogResult.Cancel Then
                    End If

                    ' Accounting Entry (Interest Crediting)
                    Dim interestAmount As Decimal = grdInterest.CurrentRow.Cells(5).Value.ToString()
                    Call AccountingEntry_SavingsInterestCrediting(txtID.Text, interestAmount, txtDesc.Text, dateint.Value, frmMain.username)

                    MessageBox.Show("You have successfully credited the Interest!")


                    'Apply Interest to all
                ElseIf rdoAll.Checked = True Then
                    If MessageBox.Show("Are you sure you want to credit all members with this interest?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                        Call LoopToAllMemberAndApplyInterest()
                        MessageBox.Show("You have successfully credited the Interest!")
                    End If
                End If
            ElseIf GetSelectSTD = 1 Then
                'Apply Interest per Member-----------------Time Deposit--------------------
                If rdoIndividual.Checked = True Then
                    Dim x As New DialogResult
                    x = MessageBox.Show("Are you sure you want to Apply Interest?", "Confirm Update", MessageBoxButtons.OKCancel, MessageBoxIcon.Information)

                    If x = Windows.Forms.DialogResult.OK Then
                        Call ApplyTimeDeposit_Interest(dateint.Value, txtDesc.Text, grdInterest.CurrentRow.Cells(6).Value.ToString, grdInterest.CurrentRow.Cells(5).Value.ToString, 1, 0)
                    ElseIf x = Windows.Forms.DialogResult.Cancel Then
                    End If

                    ' Accounting Entry (Interest Crediting)
                    Dim interestAmount As Decimal = grdInterest.CurrentRow.Cells(5).Value.ToString()
                    Call AccountingEntry_TimeInterestCrediting(txtID.Text, interestAmount, txtDesc.Text, dateint.Value, frmMain.username)

                    MessageBox.Show("You have successfully credited the Interest!")

                    'Apply Interest to all
                ElseIf rdoAll.Checked = True Then
                    If MessageBox.Show("Are you sure you want to credit all members with this interest?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                        Call LoopToAllMemberAndApplyInterestTimeDeposit()
                        MessageBox.Show("You have successfully credited the Interest!")
                    End If
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub AccountingEntry_TimeInterestCrediting(ByVal empNo As String, ByVal amount As Decimal, ByVal particulars As String, ByVal effectiveDate As Date, ByVal user As String)
        Try
            ModifiedSqlHelper.ExecuteNonQuery(gcon.cnstring, CommandType.StoredProcedure, "CIMS_InterestCrediting_AccountingEntries_ToGenJournal", _
                New SqlParameter("@employeeNo", empNo), _
                New SqlParameter("@amountInterest", amount), _
                New SqlParameter("@particulars", particulars), _
                New SqlParameter("@effectiveDate", effectiveDate), _
                New SqlParameter("@user", user))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub AccountingEntry_SavingsInterestCrediting(ByVal empNo As String, ByVal amount As Decimal, ByVal particulars As String, ByVal effectiveDate As Date, ByVal user As String)
        Try
            ModifiedSqlHelper.ExecuteNonQuery(gcon.cnstring, CommandType.StoredProcedure, "CIMS_InterestCrediting_AccountingEntries_ToGenJournal", _
                New SqlParameter("@employeeNo", empNo), _
                New SqlParameter("@amountInterest", amount), _
                New SqlParameter("@particulars", particulars), _
                New SqlParameter("@effectiveDate", effectiveDate), _
                New SqlParameter("@user", user))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub LoopToAllMemberAndApplyInterest()
        Dim dateCredited As Date = dateint.Value
        Dim desc As String = txtDesc.Text
        Dim keyMember As String
        Dim interestAmt As Decimal
        Dim isClear As Boolean = True
        Dim empNo As String

        For Each xRow As DataGridViewRow In grdInterest.Rows

            empNo = xRow.Cells("Employee ID").Value.ToString()
            keyMember = xRow.Cells("fkEmployee").Value.ToString()
            interestAmt = xRow.Cells("INTEREST CREDITED").Value

            Call Applyinterest_PerMember(dateCredited, desc, keyMember, interestAmt, isClear)
            Call AccountingEntry_SavingsInterestCrediting(empNo, interestAmt, "Interest Crediting", dateCredited, frmMain.username)
        Next

    End Sub
    Private Sub LoopToAllMemberAndApplyInterestTimeDeposit()
        Dim dateCredited As Date = dateint.Value
        Dim desc As String = txtDesc.Text
        Dim keyMember As String
        Dim interestAmt As Decimal
        Dim isClear As Boolean = True
        Dim empNo As String

        For Each xRow As DataGridViewRow In grdInterest.Rows

            empNo = xRow.Cells("Employee ID").Value.ToString()
            keyMember = xRow.Cells("fkEmployee").Value.ToString()
            interestAmt = xRow.Cells("INTEREST CREDITED").Value

            Call ApplyTimeDeposit_Interest(dateCredited, desc, keyMember, interestAmt, isClear, 0)
            Call AccountingEntry_TimeInterestCrediting(empNo, interestAmt, "Interest Crediting", dateCredited, frmMain.username)
        Next



    End Sub
End Class