<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDownloadedPayroll_Delete
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.cboPaycode = New System.Windows.Forms.ComboBox()
        Me.lblPaycode = New System.Windows.Forms.Label()
        Me.lblPaymentDate = New System.Windows.Forms.Label()
        Me.dtePaydate = New System.Windows.Forms.DateTimePicker()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.cboPaycode_fk = New System.Windows.Forms.ComboBox()
        Me.dgvDownloadedPayroll = New System.Windows.Forms.DataGridView()
        Me.PanePanelDeletePayroll = New WindowsApplication2.PanePanel()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btnDelete = New System.Windows.Forms.Button()
        Me.Panel1.SuspendLayout()
        CType(Me.dgvDownloadedPayroll, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanePanelDeletePayroll.SuspendLayout()
        Me.SuspendLayout()
        '
        'cboPaycode
        '
        Me.cboPaycode.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cboPaycode.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPaycode.FormattingEnabled = True
        Me.cboPaycode.Location = New System.Drawing.Point(104, 104)
        Me.cboPaycode.Name = "cboPaycode"
        Me.cboPaycode.Size = New System.Drawing.Size(240, 23)
        Me.cboPaycode.TabIndex = 1
        '
        'lblPaycode
        '
        Me.lblPaycode.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblPaycode.AutoSize = True
        Me.lblPaycode.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPaycode.Location = New System.Drawing.Point(12, 104)
        Me.lblPaycode.Name = "lblPaycode"
        Me.lblPaycode.Size = New System.Drawing.Size(56, 15)
        Me.lblPaycode.TabIndex = 2
        Me.lblPaycode.Text = "Paycode:"
        '
        'lblPaymentDate
        '
        Me.lblPaymentDate.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblPaymentDate.AutoSize = True
        Me.lblPaymentDate.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPaymentDate.Location = New System.Drawing.Point(12, 145)
        Me.lblPaymentDate.Name = "lblPaymentDate"
        Me.lblPaymentDate.Size = New System.Drawing.Size(85, 15)
        Me.lblPaymentDate.TabIndex = 3
        Me.lblPaymentDate.Text = "Payment Date:"
        '
        'dtePaydate
        '
        Me.dtePaydate.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dtePaydate.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtePaydate.Location = New System.Drawing.Point(104, 137)
        Me.dtePaydate.Name = "dtePaydate"
        Me.dtePaydate.Size = New System.Drawing.Size(240, 23)
        Me.dtePaydate.TabIndex = 4
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.White
        Me.Panel1.Controls.Add(Me.cboPaycode)
        Me.Panel1.Controls.Add(Me.cboPaycode_fk)
        Me.Panel1.Controls.Add(Me.lblPaycode)
        Me.Panel1.Controls.Add(Me.lblPaymentDate)
        Me.Panel1.Controls.Add(Me.dtePaydate)
        Me.Panel1.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel1.Location = New System.Drawing.Point(0, -97)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(430, 181)
        Me.Panel1.TabIndex = 37
        '
        'cboPaycode_fk
        '
        Me.cboPaycode_fk.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.cboPaycode_fk.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPaycode_fk.FormattingEnabled = True
        Me.cboPaycode_fk.Location = New System.Drawing.Point(148, 104)
        Me.cboPaycode_fk.Name = "cboPaycode_fk"
        Me.cboPaycode_fk.Size = New System.Drawing.Size(240, 23)
        Me.cboPaycode_fk.TabIndex = 39
        Me.cboPaycode_fk.Visible = False
        '
        'dgvDownloadedPayroll
        '
        Me.dgvDownloadedPayroll.AllowUserToAddRows = False
        Me.dgvDownloadedPayroll.AllowUserToDeleteRows = False
        Me.dgvDownloadedPayroll.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvDownloadedPayroll.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvDownloadedPayroll.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvDownloadedPayroll.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDownloadedPayroll.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.dgvDownloadedPayroll.GridColor = System.Drawing.SystemColors.ActiveBorder
        Me.dgvDownloadedPayroll.Location = New System.Drawing.Point(0, 69)
        Me.dgvDownloadedPayroll.MultiSelect = False
        Me.dgvDownloadedPayroll.Name = "dgvDownloadedPayroll"
        Me.dgvDownloadedPayroll.ReadOnly = True
        Me.dgvDownloadedPayroll.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvDownloadedPayroll.Size = New System.Drawing.Size(356, 248)
        Me.dgvDownloadedPayroll.TabIndex = 38
        '
        'PanePanelDeletePayroll
        '
        Me.PanePanelDeletePayroll.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanelDeletePayroll.Controls.Add(Me.btnCancel)
        Me.PanePanelDeletePayroll.Controls.Add(Me.btnDelete)
        Me.PanePanelDeletePayroll.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PanePanelDeletePayroll.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.PanePanelDeletePayroll.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanelDeletePayroll.InactiveGradientHighColor = System.Drawing.Color.Chartreuse
        Me.PanePanelDeletePayroll.InactiveGradientLowColor = System.Drawing.Color.Green
        Me.PanePanelDeletePayroll.Location = New System.Drawing.Point(0, 317)
        Me.PanePanelDeletePayroll.Name = "PanePanelDeletePayroll"
        Me.PanePanelDeletePayroll.Size = New System.Drawing.Size(356, 40)
        Me.PanePanelDeletePayroll.TabIndex = 36
        '
        'btnCancel
        '
        Me.btnCancel.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Location = New System.Drawing.Point(264, 3)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(87, 27)
        Me.btnCancel.TabIndex = 7
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.Location = New System.Drawing.Point(171, 3)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(87, 27)
        Me.btnDelete.TabIndex = 6
        Me.btnDelete.Text = "Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'frmDownloadedPayroll_Delete
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(356, 357)
        Me.Controls.Add(Me.dgvDownloadedPayroll)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.PanePanelDeletePayroll)
        Me.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmDownloadedPayroll_Delete"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Downloaded Payroll Delete"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.dgvDownloadedPayroll, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanePanelDeletePayroll.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents cboPaycode As System.Windows.Forms.ComboBox
    Friend WithEvents lblPaycode As System.Windows.Forms.Label
    Friend WithEvents lblPaymentDate As System.Windows.Forms.Label
    Friend WithEvents dtePaydate As System.Windows.Forms.DateTimePicker
    Friend WithEvents btnDelete As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents PanePanelDeletePayroll As WindowsApplication2.PanePanel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents dgvDownloadedPayroll As System.Windows.Forms.DataGridView
    Friend WithEvents cboPaycode_fk As System.Windows.Forms.ComboBox
End Class
