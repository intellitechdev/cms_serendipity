﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmAmortGenerator
    'Programmer : Vincent Nacar
    Private con As New Clsappconfiguration
    Dim rd As SqlDataReader
    Dim ctr As Integer = 0

    Private Sub frmAmortGenerator_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        ctr = 0
        lblNumber.Text = "00"
        Me.Text = "Amortization Generator"
    End Sub

    Private Sub frmAmortGenerator_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        GetLoanWO_Amort()
    End Sub

    Private Sub GetLoanWO_Amort()
        Try
            rd = SqlHelper.ExecuteReader(con.cnstring, "_AmortGenerator_GetData")
            While rd.Read
                ctr += 1
                lblNumber.Text = ctr
            End While
        Catch ex As Exception

        End Try
    End Sub

    Private Function isGetData() As Boolean
        Try
            Dim xmode As String = "MONTHLY" 'for BIR Only

            rd = SqlHelper.ExecuteReader(con.cnstring, "_AmortGenerator_GetData")
            While rd.Read
                Me.Text = "Please Wait..."
                If isGenerated(rd(0).ToString,
                               CDec(rd(1)), CDec(rd(2)), CDec(rd(3)), CDate(rd(4)), CDate(rd(5)), xmode) Then
                End If
            End While

            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Function isGenerated(ByVal loanno As String, ByVal principal As Decimal,
                                 ByVal intrate As Decimal,
                                 ByVal terms As Decimal,
                                 ByVal dgranted As Date,
                                 ByVal dfirst As Date,
                                 ByVal a_mode As String) As Boolean
        Try
            SqlHelper.ExecuteNonQuery(con.cnstring, "_Tool_Generate_Amort",
                                       New SqlParameter("@LoanNo", loanno),
                                       New SqlParameter("@PrincipalAmount", principal),
                                       New SqlParameter("@InterestRate", intrate),
                                       New SqlParameter("@Terms", terms),
                                       New SqlParameter("@DateGranted", dgranted),
                                       New SqlParameter("@FirstPayment", dfirst),
                                       New SqlParameter("@xmode", a_mode))
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Sub btnGenerateAmort_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerateAmort.Click
        If isGetData() Then
            Me.Text = "Done."
        End If
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        ctr = 0
        lblNumber.Text = "00"
        Me.Text = "Amortization Generator"
        Me.Close()
    End Sub

End Class