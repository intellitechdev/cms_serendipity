Imports System.Data
Imports System.IO
Imports System.Xml
Imports System.Xml.Xsl
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient


Public Class clsDatasetToExcel

    Public Shared Function SaveXmlSchema(ByVal ds As DataSet) As DataSet
        'Instantiate the DataSet, giving it a custom name
        '(The default name is NewDataSet)
        Dim gcon As New Clsappconfiguration()   

        ds.WriteXmlSchema("C:\ExcelImport.xsd")
        ds.WriteXml("ExcelImport.xml")

        Return ds
    End Function

    Public Sub BuildXMLMap(ByVal ds As DataSet)
        'Create an instance of Excel 2003, add a workbook, 
        'and let the user know what's happening

        Dim xl As New Excel.Application
        Dim xls As New Excel.Application

        With xl
            .Workbooks.Add()
            .ActiveSheet.Name = "Sheet1"
            .Visible = True
            .Range("A1").Value = "Loading the DataSet...."
        End With

        Try
            'Dim sMap As String = System.IO.Path.GetFullPath("ExcelImport.xsd")
            Dim sMap As String = "C:\ExcelImport.xsd"

            'Add the map to the active workbook
            'You can only add a map from a disk file.
            xl.ActiveWorkbook.XmlMaps.Add(sMap, "NewDataSet").Name = "ExcelImport_Map"

            'Specify the cells where the mapped data should go upon import
            Dim map As Microsoft.Office.Interop.Excel.XmlMap = xl.ActiveWorkbook.XmlMaps("ExcelImport_Map")
            xl.Range("A2", "B2").Select()
            Dim list As Microsoft.Office.Interop.Excel.ListObject = CType(xl.ActiveSheet, Microsoft.Office.Interop.Excel.Worksheet).ListObjects.Add
            list.ListColumns(1).XPath.SetValue(map, "/NewDataSet/Table/employeeNo")
            list.ListColumns(2).XPath.SetValue(map, "/NewDataSet/Table/fcName")

            xl.Range("A2").Value = "Employee No"
            xl.Range("B2").Value = "Member"

            With xl.Range("A1", "B1")
                .Select()
                .Merge()
                .Characters.Font.Bold = True
                .Characters.Font.Size = 16
                .HorizontalAlignment = -4108
                .Value = "Non-Existing Members"
            End With

            'Import the XML data
            xl.ActiveWorkbook.XmlMaps("ExcelImport_Map").ImportXml(ds.GetXml)

            'Open the XML Source Task Pane
            xl.DisplayXMLSourcePane(map)

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub

End Class
