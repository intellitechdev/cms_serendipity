Imports System.Data.SqlClient
Public Class frmOrgChart_Edit
    Dim orgoption As String
    Dim dtLevelDefinition, dtloadparent As DataTable
    Dim errmsg As String
    Dim Orglevel As Integer
    Dim orglevelcode, orgleveldesc, orgID As String
    Dim parentdesc, parentcode As String
    Dim parent_ID As String


    Public Property GetOrgCode() As String
        Get
            GetOrgCode = orglevelcode
        End Get
        Set(ByVal value As String)
            orglevelcode = value
        End Set
    End Property
    Public Property GetOrgDesc() As String
        Get
            GetOrgDesc = orgleveldesc
        End Get
        Set(ByVal value As String)
            orgleveldesc = value
        End Set
    End Property

    Public Property GetOrgID() As String
        Get
            GetOrgID = orgID
        End Get
        Set(ByVal value As String)
            orgID = value
        End Set
    End Property

    Public Property GetOrgLevel() As Integer
        Get
            GetOrgLevel = Orglevel
        End Get
        Set(ByVal value As Integer)
            Orglevel = value
        End Set
    End Property

    Public Property GetParentDescription() As String
        Get
            GetParentDescription = parentdesc
        End Get
        Set(ByVal value As String)
            parentdesc = value
        End Set
    End Property
    Public Enum selection
        AddOrg
        EditOrg
    End Enum

    Public Sub New(ByVal selection As selection)
        MyBase.New()
        InitializeComponent()
        orgoption = selection
    End Sub

    Public Sub initdisplay()
        Select Case orgoption
            Case selection.AddOrg
                Me.Text = "Org Structure"
                Me.lblLAbel.Text = "     Add Organization Item"

            Case selection.EditOrg
                Me.Text = "Org Structure"
                Me.lblLAbel.Text = "     Edit Organization Item"

        End Select
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.DialogResult = Windows.Forms.DialogResult.OK
    End Sub

    Private Sub EditOrg_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Select Case orgoption
            Case selection.AddOrg
                initdisplay()
                loadOrgDefinition()

            Case selection.EditOrg
                Me.cmbLevel.Enabled = False
                Me.cmbParentDesc.Enabled = False

                Me.cmbLevel.Text = Orglevel
                Me.txtCode.Text = orglevelcode
                Me.txtDesc.Text = orgleveldesc


        End Select

    End Sub

    Public Sub loadOrgDefinition()
        Dim loadER As New clsPersonnel
        dtLevelDefinition = loadER.LoadAllLevelDefinition(errmsg)
        If errmsg <> "" Then
            MessageBox.Show(errmsg, "Load Level", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Else
            Me.cmbLevel.SelectedIndex = -1
            Me.cmbLevel.Items.Clear()
            Me.cmbLevel.LoadingType = MTGCComboBox.CaricamentoCombo.DataTable
            Me.cmbLevel.SourceDataString = New String() {"OrgLevel", "OrgLevelCode", "OrgLevelDescription"}
            Me.cmbLevel.SourceDataTable = dtLevelDefinition
        End If
    End Sub


    Private Sub cmbLevel_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cmbLevel.KeyPress
        e.KeyChar = Microsoft.VisualBasic.ChrW(0)
    End Sub

    Private Sub cmbLevel_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbLevel.SelectedIndexChanged
        If Not Me.cmbLevel.SelectedItem Is Nothing Then
            Me.Orglevel = Me.cmbLevel.SelectedItem.col1
            Me.orglevelcode = Me.cmbLevel.SelectedItem.col2
            Me.orgleveldesc = Me.cmbLevel.SelectedItem.col3

            If Me.Orglevel = 0 Then
                Me.cmbParentDesc.Enabled = False
                Me.cmbParentDesc.SelectedIndex = -1
            Else
                Me.cmbParentDesc.Enabled = True
                loadparentcode()

            End If
        Else
            Me.Orglevel = 0
            orglevelcode = ""
            Me.orgleveldesc = ""
        End If
    End Sub
    Public Sub loadparentcode()

        Dim loadparentcode As New clsPersonnel
        dtloadparent = loadparentcode.LoadParentCode(Orglevel - 1, errmsg)
        If errmsg <> "" Then
            MessageBox.Show(errmsg, "Load Parent", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Else
            Me.cmbParentDesc.SelectedIndex = -1
            Me.cmbParentDesc.Items.Clear()
            Me.cmbParentDesc.LoadingType = MTGCComboBox.CaricamentoCombo.DataTable
            'Me.cmbParentDesc.SourceDataString = New String() {"Code", "Description", "OrgID"}
            Me.cmbParentDesc.SourceDataString = New String() {"OrgID", "Description", "OrgID"}
            Me.cmbParentDesc.SourceDataTable = dtloadparent
        End If

    End Sub
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Call checklevelandcode(Me.Orglevel, Me.txtCode.Text)
    End Sub

    Private Sub cmbParentDesc_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cmbParentDesc.KeyPress
        e.KeyChar = Microsoft.VisualBasic.ChrW(0)
    End Sub

    Private Sub cmbParentDesc_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbParentDesc.SelectedIndexChanged
        If Not Me.cmbParentDesc.SelectedItem Is Nothing Then
            Me.parentdesc = Me.cmbParentDesc.SelectedItem.col1
            Me.parentcode = Me.cmbParentDesc.SelectedItem.col2
            Me.parent_ID = Me.cmbParentDesc.SelectedItem.col3

        Else
            Me.parentcode = ""
            parentdesc = ""
            Me.parent_id = ""
        End If
    End Sub
#Region "Check for duplication in level and code"
    Private Sub checklevelandcode(ByVal level As String, ByVal code As String)
        'Dim myconnection As New Clsappconfiguration
        'Dim cmd As New SqlCommand("usp_per_orgchart_check_select", myconnection.sqlconn)
        'cmd.CommandType = CommandType.StoredProcedure
        'myconnection.sqlconn.Open()
        'With cmd.Parameters
        '    .Add("@orglevel", SqlDbType.Int).Value = level
        '    .Add("@code", SqlDbType.VarChar, 50).Value = code
        'End With
        'Dim myreader As SqlDataReader

        'myreader = cmd.ExecuteReader
        'Try
        '    If myreader.HasRows Then
        '        If level <> code Then
        '            MessageBox.Show("Level and Code already exist please change your code", "Duplicate Level and Code", MessageBoxButtons.OK, MessageBoxIcon.Information)
        '        End If
        '    Else
        Dim add As New clsPersonnel
        Select Case orgoption
            Case selection.AddOrg
                If Me.cmbLevel.Text <> "" And Me.txtDesc.Text <> "" Then 'Then me.txtCode.Text <>"" 
                    If parent_ID = "" Then
                        parent_ID = "000"
                    End If

                    If MessageBox.Show("Are you sure you want to save this item?", "Save", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                        add.OrgMaintenance(1, Me.Orglevel, Me.txtCode.Text, Me.txtDesc.Text, Me.parent_ID, 0, errmsg)
                        If errmsg <> "" Then
                            MessageBox.Show(errmsg, "Save", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Else
                            MessageBox.Show("Item Successfully Saved!", "Save", MessageBoxButtons.OK, MessageBoxIcon.Information)
                            Me.GroupBox1.Enabled = False
                            Me.btnSave.Enabled = False
                        End If
                    End If
                Else
                    MessageBox.Show("PLease check you inputs!", "Save", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If

                'Case selection.EditOrg
                '        If Me.txtCode.Text <> "" And Me.txtDesc.Text <> "" Then
                '            If MessageBox.Show("Are you sure you want to save this item?", "Save", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                '                add.UpdateCompany(Me.Orglevel, Me.txtCode.Text, Me.txtDesc.Text)



                '                add.OrgMaintenance(2, Me.Orglevel, Me.txtCode.Text, Me.txtDesc.Text, 0, Me.orgID, errmsg)
                '                If errmsg <> "" Then
                '                    MessageBox.Show(errmsg, "Save", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                '                Else
                '                    MessageBox.Show("Item Successfully Updated!", "Save", MessageBoxButtons.OK, MessageBoxIcon.Information)
                '                    Me.GroupBox1.Enabled = False
                '                    Me.btnSave.Enabled = False
                '                End If
                '            End If
                '        End If

        End Select
        '    End If
        'myreader.Close()
        'Finally
        '    myconnection.sqlconn.Close()
        'End Try
    End Sub
#End Region

    Private Sub btnupdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupdate.Click
        Dim Edit As New clsPersonnel
        Select Case orgoption
            Case selection.EditOrg
                If Me.txtCode.Text <> "" And Me.txtDesc.Text <> "" Then
                    If MessageBox.Show("Are you sure you want to update this item?", "Update", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                        Edit.OrgMaintenance(2, Me.Orglevel, Me.txtCode.Text, Me.txtDesc.Text, 0, Me.orgID, errmsg)
                        If errmsg <> "" Then
                            MessageBox.Show(errmsg, "Update", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Else
                            MessageBox.Show("Item Successfully Updated!", "Update", MessageBoxButtons.OK, MessageBoxIcon.Information)
                            Me.GroupBox1.Enabled = False
                            Me.btnupdate.Enabled = False
                        End If
                    End If
                End If
        End Select
        Me.Close()
    End Sub
End Class