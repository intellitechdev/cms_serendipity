﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmBrowseInvestmentApprover

    Private con As New Clsappconfiguration
    Dim ds As DataSet

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Function isInvestmentLoad() As Boolean
        Try
            cboInvestmentType.Items.Clear()
            Dim rd As SqlDataReader
            rd = SqlHelper.ExecuteReader(con.cnstring, "_InvestmentApplication_Select_Investment")
            While rd.Read
                cboInvestmentType.Items.Add(rd(0).ToString)
            End While
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Sub frmBrowseInvestmentApprover_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If isInvestmentLoad() Then

        End If
    End Sub

    Private Function isLoaded_Approver(ByVal investmentType As String) As Boolean
        Try
            ds = SqlHelper.ExecuteDataset(con.cnstring, "_InvestmentApproval_Select_Approver",
                                           New SqlParameter("@InvestmentType", investmentType))
            dgvList.DataSource = ds.Tables(0)
            dgvList.Columns(0).Visible = False
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Sub cboInvestmentType_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboInvestmentType.SelectedValueChanged
        If isLoaded_Approver(cboInvestmentType.Text) Then

        End If
    End Sub

    Private Function isSelected() As Boolean
        Try
            frmInvestmentApproval.lblInvestmentType.Text = cboInvestmentType.Text
            frmInvestmentApproval.fkemployee = dgvList.SelectedRows(0).Cells(0).Value.ToString
            frmInvestmentApproval.txtApprover.Text = dgvList.SelectedRows(0).Cells(1).Value.ToString
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Sub btnSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelect.Click
        If isSelected() Then
            CloseObj()
            Me.Close()
        End If
    End Sub

    Private Sub dgvList_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvList.CellDoubleClick
        If isSelected() Then
            CloseObj()
            Me.Close()
        End If
    End Sub

    Private Sub CloseObj()
        dgvList.Columns.Clear()
        dgvList.DataSource = Nothing
    End Sub

End Class