<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMembershipRequirements
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMembershipRequirements))
        Me.btnclose = New System.Windows.Forms.Button()
        Me.btnsave = New System.Windows.Forms.Button()
        Me.btndelete = New System.Windows.Forms.Button()
        Me.btnupdate = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtdescription_Rqts = New System.Windows.Forms.TextBox()
        Me.listMembershipRequirements_Rqts = New System.Windows.Forms.ListView()
        Me.PanePanel1 = New WindowsApplication2.PanePanel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnSave_rqts = New System.Windows.Forms.Button()
        Me.btnUpdate_rqts = New System.Windows.Forms.Button()
        Me.btnDelete_rqts = New System.Windows.Forms.Button()
        Me.btnClose_rqts = New System.Windows.Forms.Button()
        Me.PanePanel3 = New WindowsApplication2.PanePanel()
        Me.PanePanel1.SuspendLayout()
        Me.PanePanel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnclose
        '
        Me.btnclose.Image = CType(resources.GetObject("btnclose.Image"), System.Drawing.Image)
        Me.btnclose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnclose.Location = New System.Drawing.Point(218, 6)
        Me.btnclose.Name = "btnclose"
        Me.btnclose.Size = New System.Drawing.Size(66, 28)
        Me.btnclose.TabIndex = 5
        Me.btnclose.Text = "Close"
        Me.btnclose.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnclose.UseVisualStyleBackColor = True
        '
        'btnsave
        '
        Me.btnsave.Image = Global.WindowsApplication2.My.Resources.Resources.new3
        Me.btnsave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnsave.Location = New System.Drawing.Point(7, 6)
        Me.btnsave.Name = "btnsave"
        Me.btnsave.Size = New System.Drawing.Size(66, 28)
        Me.btnsave.TabIndex = 2
        Me.btnsave.Text = "New"
        Me.btnsave.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnsave.UseVisualStyleBackColor = True
        '
        'btndelete
        '
        Me.btndelete.Image = CType(resources.GetObject("btndelete.Image"), System.Drawing.Image)
        Me.btndelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btndelete.Location = New System.Drawing.Point(141, 6)
        Me.btndelete.Name = "btndelete"
        Me.btndelete.Size = New System.Drawing.Size(66, 28)
        Me.btndelete.TabIndex = 4
        Me.btndelete.Text = "Delete"
        Me.btndelete.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btndelete.UseVisualStyleBackColor = True
        '
        'btnupdate
        '
        Me.btnupdate.Image = CType(resources.GetObject("btnupdate.Image"), System.Drawing.Image)
        Me.btnupdate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnupdate.Location = New System.Drawing.Point(74, 6)
        Me.btnupdate.Name = "btnupdate"
        Me.btnupdate.Size = New System.Drawing.Size(66, 28)
        Me.btnupdate.TabIndex = 3
        Me.btnupdate.Text = "Edit"
        Me.btnupdate.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnupdate.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(12, 39)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(70, 13)
        Me.Label3.TabIndex = 28
        Me.Label3.Text = "Requirement;"
        '
        'txtdescription_Rqts
        '
        Me.txtdescription_Rqts.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtdescription_Rqts.Location = New System.Drawing.Point(84, 35)
        Me.txtdescription_Rqts.MaxLength = 50
        Me.txtdescription_Rqts.Name = "txtdescription_Rqts"
        Me.txtdescription_Rqts.Size = New System.Drawing.Size(215, 20)
        Me.txtdescription_Rqts.TabIndex = 29
        '
        'listMembershipRequirements_Rqts
        '
        Me.listMembershipRequirements_Rqts.Location = New System.Drawing.Point(8, 61)
        Me.listMembershipRequirements_Rqts.Name = "listMembershipRequirements_Rqts"
        Me.listMembershipRequirements_Rqts.Size = New System.Drawing.Size(305, 224)
        Me.listMembershipRequirements_Rqts.TabIndex = 27
        Me.listMembershipRequirements_Rqts.UseCompatibleStateImageBehavior = False
        '
        'PanePanel1
        '
        Me.PanePanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel1.Controls.Add(Me.Label1)
        Me.PanePanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanePanel1.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.PanePanel1.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel1.InactiveGradientHighColor = System.Drawing.Color.Green
        Me.PanePanel1.InactiveGradientLowColor = System.Drawing.Color.GreenYellow
        Me.PanePanel1.Location = New System.Drawing.Point(0, 0)
        Me.PanePanel1.Name = "PanePanel1"
        Me.PanePanel1.Size = New System.Drawing.Size(322, 29)
        Me.PanePanel1.TabIndex = 26
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(3, 5)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(214, 19)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Membership Requirements"
        '
        'btnSave_rqts
        '
        Me.btnSave_rqts.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave_rqts.Image = Global.WindowsApplication2.My.Resources.Resources.new3
        Me.btnSave_rqts.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSave_rqts.Location = New System.Drawing.Point(7, 3)
        Me.btnSave_rqts.Name = "btnSave_rqts"
        Me.btnSave_rqts.Size = New System.Drawing.Size(66, 28)
        Me.btnSave_rqts.TabIndex = 30
        Me.btnSave_rqts.Text = "New"
        Me.btnSave_rqts.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSave_rqts.UseVisualStyleBackColor = True
        '
        'btnUpdate_rqts
        '
        Me.btnUpdate_rqts.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUpdate_rqts.Image = CType(resources.GetObject("btnUpdate_rqts.Image"), System.Drawing.Image)
        Me.btnUpdate_rqts.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUpdate_rqts.Location = New System.Drawing.Point(74, 3)
        Me.btnUpdate_rqts.Name = "btnUpdate_rqts"
        Me.btnUpdate_rqts.Size = New System.Drawing.Size(66, 28)
        Me.btnUpdate_rqts.TabIndex = 31
        Me.btnUpdate_rqts.Text = "Edit"
        Me.btnUpdate_rqts.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnUpdate_rqts.UseVisualStyleBackColor = True
        '
        'btnDelete_rqts
        '
        Me.btnDelete_rqts.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete_rqts.Image = CType(resources.GetObject("btnDelete_rqts.Image"), System.Drawing.Image)
        Me.btnDelete_rqts.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDelete_rqts.Location = New System.Drawing.Point(141, 3)
        Me.btnDelete_rqts.Name = "btnDelete_rqts"
        Me.btnDelete_rqts.Size = New System.Drawing.Size(66, 28)
        Me.btnDelete_rqts.TabIndex = 32
        Me.btnDelete_rqts.Text = "Delete"
        Me.btnDelete_rqts.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnDelete_rqts.UseVisualStyleBackColor = True
        '
        'btnClose_rqts
        '
        Me.btnClose_rqts.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose_rqts.Image = CType(resources.GetObject("btnClose_rqts.Image"), System.Drawing.Image)
        Me.btnClose_rqts.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnClose_rqts.Location = New System.Drawing.Point(208, 3)
        Me.btnClose_rqts.Name = "btnClose_rqts"
        Me.btnClose_rqts.Size = New System.Drawing.Size(61, 28)
        Me.btnClose_rqts.TabIndex = 33
        Me.btnClose_rqts.Text = "Close"
        Me.btnClose_rqts.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClose_rqts.UseVisualStyleBackColor = True
        '
        'PanePanel3
        '
        Me.PanePanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel3.Controls.Add(Me.btnSave_rqts)
        Me.PanePanel3.Controls.Add(Me.btnClose_rqts)
        Me.PanePanel3.Controls.Add(Me.btnUpdate_rqts)
        Me.PanePanel3.Controls.Add(Me.btnDelete_rqts)
        Me.PanePanel3.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PanePanel3.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.PanePanel3.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel3.InactiveGradientHighColor = System.Drawing.Color.Green
        Me.PanePanel3.InactiveGradientLowColor = System.Drawing.Color.GreenYellow
        Me.PanePanel3.Location = New System.Drawing.Point(0, 290)
        Me.PanePanel3.Name = "PanePanel3"
        Me.PanePanel3.Size = New System.Drawing.Size(322, 36)
        Me.PanePanel3.TabIndex = 34
        '
        'frmMembershipRequirements
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(322, 326)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.PanePanel1)
        Me.Controls.Add(Me.txtdescription_Rqts)
        Me.Controls.Add(Me.listMembershipRequirements_Rqts)
        Me.Controls.Add(Me.PanePanel3)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMembershipRequirements"
        Me.ShowInTaskbar = False
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "New Member Requirements"
        Me.PanePanel1.ResumeLayout(False)
        Me.PanePanel1.PerformLayout()
        Me.PanePanel3.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PanePanel2 As WindowsApplication2.PanePanel
    Friend WithEvents btnclose As System.Windows.Forms.Button
    Friend WithEvents btnsave As System.Windows.Forms.Button
    Friend WithEvents btndelete As System.Windows.Forms.Button
    Friend WithEvents btnupdate As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents PanePanel1 As WindowsApplication2.PanePanel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtdescription_Rqts As System.Windows.Forms.TextBox
    Friend WithEvents listMembershipRequirements_Rqts As System.Windows.Forms.ListView
    Friend WithEvents btnSave_rqts As System.Windows.Forms.Button
    Friend WithEvents btnUpdate_rqts As System.Windows.Forms.Button
    Friend WithEvents btnDelete_rqts As System.Windows.Forms.Button
    Friend WithEvents btnClose_rqts As System.Windows.Forms.Button
    Friend WithEvents PanePanel3 As WindowsApplication2.PanePanel
End Class
