﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmBrowseBank
    'Coded by : Vincent Nacar 

    Dim gcon As New Clsappconfiguration
    Public bank As String

    Private Sub frmBrowseBank_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LoadBank()
    End Sub

    Private Sub LoadBank()
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(gcon.cnstring, "_Select_BankAccount")

        dgvList.DataSource = ds.Tables(0)
        dgvList.Columns(0).Visible = False
    End Sub

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        bank = Me.dgvList.SelectedRows(0).Cells(1).Value.ToString
        GetCellValue()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub dgvList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvList.Click
        bank = Me.dgvList.SelectedRows(0).Cells(1).Value.ToString
        GetCellValue()
    End Sub

    Private Sub GetCellValue()
        Try
            'Dim i As Integer = frmCashier.grdListofEntries.CurrentRow.Cells.Add   
            'frmCashier.grdListofEntries.Item("Bank", i).Value = Me.dgvList.SelectedRows(0).Cells(1).Value.ToString()
            'frmCashier.grdListofEntries.Rows(0).Cells(0).Value = Me.dgvList.SelectedRows(0).Cells(1).Value.ToString()

            Me.Close()
        Catch ex As Exception
            MessageBox.Show("Error :" + ex.ToString)
        End Try
    End Sub
End Class