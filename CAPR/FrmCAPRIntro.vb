Public Class FrmCAPRIntro
    Public xParent As frmCAPRViewer

    Private Sub BtnNextOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnNextOK.Click
        If txtAsOfYear.Text = "" Then
            MsgBox("Please, type the date of this Cooperative Annual Performance Report.", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Invalid Date")
            txtAsOfYear.SelectAll()
            Exit Sub
        End If
        If txtAsOfYear.TextLength < 4 Then
            MsgBox("Please, type the correct year.", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Invalid Year format")
            txtAsOfYear.SelectAll()
            Exit Sub
        End If
        ReceivedBy = IIf(txtReceivedBy.Text = "", " ", txtReceivedBy.Text)
        ValidateBy = IIf(txtValidatedby.Text = "", " ", txtValidatedby.Text)
        EncodedBy = IIf(txtEncodedBy.Text = "", " ", txtEncodedBy.Text)
        Verifiedby = IIf(txtVerified.Text = "", " ", txtVerified.Text)
        DateReceived = dtReceived.Value.Date
        DateValidated = dtValidated.Value.Date
        DateEncoded = dtEncoded.Value.Date
        DateVerified = dtVerified.Value.Date
        AsOfDateYear = CDec(txtAsOfYear.Text)
        Me.Hide()
        xFormSec1.Show()
    End Sub

    Private Sub txtAsOfYear_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtAsOfYear.KeyPress
        If Not (e.KeyChar.IsDigit(e.KeyChar)) And _
        e.KeyChar <> ChrW(Keys.Back) Then
            e.Handled = True
        End If
    End Sub

    Private Sub FrmCAPRIntro_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub BtnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Me.Close()
    End Sub
End Class