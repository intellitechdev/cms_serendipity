﻿Imports System.Data.Sql
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmMember_EducationalInfo

    Dim pkSchools As String

#Region "Property"
    Public Property getpkSchools() As String
        Get
            Return pkSchools
        End Get
        Set(ByVal value As String)
            pkSchools = value
        End Set
    End Property
#End Region

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        Me.Close()
    End Sub

    Private Sub AddEditEducInfo(ByVal EmpNo As String, ByVal School As String, ByVal Degree As String, ByVal dtfrom As String, ByVal dtTo As String,
                                ByVal Specify As String, ByVal Course As String, ByVal Award As String, ByVal pk_Schools As String)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_MemberEducationalInformation_AddEdit", _
                                      New SqlParameter("@EmployeeNo", EmpNo), _
                                      New SqlParameter("@fcSchool", School), _
                                      New SqlParameter("@fcDegree", Degree), _
                                      New SqlParameter("@fcFrom", dtfrom), _
                                      New SqlParameter("@fcTo", dtTo), _
                                      New SqlParameter("@fcSpecify", Specify), _
                                      New SqlParameter("@fcCourse", Course), _
                                      New SqlParameter("@fcAwards", Award), _
                                      New SqlParameter("@pk_Schools", pk_Schools))
            trans.Commit()
            MessageBox.Show("Record has been Saved", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "Save Educational Information")
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If txtSchool.Text = "" And txtAwards.Text = "" And txtFrom.Text = "" And cboDegree.Text = "" And cboSpecify.Text = "" And txtCourse.Text = "" Then
            MessageBox.Show("No Information to Save", "Educational Information", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        Else
            Call AddEditEducInfo(frmMember_Master.txtEmployeeNo.Text.Trim, txtSchool.Text.Trim, cboDegree.Text.Trim, txtFrom.Text.Trim,
                                 txtTo.Text.Trim, cboSpecify.Text.Trim, txtCourse.Text.Trim, txtAwards.Text.Trim, "")
            Call frmMember_Master.GetmemberEducationalInfo(frmMember_Master.txtEmployeeNo.Text)
            ClearText()
            'Me.Close()
        End If
    End Sub

    Private Sub btnupdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupdate.Click
        Call AddEditEducInfo(frmMember_Master.txtEmployeeNo.Text.Trim, txtSchool.Text.Trim, cboDegree.Text.Trim, txtFrom.Text.Trim,
                                 txtTo.Text.Trim, cboSpecify.Text.Trim, txtCourse.Text.Trim, txtAwards.Text.Trim, pkSchools)
        Call frmMember_Master.GetmemberEducationalInfo(frmMember_Master.txtEmployeeNo.Text)
        Me.Close()
    End Sub

    Private Sub cboDegree_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboDegree.SelectedValueChanged
        cboSpecify.Enabled = True
    End Sub

    Private Sub txtFrom_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtFrom.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                MessageBox.Show("Invalid Input! This field Allow 0-9 Only.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub txtTo_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtTo.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                MessageBox.Show("Invalid Input! This field Allow 0-9 Only.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub ClearText()
        txtAwards.Text = ""
        txtCourse.Text = ""
        txtFrom.Text = ""
        txtSchool.Text = ""
        txtTo.Text = ""
        cboDegree.Text = ""
        cboSpecify.Text = ""
    End Sub
End Class