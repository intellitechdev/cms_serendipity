<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLoan_ApplicationMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLoan_ApplicationMain))
        Me.cmdSearch = New System.Windows.Forms.Button()
        Me.txtSearch = New System.Windows.Forms.TextBox()
        Me.panelStep1 = New System.Windows.Forms.Panel()
        Me.cboProject = New System.Windows.Forms.ComboBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtnoofpayment = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.cboModeofPayment = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.btnCheckLoanNo = New System.Windows.Forms.Button()
        Me.txtLoanNo = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cboRestLoanNo = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtRestructure = New System.Windows.Forms.TextBox()
        Me.picStep1 = New WindowsApplication2.PanePanel()
        Me.lblStepOne = New System.Windows.Forms.Label()
        Me.cmdCancel = New System.Windows.Forms.Button()
        Me.cmdDone = New System.Windows.Forms.Button()
        Me.lblTerms = New System.Windows.Forms.Label()
        Me.chkPreTerminate = New System.Windows.Forms.CheckBox()
        Me.cboTerms = New System.Windows.Forms.ComboBox()
        Me.cboLoanType = New System.Windows.Forms.ComboBox()
        Me.lblChooseLoanType = New System.Windows.Forms.Label()
        Me.grpClassification = New System.Windows.Forms.GroupBox()
        Me.rdRestructure = New System.Windows.Forms.RadioButton()
        Me.rdCarryForward = New System.Windows.Forms.RadioButton()
        Me.optNew = New System.Windows.Forms.RadioButton()
        Me.optRestructure = New System.Windows.Forms.RadioButton()
        Me.TxtCoopLoan = New System.Windows.Forms.TextBox()
        Me.btnPrint = New System.Windows.Forms.Button()
        Me.panelstep2 = New System.Windows.Forms.Panel()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.dgvStandardIntMethod = New System.Windows.Forms.DataGridView()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.dgvCustomizedIntMethod = New System.Windows.Forms.DataGridView()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cboMonthDeduction = New System.Windows.Forms.ComboBox()
        Me.picStep2 = New WindowsApplication2.PanePanel()
        Me.lblSteptwo = New System.Windows.Forms.Label()
        Me.cmdContinue = New System.Windows.Forms.Button()
        Me.cmdBack = New System.Windows.Forms.Button()
        Me.txtPhilHealth = New System.Windows.Forms.TextBox()
        Me.lblPhilHealth = New System.Windows.Forms.Label()
        Me.grpMaxLoanableAmount = New System.Windows.Forms.GroupBox()
        Me.txtMaxLoanAmount = New System.Windows.Forms.TextBox()
        Me.txtCapacityToPay = New System.Windows.Forms.TextBox()
        Me.txtContribution = New System.Windows.Forms.TextBox()
        Me.cmdShowComputation = New System.Windows.Forms.Button()
        Me.lblNote = New System.Windows.Forms.Label()
        Me.lblCapacityToPay = New System.Windows.Forms.Label()
        Me.btnComputeMaxLoan = New System.Windows.Forms.Button()
        Me.lblContribution = New System.Windows.Forms.Label()
        Me.lblRate = New System.Windows.Forms.Label()
        Me.txtRateMonthly = New System.Windows.Forms.TextBox()
        Me.txtOtherNonRecurring = New System.Windows.Forms.TextBox()
        Me.txtCompanyLoan = New System.Windows.Forms.TextBox()
        Me.txtPagibigLoan = New System.Windows.Forms.TextBox()
        Me.txtSSSLoan = New System.Windows.Forms.TextBox()
        Me.lblOtherNonRecurring = New System.Windows.Forms.Label()
        Me.lblCompanyLoan = New System.Windows.Forms.Label()
        Me.lblPagibigLoan = New System.Windows.Forms.Label()
        Me.lblSSSLoan = New System.Windows.Forms.Label()
        Me.lstname = New System.Windows.Forms.ListBox()
        Me.panelStep3 = New System.Windows.Forms.Panel()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btnDeleteRow = New System.Windows.Forms.Button()
        Me.dgvComakerList = New System.Windows.Forms.DataGridView()
        Me.btnComaker = New System.Windows.Forms.Button()
        Me.picStep3 = New WindowsApplication2.PanePanel()
        Me.lblStepThree = New System.Windows.Forms.Label()
        Me.grpNecessaryInfo = New System.Windows.Forms.GroupBox()
        Me.btnshowpenalties = New System.Windows.Forms.Button()
        Me.cmdNo = New System.Windows.Forms.Button()
        Me.chkPenalties = New System.Windows.Forms.CheckBox()
        Me.btncompute1 = New System.Windows.Forms.Button()
        Me.txtPrincipalAmount = New System.Windows.Forms.TextBox()
        Me.dtpLoanApplicationDate = New System.Windows.Forms.DateTimePicker()
        Me.lblPrincipalAmount = New System.Windows.Forms.Label()
        Me.lblLoanApplicationDate = New System.Windows.Forms.Label()
        Me.grpSearchOption = New System.Windows.Forms.GroupBox()
        Me.txtSearchName = New System.Windows.Forms.TextBox()
        Me.optName = New System.Windows.Forms.RadioButton()
        Me.optNumber = New System.Windows.Forms.RadioButton()
        Me.lstNumbers = New System.Windows.Forms.ListBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.lstMembers = New System.Windows.Forms.ListView()
        Me.PanePanel3 = New WindowsApplication2.PanePanel()
        Me.btnAccounts = New System.Windows.Forms.Button()
        Me.btnRefresh = New System.Windows.Forms.Button()
        Me.btnSearch = New System.Windows.Forms.Button()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.PanePanel5 = New WindowsApplication2.PanePanel()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.panelStep1.SuspendLayout()
        Me.picStep1.SuspendLayout()
        Me.grpClassification.SuspendLayout()
        Me.panelstep2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.dgvStandardIntMethod, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        CType(Me.dgvCustomizedIntMethod, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.picStep2.SuspendLayout()
        Me.grpMaxLoanableAmount.SuspendLayout()
        Me.panelStep3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.dgvComakerList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.picStep3.SuspendLayout()
        Me.grpNecessaryInfo.SuspendLayout()
        Me.grpSearchOption.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.PanePanel3.SuspendLayout()
        Me.PanePanel5.SuspendLayout()
        Me.SuspendLayout()
        '
        'cmdSearch
        '
        Me.cmdSearch.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdSearch.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdSearch.Location = New System.Drawing.Point(199, 13)
        Me.cmdSearch.Name = "cmdSearch"
        Me.cmdSearch.Size = New System.Drawing.Size(61, 23)
        Me.cmdSearch.TabIndex = 1
        Me.cmdSearch.Text = "Search"
        Me.cmdSearch.UseVisualStyleBackColor = True
        '
        'txtSearch
        '
        Me.txtSearch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearch.Location = New System.Drawing.Point(22, 145)
        Me.txtSearch.Name = "txtSearch"
        Me.txtSearch.Size = New System.Drawing.Size(144, 21)
        Me.txtSearch.TabIndex = 22
        '
        'panelStep1
        '
        Me.panelStep1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panelStep1.Controls.Add(Me.cboProject)
        Me.panelStep1.Controls.Add(Me.Label12)
        Me.panelStep1.Controls.Add(Me.txtnoofpayment)
        Me.panelStep1.Controls.Add(Me.Label5)
        Me.panelStep1.Controls.Add(Me.cboModeofPayment)
        Me.panelStep1.Controls.Add(Me.Label4)
        Me.panelStep1.Controls.Add(Me.btnCheckLoanNo)
        Me.panelStep1.Controls.Add(Me.txtLoanNo)
        Me.panelStep1.Controls.Add(Me.Label2)
        Me.panelStep1.Controls.Add(Me.cboRestLoanNo)
        Me.panelStep1.Controls.Add(Me.Label1)
        Me.panelStep1.Controls.Add(Me.txtRestructure)
        Me.panelStep1.Controls.Add(Me.picStep1)
        Me.panelStep1.Controls.Add(Me.cmdCancel)
        Me.panelStep1.Controls.Add(Me.cmdDone)
        Me.panelStep1.Controls.Add(Me.lblTerms)
        Me.panelStep1.Controls.Add(Me.chkPreTerminate)
        Me.panelStep1.Controls.Add(Me.cboTerms)
        Me.panelStep1.Controls.Add(Me.cboLoanType)
        Me.panelStep1.Controls.Add(Me.lblChooseLoanType)
        Me.panelStep1.Controls.Add(Me.grpClassification)
        Me.panelStep1.Controls.Add(Me.TxtCoopLoan)
        Me.panelStep1.Controls.Add(Me.btnPrint)
        Me.panelStep1.Location = New System.Drawing.Point(275, 6)
        Me.panelStep1.Name = "panelStep1"
        Me.panelStep1.Size = New System.Drawing.Size(876, 157)
        Me.panelStep1.TabIndex = 23
        '
        'cboProject
        '
        Me.cboProject.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cboProject.FormattingEnabled = True
        Me.cboProject.Location = New System.Drawing.Point(67, 32)
        Me.cboProject.Name = "cboProject"
        Me.cboProject.Size = New System.Drawing.Size(265, 21)
        Me.cboProject.TabIndex = 55
        Me.cboProject.Text = "Select Project"
        '
        'Label12
        '
        Me.Label12.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(14, 36)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(47, 13)
        Me.Label12.TabIndex = 54
        Me.Label12.Text = "Project"
        '
        'txtnoofpayment
        '
        Me.txtnoofpayment.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtnoofpayment.Location = New System.Drawing.Point(414, 118)
        Me.txtnoofpayment.Name = "txtnoofpayment"
        Me.txtnoofpayment.ReadOnly = True
        Me.txtnoofpayment.Size = New System.Drawing.Size(60, 21)
        Me.txtnoofpayment.TabIndex = 38
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(411, 104)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(95, 13)
        Me.Label5.TabIndex = 37
        Me.Label5.Text = "No. of Payment"
        '
        'cboModeofPayment
        '
        Me.cboModeofPayment.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cboModeofPayment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboModeofPayment.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboModeofPayment.FormattingEnabled = True
        Me.cboModeofPayment.Items.AddRange(New Object() {"DAILY", "WEEKLY", "SEMI-MONTHLY", "MONTHLY", "QUARTERLY", "ANNUALLY", "LUMP SUM"})
        Me.cboModeofPayment.Location = New System.Drawing.Point(248, 119)
        Me.cboModeofPayment.Name = "cboModeofPayment"
        Me.cboModeofPayment.Size = New System.Drawing.Size(146, 21)
        Me.cboModeofPayment.TabIndex = 36
        '
        'Label4
        '
        Me.Label4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(249, 104)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(106, 13)
        Me.Label4.TabIndex = 35
        Me.Label4.Text = "Mode of Payment"
        '
        'btnCheckLoanNo
        '
        Me.btnCheckLoanNo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCheckLoanNo.Location = New System.Drawing.Point(765, 52)
        Me.btnCheckLoanNo.Name = "btnCheckLoanNo"
        Me.btnCheckLoanNo.Size = New System.Drawing.Size(44, 23)
        Me.btnCheckLoanNo.TabIndex = 34
        Me.btnCheckLoanNo.Text = "....."
        Me.btnCheckLoanNo.UseVisualStyleBackColor = True
        '
        'txtLoanNo
        '
        Me.txtLoanNo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtLoanNo.Enabled = False
        Me.txtLoanNo.Location = New System.Drawing.Point(581, 54)
        Me.txtLoanNo.Name = "txtLoanNo"
        Me.txtLoanNo.Size = New System.Drawing.Size(181, 21)
        Me.txtLoanNo.TabIndex = 33
        '
        'Label2
        '
        Me.Label2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.Label2.Location = New System.Drawing.Point(578, 36)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(53, 13)
        Me.Label2.TabIndex = 32
        Me.Label2.Text = "Loan No :"
        '
        'cboRestLoanNo
        '
        Me.cboRestLoanNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboRestLoanNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboRestLoanNo.FormattingEnabled = True
        Me.cboRestLoanNo.Location = New System.Drawing.Point(474, 101)
        Me.cboRestLoanNo.Name = "cboRestLoanNo"
        Me.cboRestLoanNo.Size = New System.Drawing.Size(110, 21)
        Me.cboRestLoanNo.TabIndex = 31
        Me.cboRestLoanNo.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(356, 104)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(122, 13)
        Me.Label1.TabIndex = 30
        Me.Label1.Text = "Restructuring Loan No.:"
        Me.Label1.Visible = False
        '
        'txtRestructure
        '
        Me.txtRestructure.BackColor = System.Drawing.Color.White
        Me.txtRestructure.Location = New System.Drawing.Point(371, 101)
        Me.txtRestructure.Name = "txtRestructure"
        Me.txtRestructure.ReadOnly = True
        Me.txtRestructure.Size = New System.Drawing.Size(189, 21)
        Me.txtRestructure.TabIndex = 29
        Me.txtRestructure.Visible = False
        '
        'picStep1
        '
        Me.picStep1.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.picStep1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picStep1.Controls.Add(Me.lblStepOne)
        Me.picStep1.Dock = System.Windows.Forms.DockStyle.Top
        Me.picStep1.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.picStep1.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.picStep1.InactiveGradientHighColor = System.Drawing.Color.Transparent
        Me.picStep1.InactiveGradientLowColor = System.Drawing.Color.Transparent
        Me.picStep1.Location = New System.Drawing.Point(0, 0)
        Me.picStep1.Name = "picStep1"
        Me.picStep1.Size = New System.Drawing.Size(876, 26)
        Me.picStep1.TabIndex = 28
        '
        'lblStepOne
        '
        Me.lblStepOne.AutoSize = True
        Me.lblStepOne.BackColor = System.Drawing.Color.Transparent
        Me.lblStepOne.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStepOne.ForeColor = System.Drawing.Color.Black
        Me.lblStepOne.Location = New System.Drawing.Point(3, 2)
        Me.lblStepOne.Name = "lblStepOne"
        Me.lblStepOne.Size = New System.Drawing.Size(64, 18)
        Me.lblStepOne.TabIndex = 18
        Me.lblStepOne.Text = "Step 1"
        '
        'cmdCancel
        '
        Me.cmdCancel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdCancel.Image = Global.WindowsApplication2.My.Resources.Resources.button_cancel
        Me.cmdCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancel.Location = New System.Drawing.Point(735, 92)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 22)
        Me.cmdCancel.TabIndex = 26
        Me.cmdCancel.Text = "Cancel"
        Me.cmdCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdCancel.UseVisualStyleBackColor = True
        '
        'cmdDone
        '
        Me.cmdDone.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdDone.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdDone.Image = Global.WindowsApplication2.My.Resources.Resources.apply1
        Me.cmdDone.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdDone.Location = New System.Drawing.Point(658, 92)
        Me.cmdDone.Name = "cmdDone"
        Me.cmdDone.Size = New System.Drawing.Size(75, 22)
        Me.cmdDone.TabIndex = 3
        Me.cmdDone.Text = "Done"
        Me.cmdDone.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdDone.UseVisualStyleBackColor = True
        '
        'lblTerms
        '
        Me.lblTerms.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTerms.AutoSize = True
        Me.lblTerms.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTerms.Location = New System.Drawing.Point(470, 63)
        Me.lblTerms.Name = "lblTerms"
        Me.lblTerms.Size = New System.Drawing.Size(40, 13)
        Me.lblTerms.TabIndex = 24
        Me.lblTerms.Text = "Terms:"
        '
        'chkPreTerminate
        '
        Me.chkPreTerminate.AutoSize = True
        Me.chkPreTerminate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkPreTerminate.Location = New System.Drawing.Point(248, 112)
        Me.chkPreTerminate.Name = "chkPreTerminate"
        Me.chkPreTerminate.Size = New System.Drawing.Size(154, 17)
        Me.chkPreTerminate.TabIndex = 22
        Me.chkPreTerminate.Text = "Pre-terminate other loans?"
        Me.chkPreTerminate.UseVisualStyleBackColor = True
        Me.chkPreTerminate.Visible = False
        '
        'cboTerms
        '
        Me.cboTerms.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cboTerms.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTerms.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTerms.FormattingEnabled = True
        Me.cboTerms.Location = New System.Drawing.Point(469, 80)
        Me.cboTerms.Name = "cboTerms"
        Me.cboTerms.Size = New System.Drawing.Size(86, 21)
        Me.cboTerms.TabIndex = 2
        '
        'cboLoanType
        '
        Me.cboLoanType.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cboLoanType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLoanType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLoanType.FormattingEnabled = True
        Me.cboLoanType.Location = New System.Drawing.Point(245, 80)
        Me.cboLoanType.Name = "cboLoanType"
        Me.cboLoanType.Size = New System.Drawing.Size(218, 21)
        Me.cboLoanType.TabIndex = 1
        '
        'lblChooseLoanType
        '
        Me.lblChooseLoanType.AutoSize = True
        Me.lblChooseLoanType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblChooseLoanType.Location = New System.Drawing.Point(246, 62)
        Me.lblChooseLoanType.Name = "lblChooseLoanType"
        Me.lblChooseLoanType.Size = New System.Drawing.Size(100, 13)
        Me.lblChooseLoanType.TabIndex = 20
        Me.lblChooseLoanType.Text = "Choose a loan type"
        '
        'grpClassification
        '
        Me.grpClassification.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grpClassification.Controls.Add(Me.rdRestructure)
        Me.grpClassification.Controls.Add(Me.rdCarryForward)
        Me.grpClassification.Controls.Add(Me.optNew)
        Me.grpClassification.Controls.Add(Me.optRestructure)
        Me.grpClassification.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpClassification.ForeColor = System.Drawing.Color.Black
        Me.grpClassification.Location = New System.Drawing.Point(9, 62)
        Me.grpClassification.Name = "grpClassification"
        Me.grpClassification.Size = New System.Drawing.Size(230, 78)
        Me.grpClassification.TabIndex = 0
        Me.grpClassification.TabStop = False
        Me.grpClassification.Text = "Classification"
        '
        'rdRestructure
        '
        Me.rdRestructure.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.rdRestructure.AutoSize = True
        Me.rdRestructure.Location = New System.Drawing.Point(117, 22)
        Me.rdRestructure.Name = "rdRestructure"
        Me.rdRestructure.Size = New System.Drawing.Size(82, 17)
        Me.rdRestructure.TabIndex = 5
        Me.rdRestructure.TabStop = True
        Me.rdRestructure.Text = "Restructure"
        Me.rdRestructure.UseVisualStyleBackColor = True
        '
        'rdCarryForward
        '
        Me.rdCarryForward.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.rdCarryForward.AutoSize = True
        Me.rdCarryForward.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdCarryForward.Location = New System.Drawing.Point(117, 46)
        Me.rdCarryForward.Name = "rdCarryForward"
        Me.rdCarryForward.Size = New System.Drawing.Size(95, 17)
        Me.rdCarryForward.TabIndex = 4
        Me.rdCarryForward.Text = "Carry Forward"
        Me.rdCarryForward.UseVisualStyleBackColor = True
        '
        'optNew
        '
        Me.optNew.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.optNew.AutoSize = True
        Me.optNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.optNew.Location = New System.Drawing.Point(33, 22)
        Me.optNew.Name = "optNew"
        Me.optNew.Size = New System.Drawing.Size(46, 17)
        Me.optNew.TabIndex = 3
        Me.optNew.Text = "New"
        Me.optNew.UseVisualStyleBackColor = True
        '
        'optRestructure
        '
        Me.optRestructure.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.optRestructure.AutoSize = True
        Me.optRestructure.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.optRestructure.Location = New System.Drawing.Point(33, 46)
        Me.optRestructure.Name = "optRestructure"
        Me.optRestructure.Size = New System.Drawing.Size(66, 17)
        Me.optRestructure.TabIndex = 3
        Me.optRestructure.Text = "Renewal"
        Me.optRestructure.UseVisualStyleBackColor = True
        '
        'TxtCoopLoan
        '
        Me.TxtCoopLoan.Enabled = False
        Me.TxtCoopLoan.Location = New System.Drawing.Point(17, 114)
        Me.TxtCoopLoan.Name = "TxtCoopLoan"
        Me.TxtCoopLoan.Size = New System.Drawing.Size(145, 21)
        Me.TxtCoopLoan.TabIndex = 27
        Me.TxtCoopLoan.Visible = False
        '
        'btnPrint
        '
        Me.btnPrint.Location = New System.Drawing.Point(17, 117)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(150, 23)
        Me.btnPrint.TabIndex = 39
        Me.btnPrint.Text = "Loan Application"
        Me.btnPrint.UseVisualStyleBackColor = True
        Me.btnPrint.Visible = False
        '
        'panelstep2
        '
        Me.panelstep2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panelstep2.Controls.Add(Me.GroupBox3)
        Me.panelstep2.Controls.Add(Me.picStep2)
        Me.panelstep2.Controls.Add(Me.cmdContinue)
        Me.panelstep2.Controls.Add(Me.cmdBack)
        Me.panelstep2.Controls.Add(Me.txtPhilHealth)
        Me.panelstep2.Controls.Add(Me.lblPhilHealth)
        Me.panelstep2.Controls.Add(Me.grpMaxLoanableAmount)
        Me.panelstep2.Controls.Add(Me.txtOtherNonRecurring)
        Me.panelstep2.Controls.Add(Me.txtCompanyLoan)
        Me.panelstep2.Controls.Add(Me.txtPagibigLoan)
        Me.panelstep2.Controls.Add(Me.txtSSSLoan)
        Me.panelstep2.Controls.Add(Me.lblOtherNonRecurring)
        Me.panelstep2.Controls.Add(Me.lblCompanyLoan)
        Me.panelstep2.Controls.Add(Me.lblPagibigLoan)
        Me.panelstep2.Controls.Add(Me.lblSSSLoan)
        Me.panelstep2.Location = New System.Drawing.Point(275, 169)
        Me.panelstep2.Name = "panelstep2"
        Me.panelstep2.Size = New System.Drawing.Size(883, 298)
        Me.panelstep2.TabIndex = 24
        '
        'GroupBox3
        '
        Me.GroupBox3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox3.Controls.Add(Me.TabControl1)
        Me.GroupBox3.Controls.Add(Me.Label3)
        Me.GroupBox3.Controls.Add(Me.cboMonthDeduction)
        Me.GroupBox3.Location = New System.Drawing.Point(7, 38)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(450, 221)
        Me.GroupBox3.TabIndex = 42
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Interest Method"
        '
        'TabControl1
        '
        Me.TabControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TabControl1.Appearance = System.Windows.Forms.TabAppearance.FlatButtons
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Location = New System.Drawing.Point(12, 17)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(420, 198)
        Me.TabControl1.TabIndex = 58
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.White
        Me.TabPage1.Controls.Add(Me.dgvStandardIntMethod)
        Me.TabPage1.Location = New System.Drawing.Point(4, 25)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(412, 169)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Standard"
        '
        'dgvStandardIntMethod
        '
        Me.dgvStandardIntMethod.AllowUserToAddRows = False
        Me.dgvStandardIntMethod.AllowUserToDeleteRows = False
        Me.dgvStandardIntMethod.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvStandardIntMethod.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvStandardIntMethod.BackgroundColor = System.Drawing.Color.White
        Me.dgvStandardIntMethod.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvStandardIntMethod.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.dgvStandardIntMethod.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvStandardIntMethod.Location = New System.Drawing.Point(3, 3)
        Me.dgvStandardIntMethod.Name = "dgvStandardIntMethod"
        Me.dgvStandardIntMethod.Size = New System.Drawing.Size(406, 163)
        Me.dgvStandardIntMethod.TabIndex = 0
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.Color.White
        Me.TabPage2.Controls.Add(Me.dgvCustomizedIntMethod)
        Me.TabPage2.Location = New System.Drawing.Point(4, 25)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(412, 169)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Customized"
        '
        'dgvCustomizedIntMethod
        '
        Me.dgvCustomizedIntMethod.AllowUserToAddRows = False
        Me.dgvCustomizedIntMethod.AllowUserToDeleteRows = False
        Me.dgvCustomizedIntMethod.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvCustomizedIntMethod.BackgroundColor = System.Drawing.Color.White
        Me.dgvCustomizedIntMethod.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvCustomizedIntMethod.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.dgvCustomizedIntMethod.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCustomizedIntMethod.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvCustomizedIntMethod.Location = New System.Drawing.Point(3, 3)
        Me.dgvCustomizedIntMethod.Name = "dgvCustomizedIntMethod"
        Me.dgvCustomizedIntMethod.Size = New System.Drawing.Size(406, 163)
        Me.dgvCustomizedIntMethod.TabIndex = 1
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Gray
        Me.Label3.Location = New System.Drawing.Point(175, 93)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(48, 13)
        Me.Label3.TabIndex = 53
        Me.Label3.Text = "in Month"
        Me.Label3.Visible = False
        '
        'cboMonthDeduction
        '
        Me.cboMonthDeduction.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMonthDeduction.Enabled = False
        Me.cboMonthDeduction.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMonthDeduction.FormattingEnabled = True
        Me.cboMonthDeduction.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"})
        Me.cboMonthDeduction.Location = New System.Drawing.Point(171, 72)
        Me.cboMonthDeduction.Name = "cboMonthDeduction"
        Me.cboMonthDeduction.Size = New System.Drawing.Size(69, 21)
        Me.cboMonthDeduction.TabIndex = 52
        Me.cboMonthDeduction.Visible = False
        '
        'picStep2
        '
        Me.picStep2.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.picStep2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picStep2.Controls.Add(Me.lblSteptwo)
        Me.picStep2.Dock = System.Windows.Forms.DockStyle.Top
        Me.picStep2.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.picStep2.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.picStep2.InactiveGradientHighColor = System.Drawing.Color.Transparent
        Me.picStep2.InactiveGradientLowColor = System.Drawing.Color.Transparent
        Me.picStep2.Location = New System.Drawing.Point(0, 0)
        Me.picStep2.Name = "picStep2"
        Me.picStep2.Size = New System.Drawing.Size(883, 26)
        Me.picStep2.TabIndex = 41
        '
        'lblSteptwo
        '
        Me.lblSteptwo.AutoSize = True
        Me.lblSteptwo.BackColor = System.Drawing.Color.Transparent
        Me.lblSteptwo.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSteptwo.ForeColor = System.Drawing.Color.Black
        Me.lblSteptwo.Location = New System.Drawing.Point(1, 2)
        Me.lblSteptwo.Name = "lblSteptwo"
        Me.lblSteptwo.Size = New System.Drawing.Size(64, 18)
        Me.lblSteptwo.TabIndex = 23
        Me.lblSteptwo.Text = "Step 2"
        '
        'cmdContinue
        '
        Me.cmdContinue.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdContinue.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdContinue.Location = New System.Drawing.Point(669, 265)
        Me.cmdContinue.Name = "cmdContinue"
        Me.cmdContinue.Size = New System.Drawing.Size(73, 30)
        Me.cmdContinue.TabIndex = 8
        Me.cmdContinue.Text = "Continue"
        Me.cmdContinue.UseVisualStyleBackColor = True
        '
        'cmdBack
        '
        Me.cmdBack.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdBack.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdBack.Location = New System.Drawing.Point(748, 265)
        Me.cmdBack.Name = "cmdBack"
        Me.cmdBack.Size = New System.Drawing.Size(61, 30)
        Me.cmdBack.TabIndex = 38
        Me.cmdBack.Text = "Back"
        Me.cmdBack.UseVisualStyleBackColor = True
        '
        'txtPhilHealth
        '
        Me.txtPhilHealth.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPhilHealth.Location = New System.Drawing.Point(276, 152)
        Me.txtPhilHealth.Name = "txtPhilHealth"
        Me.txtPhilHealth.Size = New System.Drawing.Size(10, 21)
        Me.txtPhilHealth.TabIndex = 3
        Me.txtPhilHealth.Text = "0"
        Me.txtPhilHealth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPhilHealth.Visible = False
        '
        'lblPhilHealth
        '
        Me.lblPhilHealth.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPhilHealth.Location = New System.Drawing.Point(254, 152)
        Me.lblPhilHealth.Name = "lblPhilHealth"
        Me.lblPhilHealth.Size = New System.Drawing.Size(23, 13)
        Me.lblPhilHealth.TabIndex = 36
        Me.lblPhilHealth.Text = "PhilHealth"
        Me.lblPhilHealth.Visible = False
        '
        'grpMaxLoanableAmount
        '
        Me.grpMaxLoanableAmount.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grpMaxLoanableAmount.Controls.Add(Me.txtMaxLoanAmount)
        Me.grpMaxLoanableAmount.Controls.Add(Me.txtCapacityToPay)
        Me.grpMaxLoanableAmount.Controls.Add(Me.txtContribution)
        Me.grpMaxLoanableAmount.Controls.Add(Me.cmdShowComputation)
        Me.grpMaxLoanableAmount.Controls.Add(Me.lblNote)
        Me.grpMaxLoanableAmount.Controls.Add(Me.lblCapacityToPay)
        Me.grpMaxLoanableAmount.Controls.Add(Me.btnComputeMaxLoan)
        Me.grpMaxLoanableAmount.Controls.Add(Me.lblContribution)
        Me.grpMaxLoanableAmount.Controls.Add(Me.lblRate)
        Me.grpMaxLoanableAmount.Controls.Add(Me.txtRateMonthly)
        Me.grpMaxLoanableAmount.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpMaxLoanableAmount.ForeColor = System.Drawing.Color.Black
        Me.grpMaxLoanableAmount.Location = New System.Drawing.Point(464, 36)
        Me.grpMaxLoanableAmount.Name = "grpMaxLoanableAmount"
        Me.grpMaxLoanableAmount.Size = New System.Drawing.Size(341, 223)
        Me.grpMaxLoanableAmount.TabIndex = 34
        Me.grpMaxLoanableAmount.TabStop = False
        Me.grpMaxLoanableAmount.Text = "Maximum Loanable Amount"
        '
        'txtMaxLoanAmount
        '
        Me.txtMaxLoanAmount.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtMaxLoanAmount.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMaxLoanAmount.Location = New System.Drawing.Point(91, 139)
        Me.txtMaxLoanAmount.Name = "txtMaxLoanAmount"
        Me.txtMaxLoanAmount.ReadOnly = True
        Me.txtMaxLoanAmount.Size = New System.Drawing.Size(197, 26)
        Me.txtMaxLoanAmount.TabIndex = 1
        Me.txtMaxLoanAmount.Text = "0.00"
        Me.txtMaxLoanAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtCapacityToPay
        '
        Me.txtCapacityToPay.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtCapacityToPay.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCapacityToPay.Location = New System.Drawing.Point(134, 57)
        Me.txtCapacityToPay.Name = "txtCapacityToPay"
        Me.txtCapacityToPay.ReadOnly = True
        Me.txtCapacityToPay.Size = New System.Drawing.Size(154, 21)
        Me.txtCapacityToPay.TabIndex = 1
        Me.txtCapacityToPay.Text = "0"
        Me.txtCapacityToPay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtContribution
        '
        Me.txtContribution.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtContribution.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtContribution.Location = New System.Drawing.Point(134, 96)
        Me.txtContribution.Name = "txtContribution"
        Me.txtContribution.ReadOnly = True
        Me.txtContribution.Size = New System.Drawing.Size(154, 21)
        Me.txtContribution.TabIndex = 1
        Me.txtContribution.Text = "0"
        Me.txtContribution.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cmdShowComputation
        '
        Me.cmdShowComputation.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdShowComputation.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdShowComputation.Image = Global.WindowsApplication2.My.Resources.Resources.files
        Me.cmdShowComputation.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdShowComputation.Location = New System.Drawing.Point(180, 187)
        Me.cmdShowComputation.Name = "cmdShowComputation"
        Me.cmdShowComputation.Size = New System.Drawing.Size(155, 30)
        Me.cmdShowComputation.TabIndex = 7
        Me.cmdShowComputation.Text = "Show Computation"
        Me.cmdShowComputation.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdShowComputation.UseVisualStyleBackColor = True
        '
        'lblNote
        '
        Me.lblNote.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblNote.AutoSize = True
        Me.lblNote.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNote.Location = New System.Drawing.Point(162, 123)
        Me.lblNote.Name = "lblNote"
        Me.lblNote.Size = New System.Drawing.Size(127, 13)
        Me.lblNote.TabIndex = 0
        Me.lblNote.Text = "Note: Whichever is lower"
        '
        'lblCapacityToPay
        '
        Me.lblCapacityToPay.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblCapacityToPay.AutoSize = True
        Me.lblCapacityToPay.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCapacityToPay.Location = New System.Drawing.Point(45, 63)
        Me.lblCapacityToPay.Name = "lblCapacityToPay"
        Me.lblCapacityToPay.Size = New System.Drawing.Size(83, 13)
        Me.lblCapacityToPay.TabIndex = 0
        Me.lblCapacityToPay.Text = "Capacity to Pay"
        '
        'btnComputeMaxLoan
        '
        Me.btnComputeMaxLoan.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnComputeMaxLoan.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnComputeMaxLoan.Image = Global.WindowsApplication2.My.Resources.Resources.cash_register
        Me.btnComputeMaxLoan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnComputeMaxLoan.Location = New System.Drawing.Point(56, 187)
        Me.btnComputeMaxLoan.Name = "btnComputeMaxLoan"
        Me.btnComputeMaxLoan.Size = New System.Drawing.Size(118, 30)
        Me.btnComputeMaxLoan.TabIndex = 6
        Me.btnComputeMaxLoan.Text = "Compute"
        Me.btnComputeMaxLoan.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnComputeMaxLoan.UseVisualStyleBackColor = True
        '
        'lblContribution
        '
        Me.lblContribution.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblContribution.AutoSize = True
        Me.lblContribution.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblContribution.Location = New System.Drawing.Point(88, 104)
        Me.lblContribution.Name = "lblContribution"
        Me.lblContribution.Size = New System.Drawing.Size(44, 13)
        Me.lblContribution.TabIndex = 0
        Me.lblContribution.Text = "Savings"
        '
        'lblRate
        '
        Me.lblRate.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblRate.AutoSize = True
        Me.lblRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRate.Location = New System.Drawing.Point(10, 29)
        Me.lblRate.Name = "lblRate"
        Me.lblRate.Size = New System.Drawing.Size(63, 13)
        Me.lblRate.TabIndex = 27
        Me.lblRate.Text = "Salary Rate"
        '
        'txtRateMonthly
        '
        Me.txtRateMonthly.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtRateMonthly.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRateMonthly.Location = New System.Drawing.Point(165, 23)
        Me.txtRateMonthly.Name = "txtRateMonthly"
        Me.txtRateMonthly.Size = New System.Drawing.Size(123, 21)
        Me.txtRateMonthly.TabIndex = 0
        Me.txtRateMonthly.Text = "0"
        Me.txtRateMonthly.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtOtherNonRecurring
        '
        Me.txtOtherNonRecurring.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOtherNonRecurring.Location = New System.Drawing.Point(276, 90)
        Me.txtOtherNonRecurring.Name = "txtOtherNonRecurring"
        Me.txtOtherNonRecurring.Size = New System.Drawing.Size(11, 21)
        Me.txtOtherNonRecurring.TabIndex = 5
        Me.txtOtherNonRecurring.Text = "0"
        Me.txtOtherNonRecurring.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtOtherNonRecurring.Visible = False
        '
        'txtCompanyLoan
        '
        Me.txtCompanyLoan.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCompanyLoan.Location = New System.Drawing.Point(276, 63)
        Me.txtCompanyLoan.Name = "txtCompanyLoan"
        Me.txtCompanyLoan.Size = New System.Drawing.Size(11, 21)
        Me.txtCompanyLoan.TabIndex = 4
        Me.txtCompanyLoan.Text = "0"
        Me.txtCompanyLoan.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCompanyLoan.Visible = False
        '
        'txtPagibigLoan
        '
        Me.txtPagibigLoan.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPagibigLoan.Location = New System.Drawing.Point(278, 133)
        Me.txtPagibigLoan.Name = "txtPagibigLoan"
        Me.txtPagibigLoan.Size = New System.Drawing.Size(10, 21)
        Me.txtPagibigLoan.TabIndex = 2
        Me.txtPagibigLoan.Text = "0"
        Me.txtPagibigLoan.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPagibigLoan.Visible = False
        '
        'txtSSSLoan
        '
        Me.txtSSSLoan.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSSSLoan.Location = New System.Drawing.Point(276, 113)
        Me.txtSSSLoan.Name = "txtSSSLoan"
        Me.txtSSSLoan.Size = New System.Drawing.Size(11, 21)
        Me.txtSSSLoan.TabIndex = 1
        Me.txtSSSLoan.Text = "0"
        Me.txtSSSLoan.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSSSLoan.Visible = False
        '
        'lblOtherNonRecurring
        '
        Me.lblOtherNonRecurring.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOtherNonRecurring.Location = New System.Drawing.Point(254, 93)
        Me.lblOtherNonRecurring.Name = "lblOtherNonRecurring"
        Me.lblOtherNonRecurring.Size = New System.Drawing.Size(33, 13)
        Me.lblOtherNonRecurring.TabIndex = 25
        Me.lblOtherNonRecurring.Text = "Other (Non-Recurring)"
        Me.lblOtherNonRecurring.Visible = False
        '
        'lblCompanyLoan
        '
        Me.lblCompanyLoan.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCompanyLoan.Location = New System.Drawing.Point(254, 65)
        Me.lblCompanyLoan.Name = "lblCompanyLoan"
        Me.lblCompanyLoan.Size = New System.Drawing.Size(17, 12)
        Me.lblCompanyLoan.TabIndex = 24
        Me.lblCompanyLoan.Text = "Company Loan"
        Me.lblCompanyLoan.Visible = False
        '
        'lblPagibigLoan
        '
        Me.lblPagibigLoan.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPagibigLoan.Location = New System.Drawing.Point(254, 133)
        Me.lblPagibigLoan.Name = "lblPagibigLoan"
        Me.lblPagibigLoan.Size = New System.Drawing.Size(30, 13)
        Me.lblPagibigLoan.TabIndex = 26
        Me.lblPagibigLoan.Text = "Pag-Ibig Loan"
        Me.lblPagibigLoan.Visible = False
        '
        'lblSSSLoan
        '
        Me.lblSSSLoan.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSSSLoan.Location = New System.Drawing.Point(254, 113)
        Me.lblSSSLoan.Name = "lblSSSLoan"
        Me.lblSSSLoan.Size = New System.Drawing.Size(26, 13)
        Me.lblSSSLoan.TabIndex = 28
        Me.lblSSSLoan.Text = "SSS Loan"
        Me.lblSSSLoan.Visible = False
        '
        'lstname
        '
        Me.lstname.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lstname.FormattingEnabled = True
        Me.lstname.Location = New System.Drawing.Point(22, 191)
        Me.lstname.Name = "lstname"
        Me.lstname.Size = New System.Drawing.Size(159, 28)
        Me.lstname.TabIndex = 1
        Me.lstname.Visible = False
        '
        'panelStep3
        '
        Me.panelStep3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.panelStep3.Controls.Add(Me.GroupBox2)
        Me.panelStep3.Controls.Add(Me.picStep3)
        Me.panelStep3.Controls.Add(Me.grpNecessaryInfo)
        Me.panelStep3.Location = New System.Drawing.Point(275, 473)
        Me.panelStep3.Name = "panelStep3"
        Me.panelStep3.Size = New System.Drawing.Size(883, 224)
        Me.panelStep3.TabIndex = 25
        '
        'GroupBox2
        '
        Me.GroupBox2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox2.Controls.Add(Me.btnDeleteRow)
        Me.GroupBox2.Controls.Add(Me.dgvComakerList)
        Me.GroupBox2.Controls.Add(Me.btnComaker)
        Me.GroupBox2.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(9, 32)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(796, 102)
        Me.GroupBox2.TabIndex = 17
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Co-Maker Information"
        '
        'btnDeleteRow
        '
        Me.btnDeleteRow.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDeleteRow.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDeleteRow.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDeleteRow.Location = New System.Drawing.Point(617, 19)
        Me.btnDeleteRow.Name = "btnDeleteRow"
        Me.btnDeleteRow.Size = New System.Drawing.Size(107, 22)
        Me.btnDeleteRow.TabIndex = 21
        Me.btnDeleteRow.Text = "Delete Row"
        Me.btnDeleteRow.UseVisualStyleBackColor = True
        '
        'dgvComakerList
        '
        Me.dgvComakerList.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvComakerList.BackgroundColor = System.Drawing.Color.White
        Me.dgvComakerList.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.dgvComakerList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvComakerList.Location = New System.Drawing.Point(6, 19)
        Me.dgvComakerList.Name = "dgvComakerList"
        Me.dgvComakerList.RowHeadersVisible = False
        Me.dgvComakerList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvComakerList.Size = New System.Drawing.Size(605, 76)
        Me.dgvComakerList.TabIndex = 20
        '
        'btnComaker
        '
        Me.btnComaker.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnComaker.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnComaker.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnComaker.Location = New System.Drawing.Point(617, 47)
        Me.btnComaker.Name = "btnComaker"
        Me.btnComaker.Size = New System.Drawing.Size(107, 22)
        Me.btnComaker.TabIndex = 19
        Me.btnComaker.Text = "Add"
        Me.btnComaker.UseVisualStyleBackColor = True
        '
        'picStep3
        '
        Me.picStep3.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.picStep3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picStep3.Controls.Add(Me.lblStepThree)
        Me.picStep3.Dock = System.Windows.Forms.DockStyle.Top
        Me.picStep3.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.picStep3.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.picStep3.InactiveGradientHighColor = System.Drawing.Color.Transparent
        Me.picStep3.InactiveGradientLowColor = System.Drawing.Color.Transparent
        Me.picStep3.Location = New System.Drawing.Point(0, 0)
        Me.picStep3.Name = "picStep3"
        Me.picStep3.Size = New System.Drawing.Size(883, 26)
        Me.picStep3.TabIndex = 8
        '
        'lblStepThree
        '
        Me.lblStepThree.AutoSize = True
        Me.lblStepThree.BackColor = System.Drawing.Color.Transparent
        Me.lblStepThree.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStepThree.ForeColor = System.Drawing.Color.White
        Me.lblStepThree.Location = New System.Drawing.Point(2, 3)
        Me.lblStepThree.Name = "lblStepThree"
        Me.lblStepThree.Size = New System.Drawing.Size(64, 18)
        Me.lblStepThree.TabIndex = 15
        Me.lblStepThree.Text = "Step 3"
        '
        'grpNecessaryInfo
        '
        Me.grpNecessaryInfo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grpNecessaryInfo.Controls.Add(Me.btnshowpenalties)
        Me.grpNecessaryInfo.Controls.Add(Me.cmdNo)
        Me.grpNecessaryInfo.Controls.Add(Me.chkPenalties)
        Me.grpNecessaryInfo.Controls.Add(Me.btncompute1)
        Me.grpNecessaryInfo.Controls.Add(Me.txtPrincipalAmount)
        Me.grpNecessaryInfo.Controls.Add(Me.dtpLoanApplicationDate)
        Me.grpNecessaryInfo.Controls.Add(Me.lblPrincipalAmount)
        Me.grpNecessaryInfo.Controls.Add(Me.lblLoanApplicationDate)
        Me.grpNecessaryInfo.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpNecessaryInfo.ForeColor = System.Drawing.Color.Black
        Me.grpNecessaryInfo.Location = New System.Drawing.Point(7, 140)
        Me.grpNecessaryInfo.Name = "grpNecessaryInfo"
        Me.grpNecessaryInfo.Size = New System.Drawing.Size(798, 78)
        Me.grpNecessaryInfo.TabIndex = 16
        Me.grpNecessaryInfo.TabStop = False
        Me.grpNecessaryInfo.Text = "Find out necessary information"
        '
        'btnshowpenalties
        '
        Me.btnshowpenalties.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnshowpenalties.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnshowpenalties.Location = New System.Drawing.Point(12, 38)
        Me.btnshowpenalties.Name = "btnshowpenalties"
        Me.btnshowpenalties.Size = New System.Drawing.Size(108, 30)
        Me.btnshowpenalties.TabIndex = 6
        Me.btnshowpenalties.Text = "Show Penalties"
        Me.btnshowpenalties.UseVisualStyleBackColor = True
        Me.btnshowpenalties.Visible = False
        '
        'cmdNo
        '
        Me.cmdNo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdNo.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdNo.Location = New System.Drawing.Point(712, 9)
        Me.cmdNo.Name = "cmdNo"
        Me.cmdNo.Size = New System.Drawing.Size(80, 30)
        Me.cmdNo.TabIndex = 18
        Me.cmdNo.Text = "Back"
        Me.cmdNo.UseVisualStyleBackColor = True
        '
        'chkPenalties
        '
        Me.chkPenalties.AutoSize = True
        Me.chkPenalties.Location = New System.Drawing.Point(19, 74)
        Me.chkPenalties.Name = "chkPenalties"
        Me.chkPenalties.Size = New System.Drawing.Size(221, 17)
        Me.chkPenalties.TabIndex = 5
        Me.chkPenalties.Text = "Pay your Penalties with this loan ?"
        Me.chkPenalties.UseVisualStyleBackColor = True
        Me.chkPenalties.Visible = False
        '
        'btncompute1
        '
        Me.btncompute1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btncompute1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btncompute1.Location = New System.Drawing.Point(637, 41)
        Me.btncompute1.Name = "btncompute1"
        Me.btncompute1.Size = New System.Drawing.Size(155, 30)
        Me.btncompute1.TabIndex = 2
        Me.btncompute1.Text = "Compute loan"
        Me.btncompute1.UseVisualStyleBackColor = True
        '
        'txtPrincipalAmount
        '
        Me.txtPrincipalAmount.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtPrincipalAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPrincipalAmount.Location = New System.Drawing.Point(252, 47)
        Me.txtPrincipalAmount.MaxLength = 500
        Me.txtPrincipalAmount.Name = "txtPrincipalAmount"
        Me.txtPrincipalAmount.Size = New System.Drawing.Size(318, 21)
        Me.txtPrincipalAmount.TabIndex = 1
        Me.txtPrincipalAmount.Text = "0.00"
        Me.txtPrincipalAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'dtpLoanApplicationDate
        '
        Me.dtpLoanApplicationDate.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dtpLoanApplicationDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpLoanApplicationDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpLoanApplicationDate.Location = New System.Drawing.Point(252, 20)
        Me.dtpLoanApplicationDate.Name = "dtpLoanApplicationDate"
        Me.dtpLoanApplicationDate.Size = New System.Drawing.Size(174, 21)
        Me.dtpLoanApplicationDate.TabIndex = 0
        '
        'lblPrincipalAmount
        '
        Me.lblPrincipalAmount.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblPrincipalAmount.AutoSize = True
        Me.lblPrincipalAmount.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPrincipalAmount.Location = New System.Drawing.Point(136, 55)
        Me.lblPrincipalAmount.Name = "lblPrincipalAmount"
        Me.lblPrincipalAmount.Size = New System.Drawing.Size(108, 13)
        Me.lblPrincipalAmount.TabIndex = 0
        Me.lblPrincipalAmount.Text = "Principal Amount:"
        '
        'lblLoanApplicationDate
        '
        Me.lblLoanApplicationDate.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblLoanApplicationDate.AutoSize = True
        Me.lblLoanApplicationDate.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanApplicationDate.Location = New System.Drawing.Point(112, 26)
        Me.lblLoanApplicationDate.Name = "lblLoanApplicationDate"
        Me.lblLoanApplicationDate.Size = New System.Drawing.Size(136, 13)
        Me.lblLoanApplicationDate.TabIndex = 0
        Me.lblLoanApplicationDate.Text = "Loan Application Date:"
        '
        'grpSearchOption
        '
        Me.grpSearchOption.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grpSearchOption.Controls.Add(Me.txtSearchName)
        Me.grpSearchOption.Controls.Add(Me.cmdSearch)
        Me.grpSearchOption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpSearchOption.Location = New System.Drawing.Point(3, 39)
        Me.grpSearchOption.Name = "grpSearchOption"
        Me.grpSearchOption.Size = New System.Drawing.Size(266, 42)
        Me.grpSearchOption.TabIndex = 27
        Me.grpSearchOption.TabStop = False
        Me.grpSearchOption.Text = "Search Option"
        '
        'txtSearchName
        '
        Me.txtSearchName.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSearchName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSearchName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchName.Location = New System.Drawing.Point(6, 14)
        Me.txtSearchName.Name = "txtSearchName"
        Me.txtSearchName.Size = New System.Drawing.Size(189, 21)
        Me.txtSearchName.TabIndex = 0
        '
        'optName
        '
        Me.optName.AutoSize = True
        Me.optName.Location = New System.Drawing.Point(96, 172)
        Me.optName.Name = "optName"
        Me.optName.Size = New System.Drawing.Size(85, 17)
        Me.optName.TabIndex = 1
        Me.optName.Text = "Last Name"
        Me.optName.UseVisualStyleBackColor = True
        '
        'optNumber
        '
        Me.optNumber.AutoSize = True
        Me.optNumber.Checked = True
        Me.optNumber.Location = New System.Drawing.Point(22, 172)
        Me.optNumber.Name = "optNumber"
        Me.optNumber.Size = New System.Drawing.Size(70, 17)
        Me.optNumber.TabIndex = 0
        Me.optNumber.TabStop = True
        Me.optNumber.Text = "Number"
        Me.optNumber.UseVisualStyleBackColor = True
        '
        'lstNumbers
        '
        Me.lstNumbers.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lstNumbers.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstNumbers.FormattingEnabled = True
        Me.lstNumbers.Location = New System.Drawing.Point(22, 225)
        Me.lstNumbers.Name = "lstNumbers"
        Me.lstNumbers.Size = New System.Drawing.Size(159, 28)
        Me.lstNumbers.TabIndex = 41
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lstMembers)
        Me.GroupBox1.Location = New System.Drawing.Point(6, 81)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(262, 622)
        Me.GroupBox1.TabIndex = 42
        Me.GroupBox1.TabStop = False
        '
        'lstMembers
        '
        Me.lstMembers.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lstMembers.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstMembers.Location = New System.Drawing.Point(6, 12)
        Me.lstMembers.Name = "lstMembers"
        Me.lstMembers.Size = New System.Drawing.Size(251, 604)
        Me.lstMembers.TabIndex = 0
        Me.lstMembers.UseCompatibleStateImageBehavior = False
        '
        'PanePanel3
        '
        Me.PanePanel3.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.PanePanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel3.Controls.Add(Me.btnAccounts)
        Me.PanePanel3.Controls.Add(Me.btnRefresh)
        Me.PanePanel3.Controls.Add(Me.btnSearch)
        Me.PanePanel3.Controls.Add(Me.btnExit)
        Me.PanePanel3.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PanePanel3.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.PanePanel3.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel3.InactiveGradientHighColor = System.Drawing.Color.Transparent
        Me.PanePanel3.InactiveGradientLowColor = System.Drawing.Color.Transparent
        Me.PanePanel3.Location = New System.Drawing.Point(0, 711)
        Me.PanePanel3.Name = "PanePanel3"
        Me.PanePanel3.Size = New System.Drawing.Size(1090, 33)
        Me.PanePanel3.TabIndex = 45
        '
        'btnAccounts
        '
        Me.btnAccounts.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnAccounts.Font = New System.Drawing.Font("Verdana", 8.25!)
        Me.btnAccounts.Location = New System.Drawing.Point(686, 0)
        Me.btnAccounts.Name = "btnAccounts"
        Me.btnAccounts.Size = New System.Drawing.Size(162, 31)
        Me.btnAccounts.TabIndex = 49
        Me.btnAccounts.Text = "View Accounts"
        Me.btnAccounts.UseVisualStyleBackColor = True
        '
        'btnRefresh
        '
        Me.btnRefresh.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnRefresh.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRefresh.Image = CType(resources.GetObject("btnRefresh.Image"), System.Drawing.Image)
        Me.btnRefresh.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRefresh.Location = New System.Drawing.Point(848, 0)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(80, 31)
        Me.btnRefresh.TabIndex = 47
        Me.btnRefresh.Text = "Refresh"
        Me.btnRefresh.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnRefresh.UseVisualStyleBackColor = True
        '
        'btnSearch
        '
        Me.btnSearch.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnSearch.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSearch.Image = Global.WindowsApplication2.My.Resources.Resources.Search
        Me.btnSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSearch.Location = New System.Drawing.Point(928, 0)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(80, 31)
        Me.btnSearch.TabIndex = 46
        Me.btnSearch.Text = "Search"
        Me.btnSearch.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSearch.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnExit.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExit.Image = CType(resources.GetObject("btnExit.Image"), System.Drawing.Image)
        Me.btnExit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnExit.Location = New System.Drawing.Point(1008, 0)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(80, 31)
        Me.btnExit.TabIndex = 45
        Me.btnExit.Text = "Close"
        Me.btnExit.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'PanePanel5
        '
        Me.PanePanel5.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.PanePanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel5.Controls.Add(Me.Label19)
        Me.PanePanel5.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.PanePanel5.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel5.InactiveGradientHighColor = System.Drawing.Color.Transparent
        Me.PanePanel5.InactiveGradientLowColor = System.Drawing.Color.Transparent
        Me.PanePanel5.Location = New System.Drawing.Point(6, 6)
        Me.PanePanel5.Name = "PanePanel5"
        Me.PanePanel5.Size = New System.Drawing.Size(263, 26)
        Me.PanePanel5.TabIndex = 30
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.BackColor = System.Drawing.Color.Transparent
        Me.Label19.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.Black
        Me.Label19.Location = New System.Drawing.Point(0, 2)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(89, 18)
        Me.Label19.TabIndex = 14
        Me.Label19.Text = "Members"
        '
        'frmLoan_ApplicationMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1090, 744)
        Me.Controls.Add(Me.PanePanel3)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.optName)
        Me.Controls.Add(Me.lstNumbers)
        Me.Controls.Add(Me.optNumber)
        Me.Controls.Add(Me.lstname)
        Me.Controls.Add(Me.PanePanel5)
        Me.Controls.Add(Me.grpSearchOption)
        Me.Controls.Add(Me.panelStep3)
        Me.Controls.Add(Me.panelstep2)
        Me.Controls.Add(Me.txtSearch)
        Me.Controls.Add(Me.panelStep1)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmLoan_ApplicationMain"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Loan Application"
        Me.panelStep1.ResumeLayout(False)
        Me.panelStep1.PerformLayout()
        Me.picStep1.ResumeLayout(False)
        Me.picStep1.PerformLayout()
        Me.grpClassification.ResumeLayout(False)
        Me.grpClassification.PerformLayout()
        Me.panelstep2.ResumeLayout(False)
        Me.panelstep2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        CType(Me.dgvStandardIntMethod, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        CType(Me.dgvCustomizedIntMethod, System.ComponentModel.ISupportInitialize).EndInit()
        Me.picStep2.ResumeLayout(False)
        Me.picStep2.PerformLayout()
        Me.grpMaxLoanableAmount.ResumeLayout(False)
        Me.grpMaxLoanableAmount.PerformLayout()
        Me.panelStep3.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.dgvComakerList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.picStep3.ResumeLayout(False)
        Me.picStep3.PerformLayout()
        Me.grpNecessaryInfo.ResumeLayout(False)
        Me.grpNecessaryInfo.PerformLayout()
        Me.grpSearchOption.ResumeLayout(False)
        Me.grpSearchOption.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.PanePanel3.ResumeLayout(False)
        Me.PanePanel5.ResumeLayout(False)
        Me.PanePanel5.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cmdSearch As System.Windows.Forms.Button
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents txtSearch As System.Windows.Forms.TextBox
    Friend WithEvents panelStep1 As System.Windows.Forms.Panel
    Friend WithEvents cmdDone As System.Windows.Forms.Button
    Friend WithEvents lblTerms As System.Windows.Forms.Label
    Friend WithEvents chkPreTerminate As System.Windows.Forms.CheckBox
    Friend WithEvents cboTerms As System.Windows.Forms.ComboBox
    Friend WithEvents cboLoanType As System.Windows.Forms.ComboBox
    Friend WithEvents lblChooseLoanType As System.Windows.Forms.Label
    Friend WithEvents grpClassification As System.Windows.Forms.GroupBox
    Friend WithEvents optNew As System.Windows.Forms.RadioButton
    Friend WithEvents optRestructure As System.Windows.Forms.RadioButton
    Friend WithEvents panelstep2 As System.Windows.Forms.Panel
    Friend WithEvents cmdBack As System.Windows.Forms.Button
    Friend WithEvents txtPhilHealth As System.Windows.Forms.TextBox
    Friend WithEvents lblPhilHealth As System.Windows.Forms.Label
    Friend WithEvents btnComputeMaxLoan As System.Windows.Forms.Button
    Friend WithEvents grpMaxLoanableAmount As System.Windows.Forms.GroupBox
    Friend WithEvents txtMaxLoanAmount As System.Windows.Forms.TextBox
    Friend WithEvents txtCapacityToPay As System.Windows.Forms.TextBox
    Friend WithEvents txtContribution As System.Windows.Forms.TextBox
    Friend WithEvents lblNote As System.Windows.Forms.Label
    Friend WithEvents lblCapacityToPay As System.Windows.Forms.Label
    Friend WithEvents lblContribution As System.Windows.Forms.Label
    Friend WithEvents txtOtherNonRecurring As System.Windows.Forms.TextBox
    Friend WithEvents txtCompanyLoan As System.Windows.Forms.TextBox
    Friend WithEvents txtPagibigLoan As System.Windows.Forms.TextBox
    Friend WithEvents txtSSSLoan As System.Windows.Forms.TextBox
    Friend WithEvents txtRateMonthly As System.Windows.Forms.TextBox
    Friend WithEvents lblOtherNonRecurring As System.Windows.Forms.Label
    Friend WithEvents lblCompanyLoan As System.Windows.Forms.Label
    Friend WithEvents lblPagibigLoan As System.Windows.Forms.Label
    Friend WithEvents lblSSSLoan As System.Windows.Forms.Label
    Friend WithEvents lblRate As System.Windows.Forms.Label
    Friend WithEvents lblSteptwo As System.Windows.Forms.Label
    Friend WithEvents panelStep3 As System.Windows.Forms.Panel
    Friend WithEvents cmdNo As System.Windows.Forms.Button
    Friend WithEvents grpNecessaryInfo As System.Windows.Forms.GroupBox
    Friend WithEvents txtPrincipalAmount As System.Windows.Forms.TextBox
    Friend WithEvents dtpLoanApplicationDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblPrincipalAmount As System.Windows.Forms.Label
    Friend WithEvents lblLoanApplicationDate As System.Windows.Forms.Label
    Friend WithEvents lblStepThree As System.Windows.Forms.Label
    Friend WithEvents cmdShowComputation As System.Windows.Forms.Button
    Friend WithEvents cmdContinue As System.Windows.Forms.Button
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents grpSearchOption As System.Windows.Forms.GroupBox
    Friend WithEvents optName As System.Windows.Forms.RadioButton
    Friend WithEvents optNumber As System.Windows.Forms.RadioButton
    Friend WithEvents lstname As System.Windows.Forms.ListBox
    Friend WithEvents lstNumbers As System.Windows.Forms.ListBox
    Friend WithEvents txtSearchName As System.Windows.Forms.TextBox
    Friend WithEvents TxtCoopLoan As System.Windows.Forms.TextBox
    Friend WithEvents lblStepOne As System.Windows.Forms.Label
    Friend WithEvents btncompute1 As System.Windows.Forms.Button
    Friend WithEvents btnshowpenalties As System.Windows.Forms.Button
    Friend WithEvents chkPenalties As System.Windows.Forms.CheckBox
    Friend WithEvents picStep1 As WindowsApplication2.PanePanel
    Friend WithEvents picStep2 As WindowsApplication2.PanePanel
    Friend WithEvents picStep3 As WindowsApplication2.PanePanel
    Friend WithEvents PanePanel5 As WindowsApplication2.PanePanel
    Friend WithEvents txtRestructure As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lstMembers As System.Windows.Forms.ListView
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cboRestLoanNo As System.Windows.Forms.ComboBox
    Friend WithEvents txtLoanNo As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnCheckLoanNo As System.Windows.Forms.Button
    Friend WithEvents btnComaker As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents dgvComakerList As System.Windows.Forms.DataGridView
    Friend WithEvents btnDeleteRow As System.Windows.Forms.Button
    Friend WithEvents PanePanel3 As WindowsApplication2.PanePanel
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents rdCarryForward As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents cboMonthDeduction As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents btnSearch As System.Windows.Forms.Button
    Friend WithEvents btnRefresh As System.Windows.Forms.Button
    Friend WithEvents txtnoofpayment As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cboModeofPayment As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents btnPrint As System.Windows.Forms.Button
    Friend WithEvents rdRestructure As System.Windows.Forms.RadioButton
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents dgvStandardIntMethod As System.Windows.Forms.DataGridView
    Friend WithEvents dgvCustomizedIntMethod As System.Windows.Forms.DataGridView
    Friend WithEvents cboProject As System.Windows.Forms.ComboBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents btnAccounts As System.Windows.Forms.Button
End Class
