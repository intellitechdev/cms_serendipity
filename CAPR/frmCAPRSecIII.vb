Imports system.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmCAPRSecIII

    Public xParent As frmCAPRViewer
    Private gCon As New Clsappconfiguration
    Private DTLoanReceivable As DataSet

    Private Sub TotalInvestment()
        Try
            Dim xAmount As Decimal = 0
            If txtGovernTotal.Text IsNot Nothing Then
                xAmount += Format(CDec(txtGovernTotal.Text), "####0.00")
            End If
            If txtInvstTotal.Text IsNot Nothing Then
                xAmount += Format(CDec(txtInvstTotal.Text), "####0.00")
            End If
            If txtLongTermTotal.Text IsNot Nothing Then
                xAmount += Format(CDec(txtLongTermTotal.Text), "####0.00")
            End If

            For Each xRow As DataGridViewRow In grdInvestment.Rows
                If xRow.Cells(4).Value <> 0 Then
                    xAmount += CDec(xRow.Cells(4).Value)
                End If
            Next
            lblTotalInvestment.Text = Format(xAmount, "##,##0.00")

        Catch ex As Exception

        End Try
    End Sub

    Private Sub TotalExternalBorrowing()
        Dim xTotalBowing As Decimal = 0
        Dim xLocCur, xGovFin, xPrivFin As Decimal

        If txtAmntLocCur.Text = "" Then
            xLocCur = 0
        Else
            xLocCur = CDec(txtAmntLocCur.Text)
        End If
        If txtGovernFinInst.Text = "" Then
            xGovFin = 0
        Else
            xGovFin = CDec(txtGovernFinInst.Text)
        End If
        If txtPrivFinInst.Text = "" Then
            xPrivFin = 0
        Else
            xPrivFin = CDec(txtPrivFinInst.Text)
        End If
        xTotalBowing = xLocCur + xGovFin + xPrivFin
        For Each xrow As DataGridViewRow In GrdExternalBorrwing.Rows
            If xrow.IsNewRow = False Then
                xTotalBowing += CDec(GrdExternalBorrwing.Item(1, xrow.Index).Value)
            End If
        Next
        tsLblTotalExtBorwng.Text = Format(xTotalBowing, "##,##0.00")
    End Sub

    Private Sub LoadLoanRecievable()
        Dim sSqlCmd As String
        sSqlCmd = "CIMS_CAPRIIIe"
        Try
            DTLoanReceivable = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSqlCmd)
        Catch ex As Exception
            MsgBox("Error at LoadLoanRecievable in frmCAPRSecIII module" + vbCr + vbCr + ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error occur!")
        End Try
    End Sub

    Private Sub txtAmntForgnCur_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtAmntForgnCur.GotFocus
        txtAmntForgnCur.Text = Format(CDec(txtAmntForgnCur.Text), "####0.00")
        txtAmntForgnCur.SelectionStart = 0
        txtAmntForgnCur.SelectionLength = Len(txtAmntForgnCur.Text)
    End Sub

    Private Sub txtConverRate_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtConverRate.GotFocus
        txtConverRate.Text = Format(CDec(txtConverRate.Text), "####0.00")
        txtConverRate.SelectionStart = 0
        txtConverRate.SelectionLength = Len(txtConverRate.Text)
    End Sub

    Private Sub txtPrivFinInst_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPrivFinInst.GotFocus
        txtPrivFinInst.Text = Format(CDec(txtPrivFinInst.Text), "####0.00")
        txtPrivFinInst.SelectionStart = 0
        txtPrivFinInst.SelectionLength = Len(txtPrivFinInst.Text)
    End Sub

    Private Sub txtGovernFinInst_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtGovernFinInst.GotFocus
        txtGovernFinInst.Text = Format(CDec(txtGovernFinInst.Text), "####0.00")
        txtGovernFinInst.SelectionStart = 0
        txtGovernFinInst.SelectionLength = Len(txtGovernFinInst.Text)
    End Sub

    Private Sub txtLongTerm1_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtLongTerm1.GotFocus
        txtLongTerm1.Text = Format(CDec(txtLongTerm1.Text), "####0.00")
        txtLongTerm1.SelectionStart = 0
        txtLongTerm1.SelectionLength = Len(txtLongTerm1.Text)
    End Sub

    Private Sub txtLongTerm2_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtLongTerm2.GotFocus
        txtLongTerm2.Text = Format(CDec(txtLongTerm2.Text), "####0.00")
        txtLongTerm2.SelectionStart = 0
        txtLongTerm2.SelectionLength = Len(txtLongTerm2.Text)
    End Sub

    Private Sub txtLongTerm3_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtLongTerm3.GotFocus
        txtLongTerm3.Text = Format(CDec(txtLongTerm3.Text), "####0.00")
        txtLongTerm3.SelectionStart = 0
        txtLongTerm3.SelectionLength = Len(txtLongTerm3.Text)
    End Sub

    Private Sub txtInvst1_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtInvst1.GotFocus
        txtInvst1.Text = Format(CDec(txtInvst1.Text), "####0.00")
        txtInvst1.SelectionStart = 0
        txtInvst1.SelectionLength = Len(txtInvst1.Text)
    End Sub

    Private Sub txtInvst2_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtInvst2.GotFocus
        txtInvst2.Text = Format(CDec(txtInvst2.Text), "####0.00")
        txtInvst2.SelectionStart = 0
        txtInvst2.SelectionLength = Len(txtInvst2.Text)
    End Sub

    Private Sub txtInvst3_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtInvst3.GotFocus
        txtInvst3.Text = Format(CDec(txtInvst3.Text), "####0.00")
        txtInvst3.SelectionStart = 0
        txtInvst3.SelectionLength = Len(txtInvst3.Text)
    End Sub

    Private Sub txtGovern1_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtGovern1.GotFocus
        txtGovern1.Text = Format(CDec(txtGovern1.Text), "####0.00")
        txtGovern1.SelectionStart = 0
        txtGovern1.SelectionLength = Len(txtGovern1.Text)
    End Sub

    Private Sub txtGovern2_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtGovern2.GotFocus
        txtGovern2.Text = Format(CDec(txtGovern2.Text), "####0.00")
        txtGovern2.SelectionStart = 0
        txtGovern2.SelectionLength = Len(txtGovern2.Text)
    End Sub

    Private Sub txtGovern3_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtGovern3.GotFocus
        txtGovern3.Text = Format(CDec(txtGovern3.Text), "####0.00")
        txtGovern3.SelectionStart = 0
        txtGovern3.SelectionLength = Len(txtGovern3.Text)
    End Sub

    Private Sub txtWithdrwl1_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtWithdrwl1.GotFocus
        txtWithdrwl1.Text = Format(CDec(txtWithdrwl1.Text), "####0")
    End Sub

    Private Sub txtWithdrwl2_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtWithdrwl2.GotFocus
        txtWithdrwl2.Text = Format(CDec(txtWithdrwl2.Text), "####0")
    End Sub

    Private Sub txtWithdrwl4_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtWithdrwl4.GotFocus
        txtWithdrwl4.Text = Format(CDec(txtWithdrwl4.Text), "####0")
    End Sub

    Private Sub txtWithdrwl3_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtWithdrwl3.GotFocus
        txtWithdrwl3.Text = Format(CDec(txtWithdrwl3.Text), "####0.00")
    End Sub

    Private Sub txtWithdrwl5_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtWithdrwl5.GotFocus
        txtWithdrwl5.Text = Format(CDec(txtWithdrwl5.Text), "####0")
    End Sub

    Private Sub txtWithdrwl6_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtWithdrwl6.GotFocus
        txtWithdrwl6.Text = Format(CDec(txtWithdrwl6.Text), "####0.00")
    End Sub

    Private Sub txt30Dys1_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt30Dys1.GotFocus
        txt30Dys1.Text = Format(CDec(txt30Dys1.Text), "####0")
    End Sub

    Private Sub txt30Dys2_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt30Dys2.GotFocus
        txt30Dys2.Text = Format(CDec(txt30Dys2.Text), "####0")
    End Sub

    Private Sub txt30Dys3_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt30Dys3.GotFocus
        txt30Dys3.Text = Format(CDec(txt30Dys3.Text), "####0.00")
    End Sub

    Private Sub txt30Dys4_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt30Dys4.GotFocus
        txt30Dys4.Text = Format(CDec(txt30Dys4.Text), "####0")
    End Sub

    Private Sub txt30Dys5_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt30Dys5.GotFocus
        txt30Dys5.Text = Format(CDec(txt30Dys5.Text), "####0")
    End Sub

    Private Sub txt30Dys6_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt30Dys6.GotFocus
        txt30Dys6.Text = Format(CDec(txt30Dys6.Text), "####0.00")
    End Sub

    Private Sub txt3090Dys1_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt3090Dys1.GotFocus
        txt3090Dys1.Text = Format(CDec(txt3090Dys1.Text), "####0")
    End Sub

    Private Sub txt3090Dys2_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt3090Dys2.GotFocus
        txt3090Dys2.Text = Format(CDec(txt3090Dys2.Text), "####0")
    End Sub

    Private Sub txt3090Dys3_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt3090Dys3.GotFocus
        txt3090Dys3.Text = Format(CDec(txt3090Dys3.Text), "####0.00")
    End Sub

    Private Sub txt3090Dys4_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt3090Dys4.GotFocus
        txt3090Dys4.Text = Format(CDec(txt3090Dys4.Text), "####0")
    End Sub

    Private Sub txt3090Dys5_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt3090Dys5.GotFocus
        txt3090Dys5.Text = Format(CDec(txt3090Dys5.Text), "####0")
    End Sub

    Private Sub txt3090Dys6_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt3090Dys6.GotFocus
        txt3090Dys6.Text = Format(CDec(txt3090Dys6.Text), "####0.00")
    End Sub

    Private Sub txt901yr1_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt901yr1.GotFocus
        txt901yr1.Text = Format(CDec(txt901yr1.Text), "####0")
    End Sub

    Private Sub txt901yr2_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt901yr2.GotFocus
        txt901yr2.Text = Format(CDec(txt901yr2.Text), "####0")
    End Sub

    Private Sub txt901yr3_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt901yr3.GotFocus
        txt901yr3.Text = Format(CDec(txt901yr3.Text), "####0.00")
    End Sub

    Private Sub txt901yr4_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt901yr4.GotFocus
        txt901yr4.Text = Format(CDec(txt901yr4.Text), "####0")
    End Sub

    Private Sub txt901yr5_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt901yr5.GotFocus
        txt901yr5.Text = Format(CDec(txt901yr5.Text), "####0")
    End Sub

    Private Sub txt901yr6_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt901yr6.GotFocus
        txt901yr6.Text = Format(CDec(txt901yr6.Text), "####0.00")
    End Sub

    Private Sub txt15yr1_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt15yr1.GotFocus
        txt15yr1.Text = Format(CDec(txt15yr1.Text), "####0")
    End Sub

    Private Sub txt15yr2_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt15yr2.GotFocus
        txt15yr2.Text = Format(CDec(txt15yr2.Text), "####0")
    End Sub

    Private Sub txt15yr3_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt15yr3.GotFocus
        txt15yr3.Text = Format(CDec(txt15yr3.Text), "####0.00")
    End Sub

    Private Sub txt15yr4_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt15yr4.GotFocus
        txt15yr4.Text = Format(CDec(txt15yr4.Text), "####0")
    End Sub

    Private Sub txt15yr5_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt15yr5.GotFocus
        txt15yr5.Text = Format(CDec(txt15yr5.Text), "####0")
    End Sub

    Private Sub txt15yr6_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt15yr6.GotFocus
        txt15yr6.Text = Format(CDec(txt15yr6.Text), "####0.00")
    End Sub

    Private Sub txt5yr1_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt5yr1.GotFocus
        txt5yr1.Text = Format(CDec(txt5yr1.Text), "####0")
    End Sub

    Private Sub txt5yr2_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt5yr2.GotFocus
        txt5yr2.Text = Format(CDec(txt5yr2.Text), "####0")
    End Sub

    Private Sub txt5yr3_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt5yr3.GotFocus
        txt5yr3.Text = Format(CDec(txt5yr3.Text), "####0.00")
    End Sub

    Private Sub txt5yr4_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt5yr4.GotFocus
        txt5yr4.Text = Format(CDec(txt5yr4.Text), "####0")
    End Sub

    Private Sub txt5yr5_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt5yr5.GotFocus
        txt5yr5.Text = Format(CDec(txt5yr5.Text), "####0")
    End Sub

    Private Sub txt5yr6_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt5yr6.GotFocus
        txt5yr6.Text = Format(CDec(txt5yr6.Text), "####0.00")
    End Sub

    Private Sub txtNonWthdrwl1_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNonWthdrwl1.GotFocus
        txtNonWthdrwl1.Text = Format(CDec(txtNonWthdrwl1.Text), "####0")
    End Sub

    Private Sub txtNonWthdrwl2_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNonWthdrwl2.GotFocus
        txtNonWthdrwl2.Text = Format(CDec(txtNonWthdrwl2.Text), "####0")
    End Sub

    Private Sub txtNonWthdrwl3_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNonWthdrwl3.GotFocus
        txtNonWthdrwl3.Text = Format(CDec(txtNonWthdrwl3.Text), "####0.00")
    End Sub

    Private Sub txtNonWthdrwl4_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNonWthdrwl4.GotFocus
        txtNonWthdrwl4.Text = Format(CDec(txtNonWthdrwl4.Text), "####0")
    End Sub

    Private Sub txtNonWthdrwl5_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNonWthdrwl5.GotFocus
        txtNonWthdrwl5.Text = Format(CDec(txtNonWthdrwl5.Text), "####0")
    End Sub

    Private Sub txtNonWthdrwl6_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNonWthdrwl6.GotFocus
        txtNonWthdrwl6.Text = Format(CDec(txtNonWthdrwl6.Text), "####0.00")
    End Sub





    Private Sub NUmericTxtbox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtLongTerm1.KeyPress, txtLongTerm2.KeyPress, _
                        txtLongTerm3.KeyPress, txtLongTermTotal.KeyPress, _
                        txtInvst1.KeyPress, txtInvst2.KeyPress, txtInvst3.KeyPress, txtInvstTotal.KeyPress, _
                        txtGovern1.KeyPress, txtGovern2.KeyPress, txtGovern3.KeyPress, txtGovernTotal.KeyPress, _
                        txtAmntForgnCur.KeyPress, txtConverRate.KeyPress, txtGovernFinInst.KeyPress, txtPrivFinInst.KeyPress, _
                        txtWithdrwl1.KeyPress, txtWithdrwl2.KeyPress, txtWithdrwl3.KeyPress, _
                        txtWithdrwl4.KeyPress, txtWithdrwl5.KeyPress, txtWithdrwl6.KeyPress, _
                        txt30Dys1.KeyPress, txt30Dys2.KeyPress, txt30Dys3.KeyPress, _
                        txt30Dys4.KeyPress, txt30Dys5.KeyPress, txt30Dys6.KeyPress, _
                        txt3090Dys1.KeyPress, txt3090Dys2.KeyPress, txt3090Dys3.KeyPress, _
                        txt3090Dys4.KeyPress, txt3090Dys5.KeyPress, txt3090Dys6.KeyPress, _
                        txt901yr1.KeyPress, txt901yr2.KeyPress, txt901yr3.KeyPress, _
                        txt901yr4.KeyPress, txt901yr5.KeyPress, txt901yr6.KeyPress, _
                        txt15yr1.KeyPress, txt15yr2.KeyPress, txt15yr3.KeyPress, _
                        txt15yr4.KeyPress, txt15yr5.KeyPress, txt15yr6.KeyPress, _
                        txt5yr1.KeyPress, txt5yr2.KeyPress, txt5yr3.KeyPress, _
                        txt5yr4.KeyPress, txt5yr5.KeyPress, txt5yr6.KeyPress, _
                        txtNonWthdrwl1.KeyPress, txtNonWthdrwl2.KeyPress, txtNonWthdrwl3.KeyPress, _
                        txtNonWthdrwl4.KeyPress, txtNonWthdrwl5.KeyPress, txtNonWthdrwl6.KeyPress
        If e.KeyChar <> "." Then
            If Not (e.KeyChar.IsDigit(e.KeyChar)) And _
            e.KeyChar <> ChrW(Keys.Back) Then
                e.Handled = True
            End If
        End If

    End Sub

    Private Sub txtLongTerm1_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtLongTerm1.LostFocus
        Try
            txtLongTerm1.Text = Format(CDec(txtLongTerm1.Text), "##,##0.00")
        Catch ex As Exception
            txtLongTerm1.Text = "0.00"
        End Try
    End Sub

    Private Sub txtLongTerm1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtLongTerm1.TextChanged

        If txtLongTerm1.Text = "" Then
            txtLongTerm1.Text = 0
        End If

        Try
            txtLongTermTotal.Text = Format(CDec(IIf(txtLongTerm1.Text = "", 0, txtLongTerm1.Text) + _
                                                  CDec(IIf(txtLongTerm2.Text = "", 0, txtLongTerm2.Text)) + _
                                                  CDec(IIf(txtLongTerm3.Text = "", 0, txtLongTerm3.Text))), "##,##0.00")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub txtLongTerm2_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtLongTerm2.LostFocus
        Try
            txtLongTerm2.Text = Format(CDec(txtLongTerm2.Text), "##,##0.00")
        Catch ex As Exception
            txtLongTerm2.Text = "0.00"
        End Try
    End Sub

    Private Sub txtLongTerm2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtLongTerm2.TextChanged
        If txtLongTerm2.Text = "" Then
            txtLongTerm2.Text = 0
        End If
        Try
            txtLongTermTotal.Text = Format(CDec(IIf(txtLongTerm1.Text = "", 0, txtLongTerm1.Text) + _
                                                  CDec(IIf(txtLongTerm2.Text = "", 0, txtLongTerm2.Text)) + _
                                                  CDec(IIf(txtLongTerm3.Text = "", 0, txtLongTerm3.Text))), "##,##0.00")
        Catch ex As Exception

        End Try

    End Sub

    Private Sub txtLongTerm3_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtLongTerm3.LostFocus
        Try
            txtLongTerm3.Text = Format(CDec(txtLongTerm3.Text), "##,##0.00")
        Catch ex As Exception
            txtLongTerm3.Text = "0.00"
        End Try
    End Sub

    Private Sub txtLongTerm3_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtLongTerm3.TextChanged
        If txtLongTerm3.Text = "" Then
            txtLongTerm3.Text = 0
        End If
        Try
            txtLongTermTotal.Text = Format(CDec(IIf(txtLongTerm1.Text = "", 0, txtLongTerm1.Text) + _
                                                   CDec(IIf(txtLongTerm2.Text = "", 0, txtLongTerm2.Text)) + _
                                                   CDec(IIf(txtLongTerm3.Text = "", 0, txtLongTerm3.Text))), "##,##0.00")
        Catch ex As Exception

        End Try

    End Sub

    Private Sub txtInvst1_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtInvst1.LostFocus
        Try
            txtInvst1.Text = Format(CDec(txtInvst1.Text), "##,##0.00")
        Catch ex As Exception
            txtInvst1.Text = "0.00"
        End Try
    End Sub

    Private Sub txtInvst1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtInvst1.TextChanged
        If txtInvst1.Text = "" Then
            txtInvst1.Text = 0
        End If
        Try
            txtInvstTotal.Text = Format(CDec(IIf(txtInvst2.Text = "", 0, txtInvst2.Text) + _
                                        CDec(IIf(txtInvst1.Text = "", 0, txtInvst1.Text)) + _
                                        CDec(IIf(txtInvst3.Text = "", 0, txtInvst3.Text))), "##,##0.00")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub txtInvst2_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtInvst2.LostFocus
        Try
            txtInvst2.Text = Format(CDec(txtInvst2.Text), "##,##0.00")
        Catch ex As Exception
            txtInvst2.Text = "0.00"
        End Try
    End Sub

    Private Sub txtInvst2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtInvst2.TextChanged
        If txtInvst2.Text = "" Then
            txtInvst2.Text = 0
        End If
        Try
            txtInvstTotal.Text = Format(CDec(IIf(txtInvst2.Text = "", 0, txtInvst2.Text) + _
                                        CDec(IIf(txtInvst1.Text = "", 0, txtInvst1.Text)) + _
                                        CDec(IIf(txtInvst3.Text = "", 0, txtInvst3.Text))), "##,##0.00")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub txtInvst3_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtInvst3.LostFocus
        Try
            txtInvst3.Text = Format(CDec(txtInvst3.Text), "##,##0.00")
        Catch ex As Exception
            txtInvst3.Text = "0.00"
        End Try
    End Sub

    Private Sub txtInvst3_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtInvst3.TextChanged
        If txtInvst3.Text = "" Then
            txtInvst3.Text = 0
        End If
        Try
            txtInvstTotal.Text = Format(CDec(IIf(txtInvst2.Text = "", 0, txtInvst2.Text) + _
                                     CDec(IIf(txtInvst1.Text = "", 0, txtInvst1.Text)) + _
                                     CDec(IIf(txtInvst3.Text = "", 0, txtInvst3.Text))), "##,##0.00")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub txtGovern1_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtGovern1.LostFocus
        Try
            txtGovern1.Text = Format(CDec(txtGovern1.Text), "##,##0.00")
        Catch ex As Exception
            txtGovern1.Text = "0.00"
        End Try
    End Sub

    Private Sub txtGovern1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtGovern1.TextChanged
        If txtGovern1.Text = "" Then
            txtGovern1.Text = 0
        End If
        Try
            txtGovernTotal.Text = Format(CDec(IIf(txtGovern1.Text = "", 0, txtGovern1.Text) + _
                                      CDec(IIf(txtGovern2.Text = "", 0, txtGovern2.Text)) + _
                                      CDec(IIf(txtGovern3.Text = "", 0, txtGovern3.Text))), "##,##0.00")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub txtGovern2_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtGovern2.LostFocus
        Try
            txtGovern2.Text = Format(CDec(txtGovern2.Text), "##,##0.00")
        Catch ex As Exception
            txtGovern2.Text = "0.00"
        End Try
    End Sub

    Private Sub txtGovern2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtGovern2.TextChanged
        If txtGovern2.Text = "" Then
            txtGovern2.Text = 0
        End If
        Try
            txtGovernTotal.Text = Format(CDec(IIf(txtGovern1.Text = "", 0, txtGovern1.Text) + _
                                      CDec(IIf(txtGovern2.Text = "", 0, txtGovern2.Text)) + _
                                      CDec(IIf(txtGovern3.Text = "", 0, txtGovern3.Text))), "##,##0.00")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub txtGovern3_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtGovern3.LostFocus
        Try
            txtGovern3.Text = Format(CDec(txtGovern3.Text), "##,##0.00")
        Catch ex As Exception
            txtGovern3.Text = "0.00"
        End Try
    End Sub

    Private Sub txtGovern3_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtGovern3.TextChanged
        If txtGovern3.Text = "" Then
            txtGovern3.Text = 0
        End If
        Try
            txtGovernTotal.Text = Format(CDec(IIf(txtGovern1.Text = "", 0, txtGovern1.Text) + _
                                      CDec(IIf(txtGovern2.Text = "", 0, txtGovern2.Text)) + _
                                      CDec(IIf(txtGovern3.Text = "", 0, txtGovern3.Text))), "##,##0.00")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub grdInvestment_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdInvestment.CellValueChanged
        If grdInvestment.Rows.Count > 1 Then
            Select Case e.ColumnIndex
                Case 0

                Case Else
                    If grdInvestment.Item(e.ColumnIndex, e.RowIndex).Value = "" Then
                        grdInvestment.Item(e.ColumnIndex, e.RowIndex).Value = "0.00"
                    Else
                        If Char.IsNumber(grdInvestment.Item(e.ColumnIndex, e.RowIndex).Value) = False Then
                            grdInvestment.Item(e.ColumnIndex, e.RowIndex).Value = "0.00"
                        End If
                    End If
                    Dim Amnt1, Amnt2, Amnt3 As Decimal
                    If grdInvestment.Item(1, e.RowIndex).Value = Nothing Then
                        Amnt1 = "0.00"
                    Else
                        Amnt1 = Format(CDec(grdInvestment.Item(1, e.RowIndex).Value), "####0.00")
                    End If
                    If grdInvestment.Item(2, e.RowIndex).Value = Nothing Then
                        Amnt2 = "0.00"
                    Else
                        Amnt2 = Format(CDec(grdInvestment.Item(2, e.RowIndex).Value), "####0.00")
                    End If
                    If grdInvestment.Item(3, e.RowIndex).Value = Nothing Then
                        Amnt3 = "0.00"
                    Else
                        Amnt3 = Format(CDec(grdInvestment.Item(3, e.RowIndex).Value), "####0.00")
                    End If
                    grdInvestment.Item(4, e.RowIndex).Value = Format(CDec(Amnt1) + CDec(Amnt2) + CDec(Amnt3), "##,##0.00")
                    grdInvestment.Item(e.ColumnIndex, e.RowIndex).Value = Format(CDec(grdInvestment.Item(e.ColumnIndex, e.RowIndex).Value), "##,##0.00")
            End Select
            TotalInvestment()
        End If
    End Sub

    Private Sub txtLongTermTotal_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtLongTermTotal.TextChanged, txtInvstTotal.TextChanged, txtGovernTotal.TextChanged
        TotalInvestment()
    End Sub

    Private Sub txtAmntForgnCur_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtAmntForgnCur.LostFocus
        txtAmntForgnCur.Text = Format(CDec(txtAmntForgnCur.Text), "##,##0.00")
    End Sub

    Private Sub txtAmntForgnCur_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtAmntForgnCur.TextChanged
        Dim xForeignAmnt As Decimal = 0
        Dim xConversionRate As Decimal = 0

        If txtAmntForgnCur.Text = "" Then
            xForeignAmnt = 0
        Else
            xForeignAmnt = CDec(txtAmntForgnCur.Text)
        End If

        If txtConverRate.Text = "" Then
            xConversionRate = 0
        Else
            xConversionRate = CDec(txtConverRate.Text)
        End If

        txtAmntLocCur.Text = Format(xForeignAmnt * xConversionRate, "##,##0.00")

    End Sub

    Private Sub txtConverRate_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtConverRate.LostFocus
        txtConverRate.Text = Format(CDec(txtConverRate.Text), "##,##0.00")
    End Sub

    Private Sub txtConverRate_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtConverRate.TextChanged
        Dim xForeignAmnt As Decimal = 0
        Dim xConversionRate As Decimal = 0

        If txtAmntForgnCur.Text = "" Then
            xForeignAmnt = 0
        Else
            xForeignAmnt = CDec(txtAmntForgnCur.Text)
        End If

        If txtConverRate.Text = "" Then
            xConversionRate = 0
        Else
            xConversionRate = CDec(txtConverRate.Text)
        End If

        txtAmntLocCur.Text = Format(xForeignAmnt * xConversionRate, "##,##0.00")
    End Sub

    Private Sub txtGovernFinInst_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtGovernFinInst.LostFocus
        txtGovernFinInst.Text = Format(CDec(txtGovernFinInst.Text), "##,##0.00")
    End Sub

    Private Sub txtPrivFinInst_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPrivFinInst.LostFocus
        txtPrivFinInst.Text = Format(CDec(txtPrivFinInst.Text), "##,##0.00")
    End Sub

    Private Sub GrdExternalBorrwing_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles GrdExternalBorrwing.CellValueChanged
        If GrdExternalBorrwing.Rows.Count > 1 Then
            If Char.IsNumber(GrdExternalBorrwing.Item(1, e.RowIndex).Value) = False Then
                GrdExternalBorrwing.Item(1, e.RowIndex).Value = "0.00"
            End If
            If e.ColumnIndex = 1 Then
                GrdExternalBorrwing.Item(e.ColumnIndex, e.RowIndex).Value = Format(CDec(GrdExternalBorrwing.Item(e.ColumnIndex, e.RowIndex).Value), "##,##0.00")
            End If
        End If
        TotalExternalBorrowing()
    End Sub

    Private Sub txtWithdrwl1_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtWithdrwl1.LostFocus
        txtWithdrwl1.Text = Format(CDec(txtWithdrwl1.Text), "##,##0")
    End Sub

    Private Sub txtWithdrwl2_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtWithdrwl2.LostFocus
        txtWithdrwl2.Text = Format(CDec(txtWithdrwl2.Text), "##,##0")
    End Sub

    Private Sub txtWithdrwl3_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtWithdrwl3.LostFocus
        txtWithdrwl3.Text = Format(CDec(txtWithdrwl3.Text), "##,##0.00")
    End Sub

    Private Sub txtWithdrwl4_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtWithdrwl4.LostFocus
        txtWithdrwl4.Text = Format(CDec(txtWithdrwl4.Text), "##,##0")
    End Sub

    Private Sub txtWithdrwl5_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtWithdrwl5.LostFocus
        txtWithdrwl5.Text = Format(CDec(txtWithdrwl5.Text), "##,##0")
    End Sub

    Private Sub txtWithdrwl6_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtWithdrwl6.LostFocus
        txtWithdrwl6.Text = Format(CDec(txtWithdrwl6.Text), "##,##0.00")
    End Sub

    Private Sub txt30Dys1_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt30Dys1.LostFocus
        txt30Dys1.Text = Format(CDec(txt30Dys1.Text), "##,##0")
    End Sub

    Private Sub txt30Dys2_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt30Dys2.LostFocus
        txt30Dys2.Text = Format(CDec(txt30Dys2.Text), "##,##0")
    End Sub

    Private Sub txt30Dys3_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt30Dys3.LostFocus
        txt30Dys3.Text = Format(CDec(txt30Dys3.Text), "##,##0.00")
    End Sub

    Private Sub txt30Dys4_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt30Dys4.LostFocus
        txt30Dys4.Text = Format(CDec(txt30Dys4.Text), "##,##0")
    End Sub

    Private Sub txt30Dys5_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt30Dys5.LostFocus
        txt30Dys5.Text = Format(CDec(txt30Dys5.Text), "##,##0")
    End Sub

    Private Sub txt30Dys6_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt30Dys6.LostFocus
        txt30Dys6.Text = Format(CDec(txt30Dys6.Text), "##,##0.00")
    End Sub

    Private Sub txt3090Dys1_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt3090Dys1.LostFocus
        txt3090Dys1.Text = Format(CDec(txt3090Dys1.Text), "##,##0")
    End Sub

    Private Sub txt3090Dys2_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt3090Dys2.LostFocus
        txt3090Dys2.Text = Format(CDec(txt3090Dys2.Text), "##,##0")
    End Sub

    Private Sub txt3090Dys3_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt3090Dys3.LostFocus
        txt3090Dys3.Text = Format(CDec(txt3090Dys3.Text), "##,##0.00")
    End Sub

    Private Sub txt3090Dys4_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt3090Dys4.LostFocus
        txt3090Dys4.Text = Format(CDec(txt3090Dys4.Text), "##,##0")
    End Sub

    Private Sub txt3090Dys5_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt3090Dys5.LostFocus
        txt3090Dys5.Text = Format(CDec(txt3090Dys5.Text), "##,##0")
    End Sub

    Private Sub txt3090Dys6_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt3090Dys6.LostFocus
        txt3090Dys6.Text = Format(CDec(txt3090Dys6.Text), "##,##0.00")
    End Sub

    Private Sub txt901yr1_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt901yr1.LostFocus
        txt901yr1.Text = Format(CDec(txt901yr1.Text), "##,##0")
    End Sub

    Private Sub txt901yr2_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt901yr2.LostFocus
        txt901yr2.Text = Format(CDec(txt901yr2.Text), "##,##0")
    End Sub

    Private Sub txt901yr3_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt901yr3.LostFocus
        txt901yr3.Text = Format(CDec(txt901yr3.Text), "##,##0.00")
    End Sub

    Private Sub txt901yr4_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt901yr4.LostFocus
        txt901yr4.Text = Format(CDec(txt901yr4.Text), "##,##0")
    End Sub

    Private Sub txt901yr5_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt901yr5.LostFocus
        txt901yr5.Text = Format(CDec(txt901yr5.Text), "##,##0")
    End Sub

    Private Sub txt901yr6_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt901yr6.LostFocus
        txt901yr6.Text = Format(CDec(txt901yr6.Text), "##,##0.00")
    End Sub

    Private Sub txt15yr1_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt15yr1.LostFocus
        txt15yr1.Text = Format(CDec(txt15yr1.Text), "##,##0")
    End Sub

    Private Sub txt15yr2_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt15yr2.LostFocus
        txt15yr2.Text = Format(CDec(txt15yr2.Text), "##,##0")
    End Sub

    Private Sub txt15yr3_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt15yr3.LostFocus
        txt15yr3.Text = Format(CDec(txt15yr3.Text), "##,##0.00")
    End Sub

    Private Sub txt15yr4_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt15yr4.LostFocus
        txt15yr4.Text = Format(CDec(txt15yr4.Text), "##,##0")
    End Sub

    Private Sub txt15yr5_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt15yr5.LostFocus
        txt15yr5.Text = Format(CDec(txt15yr5.Text), "##,##0")
    End Sub

    Private Sub txt15yr6_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt15yr6.LostFocus
        txt15yr6.Text = Format(CDec(txt15yr6.Text), "##,##0.00")
    End Sub

    Private Sub txt5yr1_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt5yr1.LostFocus
        txt5yr1.Text = Format(CDec(txt5yr1.Text), "##,##0")
    End Sub

    Private Sub txt5yr2_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt5yr2.LostFocus
        txt5yr2.Text = Format(CDec(txt5yr2.Text), "##,##0")
    End Sub

    Private Sub txt5yr3_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt5yr3.LostFocus
        txt5yr3.Text = Format(CDec(txt5yr3.Text), "##,##0.00")
    End Sub

    Private Sub txt5yr4_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt5yr4.LostFocus
        txt5yr4.Text = Format(CDec(txt5yr4.Text), "##,##0")
    End Sub

    Private Sub txt5yr5_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt5yr5.LostFocus
        txt5yr5.Text = Format(CDec(txt5yr5.Text), "##,##0")
    End Sub

    Private Sub txt5yr6_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt5yr6.LostFocus
        txt5yr6.Text = Format(CDec(txt5yr6.Text), "##,##0.00")
    End Sub

    Private Sub txtNonWthdrwl1_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNonWthdrwl1.LostFocus
        txtNonWthdrwl1.Text = Format(CDec(txtNonWthdrwl1.Text), "##,##0")
    End Sub

    Private Sub txtNonWthdrwl2_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNonWthdrwl2.LostFocus
        txtNonWthdrwl2.Text = Format(CDec(txtNonWthdrwl2.Text), "##,##0")
    End Sub

    Private Sub txtNonWthdrwl3_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNonWthdrwl3.LostFocus
        txtNonWthdrwl3.Text = Format(CDec(txtNonWthdrwl3.Text), "##,##0.00")
    End Sub

    Private Sub txtNonWthdrwl4_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNonWthdrwl4.LostFocus
        txtNonWthdrwl4.Text = Format(CDec(txtNonWthdrwl4.Text), "##,##0")
    End Sub

    Private Sub txtNonWthdrwl5_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNonWthdrwl5.LostFocus
        txtNonWthdrwl5.Text = Format(CDec(txtNonWthdrwl5.Text), "##,##0")
    End Sub

    Private Sub txtNonWthdrwl6_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNonWthdrwl6.LostFocus
        txtNonWthdrwl6.Text = Format(CDec(txtNonWthdrwl6.Text), "##,##0.00")
    End Sub

    Private Sub grdSavingDeposits_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdSavingDeposits.CellValueChanged
        If grdSavingDeposits.Rows.Count > 1 Then
            Select Case e.ColumnIndex
                Case 1
                    If Char.IsNumber(grdSavingDeposits.Item(1, e.RowIndex).Value) = False Then
                        grdSavingDeposits.Item(1, e.RowIndex).Value = "0"
                    End If
                Case 2
                    If Char.IsNumber(grdSavingDeposits.Item(2, e.RowIndex).Value) = False Then
                        grdSavingDeposits.Item(2, e.RowIndex).Value = "0"
                    End If
                Case 3
                    If Char.IsNumber(grdSavingDeposits.Item(3, e.RowIndex).Value) = False Then
                        grdSavingDeposits.Item(3, e.RowIndex).Value = "0.00"
                    End If
                Case 4
                    If Char.IsNumber(grdSavingDeposits.Item(4, e.RowIndex).Value) = False Then
                        grdSavingDeposits.Item(4, e.RowIndex).Value = "0"
                    End If
                Case 5
                    If Char.IsNumber(grdSavingDeposits.Item(5, e.RowIndex).Value) = False Then
                        grdSavingDeposits.Item(5, e.RowIndex).Value = "0"
                    End If
                Case 6
                    If Char.IsNumber(grdSavingDeposits.Item(6, e.RowIndex).Value) = False Then
                        grdSavingDeposits.Item(6, e.RowIndex).Value = "0.00"
                    End If
            End Select
            If e.ColumnIndex = 3 Or e.ColumnIndex = 6 Then
                grdSavingDeposits.Item(e.ColumnIndex, e.RowIndex).Value = Format(CDec(grdSavingDeposits.Item(e.ColumnIndex, e.RowIndex).Value), "##,##0.00")
            Else
                If e.ColumnIndex <> 0 Then
                    grdSavingDeposits.Item(e.ColumnIndex, e.RowIndex).Value = Format(CDec(grdSavingDeposits.Item(e.ColumnIndex, e.RowIndex).Value), "##,##0")
                End If
            End If
        End If
    End Sub

    Private Sub txtAmntLocCur_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtAmntLocCur.TextChanged
        TotalExternalBorrowing()
    End Sub

    Private Sub txtGovernFinInst_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtGovernFinInst.TextChanged
        TotalExternalBorrowing()
    End Sub

    Private Sub txtPrivFinInst_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtPrivFinInst.TextChanged
        TotalExternalBorrowing()
    End Sub

    Private Sub btnPrevious_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrevious.Click
        Select Case TabControl1.SelectedIndex
            Case 4
                TabControl1.SelectTab(3)

            Case 3
                TabControl1.SelectTab(2)

            Case 2
                TabControl1.SelectTab(1)

            Case 1
                TabControl1.SelectTab(0)

            Case 0
                xFormSec1.Show()
                Me.Hide()
        End Select
    End Sub

    Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNext.Click
        Select Case TabControl1.SelectedIndex
            Case 0
                TabControl1.SelectTab(1)

            Case 1
                TabControl1.SelectTab(2)

            Case 2
                TabControl1.SelectTab(3)

            Case 3
                TabControl1.SelectTab(4)

            Case 4
                VLongTerm1 = CDec(txtLongTerm1.Text)
                VLongTerm2 = CDec(txtLongTerm2.Text)
                VLongTerm3 = CDec(txtLongTerm3.Text)
                VInvst1 = CDec(txtInvst1.Text)
                VInvst2 = CDec(txtInvst2.Text)
                VInvst3 = CDec(txtInvst3.Text)
                VGovernSec1 = CDec(txtGovern1.Text)
                VGovernSec2 = CDec(txtGovern2.Text)
                VGovernSec3 = CDec(txtGovern3.Text)

                VBColType = ""
                VBAmnt1 = ""
                VBAmnt2 = ""
                VBAmnt3 = ""
                For Each xRow1 As DataGridViewRow In grdInvestment.Rows
                    If xRow1.IsNewRow = False Then
                        If VBColType = "" Then
                            If xRow1.Cells(0).Value = Nothing Then
                                VBColType &= ""
                            Else
                                VBColType &= xRow1.Cells(0).Value.ToString()
                            End If
                        Else
                            If xRow1.Cells(0).Value = Nothing Then
                                VBColType &= "▄"
                            Else
                                VBColType &= "▄" & xRow1.Cells(0).Value.ToString()
                            End If
                        End If

                        If VBAmnt1 = "" Then
                            If xRow1.Cells(1).Value = Nothing Then
                                VBAmnt1 &= "0.00"
                            Else
                                VBAmnt1 &= xRow1.Cells(1).Value.ToString()
                            End If
                        Else
                            If xRow1.Cells(1).Value = Nothing Then
                                VBAmnt1 &= "▄0.00"
                            Else
                                VBAmnt1 &= "▄" & xRow1.Cells(1).Value.ToString()
                            End If
                        End If

                        If VBAmnt2 = "" Then
                            If xRow1.Cells(2).Value = Nothing Then
                                VBAmnt2 &= "0.00"
                            Else
                                VBAmnt2 &= xRow1.Cells(2).Value.ToString()
                            End If
                        Else
                            If xRow1.Cells(2).Value = Nothing Then
                                VBAmnt2 &= "▄0.00"
                            Else
                                VBAmnt2 &= "▄" & xRow1.Cells(2).Value.ToString()
                            End If
                        End If

                        If VBAmnt3 = "" Then
                            If xRow1.Cells(3).Value = Nothing Then
                                VBAmnt3 &= "0.00"
                            Else
                                VBAmnt3 &= xRow1.Cells(3).Value.ToString()
                            End If
                        Else
                            If xRow1.Cells(3).Value = Nothing Then
                                VBAmnt3 &= "▄0.00"
                            Else
                                VBAmnt3 &= "▄" & xRow1.Cells(3).Value.ToString()
                            End If
                        End If
                    End If
                Next

                VCForeignAmnt = CDec(txtAmntForgnCur.Text)
                VCConvRte = CDec(txtConverRate.Text)
                VCLocCurAmt = CDec(txtAmntLocCur.Text)
                VCGovFinInst = CDec(txtGovernFinInst.Text)
                VCPrivFinIns = CDec(txtPrivFinInst.Text)

                VCCreditSource = ""
                VCAmnt = ""
                For Each xRow2 As DataGridViewRow In GrdExternalBorrwing.Rows
                    If xRow2.IsNewRow = False Then
                        If VCCreditSource = "" Then
                            VCCreditSource = xRow2.Cells(0).Value.ToString
                        Else
                            VCCreditSource &= "▄" & xRow2.Cells(0).Value.ToString
                        End If
                        If VCAmnt = "" Then
                            VCAmnt = xRow2.Cells(1).Value.ToString
                        Else
                            VCAmnt &= "▄" & xRow2.Cells(1).Value.ToString
                        End If
                    End If
                Next

                VDWithdwble1 = CDec(txtWithdrwl1.Text)
                VDWithdwble2 = CDec(txtWithdrwl2.Text)
                VDWithdwble3 = CDec(txtWithdrwl3.Text)
                VDWithdwble4 = CDec(txtWithdrwl4.Text)
                VDWithdwble5 = CDec(txtWithdrwl5.Text)
                VDWithdwble6 = CDec(txtWithdrwl6.Text)
                VD30days1 = CDec(txt30Dys1.Text)
                VD30days2 = CDec(txt30Dys2.Text)
                VD30days3 = CDec(txt30Dys3.Text)
                VD30days4 = CDec(txt30Dys4.Text)
                VD30days5 = CDec(txt30Dys5.Text)
                VD30days6 = CDec(txt30Dys6.Text)
                VD30to90days1 = CDec(txt3090Dys1.Text)
                VD30to90days2 = CDec(txt3090Dys2.Text)
                VD30to90days3 = CDec(txt3090Dys3.Text)
                VD30to90days4 = CDec(txt3090Dys4.Text)
                VD30to90days5 = CDec(txt3090Dys5.Text)
                VD30to90days6 = CDec(txt3090Dys6.Text)
                VD90to1yr1 = CDec(txt901yr1.Text)
                VD90to1yr2 = CDec(txt901yr2.Text)
                VD90to1yr3 = CDec(txt901yr3.Text)
                VD90to1yr4 = CDec(txt901yr4.Text)
                VD90to1yr5 = CDec(txt901yr5.Text)
                VD90to1yr6 = CDec(txt901yr6.Text)
                VD1to5yr1 = CDec(txt15yr1.Text)
                VD1to5yr2 = CDec(txt15yr2.Text)
                VD1to5yr3 = CDec(txt15yr3.Text)
                VD1to5yr4 = CDec(txt15yr4.Text)
                VD1to5yr5 = CDec(txt15yr5.Text)
                VD1to5yr6 = CDec(txt15yr6.Text)
                VD5years1 = CDec(txt5yr1.Text)
                VD5years2 = CDec(txt5yr2.Text)
                VD5years3 = CDec(txt5yr3.Text)
                VD5years4 = CDec(txt5yr4.Text)
                VD5years5 = CDec(txt5yr5.Text)
                VD5years6 = CDec(txt5yr6.Text)
                VDNonWitdrwble1 = CDec(txtNonWthdrwl1.Text)
                VDNonWitdrwble2 = CDec(txtNonWthdrwl2.Text)
                VDNonWitdrwble3 = CDec(txtNonWthdrwl3.Text)
                VDNonWitdrwble4 = CDec(txtNonWthdrwl4.Text)
                VDNonWitdrwble5 = CDec(txtNonWthdrwl5.Text)
                VDNonWitdrwble6 = CDec(txtNonWthdrwl6.Text)

                VDDepositType = ""
                VDRegNoMemWthDepAcnt = ""
                VDRegNoAcnt = ""
                VDRegTotal = ""
                VDAssNoMemWthDepAcnt = ""
                VDAssNoAcnt = ""
                VDAssTotal = ""
                For Each xRow3 As DataGridViewRow In grdSavingDeposits.Rows
                    If xRow3.IsNewRow = False Then
                        If VDDepositType = "" Then
                            VDDepositType = xRow3.Cells(0).Value.ToString
                        Else
                            VDDepositType &= "▄" & xRow3.Cells(0).Value.ToString
                        End If
                        If VDRegNoMemWthDepAcnt = "" Then
                            VDRegNoMemWthDepAcnt = xRow3.Cells(1).Value.ToString
                        Else
                            VDRegNoMemWthDepAcnt &= "▄" & xRow3.Cells(1).Value.ToString
                        End If
                        If VDRegNoAcnt = "" Then
                            VDRegNoAcnt = xRow3.Cells(2).Value.ToString
                        Else
                            VDRegNoAcnt &= "▄" & xRow3.Cells(2).Value.ToString
                        End If
                        If VDRegTotal = "" Then
                            VDRegTotal = xRow3.Cells(3).Value.ToString
                        Else
                            VDRegTotal &= "▄" & xRow3.Cells(3).Value.ToString
                        End If
                        If VDAssNoMemWthDepAcnt = "" Then
                            VDAssNoMemWthDepAcnt = xRow3.Cells(4).Value.ToString
                        Else
                            VDAssNoMemWthDepAcnt &= "▄" & xRow3.Cells(4).Value.ToString
                        End If
                        If VDAssNoAcnt = "" Then
                            VDAssNoAcnt = xRow3.Cells(5).Value.ToString
                        Else
                            VDAssNoAcnt &= "▄" & xRow3.Cells(5).Value.ToString
                        End If
                        If VDAssTotal = "" Then
                            VDAssTotal = xRow3.Cells(6).Value.ToString
                        Else
                            VDAssTotal &= "▄" & xRow3.Cells(6).Value.ToString
                        End If
                    End If
                Next
                Me.Hide()
                xFormSec3b.Show()
                '.Label5.Visible = True
                '.BGWorker5thPage.RunWorkerAsync()

        End Select
    End Sub

    Private Sub frmCAPRSecIII_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        BgLoanReceivable.RunWorkerAsync()
    End Sub

    Private Sub BgLoanReceivable_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BgLoanReceivable.DoWork
        LoadLoanRecievable()
    End Sub

    Private Sub BgLoanReceivable_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BgLoanReceivable.RunWorkerCompleted

        For Each rd As DataRow In DTLoanReceivable.Tables(0).Rows
            Dim xNewRow As New DataGridViewRow
            Dim xNewCell As DataGridViewCell
            If rd.Item("perGroup").ToString = "By Type of Loan" Then
                With grdByLoanType
                    xNewCell = New DataGridViewTextBoxCell()
                    xNewCell.Value = rd.Item("xParticular").ToString
                    xNewRow.Cells.Add(xNewCell)
                    xNewCell = New DataGridViewTextBoxCell()
                    xNewCell.Value = Format(rd.Item("xNoOfLoans"), "##,##0")
                    xNewRow.Cells.Add(xNewCell)
                    xNewCell = New DataGridViewTextBoxCell()
                    xNewCell.Value = Format(rd.Item("xAmount"), "##,##0.00")
                    xNewRow.Cells.Add(xNewCell)
                    .Rows.Add(xNewRow)
                End With
            Else
                With GrdByMaturity
                    xNewCell = New DataGridViewTextBoxCell()
                    xNewCell.Value = rd.Item("xParticular").ToString
                    xNewRow.Cells.Add(xNewCell)
                    xNewCell = New DataGridViewTextBoxCell()
                    xNewCell.Value = Format(rd.Item("xNoOfLoans"), "##,##0")
                    xNewRow.Cells.Add(xNewCell)
                    xNewCell = New DataGridViewTextBoxCell()
                    xNewCell.Value = Format(rd.Item("xAmount"), "##,##0.00")
                    xNewRow.Cells.Add(xNewCell)
                    .Rows.Add(xNewRow)
                End With
            End If
        Next
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        frmCAPRViewer.Close()
        xFormSec4.Close()
        xFormSec3b.Close()
        xFormSec3.Close()
        xFormSec2.Close()
        xFormItro.Close()
        Me.Close()
    End Sub
End Class