<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRpt_StatementofAccountReport
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmRpt_StatementofAccountReport))
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtmemberID = New System.Windows.Forms.TextBox
        Me.DTDate = New System.Windows.Forms.DateTimePicker
        Me.txtPrepby = New System.Windows.Forms.TextBox
        Me.lvlMemberlist = New System.Windows.Forms.ListView
        Me.Label5 = New System.Windows.Forms.Label
        Me.txtquicksearch = New System.Windows.Forms.TextBox
        Me.PanePanel1 = New WindowsApplication2.PanePanel
        Me.btnclose = New System.Windows.Forms.Button
        Me.BtnPreview = New System.Windows.Forms.Button
        Me.PanePanel2 = New WindowsApplication2.PanePanel
        Me.Label1 = New System.Windows.Forms.Label
        Me.PanePanel1.SuspendLayout()
        Me.PanePanel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(14, 36)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(73, 15)
        Me.Label2.TabIndex = 58
        Me.Label2.Text = "Member No."
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(13, 59)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(32, 15)
        Me.Label3.TabIndex = 59
        Me.Label3.Text = "Date"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(13, 81)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(73, 15)
        Me.Label4.TabIndex = 60
        Me.Label4.Text = "Prepared by"
        '
        'txtmemberID
        '
        Me.txtmemberID.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtmemberID.Location = New System.Drawing.Point(93, 32)
        Me.txtmemberID.Name = "txtmemberID"
        Me.txtmemberID.ReadOnly = True
        Me.txtmemberID.Size = New System.Drawing.Size(156, 22)
        Me.txtmemberID.TabIndex = 61
        '
        'DTDate
        '
        Me.DTDate.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DTDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DTDate.Location = New System.Drawing.Point(93, 55)
        Me.DTDate.Name = "DTDate"
        Me.DTDate.Size = New System.Drawing.Size(156, 22)
        Me.DTDate.TabIndex = 62
        '
        'txtPrepby
        '
        Me.txtPrepby.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPrepby.Location = New System.Drawing.Point(93, 77)
        Me.txtPrepby.Name = "txtPrepby"
        Me.txtPrepby.ReadOnly = True
        Me.txtPrepby.Size = New System.Drawing.Size(156, 22)
        Me.txtPrepby.TabIndex = 63
        '
        'lvlMemberlist
        '
        Me.lvlMemberlist.Location = New System.Drawing.Point(5, 126)
        Me.lvlMemberlist.Name = "lvlMemberlist"
        Me.lvlMemberlist.Size = New System.Drawing.Size(340, 168)
        Me.lvlMemberlist.TabIndex = 64
        Me.lvlMemberlist.UseCompatibleStateImageBehavior = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(12, 104)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(79, 15)
        Me.Label5.TabIndex = 65
        Me.Label5.Text = "Quick Search"
        '
        'txtquicksearch
        '
        Me.txtquicksearch.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtquicksearch.Location = New System.Drawing.Point(93, 100)
        Me.txtquicksearch.Name = "txtquicksearch"
        Me.txtquicksearch.Size = New System.Drawing.Size(156, 22)
        Me.txtquicksearch.TabIndex = 66
        '
        'PanePanel1
        '
        Me.PanePanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel1.Controls.Add(Me.btnclose)
        Me.PanePanel1.Controls.Add(Me.BtnPreview)
        Me.PanePanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PanePanel1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PanePanel1.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel1.InactiveGradientHighColor = System.Drawing.Color.YellowGreen
        Me.PanePanel1.InactiveGradientLowColor = System.Drawing.Color.Green
        Me.PanePanel1.Location = New System.Drawing.Point(0, 300)
        Me.PanePanel1.Name = "PanePanel1"
        Me.PanePanel1.Size = New System.Drawing.Size(350, 30)
        Me.PanePanel1.TabIndex = 57
        '
        'btnclose
        '
        Me.btnclose.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnclose.Location = New System.Drawing.Point(269, 4)
        Me.btnclose.Name = "btnclose"
        Me.btnclose.Size = New System.Drawing.Size(75, 23)
        Me.btnclose.TabIndex = 1
        Me.btnclose.Text = "Close"
        Me.btnclose.UseVisualStyleBackColor = True
        '
        'BtnPreview
        '
        Me.BtnPreview.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnPreview.Location = New System.Drawing.Point(4, 3)
        Me.BtnPreview.Name = "BtnPreview"
        Me.BtnPreview.Size = New System.Drawing.Size(115, 23)
        Me.BtnPreview.TabIndex = 0
        Me.BtnPreview.Text = "Preview Reports"
        Me.BtnPreview.UseVisualStyleBackColor = True
        '
        'PanePanel2
        '
        Me.PanePanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel2.Controls.Add(Me.Label1)
        Me.PanePanel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanePanel2.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.PanePanel2.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel2.InactiveGradientHighColor = System.Drawing.Color.Green
        Me.PanePanel2.InactiveGradientLowColor = System.Drawing.Color.YellowGreen
        Me.PanePanel2.Location = New System.Drawing.Point(0, 0)
        Me.PanePanel2.Name = "PanePanel2"
        Me.PanePanel2.Size = New System.Drawing.Size(350, 26)
        Me.PanePanel2.TabIndex = 56
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Calibri", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(-1, -1)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(202, 26)
        Me.Label1.TabIndex = 20
        Me.Label1.Text = "Statement of Account"
        '
        'frmStatementofAccountReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(350, 330)
        Me.Controls.Add(Me.txtquicksearch)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.lvlMemberlist)
        Me.Controls.Add(Me.txtPrepby)
        Me.Controls.Add(Me.DTDate)
        Me.Controls.Add(Me.txtmemberID)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.PanePanel1)
        Me.Controls.Add(Me.PanePanel2)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmStatementofAccountReport"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Report"
        Me.PanePanel1.ResumeLayout(False)
        Me.PanePanel2.ResumeLayout(False)
        Me.PanePanel2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PanePanel1 As WindowsApplication2.PanePanel
    Friend WithEvents btnclose As System.Windows.Forms.Button
    Friend WithEvents BtnPreview As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents PanePanel2 As WindowsApplication2.PanePanel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtmemberID As System.Windows.Forms.TextBox
    Friend WithEvents DTDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtPrepby As System.Windows.Forms.TextBox
    Friend WithEvents lvlMemberlist As System.Windows.Forms.ListView
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtquicksearch As System.Windows.Forms.TextBox
End Class
