Imports System.Data.Sql
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmMember_BankAccount
#Region "Variables"
    Public pkBA As String

#End Region
#Region "Property"
    Public Property getpkBA() As String
        Get
            Return pkBA
        End Get
        Set(ByVal value As String)
            pkBA = value
        End Set
    End Property
#End Region
#Region "Add/Edit Bank account"
    Private Sub AddEditbankAccount(ByVal empID As String, ByVal bankacct As String, ByVal bankname As String, ByVal branch As String, ByVal accountype As String, _
                                  ByVal atmNo As String, ByVal pkbankaccount As String)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Member_BankAccounts_AddEdit", _
                                     New SqlParameter("@employeeNo", empID), _
                                     New SqlParameter("@bankAccount", bankacct), _
                                     New SqlParameter("@bankName", bankname), _
                                     New SqlParameter("@branch", branch), _
                                     New SqlParameter("@accountType", accountype), _
                                     New SqlParameter("@AtmNo", atmNo), _
                                     New SqlParameter("@Status", cboStatus.Text), _
                                     New SqlParameter("@Approver", txtUser.Text), _
                                     New SqlParameter("@DateUpdated", Date.Now), _
                                     New SqlParameter("@pk_BankAccount", pkbankaccount))
            trans.Commit()
            MessageBox.Show("Record has been Saved", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "Save Bank Account")
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub
#End Region

    Public Function BankAccount(ByVal C As Char) As Boolean
        If Not (Char.IsDigit(C)) And Not (C = "-") And Not (Microsoft.VisualBasic.AscW(C) = 8) And Not (Microsoft.VisualBasic.AscW(C) = 13) Then
            MsgBox("Invalid Input! This field allows 0-9 only.", MsgBoxStyle.Exclamation, "Error Message")
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        Me.Close()
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If txtAccountType.Text = "" Or txtBankAccount.Text = "" Or txtBankName.Text = "" Or txtAtmNo.Text = "" Or txtBranch.Text = "" Or cboStatus.Text = "" Then
            MessageBox.Show("Complete Infomation is Needed", "Bank Account Information", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        Else
            Call AddEditbankAccount(frmMember_Master.txtEmployeeNo.Text.Trim, Me.txtBankAccount.Text.Trim, Me.txtBankName.Text.Trim, Me.txtBranch.Text.Trim, txtAccountType.Text, Me.txtAtmNo.Text.Trim, "")
            Call frmMember_Master.GetmemberBankInfo(frmMember_Master.txtEmployeeNo.Text)
            Call ClearText()
            'Me.Close()
        End If
    End Sub

    Private Sub btnupdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupdate.Click
            Call AddEditbankAccount(frmMember_Master.txtEmployeeNo.Text.Trim, Me.txtBankAccount.Text.Trim, Me.txtBankName.Text.Trim, Me.txtBranch.Text.Trim, txtAccountType.Text, Me.txtAtmNo.Text.Trim, pkBA)
            Call frmMember_Master.GetmemberBankInfo(frmMember_Master.txtEmployeeNo.Text)
            Me.Close()
    End Sub

    Private Sub txtBankAccount_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtBankAccount.KeyPress
        e.Handled = Not BankAccount(e.KeyChar)
    End Sub

    Private Sub frmMember_BankAccount_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtUser.Text = frmLogin.Username.Text
    End Sub

    Private Sub ClearText()
        txtAccountType.Text = ""
        txtAtmNo.Text = ""
        txtBankAccount.Text = ""
        txtBankName.Text = ""
        txtBranch.Text = ""
        txtUser.Text = ""
        cboStatus.Text = ""
    End Sub
End Class