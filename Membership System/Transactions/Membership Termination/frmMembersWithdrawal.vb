Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared

Public Class frmMembersWithdrawal
    Public memberid As String

    Public Property getmemberid() As String
        Get
            Return memberid
        End Get
        Set(ByVal value As String)
            memberid = value
        End Set
    End Property

    Private Sub Memberlist()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("CIMS_m_MemberList_Active", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Connection = myconnection.sqlconn
        myconnection.sqlconn.Open()
        With Me.lvlMemberlist
            .Clear()
            .View = View.Details
            .GridLines = True
            .FullRowSelect = True
            .Columns.Add("Member No.", 100, HorizontalAlignment.Left)
            .Columns.Add("Member Name", 300, HorizontalAlignment.Left)
            Dim myreader As SqlDataReader
            myreader = cmd.ExecuteReader
            Try
                While myreader.Read
                    With .Items.Add(myreader.Item(0))
                        .subitems.add(myreader.Item(1))
                    End With
                End While
            Finally
                myreader.Close()
                myconnection.sqlconn.Close()
            End Try
        End With
    End Sub
    Private Sub Selections()
        Me.txtEmployeeNo.Text = Me.lvlMemberlist.SelectedItems(0).Text
    End Sub
    Private Sub QuickSearch(ByVal Search As String)
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("CIMS_m_Member_SOA_ListParameter", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Connection = myconnection.sqlconn
        myconnection.sqlconn.Open()
        With cmd.Parameters
            .Add("@searchtext", SqlDbType.VarChar, 1000).Value = Trim(Search)
        End With

        With Me.lvlMemberlist
            .Clear()
            .View = View.Details
            .GridLines = True
            .FullRowSelect = True
            .Columns.Add("Member No.", 100, HorizontalAlignment.Left)
            .Columns.Add("Member Name", 300, HorizontalAlignment.Left)
            Dim myreader As SqlDataReader
            myreader = cmd.ExecuteReader
            Try
                While myreader.Read
                    With .Items.Add(myreader.Item(0))
                        .subitems.add(myreader.Item(1))
                    End With
                End While
            Finally
                myreader.Close()
                myconnection.sqlconn.Close()
            End Try
        End With
    End Sub

    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If

        Dim obj As ReportObject

        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function
    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        ' for each table apply connection info
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            ' check if logon was successful
            ' if TestConnectivity returns false,
            ' check logon credentials
            If (tbl.TestConnectivity()) Then
                'drop fully qualified table location
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next

        Return True
    End Function

    Private Sub frmMembersWithdrawal_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call Memberlist()
        Me.txtPreparedBy.Text = frmLogin.Username.Text
    End Sub

    Private Sub lvlMemberlist_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvlMemberlist.Click
        Try
            Call Selections()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub lvlMemberlist_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles lvlMemberlist.KeyDown
        Try
            Call Selections()
        Catch ex As Exception

        End Try
    End Sub
    Private Sub lvlMemberlist_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles lvlMemberlist.KeyPress
        Try
            Call Selections()
        Catch ex As Exception

        End Try
    End Sub
    Private Sub BtnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnPreview.Click
        Dim xform As New frmMembersWithdrawalReport
        xform.GetSetEmployeeNo() = txtEmployeeNo.Text

        xform.GetSetEffectiveDate() = dteEffectiveDate.Value.Date
        xform.Termination_effectiveDate = dteEffectiveDate.Value.Date

        xform.GetSetUser() = txtPreparedBy.Text
        xform.ShowDialog()
    End Sub

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub frmMembersWithdrawal_ResizeBegin(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ResizeBegin
        Me.Opacity = 0.5
    End Sub

    Private Sub frmMembersWithdrawal_ResizeEnd(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ResizeEnd
        Me.Opacity = 1
    End Sub

    Private Sub txtquicksearch_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtquicksearch.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Return) Then
            Call QuickSearch(Trim(Me.txtquicksearch.Text))
        End If
    End Sub
End Class