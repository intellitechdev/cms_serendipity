﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmComaker_Settings

    Private con As New Clsappconfiguration

    Private Sub frmComaker_Settings_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If isLoaded() Then
            Me.txtLoanType.Text = frmMasterfile_LoanType.txtName.Text
        End If
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        txtLoanType.Text = ""
        txtMaxAmount.Text = "0.00"
        txtTimes.Text = "0"
        chkApply.Checked = False
        Me.Close()
    End Sub

    Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        If MsgBox("Update Co-Maker Settings? this may affect the system.", vbYesNo + MsgBoxStyle.Question, "Update") = vbYes Then
            If isInsertUpdate() Then
                MsgBox("Co-Maker Settings Successfully Updated!", vbInformation, "Success")
            End If
        End If
    End Sub

    Private Function isInsertUpdate() As Boolean
        Try
            SqlHelper.ExecuteNonQuery(con.cnstring, "_ComakerSettings_InsertUpdate",
                                       New SqlParameter("@fcLoanType", txtLoanType.Text),
                                       New SqlParameter("@fdMaxTime", CInt(txtTimes.Text)),
                                       New SqlParameter("@fdMaxAmount", CDec(txtMaxAmount.Text)),
                                       New SqlParameter("@fbApply", chkApply.Checked))
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Function isLoaded() As Boolean
        Try
            Dim rd As SqlDataReader
            rd = SqlHelper.ExecuteReader(con.cnstring, "_ComakerSettings_Select",
                                          New SqlParameter("@LoanType", frmMasterfile_LoanType.txtName.Text))
            While rd.Read
                Me.txtTimes.Text = rd(0).ToString
                Me.txtMaxAmount.Text = rd(1).ToString
                Me.chkApply.Checked = rd(2)
            End While
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
End Class