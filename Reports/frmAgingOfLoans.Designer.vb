<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAgingOfLoans
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.dteAgingOfLoans = New System.Windows.Forms.DateTimePicker
        Me.Label3 = New System.Windows.Forms.Label
        Me.PanePanel1 = New WindowsApplication2.PanePanel
        Me.btnGenerate = New System.Windows.Forms.Button
        Me.btnCancel = New System.Windows.Forms.Button
        Me.PanePanelAging = New WindowsApplication2.PanePanel
        Me.Label1 = New System.Windows.Forms.Label
        Me.PanePanel1.SuspendLayout()
        Me.PanePanelAging.SuspendLayout()
        Me.SuspendLayout()
        '
        'dteAgingOfLoans
        '
        Me.dteAgingOfLoans.CalendarFont = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dteAgingOfLoans.CustomFormat = "MMMdd, yyyy"
        Me.dteAgingOfLoans.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dteAgingOfLoans.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dteAgingOfLoans.Location = New System.Drawing.Point(15, 59)
        Me.dteAgingOfLoans.Name = "dteAgingOfLoans"
        Me.dteAgingOfLoans.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.dteAgingOfLoans.Size = New System.Drawing.Size(248, 27)
        Me.dteAgingOfLoans.TabIndex = 2
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(12, 38)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(196, 18)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Generate Aging Of Loans as of:"
        '
        'PanePanel1
        '
        Me.PanePanel1.Controls.Add(Me.btnGenerate)
        Me.PanePanel1.Controls.Add(Me.btnCancel)
        Me.PanePanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PanePanel1.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.PanePanel1.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel1.InactiveGradientHighColor = System.Drawing.Color.Green
        Me.PanePanel1.InactiveGradientLowColor = System.Drawing.Color.YellowGreen
        Me.PanePanel1.Location = New System.Drawing.Point(0, 104)
        Me.PanePanel1.Name = "PanePanel1"
        Me.PanePanel1.Size = New System.Drawing.Size(275, 40)
        Me.PanePanel1.TabIndex = 1
        '
        'btnGenerate
        '
        Me.btnGenerate.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGenerate.Location = New System.Drawing.Point(118, 4)
        Me.btnGenerate.Name = "btnGenerate"
        Me.btnGenerate.Size = New System.Drawing.Size(81, 30)
        Me.btnGenerate.TabIndex = 5
        Me.btnGenerate.Text = "Preview"
        Me.btnGenerate.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCancel.Location = New System.Drawing.Point(203, 4)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(69, 30)
        Me.btnCancel.TabIndex = 6
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'PanePanelAging
        '
        Me.PanePanelAging.Controls.Add(Me.Label1)
        Me.PanePanelAging.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanePanelAging.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.PanePanelAging.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanelAging.InactiveGradientHighColor = System.Drawing.Color.Green
        Me.PanePanelAging.InactiveGradientLowColor = System.Drawing.Color.YellowGreen
        Me.PanePanelAging.Location = New System.Drawing.Point(0, 0)
        Me.PanePanelAging.Name = "PanePanelAging"
        Me.PanePanelAging.Size = New System.Drawing.Size(275, 26)
        Me.PanePanelAging.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Calibri", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(3, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(138, 26)
        Me.Label1.TabIndex = 21
        Me.Label1.Text = "Aging of Loans"
        '
        'frmAgingOfLoans
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.ClientSize = New System.Drawing.Size(275, 144)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.dteAgingOfLoans)
        Me.Controls.Add(Me.PanePanel1)
        Me.Controls.Add(Me.PanePanelAging)
        Me.MaximizeBox = False
        Me.Name = "frmAgingOfLoans"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Aging Of Loans"
        Me.TopMost = True
        Me.PanePanel1.ResumeLayout(False)
        Me.PanePanelAging.ResumeLayout(False)
        Me.PanePanelAging.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PanePanelAging As WindowsApplication2.PanePanel
    Friend WithEvents PanePanel1 As WindowsApplication2.PanePanel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dteAgingOfLoans As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents btnGenerate As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
End Class
