﻿Public Class frmLedgerFilter
    Private Sub btnProceed_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProceed.Click
        If chkSubLedger.Checked = True Then
            frmPrint.LoadLoanLedger2(frmVerification.lblLoanRef.Text)
            frmPrint.StartPosition = FormStartPosition.CenterScreen
            frmPrint.WindowState = FormWindowState.Maximized
            frmPrint.ShowDialog()
            Exit Sub
        End If
        If chkPayLedger.Checked = True Then
            frmPrint.LoadLoanLedger(frmVerification.lblLoanRef.Text)
            frmPrint.StartPosition = FormStartPosition.CenterScreen
            frmPrint.WindowState = FormWindowState.Maximized
            frmPrint.ShowDialog()
            Exit Sub
        End If
    End Sub

    Private Sub btnCLose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCLose.Click
        Me.Close()
    End Sub
End Class