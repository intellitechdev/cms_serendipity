Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Public Class frmCapitalShare

#Region "var"
    Public memberid As String
#End Region
#Region "Property"
    Public Property getmemberid() As String
        Get
            Return memberid
        End Get
        Set(ByVal value As String)
            memberid = value
        End Set
    End Property
#End Region
    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If

        Dim obj As ReportObject

        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function
    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        ' for each table apply connection info
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            ' check if logon was successful
            ' if TestConnectivity returns false,
            ' check logon credentials
            If (tbl.TestConnectivity()) Then
                'drop fully qualified table location
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next

        Return True
    End Function

    Private Sub frmStatementofAccountReport_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call Memberlist()
        Me.txtPrepby.Text = LoginForm1.Username.Text
    End Sub
#Region "Member List"
    Private Sub Memberlist()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("CIMS_m_Member_SOA_List", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Connection = myconnection.sqlconn
        myconnection.sqlconn.Open()
        With Me.lvlMemberlist
            .Clear()
            .View = View.Details
            .GridLines = True
            .FullRowSelect = True
            .Columns.Add("Member No.", 100, HorizontalAlignment.Left)
            .Columns.Add("Member Name", 300, HorizontalAlignment.Left)
            Dim myreader As SqlDataReader
            myreader = cmd.ExecuteReader
            Try
                While myreader.Read
                    With .Items.Add(myreader.Item(0))
                        .subitems.add(myreader.Item(1))
                    End With
                End While
            Finally
                myreader.Close()
                myconnection.sqlconn.Close()
            End Try
        End With
    End Sub
#End Region
#Region "Quick Search"
    Private Sub QuickSearch(ByVal Search As String)
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("CIMS_m_Member_SOA_ListParameter", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Connection = myconnection.sqlconn
        myconnection.sqlconn.Open()
        With cmd.Parameters
            .Add("@searchtext", SqlDbType.VarChar, 1000).Value = Trim(Search)
        End With

        With Me.lvlMemberlist
            .Clear()
            .View = View.Details
            .GridLines = True
            .FullRowSelect = True
            .Columns.Add("Member No.", 100, HorizontalAlignment.Left)
            .Columns.Add("Member Name", 300, HorizontalAlignment.Left)
            Dim myreader As SqlDataReader
            myreader = cmd.ExecuteReader
            Try
                While myreader.Read
                    With .Items.Add(myreader.Item(0))
                        .subitems.add(myreader.Item(1))
                    End With
                End While
            Finally
                myreader.Close()
                myconnection.sqlconn.Close()
            End Try
        End With
    End Sub
#End Region

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        Me.Close()
        Me.Dispose()
    End Sub
#Region "Selection of members"
    Private Sub Selections()
        Me.txtmemberID.Text = Me.lvlMemberlist.SelectedItems(0).Text
    End Sub
#End Region

    Private Sub lvlMemberlist_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvlMemberlist.Click
        Try
            Call Selections()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub lvlMemberlist_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles lvlMemberlist.KeyDown
        Try
            Call Selections()
        Catch ex As Exception

        End Try
    End Sub
    Private Sub lvlMemberlist_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles lvlMemberlist.KeyPress
        Try
            Call Selections()
        Catch ex As Exception

        End Try
    End Sub
    Private Sub txtquicksearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtquicksearch.TextChanged
        Call QuickSearch(Trim(Me.txtquicksearch.Text))
    End Sub

    Private Sub BtnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnPreview.Click
        If Me.txtmemberID.Text <> "" Then
            getmemberid() = Me.txtmemberID.Text
            Try
                Dim childform As Integer = 0
                Dim childforms(4) As CooperativeReports
                childform = 1
                childforms(childform) = New CooperativeReports()
                childforms(childform).Text = "Capital Share Statement"
                Dim con As New clsPersonnel
                Dim appRdr As New System.Configuration.AppSettingsReader
                Dim myconnection As New Clsappconfiguration
                Dim objreport As New CrystalDecisions.CrystalReports.Engine.ReportDocument
                objreport.Load(Application.StartupPath & "\LoanReport\MembersContribution.rpt")
                Logon(objreport, con.servername, con.databasename, con.username1, con.password1)
                objreport.Refresh()
                objreport.SetParameterValue("@employeeNo", memberid)
                objreport.SetParameterValue("@dateFrom", Me.dtDateFrom.Value.Date)
                objreport.SetParameterValue("@dateTo", Me.dtDateTo.Value.Date)
                objreport.SetParameterValue("@preparedBy", LoginForm1.Username.Text)
                childforms(childform).appreports.ReportSource = objreport
                'childforms(childform).MdiParent =me
                childforms(childform).Show()
            Catch ex As Exception
                MessageBox.Show("Supply the given Parameter", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            End Try
        Else
            MessageBox.Show("Select Member No. on the list below..", "Empty field found", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub
End Class