﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmInvestmentApproval

    Private con As New Clsappconfiguration
    Public fkemployee As String
    Dim ds As DataSet

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnApprove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        Try
            frmInvestment_Entry.StartPosition = FormStartPosition.CenterScreen
            frmInvestment_Entry.ShowDialog()
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub btnSearchApprover_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearchApprover.Click
        frmBrowseInvestmentApprover.StartPosition = FormStartPosition.CenterScreen
        frmBrowseInvestmentApprover.ShowDialog()
    End Sub

    Private Sub lblInvestmentType_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblInvestmentType.TextChanged
        If lblInvestmentType.Text <> "..." Then
            If isLoad_InvestmentApp(lblInvestmentType.Text) Then

            End If
        End If
    End Sub

    Public Function isLoad_InvestmentApp(ByVal investment As String) As Boolean
        Try
            ds = SqlHelper.ExecuteDataset(con.cnstring, "_InvestmentApproval_Select_AllInvestments",
                                           New SqlParameter("@investmentType", investment))
            dgvList.DataSource = ds.Tables(0)
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Sub dgvList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvList.Click
        If isSelected() Then
            btnApprove.Enabled = True
        End If
    End Sub

    Private Function isSelected() As Boolean
        Try
            lblSelected.Text = dgvList.SelectedRows(0).Cells(3).Value.ToString
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
End Class