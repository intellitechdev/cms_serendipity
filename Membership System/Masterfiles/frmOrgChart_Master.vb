Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports
Imports Microsoft.ApplicationBlocks.Data
Public Class frmOrgChart_Master
    Dim dt, dtorgdetails, dtgetorgchild As DataTable
    Dim errmsg As String
    Dim orglevel, orgcode, orgdesc, parentid As String
    Public org_parent_desc, orgid, department, xorgid As String
    Dim orgoption As String


    Public Enum selection
        ORg_Main
        Org_Emp
    End Enum

    Public Sub New(ByVal selection As selection)
        MyBase.New()
        InitializeComponent()
        orgoption = selection
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Select Case orgoption
            Case selection.Org_Emp
                Me.DialogResult = Windows.Forms.DialogResult.Cancel
            Case selection.ORg_Main
                Me.Close()
        End Select
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Dim addorgchart As New frmOrgChart_Edit(frmOrgChart_Edit.selection.AddOrg)
        addorgchart.btnupdate.Visible = False
        If addorgchart.ShowDialog = Windows.Forms.DialogResult.OK Then
            Call treeviewload()
        End If
    End Sub

    Private Sub menuedit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuedit.Click
        Dim editorgchart As New frmOrgChart_Edit(frmOrgChart_Edit.selection.EditOrg)
        editorgchart.lblLAbel.Text = "Edit Organization Item"
        editorgchart.btnSave.Visible = False
        editorgchart.btnupdate.Location = New System.Drawing.Point(9, 5)
        Try
            If orgid <> Nothing Or orgid <> "" Then
                editorgchart.GetOrgID = TreeView1.SelectedNode.Tag
                editorgchart.GetOrgLevel = Me.txtLevel.Text
                editorgchart.GetOrgCode = Me.txtCode.Text
                editorgchart.GetOrgDesc = Me.txtDescription.Text
                editorgchart.Label5.Text = "Edit" + " " + Me.txtDescription.Text
                editorgchart.Text = Me.txtDescription.Text
                editorgchart.GetParentDescription = org_parent_desc
                deptcode = Me.txtCode.Text
                If editorgchart.ShowDialog() = Windows.Forms.DialogResult.OK Then
                    Call treeviewload()
                    Refresh()
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub

    Public Sub treeviewload()
        TreeView1.Nodes.Clear()
        TreeView1.Nodes.Add(New TreeNode("Organizational Chart"))
        Dim tNode As New TreeNode
        tNode = TreeView1.Nodes(0)
        'PopulateTreeView(12320, tNode) '3158064
        PopulateTreeView(3158064, tNode)
        'tNode.Collapse()
    End Sub

    Private Sub OrgChart_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        TreeView1.AllowDrop = True
        Call treeviewload()
    End Sub
    Private Sub PopulateTreeView(ByVal inParentID As String, ByRef inTreeNode As TreeNode)
        Try
            Dim dsorgchart As DataSet
            Dim server, database, username, password, cnstring As String
            Dim appRdr As New System.Configuration.AppSettingsReader
            server = appRdr.GetValue("Server", GetType(String))
            database = appRdr.GetValue("Database", GetType(String))
            username = appRdr.GetValue("Username", GetType(String))
            password = appRdr.GetValue("Password", GetType(String))
            cnstring = "Data Source= '" & server & "' ;Initial Catalog= '" & database & "' ;User Id= '" & username & "';Password='" & password & "' ; Connection Timeout=0;"
            Dim cnorgchart As New SqlClient.SqlConnection(cnstring)
            'Dim DAorgchart As New SqlClient.SqlDataAdapter("SELECT OrgID,Description,Parent_ID from tblOrgChart where Parent_ID =  " & inParentID, cnorgchart)
            Dim DAorgchart As New SqlClient.SqlDataAdapter("SELECT cast(cast(orgid as binary(3)) as int) as OrgID,Description,cast(cast(Parent_ID as binary(3)) as int) as Parent_ID from tblOrgChart where cast(cast(Parent_ID as binary(3)) as int) =  " & inParentID, cnorgchart)
            dsorgchart = New DataSet
            cnorgchart.Open()
            ''temp fill
            DAorgchart.Fill(dsorgchart, "tblOrgChart")
            Dim tmpchart As New SqlDataAdapter
            'dsorgchart = New DataSet
            'cnorgchart.Open()
            'DAorgchart.Fill(dsorgchart, "tblOrgChart")
            'Close the connection to the data store; free up the resources
            cnorgchart.Close()
            Dim parentrow As DataRow
            Dim ParentTable As DataTable
            ParentTable = dsorgchart.Tables("tblOrgChart")

            For Each parentrow In ParentTable.Rows
                Dim parentnode As TreeNode
                'we'll provide some text for the tree node.
                Dim strLabel As String = parentrow.Item(1)
                parentnode = New TreeNode(strLabel)
                inTreeNode.Nodes.Add(parentnode)
                'set the tag property for the current node. This comes in useful if 
                'you want to pass the value of a specific record id.
                'since the tag value is not visible, in the TreeView1_AfterSelect event 
                'you could pass the value to another sub routine, for example:
                'FillDataGrid(TreeView1.SelectedNode.Tag)

                parentnode.Tag = parentrow.Item(0)
                'call the routine again to find childern of this record.
                PopulateTreeView(parentrow.Item(0), parentnode)
            Next parentrow
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Org Chart", MessageBoxButtons.OK)
        End Try
    End Sub

    Private Sub TreeView1_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles TreeView1.AfterSelect

        Me.txtLevel.Text = ""
        Me.txtCode.Text = ""
        Me.txtDescription.Text = ""
        Me.parentid = ""

        errmsg = ""
        orgid = TreeView1.SelectedNode.Tag
        org_parent_desc = TreeView1.SelectedNode.Text
        Dim getdetails As New clsPersonnel

        dtorgdetails = getdetails.GetOrgDetails(orgid)
        If errmsg <> "" Then
            MessageBox.Show(errmsg, "Org Chart", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Else
            If dtorgdetails.Rows.Count <> 0 Then
                Me.txtLevel.Text = Me.dtorgdetails.Rows(0).Item(0)
                Me.txtCode.Text = Me.dtorgdetails.Rows(0).Item(1)
                Me.txtDescription.Text = Me.dtorgdetails.Rows(0).Item(2)
                Me.parentid = Me.dtorgdetails.Rows(0).Item(3)
            Else
                Me.txtLevel.Text = ""
                Me.txtCode.Text = "                                                                                                                                                                                                                                                                                                                                                                                       "
                Me.txtDescription.Text = ""
                Me.parentid = ""
            End If

        End If

        Call getorgchild()
        Call getemployeename()

    End Sub
    Public Sub getorgchild()
        Dim getorgchild As New clsPersonnel

        dtgetorgchild = getorgchild.GetOrgChild(orgid, errmsg)
        If errmsg <> "" Then
            MessageBox.Show(errmsg, "Org Chart", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Else
            Me.dtgorgchild.DataSource = dtgetorgchild
        End If
    End Sub
    Public Sub getemployeename()
        Dim getorgemployee As New clsPersonnel
        dtgetorgchild = getorgemployee.getEmployee(orgid, errmsg)
        If errmsg <> "" Then
            MessageBox.Show(errmsg, "Org Chart", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Else
            Me.dtgorgchild.DataSource = dtgetorgchild
        End If

    End Sub

    Private Sub menudelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menudelete.Click
        If orgid <> "" Or orgid <> Nothing Then

            Dim x As String

            If MessageBox.Show("Are you sure you want to delete this Item?", "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                Dim delete As New clsPersonnel
                Dim mymessage As New clsPersonnel
                delete.OrgMaintenance(3, 0, "", "", 0, orgid, mymessage.myerrormessage) 'errmsg)

                If errmsg <> "" Then
                    MessageBox.Show(errmsg, "Delete", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Else
                    MessageBox.Show("Item Successfully Deleted!", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Call treeviewload()
                End If

            End If
        End If


    End Sub
#Region "Division payroll delete"
    Public Sub DivisionpayrollDelete(ByVal orgid As String)
        Try
            Dim myconnection As New Clsappconfiguration
            Dim cmd As New SqlCommand("usp_per_division_delete", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            With cmd.Parameters
                .Add("@orgid", SqlDbType.VarChar, 10).Value = orgid
            End With
            myconnection.sqlconn.Open()
            cmd.ExecuteNonQuery()
            myconnection.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show("usp_per_division_delete", "Delete Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub
#End Region
#Region "Department payroll delete"
    Public Sub deletepayrollDepartment(ByVal orgid As String)
        Try
            Dim myconnection As New Clsappconfiguration
            Dim cmd As New SqlCommand("usp_per_department_delete", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            With cmd.Parameters
                .Add("@orgid", SqlDbType.VarChar, 10).Value = orgid
            End With
            myconnection.sqlconn.Open()
            cmd.ExecuteNonQuery()
            myconnection.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show("usp_per_department_delete", "Delete Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub
#End Region
#Region "Section payroll delete"
    Public Function SectionpayrollDelete(ByVal orgid As String) As String
        'Try
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_section_delete", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        With cmd.Parameters
            .Add("@orgid", SqlDbType.VarChar, 10).Value = orgid
        End With
        myconnection.sqlconn.Open()
        'cmd.ExecuteNonQuery()
        Dim rd As SqlDataReader = cmd.ExecuteReader
        Dim x As String

        Try
            While rd.Read

                x = rd.Item(0)

            End While

        Catch ex As Exception

            x = "0"

        End Try

        Return x

        myconnection.sqlconn.Close()
        'Catch ex As Exception
        '    MessageBox.Show("usp_per_section_delete", "Delete Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        'End Try
    End Function
#End Region
#Region "Company Personnel Delete"
    Public Sub CompanyPersonnelDelete(ByVal orglevel As String)
        Try
            Dim myconnection As New Clsappconfiguration
            Dim cmd As New SqlCommand("usp_per_company_table_delete", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            With cmd.Parameters
                .Add("@RECID", SqlDbType.VarChar, 10).Value = orglevel
            End With
            myconnection.sqlconn.Open()
            cmd.ExecuteNonQuery()
            myconnection.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show("usp_per_company_table_delete", "Delete Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub
#End Region
#Region "Division Personnel Delete"
    Public Sub DivisionPersonnelDelete(ByVal orglevel As String)
        Try
            Dim myconnection As New Clsappconfiguration
            Dim cmd As New SqlCommand("usp_division_table_delete", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            With cmd.Parameters
                .Add("@RECID", SqlDbType.Char, 3).Value = orglevel
            End With
            myconnection.sqlconn.Open()
            cmd.ExecuteNonQuery()
            myconnection.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show("usp_division_table_delete", "Delete Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub
#End Region
#Region "Department Personnel Delete"
    Public Sub DepartmentPersonnelDelete(ByVal orglevel As String)
        Try
            Dim myconnection As New Clsappconfiguration
            Dim cmd As New SqlCommand("usp_per_department_table_delete", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            With cmd.Parameters
                .Add("@RECID", SqlDbType.Char, 3).Value = orglevel
            End With
            myconnection.sqlconn.Open()
            cmd.ExecuteNonQuery()
            myconnection.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show("usp_per_department_table_delete", "Delete Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub
#End Region
#Region "Section Personnel Delete"
    Public Sub SectionPersonnelDelete(ByVal orglevel As String)
        Try
            Dim myconnection As New Clsappconfiguration
            Dim cmd As New SqlCommand("usp_section_table_delete", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            With cmd.Parameters
                .Add("@RECID", SqlDbType.Char, 3).Value = orglevel
            End With
            myconnection.sqlconn.Open()
            cmd.ExecuteNonQuery()
            myconnection.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show("usp_section_table_delete", "Delete Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub
#End Region
    Private Sub btnSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelect.Click
        Try
            If TreeView1.SelectedNode.Text <> "" Then
                org_parent_desc = TreeView1.SelectedNode.FullPath 'treeview1.selectednode.text
                Me.orgid = TreeView1.SelectedNode.Tag 'TreeView1.SelectedNode.FullPath
                Me.department = TreeView1.SelectedNode.Text
                Me.DialogResult = Windows.Forms.DialogResult.Yes
                'getdepartmentequalorgid(Me.department)
            End If
        Catch ex As Exception
        End Try
    End Sub
    Public Sub getdepartmentequalorgid(ByVal dept As String)
        Dim x As Integer = 0
        Dim mycon As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_orgchart_equal_description", mycon.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        With cmd.Parameters
            .Add("@department", SqlDbType.VarChar).Value = dept
        End With
        mycon.sqlconn.Open()
        Dim read As SqlDataReader
        read = cmd.ExecuteReader

        Try
            While read.Read
                x = read.Item(0)
            End While
            read.Close()
            frmMember_Master.getxorgid = x
            Call getorgchartvalue(x)
            Call getorgchartIDtodepartment2(x)
            Call proposeddeptdivision(x)
            'form6.getorgcode(x)
            Call getorgparentid(x)
        Finally
            mycon.sqlconn.Close()
        End Try
    End Sub
#Region "Get Orgchart from the root to node"
    Public Sub getorgchartvalue(ByVal m As String)
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_orgchart_get", myconnection.sqlconn)
        With cmd.Parameters
            .Add("@getid", SqlDbType.VarChar).Value = m
        End With
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader
        Dim x As String
        Try
            While myreader.Read
                frmMember_Master.txtorgchart.Text = myreader.Item(0)
            End While
            myreader.Close()
        Finally
            myconnection.sqlconn.Close()
        End Try
    End Sub
#End Region
#Region "GetdepartmentID"
    Private Sub getorgchartIDtodepartment2(ByVal orgid As String)
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_employee_department2_select", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        With cmd.Parameters
            .Add("@department", SqlDbType.VarChar, 20).Value = orgid
        End With
        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader
        Try
            While myreader.Read
                frmMember_Master.txtDepartment2.Text = myreader.Item(0)
            End While
        Finally
            myreader.Close()
            myconnection.sqlconn.Close()
        End Try
    End Sub
#End Region
#Region "Get parent id from orgchart"
    Public Sub getorgparentid(ByVal m As String)

        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_orgchart_get_parentid", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        With cmd.Parameters
            .Add("@getid", SqlDbType.VarChar).Value = m
        End With
        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader

        Dim x1 As Integer = 0

        Dim split As New Collection
        Try
            While myreader.Read
                x1 = myreader.Item(0)
            End While
            myreader.Close()

            frmMember_Master.txtdescdivision.Text = Mid(x1, 5, 2)

        Finally
            'myreader.Close()
            myconnection.sqlconn.Close()
        End Try
    End Sub
#End Region
#Region "Proposed dept/division"
    Public Sub proposeddeptdivision(ByVal m As String)
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_orgchart_getid", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        With cmd.Parameters
            .Add("@getid", SqlDbType.VarChar).Value = m
        End With
        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader
        Dim x As String

        Try
            While myreader.Read
                x = myreader.Item(0)
            End While
            myreader.Close()

            Dim arr As String() = x.Split("\".ToCharArray())
            For i As Integer = 0 To (arr.Length - 1)
                frmMember_Master.txtkeycompany.Text = arr(1)
                frmMember_Master.txtdescdivision.Text = arr(2)
                frmMember_Master.txtdescdepartment.Text = arr(3)
                frmMember_Master.txtdescsection.Text = arr(4)
            Next

        Finally
            myconnection.sqlconn.Close()
        End Try
    End Sub
#End Region


    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Call print_report()
    End Sub
    Public Sub print_report()

        Dim myconnection As New Clsappconfiguration
        Dim appRdr As New System.Configuration.AppSettingsReader
        Dim objreport As New CrystalDecisions.CrystalReports.Engine.ReportDocument()
        objreport.Load(Application.StartupPath & "\reports\organizational_structure_with_parameter.rpt")
        objreport.SetDatabaseLogon(appRdr.GetValue("Username", GetType(String)), appRdr.GetValue("Password", GetType(String)), appRdr.GetValue("Server", GetType(String)), appRdr.GetValue("Database", GetType(String)))
        'DoCRLogin(objreport)
        objreport.Refresh()
        objreport.SetParameterValue("@parentid", orgid)
        CooperativeReports.appreports.ReportSource = objreport
        CooperativeReports.MdiParent = frmMain
        CooperativeReports.Show()
    End Sub
    Public Sub DoCRLogin(ByRef oRpt As CrystalDecisions.CrystalReports.Engine.ReportDocument)
        Dim _applyLogin As New applyCRlogin
        Dim d, s, p, u As New Clsappconfiguration
        Dim a, y, b, r As String

        y = d.Database
        a = s.Server
        b = p.Password
        r = u.Username

        ' use ApplyLogin object to apply login info to all tables in CR object
        _applyLogin.dbName = y '"Northwind"
        _applyLogin.passWord = b '"CrystalUser"
        _applyLogin.serverName = a '"(local)"
        _applyLogin.userID = r '"CrystalUser"
        _applyLogin.ApplyInfo(oRpt)
        ' clean up
        _applyLogin = Nothing
    End Sub

    Private Sub TreeView1_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles TreeView1.DragDrop
        'Check that there is a TreeNode being dragged
        If e.Data.GetDataPresent("System.Windows.Forms.TreeNode", True) = False Then Exit Sub

        'Get the TreeView raising the event (incase multiple on form)
        Dim selectedTreeview As TreeView = CType(sender, TreeView)

        'Get the TreeNode being dragged
        Dim dropNode As TreeNode = CType(e.Data.GetData("System.Windows.Forms.TreeNode"), TreeNode)

        'The target node should be selected from the DragOver event
        Dim targetNode As TreeNode = selectedTreeview.SelectedNode

        'Remove the drop node from its current location
        dropNode.Remove()

        'If there is no targetNode add dropNode to the bottom of the TreeView root
        'nodes, otherwise add it to the end of the dropNode child nodes
        If targetNode Is Nothing Then
            selectedTreeview.Nodes.Add(dropNode)
        Else
            targetNode.Nodes.Add(dropNode)
        End If

        'Ensure the newley created node is visible to the user and select it
        dropNode.EnsureVisible()
        selectedTreeview.SelectedNode = dropNode
    End Sub

    Private Sub TreeView1_DragOver(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles TreeView1.DragOver
        'Check that there is a TreeNode being dragged
        If e.Data.GetDataPresent("System.Windows.Forms.TreeNode", True) = False Then Exit Sub

        'Get the TreeView raising the event (incase multiple on form)
        Dim selectedTreeview As TreeView = CType(sender, TreeView)

        'As the mouse moves over nodes, provide feedback to the user
        'by highlighting the node that is the current drop target
        Dim pt As Point = CType(sender, TreeView).PointToClient(New Point(e.X, e.Y))
        Dim targetNode As TreeNode = selectedTreeview.GetNodeAt(pt)

        'See if the targetNode is currently selected, if so no need to validate again
        If Not (selectedTreeview Is targetNode) Then
            'Select the node currently under the cursor
            selectedTreeview.SelectedNode = targetNode

            'Check that the selected node is not the dropNode and also that it
            'is not a child of the dropNode and therefore an invalid target
            Dim dropNode As TreeNode = CType(e.Data.GetData("System.Windows.Forms.TreeNode"), TreeNode)
            Do Until targetNode Is Nothing
                If targetNode Is dropNode Then
                    e.Effect = DragDropEffects.None
                    Exit Sub
                End If
                targetNode = targetNode.Parent
            Loop
        End If

        'Currently selected node is a suitable target, allow the move
        e.Effect = DragDropEffects.Move

    End Sub

    Private Sub TreeView1_ItemDrag(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemDragEventArgs) Handles TreeView1.ItemDrag
        DoDragDrop(e.Item, DragDropEffects.Move)
    End Sub
End Class
