﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmChangeInterestRefund
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.btnChange = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.grdInterest = New System.Windows.Forms.DataGridView()
        Me.ColAcntCode = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColAcntName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColTransactionType = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColRefNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColAmount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ColOrigAmt = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bgLoad = New System.ComponentModel.BackgroundWorker()
        Me.lblLoading = New System.Windows.Forms.Label()
        Me.PanePanel5 = New WindowsApplication2.PanePanel()
        Me.lblloans = New System.Windows.Forms.Label()
        CType(Me.grdInterest, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanePanel5.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnChange
        '
        Me.btnChange.Enabled = False
        Me.btnChange.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnChange.Image = Global.WindowsApplication2.My.Resources.Resources.apply
        Me.btnChange.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnChange.Location = New System.Drawing.Point(518, 285)
        Me.btnChange.Name = "btnChange"
        Me.btnChange.Size = New System.Drawing.Size(82, 30)
        Me.btnChange.TabIndex = 0
        Me.btnChange.Text = "Change"
        Me.btnChange.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnChange.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Image = Global.WindowsApplication2.My.Resources.Resources.button_cancel
        Me.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnCancel.Location = New System.Drawing.Point(606, 285)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(82, 30)
        Me.btnCancel.TabIndex = 0
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'grdInterest
        '
        Me.grdInterest.AllowUserToAddRows = False
        Me.grdInterest.AllowUserToDeleteRows = False
        Me.grdInterest.AllowUserToResizeRows = False
        Me.grdInterest.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.grdInterest.BackgroundColor = System.Drawing.Color.White
        Me.grdInterest.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdInterest.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ColAcntCode, Me.ColAcntName, Me.ColTransactionType, Me.ColRefNo, Me.ColAmount, Me.ColOrigAmt})
        Me.grdInterest.Enabled = False
        Me.grdInterest.Location = New System.Drawing.Point(0, 32)
        Me.grdInterest.Name = "grdInterest"
        Me.grdInterest.RowHeadersVisible = False
        Me.grdInterest.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.grdInterest.Size = New System.Drawing.Size(688, 247)
        Me.grdInterest.TabIndex = 1
        '
        'ColAcntCode
        '
        Me.ColAcntCode.DataPropertyName = "acnt_code"
        DataGridViewCellStyle11.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        DataGridViewCellStyle11.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.Black
        Me.ColAcntCode.DefaultCellStyle = DataGridViewCellStyle11
        Me.ColAcntCode.HeaderText = "Account Code"
        Me.ColAcntCode.Name = "ColAcntCode"
        Me.ColAcntCode.ReadOnly = True
        '
        'ColAcntName
        '
        Me.ColAcntName.DataPropertyName = "acnt_name"
        DataGridViewCellStyle12.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        DataGridViewCellStyle12.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle12.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        DataGridViewCellStyle12.SelectionForeColor = System.Drawing.Color.Black
        Me.ColAcntName.DefaultCellStyle = DataGridViewCellStyle12
        Me.ColAcntName.HeaderText = "Account Name"
        Me.ColAcntName.Name = "ColAcntName"
        Me.ColAcntName.ReadOnly = True
        '
        'ColTransactionType
        '
        Me.ColTransactionType.DataPropertyName = "fcLoanTypeName"
        DataGridViewCellStyle13.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        DataGridViewCellStyle13.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle13.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        DataGridViewCellStyle13.SelectionForeColor = System.Drawing.Color.Black
        Me.ColTransactionType.DefaultCellStyle = DataGridViewCellStyle13
        Me.ColTransactionType.HeaderText = "Transaction Type"
        Me.ColTransactionType.Name = "ColTransactionType"
        Me.ColTransactionType.ReadOnly = True
        '
        'ColRefNo
        '
        Me.ColRefNo.DataPropertyName = "Reference"
        DataGridViewCellStyle14.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        DataGridViewCellStyle14.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle14.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        DataGridViewCellStyle14.SelectionForeColor = System.Drawing.Color.Black
        Me.ColRefNo.DefaultCellStyle = DataGridViewCellStyle14
        Me.ColRefNo.HeaderText = "Reference No."
        Me.ColRefNo.Name = "ColRefNo"
        Me.ColRefNo.ReadOnly = True
        '
        'ColAmount
        '
        Me.ColAmount.DataPropertyName = "Amount"
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle15.Format = "N2"
        DataGridViewCellStyle15.NullValue = "0"
        Me.ColAmount.DefaultCellStyle = DataGridViewCellStyle15
        Me.ColAmount.HeaderText = "Amount"
        Me.ColAmount.Name = "ColAmount"
        '
        'ColOrigAmt
        '
        Me.ColOrigAmt.DataPropertyName = "OrigAmt"
        Me.ColOrigAmt.HeaderText = "Original Amount"
        Me.ColOrigAmt.Name = "ColOrigAmt"
        Me.ColOrigAmt.ReadOnly = True
        Me.ColOrigAmt.Visible = False
        '
        'bgLoad
        '
        '
        'lblLoading
        '
        Me.lblLoading.Image = Global.WindowsApplication2.My.Resources.Resources.loading13
        Me.lblLoading.Location = New System.Drawing.Point(316, 114)
        Me.lblLoading.Name = "lblLoading"
        Me.lblLoading.Size = New System.Drawing.Size(62, 45)
        Me.lblLoading.TabIndex = 2
        '
        'PanePanel5
        '
        Me.PanePanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel5.Controls.Add(Me.lblloans)
        Me.PanePanel5.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanePanel5.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.PanePanel5.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel5.InactiveGradientHighColor = System.Drawing.Color.Green
        Me.PanePanel5.InactiveGradientLowColor = System.Drawing.Color.GreenYellow
        Me.PanePanel5.Location = New System.Drawing.Point(0, 0)
        Me.PanePanel5.Name = "PanePanel5"
        Me.PanePanel5.Size = New System.Drawing.Size(688, 26)
        Me.PanePanel5.TabIndex = 37
        '
        'lblloans
        '
        Me.lblloans.AutoSize = True
        Me.lblloans.BackColor = System.Drawing.Color.Transparent
        Me.lblloans.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold)
        Me.lblloans.ForeColor = System.Drawing.Color.White
        Me.lblloans.Location = New System.Drawing.Point(3, 2)
        Me.lblloans.Name = "lblloans"
        Me.lblloans.Size = New System.Drawing.Size(190, 19)
        Me.lblloans.TabIndex = 14
        Me.lblloans.Text = "Change Interest Refund"
        '
        'frmChangeInterestRefund
        '
        Me.AcceptButton = Me.btnChange
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(688, 317)
        Me.Controls.Add(Me.PanePanel5)
        Me.Controls.Add(Me.lblLoading)
        Me.Controls.Add(Me.grdInterest)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnChange)
        Me.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ForeColor = System.Drawing.SystemColors.ControlText
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmChangeInterestRefund"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Change Interest Refund"
        CType(Me.grdInterest, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanePanel5.ResumeLayout(False)
        Me.PanePanel5.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnChange As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents grdInterest As System.Windows.Forms.DataGridView
    Friend WithEvents bgLoad As System.ComponentModel.BackgroundWorker
    Friend WithEvents lblLoading As System.Windows.Forms.Label
    Friend WithEvents ColAcntCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColAcntName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColTransactionType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColRefNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ColOrigAmt As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PanePanel5 As WindowsApplication2.PanePanel
    Friend WithEvents lblloans As System.Windows.Forms.Label
End Class
