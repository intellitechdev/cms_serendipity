﻿Imports System.Data.Sql
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Public Class frmMember_Trainings

    Dim pkTrainings As String
    Dim fpath As String
    Dim filenym As String
    Dim fileData As Byte()
    Dim FileNames As String
    Dim ex As String
    Public FilefromMem As String
    Dim filesFormdb As Byte()
    Dim HoursDays As String

#Region "Property"
    Public Property getpkTrainings() As String
        Get
            Return pkTrainings
        End Get
        Set(ByVal value As String)
            pkTrainings = value
        End Set
    End Property
#End Region

    Private Sub AddEditTrainigs(ByVal EmpNo As String, ByVal Title As String, ByVal OfferedBy As String, ByVal dtFrom As Date, _
                                ByVal NoHrs As Integer, ByVal Filename As String, ByVal AttachedFiles As Byte(), _
                                 ByVal pk_Trainings As String)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()
        Try
            If rbHours.Checked = True Then
                HoursDays = "Hours"
            ElseIf rbDays.Checked = True Then
                HoursDays = "Days"
            End If
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Member_Trainings_AddEdit", _
                                     New SqlParameter("@employeeNo", EmpNo), _
                                     New SqlParameter("@From", dtFrom), _
                                     New SqlParameter("@OfferedBy", OfferedBy), _
                                     New SqlParameter("@TrainingTitle", Title), _
                                     New SqlParameter("@Hours", NoHrs), _
                                     New SqlParameter("@HrsDays", HoursDays), _
                                     New SqlParameter("@FileName", Filename), _
                                     New SqlParameter("@AttachedFiles", AttachedFiles), _
                                     New SqlParameter("@pk_Trainings", pk_Trainings))
            trans.Commit()
            MessageBox.Show("Record has been Saved", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "Save Training")
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub

    Private Sub txtHrs_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtHrs.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                MessageBox.Show("Invalid Input! This field Allow 0-9 Only.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub txtCost_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                MessageBox.Show("Invalid Input! This field Allow 0-9 Only.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub ClearText()
        txtHrs.Text = ""
        txtOfferedBy.Text = ""
        txtTitle.Text = ""
        rbDays.Checked = False
        rbHours.Checked = False
        txtAttachFiles.Text = ""
    End Sub

    Private Sub btnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse.Click
        Try
            Dim openFileDialog1 As New OpenFileDialog()

            openFileDialog1.Filter = "Text Files (*.txt)|*.txt|PDF Files (*.pdf)|*.pdf|Word Documents (*.docx)|*.docx|Excel Worksheets (*.xlsx)|*.xlsx|PowerPoint Presentations (*.pptx)|*.pptx|Word Documents 97-2003 (*.doc)|*.docx|Excel Worksheets 93-2003 (*.xls)|*.xls|PowerPoint Presentations (*.ppt)|*.ppt" & "|Office Files|*.docx;*.xlsx;*.pptx;*.doc;*.xls;*.ppt" & "|Images|*.jpg;*.png;*.gif"
            openFileDialog1.Title = "Select File"
            If openFileDialog1.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
                filenym = openFileDialog1.FileName
                fpath = Path.GetExtension(filenym)
                txtAttachFiles.Text = Path.GetFileName(filenym)

                fileData = File.ReadAllBytes(filenym)
                ex = Path.GetExtension(LTrim(RTrim(filenym)))

                Dim ms As New MemoryStream(fileData, 0, fileData.Length)

                ms.Write(fileData, 0, fileData.Length)
            Else
                MessageBox.Show("User Cancelled!", "Cancelled", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        If txtTitle.Text = "" Or txtOfferedBy.Text = "" Or dtFrom.Text = "" Or txtHrs.Text = "" Then
            MessageBox.Show("Complete Information is Needed", "Training Information", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        Else
            'GetFilenames(frmMember_Master.txtEmployeeNo.Text)
            'If Windows.Forms.DialogResult.OK Then
            '    Exit Sub
            'Else
            Call AddEditTrainigs(frmMember_Master.txtEmployeeNo.Text.Trim, Me.txtTitle.Text.Trim, Me.txtOfferedBy.Text.Trim,
                                 Me.dtFrom.Value, Me.txtHrs.Text.Trim, txtAttachFiles.Text, fileData, "")
            Call frmMember_Master.GetmemberTrainings(frmMember_Master.txtEmployeeNo.Text)
            ClearText()
            '    Me.Close()
            'End If
        End If
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If fileData Is Nothing Then
            Dim myconnection As New Clsappconfiguration
            Dim cmd As New SqlCommand("MSS_MembersInfo_GetTrainings_AttachedFiles", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            myconnection.sqlconn.Open()
            cmd.Parameters.Add("@EmployeeNo", SqlDbType.VarChar, 256).Value = frmMember_Master.txtEmployeeNo.Text
            cmd.Parameters.Add("@FileName", SqlDbType.VarChar, 100).Value = txtAttachFiles.Text
            Using myreader As SqlDataReader = cmd.ExecuteReader
                myreader.Read()
                filesFormdb = DirectCast(myreader("fcAttachedFiles"), Byte())
            End Using
            Call AddEditTrainigs(frmMember_Master.txtEmployeeNo.Text.Trim, Me.txtTitle.Text.Trim, Me.txtOfferedBy.Text.Trim,
                                 Me.dtFrom.Value, Me.txtHrs.Text.Trim, txtAttachFiles.Text, filesFormdb, pkTrainings)
            Call frmMember_Master.GetmemberTrainings(frmMember_Master.txtEmployeeNo.Text)
            Me.Close()
        Else
            Call AddEditTrainigs(frmMember_Master.txtEmployeeNo.Text.Trim, Me.txtTitle.Text.Trim, Me.txtOfferedBy.Text.Trim,
                                 Me.dtFrom.Value, Me.txtHrs.Text.Trim, txtAttachFiles.Text, fileData, pkTrainings)
            Call frmMember_Master.GetmemberTrainings(frmMember_Master.txtEmployeeNo.Text)
            Me.Close()
        End If
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub GetFilenames(ByVal EmpId As String)
        Dim Names As String
        Dim myconnection As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(myconnection.cnstring, "MSS_MembersInfo_GetTrainings",
                                         New SqlParameter("@EmployeeNo", EmpId))
        While rd.Read
            Names = rd.Item("FileName")
            If txtAttachFiles.Text = Names Then
                MessageBox.Show("File Name Already Exists", "Training Attach File", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            End If
        End While
    End Sub
End Class