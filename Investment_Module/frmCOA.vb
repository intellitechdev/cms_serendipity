﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmCOA

    Private con As New Clsappconfiguration
    Dim ds As DataSet

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelect.Click
        GetSelectedValue()
    End Sub

    Private Function isLoaded(ByVal Search As String) As Boolean
        Try
            dgvList.Columns.Clear()
            ds = SqlHelper.ExecuteDataset(con.cnstring, "_Investment_Master_Select_Accts",
                                           New SqlParameter("@search", Search))
            dgvList.DataSource = ds.Tables(0)
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Sub frmCOA_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If isLoaded("") Then
            ActiveControl = txtSearch
        End If
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        If isLoaded(txtSearch.Text) Then

        End If
    End Sub

    Private Sub dgvList_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvList.DoubleClick
        GetSelectedValue()
    End Sub

    Private Sub GetSelectedValue()
        Try
            With dgvList
                frmInvestmentSetup.txtCode.Text = .SelectedRows(0).Cells(0).Value.ToString
                frmInvestmentSetup.txtInvestmentType.Text = .SelectedRows(0).Cells(1).Value.ToString
                Me.Close()
            End With
        Catch ex As Exception
            Throw
        End Try
    End Sub
End Class