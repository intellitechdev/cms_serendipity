<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLoan_Policy_MaxTermLimit
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLoan_Policy_MaxTermLimit))
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.txtLower = New System.Windows.Forms.TextBox
        Me.txtUpper = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.txtTermTo = New System.Windows.Forms.TextBox
        Me.txtTermfrom = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.cboloantype = New System.Windows.Forms.ComboBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.lvlMaxTerm = New System.Windows.Forms.ListView
        Me.txttemp = New System.Windows.Forms.TextBox
        Me.PanePanel1 = New WindowsApplication2.PanePanel
        Me.btnclose = New System.Windows.Forms.Button
        Me.btnsave = New System.Windows.Forms.Button
        Me.btndelete = New System.Windows.Forms.Button
        Me.btnupdate = New System.Windows.Forms.Button
        Me.picStep1 = New WindowsApplication2.PanePanel
        Me.lblStepone = New System.Windows.Forms.Label
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.PanePanel1.SuspendLayout()
        Me.picStep1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.txtLower)
        Me.GroupBox2.Controls.Add(Me.txtUpper)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Location = New System.Drawing.Point(206, 69)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(186, 67)
        Me.GroupBox2.TabIndex = 41
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Limit"
        '
        'txtLower
        '
        Me.txtLower.Location = New System.Drawing.Point(78, 16)
        Me.txtLower.Name = "txtLower"
        Me.txtLower.Size = New System.Drawing.Size(96, 20)
        Me.txtLower.TabIndex = 41
        Me.txtLower.Text = "0.00"
        '
        'txtUpper
        '
        Me.txtUpper.Location = New System.Drawing.Point(78, 39)
        Me.txtUpper.Name = "txtUpper"
        Me.txtUpper.Size = New System.Drawing.Size(96, 20)
        Me.txtUpper.TabIndex = 40
        Me.txtUpper.Text = "0.00"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(36, 19)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(36, 13)
        Me.Label4.TabIndex = 42
        Me.Label4.Text = "Lower"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(36, 42)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(36, 13)
        Me.Label5.TabIndex = 39
        Me.Label5.Text = "Upper"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtTermTo)
        Me.GroupBox1.Controls.Add(Me.txtTermfrom)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Location = New System.Drawing.Point(13, 69)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(184, 67)
        Me.GroupBox1.TabIndex = 40
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Term"
        '
        'txtTermTo
        '
        Me.txtTermTo.Location = New System.Drawing.Point(63, 36)
        Me.txtTermTo.Name = "txtTermTo"
        Me.txtTermTo.Size = New System.Drawing.Size(81, 20)
        Me.txtTermTo.TabIndex = 38
        Me.txtTermTo.Text = "0.0"
        '
        'txtTermfrom
        '
        Me.txtTermfrom.Location = New System.Drawing.Point(63, 13)
        Me.txtTermfrom.Name = "txtTermfrom"
        Me.txtTermfrom.Size = New System.Drawing.Size(81, 20)
        Me.txtTermfrom.TabIndex = 37
        Me.txtTermfrom.Text = "0.0"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(31, 39)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(20, 13)
        Me.Label3.TabIndex = 38
        Me.Label3.Text = "To"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(21, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(30, 13)
        Me.Label2.TabIndex = 37
        Me.Label2.Text = "From"
        '
        'cboloantype
        '
        Me.cboloantype.FormattingEnabled = True
        Me.cboloantype.Location = New System.Drawing.Point(76, 38)
        Me.cboloantype.Name = "cboloantype"
        Me.cboloantype.Size = New System.Drawing.Size(121, 21)
        Me.cboloantype.TabIndex = 39
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 42)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(63, 13)
        Me.Label1.TabIndex = 38
        Me.Label1.Text = "Policy Code"
        '
        'lvlMaxTerm
        '
        Me.lvlMaxTerm.Location = New System.Drawing.Point(10, 142)
        Me.lvlMaxTerm.Name = "lvlMaxTerm"
        Me.lvlMaxTerm.Size = New System.Drawing.Size(379, 210)
        Me.lvlMaxTerm.TabIndex = 43
        Me.lvlMaxTerm.UseCompatibleStateImageBehavior = False
        '
        'txttemp
        '
        Me.txttemp.Location = New System.Drawing.Point(206, 38)
        Me.txttemp.Name = "txttemp"
        Me.txttemp.Size = New System.Drawing.Size(33, 20)
        Me.txttemp.TabIndex = 44
        Me.txttemp.Visible = False
        '
        'PanePanel1
        '
        Me.PanePanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel1.Controls.Add(Me.btnclose)
        Me.PanePanel1.Controls.Add(Me.btnsave)
        Me.PanePanel1.Controls.Add(Me.btndelete)
        Me.PanePanel1.Controls.Add(Me.btnupdate)
        Me.PanePanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PanePanel1.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.PanePanel1.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel1.InactiveGradientHighColor = System.Drawing.Color.White
        Me.PanePanel1.InactiveGradientLowColor = System.Drawing.Color.Green
        Me.PanePanel1.Location = New System.Drawing.Point(0, 358)
        Me.PanePanel1.Name = "PanePanel1"
        Me.PanePanel1.Size = New System.Drawing.Size(400, 37)
        Me.PanePanel1.TabIndex = 33
        '
        'btnclose
        '
        Me.btnclose.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnclose.Image = CType(resources.GetObject("btnclose.Image"), System.Drawing.Image)
        Me.btnclose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnclose.Location = New System.Drawing.Point(219, 4)
        Me.btnclose.Name = "btnclose"
        Me.btnclose.Size = New System.Drawing.Size(66, 28)
        Me.btnclose.TabIndex = 9
        Me.btnclose.Text = "Close"
        Me.btnclose.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnclose.UseVisualStyleBackColor = True
        '
        'btnsave
        '
        Me.btnsave.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnsave.Image = Global.WindowsApplication2.My.Resources.Resources.new3
        Me.btnsave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnsave.Location = New System.Drawing.Point(8, 4)
        Me.btnsave.Name = "btnsave"
        Me.btnsave.Size = New System.Drawing.Size(66, 28)
        Me.btnsave.TabIndex = 6
        Me.btnsave.Text = "New"
        Me.btnsave.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnsave.UseVisualStyleBackColor = True
        '
        'btndelete
        '
        Me.btndelete.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btndelete.Image = CType(resources.GetObject("btndelete.Image"), System.Drawing.Image)
        Me.btndelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btndelete.Location = New System.Drawing.Point(142, 4)
        Me.btndelete.Name = "btndelete"
        Me.btndelete.Size = New System.Drawing.Size(66, 28)
        Me.btndelete.TabIndex = 8
        Me.btndelete.Text = "Delete"
        Me.btndelete.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btndelete.UseVisualStyleBackColor = True
        '
        'btnupdate
        '
        Me.btnupdate.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnupdate.Image = CType(resources.GetObject("btnupdate.Image"), System.Drawing.Image)
        Me.btnupdate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnupdate.Location = New System.Drawing.Point(75, 4)
        Me.btnupdate.Name = "btnupdate"
        Me.btnupdate.Size = New System.Drawing.Size(66, 28)
        Me.btnupdate.TabIndex = 7
        Me.btnupdate.Text = "Edit"
        Me.btnupdate.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnupdate.UseVisualStyleBackColor = True
        '
        'picStep1
        '
        Me.picStep1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picStep1.Controls.Add(Me.lblStepone)
        Me.picStep1.Dock = System.Windows.Forms.DockStyle.Top
        Me.picStep1.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.picStep1.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.picStep1.InactiveGradientHighColor = System.Drawing.Color.Green
        Me.picStep1.InactiveGradientLowColor = System.Drawing.Color.White
        Me.picStep1.Location = New System.Drawing.Point(0, 0)
        Me.picStep1.Name = "picStep1"
        Me.picStep1.Size = New System.Drawing.Size(400, 26)
        Me.picStep1.TabIndex = 32
        '
        'lblStepone
        '
        Me.lblStepone.AutoSize = True
        Me.lblStepone.BackColor = System.Drawing.Color.Transparent
        Me.lblStepone.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStepone.ForeColor = System.Drawing.Color.White
        Me.lblStepone.Location = New System.Drawing.Point(16, 2)
        Me.lblStepone.Name = "lblStepone"
        Me.lblStepone.Size = New System.Drawing.Size(99, 19)
        Me.lblStepone.TabIndex = 18
        Me.lblStepone.Text = "Max Terms"
        '
        'frmMaxtermLimitMaster
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(400, 395)
        Me.Controls.Add(Me.txttemp)
        Me.Controls.Add(Me.lvlMaxTerm)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.cboloantype)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.PanePanel1)
        Me.Controls.Add(Me.picStep1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMaxtermLimitMaster"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Max Term Limit Master"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.PanePanel1.ResumeLayout(False)
        Me.picStep1.ResumeLayout(False)
        Me.picStep1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblStepone As System.Windows.Forms.Label
    Friend WithEvents picStep1 As WindowsApplication2.PanePanel
    Friend WithEvents btnsave As System.Windows.Forms.Button
    Friend WithEvents PanePanel1 As WindowsApplication2.PanePanel
    Friend WithEvents btnclose As System.Windows.Forms.Button
    Friend WithEvents btndelete As System.Windows.Forms.Button
    Friend WithEvents btnupdate As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents txtLower As System.Windows.Forms.TextBox
    Friend WithEvents txtUpper As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtTermTo As System.Windows.Forms.TextBox
    Friend WithEvents txtTermfrom As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cboloantype As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lvlMaxTerm As System.Windows.Forms.ListView
    Friend WithEvents txttemp As System.Windows.Forms.TextBox
End Class
