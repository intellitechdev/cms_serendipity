﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmMember_SearchClient

    Private Sub btnemp_close_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnemp_close.Click
        dgvClient.Columns.Clear()
        Me.Close()
    End Sub

    Private Sub btnfind_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnfind.Click
        GetClientInfo(txtid.Text)
    End Sub

    Private Sub GetClientInfo(ByVal ClientID As String)
        Dim gcon As New Clsappconfiguration()
        Dim ds As New DataSet

        Try
            dgvClient.Columns.Clear()
            ds = SqlHelper.ExecuteDataset(gcon.cnstring, CommandType.StoredProcedure, "MSS_MembersInfo_GetClientInfo", _
                                New SqlParameter("@employeeNo", ClientID))
            dgvClient.DataSource = ds.Tables(0)
            GridFormat()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub txtid_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtid.TextChanged
        GetClientInfo(txtid.Text)
    End Sub

    Private Sub frmMember_SearchClient_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        GetClientInfo("")
    End Sub

    Private Sub GridFormat()
        With dgvClient
            .Columns(0).Width = 130
            .Columns(1).Width = 400
            .Columns(2).Visible = False
            .Columns(3).Visible = False
            .Columns(4).Visible = False
            .Columns(5).Visible = False
            .Columns(6).Visible = False
            .Columns(7).Visible = False
            .Columns(8).Visible = False
            .Columns(9).Visible = False
            .Columns(10).Visible = False
            .Columns(11).Visible = False
            .Columns(12).Visible = False
            .Columns(13).Visible = False
            .Columns(14).Visible = False
            .Columns(15).Visible = False
            .Columns(16).Visible = False
        End With
    End Sub

    Private Sub SelectClient()
        With frmMember_Client
            .txtClientNo.Text = dgvClient.CurrentRow.Cells(0).Value
            .txtClientName.Text = dgvClient.CurrentRow.Cells(1).Value
            .txtContactPerson.Text = dgvClient.CurrentRow.Cells(2).Value
            .txtTelNo.Text = dgvClient.CurrentRow.Cells(3).Value
            .txtTIN.Text = dgvClient.CurrentRow.Cells(4).Value
            .txtAddress.Text = dgvClient.CurrentRow.Cells(5).Value
            .txtDescription.Text = dgvClient.CurrentRow.Cells(6).Value
            .dtPayrollPeriod.Text = dgvClient.CurrentRow.Cells(7).Value
            .dtPayPeriod.Text = dgvClient.CurrentRow.Cells(8).Value
            .dtBilling.Text = dgvClient.CurrentRow.Cells(9).Value
            .dtPayroll.Text = dgvClient.CurrentRow.Cells(10).Value
            .dtCollection.Text = dgvClient.CurrentRow.Cells(11).Value
            .dtPayRelease.Text = dgvClient.CurrentRow.Cells(12).Value
            .cboProjectSpecialist.Text = dgvClient.CurrentRow.Cells(13).Value
            .cboMemStatus.Text = dgvClient.CurrentRow.Cells(14).Value
        End With
    End Sub

    Private Sub btnok_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnok.Click
        SelectClient()
        frmMember_Client.picClientPhoto.Image = My.Resources.noLogo
        frmMember_Client.LoadPicture(frmMember_Client.txtClientNo.Text)
        txtid.Text = ""
        Me.Close()
    End Sub
End Class