Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Security
Imports System.Security.Principal.WindowsIdentity
Imports Microsoft.ApplicationBlocks.Data


Public Class frmLogin
#Region "variables"
    Public server As String
    Public database As String
    Public admin As String
#End Region
#Region "Get property"
    Public Property getservername() As String
        Get
            Return Server
        End Get
        Set(ByVal value As String)
            Server = value
        End Set
    End Property

    Public Property getdatabasename() As String
        Get
            Return Database
        End Get
        Set(ByVal value As String)
            Database = value
        End Set
    End Property
    Public Property getAdmin() As String
        Get
            Return admin
        End Get
        Set(ByVal value As String)
            admin = value
        End Set
    End Property
#End Region

    Private Sub OK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK.Click
        Call login2()
        Call userRights()

    End Sub

    Private Sub Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel.Click
        End
    End Sub

    Public Sub login2()
        Dim myconnection As New Clsappconfiguration
        Try
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(myconnection.cnstring, CommandType.StoredProcedure, "USP_EMPLOYEE_LOGIN_SELECT", _
                        New SqlParameter("@username", Username.Text), _
                        New SqlParameter("@password", Password.Text))

            If rd.Read() Then

                'Get User Id
                Dim userID As String = rd.Item(2).ToString()
                frmMain.GetLoggedInUserID() = userID
                mod_UsersAccessibility.getusername = rd.Item(0).ToString()
                mod_UsersAccessibility.xUser = rd.Item(0).ToString()
                'Show Main form and Hide Login Form
                frmMain.Show()
                Hide()

                'Call Members on Hold for notification
                frmNotifications.Show()
            Else
                MessageBox.Show("login failed", "UcoreSoftware", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Username.Focus()
            End If
            rd.Close()
        Catch ex As Exception
        End Try
    End Sub

    Private Sub LoginForm1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim getser, getbase As New Clsappconfiguration

        Me.txtServer.Text = getser.getservername1
        Me.txtDatabase.Text = getbase.getdatabasename1

        'Me.txtServer.ReadOnly = True
        'Me.txtDatabase.ReadOnly = True

        cbousertype.Text = ""
        'Label1.Visible = False
        'Label1.Text = ""
        'Label2.Visible = False

    End Sub
#Region "User rights"
    Public Sub userRights()
        Try
            Dim myconnection As New Clsappconfiguration
            Dim cmd As New SqlCommand("USP_EMPLOYEE_LOGIN_SELECT_User", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            myconnection.sqlconn.Open()

            cmd.Parameters.Add("@username", SqlDbType.VarChar, 50).Value = Username.Text
            cmd.Parameters.Add("@password", SqlDbType.VarChar, 50).Value = Password.Text

            Dim myreader As SqlDataReader
            myreader = cmd.ExecuteReader
            Try
                While myreader.Read
                    getadmin = myreader.Item(2)
                End While
                myreader.Close()

            Finally
                myconnection.sqlconn.Close()
            End Try
        Catch ex As Exception
            MessageBox.Show("Check your login", "User rights", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub
#End Region
    Public Sub load_usertype_in_combobox()
        Dim myadapter As New SqlDataAdapter
        Dim mydataset As New DataSet
        Dim appRdr As New System.Configuration.AppSettingsReader
        Dim myconnection As New Clsappconfiguration
        Dim cmdjol As String = "Select usertype from dbo.employee_usertype"
        Dim cmd As New SqlCommand(cmdjol, myconnection.sqlconn)

        cmd.Parameters.Add("@usertype", SqlDbType.VarChar, 50, ParameterDirection.Input).Value = cbousertype.Text
        myconnection.sqlconn.Open()

        Dim mydatareader As SqlDataReader = cmd.ExecuteReader()

        Try
            While mydatareader.Read()

                cbousertype.Items.Add(mydatareader(0))
            End While
        Finally
            mydatareader.Close()
            myconnection.sqlconn.Close()
        End Try
    End Sub
    Private Sub load_usertype()
        Dim appRdr As New System.Configuration.AppSettingsReader
        Dim myconnection As New Clsappconfiguration

        Dim cmd As New SqlCommand("usp_load_usertype_select", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("@usertype", SqlDbType.VarChar, 50).Value = cbousertype.Text

        myconnection.sqlconn.Open()
        cmd.ExecuteNonQuery()
        myconnection.sqlconn.Close()
    End Sub

    Private Sub Password_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Password.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            Call login2()
            Call userRights()
        End If
    End Sub

    Private Sub Panel1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs)

    End Sub

    Private Sub btnconnection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnconnection.Click
        frmAdmin_ChangeDBConnection.ShowDialog()
        txtServer.Text = frmAdmin_ChangeDBConnection.getserver
        txtDatabase.Text = frmAdmin_ChangeDBConnection.getdatabase
    End Sub

    Private Sub Username_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Username.Enter
        Username.SelectAll()
    End Sub

    Private Sub Password_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Password.Enter
        Password.SelectAll()
    End Sub

    Private Sub lblEnteng_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs)
        Enteng()
        entengRights()
    End Sub

    Private Sub Enteng()
        Dim myconnection As New Clsappconfiguration
        Try
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(myconnection.cnstring, CommandType.StoredProcedure, "USP_EMPLOYEE_LOGIN_SELECT", _
                        New SqlParameter("@username", "sa"), _
                        New SqlParameter("@password", "sa"))

            If rd.Read() Then

                'Get User Id
                Dim userID As String = rd.Item(2).ToString()
                frmMain.GetLoggedInUserID() = userID
                mod_UsersAccessibility.getusername = rd.Item(0).ToString()
                mod_UsersAccessibility.xUser = rd.Item(0).ToString()
                'Show Main form and Hide Login Form
                frmMain.Show()
                Hide()

                'Call Members on Hold for notification
                frmNotifications.Show()
            Else
                MessageBox.Show("login failed", "UcoreSoftware", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Username.Focus()
            End If
            rd.Close()
        Catch ex As Exception
        End Try
    End Sub

    Private Sub entengRights()
        Try
            Dim myconnection As New Clsappconfiguration
            Dim cmd As New SqlCommand("USP_EMPLOYEE_LOGIN_SELECT_User", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            myconnection.sqlconn.Open()

            cmd.Parameters.Add("@username", SqlDbType.VarChar, 50).Value = "sa"
            cmd.Parameters.Add("@password", SqlDbType.VarChar, 50).Value = "sa"

            Dim myreader As SqlDataReader
            myreader = cmd.ExecuteReader
            Try
                While myreader.Read
                    getadmin = myreader.Item(2)
                End While
                myreader.Close()

            Finally
                myconnection.sqlconn.Close()
            End Try
        Catch ex As Exception
            MessageBox.Show("Check your login", "User rights", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Private Sub Username_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Username.KeyDown
        If e.KeyCode = Keys.Enter Then
            Password.Focus()
        End If
    End Sub
End Class
