﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmLoanBalChecker

    Private con As New Clsappconfiguration

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        _Check()
    End Sub

    Private Sub _Check()
        Try
            Dim ds As DataSet = SqlHelper.ExecuteDataset(con.cnstring, "_LOANBAL_Check")
            DataGridView1.DataSource = ds.Tables(0)
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        _Correct()
        _Check()
    End Sub

    Private Sub _Correct()
        Try
            SqlHelper.ExecuteNonQuery(con.cnstring, "_LOANBAL_Correct")
        Catch ex As Exception
            Throw
        End Try
    End Sub
End Class