﻿Imports System.Data.Sql
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmMember_Organizations
    Dim pk_EmpOrg As String

#Region "Property"
    Public Property getpkEmpOrg() As String
        Get
            Return pk_EmpOrg
        End Get
        Set(ByVal value As String)
            pk_EmpOrg = value
        End Set
    End Property
#End Region

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        Me.Close()
    End Sub

    Private Sub AddEditEmpOrg(ByVal EmpNo As String, ByVal OrgName As String, ByVal Add As String, ByVal Pos As String, ByVal Period As String, ByVal pkEmpOrg As String)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Member_EmpOrg_AddEdit", _
                                     New SqlParameter("@employeeNo", EmpNo), _
                                     New SqlParameter("@fcName", OrgName), _
                                     New SqlParameter("@fcAdd", Add), _
                                     New SqlParameter("@fcPos", Pos), _
                                     New SqlParameter("@fcPeriod", Period), _
                                     New SqlParameter("@pk_EmpOrg", pkEmpOrg))
            trans.Commit()
            MessageBox.Show("Record has been Saved", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "Save Employmee Organization")
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If txtName.Text = "" Or txtAdd.Text = "" Or txtPosition.Text = "" Or txtPeriod.Text = "" Then
            MessageBox.Show("Complete Information is Needed", "Employee Organization Information", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        Else
            Call AddEditEmpOrg(frmMember_Master.txtEmployeeNo.Text.Trim, Me.txtName.Text.Trim, Me.txtAdd.Text.Trim, Me.txtPosition.Text.Trim, Me.txtPeriod.Text.Trim, "")
            Call frmMember_Master.GetmemberEmpOrg(frmMember_Master.txtEmployeeNo.Text)
            Call ClearText()
            'Me.Close()
        End If
    End Sub

    Private Sub btnupdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupdate.Click
        Call AddEditEmpOrg(frmMember_Master.txtEmployeeNo.Text.Trim, Me.txtName.Text.Trim, Me.txtAdd.Text.Trim, Me.txtPosition.Text.Trim, Me.txtPeriod.Text.Trim, pk_EmpOrg)
        Call frmMember_Master.GetmemberEmpOrg(frmMember_Master.txtEmployeeNo.Text)
        Me.Close()
    End Sub

    Private Sub ClearText()
        txtAdd.Text = ""
        txtName.Text = ""
        txtPeriod.Text = ""
        txtPosition.Text = ""
    End Sub
End Class