﻿Imports System.Data.Sql
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
'############################
'Updated by :Vincent Nacar
'Date :May 21,2014
'###########################
Public Class frmLoan_Approval

#Region "Declaration"
    Private gcon As New Clsappconfiguration
    Private Empinfo, EmployeeNo, LoanNo As String
#End Region

#Region "Pubic Property"
    Public Property getEmpinfo() As String
        Get
            Return Empinfo
        End Get
        Set(ByVal value As String)
            Empinfo = value
        End Set
    End Property
#End Region

#Region "Events"

    Private Sub frmLoan_Approval_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtSearch.Focus()
    End Sub

    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        'Search_Employee(txtSearch.Text, "On-Process")
        Search_Employee(txtSearch.Text, "Completed")
    End Sub

    Private Sub btnApprove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        frmChkVoucherEntry.LoanNo = LoanNo
        frmChkVoucherEntry.StartPosition = FormStartPosition.CenterScreen
        frmChkVoucherEntry.ShowDialog()
        If frmChkVoucherEntry.keyresult = "Ok" Then
            ApprovedLoan(LoanNo, txtReason.Text)
            Search_Employee(txtSearch.Text, "Completed")
            ViewLoan(txtEmployeeNumber.Text, "Completed", txtApprover.Tag)
        End If
    End Sub

    Private Sub btnDisapprove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDisapprove.Click
        DisapproveLoan(LoanNo, txtReason.Text)
        Search_Employee(txtSearch.Text, "Completed")
        ViewLoan(txtEmployeeNumber.Text, "Completed", txtApprover.Tag)
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub grdMemberList_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdMemberList.CellContentClick
        Dim row As DataGridViewRow
        For Each row In grdMemberList.SelectedRows
            txtEmployeeNumber.Text = row.Cells(0).Value.ToString
            txtFullName.Text = row.Cells(1).Value.ToString
            txtPosition.Text = row.Cells(2).Value.ToString
            txtCivilStatus.Text = row.Cells(3).Value.ToString
            txtSalary.Text = row.Cells(4).Value.ToString
            txtTotalLoanBalance.Text = row.Cells(5).Value.ToString
            txtApprovedLoans.Text = row.Cells(6).Value.ToString
            txtDisapprovedLoans.Text = row.Cells(7).Value.ToString
            frmLoan_ApproverList.Employee = row.Cells(0).Value.ToString
            ViewLoan(row.Cells(0).Value, "Completed", txtApprover.Tag)
            btnApprove.Enabled = False
            btnDisapprove.Enabled = False
        Next
    End Sub

    Private Sub btnFind_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFind.Click
        frmLoan_ApproverList.ShowDialog()
    End Sub

    Private Sub grdMemberList_CellClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdMemberList.CellClick
        Dim row As DataGridViewRow
        For Each row In grdMemberList.SelectedRows
            txtEmployeeNumber.Text = row.Cells(0).Value.ToString
            txtFullName.Text = row.Cells(1).Value.ToString
            txtPosition.Text = row.Cells(2).Value.ToString
            txtCivilStatus.Text = row.Cells(3).Value.ToString
            txtSalary.Text = row.Cells(4).Value.ToString
            txtTotalLoanBalance.Text = row.Cells(5).Value.ToString
            txtApprovedLoans.Text = row.Cells(6).Value.ToString
            txtDisapprovedLoans.Text = row.Cells(7).Value.ToString
            frmLoan_ApproverList.Employee = row.Cells(0).Value.ToString
            ViewLoan(row.Cells(0).Value, "Completed", txtApprover.Tag)
            btnApprove.Enabled = False
            btnDisapprove.Enabled = False
        Next
    End Sub

#End Region

#Region "Script"


    Public Sub ViewLoan(ByVal Empinfo As String, ByVal status As String, ByVal approver As String)
        Try
            GrdLoanSummary.DataSource = Nothing

            GrdLoanSummary.Columns.Clear()
            Dim ds As DataSet = SqlHelper.ExecuteDataset(gcon.cnstring, CommandType.StoredProcedure, "MSS_LoanTracking_GetLoans_ForApproval", _
                                          New SqlParameter("@EmployeeNo", Empinfo),
                                          New SqlParameter("@status", status),
                                          New SqlParameter("@approverEmployeeNo", approver))
            With GrdLoanSummary
                '.Rows.Clear()
                '.Columns.Clear()

                .DataSource = ds.Tables(0)

                .Columns(0).HeaderText = "Classification"

                .Columns(1).HeaderText = "Status"

                .Columns(2).HeaderText = "Loan No."

                .Columns(3).HeaderText = "Date"

                .Columns(4).HeaderText = "Terms"

                .Columns(5).HeaderText = "Loan type"

                .Columns(6).HeaderText = "Principal"

                .Columns(7).HeaderText = "Interest"
                .Columns(7).Visible = False
                .Columns(8).HeaderText = "Service Fee"
                .Columns(8).Visible = False
                .Columns(9).HeaderText = "Capital Share"       '----
                .Columns(9).Visible = False
                .Columns(10).HeaderText = "Amort"  '----
                .Columns(10).Visible = False
                .Columns(11).HeaderText = "Amort End"
                .Columns(11).Visible = False
                .Columns(12).HeaderText = "Interest Refund"   '----
                .Columns(12).Visible = False
                .Columns(13).HeaderText = "Net Proceeds"
                .Columns(13).Visible = False
                'visible = false
                .Columns(14).Visible = False
                .Columns(15).Visible = False
                .Columns(15).Visible = False
                .Columns(16).Visible = False
                .Columns(17).Visible = False
                .Columns(18).Visible = False
                .Columns(19).Visible = False
                .Columns(20).Visible = False
                .Columns(21).Visible = False
                .Columns(22).Visible = False
                .Columns(23).Visible = False
                .Columns(24).Visible = False
                .Columns(25).Visible = False
            End With
            If grdMemberList.SelectedRows.Count <> 0 Then
                GrdLoanSummary.Enabled = True
            End If

            _Gen_ViewButton()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub ApprovedLoan(ByVal loanNo As String, ByVal reason As String)
        Dim gcon As New Clsappconfiguration
        Try
            SqlHelper.ExecuteNonQuery(gcon.cnstring, CommandType.StoredProcedure,
                                                              "MSS_LoanApproval_Approved_v2",
                                                              New SqlParameter("@loanNo", loanNo),
                                                              New SqlParameter("@reason", reason),
                                                              New SqlParameter("@employeeNo_Approver", txtApprover.Tag))
            'MessageBox.Show("Successful", "Note:", MessageBoxButtons.OK, MessageBoxIcon.Hand)
            'frmMsgBox.txtMessage.Text = "Successful!"
            'frmMsgBox.Show()
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub

    Private Sub DisapproveLoan(ByVal loanNo As String, ByVal reason As String)
        Dim gcon As New Clsappconfiguration
        Try
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.StoredProcedure,
                                                              "MSS_LoanApproval_Disapproved",
                                                              New SqlParameter("@loanNo", loanNo),
                                                              New SqlParameter("@reason", reason),
                                                              New SqlParameter("@employeeNo_disapprover", txtApprover.Tag))
            MessageBox.Show("Successful", "Note:", MessageBoxButtons.OK, MessageBoxIcon.Hand)
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub

    Private Sub Search_Employee(ByVal employeeNo As String, ByVal fcStatusName As String)
        Dim gcon As New Clsappconfiguration
        Try
            'grdMemberList.ClearSelection()
            grdMemberList.Columns.Clear()
            Dim ds As DataSet = SqlHelper.ExecuteDataset(gcon.cnstring, CommandType.StoredProcedure,
                                                              "MSS_Admin_MemberListWithLoansToApprove",
                                                              New SqlParameter("@exceptThisMember", employeeNo),
                                                              New SqlParameter("@fcStatusName", fcStatusName))
            With grdMemberList
                '.Rows.Clear()
                ' .Columns.Clear()
                .DataSource = ds.Tables(0)
                .Columns(0).HeaderText = "ID No."
                .Columns(0).Width = "200"
                .Columns(1).HeaderText = "Name"
                .Columns(1).Width = "300"
                .Columns(2).Visible = False
                .Columns(3).Visible = False
                .Columns(4).Visible = False
                .Columns(5).Visible = False
                .Columns(6).Visible = False
                .Columns(7).Visible = False
            End With
            gcon.sqlconn.Close()
            grdMemberList.Enabled = True
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
        End Try
    End Sub

#End Region

    Private Sub GrdLoanSummary_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles GrdLoanSummary.CellClick
        'If e.ColumnIndex = 26 Then
        '    frmReport_LoanDetails.LoadREport(txtEmployeeNumber.Text, GrdLoanSummary.SelectedRows(0).Cells(2).Value)
        '    frmReport_LoanDetails.WindowState = FormWindowState.Maximized
        '    frmReport_LoanDetails.StartPosition = FormStartPosition.CenterScreen
        '    frmReport_LoanDetails.ShowDialog()
        'End If
    End Sub

    Private Sub GrdLoanSummary_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles GrdLoanSummary.Click
        Dim gcon As New Clsappconfiguration
        Try
            LoanNo = GrdLoanSummary.SelectedRows(0).Cells(2).Value.ToString
            'MessageBox.Show("Selected Loan No :" + LoanNo)
            btnApprove.Enabled = True
            btnDisapprove.Enabled = True
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString)
        End Try
    End Sub

    Public Sub RefreshLoanSummary()
        GrdLoanSummary.Columns.Clear()
    End Sub

#Region "Added by Vince 10/4/14"
    Private Sub _Gen_ViewButton()
        Dim btnView As New DataGridViewButtonColumn
        With btnView
            .Name = "btnView"
            .Text = "View Details"
            .HeaderText = ""
            .UseColumnTextForButtonValue = True
            .DefaultCellStyle.ForeColor = Color.Black
        End With
        With GrdLoanSummary
            .Columns.Add(btnView)
        End With
    End Sub
#End Region
End Class