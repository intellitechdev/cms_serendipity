﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmPriorityMaster
    '##########################
    'Module : Priority Table (MoveUp - MoveDown)
    'Created by: Vincent Nacar
    'Date: 7/4/2014
    '########################
    Dim mycon As New Clsappconfiguration

    Public loantypeCode As String
    Public fcLoanType As String
    Public isEmpty As Boolean

    Private Sub frmPriorityMaster_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblLoanType.Text = loantypeCode + " - " + fcLoanType

        LoadPriority()
    End Sub

    Private Sub InsertPriority(ByVal stat As String)
        Dim ctr As Integer = 0
        Dim desc As String = ""
        Dim no As Integer = 0

        While ctr <> 3

            Select Case ctr
                Case 0
                    desc = "Interest"
                    no = 1
                Case 1
                    desc = "Service Fee"
                    no = 2
                Case 2
                    desc = "Principal"
                    no = 3
            End Select

            SqlHelper.ExecuteNonQuery(mycon.cnstring, "_Masterfile_InsertUpdate_Priority",
                                       New SqlParameter("@Stat", stat),
                                       New SqlParameter("@LoanCode", loantypeCode),
                                       New SqlParameter("@Desc", desc),
                                       New SqlParameter("@no", no))

            ctr += 1
        End While
        RefreshPriority()
    End Sub

    Private Sub UpdatePriority(ByVal stat As String)
        Try
            Dim desc As String = dgvPriority.SelectedRows(0).Cells(0).Value.ToString
            Dim no As String = dgvPriority.SelectedRows(0).Cells(1).Value.ToString

            SqlHelper.ExecuteNonQuery(mycon.cnstring, "_Masterfile_InsertUpdate_Priority",
                                       New SqlParameter("@Stat", stat),
                                       New SqlParameter("@LoanCode", loantypeCode),
                                       New SqlParameter("@Desc", desc),
                                       New SqlParameter("@no", no))
            RefreshPriority()
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Sub LoadPriority()
        dgvPriority.Columns.Clear()

        Try
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(mycon.cnstring, "_Masterfile_LoadPriority",
                                           New SqlParameter("@LoanCode", Convert.ToInt32(loantypeCode)))
            If ds.Tables(0).Rows.Count = 0 Then
                LoadDefault()
            Else
                dgvPriority.DataSource = ds.Tables(0)
                dgvPriority.Columns(1).Visible = False
            End If
        Catch ex As Exception
            'Throw
        End Try
    End Sub

    Private Sub RefreshPriority()
        Try
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(mycon.cnstring, "_Masterfile_LoadPriority",
                                           New SqlParameter("@LoanCode", Convert.ToInt32(loantypeCode)))
            dgvPriority.DataSource = ds.Tables(0)
            dgvPriority.Columns(1).Visible = False
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Sub LoadDefault()
        InsertPriority("")
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        UpdatePriority("")
        frmMsgBox.txtMessage.Text = "Saving Success!"
        frmMsgBox.ShowDialog()
        Me.Close()
    End Sub

    Private Sub btnMoveUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMoveUp.Click
        UpdatePriority("-")
    End Sub

    Private Sub btnMoveDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMoveDown.Click
        UpdatePriority("+")
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

End Class