Imports System.Data.SqlClient
Imports System.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Windows.Forms

Module mod_UsersAccessibility
    Public username As String
    Private gCon As New Clsappconfiguration
    Public xUser As String
    Public Sub UserRightsModules()
        gCon.sqlconn.Close()
        frmMain.ExistingMember.Visible = isAllowed_MM_MEMBER()
        'frmMain.MembershipTerminationToolStripMenuItem.Visible = isAllowed_MM_WITHDRAWAL()
        frmMain.Newloans.Visible = isAllowed_LM_LOANAPP()
        frmMain.ExistingLoans.Visible = isAllowed_LM_LOANAPP()
        frmMain.LoanApproval.Visible = isAllowed_LM_LOANAPPROVAL()
        'frmMain.AccrualOfInterestToolStripMenuItem.Visible = isAllowed_LM_AE()
        'frmMain.LoanOverpaymentToolStripMenuItem.Visible = isAllowed_LM_LO()
        'frmMain.CashPaymentMasterToolStripMenuItem.Visible = isAllowed_PAYMENT()
        'frmMain.Deposits.Visible = isAllowed_DEPOSITS()
        frmMain.MemberContributionToolStripMenuItem.Visible = isAllowed_CONTRI_MEM()
        'frmMain.CapitalShareDepositAndWithdrawalToolStripMenuItem.Visible = isAllowed_CONTRI_CAP()

        'frmMain.DownloadingUploadingPayrollScheduToolStripMenuItem.Visible = isAllowed_PAYROLL_INTEG()
        frmMain.BalanceSheetToolStripMenuItem.Visible = isAllowed_R_A1_BAL()
        frmMain.StatementOfNetSurplusToolStripMenuItem.Visible = isAllowed_R_A1_STATE_SUR()
        frmMain.SummaryOfAgingReportsToolStripMenuItem.Visible = isAllowed_R_A1_SUMM()
        frmMain.StatementOfUtilizationOfStatutoryFundsToolStripMenuItem.Visible = isAllowed_R_A1_STATE_UTIL()
        frmMain.LoanReceivableAgingReportToolStripMenuItem.Visible = isAllowed_R_A2_LRAR()
        frmMain.ScheduleOfLoanReceivaToolStripMenuItem.Visible = isAllowed_R_A2_SLR()
        frmMain.PaidUpCapitalShareToolStripMenuItem.Visible = isAllowed_R_A2_PUCAS()
        frmMain.AccountsPayableToolStripMenuItem.Visible = isAllowed_R_A2_AP()
        frmMain.LoansPayableToolStripMenuItem.Visible = isAllowed_R_A2_LP()
        frmMain.GeneralLedgerToolStripMenuItem.Visible = isAllowed_R_A2_GLAR()
        frmMain.PropertyToolStripMenuItem.Visible = isAllowed_R_A2_PE()
        frmMain.CapitalShareToolStripMenuItem.Visible = isAllowed_R_IS_CS()
        frmMain.LoansToolStripMenuItem.Visible = isAllowed_R_IS_LSA()
        frmMain.SavingsAndToolStripMenuItem.Visible = isAllowed_R_IS_STD()
        frmMain.AverageCapitalShareAndDividendsToolStripMenuItem.Visible = isAllowed_R_IS_ACSD()
        frmMain.CAPRToolStripMenuItem.Visible = isAllowed_R_PR()
        'frmMain.ContributionBalanceMasterToolStripMenuItem.Visible = isAllowed_R_OR_CBS()
        'frmMain.PayrollSettlementReportToolStripMenuItem.Visible = isAllowed_R_OR_PSS()

        frmMain.UserToolStripMenuItem.Visible = isAllowed_S_U_CREATE()
        frmMain.ChangePasswordToolStripMenuItem.Visible = isAllowed_S_U_CHANGE()
        frmMain.RightsToolStripMenuItem.Visible = isAllowed_S_U_RIGHTS()
        frmMain.CompanyInformationToolStripMenuItem.Visible = isAllowed_S_COMPANY()
        'frmMain.AccountsConfigurationToolStripMenuItem.Visible = isAllowed_S_U_ACC()

        'frmMain.BackupToolStripMenuItem.Visible = isAllowed_DATABASE()
        'frmMain.LoanMasterUploaderToolStripMenuItem.Visible = isAllowed_
        'frmMain.LoanPaymentUploaderToolStripMenuItem = isAllowed_
        'frmMain.MemberContributionUploaderToolStripMenuItem = isAllowed_
        ' frmMain.DataMigrationToolStripMenuItem.Visible = isAllowed_DATABASE()

        'Bank information
        frmMember_Master.btnNewBA.Visible = isAllowed_BANK_INFO()
        frmMember_Master.btnUpdateBA.Visible = isAllowed_BANK_INFO()
        frmMember_Master.btnDeleteBA.Visible = isAllowed_BANK_INFO()
    End Sub

    Public Property getusername() As String
        Get
            Return username
        End Get
        Set(ByVal value As String)
            username = value
        End Set
    End Property

    Public Function isAllowed_M_POSITION() As Boolean 'POSITION   
        Dim gCon As New Clsappconfiguration
        Try

            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.sqlconn, CommandType.StoredProcedure, "CIMS_USERRIGHTS_SELECTBYUSERandID", _
                                         New SqlParameter("@UserID", username), _
                                         New SqlParameter("@Fk_ModuleID", "M_POSITION"))
            While rd.Read
                isAllowed_M_POSITION = rd("IsAllowed")
            End While
            rd.Close()
            Return isAllowed_M_POSITION
        Catch ex As Exception

        Finally
            gCon.sqlconn.Close()
        End Try
    End Function

    Public Function isAllowed_M_PAYROLLC() As Boolean 'PAYROLL CODE
        Dim gCon As New Clsappconfiguration
        Try

            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.sqlconn, CommandType.StoredProcedure, "CIMS_USERRIGHTS_SELECTBYUSERandID", _
                                         New SqlParameter("@UserID", username), _
                                         New SqlParameter("@Fk_ModuleID", "M_PAYROLLC"))
            While rd.Read
                isAllowed_M_PAYROLLC = rd("IsAllowed")
            End While
            rd.Close()
            Return isAllowed_M_PAYROLLC

        Catch ex As Exception

        Finally
            gCon.sqlconn.Close()
        End Try
    End Function
    Public Function isAllowed_M_PAYC() As Boolean 'PAY CODE
        Dim gCon As New Clsappconfiguration
        Try

            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.sqlconn, CommandType.StoredProcedure, "CIMS_USERRIGHTS_SELECTBYUSERandID", _
                                         New SqlParameter("@UserID", username), _
                                         New SqlParameter("@Fk_ModuleID", "M_PAYC"))
            While rd.Read
                isAllowed_M_PAYC = rd("IsAllowed")
            End While
            rd.Close()
            Return isAllowed_M_PAYC
        Catch ex As Exception

        Finally
            gCon.sqlconn.Close()
        End Try
    End Function
    Public Function isAllowed_M_PCALENDAR() As Boolean 'PAY CALENDAR
        Dim gCon As New Clsappconfiguration
        Try

            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.sqlconn, CommandType.StoredProcedure, "CIMS_USERRIGHTS_SELECTBYUSERandID", _
                                         New SqlParameter("@UserID", username), _
                                         New SqlParameter("@Fk_ModuleID", "M_PCALENDAR"))
            While rd.Read
                isAllowed_M_PCALENDAR = rd("IsAllowed")
            End While
            rd.Close()
            Return isAllowed_M_PCALENDAR
        Catch ex As Exception

        Finally
            gCon.sqlconn.Close()
        End Try
    End Function
    Public Function isAllowed_M_PINTEGRATOR() As Boolean 'PAYROLL INTEGRATION
        Dim gCon As New Clsappconfiguration
        Try

            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.sqlconn, CommandType.StoredProcedure, "CIMS_USERRIGHTS_SELECTBYUSERandID", _
                                         New SqlParameter("@UserID", username), _
                                         New SqlParameter("@Fk_ModuleID", "M_PINTEGRATOR"))
            While rd.Read
                isAllowed_M_PINTEGRATOR = rd("IsAllowed")
            End While
            rd.Close()
            Return isAllowed_M_PINTEGRATOR
        Catch ex As Exception

        Finally
            gCon.sqlconn.Close()
        End Try
    End Function
    Public Function isAllowed_M_LOANTYPE() As Boolean 'LOAN TYPE
        Dim gCon As New Clsappconfiguration
        Try

            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.sqlconn, CommandType.StoredProcedure, "CIMS_USERRIGHTS_SELECTBYUSERandID", _
                                         New SqlParameter("@UserID", username), _
                                         New SqlParameter("@Fk_ModuleID", "M_LOANTYPE"))
            While rd.Read
                isAllowed_M_LOANTYPE = rd("IsAllowed")
            End While
            rd.Close()
            Return isAllowed_M_LOANTYPE

        Catch ex As Exception

        Finally
            gCon.sqlconn.Close()
        End Try
    End Function
    Public Function isAllowed_M_POLICY() As Boolean 'POLICY
        Dim gCon As New Clsappconfiguration
        Try

            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.sqlconn, CommandType.StoredProcedure, "CIMS_USERRIGHTS_SELECTBYUSERandID", _
                                         New SqlParameter("@UserID", username), _
                                         New SqlParameter("@Fk_ModuleID", "M_POLICY"))
            While rd.Read
                isAllowed_M_POLICY = rd("IsAllowed")
            End While
            rd.Close()
            Return isAllowed_M_POLICY

        Catch ex As Exception

        Finally
            gCon.sqlconn.Close()
        End Try
    End Function
    Public Function isAllowed_M_PAGIBIG() As Boolean 'PAG-IBIG
        Dim gCon As New Clsappconfiguration
        Try

            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.sqlconn, CommandType.StoredProcedure, "CIMS_USERRIGHTS_SELECTBYUSERandID", _
                                         New SqlParameter("@UserID", username), _
                                         New SqlParameter("@Fk_ModuleID", "M_PAG-IBIG"))
            While rd.Read
                isAllowed_M_PAGIBIG = rd("IsAllowed")
            End While
            rd.Close()
            Return isAllowed_M_PAGIBIG
        Catch ex As Exception

        Finally
            gCon.sqlconn.Close()
        End Try
    End Function
    Public Function isAllowed_M_PHIL() As Boolean 'PHILHEALTH
        Dim gCon As New Clsappconfiguration
        Try

            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.sqlconn, CommandType.StoredProcedure, "CIMS_USERRIGHTS_SELECTBYUSERandID", _
                                         New SqlParameter("@UserID", username), _
                                         New SqlParameter("@Fk_ModuleID", "M_PHIL"))
            While rd.Read
                isAllowed_M_PHIL = rd("IsAllowed")
            End While
            rd.Close()
            Return isAllowed_M_PHIL
        Catch ex As Exception

        Finally
            gCon.sqlconn.Close()
        End Try
    End Function
    Public Function isAllowed_M_SSS() As Boolean 'SSS
        Dim gCon As New Clsappconfiguration
        Try

            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.sqlconn, CommandType.StoredProcedure, "CIMS_USERRIGHTS_SELECTBYUSERandID", _
                                         New SqlParameter("@UserID", username), _
                                         New SqlParameter("@Fk_ModuleID", "M_SSS"))
            While rd.Read
                isAllowed_M_SSS = rd("IsAllowed")
            End While
            rd.Close()
            Return isAllowed_M_SSS
        Catch ex As Exception

        Finally
            gCon.sqlconn.Close()
        End Try
    End Function
    Public Function isAllowed_M_HOLDINGTAX() As Boolean 'WITHOLDING TAX
        Dim gCon As New Clsappconfiguration
        Try

            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.sqlconn, CommandType.StoredProcedure, "CIMS_USERRIGHTS_SELECTBYUSERandID", _
                                         New SqlParameter("@UserID", username), _
                                         New SqlParameter("@Fk_ModuleID", "M_HOLDINGTAX"))
            While rd.Read
                isAllowed_M_HOLDINGTAX = rd("IsAllowed")
            End While
            rd.Close()
            Return isAllowed_M_HOLDINGTAX
        Catch ex As Exception

        Finally
            gCon.sqlconn.Close()
        End Try
    End Function
    Public Function isAllowed_M_APPROVER() As Boolean 'APPROVER
        Dim gCon As New Clsappconfiguration
        Try

            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.sqlconn, CommandType.StoredProcedure, "CIMS_USERRIGHTS_SELECTBYUSERandID", _
                                         New SqlParameter("@UserID", username), _
                                         New SqlParameter("@Fk_ModuleID", "M_APPROVER"))
            While rd.Read
                isAllowed_M_APPROVER = rd("IsAllowed")
            End While
            rd.Close()
            Return isAllowed_M_APPROVER
        Catch ex As Exception

        Finally
            gCon.sqlconn.Close()
        End Try
    End Function
    Public Function isAllowed_M_APLLICANT() As Boolean 'APLLICANT REQT
        Dim gCon As New Clsappconfiguration
        Try

            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.sqlconn, CommandType.StoredProcedure, "CIMS_USERRIGHTS_SELECTBYUSERandID", _
                                         New SqlParameter("@UserID", username), _
                                         New SqlParameter("@Fk_ModuleID", "M_APLLICANT"))
            While rd.Read
                isAllowed_M_APLLICANT = rd("IsAllowed")
            End While
            rd.Close()
            Return isAllowed_M_APLLICANT
        Catch ex As Exception

        Finally
            gCon.sqlconn.Close()
        End Try
    End Function
    Public Function isAllowed_MM_MEMBER() As Boolean 'MM_MEMBER
        Dim gCon As New Clsappconfiguration
        Try

            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.sqlconn, CommandType.StoredProcedure, "CIMS_USERRIGHTS_SELECTBYUSERandID", _
                                         New SqlParameter("@UserID", username), _
                                         New SqlParameter("@Fk_ModuleID", "MM_MEMBER"))
            While rd.Read
                isAllowed_MM_MEMBER = rd("IsAllowed")
            End While
            rd.Close()
            Return isAllowed_MM_MEMBER
        Catch ex As Exception

        Finally
            gCon.sqlconn.Close()
        End Try
    End Function
    Public Function isAllowed_MM_WITHDRAWAL() As Boolean 'WITHDRAWAL
        Dim gCon As New Clsappconfiguration
        Try

            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.sqlconn, CommandType.StoredProcedure, "CIMS_USERRIGHTS_SELECTBYUSERandID", _
                                         New SqlParameter("@UserID", username), _
                                         New SqlParameter("@Fk_ModuleID", "MM_WITHDRAWAL"))
            While rd.Read
                isAllowed_MM_WITHDRAWAL = rd("IsAllowed")
            End While
            rd.Close()
            Return isAllowed_MM_WITHDRAWAL
        Catch ex As Exception

        Finally
            gCon.sqlconn.Close()
        End Try
    End Function
    Public Function isAllowed_MM_NEWMEM() As Boolean 'NEW MEMBERS
        Dim gCon As New Clsappconfiguration
        Try

            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.sqlconn, CommandType.StoredProcedure, "CIMS_USERRIGHTS_SELECTBYUSERandID", _
                                         New SqlParameter("@UserID", username), _
                                         New SqlParameter("@Fk_ModuleID", "MM_NEWMEM"))
            While rd.Read
                isAllowed_MM_NEWMEM = rd("IsAllowed")
            End While
            rd.Close()
            Return isAllowed_MM_NEWMEM
        Catch ex As Exception

        Finally
            gCon.sqlconn.Close()
        End Try
    End Function
    Public Function isAllowed_MM_APPROVAL() As Boolean 'APPROVAL
        Dim gCon As New Clsappconfiguration
        Try

            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.sqlconn, CommandType.StoredProcedure, "CIMS_USERRIGHTS_SELECTBYUSERandID", _
                                         New SqlParameter("@UserID", username), _
                                         New SqlParameter("@Fk_ModuleID", "MM_APPROVAL"))
            While rd.Read
                isAllowed_MM_APPROVAL = rd("IsAllowed")
            End While
            rd.Close()
            Return isAllowed_MM_APPROVAL
        Catch ex As Exception

        Finally
            gCon.sqlconn.Close()
        End Try
    End Function
    Public Function isAllowed_LM_LOANAPP() As Boolean 'LOAN APPLICATION
        Dim gCon As New Clsappconfiguration
        Try

            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.sqlconn, CommandType.StoredProcedure, "CIMS_USERRIGHTS_SELECTBYUSERandID", _
                                         New SqlParameter("@UserID", username), _
                                         New SqlParameter("@Fk_ModuleID", "LM_LOANAPP"))
            While rd.Read
                isAllowed_LM_LOANAPP = rd("IsAllowed")
            End While
            rd.Close()
            Return isAllowed_LM_LOANAPP
        Catch ex As Exception

        Finally
            gCon.sqlconn.Close()
        End Try
    End Function
    Public Function isAllowed_LM_LOANPRO() As Boolean 'LOAN PROCESSING
        Dim gCon As New Clsappconfiguration
        Try

            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.sqlconn, CommandType.StoredProcedure, "CIMS_USERRIGHTS_SELECTBYUSERandID", _
                                         New SqlParameter("@UserID", username), _
                                         New SqlParameter("@Fk_ModuleID", "LM_LOANPRO"))
            While rd.Read
                isAllowed_LM_LOANPRO = rd("IsAllowed")
            End While
            rd.Close()
            Return isAllowed_LM_LOANPRO
        Catch ex As Exception

        Finally
            gCon.sqlconn.Close()
        End Try
    End Function
    Public Function isAllowed_LM_LOANAPPROVAL() As Boolean 'LOAN APPROVAL
        Dim gCon As New Clsappconfiguration
        Try

            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.sqlconn, CommandType.StoredProcedure, "CIMS_USERRIGHTS_SELECTBYUSERandID", _
                                         New SqlParameter("@UserID", username), _
                                         New SqlParameter("@Fk_ModuleID", "LM_LOANAPPROVAL"))
            While rd.Read
                isAllowed_LM_LOANAPPROVAL = rd("IsAllowed")
            End While
            rd.Close()
            Return isAllowed_LM_LOANAPPROVAL
        Catch ex As Exception

        Finally
            gCon.sqlconn.Close()
        End Try
    End Function
    Public Function isAllowed_PAYMENT() As Boolean 'RECIEVE PAYMENT
        Dim gCon As New Clsappconfiguration
        Try

            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.sqlconn, CommandType.StoredProcedure, "CIMS_USERRIGHTS_SELECTBYUSERandID", _
                                         New SqlParameter("@UserID", username), _
                                         New SqlParameter("@Fk_ModuleID", "PAYMENT"))
            While rd.Read
                isAllowed_PAYMENT = rd("IsAllowed")
            End While
            rd.Close()
            Return isAllowed_PAYMENT
        Catch ex As Exception

        Finally
            gCon.sqlconn.Close()
        End Try
    End Function
    Public Function isAllowed_DEPOSITS() As Boolean 'TIME AND SAVINGS
        Dim gCon As New Clsappconfiguration
        Try

            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.sqlconn, CommandType.StoredProcedure, "CIMS_USERRIGHTS_SELECTBYUSERandID", _
                                         New SqlParameter("@UserID", username), _
                                         New SqlParameter("@Fk_ModuleID", "DEPOSITS"))
            While rd.Read
                isAllowed_DEPOSITS = rd("IsAllowed")
            End While
            rd.Close()
            Return isAllowed_DEPOSITS
        Catch ex As Exception

        Finally
            gCon.sqlconn.Close()
        End Try
    End Function
    Public Function isAllowed_CONTRI_MEM() As Boolean 'MEMBER CONTRIBUTIONS
        Dim gCon As New Clsappconfiguration
        Try

            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.sqlconn, CommandType.StoredProcedure, "CIMS_USERRIGHTS_SELECTBYUSERandID", _
                                         New SqlParameter("@UserID", username), _
                                         New SqlParameter("@Fk_ModuleID", "CONTRI_MEM"))
            While rd.Read
                isAllowed_CONTRI_MEM = rd("IsAllowed")
            End While
            rd.Close()
            Return isAllowed_CONTRI_MEM
        Catch ex As Exception

        Finally
            gCon.sqlconn.Close()
        End Try
    End Function
    Public Function isAllowed_CONTRI_CAP() As Boolean 'CAPITAL SHARE DEPOSIT AND WITHDRAWAL
        Dim gCon As New Clsappconfiguration
        Try

            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.sqlconn, CommandType.StoredProcedure, "CIMS_USERRIGHTS_SELECTBYUSERandID", _
                                         New SqlParameter("@UserID", username), _
                                         New SqlParameter("@Fk_ModuleID", "CONTRI_CAP"))
            While rd.Read
                isAllowed_CONTRI_CAP = rd("IsAllowed")
            End While
            rd.Close()
            Return isAllowed_CONTRI_CAP
        Catch ex As Exception

        Finally
            gCon.sqlconn.Close()
        End Try
    End Function
    Public Function isAllowed_PAYROLL_INTEG() As Boolean 'PAYROLL INTEGRATOR
        Dim gCon As New Clsappconfiguration
        Try

            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.sqlconn, CommandType.StoredProcedure, "CIMS_USERRIGHTS_SELECTBYUSERandID", _
                                         New SqlParameter("@UserID", username), _
                                         New SqlParameter("@Fk_ModuleID", "PAYROLL_INTEG"))
            While rd.Read
                isAllowed_PAYROLL_INTEG = rd("IsAllowed")
            End While
            rd.Close()
            Return isAllowed_PAYROLL_INTEG
        Catch ex As Exception

        Finally
            gCon.sqlconn.Close()
        End Try
    End Function
    Public Function isAllowed_ORG_LEVEL() As Boolean 'LEVEL DEFINITION
        Dim gCon As New Clsappconfiguration
        Try

            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.sqlconn, CommandType.StoredProcedure, "CIMS_USERRIGHTS_SELECTBYUSERandID", _
                                         New SqlParameter("@UserID", username), _
                                         New SqlParameter("@Fk_ModuleID", "ORG_LEVEL"))
            While rd.Read
                isAllowed_ORG_LEVEL = rd("IsAllowed")
            End While
            rd.Close()
            Return isAllowed_ORG_LEVEL
        Catch ex As Exception

        Finally
            gCon.sqlconn.Close()
        End Try
    End Function
    Public Function isAllowed_ORG_ORG() As Boolean 'ORGANIZATION
        Dim gCon As New Clsappconfiguration
        Try

            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.sqlconn, CommandType.StoredProcedure, "CIMS_USERRIGHTS_SELECTBYUSERandID", _
                                         New SqlParameter("@UserID", username), _
                                         New SqlParameter("@Fk_ModuleID", "ORG_ORG"))
            While rd.Read
                isAllowed_ORG_ORG = rd("IsAllowed")
            End While
            rd.Close()
            Return isAllowed_ORG_ORG
        Catch ex As Exception

        Finally
            gCon.sqlconn.Close()
        End Try
    End Function
    Public Function isAllowed_M_MM_MEMBER_TER() As Boolean 'MEMBERSHIP TERMINATION
        Dim gCon As New Clsappconfiguration
        Try

            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.sqlconn, CommandType.StoredProcedure, "CIMS_USERRIGHTS_SELECTBYUSERandID", _
                                         New SqlParameter("@UserID", username), _
                                         New SqlParameter("@Fk_ModuleID", "M_MM_MEMBER_TER"))
            While rd.Read
                isAllowed_M_MM_MEMBER_TER = rd("IsAllowed")
            End While
            rd.Close()
            Return isAllowed_M_MM_MEMBER_TER
        Catch ex As Exception '

        Finally
            gCon.sqlconn.Close()
        End Try
    End Function
    Public Function isAllowed_R_A1_BAL() As Boolean 'BALANCE SHEET
        Dim gCon As New Clsappconfiguration
        Try

            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.sqlconn, CommandType.StoredProcedure, "CIMS_USERRIGHTS_SELECTBYUSERandID", _
                                         New SqlParameter("@UserID", username), _
                                         New SqlParameter("@Fk_ModuleID", "R_A1_BAL"))
            While rd.Read
                isAllowed_R_A1_BAL = rd("IsAllowed")
            End While
            rd.Close()
            Return isAllowed_R_A1_BAL
        Catch ex As Exception

        Finally
            gCon.sqlconn.Close()
        End Try
    End Function
    Public Function isAllowed_R_A1_STATE_SUR() As Boolean 'STATEMENT OF NET SURPLUS
        Dim gCon As New Clsappconfiguration
        Try

            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.sqlconn, CommandType.StoredProcedure, "CIMS_USERRIGHTS_SELECTBYUSERandID", _
                                         New SqlParameter("@UserID", username), _
                                         New SqlParameter("@Fk_ModuleID", "R_A1_STATE_SUR"))
            While rd.Read
                isAllowed_R_A1_STATE_SUR = rd("IsAllowed")
            End While
            rd.Close()
            Return isAllowed_R_A1_STATE_SUR
        Catch ex As Exception

        Finally
            gCon.sqlconn.Close()
        End Try
    End Function
    Public Function isAllowed_R_A1_SUMM() As Boolean 'SUMMARY OF AGING REPORTS
        Dim gCon As New Clsappconfiguration
        Try

            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.sqlconn, CommandType.StoredProcedure, "CIMS_USERRIGHTS_SELECTBYUSERandID", _
                                         New SqlParameter("@UserID", username), _
                                         New SqlParameter("@Fk_ModuleID", "R_A1_SUMM"))
            While rd.Read
                isAllowed_R_A1_SUMM = rd("IsAllowed")
            End While
            rd.Close()
            Return isAllowed_R_A1_SUMM
        Catch ex As Exception

        Finally
            gCon.sqlconn.Close()
        End Try
    End Function

    Public Function isAllowed_R_A1_STATE_UTIL() As Boolean 'STATEMENT OF UTILIZATION STATUTORY FUNDS
        Dim gCon As New Clsappconfiguration
        Try

            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.sqlconn, CommandType.StoredProcedure, "CIMS_USERRIGHTS_SELECTBYUSERandID", _
                                         New SqlParameter("@UserID", username), _
                                         New SqlParameter("@Fk_ModuleID", "R_A1_STATE_UTIL"))
            While rd.Read
                isAllowed_R_A1_STATE_UTIL = rd("IsAllowed")
            End While
            rd.Close()
            Return isAllowed_R_A1_STATE_UTIL
        Catch ex As Exception

        Finally
            gCon.sqlconn.Close()
        End Try
    End Function

    Public Function isAllowed_R_A2_LRAR() As Boolean 'LOAN RECIEVABLE AGING REPORT
        Dim gCon As New Clsappconfiguration
        Try

            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.sqlconn, CommandType.StoredProcedure, "CIMS_USERRIGHTS_SELECTBYUSERandID", _
                                         New SqlParameter("@UserID", username), _
                                         New SqlParameter("@Fk_ModuleID", "R_A2_LRAR"))
            While rd.Read
                isAllowed_R_A2_LRAR = rd("IsAllowed")
            End While
            rd.Close()
            Return isAllowed_R_A2_LRAR
        Catch ex As Exception

        Finally
            gCon.sqlconn.Close()
        End Try
    End Function

    Public Function isAllowed_R_A2_SLR() As Boolean 'SCHEDULE OF LOAN RECIEVABLE
        Dim gCon As New Clsappconfiguration
        Try

            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.sqlconn, CommandType.StoredProcedure, "CIMS_USERRIGHTS_SELECTBYUSERandID", _
                                         New SqlParameter("@UserID", username), _
                                         New SqlParameter("@Fk_ModuleID", "R_A2_SLR"))
            While rd.Read
                isAllowed_R_A2_SLR = rd("IsAllowed")
            End While
            rd.Close()
            Return isAllowed_R_A2_SLR
        Catch ex As Exception

        Finally
            gCon.sqlconn.Close()
        End Try
    End Function
    Public Function isAllowed_R_A2_PUCAS() As Boolean 'PAID UP CAPITAL SHARE
        Dim gCon As New Clsappconfiguration
        Try

            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.sqlconn, CommandType.StoredProcedure, "CIMS_USERRIGHTS_SELECTBYUSERandID", _
                                         New SqlParameter("@UserID", username), _
                                         New SqlParameter("@Fk_ModuleID", "R_A2_PUCAS"))
            While rd.Read
                isAllowed_R_A2_PUCAS = rd("IsAllowed")
            End While
            rd.Close()
            Return isAllowed_R_A2_PUCAS
        Catch ex As Exception

        Finally
            gCon.sqlconn.Close()
        End Try
    End Function
    Public Function isAllowed_R_A2_AP() As Boolean 'ACCOUNTS PAYABLE
        Dim gCon As New Clsappconfiguration
        Try

            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.sqlconn, CommandType.StoredProcedure, "CIMS_USERRIGHTS_SELECTBYUSERandID", _
                                         New SqlParameter("@UserID", username), _
                                         New SqlParameter("@Fk_ModuleID", "R_A2_AP"))
            While rd.Read
                isAllowed_R_A2_AP = rd("IsAllowed")
            End While
            rd.Close()
            Return isAllowed_R_A2_AP
        Catch ex As Exception

        Finally
            gCon.sqlconn.Close()
        End Try
    End Function

    Public Function isAllowed_R_A2_LP() As Boolean 'LOANS PAYABLE
        Dim gCon As New Clsappconfiguration
        Try

            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.sqlconn, CommandType.StoredProcedure, "CIMS_USERRIGHTS_SELECTBYUSERandID", _
                                         New SqlParameter("@UserID", username), _
                                         New SqlParameter("@Fk_ModuleID", "R_A2_LP"))
            While rd.Read
                isAllowed_R_A2_LP = rd("IsAllowed")
            End While
            rd.Close()
            Return isAllowed_R_A2_LP
        Catch ex As Exception

        Finally
            gCon.sqlconn.Close()
        End Try
    End Function
    Public Function isAllowed_R_A2_GLAR() As Boolean 'GENERAL LEDGER ACTIVITY REPORT
        Dim gCon As New Clsappconfiguration
        Try

            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.sqlconn, CommandType.StoredProcedure, "CIMS_USERRIGHTS_SELECTBYUSERandID", _
                                         New SqlParameter("@UserID", username), _
                                         New SqlParameter("@Fk_ModuleID", "R_A2_GLAR"))
            While rd.Read
                isAllowed_R_A2_GLAR = rd("IsAllowed")
            End While
            rd.Close()
            Return isAllowed_R_A2_GLAR
        Catch ex As Exception

        Finally
            gCon.sqlconn.Close()
        End Try
    End Function
    Public Function isAllowed_R_A2_PE() As Boolean 'PROPERTY AND EQUIPMENT
        Dim gCon As New Clsappconfiguration
        Try

            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.sqlconn, CommandType.StoredProcedure, "CIMS_USERRIGHTS_SELECTBYUSERandID", _
                                         New SqlParameter("@UserID", username), _
                                         New SqlParameter("@Fk_ModuleID", "R_A2_PE"))
            While rd.Read
                isAllowed_R_A2_PE = rd("IsAllowed")
            End While
            rd.Close()
            Return isAllowed_R_A2_PE
        Catch ex As Exception

        Finally
            gCon.sqlconn.Close()
        End Try
    End Function
    Public Function isAllowed_R_IS_CS() As Boolean 'CAPITAL SHARE
        Dim gCon As New Clsappconfiguration
        Try

            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.sqlconn, CommandType.StoredProcedure, "CIMS_USERRIGHTS_SELECTBYUSERandID", _
                                         New SqlParameter("@UserID", username), _
                                         New SqlParameter("@Fk_ModuleID", "R_IS_CS"))
            While rd.Read
                isAllowed_R_IS_CS = rd("IsAllowed")
            End While
            rd.Close()
            Return isAllowed_R_IS_CS
        Catch ex As Exception

        Finally
            gCon.sqlconn.Close()
        End Try
    End Function
    Public Function isAllowed_R_IS_LSA() As Boolean 'LOANS STATEMENT OF ACCOUNTS
        Dim gCon As New Clsappconfiguration
        Try

            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.sqlconn, CommandType.StoredProcedure, "CIMS_USERRIGHTS_SELECTBYUSERandID", _
                                         New SqlParameter("@UserID", username), _
                                         New SqlParameter("@Fk_ModuleID", "R_IS_LSA"))
            While rd.Read
                isAllowed_R_IS_LSA = rd("IsAllowed")
            End While
            rd.Close()
            Return isAllowed_R_IS_LSA
        Catch ex As Exception

        Finally
            gCon.sqlconn.Close()
        End Try
    End Function
    Public Function isAllowed_R_IS_STD() As Boolean 'SAVINGS AND TIME DEPOSIT
        Dim gCon As New Clsappconfiguration
        Try

            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.sqlconn, CommandType.StoredProcedure, "CIMS_USERRIGHTS_SELECTBYUSERandID", _
                                         New SqlParameter("@UserID", username), _
                                         New SqlParameter("@Fk_ModuleID", "R_IS_STD"))
            While rd.Read
                isAllowed_R_IS_STD = rd("IsAllowed")
            End While
            rd.Close()
            Return isAllowed_R_IS_STD
        Catch ex As Exception

        Finally
            gCon.sqlconn.Close()
        End Try
    End Function
    Public Function isAllowed_R_IS_ACSD() As Boolean 'AVERAGE CAPITAL SHARE AND DIVIDENDS
        Dim gCon As New Clsappconfiguration
        Try

            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.sqlconn, CommandType.StoredProcedure, "CIMS_USERRIGHTS_SELECTBYUSERandID", _
                                         New SqlParameter("@UserID", username), _
                                         New SqlParameter("@Fk_ModuleID", "R_IS_ACSD"))
            While rd.Read
                isAllowed_R_IS_ACSD = rd("IsAllowed")
            End While
            rd.Close()
            Return isAllowed_R_IS_ACSD
        Catch ex As Exception

        Finally
            gCon.sqlconn.Close()
        End Try
    End Function
    Public Function isAllowed_R_PR() As Boolean 'COOPERATIVE ANNUAL PERFORMANCE REPORT
        Dim gCon As New Clsappconfiguration
        Try

            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.sqlconn, CommandType.StoredProcedure, "CIMS_USERRIGHTS_SELECTBYUSERandID", _
                                         New SqlParameter("@UserID", username), _
                                         New SqlParameter("@Fk_ModuleID", "R_PR"))
            While rd.Read
                isAllowed_R_PR = rd("IsAllowed")
            End While
            rd.Close()
            Return isAllowed_R_PR
        Catch ex As Exception

        Finally
            gCon.sqlconn.Close()
        End Try
    End Function
    Public Function isAllowed_R_OR_CBS() As Boolean 'CONTRIBUTION BALANCE SUMMARY
        Dim gCon As New Clsappconfiguration
        Try

            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.sqlconn, CommandType.StoredProcedure, "CIMS_USERRIGHTS_SELECTBYUSERandID", _
                                         New SqlParameter("@UserID", username), _
                                         New SqlParameter("@Fk_ModuleID", "R_OR_CBS"))
            While rd.Read
                isAllowed_R_OR_CBS = rd("IsAllowed")
            End While
            rd.Close()
            Return isAllowed_R_OR_CBS
        Catch ex As Exception

        Finally
            gCon.sqlconn.Close()
        End Try
    End Function
    Public Function isAllowed_R_OR_PSS() As Boolean 'PAYROLL SETTLEMENT SUMMARY
        Dim gCon As New Clsappconfiguration
        Try

            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.sqlconn, CommandType.StoredProcedure, "CIMS_USERRIGHTS_SELECTBYUSERandID", _
                                         New SqlParameter("@UserID", username), _
                                         New SqlParameter("@Fk_ModuleID", "R_OR_PSS"))
            While rd.Read
                isAllowed_R_OR_PSS = rd("IsAllowed")
            End While
            rd.Close()
            Return isAllowed_R_OR_PSS
        Catch ex As Exception

        Finally
            gCon.sqlconn.Close()
        End Try
    End Function
    Public Function isAllowed_S_U_CREATE() As Boolean 'CREATE USER
        Dim gCon As New Clsappconfiguration
        Try

            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.sqlconn, CommandType.StoredProcedure, "CIMS_USERRIGHTS_SELECTBYUSERandID", _
                                         New SqlParameter("@UserID", username), _
                                         New SqlParameter("@Fk_ModuleID", "S_U_CREATE"))
            While rd.Read
                isAllowed_S_U_CREATE = rd("IsAllowed")
            End While
            rd.Close()
            Return isAllowed_S_U_CREATE
        Catch ex As Exception

        Finally
            gCon.sqlconn.Close()
        End Try
    End Function
    Public Function isAllowed_S_U_CHANGE() As Boolean 'CHANGE PASSWORD
        Dim gCon As New Clsappconfiguration
        Try
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.sqlconn, CommandType.StoredProcedure, "CIMS_USERRIGHTS_SELECTBYUSERandID", _
                                         New SqlParameter("@UserID", username), _
                                         New SqlParameter("@Fk_ModuleID", "S_U_CHANGE"))
            While rd.Read
                isAllowed_S_U_CHANGE = Convert.ToBoolean(rd("IsAllowed"))
            End While
            rd.Close()
            Return isAllowed_S_U_CHANGE

        Catch ex As Exception

        Finally
            gCon.sqlconn.Close()
        End Try
    End Function
    Public Function isAllowed_S_COMPANY() As Boolean 'COMPANY INFORMATION
        Dim gCon As New Clsappconfiguration
        Try

            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.sqlconn, CommandType.StoredProcedure, "CIMS_USERRIGHTS_SELECTBYUSERandID", _
                                         New SqlParameter("@UserID", username), _
                                         New SqlParameter("@Fk_ModuleID", "S_COMPANY"))
            While rd.Read
                isAllowed_S_COMPANY = Convert.ToBoolean(rd("IsAllowed"))
            End While

            Return isAllowed_S_COMPANY
            rd.Close()
        Catch ex As Exception

        Finally
            gCon.sqlconn.Close()
        End Try
    End Function
    Public Function isAllowed_S_U_ACC() As Boolean 'ACCOUNT SETTINGS
        Dim gCon As New Clsappconfiguration
        Try

            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.sqlconn, CommandType.StoredProcedure, "CIMS_USERRIGHTS_SELECTBYUSERandID", _
                                         New SqlParameter("@UserID", username), _
                                         New SqlParameter("@Fk_ModuleID", "S_U_ACC"))
            While rd.Read
                isAllowed_S_U_ACC = rd("IsAllowed")
            End While
            rd.Close()
            Return isAllowed_S_U_ACC
        Catch ex As Exception

        Finally
            gCon.sqlconn.Close()
        End Try
    End Function
    Public Function isAllowed_DATABASE() As Boolean 'DATABASE
        Dim gCon As New Clsappconfiguration
        Try

            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.sqlconn, CommandType.StoredProcedure, "CIMS_USERRIGHTS_SELECTBYUSERandID", _
                                         New SqlParameter("@UserID", username), _
                                         New SqlParameter("@Fk_ModuleID", "DATABASE"))
            While rd.Read
                isAllowed_DATABASE = rd("IsAllowed")
            End While
            rd.Close()
            Return isAllowed_DATABASE
        Catch ex As Exception

        Finally
            gCon.sqlconn.Close()
        End Try
    End Function

    Public Function isAllowed_LM_ACC() As Boolean 'ACCRUAL OF INTEREST
        Dim gCon As New Clsappconfiguration
        Try

            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.sqlconn, CommandType.StoredProcedure, "CIMS_USERRIGHTS_SELECTBYUSERandID", _
                                         New SqlParameter("@UserID", username), _
                                         New SqlParameter("@Fk_ModuleID", "LM_ACC"))
            While rd.Read
                isAllowed_LM_ACC = rd("IsAllowed")
            End While
            rd.Close()
            Return isAllowed_LM_ACC
        Catch ex As Exception

        Finally
            gCon.sqlconn.Close()
        End Try
    End Function
    Public Function isAllowed_LM_LO() As Boolean 'LOAN OVERPAYMENT   
        Dim gCon As New Clsappconfiguration
        Try

            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.sqlconn, CommandType.StoredProcedure, "CIMS_USERRIGHTS_SELECTBYUSERandID", _
                                         New SqlParameter("@UserID", username), _
                                         New SqlParameter("@Fk_ModuleID", "LM_LO"))
            While rd.Read
                isAllowed_LM_LO = rd("IsAllowed")
            End While
            rd.Close()
            Return isAllowed_LM_LO
        Catch ex As Exception

        Finally
            gCon.sqlconn.Close()
        End Try
    End Function
    Public Function isAllowed_S_U_RIGHTS() As Boolean 'USER RIGHTS    
        Dim gCon As New Clsappconfiguration
        Try

            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.sqlconn, CommandType.StoredProcedure, "CIMS_USERRIGHTS_SELECTBYUSERandID", _
                                         New SqlParameter("@UserID", username), _
                                         New SqlParameter("@Fk_ModuleID", "S_U_RIGHTS"))
            While rd.Read
                isAllowed_S_U_RIGHTS = rd("IsAllowed")
            End While
            rd.Close()
            Return isAllowed_S_U_RIGHTS
        Catch ex As Exception

        Finally
            gCon.sqlconn.Close()
        End Try
    End Function
    Public Function isAllowed_LM_AE() As Boolean 'USER RIGHTS    
        Dim gCon As New Clsappconfiguration
        Try

            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.sqlconn, CommandType.StoredProcedure, "CIMS_USERRIGHTS_SELECTBYUSERandID", _
                                         New SqlParameter("@UserID", username), _
                                         New SqlParameter("@Fk_ModuleID", "LM_AE"))
            While rd.Read
                isAllowed_LM_AE = rd("IsAllowed")
            End While
            rd.Close()
            Return isAllowed_LM_AE
        Catch ex As Exception

        Finally
            gCon.sqlconn.Close()
        End Try
    End Function

    Public Function isAllowed_BANK_INFO() As Boolean 'USER RIGHTS    
        Dim gCon As New Clsappconfiguration
        Try

            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.sqlconn, CommandType.StoredProcedure, "CIMS_USERRIGHTS_SELECTBYUSERandID", _
                                         New SqlParameter("@UserID", username), _
                                         New SqlParameter("@Fk_ModuleID", "BANK_INFO"))
            While rd.Read
                isAllowed_BANK_INFO = rd("IsAllowed")
            End While
            rd.Close()
            Return isAllowed_BANK_INFO
        Catch ex As Exception

        Finally
            gCon.sqlconn.Close()
        End Try
    End Function
End Module
