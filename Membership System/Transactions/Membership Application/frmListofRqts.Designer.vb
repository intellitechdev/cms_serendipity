<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListofRqts
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cmdCancel_MembershipRqts = New System.Windows.Forms.Button()
        Me.cmdUpdate_MembershipRqts = New System.Windows.Forms.Button()
        Me.grdMembershipRqts = New System.Windows.Forms.DataGridView()
        Me.lblEmpNo = New System.Windows.Forms.Label()
        Me.PanePanel5 = New WindowsApplication2.PanePanel()
        Me.lblRqts = New System.Windows.Forms.Label()
        CType(Me.grdMembershipRqts, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanePanel5.SuspendLayout()
        Me.SuspendLayout()
        '
        'cmdCancel_MembershipRqts
        '
        Me.cmdCancel_MembershipRqts.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdCancel_MembershipRqts.Location = New System.Drawing.Point(331, 180)
        Me.cmdCancel_MembershipRqts.Name = "cmdCancel_MembershipRqts"
        Me.cmdCancel_MembershipRqts.Size = New System.Drawing.Size(72, 25)
        Me.cmdCancel_MembershipRqts.TabIndex = 34
        Me.cmdCancel_MembershipRqts.Text = "Cancel"
        Me.cmdCancel_MembershipRqts.UseVisualStyleBackColor = True
        '
        'cmdUpdate_MembershipRqts
        '
        Me.cmdUpdate_MembershipRqts.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdUpdate_MembershipRqts.Location = New System.Drawing.Point(253, 180)
        Me.cmdUpdate_MembershipRqts.Name = "cmdUpdate_MembershipRqts"
        Me.cmdUpdate_MembershipRqts.Size = New System.Drawing.Size(72, 25)
        Me.cmdUpdate_MembershipRqts.TabIndex = 33
        Me.cmdUpdate_MembershipRqts.Text = "Update"
        Me.cmdUpdate_MembershipRqts.UseVisualStyleBackColor = True
        '
        'grdMembershipRqts
        '
        Me.grdMembershipRqts.AllowUserToAddRows = False
        Me.grdMembershipRqts.AllowUserToDeleteRows = False
        Me.grdMembershipRqts.BackgroundColor = System.Drawing.Color.White
        Me.grdMembershipRqts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdMembershipRqts.Location = New System.Drawing.Point(-2, 24)
        Me.grdMembershipRqts.Name = "grdMembershipRqts"
        Me.grdMembershipRqts.Size = New System.Drawing.Size(405, 150)
        Me.grdMembershipRqts.TabIndex = 32
        '
        'lblEmpNo
        '
        Me.lblEmpNo.AutoSize = True
        Me.lblEmpNo.Location = New System.Drawing.Point(12, 186)
        Me.lblEmpNo.Name = "lblEmpNo"
        Me.lblEmpNo.Size = New System.Drawing.Size(0, 13)
        Me.lblEmpNo.TabIndex = 36
        '
        'PanePanel5
        '
        Me.PanePanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel5.Controls.Add(Me.lblRqts)
        Me.PanePanel5.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanePanel5.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.PanePanel5.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel5.InactiveGradientHighColor = System.Drawing.Color.Green
        Me.PanePanel5.InactiveGradientLowColor = System.Drawing.Color.GreenYellow
        Me.PanePanel5.Location = New System.Drawing.Point(0, 0)
        Me.PanePanel5.Name = "PanePanel5"
        Me.PanePanel5.Size = New System.Drawing.Size(403, 26)
        Me.PanePanel5.TabIndex = 35
        '
        'lblRqts
        '
        Me.lblRqts.AutoSize = True
        Me.lblRqts.BackColor = System.Drawing.Color.Transparent
        Me.lblRqts.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold)
        Me.lblRqts.ForeColor = System.Drawing.Color.White
        Me.lblRqts.Location = New System.Drawing.Point(3, 3)
        Me.lblRqts.Name = "lblRqts"
        Me.lblRqts.Size = New System.Drawing.Size(116, 19)
        Me.lblRqts.TabIndex = 14
        Me.lblRqts.Text = "Requirements"
        '
        'frmListofRqts
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(403, 208)
        Me.Controls.Add(Me.lblEmpNo)
        Me.Controls.Add(Me.PanePanel5)
        Me.Controls.Add(Me.cmdCancel_MembershipRqts)
        Me.Controls.Add(Me.cmdUpdate_MembershipRqts)
        Me.Controls.Add(Me.grdMembershipRqts)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmListofRqts"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "List of Rqts"
        CType(Me.grdMembershipRqts, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanePanel5.ResumeLayout(False)
        Me.PanePanel5.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PanePanel5 As WindowsApplication2.PanePanel
    Friend WithEvents lblRqts As System.Windows.Forms.Label
    Friend WithEvents cmdCancel_MembershipRqts As System.Windows.Forms.Button
    Friend WithEvents cmdUpdate_MembershipRqts As System.Windows.Forms.Button
    Friend WithEvents grdMembershipRqts As System.Windows.Forms.DataGridView
    Friend WithEvents lblEmpNo As System.Windows.Forms.Label
End Class
