Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmMasterfile_LoanType

#Region "Variables"
    Private code As Integer
    Private LoanCode, LoanTypeID As String
    Private earnedIncomeAcnt As String
    Private AccountList As Boolean = False
    Private AccountUpdate As Boolean = False
    Private ouT As String
#End Region

#Region "Property"
    Public Property GetEarnedIncome() As String
        Get
            Return earnedIncomeAcnt
        End Get
        Set(ByVal value As String)
            earnedIncomeAcnt = value
        End Set
    End Property

    Private Property getcode() As Integer
        Get
            Return code
        End Get
        Set(ByVal value As Integer)
            code = value
        End Set
    End Property
#End Region

#Region "Function"
    Public Function Numbers(ByVal C As Char) As Boolean
        If Not (Char.IsDigit(C)) And Not (Microsoft.VisualBasic.AscW(C) = 8) And Not (Microsoft.VisualBasic.AscW(C) = 13) Then
            MsgBox("Invalid Input! This field allows 0-9 only.", MsgBoxStyle.Exclamation, "Invalid")
            Return False
        Else
            Return True
        End If
    End Function
    Public Function Decimals(ByVal C As Char) As Boolean
        If Not (Char.IsDigit(C)) And Not (C = ".") And Not (Microsoft.VisualBasic.AscW(C) = 8) And Not (Microsoft.VisualBasic.AscW(C) = 13) Then
            MsgBox("Invalid Input! This field allows 0-9 and '.' only.", MsgBoxStyle.Exclamation, "Invalid")
            Return False
        Else
            Return True
        End If
    End Function
#End Region

#Region "View Account Code without param"
    'Private Sub AccountCode()
    '    Dim mycon As New Clsappconfiguration

    '    Using rd As SqlDataReader = SqlHelper.ExecuteReader(mycon.cnstring, CommandType.StoredProcedure, "CIMS_Masterfiles_Accounts_Load")
    '        Try
    '            While rd.Read
    '                cboAcntCode_LoanReceivable.Items.Add(rd.Item(1))
    '                cboAcntCode_Unearned.Items.Add(rd.Item(1))
    '                cboAcntCode_Earned.Items.Add(rd.Item(1))
    '                cboAcntCode_Service.Items.Add(rd.Item(1))
    '                cboAcntCode_CS.Items.Add(rd.Item(1))
    '                cboAcntCode_LoanPayable.Items.Add(rd.Item(1))
    '                cboAcntCode_Penalty.Items.Add(rd.Item(1))
    '            End While
    '        Catch ex As Exception
    '            MessageBox.Show(ex.Message, "AccountCode")
    '        End Try
    '    End Using
    'End Sub
#End Region

#Region "View Account Name without param"
    'Private Sub AccountName()
    '    'Dim mycon As New Clsappconfiguration
    '    'Dim rd As SqlDataReader
    '    'rd = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "CIMS_Masterfiles_Accounts_Load")
    '    'Try
    '    '    While rd.Read
    '    '        Me.cboAcntName_LoanReceivable.Items.Add(rd.Item(2))
    '    '        Me.cboAcntName_Unearned.Items.Add(rd.Item(2))
    '    '        Me.cboAcntName_Earned.Items.Add(rd.Item(2))
    '    '        Me.cboAcntName_Service.Items.Add(rd.Item(2))
    '    '        Me.cboAcntName_CS.Items.Add(rd.Item(2))
    '    '        Me.cboAcntName_Penalty.Items.Add(rd.Item(2))
    '    '        Me.cboAcntName_LoanPayable.Items.Add(rd.Item(2))
    '    '    End While
    '    '    rd.Close()
    '    'Catch ex As Exception
    '    '    MessageBox.Show(ex.Message, "AccountName")
    '    'End Try
    'End Sub
#End Region

#Region "View Account Code with param left side"
    'Private Sub AccountCodeparam(ByVal acctcode As String)
    '    Dim mycon As New Clsappconfiguration
    '    Dim rd As SqlDataReader
    '    rd = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "CIMS_Masterfiles_Accounts_LoadParam", _
    '                                 New SqlParameter("@acctcode", acctcode))
    '    Try
    '        While rd.Read
    '            getAcctcode() = rd.Item(0)
    '            Me.cboAcntName_LoanReceivable.Text = rd.Item(2)
    '        End While
    '        rd.Close()
    '    Catch ex As Exception
    '        MessageBox.Show(ex.Message, "AccountCode")
    '    End Try
    'End Sub
    'Private Sub AccountRateparam(ByVal acctcode As String)
    '    Dim mycon As New Clsappconfiguration
    '    Dim rd As SqlDataReader
    '    rd = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "CIMS_Masterfiles_Accounts_LoadParam", _
    '                                 New SqlParameter("@acctcode", acctcode))
    '    Try
    '        While rd.Read
    '            getAcctRate() = rd.Item(0)
    '            Me.cboAcntName_Unearned.Text = rd.Item(2)
    '        End While
    '        rd.Close()
    '    Catch ex As Exception
    '        MessageBox.Show(ex.Message, "AccountRateparam")
    '    End Try
    'End Sub
    'Private Sub AccountFeeparam(ByVal acctcode As String)
    '    Dim mycon As New Clsappconfiguration
    '    Dim rd As SqlDataReader
    '    rd = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "CIMS_Masterfiles_Accounts_LoadParam", _
    '                                 New SqlParameter("@acctcode", acctcode))
    '    Try
    '        While rd.Read
    '            getAcctFee() = rd.Item(0)
    '            Me.cboAcntName_Service.Text = rd.Item(2)
    '        End While
    '        rd.Close()
    '    Catch ex As Exception
    '        MessageBox.Show(ex.Message, "AccountFeeparam")
    '    End Try
    'End Sub
    'Private Sub AccountShareparam(ByVal acctcode As String)
    '    Dim mycon As New Clsappconfiguration
    '    Dim rd As SqlDataReader
    '    rd = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "CIMS_Masterfiles_Accounts_LoadParam", _
    '                                 New SqlParameter("@acctcode", acctcode))
    '    Try
    '        While rd.Read
    '            getAcctShare() = rd.Item(0)
    '            Me.cboAcntName_CS.Text = rd.Item(2)
    '        End While
    '        rd.Close()
    '    Catch ex As Exception
    '        MessageBox.Show(ex.Message, "AccountShareparam")
    '    End Try
    'End Sub
    'Private Sub AccountCash(ByVal acctcash As String)
    '    Dim mycon As New Clsappconfiguration
    '    Dim rd As SqlDataReader
    '    rd = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "CIMS_Masterfiles_Accounts_LoadParam", _
    '                                 New SqlParameter("@acctcode", acctcash))
    '    Try
    '        While rd.Read
    '            getAcctCash() = rd.Item(0)
    '            Me.cboAcntName_LoanPayable.Text = rd.Item(2)
    '        End While
    '        rd.Close()
    '    Catch ex As Exception
    '        MessageBox.Show(ex.Message, "AccountCash")
    '    End Try
    'End Sub
    'Private Sub AccountPenalty(ByVal acctPenalty As String)
    '    Dim mycon As New Clsappconfiguration
    '    Dim rd As SqlDataReader
    '    rd = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "CIMS_Masterfiles_Accounts_LoadParam", _
    '                                 New SqlParameter("@acctcode", acctPenalty))
    '    Try
    '        While rd.Read
    '            getAcctPenalty() = rd.Item(0)
    '            Me.cboAcntName_Penalty.Text = rd.Item(2)
    '        End While
    '        rd.Close()
    '    Catch ex As Exception
    '        MessageBox.Show(ex.Message, "AccountPenalty")
    '    End Try
    'End Sub

    'Private Function LoadAccountName(ByVal AccountCode As String, ByVal cboAccountName As ComboBox) As String
    '    Dim gCon As New Clsappconfiguration
    '    Dim acntID As String

    '    Using rd As SqlDataReader = ModifiedSqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "CIMS_Masterfiles_Accounts_LoadParam", _
    '                    New SqlParameter("@acctcode", AccountCode))
    '        Try
    '            While rd.Read
    '                cboAccountName.Text = rd.Item(1)

    '                acntID = rd.Item(0)
    '                Return acntID
    '            End While
    '        Catch ex As Exception
    '            Throw ex
    '        End Try

    '    End Using
    'End Function
#End Region

#Region "View Account Code with param right side"
    'Private Sub AccountCodeparamR(ByVal acctcode As String)
    '    Dim mycon As New Clsappconfiguration
    '    Dim rd As SqlDataReader
    '    rd = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "CIMS_Masterfiles_Accounts_LoadParam", _
    '                                 New SqlParameter("@acctcode", acctcode))
    '    Try
    '        While rd.Read
    '            getAcctcode() = rd.Item(0)
    '            Me.cboAcntCode_LoanReceivable.Text = rd.Item(1)
    '        End While
    '        rd.Close()
    '    Catch ex As Exception
    '        MessageBox.Show(ex.Message, "AccountCodeparamR")
    '    End Try
    'End Sub
    'Private Sub AccountRateparamR(ByVal acctcode As String)
    '    Dim mycon As New Clsappconfiguration
    '    Dim rd As SqlDataReader
    '    rd = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "CIMS_Masterfiles_Accounts_LoadParam", _
    '                                 New SqlParameter("@acctcode", acctcode))
    '    Try
    '        While rd.Read
    '            getAcctRate() = rd.Item(0)
    '            Me.cboAcntCode_Unearned.Text = rd.Item(1)
    '        End While
    '        rd.Close()
    '    Catch ex As Exception
    '        MessageBox.Show(ex.Message, "AccountRateparamR")
    '    End Try
    'End Sub
    'Private Sub AccountFeeparamR(ByVal acctcode As String)
    '    Dim mycon As New Clsappconfiguration
    '    Dim rd As SqlDataReader
    '    rd = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "CIMS_Masterfiles_Accounts_LoadParam", _
    '                                 New SqlParameter("@acctcode", acctcode))
    '    Try
    '        While rd.Read
    '            getAcctFee() = rd.Item(0)
    '            Me.cboAcntCode_Service.Text = rd.Item(1)
    '        End While
    '        rd.Close()
    '    Catch ex As Exception
    '        MessageBox.Show(ex.Message, "AccountFeeparamR")
    '    End Try
    'End Sub
    'Private Sub AccountShareparamR(ByVal acctcode As String)
    '    Dim mycon As New Clsappconfiguration
    '    Dim rd As SqlDataReader
    '    rd = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "CIMS_Masterfiles_Accounts_LoadParam", _
    '                                 New SqlParameter("@acctcode", acctcode))
    '    Try
    '        While rd.Read
    '            getAcctShare() = rd.Item(0)
    '            Me.cboAcntCode_CS.Text = rd.Item(1)
    '        End While
    '        rd.Close()
    '    Catch ex As Exception
    '        MessageBox.Show(ex.Message, "AccountShareparamR")
    '    End Try
    'End Sub
    'Private Sub AccountCashR(ByVal acctcash As String)
    '    Dim mycon As New Clsappconfiguration
    '    Dim rd As SqlDataReader
    '    rd = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "CIMS_Masterfiles_Accounts_LoadParam", _
    '                                 New SqlParameter("@acctcode", acctcash))
    '    Try
    '        'While rd.Read
    '        If rd.Read = True Then
    '            getAcctCash() = rd.Item(0)
    '            Me.cboAcntCode_LoanPayable.Text = rd.Item(1)
    '        End If

    '        'End While
    '        rd.Close()
    '    Catch ex As Exception
    '        MessageBox.Show(ex.Message, "AccountCashR")
    '    End Try
    'End Sub
    'Private Sub AccountPenaltyR(ByVal acctPenalty As String)
    '    Dim mycon As New Clsappconfiguration
    '    Dim rd As SqlDataReader
    '    rd = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "CIMS_Masterfiles_Accounts_LoadParam", _
    '                                 New SqlParameter("@acctcode", acctPenalty))
    '    Try
    '        ' While rd.Read
    '        If rd.Read = True Then
    '            getAcctPenalty() = rd.Item(0)
    '            Me.cboAcntCode_Penalty.Text = rd.Item(1)
    '        End If

    '        'End While

    '        rd.Close()
    '    Catch ex As Exception
    '        MessageBox.Show(ex.Message, "AccountPenaltyR")
    '    End Try
    'End Sub
    'Private Function LoadAccountCode(ByVal AccountCode As String, ByVal cboAccountName As ComboBox) As String
    '    Dim gCon As New Clsappconfiguration
    '    Dim acntID As String

    '    Using rd As SqlDataReader = ModifiedSqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "CIMS_Masterfiles_Accounts_LoadParam", _
    '                    New SqlParameter("@acctcode", AccountCode))
    '        Try
    '            While rd.Read
    '                cboAccountName.Text = rd.Item(2).ToString
    '                acntID = rd.Item(0).ToString
    '                Return acntID
    '            End While
    '        Catch ex As Exception
    '            Return Nothing
    '            Throw ex
    '        End Try
    '    End Using
    'End Function

#End Region

#Region "View Policies"
    Private Sub ListPolicies()
        Dim mycon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "CIMS_Masterfiles_Policy_LoadCodes_byProj",
                                     New SqlParameter("@ProjName", cboProject.Text))
        Try
            cboPoliciesCode.Items.Clear()
            Refresh()
            While rd.Read
                Me.cboPoliciesCode.Items.Add(rd.Item(0))
            End While
            rd.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Policies")
        End Try
    End Sub
#End Region

    'Private Sub viewval()
    '    Dim mycon As New Clsappconfiguration
    '    Dim rd As SqlDataReader
    '    rd = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "viewval")
    '    Try
    '        While rd.Read
    '            Me.TextBox1.Text.Insert(rd.Read)
    '        End While
    '        rd.Close()
    '    Catch ex As Exception
    '        MessageBox.Show(ex.Message, "Policies")
    '    End Try
    'End Sub

#Region "view create loan"
    Private Sub ViewCreatedLoan()
        Dim myconnection As New Clsappconfiguration
        Try
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(myconnection.cnstring, "CIMS_Masterfiles_LoanType_Load_byProject",
                              New SqlParameter("@ProjDesc", cboProject.Text))

            dgvLoanTypes.DataSource = ds.Tables(0)
            dgvLoanTypes.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            dgvLoanTypes.Columns(0).Visible = False
            dgvLoanTypes.Columns(1).Width = 100
            dgvLoanTypes.Columns(2).Width = 200
            dgvLoanTypes.Columns(3).Visible = False
            dgvLoanTypes.Columns(4).Visible = False
            dgvLoanTypes.Columns(5).Visible = False
            dgvLoanTypes.Columns(6).Visible = False
            dgvLoanTypes.Columns(7).Width = 350
            dgvLoanTypes.Columns(8).Visible = False
            dgvLoanTypes.Columns(9).Visible = False
            dgvLoanTypes.Columns(10).Visible = False
            dgvLoanTypes.Columns(11).Visible = False
            dgvLoanTypes.Columns(13).Visible = False
            dgvLoanTypes.Columns(14).Visible = False
            dgvLoanTypes.Columns(15).Visible = False
            dgvLoanTypes.Columns(16).Visible = True
            dgvLoanTypes.Columns(16).HeaderText = "Interest Type"
            dgvLoanTypes.Columns(16).Width = 200
        Catch ex As Exception
            frmMsgBox.txtMessage.Text = "Error! " + ex.ToString
            frmMsgBox.ShowDialog()
        End Try
    End Sub
#End Region

    Private Sub frmLoanTypeMaster_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        CreateColumn()
        Call _LoadAcctg_Proj()
        Call Load_LoanAccountName()
        'Call ListPolicies()
        'Call ViewCreatedLoan_Format()
        Call ViewCreatedLoan()
        Call ReadonlyTrue()
        Call SelectLoanDefault()
    End Sub

    Private Sub SelectLoanDefault()

    End Sub

#Region "Readonly Text/combo"
    Private Sub ReadonlyTrue()
        'txtname.ReadOnly = True
        txtName.Enabled = False
        txtpenaltyperc.ReadOnly = True
        txtTotalAllowable.ReadOnly = True
        txtCreditCommittee.ReadOnly = True
        txtboardmem.ReadOnly = True
        txtdescription.ReadOnly = True
        txtDaysValid.ReadOnly = True
        txtPercentAllowedToReloan.ReadOnly = True
        txtInterest.ReadOnly = True
        txtServiceFeeAmt.ReadOnly = True
        txtServiceFeePer.ReadOnly = True
        cboInterestType.Enabled = False
        txtRefund.ReadOnly = True
        cboPoliciesCode.Enabled = False
        cboIntType.Enabled = False
    End Sub
    Private Sub Readonlyfalse()
        'txtName.ReadOnly = False
        txtName.Enabled = True
        txtpenaltyperc.ReadOnly = False
        txtTotalAllowable.ReadOnly = False
        txtCreditCommittee.ReadOnly = False
        txtboardmem.ReadOnly = False
        txtdescription.ReadOnly = False
        txtDaysValid.ReadOnly = False
        txtPercentAllowedToReloan.ReadOnly = False
        txtInterest.ReadOnly = False
        txtServiceFeeAmt.ReadOnly = False
        txtServiceFeePer.ReadOnly = False
        cboInterestType.Enabled = True
        txtRefund.ReadOnly = False
        cboPoliciesCode.Enabled = True
        cboIntType.Enabled = True
    End Sub
#End Region

    Private Sub cboAcctFee_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.KeyChar = Microsoft.VisualBasic.ChrW(0)
    End Sub

    Private Sub cboAcctCode_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.KeyChar = Microsoft.VisualBasic.ChrW(0)
    End Sub

    Private Sub cboAcctShare_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.KeyChar = Microsoft.VisualBasic.ChrW(0)
    End Sub

    Private Sub cboCashAcct_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.KeyChar = Microsoft.VisualBasic.ChrW(0)
    End Sub

    Private Sub cboPenaltyAcct_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.KeyChar = Microsoft.VisualBasic.ChrW(0)
    End Sub

#Region "Clear Text"
    Private Sub ClearText()
        Me.txtLoanCode.Clear()
        Me.txtcode.Text = "0"
        Me.txtName.Text = ""
        txtTotalAllowable.Text = "0"
        Me.txtTotalAllowable.Text = "0"
        txtdescription.Text = ""
        Me.txtDaysValid.Text = "0"
        txtRefund.Text = ""
        txtServiceFeeAmt.Text = "0"
        txtServiceFeePer.Text = "0"
        txtInterest.Text = "0"
        Me.txtpenaltyperc.Text = "0"
        Me.txtCreditCommittee.Text = "0"
        Me.txtboardmem.Text = "0"
        txtAccountContribution.Text = ""
        txtAcctContriCode.Text = ""
        cboProject.Text = "Select Project"
        cboPoliciesCode.Text = ""
    End Sub
#End Region

#Region "Add/Edit Loan Type"
    Private Sub AddEditLoanType(ByVal loancode As Integer, ByVal loanname As String, ByVal policycode As Integer, ByVal loandesc As String, ByVal percentallowable As Integer, _
                                ByVal penalty As Decimal, ByVal patronagerefund As Integer,
                                ByVal noOfBoardMember As Integer, ByVal noOfCreditCommittee As Integer, ByVal dtDays As Integer, ByVal percentToReloan As Integer,
                                ByVal interest As Decimal, ByVal fnInterestType As String, ByVal serviceFeeInPercent As Integer, ByVal serviceFeeInAmt As Decimal)
        Dim myadapter As New SqlDataAdapter
        Dim mydataset As New DataSet
        Dim myconnection As New Clsappconfiguration

        Dim cmd As New SqlCommand("CIMS_Masterfiles_LoanType_AddEdit", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        Try
            With cmd
                .Parameters.Add("@loanCode", SqlDbType.Int, ParameterDirection.Input).Value = loancode
                .Parameters.Add("@AccountCode", SqlDbType.VarChar, 50, ParameterDirection.Input).Value = txtLoanCode.Text
                .Parameters.Add("@loanTypeName", SqlDbType.VarChar, 100, ParameterDirection.Input).Value = loanname
                .Parameters.Add("@policyCode", SqlDbType.Int, ParameterDirection.Input).Value = policycode
                .Parameters.Add("@loanDesc", SqlDbType.VarChar, 250, ParameterDirection.Input).Value = loandesc
                .Parameters.Add("@percentAllowable", SqlDbType.Int, ParameterDirection.Input).Value = percentallowable
                .Parameters.Add("@penalty", SqlDbType.Int, ParameterDirection.Input).Value = penalty
                .Parameters.Add("@patronageRefund", SqlDbType.Int, ParameterDirection.Input).Value = patronagerefund
                .Parameters.Add("@noOfBoardMember", SqlDbType.Int, ParameterDirection.Input).Value = noOfBoardMember
                .Parameters.Add("@noOfCreditCommittee", SqlDbType.Int, ParameterDirection.Input).Value = noOfCreditCommittee
                .Parameters.Add("@dtDays", SqlDbType.Int, ParameterDirection.Input).Value = dtDays
                .Parameters.Add("@percentToReloan", SqlDbType.Int, ParameterDirection.Input).Value = percentToReloan

                .Parameters.Add("@LoanTypeID", SqlDbType.Int)
                .Parameters("@LoanTypeID").Direction = ParameterDirection.Output

                .Parameters.Add("@interest", SqlDbType.Float, ParameterDirection.Input).Value = interest
                .Parameters.Add("@fcInterestType", SqlDbType.VarChar, 20, ParameterDirection.Input).Value = fnInterestType
                .Parameters.Add("@serviceFeeInPercent", SqlDbType.Int, ParameterDirection.Input).Value = serviceFeeInPercent
                .Parameters.Add("@serviceFeeInAmt", SqlDbType.Decimal, ParameterDirection.Input).Value = serviceFeeInAmt

                .Parameters.Add("@ContriCode", SqlDbType.VarChar, 50, ParameterDirection.Input).Value = txtAcctContriCode.Text
                .Parameters.Add("@ContriName", SqlDbType.VarChar, 150, ParameterDirection.Input).Value = txtAccountContribution.Text
                .Parameters.Add("@IntType", SqlDbType.VarChar, 20, ParameterDirection.Input).Value = cboIntType.Text

                .Parameters.Add("@co_name", SqlDbType.VarChar, 500, ParameterDirection.Input).Value = cboProject.Text
            End With

            myconnection.sqlconn.Open()
            cmd.ExecuteNonQuery()
            ouT = cmd.Parameters("@LoanTypeID").Value.ToString
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(myconnection.cnstring, CommandType.StoredProcedure, "CIMS_Masterfiles_Accounts_LoadParam",
                                                     New SqlParameter("@pk_Loantype", txtcode.Text),
                                                     New SqlParameter("@co_name", cboProject.Text))

            If AccountUpdate = True Then
                If rd.HasRows Then
                    Delete_Accounts_All(txtcode.Text)
                    For Each Rows As DataGridViewRow In GrdAccounts.Rows
                        AddAccounts(Rows.Cells(0).Value.ToString, txtcode.Text, Rows.Cells(1).Value.ToString, Rows.Cells(3).Value.ToString, _
                           Rows.Cells(4).Value.ToString, Rows.Cells(5).Value.ToString, Rows.Cells(6).Value.ToString, Rows.Cells(7).Value.ToString, Rows.Cells(8).Value.ToString)
                    Next
                Else
                    For Each Rows As DataGridViewRow In GrdAccounts.Rows
                        AddAccounts(Rows.Cells(0).Value.ToString, txtcode.Text, Rows.Cells(1).Value.ToString, Rows.Cells(3).Value.ToString, _
                           Rows.Cells(4).Value.ToString, Rows.Cells(5).Value.ToString, Rows.Cells(6).Value.ToString, Rows.Cells(7).Value.ToString, Rows.Cells(8).Value.ToString)
                    Next
                End If
            Else
                For Each Rows As DataGridViewRow In GrdAccounts.Rows
                    AddAccounts(Rows.Cells(0).Value.ToString, ouT, Rows.Cells(1).Value.ToString, Rows.Cells(3).Value.ToString, _
                       Rows.Cells(4).Value.ToString, Rows.Cells(5).Value.ToString, Rows.Cells(6).Value.ToString, Rows.Cells(7).Value.ToString, Rows.Cells(8).Value.ToString)
                Next
            End If

            myconnection.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString, "Add/Edit Loan Type", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
#End Region

#Region "Delete Loan Type"
    Private Sub DeleteLoanType(ByVal loancode As Integer, ByVal coid As String)
        Dim mycon As New Clsappconfiguration
        mycon.sqlconn.Open()
        Dim trans As SqlTransaction = mycon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Masterfiles_LoanType_Delete", _
                                      New SqlParameter("@loanCode", loancode))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "DeleteLoanType")
            'MessageBox.Show("Loan Type has Transaction", "Loan Type Delete", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Finally
            mycon.sqlconn.Close()
        End Try
    End Sub

#End Region

#Region "Check if loan type name is already exist"
    Private Sub CheckLoantypeName(ByVal loanname As String)
        Dim mycon As New Clsappconfiguration
        Dim rd As SqlDataReader
        'rd = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "CIMS_CIMS_m_Loans_LoanType_Validate", _
        '                             New SqlParameter("@fcLoanTypeName", loanname))
        Try
            'If rd.HasRows Then
            '    MessageBox.Show("Loan Type name is already exists. Please try another one.", "Invalid Duplicate Loan Name", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            'Else
            Call AddEditLoanType(0, Me.txtName.Text, Convert.ToInt32(Me.cboPoliciesCode.Text), Me.txtdescription.Text, _
                                CType(Me.txtTotalAllowable.Text, Integer), _
                                Me.txtpenaltyperc.Text, Me.txtRefund.Text, _
                                Me.txtboardmem.Text, Me.txtCreditCommittee.Text, Me.txtDaysValid.Text, GetEarnedIncome(),
                                CType(Me.txtInterest.Text, Decimal), cboInterestType.Text, CType(txtServiceFeePer.Text, Integer), CType(txtServiceFeeAmt.Text, Decimal))
            Call ViewCreatedLoan()
            'End If
        Catch ex As Exception
            'MessageBox.Show(ex.Message, "Loan Type Master", MessageBoxButtons.OK, MessageBoxIcon.Error)
            frmMsgBox.txtMessage.Text = "Please fill up the required fields!"
            frmMsgBox.Text = "Oops!"
            frmMsgBox.ShowDialog()
        End Try
    End Sub
#End Region

#Region "INSERT/UPDATE/Delete Accounts"
    Public Sub AddItem(ByVal PKAccount As String, ByVal accountname As String, ByVal accountcode As String, ByVal percentage As Boolean, ByVal percent As Double, ByVal amount As Double, ByVal debit As Boolean, ByVal inpayment As Boolean, ByVal accttype As String)
        Try
            Dim i As String
            Dim row As String() = {PKAccount, accountname, accountcode, percentage, percent, amount, debit, inpayment, accttype}
            Dim nRowIndex As Integer = GrdAccounts.Rows.Count - 1
            Dim nColumnIndex As Integer = 1
            If GrdAccounts.Rows.Count <> 0 Then
                For Each Rows As DataGridViewRow In GrdAccounts.Rows
                    i = Rows.Cells(nColumnIndex).Value.ToString
                Next
                If i <> accountcode Then
                    Me.GrdAccounts.Rows.Add(row)
                    GrdAccounts.Rows(nRowIndex).Selected = True
                    GrdAccounts.Rows(nRowIndex).Cells(nColumnIndex).Selected = True
                    GrdAccounts.FirstDisplayedScrollingRowIndex = nRowIndex
                End If
            Else
                Me.GrdAccounts.Rows.Add(row)
                Me.GrdAccounts.ClearSelection()
            End If
        Catch
            MessageBox.Show(Err.ToString, "Error..", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub AddAccounts(ByVal AccountID As String, ByVal LoanCode As String, ByVal Description As String, ByVal Percentage As Boolean, ByVal Percent As Decimal, ByVal Amount As Decimal, ByVal Debit As Boolean, ByVal Payment As Boolean, ByVal accttype As String)
        Dim mycon As New Clsappconfiguration
        Dim GUID As New Guid(AccountID)
        mycon.sqlconn.Open()
        Try
            SqlHelper.ExecuteNonQuery(mycon.cnstring, CommandType.StoredProcedure, "CIMS_Masterfiles_Accounts_Insert", _
                                      New SqlParameter("@fk_Account", GUID), _
                                      New SqlParameter("@pk_LoanType", LoanCode), _
                                      New SqlParameter("@Description", Description), _
                                      New SqlParameter("@Percentage", Percentage), _
                                      New SqlParameter("@Percent", Percent), _
                                      New SqlParameter("@Amount", Amount), _
                                      New SqlParameter("@Debit", Debit), _
                                      New SqlParameter("@InPayment", Payment), _
                                      New SqlParameter("@AcctType", accttype))
            mycon.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error......Add Accounts!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub Load_Accounts(ByVal LoanType As String)
        Dim mycon As New Clsappconfiguration
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(mycon.cnstring, CommandType.StoredProcedure, "CIMS_Masterfiles_Accounts_LoadParam",
                                                     New SqlParameter("@pk_Loantype", LoanType),
                                                     New SqlParameter("@co_name", cboProject.Text))
        Try
            GrdAccounts.Rows.Clear()
            While rd.Read = True
                AccountList = True
                AddItem(rd.Item(0).ToString, rd.Item(1).ToString, rd.Item(2).ToString, CType(rd.Item(3), Boolean), CType(rd.Item(4),
                    Double), CType(rd.Item(5), Double), CType(rd.Item(6), Boolean), CType(rd.Item(7), Boolean), CType(rd.Item(8), String))
            End While
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error...... Loan Accounts!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub Delete_Accounts_ByID(ByVal fk_Account As String, ByVal pk_LoanType As String)
        Dim mycon As New Clsappconfiguration
        Dim GUID As New Guid(fk_Account)
        mycon.sqlconn.Open()
        If fk_Account <> "" And pk_LoanType <> "" Then
            Try
                GrdAccounts.Rows.RemoveAt(GrdAccounts.SelectedRows(0).Index)
                SqlHelper.ExecuteNonQuery(mycon.cnstring, CommandType.StoredProcedure, "CIMS_Masterfiles_Accounts_Delete", _
                                          New SqlParameter("@fk_Account", fk_Account),
                                          New SqlParameter("@pk_LoanType", pk_LoanType))
                fk_Account = ""
                pk_LoanType = ""
                mycon.sqlconn.Close()
                Load_Accounts(LoanCode)
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Error...... Delete Account!", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try

        End If
    End Sub

    Private Sub Delete_Accounts_All(ByVal pk_LoanType As String)
        Dim mycon As New Clsappconfiguration
        mycon.sqlconn.Open()
        Try
            SqlHelper.ExecuteNonQuery(mycon.cnstring, CommandType.StoredProcedure, "CIMS_Masterfiles_Accounts_DeleteAll", _
                                      New SqlParameter("@pk_LoanType", pk_LoanType))
            mycon.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error...... Delete All Accounts!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub CreateColumn()
        Dim Percentage As New DataGridViewCheckBoxColumn()
        Dim Debit As New DataGridViewCheckBoxColumn()
        Dim InPayment As New DataGridViewCheckBoxColumn()

        With GrdAccounts
            .Columns.Clear()
            .Columns.Add("PK_Account", "PK_Account")
            .Columns("PK_Account").Visible = False

            .Columns.Add("Account", "Account Name")
            .Columns("Account").Width = 100

            .Columns.Add("AC", "Account Code")
            .Columns("AC").Width = 100

            With Percentage
                .HeaderText = "Percentage"
                .Name = "Per"
                .Width = 100
            End With
            .Columns.Insert(3, Percentage)

            .Columns.Add("Percent", "Percent")
            .Columns("Percent").Width = 100

            .Columns.Add("Amount", "Amount")
            .Columns("Percent").Width = 100

            With Debit
                .HeaderText = "Debit"
                .Name = "Debit"
                .Width = 100
            End With
            .Columns.Insert(6, Debit)

            With InPayment
                .HeaderText = "In Payment"
                .Name = "InPayment"
                .Width = 100
            End With
            .Columns.Insert(7, InPayment)

            .Columns.Add("AcctType", "Account Type")
            .Columns("AcctType").Width = 100
        End With
    End Sub
#End Region

    Private Sub btnsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsave.Click
        Try
            If btnsave.Text = "New" Then
                Call ClearText()
                Call Readonlyfalse()
                '#######################
                GrdAccounts.Columns.Clear()
                dgvLoanTypes.Columns.Clear()
                CreateColumn()
                '#######################
                btndelete.Visible = False
                btnupdate.Visible = False
                btnsave.Text = "Save"
                btnsave.Image = My.Resources.save2
                btnclose.Text = "Cancel"
                btnclose.Location = New System.Drawing.Point(75, 3)
                btnAdd.Enabled = True
                btnClear.Enabled = True
                btnRemove.Enabled = True
                GrdAccounts.ReadOnly = False
                AccountUpdate = False
            ElseIf btnsave.Text = "Save" Then
                If Me.txtname.Text <> "" Then
                    Call CheckLoantypeName(Me.txtname.Text)
                    'Call ClearText()
                    Call ReadonlyTrue()
                    btnsave.Text = "New"
                    btnsave.Image = My.Resources.new3
                    btnclose.Text = "Close"
                    btnclose.Location = New System.Drawing.Point(209, 3)
                    btndelete.Visible = True
                    btnupdate.Visible = True
                    btnAdd.Enabled = False
                    btnClear.Enabled = False
                    btnRemove.Enabled = False
                    GrdAccounts.ReadOnly = True
                Else
                    'MessageBox.Show("Empty field found! ", "Save", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    frmMsgBox.Text = "Oops!"
                    frmMsgBox.txtMessage.Text = "Please supply the Required field!"
                    frmMsgBox.ShowDialog()
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub btnupdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupdate.Click
        Try
            If cboProject.Text = "Select Project" Then
                MessageBox.Show("Please Select Project Name to Edit! ", "Edit Loan Type", MessageBoxButtons.OK, MessageBoxIcon.Information)
            ElseIf btnupdate.Text = "Edit" Then
                    Call Readonlyfalse()
                    'Call LoadLoanDetails()
                    Me.btnsave.Enabled = False
                    btnupdate.Text = "Update"
                    btnupdate.Image = My.Resources.save2
                    btnclose.Text = "Cancel"
                    btnclose.Location = New System.Drawing.Point(142, 3)
                    btndelete.Visible = False
                    btnAdd.Enabled = True
                    btnClear.Enabled = True
                    btnRemove.Enabled = True
                    GrdAccounts.ReadOnly = False
                    AccountUpdate = True
                ElseIf btnupdate.Text = "Update" Then
                    Call AddEditLoanType(CType(Me.txtcode.Text, Integer), Me.txtName.Text, Convert.ToInt32(Me.cboPoliciesCode.Text), Me.txtdescription.Text, _
                                        CType(Me.txtTotalAllowable.Text, Integer), _
                                        Me.txtpenaltyperc.Text, Me.txtRefund.Text, _
                                        Me.txtboardmem.Text, Me.txtCreditCommittee.Text, Me.txtDaysValid.Text, GetEarnedIncome(),
                                        CType(Me.txtInterest.Text, Decimal), cboInterestType.Text, CType(txtServiceFeePer.Text, Integer), CType(txtServiceFeeAmt.Text, Decimal))
                    Call ViewCreatedLoan()
                    'Call ClearText()
                    Call ReadonlyTrue()
                    btndelete.Visible = True
                    btnupdate.Text = "Edit"
                    btnupdate.Image = My.Resources.edit1
                    'GrdAccounts.Rows.Clear()
                    btnclose.Location = New System.Drawing.Point(209, 3)
                    btnclose.Text = "Close"
                    Me.btnsave.Enabled = True
                    Me.btndelete.Visible = True
                    btnAdd.Enabled = False
                    btnClear.Enabled = False
                    btnRemove.Enabled = False
                    GrdAccounts.ReadOnly = True
                Else
                    MessageBox.Show("Please Select Loan Type to Edit! ", "Edit Loan Type", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub btndelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndelete.Click
        Try
            getcode() = dgvLoanTypes.SelectedRows(0).Cells(0).Value.ToString
            Dim x As New DialogResult
            x = MessageBox.Show("Are you sure you want to permanently delete this record?", "Confirm Deletion", MessageBoxButtons.OKCancel, MessageBoxIcon.Information)
            If x = System.Windows.Forms.DialogResult.OK Then
                Call DeleteLoanType(code, cboProject.Text)
                Call ViewCreatedLoan()
                Call ClearText()
            ElseIf x = System.Windows.Forms.DialogResult.Cancel Then
            End If
        Catch ex As Exception
            MessageBox.Show("Please Select Loan Type to Delete! ", "Delete Loan Type", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        If btnclose.Text = "Cancel" Then
            Call ReadonlyTrue()
            btnsave.Text = "New"
            btnsave.Image = My.Resources.new3
            btnupdate.Text = "Edit"
            btnupdate.Image = My.Resources.edit1
            btnclose.Text = "Close"
            btnclose.Location = New System.Drawing.Point(209, 3)
            btndelete.Visible = True
            btnupdate.Visible = True
            btnsave.Enabled = True
            btnAdd.Enabled = False
            btnClear.Enabled = False
            btnRemove.Enabled = False
            GrdAccounts.ReadOnly = False
            GrdAccounts.Rows.Clear()
            'txtName.Text = ""
        ElseIf btnclose.Text = "Close" Then
            Me.Close()
        End If
    End Sub

    Private Sub txtInterestRate_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        'e.Handled = Not Numbers(e.KeyChar)
        e.Handled = Not Decimals(e.KeyChar)
    End Sub

    Private Sub txtServicefee_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        ' e.Handled = Not Numbers(e.KeyChar)
        e.Handled = Not Decimals(e.KeyChar)
    End Sub

    Private Sub txtcapitalShare_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        ' e.Handled = Not Numbers(e.KeyChar)
        e.Handled = Not Decimals(e.KeyChar)
    End Sub

    Private Sub txtMaxterm_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtTotalAllowable.KeyPress
        e.Handled = Not Numbers(e.KeyChar)
    End Sub

    Private Sub txtServiceAmt_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.Handled = Not Decimals(e.KeyChar)
    End Sub

    Private Sub txtRefund_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtRefund.KeyPress
        e.Handled = Not Numbers(e.KeyChar)
    End Sub

    Private Sub txtMaxterm_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTotalAllowable.TextChanged
        If Me.txtTotalAllowable.Text = "" Then
            Me.txtTotalAllowable.Text = "0"
        End If
    End Sub

    Private Sub txtRefund_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRefund.TextChanged
        If Me.txtRefund.Text = "" Then
            Me.txtRefund.Text = "0"
        End If
    End Sub

    Private Sub btnApprover_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnApprover.Click
        If cboProject.Text = "Select Project" Then
            MessageBox.Show("Select Project First", "Project", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Else
            frmLoan_Policy_Approvers_Master.ShowDialog()
        End If
    End Sub

    Private Sub txtpenaltyperc_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtpenaltyperc.TextChanged
        If Me.txtpenaltyperc.Text = "" Then
            Me.txtpenaltyperc.Text = "0"
        End If
    End Sub

    Private Sub LoadLoanDetails()
        Try
            With dgvLoanTypes
                LoanCode = .SelectedRows(0).Cells(0).Value.ToString
                txtcode.Text = .SelectedRows(0).Cells(0).Value.ToString
                txtLoanCode.Text = .SelectedRows(0).Cells(1).Value.ToString
                txtName.Text = .SelectedRows(0).Cells(2).Value.ToString
                txtTotalAllowable.Text = .SelectedRows(0).Cells(3).Value.ToString
                txtpenaltyperc.Text = .SelectedRows(0).Cells(4).Value.ToString
                txtRefund.Text = .SelectedRows(0).Cells(5).Value.ToString
                cboPoliciesCode.Text = .SelectedRows(0).Cells(6).Value.ToString
                txtdescription.Text = .SelectedRows(0).Cells(7).Value.ToString
                txtCreditCommittee.Text = .SelectedRows(0).Cells(8).Value.ToString
                txtboardmem.Text = .SelectedRows(0).Cells(9).Value.ToString
                txtDaysValid.Text = .SelectedRows(0).Cells(10).Value.ToString
                txtPercentAllowedToReloan.Text = .SelectedRows(0).Cells(11).Value.ToString
                txtInterest.Text = .SelectedRows(0).Cells(12).Value.ToString
                cboInterestType.Text = .SelectedRows(0).Cells(13).Value.ToString
                txtServiceFeePer.Text = .SelectedRows(0).Cells(14).Value.ToString
                txtServiceFeeAmt.Text = .SelectedRows(0).Cells(15).Value.ToString
                cboIntType.Text = .SelectedRows(0).Cells(16).Value.ToString
            End With
            Load_Accounts(txtcode.Text)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Load Loan Details", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub txtCBOAcctCode_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.KeyChar = Microsoft.VisualBasic.ChrW(0)
    End Sub

    Private Sub txtCBOAcctRate_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.KeyChar = Microsoft.VisualBasic.ChrW(0)
    End Sub

    Private Sub txtCBOAcctFee_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.KeyChar = Microsoft.VisualBasic.ChrW(0)
    End Sub

    Private Sub txtCBOAcctShare_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.KeyChar = Microsoft.VisualBasic.ChrW(0)
    End Sub

    Private Sub txtCBOCashacct_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.KeyChar = Microsoft.VisualBasic.ChrW(0)
    End Sub

    Private Sub txtCBOpenaltyacct_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.KeyChar = Microsoft.VisualBasic.ChrW(0)
    End Sub

    Private Sub btnPolicies_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPolicies.Click
        Try
            If cboProject.Text = "Select Project" Then
                MessageBox.Show("Select Project First", "Project", MessageBoxButtons.OK, MessageBoxIcon.Information)
            ElseIf Me.cboPoliciesCode.Text = "" Then
                MessageBox.Show("Select Loan Type or Policy code", "Policy code", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                frmLoan_Policy_Master.ShowDialog()
            End If
        Catch

        End Try
    End Sub

    Private Sub cboPoliciesCode_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboPoliciesCode.KeyPress
        e.KeyChar = Microsoft.VisualBasic.ChrW(0)
    End Sub

    Private Sub txtCreditCommittee_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtCreditCommittee.KeyPress
        e.Handled = Not Numbers(e.KeyChar)
    End Sub

    Private Sub txtboardmem_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtboardmem.KeyPress
        e.Handled = Not Numbers(e.KeyChar)
    End Sub

    Private Sub txtCreditCommittee_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCreditCommittee.TextChanged
        If Me.txtCreditCommittee.Text = "" Then
            Me.txtCreditCommittee.Text = "0"
        End If
    End Sub

    Private Sub txtboardmem_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtboardmem.TextChanged
        If Me.txtboardmem.Text = "" Then
            Me.txtboardmem.Text = "0"
        End If
    End Sub

    Private Sub txtname_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.lblStepone.Text = Me.txtName.Text
    End Sub

    Private Sub btnloanTerms_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnloanTerms.Click
        If cboProject.Text = "Select Project" Then
            MessageBox.Show("Select Project First", "Project", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Else
            frmMasterfile_LoanTerms.ShowDialog()
        End If
    End Sub

    Private Sub BtnRequirements_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnRequirements.Click
        If cboProject.Text = "Select Project" Then
            MessageBox.Show("Select Project First", "Project", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Else
            frmMasterfile_LoanType_Requirements.ShowDialog()
        End If
    End Sub

    Private Sub lvlViewLoans_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Call LoadLoanDetails()
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtDaysValid.KeyPress
        e.Handled = Not Numbers(e.KeyChar)
    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtDaysValid.TextChanged
        If Me.txtboardmem.Text = "" Then
            Me.txtboardmem.Text = "0"
        End If
    End Sub

    Private Sub btnPriority_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPriority.Click
        ' frmMasterfile_LoanType_Priority.Show() -- Old Priority
        frmPriorityMaster.fcLoanType = txtName.Text
        frmPriorityMaster.loantypeCode = txtcode.Text
        frmPriorityMaster.ShowDialog()
    End Sub

    Private Sub txtPercentAllowedToReloan_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPercentAllowedToReloan.KeyPress
        e.Handled = Not Numbers(e.KeyChar)
    End Sub

    Private Sub txtPercentAllowedToReloan_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPercentAllowedToReloan.TextChanged
        If Me.txtboardmem.Text = "" Then
            Me.txtboardmem.Text = "0"
        End If
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        frmMasterfile_Accounts.GetActive = 0
        frmMasterfile_Accounts.ShowDialog()
    End Sub

    Private Sub btnRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemove.Click
        If GrdAccounts.SelectedCells.Count <> 0 Then
            For Each row As DataGridViewRow In GrdAccounts.SelectedRows
                Delete_Accounts_ByID(row.Cells(0).Value.ToString(), LoanCode)
            Next
        End If
    End Sub

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        If AccountList = True Then
            If txtcode.Text <> "" Then
                Delete_Accounts_All(txtcode.Text)
            Else
                Delete_Accounts_All(ouT)
            End If
        Else
            GrdAccounts.Rows.Clear()
        End If
    End Sub

    Private Sub Load_LoanAccountName()
        Dim mycon As New Clsappconfiguration
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(mycon.cnstring, "_Loan_LoanAccountName")

        With txtName
            .DataSource = ds.Tables(0)
            .ValueMember = "acnt_code"
            .DisplayMember = "acnt_name"
        End With
    End Sub

    Private Sub txtName_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtName.SelectedIndexChanged
        txtLoanCode.Text = txtName.SelectedValue.ToString
    End Sub

    Private Sub dgvLoanTypes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvLoanTypes.Click
        LoadLoanDetails()
    End Sub

    Private Sub btnRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        Call ClearText()
        Call ReadonlyTrue()
        '#######################
        GrdAccounts.Columns.Clear()
        CreateColumn()
        '#######################
    End Sub

    Private Sub btnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse.Click
        frmBrowseAccounts.StartPosition = FormStartPosition.CenterScreen
        frmBrowseAccounts.ShowDialog()
    End Sub

    Public Sub GetAccountForContribution(ByVal code As String, ByVal name As String)
        txtAcctContriCode.Text = code
        txtAccountContribution.Text = name
    End Sub

    Private Sub txtcode_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtcode.TextChanged
        LoanUsed_AcctForContribution()
    End Sub

    Private Sub LoanUsed_AcctForContribution()
        Dim mycon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(mycon.cnstring, "_Select_LoanType_AccountForContribution",
                                      New SqlParameter("@LoanCode", txtcode.Text))
        While rd.Read
            txtAcctContriCode.Text = rd(0)
            txtAccountContribution.Text = rd(1)
        End While
    End Sub

    Private Sub btnComaker_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnComaker.Click
        frmComaker_Settings.StartPosition = FormStartPosition.CenterScreen
        frmComaker_Settings.ShowDialog()
    End Sub

#Region "Filter Project"

    Private Sub _LoadAcctg_Proj()
        Dim mycon As New Clsappconfiguration
        Try
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(mycon.cnstring, "_Load_Project")
            cboProject.Items.Clear()
            While rd.Read
                With cboProject
                    .Items.Add(rd(0).ToString)
                End With
            End While
            cboProject.Text = "Select Project"
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub Load_LoanAccountName_byProj()
        Dim mycon As New Clsappconfiguration
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(mycon.cnstring, "_Loan_LoanAccountName_byProj",
                                      New SqlParameter("@ProjDesc", cboProject.Text))

        With txtName
            .DataSource = ds.Tables(0)
            .ValueMember = "acnt_code"
            .DisplayMember = "acnt_name"
        End With
    End Sub

    Private Sub cboProject_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboProject.SelectedValueChanged
        ViewCreatedLoan()
    End Sub

    Private Sub cboProject_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboProject.TextChanged
        Call Load_LoanAccountName_byProj()
        Call ListPolicies()
    End Sub
#End Region

    Private Sub cboProject_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboProject.SelectedIndexChanged

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        frmComputationTableLoan.txtLoanType.Text = txtName.Text
        frmComputationTableLoan._LoadSettings(txtName.Text)
        frmComputationTableLoan.StartPosition = FormStartPosition.CenterScreen
        frmComputationTableLoan.ShowDialog()
    End Sub
End Class