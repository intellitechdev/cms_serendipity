<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMembersWithdrawalReport
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMembersWithdrawalReport))
        Me.crvRpt = New CrystalDecisions.Windows.Forms.CrystalReportViewer()
        Me.bgwMembersTermination = New System.ComponentModel.BackgroundWorker()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.ToolStrip_Journal = New System.Windows.Forms.ToolStripButton()
        Me.toolStrip_Post = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.bgwJournalEntries = New System.ComponentModel.BackgroundWorker()
        Me.bgwProcessTermination = New System.ComponentModel.BackgroundWorker()
        Me.loadingLabel = New System.Windows.Forms.Label()
        Me.btnEditInterest = New System.Windows.Forms.Button()
        Me.ToolStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'crvRpt
        '
        Me.crvRpt.ActiveViewIndex = -1
        Me.crvRpt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.crvRpt.Cursor = System.Windows.Forms.Cursors.Default
        Me.crvRpt.DisplayBackgroundEdge = False
        Me.crvRpt.Dock = System.Windows.Forms.DockStyle.Fill
        Me.crvRpt.EnableDrillDown = False
        Me.crvRpt.Location = New System.Drawing.Point(0, 76)
        Me.crvRpt.Name = "crvRpt"
        Me.crvRpt.SelectionFormula = ""
        Me.crvRpt.ShowCloseButton = False
        Me.crvRpt.ShowGroupTreeButton = False
        Me.crvRpt.ShowRefreshButton = False
        Me.crvRpt.Size = New System.Drawing.Size(852, 414)
        Me.crvRpt.TabIndex = 1
        Me.crvRpt.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
        Me.crvRpt.ViewTimeSelectionFormula = ""
        '
        'bgwMembersTermination
        '
        '
        'ToolStrip1
        '
        Me.ToolStrip1.AutoSize = False
        Me.ToolStrip1.BackColor = System.Drawing.Color.White
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStrip_Journal, Me.toolStrip_Post, Me.ToolStripSeparator1})
        Me.ToolStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(852, 76)
        Me.ToolStrip1.TabIndex = 5
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'ToolStrip_Journal
        '
        Me.ToolStrip_Journal.Image = CType(resources.GetObject("ToolStrip_Journal.Image"), System.Drawing.Image)
        Me.ToolStrip_Journal.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ToolStrip_Journal.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStrip_Journal.Name = "ToolStrip_Journal"
        Me.ToolStrip_Journal.Size = New System.Drawing.Size(82, 65)
        Me.ToolStrip_Journal.Text = "Journal Entries"
        Me.ToolStrip_Journal.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'toolStrip_Post
        '
        Me.toolStrip_Post.Image = Global.WindowsApplication2.My.Resources.Resources.post_add
        Me.toolStrip_Post.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.toolStrip_Post.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.toolStrip_Post.Name = "toolStrip_Post"
        Me.toolStrip_Post.Size = New System.Drawing.Size(52, 65)
        Me.toolStrip_Post.Text = "Post"
        Me.toolStrip_Post.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.AutoSize = False
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 65)
        '
        'bgwProcessTermination
        '
        '
        'loadingLabel
        '
        Me.loadingLabel.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.loadingLabel.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.loadingLabel.Image = Global.WindowsApplication2.My.Resources.Resources.LoadingData
        Me.loadingLabel.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.loadingLabel.Location = New System.Drawing.Point(352, 190)
        Me.loadingLabel.Name = "loadingLabel"
        Me.loadingLabel.Size = New System.Drawing.Size(119, 114)
        Me.loadingLabel.TabIndex = 3
        Me.loadingLabel.Text = "Loading Report..."
        Me.loadingLabel.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'btnEditInterest
        '
        Me.btnEditInterest.BackColor = System.Drawing.Color.White
        Me.btnEditInterest.Enabled = False
        Me.btnEditInterest.FlatAppearance.BorderSize = 0
        Me.btnEditInterest.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditInterest.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEditInterest.Image = Global.WindowsApplication2.My.Resources.Resources.editpaste
        Me.btnEditInterest.Location = New System.Drawing.Point(148, 3)
        Me.btnEditInterest.Name = "btnEditInterest"
        Me.btnEditInterest.Size = New System.Drawing.Size(75, 63)
        Me.btnEditInterest.TabIndex = 6
        Me.btnEditInterest.Text = "Edit Interest Refund"
        Me.btnEditInterest.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnEditInterest.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnEditInterest.UseVisualStyleBackColor = False
        '
        'frmMembersWithdrawalReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(852, 490)
        Me.Controls.Add(Me.btnEditInterest)
        Me.Controls.Add(Me.loadingLabel)
        Me.Controls.Add(Me.crvRpt)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(668, 429)
        Me.Name = "frmMembersWithdrawalReport"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Membership Termination"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents crvRpt As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents bgwMembersTermination As System.ComponentModel.BackgroundWorker
    Friend WithEvents loadingLabel As System.Windows.Forms.Label
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents ToolStrip_Journal As System.Windows.Forms.ToolStripButton
    Friend WithEvents toolStrip_Post As System.Windows.Forms.ToolStripButton
    Friend WithEvents bgwJournalEntries As System.ComponentModel.BackgroundWorker
    Friend WithEvents bgwProcessTermination As System.ComponentModel.BackgroundWorker
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents btnEditInterest As System.Windows.Forms.Button
End Class
