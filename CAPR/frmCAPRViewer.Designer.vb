<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCAPRViewer
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.BGWorker1stPage = New System.ComponentModel.BackgroundWorker
        Me.BGWorker2nd3rdPages = New System.ComponentModel.BackgroundWorker
        Me.TabControl1 = New System.Windows.Forms.TabControl
        Me.TabPage1 = New System.Windows.Forms.TabPage
        Me.Label1 = New System.Windows.Forms.Label
        Me.CRVRpt = New CrystalDecisions.Windows.Forms.CrystalReportViewer
        Me.TabPage2 = New System.Windows.Forms.TabPage
        Me.Label2 = New System.Windows.Forms.Label
        Me.CrvRpt2 = New CrystalDecisions.Windows.Forms.CrystalReportViewer
        Me.TabPage3 = New System.Windows.Forms.TabPage
        Me.Label3 = New System.Windows.Forms.Label
        Me.CrvRpt3 = New CrystalDecisions.Windows.Forms.CrystalReportViewer
        Me.TabPage4 = New System.Windows.Forms.TabPage
        Me.TabPage5 = New System.Windows.Forms.TabPage
        Me.Label5 = New System.Windows.Forms.Label
        Me.CrvRpt5 = New CrystalDecisions.Windows.Forms.CrystalReportViewer
        Me.BGWorker5thPage = New System.ComponentModel.BackgroundWorker
        Me.btnPrint = New System.Windows.Forms.Button
        Me.BtnBack = New System.Windows.Forms.Button
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.TabPage5.SuspendLayout()
        Me.SuspendLayout()
        '
        'BGWorker1stPage
        '
        Me.BGWorker1stPage.WorkerSupportsCancellation = True
        '
        'BGWorker2nd3rdPages
        '
        Me.BGWorker2nd3rdPages.WorkerSupportsCancellation = True
        '
        'TabControl1
        '
        Me.TabControl1.Appearance = System.Windows.Forms.TabAppearance.FlatButtons
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Controls.Add(Me.TabPage4)
        Me.TabControl1.Controls.Add(Me.TabPage5)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabControl1.Location = New System.Drawing.Point(0, 0)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(491, 453)
        Me.TabControl1.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.White
        Me.TabPage1.Controls.Add(Me.Label1)
        Me.TabPage1.Controls.Add(Me.CRVRpt)
        Me.TabPage1.Location = New System.Drawing.Point(4, 27)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(483, 422)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Page 1"
        '
        'Label1
        '
        Me.Label1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label1.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Image = Global.WindowsApplication2.My.Resources.Resources.LoadingData
        Me.Label1.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Label1.Location = New System.Drawing.Point(183, 149)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(112, 125)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Loading. . ."
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'CRVRpt
        '
        Me.CRVRpt.ActiveViewIndex = -1
        Me.CRVRpt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CRVRpt.DisplayGroupTree = False
        Me.CRVRpt.DisplayStatusBar = False
        Me.CRVRpt.DisplayToolbar = False
        Me.CRVRpt.Dock = System.Windows.Forms.DockStyle.Fill
        Me.CRVRpt.Location = New System.Drawing.Point(3, 3)
        Me.CRVRpt.Name = "CRVRpt"
        Me.CRVRpt.SelectionFormula = ""
        Me.CRVRpt.ShowGotoPageButton = False
        Me.CRVRpt.ShowGroupTreeButton = False
        Me.CRVRpt.ShowRefreshButton = False
        Me.CRVRpt.Size = New System.Drawing.Size(477, 416)
        Me.CRVRpt.TabIndex = 1
        Me.CRVRpt.ViewTimeSelectionFormula = ""
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.Color.White
        Me.TabPage2.Controls.Add(Me.Label2)
        Me.TabPage2.Controls.Add(Me.CrvRpt2)
        Me.TabPage2.Location = New System.Drawing.Point(4, 27)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(483, 422)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Page 2"
        '
        'Label2
        '
        Me.Label2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label2.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Image = Global.WindowsApplication2.My.Resources.Resources.LoadingData
        Me.Label2.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Label2.Location = New System.Drawing.Point(183, 149)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(112, 125)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Loading. . ."
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'CrvRpt2
        '
        Me.CrvRpt2.ActiveViewIndex = -1
        Me.CrvRpt2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CrvRpt2.DisplayGroupTree = False
        Me.CrvRpt2.DisplayStatusBar = False
        Me.CrvRpt2.DisplayToolbar = False
        Me.CrvRpt2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.CrvRpt2.Location = New System.Drawing.Point(3, 3)
        Me.CrvRpt2.Name = "CrvRpt2"
        Me.CrvRpt2.SelectionFormula = ""
        Me.CrvRpt2.ShowGotoPageButton = False
        Me.CrvRpt2.ShowGroupTreeButton = False
        Me.CrvRpt2.ShowRefreshButton = False
        Me.CrvRpt2.Size = New System.Drawing.Size(477, 416)
        Me.CrvRpt2.TabIndex = 2
        Me.CrvRpt2.ViewTimeSelectionFormula = ""
        '
        'TabPage3
        '
        Me.TabPage3.BackColor = System.Drawing.Color.White
        Me.TabPage3.Controls.Add(Me.Label3)
        Me.TabPage3.Controls.Add(Me.CrvRpt3)
        Me.TabPage3.Location = New System.Drawing.Point(4, 27)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(483, 422)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Page 3"
        '
        'Label3
        '
        Me.Label3.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label3.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Image = Global.WindowsApplication2.My.Resources.Resources.LoadingData
        Me.Label3.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Label3.Location = New System.Drawing.Point(183, 149)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(112, 125)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Loading. . ."
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'CrvRpt3
        '
        Me.CrvRpt3.ActiveViewIndex = -1
        Me.CrvRpt3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CrvRpt3.DisplayGroupTree = False
        Me.CrvRpt3.DisplayStatusBar = False
        Me.CrvRpt3.DisplayToolbar = False
        Me.CrvRpt3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.CrvRpt3.Location = New System.Drawing.Point(3, 3)
        Me.CrvRpt3.Name = "CrvRpt3"
        Me.CrvRpt3.SelectionFormula = ""
        Me.CrvRpt3.ShowGotoPageButton = False
        Me.CrvRpt3.ShowGroupTreeButton = False
        Me.CrvRpt3.ShowRefreshButton = False
        Me.CrvRpt3.Size = New System.Drawing.Size(477, 416)
        Me.CrvRpt3.TabIndex = 2
        Me.CrvRpt3.ViewTimeSelectionFormula = ""
        '
        'TabPage4
        '
        Me.TabPage4.BackColor = System.Drawing.Color.White
        Me.TabPage4.Location = New System.Drawing.Point(4, 27)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage4.Size = New System.Drawing.Size(483, 422)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "Page 4"
        '
        'TabPage5
        '
        Me.TabPage5.BackColor = System.Drawing.Color.White
        Me.TabPage5.Controls.Add(Me.Label5)
        Me.TabPage5.Controls.Add(Me.CrvRpt5)
        Me.TabPage5.Location = New System.Drawing.Point(4, 27)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage5.Size = New System.Drawing.Size(483, 422)
        Me.TabPage5.TabIndex = 4
        Me.TabPage5.Text = "Page 5"
        '
        'Label5
        '
        Me.Label5.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label5.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Image = Global.WindowsApplication2.My.Resources.Resources.LoadingData
        Me.Label5.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Label5.Location = New System.Drawing.Point(183, 149)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(112, 125)
        Me.Label5.TabIndex = 5
        Me.Label5.Text = "Loading. . ."
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'CrvRpt5
        '
        Me.CrvRpt5.ActiveViewIndex = -1
        Me.CrvRpt5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CrvRpt5.DisplayGroupTree = False
        Me.CrvRpt5.DisplayStatusBar = False
        Me.CrvRpt5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.CrvRpt5.EnableDrillDown = False
        Me.CrvRpt5.Location = New System.Drawing.Point(3, 3)
        Me.CrvRpt5.Name = "CrvRpt5"
        Me.CrvRpt5.SelectionFormula = ""
        Me.CrvRpt5.ShowCloseButton = False
        Me.CrvRpt5.ShowExportButton = False
        Me.CrvRpt5.ShowGotoPageButton = False
        Me.CrvRpt5.ShowGroupTreeButton = False
        Me.CrvRpt5.ShowPrintButton = False
        Me.CrvRpt5.ShowRefreshButton = False
        Me.CrvRpt5.Size = New System.Drawing.Size(477, 416)
        Me.CrvRpt5.TabIndex = 4
        Me.CrvRpt5.ViewTimeSelectionFormula = ""
        '
        'BGWorker5thPage
        '
        Me.BGWorker5thPage.WorkerSupportsCancellation = True
        '
        'btnPrint
        '
        Me.btnPrint.Enabled = False
        Me.btnPrint.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPrint.Location = New System.Drawing.Point(404, 1)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(75, 27)
        Me.btnPrint.TabIndex = 6
        Me.btnPrint.Text = "Print"
        Me.btnPrint.UseVisualStyleBackColor = True
        '
        'BtnBack
        '
        Me.BtnBack.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnBack.Location = New System.Drawing.Point(323, 1)
        Me.BtnBack.Name = "BtnBack"
        Me.BtnBack.Size = New System.Drawing.Size(75, 28)
        Me.BtnBack.TabIndex = 4
        Me.BtnBack.Text = "Back"
        Me.BtnBack.UseVisualStyleBackColor = True
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        '
        'frmCAPRViewer
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(491, 453)
        Me.Controls.Add(Me.BtnBack)
        Me.Controls.Add(Me.btnPrint)
        Me.Controls.Add(Me.TabControl1)
        Me.MinimumSize = New System.Drawing.Size(434, 487)
        Me.Name = "frmCAPRViewer"
        Me.Text = "Cooperative Annual Performance Report"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage5.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents BGWorker1stPage As System.ComponentModel.BackgroundWorker
    Friend WithEvents BGWorker2nd3rdPages As System.ComponentModel.BackgroundWorker
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents CRVRpt As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents CrvRpt2 As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents CrvRpt3 As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage5 As System.Windows.Forms.TabPage
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents CrvRpt5 As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents BGWorker5thPage As System.ComponentModel.BackgroundWorker
    Friend WithEvents btnPrint As System.Windows.Forms.Button
    Friend WithEvents BtnBack As System.Windows.Forms.Button
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
End Class
