﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmInvestment_Entry

    Private con As New Clsappconfiguration
    Dim i As Integer = 0
    'Public xCtr As Integer
    Public totDebit As Double = 0
    Public totcredit As Double = 0
    Public fxkey As String
    Public keyresult As String = ""
    Public xCtr As Integer

    Private Sub frmInvestment_Entry_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        cbDocNum.Text = ""
        txtParticulars.Text = ""
        totDebit = 0
        totcredit = 0
        txtDate.Text = Format(Date.Now, "MM/dd/yyyy")
        LoadUser()
        GenerateLoanPaymentEntry()
        If isLoadHeader() Then
            RecomputeNetproceed()
        End If
    End Sub

    Private Sub RecomputeNetproceed()
        Dim subtotcredit As Decimal
        Dim subtotdebit As Decimal

        For x As Integer = 0 To dgvEntry.RowCount - 1
            With dgvEntry
                subtotcredit = subtotcredit + Val(.Item("Credit", x).Value)
                subtotdebit = subtotdebit + Val(.Item("Debit", x).Value)
            End With
        Next
        txtDebit.Text = Format(subtotdebit, "##,##0.00")
        txtCredit.Text = Format(subtotcredit, "##,##0.00")
        txtNetproceeds.Text = Format(CDec(txtDebit.Text) - CDec(txtCredit.Text), "##,##0.00")
    End Sub

    Private Sub LoadUser()
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(con.cnstring, "_Select_Cashier",
                                      New SqlParameter("@username", mod_UsersAccessibility.xUser))
        While rd.Read
            txtCreatedby.Text = rd("email").ToString
            'txtCashier.Text = rd("Fullname")
        End While
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        txtDebit.Text = ""
        txtCredit.Text = ""
        GetJVKeyID = ""
        keyresult = "cancelled"
        i = 0
        dgvEntry.Columns.Clear()
        Me.Close()
    End Sub

    Private Function isLoadHeader() As Boolean
        Try
            Dim rd As SqlDataReader
            rd = SqlHelper.ExecuteReader(con.cnstring, "_Investment_CreateHeader",
                                          New SqlParameter("@investmentNo", frmInvestmentApproval.lblSelected.Text))
            While rd.Read
                With dgvEntry
                    .Rows.Add()
                    .Item("IDNo", i).Value = rd(0)
                    .Item("ClientName", i).Value = rd(1)
                    .Item("AcctRef", i).Value = rd(2)
                    .Item("Code", i).Value = rd(3)
                    .Item("AccountTitle", i).Value = rd(4)
                    .Item("Credit", i).Value = rd(5)
                End With
            End While
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Sub GenerateLoanPaymentEntry()
        Dim btnselect As New DataGridViewCheckBoxColumn
        Dim IDNo As New DataGridViewTextBoxColumn
        Dim ClientName As New DataGridViewTextBoxColumn
        Dim LoanNo As New DataGridViewTextBoxColumn
        Dim AcctRef As New DataGridViewTextBoxColumn
        Dim Code As New DataGridViewTextBoxColumn
        Dim AccountTitle As New DataGridViewTextBoxColumn
        Dim Debit As New DataGridViewTextBoxColumn
        Dim Credit As New DataGridViewTextBoxColumn

        With btnselect
            .Name = "btnselect"
            .HeaderText = "Select"
            .Width = 60
        End With
        With IDNo
            .HeaderText = "ID No"
            .Name = "IDNo"
            .DataPropertyName = "IDNo"
            .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .ReadOnly = True
        End With
        With ClientName
            .HeaderText = "Client Name"
            .Name = "ClientName"
            .DataPropertyName = "ClientName"
            .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .ReadOnly = True
        End With
        With LoanNo
            .HeaderText = "Loan No."
            .Name = "LoanNo"
            .DataPropertyName = "LoanNo"
            .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .ReadOnly = True
        End With
        With AcctRef
            .HeaderText = "Acct Ref."
            .Name = "AcctRef"
            .DataPropertyName = "AcctRef"
            .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .ReadOnly = True
        End With
        With Code
            .HeaderText = "Code"
            .Name = "Code"
            .DataPropertyName = "Code"
            .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .ReadOnly = True
        End With
        With AccountTitle
            .HeaderText = "Account Title"
            .Name = "AccountTitle"
            .DataPropertyName = "AccountTitle"
            .ReadOnly = True
        End With
        With Debit
            .HeaderText = "Debit"
            .Name = "Debit"
            .DataPropertyName = "Debit"
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            '.ReadOnly = True
        End With
        With Credit
            .HeaderText = "Credit"
            .Name = "Credit"
            .DataPropertyName = "Credit"
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            ' .ReadOnly = True
        End With
        With dgvEntry
            .Columns.Clear()
            .Columns.Add(btnselect)
            .Columns.Add(IDNo)
            .Columns.Add(ClientName)
            .Columns.Add(LoanNo)
            .Columns.Add(AcctRef)
            .Columns.Add(Code)
            .Columns.Add(AccountTitle)
            .Columns.Add(Debit)
            .Columns.Add(Credit)
            .Columns("Debit").DefaultCellStyle.Format = "##,##0.00"
            .Columns("Credit").DefaultCellStyle.Format = "##,##0.00"
        End With

    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        For Each row As DataGridViewRow In dgvEntry.Rows
            With dgvEntry
                If .Item("btnselect", row.Index).Value = True Then
                    .Rows.Remove(row)
                    i -= 1
                End If
            End With
        Next
    End Sub

    Private Sub dgvEntry_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvEntry.CellValueChanged
        RecomputeNetproceed()
    End Sub

    Private Sub btnCheckNo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCheckNo.Click
        frmInvestment_Doc.StartPosition = FormStartPosition.CenterScreen
        frmInvestment_Doc.ShowDialog()
    End Sub

    Private Sub SaveEntry()

        If GetJVKeyID() = "" Then
            GetJVKeyID() = System.Guid.NewGuid.ToString()
        End If

        Dim MyGuid As Guid = New Guid(GetJVKeyID)
        fxkey = MyGuid.ToString

        Dim maxSerialNo As Integer = Generate_JournalNo_Serial(Date.Now, "OR")
        'Dim empno = dgvPaymentEntry.SelectedRows(0).Cells(0).Value.ToString

        SqlHelper.ExecuteNonQuery(con.cnstring, "_Investment_CAS_t_GeneralJournal_Header_InsertUpdate",
                              New SqlParameter("@fxKeyJVNo", MyGuid),
                              New SqlParameter("@fdTransDate", txtDate.Text),
                              New SqlParameter("@fbAdjust", False),
                              New SqlParameter("@fcEmployeeNo", " "),
                              New SqlParameter("@fcMemo", " "),
                              New SqlParameter("@fdTotAmt", txtDebit.Text),
                              New SqlParameter("@user", txtCreatedby.Text),
                              New SqlParameter("@doctypeInitial", "OR"),
                              New SqlParameter("@maxSerialNo", maxSerialNo),
                              New SqlParameter("@fiEntryNo", cbDocNum.Text),
                              New SqlParameter("@fbIsCancelled", False))

    End Sub


    Private jvKeyID As String

    Public Property GetJVKeyID() As String
        Get
            Return jvKeyID
        End Get
        Set(ByVal value As String)
            jvKeyID = value
        End Set
    End Property

    Private Function Generate_JournalNo_Serial(ByVal transDate As Date, ByVal docTypeInitial As String) As Integer
        Dim serialno As Integer
        ' Dim gCon As New Clsappconfiguration
        Using rd As SqlDataReader = SqlHelper.ExecuteReader(con.cnstring, CommandType.StoredProcedure, "usp_m_JournalNo_GenerateSerialPerMonthPerYear", _
                                                            New SqlParameter("@date", transDate), _
                                                            New SqlParameter("@doctypeInitial", docTypeInitial))
            If rd.Read() Then
                serialno = rd("maxSerialNo").ToString
            Else
                serialno = ""
            End If
            Return serialno
        End Using
    End Function

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If cbDocNum.Text = "" Then
                MsgBox("Please Select Document No.!", vbInformation, "Oops!")
                Exit Sub
            Else
                SaveEntry()
                SaveDetails()
                UpdateDocNumber()

                If isUpdate_Status() Then
                    frmInvestmentApproval.lblSelected.Text = "No Selected"
                    frmInvestmentApproval.btnApprove.Enabled = False
                    If frmInvestmentApproval.isLoad_InvestmentApp(frmInvestmentApproval.lblInvestmentType.Text) Then
                        'hey :D
                    End If
                End If

                frmMsgBox.txtMessage.Text = "Data Successfully Saved!"
                frmMsgBox.ShowDialog()

                GetJVKeyID = ""
                GenerateLoanPaymentEntry()
                txtDebit.Text = ""
                txtCredit.Text = ""

                keyresult = "Ok"
                frmLoan_Approval.btnApprove.Enabled = False
                frmLoan_Approval.btnDisapprove.Enabled = False
                i = 0
                dgvEntry.Columns.Clear()
                Me.Close()
            End If
        Catch ex As Exception
            frmMsgBox.txtMessage.Text = "Error :" + ex.Message
            frmMsgBox.ShowDialog()
        End Try
    End Sub

    Private Sub UpdateDocNumber()
        SqlHelper.ExecuteNonQuery(con.cnstring, "_Update_DocNumber_Used",
                                   New SqlParameter("@DocNumber", cbDocNum.Text),
                                   New SqlParameter("@DateUsed", txtDate.Text))
    End Sub

    Private Sub SaveDetails()
        Try
            Dim MyGuid As Guid = New Guid(fxkey)

            For xCtr = 0 To dgvEntry.RowCount - 1

                With dgvEntry

                    Dim accounttitle As String = IIf(IsDBNull(.Item("Code", xCtr).Value), Nothing, .Item("Code", xCtr).Value)
                    Dim empno As String = .Item("IDNo", xCtr).Value
                    Dim loan As String = .Item("LoanNo", xCtr).Value
                    Dim acctref As String = .Item("AcctRef", xCtr).Value
                    Dim debit As Double = .Item("Debit", xCtr).Value
                    Dim credit As Double = .Item("Credit", xCtr).Value

                    SqlHelper.ExecuteNonQuery(con.cnstring, "Investment_t_tJVEntry_details_save",
                             New SqlParameter("@fiEntryNo", cbDocNum.Text),
                             New SqlParameter("@AccountCode", accounttitle),
                             New SqlParameter("@fdDebit", debit),
                             New SqlParameter("@fdCredit", credit),
                             New SqlParameter("@fcMemo", txtParticulars.Text),
                             New SqlParameter("@EmployeeNo", empno),
                             New SqlParameter("@fbBillable", False),
                             New SqlParameter("@fxKey", System.Guid.NewGuid),
                             New SqlParameter("@fk_JVHeader", MyGuid),
                             New SqlParameter("@transDate", txtDate.Text),
                             New SqlParameter("@fcLoanRef", loan),
                             New SqlParameter("@fcAccRef", acctref))

                End With

            Next
        Catch ex As Exception
            ' MessageBox.Show(ex.ToString)
            MsgBox(ex.ToString)
            'MessageBox.Show("Error! Duplicate ID of Accounts. Please ask Administrator.", "Oops")
            Exit Sub
        End Try

        xCtr = 0
    End Sub

    Private Function isUpdate_Status() As Boolean
        Try
            SqlHelper.ExecuteNonQuery(con.cnstring, "_Investment_Status_Update",
                                       New SqlParameter("@investmentNo", frmInvestmentApproval.lblSelected.Text))
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

End Class