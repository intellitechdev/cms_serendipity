﻿Imports System.Data.SqlClient
Imports Microsoft.VisualBasic.FileSystem
Imports System.Data
Imports System.IO
Imports Microsoft.ApplicationBlocks.Data

'Module : Loan Amo Uploader
'Programmer : Vincent Nacar
'Minimum Upload Speed : 0.1 Sec
'Maximum Upload Speed : 60 Sec
'-------------------------------------------------------
Public Class frmAmortizationUploader

    Dim filenym As String
    Dim fpath As String
    Dim itemCount As Long
    Dim con As New Clsappconfiguration

    Private Sub frmAmortizationUploader_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub Import()
        Dim MyConnection As System.Data.OleDb.OleDbConnection
        Dim DtSet As System.Data.DataSet
        Dim MyCommand As System.Data.OleDb.OleDbDataAdapter
        MyConnection = New System.Data.OleDb.OleDbConnection("provider=Microsoft.Jet.OLEDB.4.0;Data Source='" + txtPath.Text.ToString + "';Extended Properties=Excel 8.0;")
        MyCommand = New System.Data.OleDb.OleDbDataAdapter("select * from [Sheet1$]", MyConnection)
        MyCommand.TableMappings.Add("Table", "Vincent00x21")
        DtSet = New System.Data.DataSet
        MyCommand.Fill(DtSet)
        dgvList.DataSource = DtSet.Tables(0)
        MyConnection.Close()
    End Sub

    Private Sub btnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse.Click
        Try
            OpenFileDialog1.ShowDialog()
            filenym = OpenFileDialog1.FileName
            fpath = Path.GetFullPath(filenym)
            txtPath.Text = fpath

            Import()
        Catch ex As Exception
            ' frmMsgBox.txtMessage.Text = "Error :" + ex.ToString
            frmMsgBox.txtMessage.Text = "User Cancelled!"
            frmMsgBox.ShowDialog()
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnUpload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        Select Case MsgBox("Are your sure?", vbYesNo, "Upload Loan Forward Balance")
            Case vbYes
                processWait.Style = ProgressBarStyle.Marquee
                processWait.MarqueeAnimationSpeed = 5
                If is_Uploaded() = True Then
                    processWait.MarqueeAnimationSpeed = 0
                    frmMsgBox.txtMessage.Text = "Upload Complete , Total No. of Item(s):" + itemCount.ToString
                    frmMsgBox.ShowDialog()
                    dgvList.Columns.Clear()
                Else
                    Exit Sub
                End If
            Case Else
                Exit Sub
        End Select
    End Sub
    Dim myid As New Guid
    Private Function is_Uploaded() As Boolean

        Try
            For xCtr As Integer = 0 To dgvList.RowCount - 1

                With dgvList
                    Dim loanNo As String = .Item("LoanNo", xCtr).Value.ToString
                    Dim idno As Integer = .Item("PayNo", xCtr).Value
                    Dim xdate As Date = .Item("Date", xCtr).Value
                    Dim principal As String = .Item("Principal", xCtr).Value.ToString
                    Dim interest As String = .Item("Interest", xCtr).Value.ToString
                    Dim servicefee As String = .Item("ServiceFee", xCtr).Value.ToString
                    myid = Guid.NewGuid

                    SqlHelper.ExecuteNonQuery(con.cnstring, "_Amortization_Upload",
                                               New SqlParameter("@myid", myid),
                                               New SqlParameter("@LoanNo", loanNo),
                                               New SqlParameter("@PayNo", idno),
                                               New SqlParameter("@Date", xdate),
                                               New SqlParameter("@Principal", principal),
                                               New SqlParameter("@Interest", interest))
                End With

                itemCount += 1
            Next

        Catch ex As Exception
        End Try

        Return True
    End Function

End Class