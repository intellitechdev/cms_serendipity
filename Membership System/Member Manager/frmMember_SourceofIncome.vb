Imports System.Data.Sql
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmMember_SourceofIncome
    Public Function annuall(ByVal C As Char) As Boolean
        If Not (Char.IsDigit(C)) And Not (C = ".") And Not (Microsoft.VisualBasic.AscW(C) = 8) And Not (Microsoft.VisualBasic.AscW(C) = 13) Then
            MsgBox("Invalid Input! This field allows 0-9 only.", MsgBoxStyle.Exclamation, "Error Message")
            Return False
        Else
            Return True
        End If
    End Function
#Region "Variables"
    Public pkId As String
#End Region
#Region "Property"
    Public Property getpksource() As String
        Get
            Return pkId
        End Get
        Set(ByVal value As String)
            pkId = value
        End Set
    End Property
#End Region
#Region "Add/Edit Source of Income"
    Private Sub AddEditSourceIncome(ByVal empid As String, ByVal sourceincome As String, ByVal annuall As Decimal, ByVal pkID As String)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Member_OtherIncomeSource_AddEdit", _
                                      New SqlParameter("@employeeNo", empid), _
                                      New SqlParameter("@fcSourceOfIncome", sourceincome), _
                                      New SqlParameter("@fcAnnualIncome", annuall), _
                                      New SqlParameter("@pk_IncomeSource", pkID))
            trans.Commit()

        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "Add Source Income")
        Finally
            gcon.sqlconn.Close()
        End Try

    End Sub
#End Region
    Private Sub frmSourceofIncome_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub txtAnnuallInc_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtAnnuallInc.KeyPress
        e.Handled = Not annuall(e.KeyChar)
    End Sub


    Private Sub txtAnnuallInc_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtAnnuallInc.TextChanged
        If Me.txtAnnuallInc.Text = "" Then
            Me.txtAnnuallInc.Text = "0.00"
        End If
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Call AddEditSourceIncome(frmMember_Master.txtEmployeeNo.Text.Trim, Me.txtSourceInc.Text, Me.txtAnnuallInc.Text, "")
        Call frmMember_Master.GetsourceofIncomce(frmMember_Master.txtEmployeeNo.Text.Trim)
        Me.Close()
    End Sub

    Private Sub btnupdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupdate.Click
        Call AddEditSourceIncome(frmMember_Master.txtEmployeeNo.Text.Trim, Me.txtSourceInc.Text, Me.txtAnnuallInc.Text, pkId)
        Call frmMember_Master.GetsourceofIncomce(frmMember_Master.txtEmployeeNo.Text.Trim)
        Me.Close()
    End Sub

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        Me.Close()
    End Sub
End Class