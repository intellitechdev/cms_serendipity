﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmOPSetup

    Private con As New Clsappconfiguration

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub frmOPSetup_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        _loadAccts("")
    End Sub

    Private Sub _loadAccts(ByVal key As String)
        Try
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(con.cnstring, "_REBATE_SElect_ACCOUNT",
                                           New SqlParameter("@key", key))
            DataGridView1.DataSource = ds.Tables(0)
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged
        _loadAccts(TextBox1.Text)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If MsgBox("Are you sure?", vbYesNo, "Add Account") = vbYes Then
            With DataGridView1.CurrentRow
                frmAmortOtherPayment._AddAcct(.Cells(0).Value.ToString, .Cells(1).Value.ToString)
                frmAmortOtherPayment._LoadAcct()
            End With
            Me.Close()
        Else
            Exit Sub
        End If
    End Sub

End Class