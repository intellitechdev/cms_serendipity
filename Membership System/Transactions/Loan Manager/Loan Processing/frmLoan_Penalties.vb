Imports System.Data.SqlClient
Imports System.Data.Sql
Imports Microsoft.ApplicationBlocks.Data
Public Class frmloan_Penalties
#Region "Property"
    Public empid As String
    Public lastname As String
    Public Property getempno() As String
        Get
            Return empid
        End Get
        Set(ByVal value As String)
            empid = value
        End Set
    End Property
    Public Property getlastname() As String
        Get
            Return lastname
        End Get
        Set(ByVal value As String)
            lastname = value
        End Set
    End Property
#End Region
    Private Sub frmloan_Penalties_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If empid Is Nothing Then

        Else
            Call ViewPenalties(empid)

        End If
        If lastname Is Nothing Then

        Else
            Call getID4lastname(lastname)
        End If

    End Sub
#Region "Show Penalties"
    Public Sub ViewPenalties(ByVal ID As String)
        Dim gcon As New Clsappconfiguration
        Dim ds As New DataSet
        Dim ad As New SqlDataAdapter
        Dim cmd As New SqlCommand("MSS_LoanTracking_GetLoansPenalties", gcon.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        gcon.sqlconn.Open()
        Try
            With cmd.Parameters
                .Add("@EmployeeNo", SqlDbType.VarChar, 256).Value = ID.Trim
                .Add("@status", SqlDbType.Bit).Value = 0
            End With

            ad.SelectCommand = cmd
            ad.Fill(ds, "CIMS_t_Loans_Penalties")

            Me.GridPenalties.DataSource = ds
            Me.GridPenalties.DataMember = "CIMS_t_Loans_Penalties"

            cmd.ExecuteNonQuery()
            gcon.sqlconn.Close()
        Catch ex As Exception
            Throw ex
        End Try

    End Sub
#End Region
#Region "Get ID for lastname"
    Public Sub getID4lastname(ByVal lastname As String)
        Dim gcon As New Clsappconfiguration
        Dim dr As SqlDataReader = SqlHelper.ExecuteReader(gcon.sqlconn, CommandType.StoredProcedure, "MSS_MembersInfo_SearchID", New SqlParameter("@membername", lastname))
        Try
            While dr.Read
                Dim result As String = dr.Item(0).ToString
                Call ViewPenalties(result.Trim)
            End While
        Catch ex As Exception

        End Try
    End Sub
#End Region
#Region "Show Penalties by lastname using ID"
   

#End Region
End Class