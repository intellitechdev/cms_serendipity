﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Module AuditTrail

    Private gcon As New Clsappconfiguration
    Public fxKeyUser As String

    Public Sub _AuditTrail_Insert(ByVal xmodule As String, ByVal desc As String)
        Try
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, "_AuditTrail_Get_fxKeyUser",
                                   New SqlParameter("@username", xUserStack.xUsername))
            If rd.HasRows Then
                While rd.Read
                    fxKeyUser = rd(0).ToString
                End While
                SqlHelper.ExecuteNonQuery(gcon.cnstring, "_AuditTrail_CMS_Insert",
                           New SqlParameter("@compname", getCoop()),
                           New SqlParameter("@fdDate", Date.Now),
                           New SqlParameter("@keyUser", fxKeyUser),
                           New SqlParameter("@module", xmodule),
                           New SqlParameter("@fcDescription", desc))
            Else
                ' MsgBox("User not found in Accounting!, Audit Trail may not Recorded.", vbCritical, "Warning.")
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Function getCoop()
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gcon.cnstring, "_Select_CooperativeName")
        While rd.Read
            Return rd("fcCoopName").ToString
        End While
    End Function

End Module
Module xUserStack
    Public xUsername As String
End Module
