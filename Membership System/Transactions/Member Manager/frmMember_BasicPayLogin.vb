﻿Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Security
Imports System.Security.Principal.WindowsIdentity
Imports Microsoft.ApplicationBlocks.Data

Public Class frmMember_BasicPayLogin

    Dim pswrd As String
    Dim uNym As String
    Dim userID As String

    Private Sub Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel.Click
        Me.Close()
    End Sub

    Public Sub GetPassword(ByVal id As Integer)
        Dim myconnection As New Clsappconfiguration
        Try
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(myconnection.cnstring, CommandType.StoredProcedure, "USP_EMPLOYEE_BASICPAYLOGIN_SELECT", _
                           New SqlParameter("@Emp_ID", id))
            If rd.Read() Then
                Dim user As String = rd("username")
                If user = uNym Then
                    pswrd = rd("Password")
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub

    Public Sub Login()
        Dim myconnection As New Clsappconfiguration
        Try
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(myconnection.cnstring, CommandType.StoredProcedure, "USP_EMPLOYEE_LOGIN_SELECT", _
                        New SqlParameter("@username", txtUsername.Text), _
                        New SqlParameter("@password", txtPassword.Text))

            If rd.Read() Then

                'Get User Id

                userID = rd.Item(2).ToString()
                'mod_UsersAccessibility.getusername = rd.Item(0).ToString()
            End If

            Call GetPassword(userID)

            If txtUsername.Text = uNym And txtPassword.Text = pswrd Then
                'Show basic pay and hide login
                Me.Hide()
                frmMember_Master.btnrestricted.Hide()
                txtPassword.Text = ""
                txtUsername.Text = ""
                txtUsername.Focus()
            Else
                MessageBox.Show("login failed", "UcoreSoftware", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtUsername.Focus()
            End If
            rd.Close()
        Catch ex As Exception
        End Try

    End Sub

    Private Sub Ok_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Ok.Click
        Call Login()
        Me.Text = "Login Basic Pay"
        Call frmMember_Master.GetmemberPayrollHistory(frmMember_Master.txtEmployeeNo.Text)
        frmMember_Master.btnPayrollHistory.Text = "Close"
    End Sub

    Private Sub txtPassword_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPassword.KeyPress
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            Call Login()
        End If
    End Sub

    Private Sub frmMember_BasicPayLogin_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        uNym = frmLogin.Username.Text
    End Sub
End Class