﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmReport_LoanDetails

    Private rptsummary As New ReportDocument
    Private gcon As New Clsappconfiguration

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        CrvRpt.PrintReport()
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If

        Dim obj As ReportObject

        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function
    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        ' for each table apply connection info
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            ' check if logon was successful
            ' if TestConnectivity returns false,
            ' check logon credentials
            If (tbl.TestConnectivity()) Then
                'drop fully qualified table location
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next
        Return True
    End Function

    Public Sub LoadREport(ByVal empno As String, ByVal loanNo As String, ByVal comp As String)
        Try
            rptsummary.Load(Application.StartupPath & "\LoanReport\Analog_LoanDetails.rpt")
            Logon(rptsummary, gcon.Server, gcon.Database, gcon.Username, gcon.Password)
            rptsummary.Refresh()
            rptsummary.SetParameterValue("@EmployeeNo", empno)
            rptsummary.SetParameterValue("@loanNo", loanNo)
            rptsummary.SetParameterValue("@Company", comp)
            rptsummary.SetParameterValue("@LoanNo", loanNo, "Analog_ApprovedLoanSubreport.rpt")
            rptsummary.SetParameterValue("@LoanNo", loanNo, "SOL_AmortSchedule.rpt")

            CrvRpt.ReportSource = rptsummary
        Catch ex As Exception
            frmMsgBox.txtMessage.Text = "Error : " + ex.ToString
            frmMsgBox.ShowDialog()
        End Try
    End Sub

    Private Sub frmReport_LoanDetails_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Text = "Loan Summary"
    End Sub

    Private Sub btnSignatories_Click(sender As System.Object, e As System.EventArgs) Handles btnSignatories.Click
        frmVoucherSignatories.ShowDialog()
    End Sub
End Class