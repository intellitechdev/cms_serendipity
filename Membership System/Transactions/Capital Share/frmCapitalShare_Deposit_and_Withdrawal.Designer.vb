﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCapitalShare_Deposit_and_Withdrawal
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblMemberName = New System.Windows.Forms.Label()
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.txtAmount = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txttotalContri = New System.Windows.Forms.TextBox()
        Me.MTcboBackAcnt = New MTGCComboBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.PanePanel1 = New WindowsApplication2.PanePanel()
        Me.btnDeposit = New System.Windows.Forms.Button()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.PanePanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblMemberName
        '
        Me.lblMemberName.AutoSize = True
        Me.lblMemberName.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMemberName.Location = New System.Drawing.Point(4, 9)
        Me.lblMemberName.Name = "lblMemberName"
        Me.lblMemberName.Size = New System.Drawing.Size(57, 23)
        Me.lblMemberName.TabIndex = 0
        Me.lblMemberName.Text = "Name"
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DateTimePicker1.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DateTimePicker1.Location = New System.Drawing.Point(277, 68)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(105, 23)
        Me.DateTimePicker1.TabIndex = 1
        '
        'txtAmount
        '
        Me.txtAmount.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtAmount.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAmount.Location = New System.Drawing.Point(233, 123)
        Me.txtAmount.Name = "txtAmount"
        Me.txtAmount.Size = New System.Drawing.Size(149, 23)
        Me.txtAmount.TabIndex = 5
        Me.txtAmount.Text = "0.00"
        Me.txtAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(5, 103)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(97, 14)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Total Contribution:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(5, 75)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(34, 14)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "Date:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(5, 132)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(49, 14)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "Amount:"
        '
        'txttotalContri
        '
        Me.txttotalContri.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txttotalContri.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.txttotalContri.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txttotalContri.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txttotalContri.Location = New System.Drawing.Point(243, 101)
        Me.txttotalContri.Name = "txttotalContri"
        Me.txttotalContri.ReadOnly = True
        Me.txttotalContri.Size = New System.Drawing.Size(139, 16)
        Me.txttotalContri.TabIndex = 42
        Me.txttotalContri.Text = "0.00"
        Me.txttotalContri.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'MTcboBackAcnt
        '
        Me.MTcboBackAcnt.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MTcboBackAcnt.ArrowBoxColor = System.Drawing.SystemColors.Control
        Me.MTcboBackAcnt.ArrowColor = System.Drawing.Color.Black
        Me.MTcboBackAcnt.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.MTcboBackAcnt.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.MTcboBackAcnt.BindedControl = Nothing
        Me.MTcboBackAcnt.BorderStyle = MTGCComboBox.TipiBordi.Fixed3D
        Me.MTcboBackAcnt.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.MTcboBackAcnt.ColumnNum = 2
        Me.MTcboBackAcnt.ColumnWidth = "250;0"
        Me.MTcboBackAcnt.DisabledArrowBoxColor = System.Drawing.SystemColors.Control
        Me.MTcboBackAcnt.DisabledArrowColor = System.Drawing.Color.LightGray
        Me.MTcboBackAcnt.DisabledBackColor = System.Drawing.SystemColors.Control
        Me.MTcboBackAcnt.DisabledBorderColor = System.Drawing.SystemColors.InactiveBorder
        Me.MTcboBackAcnt.DisabledForeColor = System.Drawing.SystemColors.GrayText
        Me.MTcboBackAcnt.DisplayMember = "Text"
        Me.MTcboBackAcnt.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.MTcboBackAcnt.DropDownBackColor = System.Drawing.Color.FromArgb(CType(CType(193, Byte), Integer), CType(CType(210, Byte), Integer), CType(CType(238, Byte), Integer))
        Me.MTcboBackAcnt.DropDownForeColor = System.Drawing.Color.Black
        Me.MTcboBackAcnt.DropDownStyle = MTGCComboBox.CustomDropDownStyle.DropDown
        Me.MTcboBackAcnt.DropDownWidth = 270
        Me.MTcboBackAcnt.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold)
        Me.MTcboBackAcnt.GridLineColor = System.Drawing.Color.LightGray
        Me.MTcboBackAcnt.GridLineHorizontal = False
        Me.MTcboBackAcnt.GridLineVertical = True
        Me.MTcboBackAcnt.LoadingType = MTGCComboBox.CaricamentoCombo.ComboBoxItem
        Me.MTcboBackAcnt.Location = New System.Drawing.Point(112, 152)
        Me.MTcboBackAcnt.ManagingFastMouseMoving = True
        Me.MTcboBackAcnt.ManagingFastMouseMovingInterval = 30
        Me.MTcboBackAcnt.MaxDropDownItems = 20
        Me.MTcboBackAcnt.Name = "MTcboBackAcnt"
        Me.MTcboBackAcnt.SelectedItem = Nothing
        Me.MTcboBackAcnt.SelectedValue = Nothing
        Me.MTcboBackAcnt.Size = New System.Drawing.Size(270, 24)
        Me.MTcboBackAcnt.TabIndex = 44
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Bold)
        Me.Label11.Location = New System.Drawing.Point(5, 162)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(49, 14)
        Me.Label11.TabIndex = 43
        Me.Label11.Text = "Account:"
        '
        'ComboBox1
        '
        Me.ComboBox1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.ComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox1.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Bold)
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Items.AddRange(New Object() {"Deposit", "Withdrawal"})
        Me.ComboBox1.Location = New System.Drawing.Point(168, 40)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(214, 22)
        Me.ComboBox1.TabIndex = 45
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(5, 43)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(99, 14)
        Me.Label4.TabIndex = 46
        Me.Label4.Text = "Select Transaction:"
        '
        'PanePanel1
        '
        Me.PanePanel1.Controls.Add(Me.btnDeposit)
        Me.PanePanel1.Controls.Add(Me.btnClose)
        Me.PanePanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PanePanel1.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.PanePanel1.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel1.InactiveGradientHighColor = System.Drawing.Color.LawnGreen
        Me.PanePanel1.InactiveGradientLowColor = System.Drawing.Color.Lime
        Me.PanePanel1.InactiveTextColor = System.Drawing.Color.Blue
        Me.PanePanel1.Location = New System.Drawing.Point(0, 194)
        Me.PanePanel1.Name = "PanePanel1"
        Me.PanePanel1.Size = New System.Drawing.Size(387, 30)
        Me.PanePanel1.TabIndex = 41
        '
        'btnDeposit
        '
        Me.btnDeposit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDeposit.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold)
        Me.btnDeposit.Location = New System.Drawing.Point(212, 3)
        Me.btnDeposit.Name = "btnDeposit"
        Me.btnDeposit.Size = New System.Drawing.Size(82, 23)
        Me.btnDeposit.TabIndex = 2
        Me.btnDeposit.Text = "Deposit"
        Me.btnDeposit.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold)
        Me.btnClose.Location = New System.Drawing.Point(300, 3)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(82, 23)
        Me.btnClose.TabIndex = 4
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmCapitalShare_Deposit_and_Withdrawal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(387, 224)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.ComboBox1)
        Me.Controls.Add(Me.MTcboBackAcnt)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.txttotalContri)
        Me.Controls.Add(Me.PanePanel1)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtAmount)
        Me.Controls.Add(Me.DateTimePicker1)
        Me.Controls.Add(Me.lblMemberName)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frmCapitalShare_Deposit_and_Withdrawal"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "CapitalShare Deposit and Withdrawal"
        Me.PanePanel1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblMemberName As System.Windows.Forms.Label
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents btnDeposit As System.Windows.Forms.Button
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents txtAmount As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents PanePanel1 As WindowsApplication2.PanePanel
    Friend WithEvents txttotalContri As System.Windows.Forms.TextBox
    Friend WithEvents MTcboBackAcnt As MTGCComboBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
End Class
