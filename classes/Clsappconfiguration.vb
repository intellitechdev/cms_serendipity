Option Strict Off
Option Explicit On
Imports System.Text
Imports System.Security.Cryptography
Imports System.Data.SqlClient.SqlConnection
Imports System.Data.SqlClient
Imports System.Configuration
Public Class Clsappconfiguration
    Public cnstring As String
    Public sqlconn As New SqlConnection
    Public Server As String
    Public Database As String
    Public Username As String
    Public Password As String
    Public ReadOnly Property getservername1() As String
        Get
            Return Server
        End Get
    End Property
    Public ReadOnly Property getdatabasename1() As String
        Get
            Return Database
        End Get
    End Property

    Public Sub New()

        Dim appRdr As New System.Configuration.AppSettingsReader

        Server = appRdr.GetValue("Server", GetType(String))
        Database = appRdr.GetValue("Database", GetType(String))
        Username = appRdr.GetValue("Username", GetType(String))
        Password = appRdr.GetValue("Password", GetType(String))

        cnstring = "Data Source= '" & Server & "' ;Initial Catalog= '" & Database & "' ;User Id= '" & Username & "';Password='" & Password & "' ; Connection Timeout=0;"
        sqlconn = New SqlConnection(cnstring)

    End Sub

    '-------- Encrypt the Data ---------'
    Public Shared Function GetEncryptedData(ByVal Data As String) As String

        Dim shaM As New SHA1Managed
        Convert.ToBase64String(shaM.ComputeHash(Encoding.ASCII.GetBytes(Data)))
        Dim eNC_data() As Byte = ASCIIEncoding.ASCII.GetBytes(Data)
        Dim eNC_str As String = Convert.ToBase64String(eNC_data)
        GetEncryptedData = eNC_str

    End Function

    '-------- Decrypt the Data ---------'

    Public Shared Function GetDecryptedData(ByVal Data As String) As String
        Dim dEC_data() As Byte = Convert.FromBase64String(Data)
        Dim dEC_Str As String = ASCIIEncoding.ASCII.GetString(dEC_data)
        GetDecryptedData = dEC_Str

    End Function

    Public Shared Function getuserrights(ByVal username As String, ByVal moduletype As String) As DataTable

        'Dim appRdr As New System.Configuration.AppSettingsReader
        'Dim myconnection As New Clsappconfiguration
        Dim myconnection As New SqlConnection(My.Settings.CNString)

        Dim cmd As New SqlCommand("usp_SYS_GetUserRights", myconnection)

        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Clear()
        cmd.Parameters.Add("fcUserName", SqlDbType.VarChar, 50, ParameterDirection.Input).Value = username
        cmd.Parameters.Add("fcModuleType", SqlDbType.VarChar, 50, ParameterDirection.Input).Value = moduletype

        myconnection.Open()
        cmd.ExecuteNonQuery()

        Try

            Dim myadapter As New SqlDataAdapter
            Dim DT As New DataTable
            Dim sqlDa As New SqlDataAdapter(cmd)
            sqlDa.SelectCommand = New SqlCommand("usp_SYS_GetUserRights", myconnection)

            myadapter.SelectCommand = cmd
            myadapter.Fill(DT)

            DT.TableName = "dbo.tSysUserRights"
            Return DT

        Finally

            myconnection.Close()

        End Try

    End Function
#Region "Update Personnel Basic Pay"
    Public Sub UploadEmployeeMaster(ByVal EmployeeID As String, ByVal basic_salary As String, Optional ByVal errmsg As String = "")
        errmsg = ""
        Try
            Dim myconnection As New Clsappconfiguration
            Dim cmd As New SqlCommand("UploadBasicPay", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            With cmd.Parameters
                .Add("@EmployeeID", SqlDbType.VarChar, 50).Value = EmployeeID
                .Add("@Basic_Salary", SqlDbType.VarChar, 50).Value = basic_salary
            End With
            myconnection.sqlconn.Open()
            cmd.ExecuteNonQuery()
            myconnection.sqlconn.Close()
            If errmsg <> "" Then
                Throw New Exception(errmsg)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Check Personnel Basic", MessageBoxButtons.OK)
        End Try
    End Sub
#End Region
#Region "Update Payroll Basic Pay"
    Public Sub UploadPayrollMaster(ByVal EmployeeID As String, ByVal basic_salary As String, Optional ByVal errmsg As String = "")
        errmsg = ""
        Try
            Dim myconnection As New Clsappconfiguration
            Dim cmd As New SqlCommand("UploadBasicPay_ToPayroll", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            With cmd.Parameters
                .Add("@EmployeeID", SqlDbType.VarChar, 50).Value = EmployeeID
                .Add("@Basic_Salary", SqlDbType.VarChar, 50).Value = basic_salary
            End With
            myconnection.sqlconn.Open()
            cmd.ExecuteNonQuery()
            myconnection.sqlconn.Close()

            If errmsg <> "" Then
                Throw New Exception(errmsg)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Check Payroll Basic", MessageBoxButtons.OK)
        End Try
    End Sub
#End Region
#Region "Upload Personnel ATM"
    Public Sub UploadATM(ByVal EmployeeID As String, ByVal ATM As String, Optional ByVal errmsg As String = "")
        errmsg = ""
        Try
            Dim myconnection As New Clsappconfiguration
            Dim cmd As New SqlCommand("UploadATM", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            With cmd.Parameters
                .Add("@EmployeeID", SqlDbType.VarChar, 50).Value = EmployeeID
                .Add("@ATM", SqlDbType.VarChar, 50).Value = ATM
            End With
            myconnection.sqlconn.Open()
            cmd.ExecuteNonQuery()
            myconnection.sqlconn.Close()

            If errmsg <> "" Then
                Throw New Exception(errmsg)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Check Personnel ATM", MessageBoxButtons.OK)
        End Try
    End Sub
#End Region
#Region "Update Payroll ATM"
    Public Sub UploadPayrollATM(ByVal EmployeeID As String, ByVal ATM As String, Optional ByVal errmsg As String = "")
        errmsg = ""
        Try
            Dim myconnection As New Clsappconfiguration
            Dim cmd As New SqlCommand("UploadATM_ToPayroll", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            With cmd.Parameters
                .Add("@EmployeeID", SqlDbType.VarChar, 50).Value = EmployeeID
                .Add("@ATM", SqlDbType.VarChar, 50).Value = ATM
            End With
            myconnection.sqlconn.Open()
            cmd.ExecuteNonQuery()
            myconnection.sqlconn.Close()

            If errmsg <> "" Then
                Throw New Exception(errmsg)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Check Payroll ATM", MessageBoxButtons.OK)
        End Try
    End Sub
#End Region
#Region "Update Tax Code"
    Public Sub UpdateTaxcode(ByVal EmployeeID As String, ByVal taxcode As String, Optional ByVal errmsg As String = "")
        errmsg = ""
        Try
            Dim myconnection As New Clsappconfiguration
            Dim cmd As New SqlCommand("uploadTaxcode", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            With cmd.Parameters
                .Add("@employeeid", SqlDbType.VarChar, 50).Value = EmployeeID
                .Add("@taxcode", SqlDbType.VarChar, 50).Value = taxcode
            End With
            myconnection.sqlconn.Open()
            cmd.ExecuteNonQuery()
            myconnection.sqlconn.Close()

            If errmsg <> "" Then
                Throw New Exception(errmsg)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Check Personnel Tax Code", MessageBoxButtons.OK)
        End Try
    End Sub
#End Region
#Region "Update tax code to payroll"
    Public Sub updateTaxCodePayroll(ByVal EmployeeID As String, ByVal taxcode As String, Optional ByVal errmsg As String = "")
        errmsg = ""
        Try
            Dim myconnection As New Clsappconfiguration
            Dim cmd As New SqlCommand("uploadfcTaxcode_ToPayroll", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            With cmd.Parameters
                .Add("@EmployeeID", SqlDbType.VarChar, 50).Value = EmployeeID
                .Add("@taxcode", SqlDbType.VarChar, 50).Value = taxcode
            End With
            myconnection.sqlconn.Open()
            cmd.ExecuteNonQuery()
            myconnection.sqlconn.Close()

            If errmsg <> "" Then
                Throw New Exception(errmsg)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Check Payroll Tax Code", MessageBoxButtons.OK)
        End Try
    End Sub
#End Region
#Region "Update Position to Personnel"
    Public Sub UpdatePositionPersonnel(ByVal EmployeeID As String, ByVal Position As String, Optional ByVal errmsg As String = "")
        errmsg = ""
        Try
            Dim myconnection As New Clsappconfiguration
            Dim cmd As New SqlCommand("uploadEmployeePosition", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            With cmd.Parameters
                .Add("@EmployeeID", SqlDbType.VarChar, 50).Value = EmployeeID
                .Add("@Position", SqlDbType.VarChar, 50).Value = Position
            End With
            myconnection.sqlconn.Open()
            cmd.ExecuteNonQuery()
            myconnection.sqlconn.Close()

            If errmsg <> "" Then
                Throw New Exception(errmsg)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Check Personnel Position", MessageBoxButtons.OK)
        End Try
    End Sub
#End Region
#Region "Update Personnel Department"
    Public Sub UpdatePersonnelDepartment(ByVal EmployeeID As String, ByVal department As String, Optional ByVal errmsg As String = "")
        errmsg = ""
        Try
            Dim myconnection As New Clsappconfiguration
            Dim cmd As New SqlCommand("uploadEmployeeDepartment", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            With cmd.Parameters
                .Add("@EmployeeID", SqlDbType.VarChar, 50).Value = EmployeeID
                .Add("@department", SqlDbType.VarChar, 50).Value = department
            End With
            myconnection.sqlconn.Open()
            cmd.ExecuteNonQuery()
            myconnection.sqlconn.Close()

            If errmsg <> "" Then
                Throw New Exception(errmsg)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Check Personnel Department", MessageBoxButtons.OK)
        End Try
    End Sub
#End Region
#Region "Update Payroll Department/fxkeysection"
    Public Sub updatepayrolldepartment(ByVal EmployeeID As String, ByVal department As String, Optional ByVal errmsg As String = "")
        errmsg = ""
        Try
            Dim myconnection As New Clsappconfiguration
            Dim cmd As New SqlCommand("uploadPayrollfxkeysection", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            With cmd.Parameters
                .Add("@EmployeeID", SqlDbType.VarChar, 50).Value = EmployeeID
                .Add("@department", SqlDbType.VarChar, 50).Value = department
            End With
            myconnection.sqlconn.Open()
            cmd.ExecuteNonQuery()
            myconnection.sqlconn.Close()

            If errmsg <> "" Then
                Throw New Exception(errmsg)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Check Payroll Department", MessageBoxButtons.OK)
        End Try

    End Sub
#End Region
#Region "Update Personnel jobgrade,hiringdate,regularizationdate,position,resignationdate,gender"
    Public Sub updatePersonnelJHRPRG(ByVal employeeid As String, ByVal jobgrade As String, ByVal hiringdate As Date, _
                                     ByVal regularizationdate As Date, ByVal position As String, ByVal resignedate As Date, ByVal gender As String, _
                                      Optional ByVal errmsg As String = "")
        errmsg = ""
        Try
            Dim mycon As New Clsappconfiguration
            Dim cmd As New SqlCommand("usp_personnel_7columnupload", mycon.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            With cmd.Parameters
                .Add("@employeeid", SqlDbType.VarChar).Value = employeeid
                .Add("@jobgrade", SqlDbType.VarChar).Value = jobgrade
                .Add("@datehired", SqlDbType.DateTime).Value = hiringdate
                .Add("@dateregular", SqlDbType.DateTime).Value = regularizationdate
                .Add("@position", SqlDbType.VarChar).Value = position
                .Add("@dateresigned", SqlDbType.DateTime).Value = resignedate
                .Add("@gender", SqlDbType.VarChar).Value = gender
            End With
            mycon.sqlconn.Open()
            cmd.ExecuteNonQuery()
            mycon.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Check Personnel JHRPRG", MessageBoxButtons.OK)
        End Try
    End Sub
#End Region
#Region "Update TKSID and Client ID"
    Public Sub UpdateTKClientID(ByVal EmpID As String, ByVal TKID As String, ByVal ClientID As String)
        Dim mycon As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_UpdateUploaded_TKClientID", mycon.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        Try
            With cmd.Parameters
                .Add("@IDNUMBER", SqlDbType.VarChar, 50).Value = RTrim(EmpID)
                .Add("@TKID", SqlDbType.VarChar, 30).Value = RTrim(TKID)
                .Add("@CLIENTID", SqlDbType.VarChar, 30).Value = RTrim(ClientID)
            End With
            mycon.sqlconn.Open()
            cmd.ExecuteNonQuery()
            mycon.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Check TKS ID and Client ID", MessageBoxButtons.OK)
        End Try
    End Sub
#End Region
#Region "Update Resigned Employee"
    Public Sub updateResignedActive(ByVal EmpID As String, ByVal PerActive As String, ByVal PayActive As String)
        Dim mycon As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_UpdateUploaded_ResignedtoActive", mycon.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        Try
            With cmd.Parameters
                .Add("@IDNUMBER", SqlDbType.VarChar, 50).Value = EmpID
                .Add("@Peractive", SqlDbType.VarChar, 3).Value = PerActive
                .Add("@Payactive", SqlDbType.VarChar, 3).Value = PayActive
            End With
            mycon.sqlconn.Open()
            cmd.ExecuteNonQuery()
            mycon.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Check Resigned to Active", MessageBoxButtons.OK)
        End Try

    End Sub
#End Region
End Class
