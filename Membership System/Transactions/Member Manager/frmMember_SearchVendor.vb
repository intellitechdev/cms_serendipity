﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmMember_SearchVendor

    Dim TaxClassification As String
    Dim TaxAgent As Boolean
    Dim TaxCategory As Boolean

    Private Sub GetVendorInfo(ByVal ClientID As String)
        Dim gcon As New Clsappconfiguration()
        Dim ds As New DataSet

        Try
            gridExistingVendor.Columns.Clear()
            ds = SqlHelper.ExecuteDataset(gcon.cnstring, CommandType.StoredProcedure, "MSS_MembersInfo_GetVendor", _
                                New SqlParameter("@fcVendorNo", ClientID))
            gridExistingVendor.DataSource = ds.Tables(0)
            GridFormat()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub GridFormat()
        With gridExistingVendor
            .Columns(0).Width = 100
            .Columns(1).Width = 300
            .Columns(2).Visible = False
            .Columns(3).Visible = False
            .Columns(4).Visible = False
            .Columns(5).Visible = False
            .Columns(6).Visible = False
            .Columns(7).Visible = False
            .Columns(8).Visible = False
            .Columns(9).Visible = False
            .Columns(10).Visible = False
            .Columns(11).Visible = False
            .Columns(12).Visible = False
            .Columns(13).Visible = False
            .Columns(14).Visible = False
            .Columns(15).Visible = False
            .Columns(16).Visible = False
            .Columns(17).Visible = False
            .Columns(18).Visible = False
            .Columns(19).Visible = False
            .Columns(20).Visible = False
            .Columns(21).Visible = False
            .Columns(22).Visible = False
            .Columns(23).Visible = False
            .Columns(24).Visible = False
        End With
    End Sub

    Private Sub btnemp_close_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnemp_close.Click
        gridExistingVendor.Columns.Clear()
        Me.Close()
    End Sub

    Private Sub frmMember_SearchVendor_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        GetVendorInfo("")
    End Sub

    Private Sub txtid_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtid.TextChanged
        GetVendorInfo(txtid.Text)
    End Sub

    Private Sub btnfind_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnfind.Click
        GetVendorInfo(txtid.Text)
    End Sub

    Private Sub SelectVendor()
        With frmMember_Vendors
            .txtID.Text = gridExistingVendor.CurrentRow.Cells(0).Value
            .txtName.Text = gridExistingVendor.CurrentRow.Cells(1).Value
            .txtPurchase.Text = gridExistingVendor.CurrentRow.Cells(2).Value
            .txtAddress.Text = gridExistingVendor.CurrentRow.Cells(3).Value
            .txtContactPerson.Text = gridExistingVendor.CurrentRow.Cells(4).Value
            .txtTerms.Text = gridExistingVendor.CurrentRow.Cells(5).Value
            .txtMobNo.Text = gridExistingVendor.CurrentRow.Cells(6).Value
            .txtTelephone.Text = gridExistingVendor.CurrentRow.Cells(7).Value
            .txtTIN.Text = gridExistingVendor.CurrentRow.Cells(8).Value
            .txtFAX.Text = gridExistingVendor.CurrentRow.Cells(9).Value
            .txtEmail.Text = gridExistingVendor.CurrentRow.Cells(10).Value
            .txtPosition.Text = gridExistingVendor.CurrentRow.Cells(11).Value
            .dtBIRVerify.Text = gridExistingVendor.CurrentRow.Cells(12).Value
            .txtCategory.Text = gridExistingVendor.CurrentRow.Cells(13).Value
            .txtBusiness.Text = gridExistingVendor.CurrentRow.Cells(14).Value
            .txtRemarks.Text = gridExistingVendor.CurrentRow.Cells(15).Value
            .txtPreparedBy.Text = gridExistingVendor.CurrentRow.Cells(16).Value
            .dtPrepared.Text = gridExistingVendor.CurrentRow.Cells(17).Value
            .txtEncodeby.Text = gridExistingVendor.CurrentRow.Cells(18).Value
            .dtEncode.Text = gridExistingVendor.CurrentRow.Cells(19).Value
            TaxClassification = gridExistingVendor.CurrentRow.Cells(20).Value
            If TaxClassification = "Government" Then
                .rbGovernment.Checked = True
            ElseIf TaxClassification = "Vatable" Then
                .rbVatable.Checked = True
            ElseIf TaxClassification = "Zero-Rated" Then
                .rbZeroRated.Checked = True
            End If
            TaxAgent = gridExistingVendor.CurrentRow.Cells(21).Value
            If TaxAgent = True Then
                .rbYes.Checked = True
            Else
                .rbNo.Checked = True
            End If
            .txtPercent.Text = gridExistingVendor.CurrentRow.Cells(22).Value
            TaxCategory = gridExistingVendor.CurrentRow.Cells(23).Value
            If TaxCategory = True Then
                .chkCategory.Checked = True
            Else
                .chkCategory.Checked = False
            End If
            .txtTAXon.Text = gridExistingVendor.CurrentRow.Cells(24).Value
            .GetVendorrAttach(frmMember_Vendors.txtID.Text)
        End With
    End Sub

    Private Sub btnok_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnok.Click
        SelectVendor()
        txtid.Text = ""
        Me.Close()
    End Sub
End Class