﻿Imports System.Data.Sql
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO

Public Class frmMember_Requirements

    Dim pkReq As String
    Dim Reqpath As String
    Dim Reqfilenym As String
    Dim ReqfileData As Byte()
    Dim ex As String
    Dim Reqfiles As String

#Region "Property"
    Public Property getpkRequirements() As String
        Get
            Return pkReq
        End Get
        Set(ByVal value As String)
            pkReq = value
        End Set
    End Property
#End Region

    Private Sub AddEditReq(ByVal EmpNo As String, ByVal AttachedName As String, ByVal dtReceive As Date, ByVal AttachedFiles As Byte(), _
                            ByVal ReqType As String, ByVal ReceiveBy As String, ByVal pk_Req As String)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Member_ReqAttached_AddEdit", _
                                     New SqlParameter("@employeeNo", EmpNo), _
                                     New SqlParameter("@FileName", AttachedName), _
                                     New SqlParameter("@DateAttached", dtReceive), _
                                     New SqlParameter("@AttachedFiles", AttachedFiles), _
                                     New SqlParameter("@ReqType", ReqType), _
                                     New SqlParameter("@ReceiveBy", ReceiveBy), _
                                     New SqlParameter("@pk_Attach", pk_Req))
            trans.Commit()
            MessageBox.Show("Record has been Saved", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "Save Requirements")
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub

    Private Sub btnAttach_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAttach.Click
        Dim openFileDialog1 As New OpenFileDialog()
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()

        Try
            openFileDialog1.Filter = "Text Files (*.txt)|*.txt|PDF Files (*.pdf)|*.pdf|Word Documents (*.docx)|*.docx|Excel Worksheets (*.xlsx)|*.xlsx|PowerPoint Presentations (*.pptx)|*.pptx|Word Documents 97-2003 (*.doc)|*.docx|Excel Worksheets 93-2003 (*.xls)|*.xls|PowerPoint Presentations (*.ppt)|*.ppt" & "|Office Files|*.docx;*.xlsx;*.pptx;*.doc;*.xls;*.ppt" & "|Images|*.jpg;*.png;*.gif"
            openFileDialog1.Title = "Select File"
            If openFileDialog1.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
                Reqfilenym = openFileDialog1.FileName
                Reqpath = Path.GetExtension(Reqfilenym)
                Reqfiles = Path.GetFileName(Reqfilenym)

                ReqfileData = File.ReadAllBytes(Reqfilenym)
                ex = Path.GetExtension(LTrim(RTrim(Reqfilenym)))

                Dim ms As New MemoryStream(ReqfileData, 0, ReqfileData.Length)

                ms.Write(ReqfileData, 0, ReqfileData.Length)
                txtAttached.Text = Reqfiles
            Else
                MessageBox.Show("User Cancelled!", "Cancelled", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "Attach Requirements")
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If txtAttached.Text = "" Or cboReqType.Text = "" Or txtReceive.Text = "" Then
            MessageBox.Show("Complete Information is Needed", "Requirement Information", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        Else
            Call AddEditReq(frmMember_Master.txtEmployeeNo.Text, txtAttached.Text, dtReceive.Value, ReqfileData, cboReqType.Text, txtReceive.Text, "")
            Call frmMember_Master.GetmemberAttachReq(frmMember_Master.txtEmployeeNo.Text)
            ClearText()
            'Me.Close()
        End If
    End Sub

    Private Sub ClearText()
        txtAttached.Text = ""
        txtReceive.Text = ""
        cboReqType.Text = ""
    End Sub

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        Me.Close()
    End Sub
End Class