﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmAddItem

    Private con As New Clsappconfiguration

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub _LoadAccts(ByVal comp As String)
        Try
            dgvItems.DataSource = Nothing
            dgvItems.Columns.Clear()
            dgvItems.Rows.Clear()
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(con.cnstring, "_LOad_AddItem_Accounts_ByCompany",
                                           New SqlParameter("@ProjDesc", comp),
                                           New SqlParameter("@Search", txtSearch.Text))
            dgvItems.DataSource = ds.Tables(0)
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub frmAddItem_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        _LoadAcctg_Proj()
    End Sub

    Private Sub _LoadAcctg_Proj()
        Dim mycon As New Clsappconfiguration
        Try
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(mycon.cnstring, "_Load_Project")
            cboProject.Items.Clear()
            While rd.Read
                With cboProject
                    .Items.Add(rd(0).ToString)
                End With
            End While
            cboProject.Text = "Choose Company"
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub cboProject_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboProject.TextChanged
        '_LoadAccts(cboProject.Text)
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        If cboView.Text = "Chart of Accounts" Then
            _LoadAccts(cboProject.Text)
            Exit Sub
        End If
        If cboView.Text = "Account Register" Then
            _GetReferences()
            Exit Sub
        End If
    End Sub

    Private Sub btnSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelect.Click
        If cboView.Text = "Chart of Accounts" Then
            frmChkVoucherEntry.AddItems(" ", " ", " ", " ", dgvItems.CurrentRow.Cells(0).Value.ToString, dgvItems.CurrentRow.Cells(1).Value.ToString, "0.00")
            Me.Close()
        End If
        If cboView.Text = "Account Register" Then
            frmChkVoucherEntry.AddItems(dgvItems.CurrentRow.Cells(0).Value.ToString, dgvItems.CurrentRow.Cells(1).Value.ToString, " ", dgvItems.CurrentRow.Cells(2).Value.ToString, dgvItems.CurrentRow.Cells(3).Value.ToString, dgvItems.CurrentRow.Cells(4).Value.ToString, "0.00")
            Me.Close()
        End If
    End Sub

    Private Sub _GetReferences()
        Try
            dgvItems.DataSource = Nothing
            dgvItems.Columns.Clear()
            dgvItems.Rows.Clear()
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(con.cnstring, "_Select_AcctRegister_Release",
                                           New SqlParameter("@Search", txtSearch.Text))
            dgvItems.DataSource = ds.Tables(0)
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub cboView_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboView.TextChanged
        If cboView.Text = "Chart of Accounts" Then
            _LoadAccts(cboProject.Text)
            Exit Sub
        End If
        If cboView.Text = "Account Register" Then
            _GetReferences()
            Exit Sub
        End If
    End Sub

End Class