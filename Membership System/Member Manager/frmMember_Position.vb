Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmMember_Position
#Region "Variables"
    Public fxkeyposition As String
    Public positionid As String
#End Region
#Region "Get property"
    Public Property getfexkeyemployee() As String
        Get
            Return fxkeyposition
        End Get
        Set(ByVal value As String)
            fxkeyposition = value
        End Set
    End Property
    Public Property getpositionid() As String
        Get
            Return positionid
        End Get
        Set(ByVal value As String)
            positionid = value
        End Set
    End Property
#End Region
    Public myconnection As New Clsappconfiguration
    Private Sub Applicant_Position_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call Position()

    End Sub

    Private Sub btnsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsave.Click
        Try
            If btnsave.Text = "New" Then
                Me.txtcode.Focus()
                Call cleartxt()
                Label1.Text = "Add New Position"
                btndelete.Visible = False
                btnupdate.Visible = False
                btnsave.Text = "Save"
                btnsave.Image = My.Resources.save2
                btnclose.Text = "Cancel"
                btnclose.Location = New System.Drawing.Point(74, 6)

            ElseIf btnsave.Text = "Save" Then
                If Me.txtcode.Text <> "" And Me.txtdescription.Text <> "" Then
                    Label1.Text = "Position"
                    Call Validatecode()
                    Call cleartxt()
                    txtcode.Focus()
                    btnsave.Text = "New"
                    btnsave.Image = My.Resources.new3
                    btnclose.Text = "Close"
                    btnclose.Location = New System.Drawing.Point(218, 6)
                    btndelete.Visible = True
                    btnupdate.Visible = True
                Else
                    MessageBox.Show("Empty field found! ", "Save", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub

#Region "Employee Position get/stored proc"
    Public Sub Position()

        Dim myconnection As New Clsappconfiguration


        Dim cmd As New SqlCommand("CIMS_m_Employment_Position_Select", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Connection = myconnection.sqlconn
        myconnection.sqlconn.Open()


        With Me.lvlEmployeePosition
            .Clear()
            .View = View.Details
            .GridLines = True
            .FullRowSelect = True
            .Columns.Add("Position Code", 100, HorizontalAlignment.Left)
            .Columns.Add("Position Description", 300, HorizontalAlignment.Left)

            Dim myreader As SqlDataReader
            myreader = cmd.ExecuteReader
            Try
                While myreader.Read
                    With .Items.Add(myreader.Item(0))
                        .subitems.add(myreader.Item(1))
                        .subitems.add(myreader.Item(2))
                    End With
                End While
            Finally
                myreader.Close()
                myconnection.sqlconn.Close()
            End Try
        End With

    End Sub
#End Region
    Public Sub cleartxt()
        Me.txtcode.Text = ""
        Me.txtdescription.Text = ""
    End Sub
#Region "Employee Position Payroll Update/stored proc"
    Public Sub Employee_position_payroll_tk_update()
        Try
            Dim myconnection As New Clsappconfiguration
            Dim cmd As New SqlCommand("usp_employee_position_update", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            With cmd.Parameters
                .Add("@fxKeyPositionLevel", SqlDbType.Char, 4).Value = fxkeyposition
                .Add("@fcPositionCode", SqlDbType.VarChar, 12).Value = Me.txtcode.Text
                .Add("@fcPositionName", SqlDbType.VarChar, 60).Value = Me.txtdescription.Text
            End With
            myconnection.sqlconn.Open()
            cmd.ExecuteNonQuery()
            myconnection.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Check", MessageBoxButtons.OK)
        End Try
    End Sub
#End Region
#Region "Employee Position Payroll Delete/stored proc"
    Public Sub Employee_position_payroll_delete(ByVal keypostion As String)
        Try
            Dim myconnection As New Clsappconfiguration
            Dim cmd As New SqlCommand("usp_per_employee_position_level_delete", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@fxKeyPositionLevel", SqlDbType.Char, 4).Value = keypostion
            myconnection.sqlconn.Open()
            cmd.ExecuteNonQuery()
            myconnection.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Check", MessageBoxButtons.OK)
        End Try
    End Sub
#End Region
    Private Sub btnupdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupdate.Click
        Try
            If btnupdate.Text = "Edit" Then
                Me.txtcode.Focus()
                Label1.Text = "Edit Position"
                Me.txtcode.Text = Me.lvlEmployeePosition.SelectedItems(0).Text
                Me.txtcode.Tag = Me.lvlEmployeePosition.SelectedItems(0).Text
                Me.txtdescription.Text = Me.lvlEmployeePosition.SelectedItems(0).SubItems(1).Text
                Me.txtdescription.Tag = Me.lvlEmployeePosition.SelectedItems(0).SubItems(1).Text


                getpositionid() = Me.lvlEmployeePosition.SelectedItems(0).SubItems(2).Text 'posiitionid
                getfexkeyemployee = Me.lvlEmployeePosition.SelectedItems(0).SubItems(2).Text 'fxkeyposition

                Me.btnsave.Enabled = False
                btnupdate.Text = "Update"
                btnupdate.Image = My.Resources.save2
                btnclose.Text = "Cancel"
                btnclose.Location = New System.Drawing.Point(141, 6)
                btndelete.Visible = False

            ElseIf btnupdate.Text = "Update" Then
                If Me.txtcode.Text <> "" And Me.txtdescription.Text <> "" Then
                    Label1.Text = "Position"
                    Call ValidateCodeUpdate()
                    Call cleartxt()
                    btndelete.Visible = True
                    btnupdate.Text = "Edit"
                    btnupdate.Image = My.Resources.edit1
                    btnclose.Text = "Cancel"
                    btnclose.Location = New System.Drawing.Point(218, 6)
                    btnclose.Text = "Close"
                    Me.btnsave.Enabled = True
                    Me.btndelete.Visible = True

                Else
                    MessageBox.Show("Empty field found! ", "Edit", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            End If

        Catch ex As Exception
        End Try
    End Sub
#Region "Update Member Position"
    Private Sub EditMemberPosition(ByVal code As String, ByVal desc As String, ByVal posid As String)

        Dim mycon As New Clsappconfiguration
        mycon.sqlconn.Open()
        Dim trans As SqlTransaction = mycon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_m_Employment_Position_Update", _
                                      New SqlParameter("@fcPositionCode", code), _
                                      New SqlParameter("@fcPositionDescription", desc), _
                                      New SqlParameter("@pk_Position", posid))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "EditMemberPosition")
        Finally
            mycon.sqlconn.Close()
        End Try

    End Sub
#End Region

    Private Sub btndelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndelete.Click
        Try
            getpositionid() = Me.lvlEmployeePosition.SelectedItems(0).SubItems(2).Text
            getfexkeyemployee = Me.lvlEmployeePosition.SelectedItems(0).SubItems(2).Text
            Dim x As New DialogResult

            x = MessageBox.Show("Are you sure you want to permanently delete this record?", "Confirm Deletion", MessageBoxButtons.OKCancel, MessageBoxIcon.Information)
            If x = System.Windows.Forms.DialogResult.OK Then
                Call DeleteMemberPosition(positionid)
                Call Position()
                Call cleartxt()
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
#Region "Delete Member Position"
    Private Sub DeleteMemberPosition(ByVal posid As String)
        Dim mycon As New Clsappconfiguration
        Try
            SqlHelper.ExecuteNonQuery(mycon.cnstring, CommandType.StoredProcedure, "CIMS_m_Employment_Position_Delete", _
                                      New SqlParameter("@pk_Position", posid))
            MessageBox.Show("Successfully Deleted!", "Position Masterfile", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            MessageBox.Show("Cannot delete position. This position is currently used by a member.", "DeleteMemberPosition", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

#End Region

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        If btnclose.Text = "Cancel" Then
            Label1.Text = "Position"
            btnsave.Text = "New"
            btnsave.Image = My.Resources.new3
            btnupdate.Text = "Edit"
            btnupdate.Image = My.Resources.edit1
            btnclose.Text = "Close"
            btnclose.Location = New System.Drawing.Point(218, 6)
            btndelete.Visible = True
            btnupdate.Visible = True
            btnsave.Enabled = True
        ElseIf btnclose.Text = "Close" Then
            Me.Close()
        End If
    End Sub

    Private Sub cbojobrank_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        e.KeyChar = Microsoft.VisualBasic.ChrW(0)
    End Sub
#Region "Employee Position Insert/stored proc"
    Public Sub Validatecode()

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(myconnection.cnstring, CommandType.StoredProcedure, "CIMS_m_Employment_Position_Validate", _
                                        New SqlParameter("@code", Me.txtcode.Text), _
                                        New SqlParameter("@id", positionid))
                If rd.HasRows Then
                    MessageBox.Show("Code already exist, please try another one.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
                Else
                    Call AddNewPosition(Me.txtcode.Text, Me.txtdescription.Text)
                    Call Position()
                End If
            End Using
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Position", MessageBoxButtons.OK)
        End Try

    End Sub
#End Region
#Region "Validating code in update"
    Private Sub ValidateCodeUpdate()
        Dim myconnection As New Clsappconfiguration

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(myconnection.cnstring, CommandType.StoredProcedure, "CIMS_m_Employment_Position_Validate", _
                    New SqlParameter("@code", Me.txtcode.Text), _
                    New SqlParameter("@id", positionid))
                If rd.HasRows Then
                    MessageBox.Show("Code already exist, please try another one.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
                Else
                    Call EditMemberPosition(Me.txtcode.Text, Me.txtdescription.Text, positionid)
                    Call Position()
                End If
            End Using
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Position", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try

    End Sub
#End Region
#Region "Add New Member Position"
    Private Sub AddNewPosition(ByVal code As String, ByVal desc As String)
        Dim mycon As New Clsappconfiguration
        mycon.sqlconn.Open()
        Dim trans As SqlTransaction = mycon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_m_Employment_Position_Insert", _
                                      New SqlParameter("@fcPositionCode", code), _
                                      New SqlParameter("@fcPositionDescription", desc))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "AddNewPosition")
        Finally
            mycon.sqlconn.Close()
        End Try
    End Sub
#End Region
    Public Sub check_insertposition_validation()

        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("USP_CHECK_JOBRANK_withparameters", myconnection.sqlconn)
        myconnection.sqlconn.Open()
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@description", SqlDbType.VarChar, 80).Value = Me.txtdescription.Text
        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader()
        Try
            If myreader.Read Then
                MessageBox.Show("That Position Title" + " ' " + Me.txtdescription.Text + " ' " + "is already used , Please try another one", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
            Else

            End If
        Catch ex As Exception
        End Try
    End Sub


    Private Sub txtdescription_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtdescription.KeyPress
        e.Handled = Not alphanumeric_Validate(e.KeyChar)
    End Sub


    Private Sub cbojobrank_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub lvlEmployeePosition_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvlEmployeePosition.SelectedIndexChanged

    End Sub

    Private Sub txtdescription_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtdescription.TextChanged
        Me.txtdescription.CharacterCasing = CharacterCasing.Upper
    End Sub

    Private Sub txtcode_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtcode.TextChanged
        Me.txtcode.CharacterCasing = CharacterCasing.Upper
    End Sub
End Class