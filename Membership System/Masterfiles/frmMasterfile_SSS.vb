Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmMasterfile_SSS
#Region "Var"
    Private SSSbracketno As Integer
#End Region
#Region "VarProperty"
    Private Property getSSSbracketno() As Integer
        Get
            Return SSSbracketno
        End Get
        Set(ByVal value As Integer)
            SSSbracketno = value
        End Set
    End Property
#End Region
#Region "Function"
    Public Function Numbers(ByVal C As Char) As Boolean
        If Not (Char.IsDigit(C)) And Not (C = ".") And Not (Microsoft.VisualBasic.AscW(C) = 8) And Not (Microsoft.VisualBasic.AscW(C) = 13) Then
            MsgBox("Invalid Input! This field allows 0-9 only.", MsgBoxStyle.Exclamation, "Invalid")
            Return False
        Else
            Return True
        End If
    End Function
#End Region
#Region "AddEdit ContributionSSS"
    Private Sub AddEditContributionSSS(ByVal salaryfrom As Decimal, ByVal salaryto As Decimal, ByVal employeer As Decimal, ByVal sssbracketno As Integer)
        Dim mycon As New Clsappconfiguration
        mycon.sqlconn.Open()
        Dim trans As SqlTransaction = mycon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Contribution_SSS_AddEdit", _
                                      New SqlParameter("@fnSalaryRangeFrom", salaryfrom), _
                                      New SqlParameter("@fnSalaryRangeTo", salaryto), _
                                      New SqlParameter("@fnEmployeeSSS", employeer), _
                                      New SqlParameter("@fnSSBracketNo", sssbracketno))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "AddEditContributionSSS")
        Finally
            mycon.sqlconn.Close()
        End Try

    End Sub
#End Region
#Region "Delete ContributionSSS"
    Private Sub DeleteContributionSSS(ByVal sssbracketno As Integer)
        Dim mycon As New Clsappconfiguration
        mycon.sqlconn.Open()
        Dim trans As SqlTransaction = mycon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Contribution_SSS_Delete", _
                                      New SqlParameter("@fnSSBracketNo", sssbracketno))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "DeleteContributionSSS")
        Finally
            mycon.sqlconn.Close()
        End Try
    End Sub
#End Region
#Region "View ContributionSSS"
    Private Sub ViewContributionSSS()
        Dim mycon As New Clsappconfiguration
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "CIMS_Contribution_SSS_Select")
        With Me.listContribution
            .Clear()
            .View = View.Details
            .GridLines = True
            .FullRowSelect = True
            .Columns.Add("Salary From", 100, HorizontalAlignment.Left)
            .Columns.Add("Salary To", 100, HorizontalAlignment.Left)
            .Columns.Add("Employeer", 300, HorizontalAlignment.Left)
            Try
                While rd.Read
                    With .Items.Add(rd.Item(0))
                        .SubItems.Add(rd.Item(1))
                        .SubItems.Add(rd.Item(2))
                        .SubItems.Add(rd.Item(3))
                    End With
                End While
                rd.Close()
            Catch ex As Exception

            End Try
        End With
    End Sub
#End Region
#Region "Cleartext"
    Private Sub Cleartext()
        Me.txtSalaryfrom.Text = "0.00"
        Me.txtSalaryTo.Text = "0.00"
        Me.txtemployeer.Text = "0.00"
    End Sub
#End Region
    Private Sub frmContribution_SSS_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call ViewContributionSSS()
    End Sub

    Private Sub btnsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsave.Click
        Try
            If btnsave.Text = "New" Then
                Call Cleartext()
                Label1.Text = "Add SSS Contribution"
                btndelete.Visible = False
                btnupdate.Visible = False
                btnsave.Text = "Save"
                btnsave.Image = My.Resources.save2
                btnclose.Text = "Cancel"
                btnclose.Location = New System.Drawing.Point(86, 7)

            ElseIf btnsave.Text = "Save" Then
                If Me.txtSalaryfrom.Text <> "" And Me.txtSalaryTo.Text <> "" And Me.txtemployeer.Text <> "" Then
                    Label1.Text = "SSS Contribution"
                    Call AddEditContributionSSS(Decimal.Parse(Me.txtSalaryfrom.Text), Decimal.Parse(Me.txtSalaryTo.Text), Decimal.Parse(Me.txtemployeer.Text), 0)
                    Call ViewContributionSSS()
                    Call Cleartext()
                    btnsave.Text = "New"
                    btnsave.Image = My.Resources.new3
                    btnclose.Text = "Close"
                    btnclose.Location = New System.Drawing.Point(218, 6)
                    btndelete.Visible = True
                    btnupdate.Visible = True
                Else
                    MessageBox.Show("Empty field found! ", "Save", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub btnupdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupdate.Click
        Try
            If btnupdate.Text = "Edit" Then
                Label1.Text = "Edit SSS Contribution"
                Me.txtSalaryfrom.Text = Me.listContribution.SelectedItems(0).Text
                Me.txtSalaryTo.Text = Me.listContribution.SelectedItems(0).SubItems(1).Text
                Me.txtemployeer.Text = Me.listContribution.SelectedItems(0).SubItems(2).Text
                getSSSbracketno() = Me.listContribution.SelectedItems(0).SubItems(3).Text
                Me.btnsave.Enabled = False
                btnupdate.Text = "Update"
                btnupdate.Image = My.Resources.save2
                btnclose.Text = "Cancel"
                btnclose.Location = New System.Drawing.Point(164, 7)
                btndelete.Visible = False

            ElseIf btnupdate.Text = "Update" Then
                If Me.txtSalaryfrom.Text <> "" And Me.txtSalaryTo.Text <> "" And Me.txtemployeer.Text <> "" Then
                    Label1.Text = "SSS Contribution"
                    Call AddEditContributionSSS(Decimal.Parse(Me.txtSalaryfrom.Text), Decimal.Parse(Me.txtSalaryTo.Text), Decimal.Parse(Me.txtemployeer.Text), SSSbracketno)
                    Call ViewContributionSSS()
                    Call Cleartext()
                    btndelete.Visible = True
                    btnupdate.Text = "Edit"
                    btnupdate.Image = My.Resources.edit1
                    btnclose.Text = "Cancel"
                    btnclose.Location = New System.Drawing.Point(218, 6)
                    btnclose.Text = "Close"
                    Me.btnsave.Enabled = True
                    Me.btndelete.Visible = True

                Else
                    MessageBox.Show("Empty field found! ", "Edit", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            End If

        Catch ex As Exception
        End Try
    End Sub

    Private Sub btndelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndelete.Click
        Try
            getSSSbracketno() = Me.listContribution.SelectedItems(0).SubItems(3).Text
            Dim x As New DialogResult
            x = MessageBox.Show("Are you sure you want to permanently delete this record?", "Confirm Deletion", MessageBoxButtons.OKCancel, MessageBoxIcon.Information)
            If x = System.Windows.Forms.DialogResult.OK Then
                Call DeleteContributionSSS(SSSbracketno)
                Call ViewContributionSSS()
                Call Cleartext()
            ElseIf x = System.Windows.Forms.DialogResult.Cancel Then
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub txtSalaryfrom_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtSalaryfrom.KeyPress
        e.Handled = Not Numbers(e.KeyChar)
    End Sub

    Private Sub txtSalaryTo_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtSalaryTo.KeyPress
        e.Handled = Not Numbers(e.KeyChar)
    End Sub

    Private Sub txtemployeer_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtemployeer.KeyPress
        e.Handled = Not Numbers(e.KeyChar)
    End Sub

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        If btnclose.Text = "Cancel" Then
            Label1.Text = "SSS Contribution"
            btnsave.Text = "New"
            btnsave.Image = My.Resources.new3
            btnupdate.Text = "Edit"
            btnupdate.Image = My.Resources.edit1
            btnclose.Text = "Close"
            btnclose.Location = New System.Drawing.Point(242, 7)
            btndelete.Visible = True
            btnupdate.Visible = True
            btnsave.Enabled = True
        ElseIf btnclose.Text = "Close" Then
            Me.Close()
        End If
    End Sub
End Class