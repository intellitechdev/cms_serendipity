Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class clsDatabaseFunctions
    Private gcon As New Clsappconfiguration

    Public Sub BackupDatabase(ByVal dbName As String, ByVal path As String, ByVal user As String)
        Try
            ModifiedSqlHelper.ExecuteNonQuery(gcon.cnstring, CommandType.StoredProcedure, "CIMS_Backup_Database", _
                New SqlParameter("@dbName", dbName), _
                New SqlParameter("@backupFilePath", path), _
                New SqlParameter("@user", user))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub RestoreDatabase(ByVal dbName As String, ByVal path As String, ByVal user As String)
        Try

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class
