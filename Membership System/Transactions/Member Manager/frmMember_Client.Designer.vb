﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMember_Client
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMember_Client))
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtClientName = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtTIN = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtContactPerson = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtClientNo = New System.Windows.Forms.TextBox()
        Me.cboMemStatus = New System.Windows.Forms.ComboBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.btnMaxPhoto = New System.Windows.Forms.Button()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.btnBrowsePicture = New System.Windows.Forms.Button()
        Me.picClientPhoto = New System.Windows.Forms.PictureBox()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.cboProjectSpecialist = New System.Windows.Forms.ComboBox()
        Me.txtPosHeadcount = New System.Windows.Forms.TextBox()
        Me.txtProjHeadcount = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.btnContract = New System.Windows.Forms.Button()
        Me.btnEmployeeList = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.dtPayRelease = New System.Windows.Forms.DateTimePicker()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.dtPayroll = New System.Windows.Forms.DateTimePicker()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.dtPayPeriod = New System.Windows.Forms.DateTimePicker()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.dtPayrollPeriod = New System.Windows.Forms.DateTimePicker()
        Me.dtBilling = New System.Windows.Forms.DateTimePicker()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.dtCollection = New System.Windows.Forms.DateTimePicker()
        Me.txtDescription = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtTelNo = New System.Windows.Forms.TextBox()
        Me.txtAddress = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.btnemp_close = New System.Windows.Forms.Button()
        Me.btnemp_next = New System.Windows.Forms.Button()
        Me.btnemp_delete = New System.Windows.Forms.Button()
        Me.BTNSEARCH = New System.Windows.Forms.Button()
        Me.btnemp_edit = New System.Windows.Forms.Button()
        Me.btnemp_previous = New System.Windows.Forms.Button()
        Me.btnemp_add = New System.Windows.Forms.Button()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.lvlAttach = New System.Windows.Forms.ListView()
        Me.btnAttach = New System.Windows.Forms.Button()
        Me.btnUpload = New System.Windows.Forms.Button()
        Me.txtAttachment = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        Me.PanePanel2 = New WindowsApplication2.PanePanel()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.btnDelete = New System.Windows.Forms.Button()
        Me.btnSearchEmp = New System.Windows.Forms.Button()
        Me.btnEdit = New System.Windows.Forms.Button()
        Me.btnAdd = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.picClientPhoto, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.PanePanel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(7, 19)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(79, 15)
        Me.Label3.TabIndex = 13
        Me.Label3.Text = "Client ID No.:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(6, 43)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(79, 15)
        Me.Label2.TabIndex = 87
        Me.Label2.Text = "Client Name:"
        '
        'txtClientName
        '
        Me.txtClientName.BackColor = System.Drawing.SystemColors.Window
        Me.txtClientName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtClientName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtClientName.Enabled = False
        Me.txtClientName.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtClientName.Location = New System.Drawing.Point(108, 41)
        Me.txtClientName.MaxLength = 50
        Me.txtClientName.Name = "txtClientName"
        Me.txtClientName.Size = New System.Drawing.Size(460, 21)
        Me.txtClientName.TabIndex = 10
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(356, 118)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(51, 15)
        Me.Label4.TabIndex = 89
        Me.Label4.Text = "TIN No.:"
        '
        'txtTIN
        '
        Me.txtTIN.BackColor = System.Drawing.SystemColors.Window
        Me.txtTIN.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtTIN.Enabled = False
        Me.txtTIN.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTIN.Location = New System.Drawing.Point(415, 116)
        Me.txtTIN.MaxLength = 50
        Me.txtTIN.Name = "txtTIN"
        Me.txtTIN.Size = New System.Drawing.Size(153, 21)
        Me.txtTIN.TabIndex = 15
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(6, 69)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(94, 15)
        Me.Label5.TabIndex = 91
        Me.Label5.Text = "Contact Person:"
        '
        'txtContactPerson
        '
        Me.txtContactPerson.BackColor = System.Drawing.SystemColors.Window
        Me.txtContactPerson.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtContactPerson.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtContactPerson.Enabled = False
        Me.txtContactPerson.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtContactPerson.Location = New System.Drawing.Point(108, 66)
        Me.txtContactPerson.MaxLength = 50
        Me.txtContactPerson.Name = "txtContactPerson"
        Me.txtContactPerson.Size = New System.Drawing.Size(228, 21)
        Me.txtContactPerson.TabIndex = 11
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.txtClientNo)
        Me.GroupBox1.Controls.Add(Me.cboMemStatus)
        Me.GroupBox1.Controls.Add(Me.Label18)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.GroupBox3)
        Me.GroupBox1.Controls.Add(Me.Panel3)
        Me.GroupBox1.Controls.Add(Me.Label16)
        Me.GroupBox1.Controls.Add(Me.cboProjectSpecialist)
        Me.GroupBox1.Controls.Add(Me.txtPosHeadcount)
        Me.GroupBox1.Controls.Add(Me.txtProjHeadcount)
        Me.GroupBox1.Controls.Add(Me.Label17)
        Me.GroupBox1.Controls.Add(Me.Label15)
        Me.GroupBox1.Controls.Add(Me.btnContract)
        Me.GroupBox1.Controls.Add(Me.btnEmployeeList)
        Me.GroupBox1.Controls.Add(Me.GroupBox2)
        Me.GroupBox1.Controls.Add(Me.txtDescription)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.txtTelNo)
        Me.GroupBox1.Controls.Add(Me.txtAddress)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.txtClientName)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txtContactPerson)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.txtTIN)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(835, 294)
        Me.GroupBox1.TabIndex = 93
        Me.GroupBox1.TabStop = False
        '
        'txtClientNo
        '
        Me.txtClientNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtClientNo.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtClientNo.Location = New System.Drawing.Point(108, 17)
        Me.txtClientNo.Name = "txtClientNo"
        Me.txtClientNo.Size = New System.Drawing.Size(158, 21)
        Me.txtClientNo.TabIndex = 8
        '
        'cboMemStatus
        '
        Me.cboMemStatus.FormattingEnabled = True
        Me.cboMemStatus.Location = New System.Drawing.Point(417, 17)
        Me.cboMemStatus.Name = "cboMemStatus"
        Me.cboMemStatus.Size = New System.Drawing.Size(141, 21)
        Me.cboMemStatus.TabIndex = 9
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.BackColor = System.Drawing.Color.Transparent
        Me.Label18.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(335, 21)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(76, 15)
        Me.Label18.TabIndex = 128
        Me.Label18.Text = "Client Status"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.btnMaxPhoto)
        Me.GroupBox3.Controls.Add(Me.Label32)
        Me.GroupBox3.Controls.Add(Me.btnBrowsePicture)
        Me.GroupBox3.Controls.Add(Me.picClientPhoto)
        Me.GroupBox3.Location = New System.Drawing.Point(593, 15)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(226, 119)
        Me.GroupBox3.TabIndex = 127
        Me.GroupBox3.TabStop = False
        '
        'btnMaxPhoto
        '
        Me.btnMaxPhoto.Location = New System.Drawing.Point(191, 14)
        Me.btnMaxPhoto.Name = "btnMaxPhoto"
        Me.btnMaxPhoto.Size = New System.Drawing.Size(29, 23)
        Me.btnMaxPhoto.TabIndex = 17
        Me.btnMaxPhoto.Text = "[* ]"
        Me.btnMaxPhoto.UseVisualStyleBackColor = True
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label32.Location = New System.Drawing.Point(6, 12)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(44, 15)
        Me.Label32.TabIndex = 126
        Me.Label32.Text = "Photo:"
        '
        'btnBrowsePicture
        '
        Me.btnBrowsePicture.Location = New System.Drawing.Point(191, 89)
        Me.btnBrowsePicture.Name = "btnBrowsePicture"
        Me.btnBrowsePicture.Size = New System.Drawing.Size(29, 23)
        Me.btnBrowsePicture.TabIndex = 18
        Me.btnBrowsePicture.Text = "..."
        Me.btnBrowsePicture.UseVisualStyleBackColor = True
        '
        'picClientPhoto
        '
        Me.picClientPhoto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picClientPhoto.Image = Global.WindowsApplication2.My.Resources.Resources.noLogo
        Me.picClientPhoto.Location = New System.Drawing.Point(65, 12)
        Me.picClientPhoto.Name = "picClientPhoto"
        Me.picClientPhoto.Size = New System.Drawing.Size(120, 100)
        Me.picClientPhoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picClientPhoto.TabIndex = 122
        Me.picClientPhoto.TabStop = False
        '
        'Panel3
        '
        Me.Panel3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel3.BackColor = System.Drawing.Color.Transparent
        Me.Panel3.Location = New System.Drawing.Point(590, 9)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(237, 128)
        Me.Panel3.TabIndex = 113
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.BackColor = System.Drawing.Color.Transparent
        Me.Label16.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(551, 250)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(117, 15)
        Me.Label16.TabIndex = 106
        Me.Label16.Text = "Position Headcount:"
        Me.Label16.Visible = False
        '
        'cboProjectSpecialist
        '
        Me.cboProjectSpecialist.FormattingEnabled = True
        Me.cboProjectSpecialist.Location = New System.Drawing.Point(108, 116)
        Me.cboProjectSpecialist.Name = "cboProjectSpecialist"
        Me.cboProjectSpecialist.Size = New System.Drawing.Size(228, 21)
        Me.cboProjectSpecialist.TabIndex = 14
        '
        'txtPosHeadcount
        '
        Me.txtPosHeadcount.BackColor = System.Drawing.SystemColors.Window
        Me.txtPosHeadcount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtPosHeadcount.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPosHeadcount.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPosHeadcount.Location = New System.Drawing.Point(669, 247)
        Me.txtPosHeadcount.MaxLength = 50
        Me.txtPosHeadcount.Name = "txtPosHeadcount"
        Me.txtPosHeadcount.Size = New System.Drawing.Size(24, 21)
        Me.txtPosHeadcount.TabIndex = 107
        Me.txtPosHeadcount.Visible = False
        '
        'txtProjHeadcount
        '
        Me.txtProjHeadcount.BackColor = System.Drawing.SystemColors.Window
        Me.txtProjHeadcount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtProjHeadcount.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtProjHeadcount.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtProjHeadcount.Location = New System.Drawing.Point(522, 248)
        Me.txtProjHeadcount.MaxLength = 50
        Me.txtProjHeadcount.Name = "txtProjHeadcount"
        Me.txtProjHeadcount.Size = New System.Drawing.Size(23, 21)
        Me.txtProjHeadcount.TabIndex = 95
        Me.txtProjHeadcount.Visible = False
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.BackColor = System.Drawing.Color.Transparent
        Me.Label17.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(6, 119)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(103, 15)
        Me.Label17.TabIndex = 111
        Me.Label17.Text = "Project Specialist:"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.BackColor = System.Drawing.Color.Transparent
        Me.Label15.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(414, 249)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(112, 15)
        Me.Label15.TabIndex = 94
        Me.Label15.Text = "Project Headcount:"
        Me.Label15.Visible = False
        '
        'btnContract
        '
        Me.btnContract.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnContract.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnContract.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnContract.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnContract.Location = New System.Drawing.Point(709, 251)
        Me.btnContract.Name = "btnContract"
        Me.btnContract.Size = New System.Drawing.Size(117, 34)
        Me.btnContract.TabIndex = 110
        Me.btnContract.Text = "Contracts to Expire"
        Me.btnContract.UseVisualStyleBackColor = False
        '
        'btnEmployeeList
        '
        Me.btnEmployeeList.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnEmployeeList.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnEmployeeList.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnEmployeeList.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEmployeeList.Location = New System.Drawing.Point(5, 251)
        Me.btnEmployeeList.Name = "btnEmployeeList"
        Me.btnEmployeeList.Size = New System.Drawing.Size(134, 34)
        Me.btnEmployeeList.TabIndex = 25
        Me.btnEmployeeList.Text = "Employee Master List"
        Me.btnEmployeeList.UseVisualStyleBackColor = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.dtPayRelease)
        Me.GroupBox2.Controls.Add(Me.Label14)
        Me.GroupBox2.Controls.Add(Me.dtPayroll)
        Me.GroupBox2.Controls.Add(Me.Label13)
        Me.GroupBox2.Controls.Add(Me.dtPayPeriod)
        Me.GroupBox2.Controls.Add(Me.Label12)
        Me.GroupBox2.Controls.Add(Me.dtPayrollPeriod)
        Me.GroupBox2.Controls.Add(Me.dtBilling)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Controls.Add(Me.Label11)
        Me.GroupBox2.Controls.Add(Me.Label10)
        Me.GroupBox2.Controls.Add(Me.dtCollection)
        Me.GroupBox2.Location = New System.Drawing.Point(9, 168)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(817, 79)
        Me.GroupBox2.TabIndex = 105
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Schedule"
        '
        'dtPayRelease
        '
        Me.dtPayRelease.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtPayRelease.Location = New System.Drawing.Point(683, 43)
        Me.dtPayRelease.Name = "dtPayRelease"
        Me.dtPayRelease.Size = New System.Drawing.Size(106, 20)
        Me.dtPayRelease.TabIndex = 24
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.BackColor = System.Drawing.Color.Transparent
        Me.Label14.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(540, 45)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(144, 15)
        Me.Label14.TabIndex = 109
        Me.Label14.Text = "13th Month Pay Release:"
        '
        'dtPayroll
        '
        Me.dtPayroll.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtPayroll.Location = New System.Drawing.Point(683, 17)
        Me.dtPayroll.Name = "dtPayroll"
        Me.dtPayroll.Size = New System.Drawing.Size(106, 20)
        Me.dtPayroll.TabIndex = 21
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.BackColor = System.Drawing.Color.Transparent
        Me.Label13.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(558, 19)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(126, 15)
        Me.Label13.TabIndex = 107
        Me.Label13.Text = "Submission of Payroll:"
        '
        'dtPayPeriod
        '
        Me.dtPayPeriod.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtPayPeriod.Location = New System.Drawing.Point(115, 43)
        Me.dtPayPeriod.Name = "dtPayPeriod"
        Me.dtPayPeriod.Size = New System.Drawing.Size(106, 20)
        Me.dtPayPeriod.TabIndex = 22
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.BackColor = System.Drawing.Color.Transparent
        Me.Label12.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(23, 48)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(69, 15)
        Me.Label12.TabIndex = 105
        Me.Label12.Text = "Pay Period:"
        '
        'dtPayrollPeriod
        '
        Me.dtPayrollPeriod.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtPayrollPeriod.Location = New System.Drawing.Point(115, 17)
        Me.dtPayrollPeriod.Name = "dtPayrollPeriod"
        Me.dtPayrollPeriod.Size = New System.Drawing.Size(106, 20)
        Me.dtPayrollPeriod.TabIndex = 19
        '
        'dtBilling
        '
        Me.dtBilling.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtBilling.Location = New System.Drawing.Point(386, 17)
        Me.dtBilling.Name = "dtBilling"
        Me.dtBilling.Size = New System.Drawing.Size(106, 20)
        Me.dtBilling.TabIndex = 20
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.BackColor = System.Drawing.Color.Transparent
        Me.Label9.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(23, 22)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(87, 15)
        Me.Label9.TabIndex = 99
        Me.Label9.Text = "Payroll Period:"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.BackColor = System.Drawing.Color.Transparent
        Me.Label11.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(264, 19)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(121, 15)
        Me.Label11.TabIndex = 103
        Me.Label11.Text = "Submission of Billing:"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.BackColor = System.Drawing.Color.Transparent
        Me.Label10.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(277, 45)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(104, 15)
        Me.Label10.TabIndex = 101
        Me.Label10.Text = "Collection Period:"
        '
        'dtCollection
        '
        Me.dtCollection.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtCollection.Location = New System.Drawing.Point(386, 43)
        Me.dtCollection.Name = "dtCollection"
        Me.dtCollection.Size = New System.Drawing.Size(106, 20)
        Me.dtCollection.TabIndex = 23
        '
        'txtDescription
        '
        Me.txtDescription.BackColor = System.Drawing.SystemColors.Window
        Me.txtDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtDescription.Enabled = False
        Me.txtDescription.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescription.Location = New System.Drawing.Point(108, 141)
        Me.txtDescription.MaxLength = 50
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.Size = New System.Drawing.Size(720, 21)
        Me.txtDescription.TabIndex = 16
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.Transparent
        Me.Label8.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(7, 143)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(73, 15)
        Me.Label8.TabIndex = 97
        Me.Label8.Text = "Description:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.Label7.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(345, 69)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(90, 15)
        Me.Label7.TabIndex = 95
        Me.Label7.Text = "Telephone No.:"
        '
        'txtTelNo
        '
        Me.txtTelNo.BackColor = System.Drawing.SystemColors.Window
        Me.txtTelNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtTelNo.Enabled = False
        Me.txtTelNo.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTelNo.Location = New System.Drawing.Point(441, 66)
        Me.txtTelNo.MaxLength = 50
        Me.txtTelNo.Name = "txtTelNo"
        Me.txtTelNo.Size = New System.Drawing.Size(127, 21)
        Me.txtTelNo.TabIndex = 12
        '
        'txtAddress
        '
        Me.txtAddress.BackColor = System.Drawing.SystemColors.Window
        Me.txtAddress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtAddress.Enabled = False
        Me.txtAddress.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAddress.Location = New System.Drawing.Point(108, 91)
        Me.txtAddress.MaxLength = 50
        Me.txtAddress.Name = "txtAddress"
        Me.txtAddress.Size = New System.Drawing.Size(460, 21)
        Me.txtAddress.TabIndex = 13
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(7, 95)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(55, 15)
        Me.Label6.TabIndex = 93
        Me.Label6.Text = "Address:"
        '
        'btnemp_close
        '
        Me.btnemp_close.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnemp_close.BackColor = System.Drawing.Color.White
        Me.btnemp_close.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnemp_close.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnemp_close.Image = Global.WindowsApplication2.My.Resources.Resources.eventlogError
        Me.btnemp_close.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnemp_close.Location = New System.Drawing.Point(776, 3)
        Me.btnemp_close.Name = "btnemp_close"
        Me.btnemp_close.Size = New System.Drawing.Size(66, 28)
        Me.btnemp_close.TabIndex = 7
        Me.btnemp_close.Text = "Close"
        Me.btnemp_close.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnemp_close.UseVisualStyleBackColor = False
        '
        'btnemp_next
        '
        Me.btnemp_next.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnemp_next.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnemp_next.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnemp_next.Location = New System.Drawing.Point(147, 3)
        Me.btnemp_next.Name = "btnemp_next"
        Me.btnemp_next.Size = New System.Drawing.Size(66, 28)
        Me.btnemp_next.TabIndex = 3
        Me.btnemp_next.Text = ">>"
        Me.btnemp_next.UseVisualStyleBackColor = True
        '
        'btnemp_delete
        '
        Me.btnemp_delete.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnemp_delete.BackColor = System.Drawing.Color.White
        Me.btnemp_delete.Image = CType(resources.GetObject("btnemp_delete.Image"), System.Drawing.Image)
        Me.btnemp_delete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnemp_delete.Location = New System.Drawing.Point(709, 3)
        Me.btnemp_delete.Name = "btnemp_delete"
        Me.btnemp_delete.Size = New System.Drawing.Size(66, 28)
        Me.btnemp_delete.TabIndex = 6
        Me.btnemp_delete.Text = "Delete"
        Me.btnemp_delete.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnemp_delete.UseVisualStyleBackColor = False
        '
        'BTNSEARCH
        '
        Me.BTNSEARCH.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.BTNSEARCH.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BTNSEARCH.Image = CType(resources.GetObject("BTNSEARCH.Image"), System.Drawing.Image)
        Me.BTNSEARCH.Location = New System.Drawing.Point(80, 3)
        Me.BTNSEARCH.Name = "BTNSEARCH"
        Me.BTNSEARCH.Size = New System.Drawing.Size(66, 28)
        Me.BTNSEARCH.TabIndex = 2
        Me.BTNSEARCH.UseVisualStyleBackColor = True
        '
        'btnemp_edit
        '
        Me.btnemp_edit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnemp_edit.BackColor = System.Drawing.Color.White
        Me.btnemp_edit.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnemp_edit.Image = CType(resources.GetObject("btnemp_edit.Image"), System.Drawing.Image)
        Me.btnemp_edit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnemp_edit.Location = New System.Drawing.Point(642, 3)
        Me.btnemp_edit.Name = "btnemp_edit"
        Me.btnemp_edit.Size = New System.Drawing.Size(66, 28)
        Me.btnemp_edit.TabIndex = 5
        Me.btnemp_edit.Text = "Edit"
        Me.btnemp_edit.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnemp_edit.UseVisualStyleBackColor = False
        '
        'btnemp_previous
        '
        Me.btnemp_previous.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnemp_previous.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnemp_previous.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnemp_previous.Location = New System.Drawing.Point(13, 3)
        Me.btnemp_previous.Name = "btnemp_previous"
        Me.btnemp_previous.Size = New System.Drawing.Size(66, 28)
        Me.btnemp_previous.TabIndex = 1
        Me.btnemp_previous.Text = "<<"
        Me.btnemp_previous.UseVisualStyleBackColor = True
        '
        'btnemp_add
        '
        Me.btnemp_add.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnemp_add.BackColor = System.Drawing.Color.White
        Me.btnemp_add.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnemp_add.Image = CType(resources.GetObject("btnemp_add.Image"), System.Drawing.Image)
        Me.btnemp_add.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnemp_add.Location = New System.Drawing.Point(575, 3)
        Me.btnemp_add.Name = "btnemp_add"
        Me.btnemp_add.Size = New System.Drawing.Size(66, 28)
        Me.btnemp_add.TabIndex = 4
        Me.btnemp_add.Text = "New"
        Me.btnemp_add.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnemp_add.UseVisualStyleBackColor = False
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'GroupBox4
        '
        Me.GroupBox4.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox4.Controls.Add(Me.lvlAttach)
        Me.GroupBox4.Controls.Add(Me.btnAttach)
        Me.GroupBox4.Controls.Add(Me.btnUpload)
        Me.GroupBox4.Controls.Add(Me.txtAttachment)
        Me.GroupBox4.Controls.Add(Me.Label1)
        Me.GroupBox4.Location = New System.Drawing.Point(5, 303)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(838, 184)
        Me.GroupBox4.TabIndex = 94
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Contract Copy Attachment"
        '
        'lvlAttach
        '
        Me.lvlAttach.Location = New System.Drawing.Point(6, 48)
        Me.lvlAttach.Name = "lvlAttach"
        Me.lvlAttach.Size = New System.Drawing.Size(824, 90)
        Me.lvlAttach.TabIndex = 28
        Me.lvlAttach.UseCompatibleStateImageBehavior = False
        '
        'btnAttach
        '
        Me.btnAttach.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnAttach.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnAttach.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnAttach.Enabled = False
        Me.btnAttach.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAttach.Image = Global.WindowsApplication2.My.Resources.Resources.images__14_
        Me.btnAttach.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAttach.Location = New System.Drawing.Point(417, 13)
        Me.btnAttach.Name = "btnAttach"
        Me.btnAttach.Size = New System.Drawing.Size(128, 30)
        Me.btnAttach.TabIndex = 27
        Me.btnAttach.Text = "Attach File"
        Me.btnAttach.UseVisualStyleBackColor = False
        '
        'btnUpload
        '
        Me.btnUpload.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnUpload.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnUpload.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnUpload.Enabled = False
        Me.btnUpload.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUpload.Image = Global.WindowsApplication2.My.Resources.Resources.images__36_
        Me.btnUpload.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUpload.Location = New System.Drawing.Point(702, 144)
        Me.btnUpload.Name = "btnUpload"
        Me.btnUpload.Size = New System.Drawing.Size(127, 34)
        Me.btnUpload.TabIndex = 7
        Me.btnUpload.Text = "Upload File"
        Me.btnUpload.UseVisualStyleBackColor = False
        '
        'txtAttachment
        '
        Me.txtAttachment.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtAttachment.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAttachment.Location = New System.Drawing.Point(134, 19)
        Me.txtAttachment.Name = "txtAttachment"
        Me.txtAttachment.Size = New System.Drawing.Size(280, 21)
        Me.txtAttachment.TabIndex = 26
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(24, 21)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(112, 15)
        Me.Label1.TabIndex = 130
        Me.Label1.Text = "Attachment Name:"
        '
        'PanePanel2
        '
        Me.PanePanel2.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.PanePanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel2.Controls.Add(Me.btnClose)
        Me.PanePanel2.Controls.Add(Me.btnDelete)
        Me.PanePanel2.Controls.Add(Me.btnSearchEmp)
        Me.PanePanel2.Controls.Add(Me.btnEdit)
        Me.PanePanel2.Controls.Add(Me.btnAdd)
        Me.PanePanel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PanePanel2.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PanePanel2.InactiveGradientHighColor = System.Drawing.Color.Transparent
        Me.PanePanel2.InactiveGradientLowColor = System.Drawing.Color.Transparent
        Me.PanePanel2.Location = New System.Drawing.Point(0, 494)
        Me.PanePanel2.Name = "PanePanel2"
        Me.PanePanel2.Size = New System.Drawing.Size(850, 35)
        Me.PanePanel2.TabIndex = 84
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnClose.Image = Global.WindowsApplication2.My.Resources.Resources.eventlogError
        Me.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnClose.Location = New System.Drawing.Point(774, 3)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(66, 28)
        Me.btnClose.TabIndex = 6
        Me.btnClose.Text = "Close"
        Me.btnClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClose.UseVisualStyleBackColor = False
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.Image = CType(resources.GetObject("btnDelete.Image"), System.Drawing.Image)
        Me.btnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDelete.Location = New System.Drawing.Point(706, 3)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(66, 28)
        Me.btnDelete.TabIndex = 5
        Me.btnDelete.Text = "Delete"
        Me.btnDelete.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnDelete.UseVisualStyleBackColor = False
        '
        'btnSearchEmp
        '
        Me.btnSearchEmp.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnSearchEmp.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnSearchEmp.Image = CType(resources.GetObject("btnSearchEmp.Image"), System.Drawing.Image)
        Me.btnSearchEmp.Location = New System.Drawing.Point(4, 3)
        Me.btnSearchEmp.Name = "btnSearchEmp"
        Me.btnSearchEmp.Size = New System.Drawing.Size(66, 28)
        Me.btnSearchEmp.TabIndex = 1
        Me.btnSearchEmp.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnEdit.Image = CType(resources.GetObject("btnEdit.Image"), System.Drawing.Image)
        Me.btnEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnEdit.Location = New System.Drawing.Point(639, 3)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(66, 28)
        Me.btnEdit.TabIndex = 4
        Me.btnEdit.Text = "Edit"
        Me.btnEdit.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnEdit.UseVisualStyleBackColor = False
        '
        'btnAdd
        '
        Me.btnAdd.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAdd.BackColor = System.Drawing.Color.White
        Me.btnAdd.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnAdd.Image = CType(resources.GetObject("btnAdd.Image"), System.Drawing.Image)
        Me.btnAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAdd.Location = New System.Drawing.Point(572, 3)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(66, 28)
        Me.btnAdd.TabIndex = 3
        Me.btnAdd.Text = "New"
        Me.btnAdd.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnAdd.UseVisualStyleBackColor = False
        '
        'frmMember_Client
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(850, 529)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.PanePanel2)
        Me.DoubleBuffered = True
        Me.MaximizeBox = False
        Me.Name = "frmMember_Client"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Existing Client File"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.picClientPhoto, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.PanePanel2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents PanePanel2 As WindowsApplication2.PanePanel
    Friend WithEvents btnemp_close As System.Windows.Forms.Button
    Friend WithEvents btnemp_next As System.Windows.Forms.Button
    Friend WithEvents btnemp_delete As System.Windows.Forms.Button
    Friend WithEvents BTNSEARCH As System.Windows.Forms.Button
    Friend WithEvents btnemp_edit As System.Windows.Forms.Button
    Friend WithEvents btnemp_previous As System.Windows.Forms.Button
    Friend WithEvents btnemp_add As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtClientName As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtTIN As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtContactPerson As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents dtPayrollPeriod As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtDescription As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtTelNo As System.Windows.Forms.TextBox
    Friend WithEvents txtAddress As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents btnEmployeeList As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents dtPayroll As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents dtPayPeriod As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents dtBilling As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents dtCollection As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txtProjHeadcount As System.Windows.Forms.TextBox
    Friend WithEvents btnContract As System.Windows.Forms.Button
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents txtPosHeadcount As System.Windows.Forms.TextBox
    Friend WithEvents cboProjectSpecialist As System.Windows.Forms.ComboBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents dtPayRelease As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents btnDelete As System.Windows.Forms.Button
    Friend WithEvents btnSearchEmp As System.Windows.Forms.Button
    Friend WithEvents btnEdit As System.Windows.Forms.Button
    Friend WithEvents btnAdd As System.Windows.Forms.Button
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents btnMaxPhoto As System.Windows.Forms.Button
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents btnBrowsePicture As System.Windows.Forms.Button
    Friend WithEvents picClientPhoto As System.Windows.Forms.PictureBox
    Friend WithEvents txtClientNo As System.Windows.Forms.TextBox
    Friend WithEvents cboMemStatus As System.Windows.Forms.ComboBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents btnAttach As System.Windows.Forms.Button
    Friend WithEvents btnUpload As System.Windows.Forms.Button
    Friend WithEvents txtAttachment As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lvlAttach As System.Windows.Forms.ListView
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
End Class
