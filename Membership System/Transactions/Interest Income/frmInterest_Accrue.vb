'                   ########################################################
'                   ### Created by: Charl Magne "Ionflux" Onod San Pedro ###
'                   ### Date Created: August 10, 2010                    ###
'                   ### Website: http://www.ionflux.site50.net           ###
'                   ########################################################

Imports system.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmInterest_Accrue

    Private xCounter As Integer = 0
    Private xTotalLoan As Integer = 0
    Private xLoanNo As String = ""
    Public xMonthInt As Integer
    Public xYear As Integer
    Private gCon As New Clsappconfiguration
    Private Sub Accrue()
        Dim sSqlCmd As String
        Dim DTLoanDetail As DataTable

        sSqlCmd = "select count(fnLoanNo) as TotalLoans from dbo.View_1_LoanMaster_Current"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                If rd.Read Then
                    Dim x As Integer = rd.Item(0)
                    xTotalLoan = rd.Item(0)
                End If
            End Using
        Catch ex As Exception
            MsgBox("Error at BGWorker_DoWork on frmInterestIncome module." & vbCr & vbCr & ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error occur!")
        End Try

        sSqlCmd = "select pk_Members_Loan, fnLoanNo from dbo.View_1_LoanMaster_Current"
        Try
            DTLoanDetail = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.Text, sSqlCmd).Tables(0)
        Catch ex As Exception
            MsgBox("Error at BGWorker_DoWork on frmInterestIncome module." & vbCr & vbCr & ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error occur!")
        End Try

        For Each xItem As DataRow In DTLoanDetail.Rows
            xCounter += 1
            xLoanNo = xItem.Item(1).ToString
            sSqlCmd = "CIMS_Accrual_Interest_Accrue '" & xYear & _
                    "','" & xMonthInt & "','" & xItem.Item(0).ToString & "'"
            Try
                SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.Text, sSqlCmd)
            Catch ex As Exception
                Throw ex
            End Try
        Next

    End Sub
    Private Sub FrmProcessAccrue_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        PBar.Maximum = xTotalLoan
        PBar.Minimum = 0
        BGWorker.RunWorkerAsync()
    End Sub

    Private Sub BGWorker_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BGWorker.DoWork
        Try
            Accrue()
            MsgBox("Accrue of interest successful!", MsgBoxStyle.Information + MsgBoxStyle.OkOnly, "Access granted!")
        Catch ex As Exception
            MessageBox.Show(ex.Message, "System Validation: Accruing Process", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub BGWorker_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BGWorker.RunWorkerCompleted
        Me.Close()
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        PBar.Maximum = xTotalLoan
        lblLoanNo.Text = "Processing: " & xLoanNo & " . . . "
        LblCounter.Text = xCounter & " of " & xTotalLoan & " record/s"
        PBar.Value = xCounter
    End Sub

    Private Sub FrmProcessAccrue_ResizeBegin(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ResizeBegin
        Me.Opacity = 0.5
    End Sub

    Private Sub FrmProcessAccrue_ResizeEnd(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ResizeEnd
        Me.Opacity = 1
    End Sub
End Class