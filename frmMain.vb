Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports Microsoft.VisualBasic
Imports System.Security
Imports System.Security.Principal.WindowsIdentity
Imports System.Threading
Imports Microsoft.ApplicationBlocks.Data

Public Class frmMain

#Region "----"
    Inherits System.Windows.Forms.Form
    Private myimagelist As New ImageList
    Friend WithEvents imglBars As System.Windows.Forms.ImageList
    Friend WithEvents lblCompanyTitle As System.Windows.Forms.Label
    Friend WithEvents Imglitems As System.Windows.Forms.ImageList
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AcclExplorerBar1 As vbAccelerator.Components.Controls.acclExplorerBar
    Friend WithEvents ToolStripSeparator7 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents RequisitionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RequisitionOfManpowerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FileToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StrJobRequisition As System.Windows.Forms.MenuStrip
    Friend WithEvents Timer2 As System.Windows.Forms.Timer

    Dim bar As New ProgressBar
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents StLoginName As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents LoanManager As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Deposit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SecurityToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Contribution As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HelpToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UserToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PrintDialog1 As System.Windows.Forms.PrintDialog
    Friend WithEvents PanePanel1 As WindowsApplication2.PanePanel
    Friend WithEvents StTime As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents Stdate As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents StComputername As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents Stcomputeruser As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents MemberContributionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Newloans As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExistingLoans As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ChangePasswordToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolReports As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DatabaseToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents bgwDatabaseFunctions As System.ComponentModel.BackgroundWorker
    Friend WithEvents A1ReportsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SummaryOfAgingReportsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BalanceSheetToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StatementOfNetSurplusToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StatementOfUtilizationOfStatutoryFundsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IndividualStatementsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CapitalShareToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LoansToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SavingsAndToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents A2ReportsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LoanReceivableAgingReportToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ScheduleOfLoanReceivaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PaidUpCapitalShareToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AccountsPayableToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LoansPayableToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GeneralLedgerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PropertyToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PESOReportsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CAPRToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AverageCapitalShareAndDividendsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MemberManager As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExistingMember As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ExitToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CompanyInformationToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnShowHideSubmenu As System.Windows.Forms.Button
    Friend WithEvents LoanApproval As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RightsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents DebitCreditToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AccountRegisterToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CapitalShareDepositAndWithdrawalToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PanePanel5 As WindowsApplication2.PanePanel
    Friend WithEvents btnEnteng As System.Windows.Forms.Button
    Friend WithEvents lblCoopName As System.Windows.Forms.Label
    Friend WithEvents LoanReportsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ScheduleOfLoansReceivableToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ScheduleOfLoansRToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LoanStatusReportToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LoanLedgerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ClientListReportToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AccountVerificationToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ClientUploadToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LoanForwardBalanceUploaderToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LoanAmortizationUploaderToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ClientsList2ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IntegratorToolStrip As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ClientsIntegritorToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LoanUploader3ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Investment As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InvestmentTransactionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InvestmentApprovalToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InvestmentSetupToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents LoanAmortizationEditorToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EmployeeMasterToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ClientMasterToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EmployeeInformationReportToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HistoryReportToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ContractSummaryPerCompanyToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VendorMasterToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DownloaderToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Private dtEmployee As DataTable
#End Region
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents ErrorProvider1 As System.Windows.Forms.ErrorProvider
    Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.ErrorProvider1 = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.imglBars = New System.Windows.Forms.ImageList(Me.components)
        Me.Imglitems = New System.Windows.Forms.ImageList(Me.components)
        Me.ToolStripSeparator7 = New System.Windows.Forms.ToolStripSeparator()
        Me.RequisitionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RequisitionOfManpowerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FileToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.StrJobRequisition = New System.Windows.Forms.MenuStrip()
        Me.MemberManager = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExistingMember = New System.Windows.Forms.ToolStripMenuItem()
        Me.EmployeeMasterToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClientMasterToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VendorMasterToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.ExitToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.LoanManager = New System.Windows.Forms.ToolStripMenuItem()
        Me.Newloans = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExistingLoans = New System.Windows.Forms.ToolStripMenuItem()
        Me.LoanApproval = New System.Windows.Forms.ToolStripMenuItem()
        Me.DebitCreditToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AccountRegisterToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator()
        Me.LoanAmortizationEditorToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Investment = New System.Windows.Forms.ToolStripMenuItem()
        Me.InvestmentSetupToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.InvestmentTransactionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InvestmentApprovalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Deposit = New System.Windows.Forms.ToolStripMenuItem()
        Me.LoanLedgerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AccountVerificationToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DatabaseToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClientUploadToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LoanForwardBalanceUploaderToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LoanAmortizationUploaderToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LoanUploader3ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DownloaderToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolReports = New System.Windows.Forms.ToolStripMenuItem()
        Me.A1ReportsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BalanceSheetToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StatementOfNetSurplusToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SummaryOfAgingReportsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StatementOfUtilizationOfStatutoryFundsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.A2ReportsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LoanReceivableAgingReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ScheduleOfLoanReceivaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PaidUpCapitalShareToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AccountsPayableToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LoansPayableToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GeneralLedgerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PropertyToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IndividualStatementsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CapitalShareToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LoansToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SavingsAndToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AverageCapitalShareAndDividendsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PESOReportsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CAPRToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LoanReportsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ScheduleOfLoansReceivableToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ScheduleOfLoansRToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LoanStatusReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClientListReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClientsList2ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EmployeeInformationReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HistoryReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ContractSummaryPerCompanyToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IntegratorToolStrip = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClientsIntegritorToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SecurityToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UserToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ChangePasswordToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RightsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.CompanyInformationToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Contribution = New System.Windows.Forms.ToolStripMenuItem()
        Me.MemberContributionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CapitalShareDepositAndWithdrawalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Timer2 = New System.Windows.Forms.Timer(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.StLoginName = New System.Windows.Forms.ToolStripStatusLabel()
        Me.StTime = New System.Windows.Forms.ToolStripStatusLabel()
        Me.Stdate = New System.Windows.Forms.ToolStripStatusLabel()
        Me.StComputername = New System.Windows.Forms.ToolStripStatusLabel()
        Me.Stcomputeruser = New System.Windows.Forms.ToolStripStatusLabel()
        Me.PrintDialog1 = New System.Windows.Forms.PrintDialog()
        Me.bgwDatabaseFunctions = New System.ComponentModel.BackgroundWorker()
        Me.btnShowHideSubmenu = New System.Windows.Forms.Button()
        Me.lblCompanyTitle = New System.Windows.Forms.Label()
        Me.AcclExplorerBar1 = New vbAccelerator.Components.Controls.acclExplorerBar()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.PanePanel5 = New WindowsApplication2.PanePanel()
        Me.lblCoopName = New System.Windows.Forms.Label()
        Me.btnEnteng = New System.Windows.Forms.Button()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StrJobRequisition.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanePanel5.SuspendLayout()
        Me.SuspendLayout()
        '
        'ErrorProvider1
        '
        Me.ErrorProvider1.ContainerControl = Me
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ExitToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(35, 20)
        Me.FileToolStripMenuItem.Text = "&File"
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Image = CType(resources.GetObject("ExitToolStripMenuItem.Image"), System.Drawing.Image)
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.X), System.Windows.Forms.Keys)
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(133, 22)
        Me.ExitToolStripMenuItem.Text = "Exit"
        '
        'Timer1
        '
        Me.Timer1.Interval = 10
        '
        'imglBars
        '
        Me.imglBars.ImageStream = CType(resources.GetObject("imglBars.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imglBars.TransparentColor = System.Drawing.Color.Transparent
        Me.imglBars.Images.SetKeyName(0, "")
        Me.imglBars.Images.SetKeyName(1, "")
        Me.imglBars.Images.SetKeyName(2, "")
        Me.imglBars.Images.SetKeyName(3, "")
        Me.imglBars.Images.SetKeyName(4, "")
        Me.imglBars.Images.SetKeyName(5, "")
        Me.imglBars.Images.SetKeyName(6, "")
        Me.imglBars.Images.SetKeyName(7, "")
        Me.imglBars.Images.SetKeyName(8, "")
        Me.imglBars.Images.SetKeyName(9, "")
        Me.imglBars.Images.SetKeyName(10, "")
        Me.imglBars.Images.SetKeyName(11, "")
        Me.imglBars.Images.SetKeyName(12, "")
        Me.imglBars.Images.SetKeyName(13, "Money bag.ico")
        '
        'Imglitems
        '
        Me.Imglitems.ImageStream = CType(resources.GetObject("Imglitems.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.Imglitems.TransparentColor = System.Drawing.Color.Transparent
        Me.Imglitems.Images.SetKeyName(0, "MBALogoSmall.bmp")
        '
        'ToolStripSeparator7
        '
        Me.ToolStripSeparator7.Name = "ToolStripSeparator7"
        Me.ToolStripSeparator7.Size = New System.Drawing.Size(252, 6)
        '
        'RequisitionToolStripMenuItem
        '
        Me.RequisitionToolStripMenuItem.Name = "RequisitionToolStripMenuItem"
        Me.RequisitionToolStripMenuItem.Size = New System.Drawing.Size(71, 20)
        Me.RequisitionToolStripMenuItem.Text = "&Requisition"
        '
        'RequisitionOfManpowerToolStripMenuItem
        '
        Me.RequisitionOfManpowerToolStripMenuItem.Name = "RequisitionOfManpowerToolStripMenuItem"
        Me.RequisitionOfManpowerToolStripMenuItem.Size = New System.Drawing.Size(203, 22)
        Me.RequisitionOfManpowerToolStripMenuItem.Text = "Requisition of Manpower"
        '
        'FileToolStripMenuItem1
        '
        Me.FileToolStripMenuItem1.Name = "FileToolStripMenuItem1"
        Me.FileToolStripMenuItem1.Size = New System.Drawing.Size(35, 20)
        Me.FileToolStripMenuItem1.Text = "&File"
        '
        'StrJobRequisition
        '
        Me.StrJobRequisition.BackColor = System.Drawing.Color.White
        Me.StrJobRequisition.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.StrJobRequisition.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MemberManager, Me.LoanManager, Me.Investment, Me.Deposit, Me.DatabaseToolStripMenuItem, Me.ToolReports, Me.IntegratorToolStrip, Me.SecurityToolStripMenuItem, Me.Contribution, Me.HelpToolStripMenuItem})
        Me.StrJobRequisition.Location = New System.Drawing.Point(0, 0)
        Me.StrJobRequisition.Name = "StrJobRequisition"
        Me.StrJobRequisition.Size = New System.Drawing.Size(1028, 24)
        Me.StrJobRequisition.TabIndex = 27
        Me.StrJobRequisition.Text = "Job Requisition"
        '
        'MemberManager
        '
        Me.MemberManager.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ExistingMember, Me.ToolStripSeparator2, Me.ExitToolStripMenuItem1})
        Me.MemberManager.Image = CType(resources.GetObject("MemberManager.Image"), System.Drawing.Image)
        Me.MemberManager.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.MemberManager.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.MemberManager.Name = "MemberManager"
        Me.MemberManager.Size = New System.Drawing.Size(121, 20)
        Me.MemberManager.Text = "Client Manager"
        '
        'ExistingMember
        '
        Me.ExistingMember.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.EmployeeMasterToolStripMenuItem, Me.ClientMasterToolStripMenuItem, Me.VendorMasterToolStripMenuItem})
        Me.ExistingMember.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ExistingMember.Image = CType(resources.GetObject("ExistingMember.Image"), System.Drawing.Image)
        Me.ExistingMember.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ExistingMember.ImageTransparentColor = System.Drawing.Color.White
        Me.ExistingMember.Name = "ExistingMember"
        Me.ExistingMember.Size = New System.Drawing.Size(148, 24)
        Me.ExistingMember.Text = "Client"
        '
        'EmployeeMasterToolStripMenuItem
        '
        Me.EmployeeMasterToolStripMenuItem.Name = "EmployeeMasterToolStripMenuItem"
        Me.EmployeeMasterToolStripMenuItem.Size = New System.Drawing.Size(191, 24)
        Me.EmployeeMasterToolStripMenuItem.Text = "Employee Master"
        '
        'ClientMasterToolStripMenuItem
        '
        Me.ClientMasterToolStripMenuItem.Name = "ClientMasterToolStripMenuItem"
        Me.ClientMasterToolStripMenuItem.Size = New System.Drawing.Size(191, 24)
        Me.ClientMasterToolStripMenuItem.Text = "Client Master"
        '
        'VendorMasterToolStripMenuItem
        '
        Me.VendorMasterToolStripMenuItem.Name = "VendorMasterToolStripMenuItem"
        Me.VendorMasterToolStripMenuItem.Size = New System.Drawing.Size(191, 24)
        Me.VendorMasterToolStripMenuItem.Text = "Vendor Master"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(145, 6)
        '
        'ExitToolStripMenuItem1
        '
        Me.ExitToolStripMenuItem1.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ExitToolStripMenuItem1.Image = CType(resources.GetObject("ExitToolStripMenuItem1.Image"), System.Drawing.Image)
        Me.ExitToolStripMenuItem1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ExitToolStripMenuItem1.Name = "ExitToolStripMenuItem1"
        Me.ExitToolStripMenuItem1.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.X), System.Windows.Forms.Keys)
        Me.ExitToolStripMenuItem1.Size = New System.Drawing.Size(148, 24)
        Me.ExitToolStripMenuItem1.Text = "Exit"
        '
        'LoanManager
        '
        Me.LoanManager.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Newloans, Me.ExistingLoans, Me.LoanApproval, Me.DebitCreditToolStripMenuItem, Me.AccountRegisterToolStripMenuItem, Me.ToolStripSeparator4, Me.LoanAmortizationEditorToolStripMenuItem})
        Me.LoanManager.Image = CType(resources.GetObject("LoanManager.Image"), System.Drawing.Image)
        Me.LoanManager.Name = "LoanManager"
        Me.LoanManager.Size = New System.Drawing.Size(115, 20)
        Me.LoanManager.Text = "&Loan Manager"
        '
        'Newloans
        '
        Me.Newloans.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Newloans.Image = CType(resources.GetObject("Newloans.Image"), System.Drawing.Image)
        Me.Newloans.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.Newloans.Name = "Newloans"
        Me.Newloans.Size = New System.Drawing.Size(235, 24)
        Me.Newloans.Text = "Loan &Application"
        '
        'ExistingLoans
        '
        Me.ExistingLoans.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ExistingLoans.Image = CType(resources.GetObject("ExistingLoans.Image"), System.Drawing.Image)
        Me.ExistingLoans.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ExistingLoans.Name = "ExistingLoans"
        Me.ExistingLoans.Size = New System.Drawing.Size(235, 24)
        Me.ExistingLoans.Text = "Loan &Processing"
        '
        'LoanApproval
        '
        Me.LoanApproval.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LoanApproval.Image = CType(resources.GetObject("LoanApproval.Image"), System.Drawing.Image)
        Me.LoanApproval.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.LoanApproval.Name = "LoanApproval"
        Me.LoanApproval.Size = New System.Drawing.Size(235, 24)
        Me.LoanApproval.Text = "Loan Approval"
        '
        'DebitCreditToolStripMenuItem
        '
        Me.DebitCreditToolStripMenuItem.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DebitCreditToolStripMenuItem.Name = "DebitCreditToolStripMenuItem"
        Me.DebitCreditToolStripMenuItem.Size = New System.Drawing.Size(235, 24)
        Me.DebitCreditToolStripMenuItem.Text = "Debit/Credit Accounts"
        Me.DebitCreditToolStripMenuItem.Visible = False
        '
        'AccountRegisterToolStripMenuItem
        '
        Me.AccountRegisterToolStripMenuItem.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AccountRegisterToolStripMenuItem.Name = "AccountRegisterToolStripMenuItem"
        Me.AccountRegisterToolStripMenuItem.Size = New System.Drawing.Size(235, 24)
        Me.AccountRegisterToolStripMenuItem.Text = "Account Register"
        Me.AccountRegisterToolStripMenuItem.Visible = False
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(232, 6)
        '
        'LoanAmortizationEditorToolStripMenuItem
        '
        Me.LoanAmortizationEditorToolStripMenuItem.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.LoanAmortizationEditorToolStripMenuItem.Name = "LoanAmortizationEditorToolStripMenuItem"
        Me.LoanAmortizationEditorToolStripMenuItem.Size = New System.Drawing.Size(235, 24)
        Me.LoanAmortizationEditorToolStripMenuItem.Text = "Loan Amortization Editor"
        '
        'Investment
        '
        Me.Investment.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.InvestmentSetupToolStripMenuItem, Me.ToolStripSeparator1, Me.InvestmentTransactionToolStripMenuItem, Me.InvestmentApprovalToolStripMenuItem})
        Me.Investment.Image = CType(resources.GetObject("Investment.Image"), System.Drawing.Image)
        Me.Investment.Name = "Investment"
        Me.Investment.Size = New System.Drawing.Size(153, 20)
        Me.Investment.Text = "Investment Manager"
        '
        'InvestmentSetupToolStripMenuItem
        '
        Me.InvestmentSetupToolStripMenuItem.Image = CType(resources.GetObject("InvestmentSetupToolStripMenuItem.Image"), System.Drawing.Image)
        Me.InvestmentSetupToolStripMenuItem.Name = "InvestmentSetupToolStripMenuItem"
        Me.InvestmentSetupToolStripMenuItem.Size = New System.Drawing.Size(205, 22)
        Me.InvestmentSetupToolStripMenuItem.Text = "Investment Setup"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(202, 6)
        '
        'InvestmentTransactionToolStripMenuItem
        '
        Me.InvestmentTransactionToolStripMenuItem.Image = CType(resources.GetObject("InvestmentTransactionToolStripMenuItem.Image"), System.Drawing.Image)
        Me.InvestmentTransactionToolStripMenuItem.Name = "InvestmentTransactionToolStripMenuItem"
        Me.InvestmentTransactionToolStripMenuItem.Size = New System.Drawing.Size(205, 22)
        Me.InvestmentTransactionToolStripMenuItem.Text = "Investment Application"
        '
        'InvestmentApprovalToolStripMenuItem
        '
        Me.InvestmentApprovalToolStripMenuItem.Image = CType(resources.GetObject("InvestmentApprovalToolStripMenuItem.Image"), System.Drawing.Image)
        Me.InvestmentApprovalToolStripMenuItem.Name = "InvestmentApprovalToolStripMenuItem"
        Me.InvestmentApprovalToolStripMenuItem.Size = New System.Drawing.Size(205, 22)
        Me.InvestmentApprovalToolStripMenuItem.Text = "Investment Approval"
        '
        'Deposit
        '
        Me.Deposit.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.LoanLedgerToolStripMenuItem, Me.AccountVerificationToolStripMenuItem})
        Me.Deposit.Image = CType(resources.GetObject("Deposit.Image"), System.Drawing.Image)
        Me.Deposit.Name = "Deposit"
        Me.Deposit.Size = New System.Drawing.Size(125, 20)
        Me.Deposit.Text = "&Account Inquiry"
        '
        'LoanLedgerToolStripMenuItem
        '
        Me.LoanLedgerToolStripMenuItem.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.LoanLedgerToolStripMenuItem.Name = "LoanLedgerToolStripMenuItem"
        Me.LoanLedgerToolStripMenuItem.Size = New System.Drawing.Size(176, 24)
        Me.LoanLedgerToolStripMenuItem.Text = "Loan Ledger"
        Me.LoanLedgerToolStripMenuItem.Visible = False
        '
        'AccountVerificationToolStripMenuItem
        '
        Me.AccountVerificationToolStripMenuItem.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.AccountVerificationToolStripMenuItem.Image = CType(resources.GetObject("AccountVerificationToolStripMenuItem.Image"), System.Drawing.Image)
        Me.AccountVerificationToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.AccountVerificationToolStripMenuItem.Name = "AccountVerificationToolStripMenuItem"
        Me.AccountVerificationToolStripMenuItem.Size = New System.Drawing.Size(176, 24)
        Me.AccountVerificationToolStripMenuItem.Text = "Account Inquiry"
        '
        'DatabaseToolStripMenuItem
        '
        Me.DatabaseToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ClientUploadToolStripMenuItem, Me.LoanForwardBalanceUploaderToolStripMenuItem, Me.LoanAmortizationUploaderToolStripMenuItem, Me.LoanUploader3ToolStripMenuItem, Me.DownloaderToolStripMenuItem})
        Me.DatabaseToolStripMenuItem.Image = CType(resources.GetObject("DatabaseToolStripMenuItem.Image"), System.Drawing.Image)
        Me.DatabaseToolStripMenuItem.Name = "DatabaseToolStripMenuItem"
        Me.DatabaseToolStripMenuItem.Size = New System.Drawing.Size(112, 20)
        Me.DatabaseToolStripMenuItem.Text = "&System Tools"
        '
        'ClientUploadToolStripMenuItem
        '
        Me.ClientUploadToolStripMenuItem.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.ClientUploadToolStripMenuItem.Image = CType(resources.GetObject("ClientUploadToolStripMenuItem.Image"), System.Drawing.Image)
        Me.ClientUploadToolStripMenuItem.Name = "ClientUploadToolStripMenuItem"
        Me.ClientUploadToolStripMenuItem.Size = New System.Drawing.Size(282, 24)
        Me.ClientUploadToolStripMenuItem.Text = "Client Uploader"
        '
        'LoanForwardBalanceUploaderToolStripMenuItem
        '
        Me.LoanForwardBalanceUploaderToolStripMenuItem.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.LoanForwardBalanceUploaderToolStripMenuItem.Image = CType(resources.GetObject("LoanForwardBalanceUploaderToolStripMenuItem.Image"), System.Drawing.Image)
        Me.LoanForwardBalanceUploaderToolStripMenuItem.Name = "LoanForwardBalanceUploaderToolStripMenuItem"
        Me.LoanForwardBalanceUploaderToolStripMenuItem.Size = New System.Drawing.Size(282, 24)
        Me.LoanForwardBalanceUploaderToolStripMenuItem.Text = "Loan Forward Balance Uploader"
        '
        'LoanAmortizationUploaderToolStripMenuItem
        '
        Me.LoanAmortizationUploaderToolStripMenuItem.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.LoanAmortizationUploaderToolStripMenuItem.Image = CType(resources.GetObject("LoanAmortizationUploaderToolStripMenuItem.Image"), System.Drawing.Image)
        Me.LoanAmortizationUploaderToolStripMenuItem.Name = "LoanAmortizationUploaderToolStripMenuItem"
        Me.LoanAmortizationUploaderToolStripMenuItem.Size = New System.Drawing.Size(282, 24)
        Me.LoanAmortizationUploaderToolStripMenuItem.Text = "Loan Amortization Uploader"
        '
        'LoanUploader3ToolStripMenuItem
        '
        Me.LoanUploader3ToolStripMenuItem.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.LoanUploader3ToolStripMenuItem.Image = CType(resources.GetObject("LoanUploader3ToolStripMenuItem.Image"), System.Drawing.Image)
        Me.LoanUploader3ToolStripMenuItem.Name = "LoanUploader3ToolStripMenuItem"
        Me.LoanUploader3ToolStripMenuItem.Size = New System.Drawing.Size(282, 24)
        Me.LoanUploader3ToolStripMenuItem.Text = "Loan Uploader - 3"
        '
        'DownloaderToolStripMenuItem
        '
        Me.DownloaderToolStripMenuItem.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DownloaderToolStripMenuItem.Image = Global.WindowsApplication2.My.Resources.Resources.images__36_
        Me.DownloaderToolStripMenuItem.Name = "DownloaderToolStripMenuItem"
        Me.DownloaderToolStripMenuItem.Size = New System.Drawing.Size(282, 24)
        Me.DownloaderToolStripMenuItem.Text = "Data Export"
        '
        'ToolReports
        '
        Me.ToolReports.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.A1ReportsToolStripMenuItem, Me.A2ReportsToolStripMenuItem, Me.IndividualStatementsToolStripMenuItem, Me.PESOReportsToolStripMenuItem, Me.LoanReportsToolStripMenuItem, Me.ClientListReportToolStripMenuItem, Me.ClientsList2ToolStripMenuItem, Me.EmployeeInformationReportToolStripMenuItem, Me.HistoryReportToolStripMenuItem, Me.ContractSummaryPerCompanyToolStripMenuItem})
        Me.ToolReports.Image = Global.WindowsApplication2.My.Resources.Resources.images__47_
        Me.ToolReports.Name = "ToolReports"
        Me.ToolReports.Size = New System.Drawing.Size(79, 20)
        Me.ToolReports.Text = "&Reports"
        '
        'A1ReportsToolStripMenuItem
        '
        Me.A1ReportsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BalanceSheetToolStripMenuItem, Me.StatementOfNetSurplusToolStripMenuItem, Me.SummaryOfAgingReportsToolStripMenuItem, Me.StatementOfUtilizationOfStatutoryFundsToolStripMenuItem})
        Me.A1ReportsToolStripMenuItem.Name = "A1ReportsToolStripMenuItem"
        Me.A1ReportsToolStripMenuItem.Size = New System.Drawing.Size(281, 38)
        Me.A1ReportsToolStripMenuItem.Text = "A1 Reports"
        Me.A1ReportsToolStripMenuItem.Visible = False
        '
        'BalanceSheetToolStripMenuItem
        '
        Me.BalanceSheetToolStripMenuItem.Name = "BalanceSheetToolStripMenuItem"
        Me.BalanceSheetToolStripMenuItem.Size = New System.Drawing.Size(316, 22)
        Me.BalanceSheetToolStripMenuItem.Text = "Balance Sheet"
        '
        'StatementOfNetSurplusToolStripMenuItem
        '
        Me.StatementOfNetSurplusToolStripMenuItem.Name = "StatementOfNetSurplusToolStripMenuItem"
        Me.StatementOfNetSurplusToolStripMenuItem.Size = New System.Drawing.Size(316, 22)
        Me.StatementOfNetSurplusToolStripMenuItem.Text = "Statement of Net Surplus"
        '
        'SummaryOfAgingReportsToolStripMenuItem
        '
        Me.SummaryOfAgingReportsToolStripMenuItem.Checked = True
        Me.SummaryOfAgingReportsToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked
        Me.SummaryOfAgingReportsToolStripMenuItem.Name = "SummaryOfAgingReportsToolStripMenuItem"
        Me.SummaryOfAgingReportsToolStripMenuItem.Size = New System.Drawing.Size(316, 22)
        Me.SummaryOfAgingReportsToolStripMenuItem.Text = "Summary of Aging Reports"
        '
        'StatementOfUtilizationOfStatutoryFundsToolStripMenuItem
        '
        Me.StatementOfUtilizationOfStatutoryFundsToolStripMenuItem.Name = "StatementOfUtilizationOfStatutoryFundsToolStripMenuItem"
        Me.StatementOfUtilizationOfStatutoryFundsToolStripMenuItem.Size = New System.Drawing.Size(316, 22)
        Me.StatementOfUtilizationOfStatutoryFundsToolStripMenuItem.Text = "Statement of Utilization of Statutory Funds"
        '
        'A2ReportsToolStripMenuItem
        '
        Me.A2ReportsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.LoanReceivableAgingReportToolStripMenuItem, Me.ScheduleOfLoanReceivaToolStripMenuItem, Me.PaidUpCapitalShareToolStripMenuItem, Me.AccountsPayableToolStripMenuItem, Me.LoansPayableToolStripMenuItem, Me.GeneralLedgerToolStripMenuItem, Me.PropertyToolStripMenuItem})
        Me.A2ReportsToolStripMenuItem.Name = "A2ReportsToolStripMenuItem"
        Me.A2ReportsToolStripMenuItem.Size = New System.Drawing.Size(281, 38)
        Me.A2ReportsToolStripMenuItem.Text = "A2 Reports"
        Me.A2ReportsToolStripMenuItem.Visible = False
        '
        'LoanReceivableAgingReportToolStripMenuItem
        '
        Me.LoanReceivableAgingReportToolStripMenuItem.Checked = True
        Me.LoanReceivableAgingReportToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked
        Me.LoanReceivableAgingReportToolStripMenuItem.Name = "LoanReceivableAgingReportToolStripMenuItem"
        Me.LoanReceivableAgingReportToolStripMenuItem.Size = New System.Drawing.Size(250, 22)
        Me.LoanReceivableAgingReportToolStripMenuItem.Text = "Loan Receivable Aging Report"
        '
        'ScheduleOfLoanReceivaToolStripMenuItem
        '
        Me.ScheduleOfLoanReceivaToolStripMenuItem.Checked = True
        Me.ScheduleOfLoanReceivaToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ScheduleOfLoanReceivaToolStripMenuItem.Name = "ScheduleOfLoanReceivaToolStripMenuItem"
        Me.ScheduleOfLoanReceivaToolStripMenuItem.Size = New System.Drawing.Size(250, 22)
        Me.ScheduleOfLoanReceivaToolStripMenuItem.Text = "Loan Balance Report"
        '
        'PaidUpCapitalShareToolStripMenuItem
        '
        Me.PaidUpCapitalShareToolStripMenuItem.Checked = True
        Me.PaidUpCapitalShareToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked
        Me.PaidUpCapitalShareToolStripMenuItem.Name = "PaidUpCapitalShareToolStripMenuItem"
        Me.PaidUpCapitalShareToolStripMenuItem.Size = New System.Drawing.Size(250, 22)
        Me.PaidUpCapitalShareToolStripMenuItem.Text = "Paid Up Capital Share"
        '
        'AccountsPayableToolStripMenuItem
        '
        Me.AccountsPayableToolStripMenuItem.Name = "AccountsPayableToolStripMenuItem"
        Me.AccountsPayableToolStripMenuItem.Size = New System.Drawing.Size(250, 22)
        Me.AccountsPayableToolStripMenuItem.Text = "Accounts Payable"
        '
        'LoansPayableToolStripMenuItem
        '
        Me.LoansPayableToolStripMenuItem.Name = "LoansPayableToolStripMenuItem"
        Me.LoansPayableToolStripMenuItem.Size = New System.Drawing.Size(250, 22)
        Me.LoansPayableToolStripMenuItem.Text = "Loans Payable"
        '
        'GeneralLedgerToolStripMenuItem
        '
        Me.GeneralLedgerToolStripMenuItem.Name = "GeneralLedgerToolStripMenuItem"
        Me.GeneralLedgerToolStripMenuItem.Size = New System.Drawing.Size(250, 22)
        Me.GeneralLedgerToolStripMenuItem.Text = "General Ledger Activity Report"
        '
        'PropertyToolStripMenuItem
        '
        Me.PropertyToolStripMenuItem.Name = "PropertyToolStripMenuItem"
        Me.PropertyToolStripMenuItem.Size = New System.Drawing.Size(250, 22)
        Me.PropertyToolStripMenuItem.Text = "Property and Equipment"
        '
        'IndividualStatementsToolStripMenuItem
        '
        Me.IndividualStatementsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CapitalShareToolStripMenuItem, Me.LoansToolStripMenuItem, Me.SavingsAndToolStripMenuItem, Me.AverageCapitalShareAndDividendsToolStripMenuItem})
        Me.IndividualStatementsToolStripMenuItem.Name = "IndividualStatementsToolStripMenuItem"
        Me.IndividualStatementsToolStripMenuItem.Size = New System.Drawing.Size(281, 38)
        Me.IndividualStatementsToolStripMenuItem.Text = "Individual Statements"
        Me.IndividualStatementsToolStripMenuItem.Visible = False
        '
        'CapitalShareToolStripMenuItem
        '
        Me.CapitalShareToolStripMenuItem.Checked = True
        Me.CapitalShareToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CapitalShareToolStripMenuItem.Name = "CapitalShareToolStripMenuItem"
        Me.CapitalShareToolStripMenuItem.Size = New System.Drawing.Size(290, 22)
        Me.CapitalShareToolStripMenuItem.Text = "Capital Share"
        '
        'LoansToolStripMenuItem
        '
        Me.LoansToolStripMenuItem.Checked = True
        Me.LoansToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked
        Me.LoansToolStripMenuItem.Name = "LoansToolStripMenuItem"
        Me.LoansToolStripMenuItem.Size = New System.Drawing.Size(290, 22)
        Me.LoansToolStripMenuItem.Text = "Loans and Statement of Accounts"
        '
        'SavingsAndToolStripMenuItem
        '
        Me.SavingsAndToolStripMenuItem.Name = "SavingsAndToolStripMenuItem"
        Me.SavingsAndToolStripMenuItem.Size = New System.Drawing.Size(290, 22)
        Me.SavingsAndToolStripMenuItem.Text = "Savings And Time Deposit"
        Me.SavingsAndToolStripMenuItem.Visible = False
        '
        'AverageCapitalShareAndDividendsToolStripMenuItem
        '
        Me.AverageCapitalShareAndDividendsToolStripMenuItem.Checked = True
        Me.AverageCapitalShareAndDividendsToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked
        Me.AverageCapitalShareAndDividendsToolStripMenuItem.Name = "AverageCapitalShareAndDividendsToolStripMenuItem"
        Me.AverageCapitalShareAndDividendsToolStripMenuItem.Size = New System.Drawing.Size(290, 22)
        Me.AverageCapitalShareAndDividendsToolStripMenuItem.Text = "Average Capital Share And Dividends"
        '
        'PESOReportsToolStripMenuItem
        '
        Me.PESOReportsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CAPRToolStripMenuItem})
        Me.PESOReportsToolStripMenuItem.Name = "PESOReportsToolStripMenuItem"
        Me.PESOReportsToolStripMenuItem.Size = New System.Drawing.Size(281, 38)
        Me.PESOReportsToolStripMenuItem.Text = "PESO Reports"
        Me.PESOReportsToolStripMenuItem.Visible = False
        '
        'CAPRToolStripMenuItem
        '
        Me.CAPRToolStripMenuItem.Checked = True
        Me.CAPRToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CAPRToolStripMenuItem.Name = "CAPRToolStripMenuItem"
        Me.CAPRToolStripMenuItem.Size = New System.Drawing.Size(306, 22)
        Me.CAPRToolStripMenuItem.Text = "Cooperative Annual Performance Report"
        '
        'LoanReportsToolStripMenuItem
        '
        Me.LoanReportsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ScheduleOfLoansReceivableToolStripMenuItem, Me.ScheduleOfLoansRToolStripMenuItem, Me.LoanStatusReportToolStripMenuItem})
        Me.LoanReportsToolStripMenuItem.Image = Global.WindowsApplication2.My.Resources.Resources.check
        Me.LoanReportsToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.LoanReportsToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.LoanReportsToolStripMenuItem.Name = "LoanReportsToolStripMenuItem"
        Me.LoanReportsToolStripMenuItem.Size = New System.Drawing.Size(281, 38)
        Me.LoanReportsToolStripMenuItem.Text = "Loan Reports"
        '
        'ScheduleOfLoansReceivableToolStripMenuItem
        '
        Me.ScheduleOfLoansReceivableToolStripMenuItem.Name = "ScheduleOfLoansReceivableToolStripMenuItem"
        Me.ScheduleOfLoansReceivableToolStripMenuItem.Size = New System.Drawing.Size(329, 22)
        Me.ScheduleOfLoansReceivableToolStripMenuItem.Text = "Schedule of Loans Receivable - All Types"
        '
        'ScheduleOfLoansRToolStripMenuItem
        '
        Me.ScheduleOfLoansRToolStripMenuItem.Name = "ScheduleOfLoansRToolStripMenuItem"
        Me.ScheduleOfLoansRToolStripMenuItem.Size = New System.Drawing.Size(329, 22)
        Me.ScheduleOfLoansRToolStripMenuItem.Text = "Schedule of Loans Receivable - By Category"
        '
        'LoanStatusReportToolStripMenuItem
        '
        Me.LoanStatusReportToolStripMenuItem.Checked = True
        Me.LoanStatusReportToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked
        Me.LoanStatusReportToolStripMenuItem.Name = "LoanStatusReportToolStripMenuItem"
        Me.LoanStatusReportToolStripMenuItem.Size = New System.Drawing.Size(329, 22)
        Me.LoanStatusReportToolStripMenuItem.Text = "Loan Status Report"
        Me.LoanStatusReportToolStripMenuItem.Visible = False
        '
        'ClientListReportToolStripMenuItem
        '
        Me.ClientListReportToolStripMenuItem.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ClientListReportToolStripMenuItem.Image = CType(resources.GetObject("ClientListReportToolStripMenuItem.Image"), System.Drawing.Image)
        Me.ClientListReportToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ClientListReportToolStripMenuItem.Name = "ClientListReportToolStripMenuItem"
        Me.ClientListReportToolStripMenuItem.Size = New System.Drawing.Size(281, 38)
        Me.ClientListReportToolStripMenuItem.Text = "Clients List"
        '
        'ClientsList2ToolStripMenuItem
        '
        Me.ClientsList2ToolStripMenuItem.Font = New System.Drawing.Font("Verdana", 9.75!)
        Me.ClientsList2ToolStripMenuItem.Image = CType(resources.GetObject("ClientsList2ToolStripMenuItem.Image"), System.Drawing.Image)
        Me.ClientsList2ToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ClientsList2ToolStripMenuItem.Name = "ClientsList2ToolStripMenuItem"
        Me.ClientsList2ToolStripMenuItem.Size = New System.Drawing.Size(281, 38)
        Me.ClientsList2ToolStripMenuItem.Text = "Clients List - 2"
        '
        'EmployeeInformationReportToolStripMenuItem
        '
        Me.EmployeeInformationReportToolStripMenuItem.Image = Global.WindowsApplication2.My.Resources.Resources.lists
        Me.EmployeeInformationReportToolStripMenuItem.Name = "EmployeeInformationReportToolStripMenuItem"
        Me.EmployeeInformationReportToolStripMenuItem.Size = New System.Drawing.Size(281, 38)
        Me.EmployeeInformationReportToolStripMenuItem.Text = "Employee InformationReport"
        '
        'HistoryReportToolStripMenuItem
        '
        Me.HistoryReportToolStripMenuItem.Image = Global.WindowsApplication2.My.Resources.Resources.images__37_
        Me.HistoryReportToolStripMenuItem.Name = "HistoryReportToolStripMenuItem"
        Me.HistoryReportToolStripMenuItem.Size = New System.Drawing.Size(281, 38)
        Me.HistoryReportToolStripMenuItem.Text = "History Report"
        '
        'ContractSummaryPerCompanyToolStripMenuItem
        '
        Me.ContractSummaryPerCompanyToolStripMenuItem.Image = Global.WindowsApplication2.My.Resources.Resources.news
        Me.ContractSummaryPerCompanyToolStripMenuItem.Name = "ContractSummaryPerCompanyToolStripMenuItem"
        Me.ContractSummaryPerCompanyToolStripMenuItem.Size = New System.Drawing.Size(281, 38)
        Me.ContractSummaryPerCompanyToolStripMenuItem.Text = "Contract Summary per Company"
        '
        'IntegratorToolStrip
        '
        Me.IntegratorToolStrip.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ClientsIntegritorToolStripMenuItem})
        Me.IntegratorToolStrip.Image = CType(resources.GetObject("IntegratorToolStrip.Image"), System.Drawing.Image)
        Me.IntegratorToolStrip.Name = "IntegratorToolStrip"
        Me.IntegratorToolStrip.Size = New System.Drawing.Size(136, 20)
        Me.IntegratorToolStrip.Text = "Payroll Integrator"
        '
        'ClientsIntegritorToolStripMenuItem
        '
        Me.ClientsIntegritorToolStripMenuItem.Image = CType(resources.GetObject("ClientsIntegritorToolStripMenuItem.Image"), System.Drawing.Image)
        Me.ClientsIntegritorToolStripMenuItem.Name = "ClientsIntegritorToolStripMenuItem"
        Me.ClientsIntegritorToolStripMenuItem.Size = New System.Drawing.Size(165, 22)
        Me.ClientsIntegritorToolStripMenuItem.Text = "Transfer Clients"
        '
        'SecurityToolStripMenuItem
        '
        Me.SecurityToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.UserToolStripMenuItem, Me.ChangePasswordToolStripMenuItem, Me.RightsToolStripMenuItem, Me.ToolStripSeparator3, Me.CompanyInformationToolStripMenuItem})
        Me.SecurityToolStripMenuItem.Image = CType(resources.GetObject("SecurityToolStripMenuItem.Image"), System.Drawing.Image)
        Me.SecurityToolStripMenuItem.Name = "SecurityToolStripMenuItem"
        Me.SecurityToolStripMenuItem.Size = New System.Drawing.Size(81, 20)
        Me.SecurityToolStripMenuItem.Text = "&Settings"
        '
        'UserToolStripMenuItem
        '
        Me.UserToolStripMenuItem.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UserToolStripMenuItem.Image = CType(resources.GetObject("UserToolStripMenuItem.Image"), System.Drawing.Image)
        Me.UserToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.UserToolStripMenuItem.Name = "UserToolStripMenuItem"
        Me.UserToolStripMenuItem.Size = New System.Drawing.Size(215, 24)
        Me.UserToolStripMenuItem.Text = "Create &User"
        '
        'ChangePasswordToolStripMenuItem
        '
        Me.ChangePasswordToolStripMenuItem.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.ChangePasswordToolStripMenuItem.Image = CType(resources.GetObject("ChangePasswordToolStripMenuItem.Image"), System.Drawing.Image)
        Me.ChangePasswordToolStripMenuItem.Name = "ChangePasswordToolStripMenuItem"
        Me.ChangePasswordToolStripMenuItem.Size = New System.Drawing.Size(215, 24)
        Me.ChangePasswordToolStripMenuItem.Text = "Change Password"
        '
        'RightsToolStripMenuItem
        '
        Me.RightsToolStripMenuItem.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.RightsToolStripMenuItem.Image = CType(resources.GetObject("RightsToolStripMenuItem.Image"), System.Drawing.Image)
        Me.RightsToolStripMenuItem.Name = "RightsToolStripMenuItem"
        Me.RightsToolStripMenuItem.Size = New System.Drawing.Size(215, 24)
        Me.RightsToolStripMenuItem.Text = "User Rights"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(212, 6)
        '
        'CompanyInformationToolStripMenuItem
        '
        Me.CompanyInformationToolStripMenuItem.Font = New System.Drawing.Font("Calibri", 12.0!)
        Me.CompanyInformationToolStripMenuItem.Image = CType(resources.GetObject("CompanyInformationToolStripMenuItem.Image"), System.Drawing.Image)
        Me.CompanyInformationToolStripMenuItem.Name = "CompanyInformationToolStripMenuItem"
        Me.CompanyInformationToolStripMenuItem.Size = New System.Drawing.Size(215, 24)
        Me.CompanyInformationToolStripMenuItem.Text = "Company Information"
        '
        'Contribution
        '
        Me.Contribution.Checked = True
        Me.Contribution.CheckOnClick = True
        Me.Contribution.CheckState = System.Windows.Forms.CheckState.Checked
        Me.Contribution.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MemberContributionToolStripMenuItem, Me.CapitalShareDepositAndWithdrawalToolStripMenuItem})
        Me.Contribution.Name = "Contribution"
        Me.Contribution.Size = New System.Drawing.Size(89, 20)
        Me.Contribution.Text = "&Contribution"
        Me.Contribution.Visible = False
        '
        'MemberContributionToolStripMenuItem
        '
        Me.MemberContributionToolStripMenuItem.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MemberContributionToolStripMenuItem.Image = Global.WindowsApplication2.My.Resources.Resources.financial_services
        Me.MemberContributionToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.MemberContributionToolStripMenuItem.Name = "MemberContributionToolStripMenuItem"
        Me.MemberContributionToolStripMenuItem.Size = New System.Drawing.Size(355, 38)
        Me.MemberContributionToolStripMenuItem.Text = "Member &Contribution"
        '
        'CapitalShareDepositAndWithdrawalToolStripMenuItem
        '
        Me.CapitalShareDepositAndWithdrawalToolStripMenuItem.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold)
        Me.CapitalShareDepositAndWithdrawalToolStripMenuItem.Name = "CapitalShareDepositAndWithdrawalToolStripMenuItem"
        Me.CapitalShareDepositAndWithdrawalToolStripMenuItem.Size = New System.Drawing.Size(355, 38)
        Me.CapitalShareDepositAndWithdrawalToolStripMenuItem.Text = "Capital Share Deposit And Withdrawal"
        Me.CapitalShareDepositAndWithdrawalToolStripMenuItem.Visible = False
        '
        'HelpToolStripMenuItem
        '
        Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
        Me.HelpToolStripMenuItem.Size = New System.Drawing.Size(52, 20)
        Me.HelpToolStripMenuItem.Text = "About"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.StLoginName, Me.StTime, Me.Stdate, Me.StComputername, Me.Stcomputeruser})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 575)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(1028, 22)
        Me.StatusStrip1.TabIndex = 35
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'StLoginName
        '
        Me.StLoginName.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.StLoginName.Image = Global.WindowsApplication2.My.Resources.Resources.men
        Me.StLoginName.Name = "StLoginName"
        Me.StLoginName.Size = New System.Drawing.Size(91, 17)
        Me.StLoginName.Text = "Login Name:"
        '
        'StTime
        '
        Me.StTime.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.StTime.Image = CType(resources.GetObject("StTime.Image"), System.Drawing.Image)
        Me.StTime.Name = "StTime"
        Me.StTime.Size = New System.Drawing.Size(56, 17)
        Me.StTime.Text = "Time :"
        '
        'Stdate
        '
        Me.Stdate.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.Stdate.Image = CType(resources.GetObject("Stdate.Image"), System.Drawing.Image)
        Me.Stdate.Name = "Stdate"
        Me.Stdate.Size = New System.Drawing.Size(53, 17)
        Me.Stdate.Text = "Date :"
        '
        'StComputername
        '
        Me.StComputername.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.StComputername.Image = CType(resources.GetObject("StComputername.Image"), System.Drawing.Image)
        Me.StComputername.Name = "StComputername"
        Me.StComputername.Size = New System.Drawing.Size(121, 17)
        Me.StComputername.Text = "Computer  Name :"
        '
        'Stcomputeruser
        '
        Me.Stcomputeruser.Image = CType(resources.GetObject("Stcomputeruser.Image"), System.Drawing.Image)
        Me.Stcomputeruser.Name = "Stcomputeruser"
        Me.Stcomputeruser.Size = New System.Drawing.Size(109, 17)
        Me.Stcomputeruser.Text = "Computer User :"
        '
        'PrintDialog1
        '
        Me.PrintDialog1.UseEXDialog = True
        '
        'bgwDatabaseFunctions
        '
        '
        'btnShowHideSubmenu
        '
        Me.btnShowHideSubmenu.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnShowHideSubmenu.BackColor = System.Drawing.Color.ForestGreen
        Me.btnShowHideSubmenu.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnShowHideSubmenu.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnShowHideSubmenu.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnShowHideSubmenu.ForeColor = System.Drawing.Color.White
        Me.btnShowHideSubmenu.Location = New System.Drawing.Point(978, 3)
        Me.btnShowHideSubmenu.Name = "btnShowHideSubmenu"
        Me.btnShowHideSubmenu.Size = New System.Drawing.Size(53, 23)
        Me.btnShowHideSubmenu.TabIndex = 42
        Me.btnShowHideSubmenu.Text = "Hide"
        Me.btnShowHideSubmenu.UseVisualStyleBackColor = True
        '
        'lblCompanyTitle
        '
        Me.lblCompanyTitle.AutoSize = True
        Me.lblCompanyTitle.BackColor = System.Drawing.Color.Transparent
        Me.lblCompanyTitle.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCompanyTitle.ForeColor = System.Drawing.SystemColors.Info
        Me.lblCompanyTitle.Location = New System.Drawing.Point(12, 2)
        Me.lblCompanyTitle.Name = "lblCompanyTitle"
        Me.lblCompanyTitle.Size = New System.Drawing.Size(25, 19)
        Me.lblCompanyTitle.TabIndex = 21
        Me.lblCompanyTitle.Text = "**"
        '
        'AcclExplorerBar1
        '
        Me.AcclExplorerBar1.AnimateStateChanges = True
        Me.AcclExplorerBar1.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.AcclExplorerBar1.BackColorEnd = System.Drawing.Color.Transparent
        Me.AcclExplorerBar1.BackColorStart = System.Drawing.Color.Transparent
        Me.AcclExplorerBar1.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.AcclExplorerBar1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.AcclExplorerBar1.Dock = System.Windows.Forms.DockStyle.Left
        Me.AcclExplorerBar1.DrawingStyle = vbAccelerator.Components.Controls.ExplorerBarDrawingStyle.Custom
        Me.AcclExplorerBar1.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AcclExplorerBar1.ImageList = Me.imglBars
        Me.AcclExplorerBar1.Location = New System.Drawing.Point(0, 24)
        Me.AcclExplorerBar1.Mode = vbAccelerator.Components.Controls.ExplorerBarMode.[Default]
        Me.AcclExplorerBar1.Name = "AcclExplorerBar1"
        Me.AcclExplorerBar1.Redraw = True
        Me.AcclExplorerBar1.ShowFocusRect = True
        Me.AcclExplorerBar1.Size = New System.Drawing.Size(180, 551)
        Me.AcclExplorerBar1.TabIndex = 22
        Me.AcclExplorerBar1.TitleImageList = Me.imglBars
        Me.AcclExplorerBar1.ToolTip = Nothing
        '
        'PictureBox3
        '
        Me.PictureBox3.Image = CType(resources.GetObject("PictureBox3.Image"), System.Drawing.Image)
        Me.PictureBox3.Location = New System.Drawing.Point(0, 48)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(42, 31)
        Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox3.TabIndex = 31
        Me.PictureBox3.TabStop = False
        Me.PictureBox3.Visible = False
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(77, 118)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(74, 65)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 24
        Me.PictureBox2.TabStop = False
        Me.PictureBox2.Visible = False
        '
        'PanePanel5
        '
        Me.PanePanel5.BackColor = System.Drawing.Color.White
        Me.PanePanel5.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.PanePanel5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PanePanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel5.Controls.Add(Me.lblCoopName)
        Me.PanePanel5.Controls.Add(Me.btnEnteng)
        Me.PanePanel5.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanePanel5.Font = New System.Drawing.Font("Verdana", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PanePanel5.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel5.InactiveGradientHighColor = System.Drawing.Color.Transparent
        Me.PanePanel5.InactiveGradientLowColor = System.Drawing.Color.Transparent
        Me.PanePanel5.Location = New System.Drawing.Point(180, 24)
        Me.PanePanel5.Name = "PanePanel5"
        Me.PanePanel5.Size = New System.Drawing.Size(848, 32)
        Me.PanePanel5.TabIndex = 57
        '
        'lblCoopName
        '
        Me.lblCoopName.AutoSize = True
        Me.lblCoopName.BackColor = System.Drawing.Color.Transparent
        Me.lblCoopName.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCoopName.ForeColor = System.Drawing.Color.White
        Me.lblCoopName.Location = New System.Drawing.Point(5, 7)
        Me.lblCoopName.Name = "lblCoopName"
        Me.lblCoopName.Size = New System.Drawing.Size(56, 16)
        Me.lblCoopName.TabIndex = 1
        Me.lblCoopName.Text = "Label1"
        '
        'btnEnteng
        '
        Me.btnEnteng.BackColor = System.Drawing.Color.Transparent
        Me.btnEnteng.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnEnteng.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnEnteng.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEnteng.Location = New System.Drawing.Point(700, 0)
        Me.btnEnteng.Name = "btnEnteng"
        Me.btnEnteng.Size = New System.Drawing.Size(146, 30)
        Me.btnEnteng.TabIndex = 0
        Me.btnEnteng.Text = "HIDE SIDE BAR"
        Me.btnEnteng.UseVisualStyleBackColor = False
        '
        'frmMain
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BackColor = System.Drawing.Color.White
        Me.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.CausesValidation = False
        Me.ClientSize = New System.Drawing.Size(1028, 597)
        Me.Controls.Add(Me.PanePanel5)
        Me.Controls.Add(Me.AcclExplorerBar1)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.PictureBox3)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.StrJobRequisition)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ForeColor = System.Drawing.Color.Black
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.IsMdiContainer = True
        Me.Name = "frmMain"
        Me.Text = "UCore Client Management System"
        Me.TransparencyKey = System.Drawing.Color.White
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StrJobRequisition.ResumeLayout(False)
        Me.StrJobRequisition.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanePanel5.ResumeLayout(False)
        Me.PanePanel5.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region
#Region "Variables"
    Public username As String
    Public ADMIN1 As String

    Private gFilePath As String = ""
    Private gcon As New Clsappconfiguration
#End Region

#Region "Controls"
    Private Shared Function GetProgress() As ProgressBar
        Dim progress As New ProgressBar()
        Return progress
    End Function
#End Region

#Region "Get property"
    Public Property getusername() As String
        Get
            Return username
        End Get
        Set(ByVal value As String)
            username = value
        End Set
    End Property

    Private loggedInUserID As Integer
    Public Property GetLoggedInUserID() As Integer
        Get
            Return loggedInUserID
        End Get
        Set(ByVal value As Integer)
            loggedInUserID = value
        End Set
    End Property

#End Region

#Region "Deposits"
    Public Sub ListDepositsSidemenus()
        Dim deposit As ExplorerBarLinkItem
        deposit = New ExplorerBarLinkItem
        With deposit
            .Bold = False
            .Tag = "Deposits"
            .Text = "Account Inquiry"
        End With
        AcclExplorerBar1.Bars(3).Items.Add(deposit)
    End Sub
#End Region

#Region "Contributions"
    Public Sub ListContributionSubmenus()
        Dim contribution As ExplorerBarLinkItem
        contribution = New ExplorerBarLinkItem
        With contribution
            .Bold = False
            .Tag = "Contribution"
            .Text = "Member Contribution"
        End With
        AcclExplorerBar1.Bars(5).Items.Add(contribution)
    End Sub
#End Region

#Region "Report logon function"
    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If

        Dim obj As ReportObject

        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function
    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        ' for each table apply connection info
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            ' check if logon was successful
            ' if TestConnectivity returns false,
            ' check logon credentials
            If (tbl.TestConnectivity()) Then
                'drop fully qualified table location
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next

        Return True
    End Function
#End Region

#Region "Other Events and Functions"
    Private Sub Form1_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If MessageBox.Show("Are your sure you want to exit?", "Close Program", MessageBoxButtons.OKCancel, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.OK Then
            frmWait.Show()
        Else
            e.Cancel = True
        End If
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LoadCooperativeName() 'enteng

        Me.BackColor = Color.White
        If Me.StatusStrip1.Items(1).Name = "StTime" Then
            'StTime.Text = "Time : " + TimeOfDay + " | "
        End If
        If Me.StatusStrip1.Items(2).Name = "Stdate" Then
            Stdate.Text = "Date : " + Format(Date.Now, "MM/dd/yyyy") + " | "
        End If
        If Me.StatusStrip1.Items(3).Name = "StComputername" Then
            StComputername.Text = "Computer Name : " + My.Computer.Name + " | "
        End If

        If Me.StatusStrip1.Items(4).Name = "Stcomputeruser" Then
            Stcomputeruser.Text = "Computer User : " + GetCurrent.Name
        End If
        Me.Timer1.Start()

        Call load_configurationfile()

        'Load the side menu on the left part
        Call LoadSideBar()
        Call UserRightsModules()

        'frmPaymentNotification.StartPosition = FormStartPosition.CenterScreen
        'frmPaymentNotification.ShowDialog()
    End Sub
    Private Sub LoadSideBar()
        Dim masterFile As New ExplorerBar
        Dim memberManager As New ExplorerBar
        Dim loanManager As New ExplorerBar
        Dim organizational As New ExplorerBar
        Dim Info As New ExplorerBar
        Dim deposits As New ExplorerBar
        'Dim contributions As New ExplorerBar
        'Dim Payment As New ExplorerBar

        With AcclExplorerBar1
            .Bars.Add(masterFile)
            .Bars.Add(memberManager)
            .Bars.Add(loanManager)
            '.Bars.Add(Payment)
            .Bars.Add(deposits)
            ' .Bars.Add(contributions)
            .Bars.Add(organizational)
            .Bars.Add(Info)


            With masterFile
                '.BackColor = Color.AliceBlue
                .BackColor = Color.CornflowerBlue
                .TitleBackColorStart = Color.SteelBlue
                '.TitleBackColorEnd = Color.LightSkyBlue
                .TitleBackColorEnd = Color.SteelBlue
                .TitleForeColor = Color.Ivory
                .TitleForeColorHot = Color.DarkOrange
                .Text = "Master File"
                .IsSpecial = True
                .ToolTipText = "Master File"
                .State = ExplorerBarState.Collapsed
                .WatermarkMode = ExplorerBarWatermarkMode.Colourise
                .WatermarkVAlign = ExplorerBarWatermarkAlignment.Center
                .IconIndex = 0
            End With

            With memberManager
                .BackColor = Color.AliceBlue
                .TitleBackColorStart = Color.DarkBlue
                .TitleBackColorEnd = Color.LightSkyBlue
                .TitleForeColor = Color.Ivory
                .TitleForeColorHot = Color.DarkOrange
                .Text = "Client Manager"
                .IsSpecial = True
                .ToolTipText = "Contains Employee Master File Screens"
                .State = ExplorerBarState.Collapsed
                .WatermarkMode = ExplorerBarWatermarkMode.Colourise
                .WatermarkVAlign = ExplorerBarWatermarkAlignment.Center
                .IconIndex = 0

            End With
            With organizational
                .BackColor = Color.AliceBlue
                .TitleBackColorStart = Color.DarkBlue
                .TitleBackColorEnd = Color.LightSkyBlue
                .TitleForeColor = Color.Ivory
                .TitleForeColorHot = Color.DarkOrange
                .Text = "Organization"
                .IsSpecial = True
                .ToolTipText = "Contains Organizational Chart Screens"
                .State = ExplorerBarState.Collapsed
                .WatermarkMode = ExplorerBarWatermarkMode.Colourise
                .IconIndex = 4
            End With

            With Info
                .BackColor = Color.AliceBlue
                .TitleBackColorStart = Color.DarkBlue
                .TitleBackColorEnd = Color.LightSkyBlue
                .TitleForeColor = Color.Ivory
                .TitleForeColorHot = Color.DarkOrange
                .Text = "About"
                .IsSpecial = True
                .ToolTipText = "Contains Information About the System "
                .State = ExplorerBarState.Expanded
                .WatermarkMode = ExplorerBarWatermarkMode.Colourise
                .WatermarkVAlign = ExplorerBarWatermarkAlignment.Center
                .IconIndex = 10
            End With

            With loanManager
                .BackColor = Color.AliceBlue
                .TitleBackColorStart = Color.DarkBlue
                .TitleBackColorEnd = Color.LightSkyBlue
                .TitleForeColor = Color.Ivory
                .TitleForeColorHot = Color.DarkOrange
                .Text = "Loan Manager"
                .IsSpecial = True
                .ToolTipText = "Contains Loan Information"
                .State = ExplorerBarState.Expanded
                '.Watermark = PictureBox2.Image
                '.WatermarkMode = ExplorerBarWatermarkMode.Direct
                PictureBox2.Visible = False
                .IconIndex = 3
            End With

            With deposits
                .BackColor = Color.AliceBlue
                .TitleBackColorStart = Color.DarkBlue
                .TitleBackColorEnd = Color.LightSkyBlue
                .TitleForeColor = Color.Ivory
                .TitleForeColorHot = Color.DarkOrange
                .Text = "Account Inquiry"
                .IsSpecial = True
                .ToolTipText = "Contains Inquiry"
                .State = ExplorerBarState.Collapsed
                .WatermarkMode = ExplorerBarWatermarkMode.Colourise
                .IconIndex = 9
            End With

            'With contributions
            '    .BackColor = Color.AliceBlue
            '    .TitleBackColorStart = Color.DarkBlue
            '    .TitleBackColorEnd = Color.LightSkyBlue
            '    .TitleForeColor = Color.Ivory
            '    .TitleForeColorHot = Color.DarkOrange
            '    .Text = "Contributions"
            '    .IsSpecial = True
            '    .ToolTipText = "Contains Contribution"
            '    .State = ExplorerBarState.Collapsed
            '    .WatermarkMode = ExplorerBarWatermarkMode.Colourise
            '    .IconIndex = 8
            'End With

            'With Payment
            '    .BackColor = Color.AliceBlue
            '    .TitleBackColorStart = Color.DarkBlue
            '    .TitleBackColorEnd = Color.LightSkyBlue
            '    .TitleForeColor = Color.Ivory
            '    .TitleForeColorHot = Color.DarkOrange
            '    .Text = "Transaction"
            '    .IsSpecial = True
            '    .ToolTipText = "Contains Transaction"
            '    .State = ExplorerBarState.Expanded
            '    .WatermarkMode = ExplorerBarWatermarkMode.Colourise
            '    .IconIndex = 13
            'End With

        End With

        'Loads Submenus 
        Call ListMasterfileSubmenus()
        Call ListMemberManagerSubmenus()
        Call ListLoanManagerSubmenus()
        Call ListDepositsSidemenus()
        'Call ListContributionSubmenus()
        Call ListOrganizationalStructureSubmenus()
        Call ListOtherInfoSubmenus()
        'Call ListPaymentSubmenus()
    End Sub
    Public Sub masterfile1()
        Dim Positionmasterfile As New ExplorerBarLinkItem
        Dim employeetype As New ExplorerBarLinkItem
        Dim payschedule As New ExplorerBarLinkItem
        Dim shiftschedule As New ExplorerBarLinkItem
        Dim taxcode As New ExplorerBarLinkItem
        Dim ratetype As New ExplorerBarLinkItem
        Dim jobrank As New ExplorerBarLinkItem
        Dim compensation As New ExplorerBarLinkItem
        Dim benefitmaster As New ExplorerBarLinkItem

        With Positionmasterfile
            .Bold = False
            .Tag = "positionmasterfile"
            .Text = "  Employee Position"
        End With

        With employeetype
            .Bold = False
            .Tag = "employeetype"
            .Text = "  Employee Type"
        End With

        With payschedule
            .Bold = False
            .Tag = "payschedule"
            .Text = "  Pay Category"
        End With
        With shiftschedule
            .Bold = False
            .Tag = "shiftschedule"
            .Text = "  Shift"
        End With
        With taxcode
            .Bold = False
            .Tag = "taxcode"
            .Text = "  Tax Code"
        End With
        With ratetype
            .Bold = False
            .Tag = "ratetype"
            .Text = "  Rate Type"
        End With
        With jobrank
            .Bold = False
            .Tag = "jobrank"
            .Text = "  Job Rank"
        End With
        With compensation
            .Bold = False
            .Tag = "compensation"
            .Text = "  Salary Grade"
        End With

        With benefitmaster
            .Bold = False
            .Tag = "benefitmaster"
            .Text = "  Benefit Master"
        End With

        AcclExplorerBar1.Bars(0).Items.Add(Positionmasterfile)
        AcclExplorerBar1.Bars(0).Items.Add(employeetype)
        AcclExplorerBar1.Bars(0).Items.Add(payschedule)
        AcclExplorerBar1.Bars(0).Items.Add(shiftschedule)
        AcclExplorerBar1.Bars(0).Items.Add(taxcode)
        AcclExplorerBar1.Bars(0).Items.Add(ratetype)
        AcclExplorerBar1.Bars(0).Items.Add(jobrank)
        AcclExplorerBar1.Bars(0).Items.Add(compensation)
        AcclExplorerBar1.Bars(0).Items.Add(benefitmaster)
    End Sub
    'FIRST SIDE MENU
    Public Sub ListMasterfileSubmenus()
        Dim PositionMaster As New ExplorerBarLinkItem
        Dim PayrollCodeMaster As New ExplorerBarLinkItem
        Dim paycode As New ExplorerBarLinkItem
        Dim PayCalendar As New ExplorerBarLinkItem
        Dim payrollintegrator As New ExplorerBarLinkItem
        Dim MemberStatus As New ExplorerBarLinkItem

        Dim Group As New ExplorerBarLinkItem
        Dim subgroup As New ExplorerBarLinkItem
        Dim Category As New ExplorerBarLinkItem
        Dim Type As New ExplorerBarLinkItem
        Dim rank As New ExplorerBarLinkItem

        Dim LoanType As New ExplorerBarLinkItem
        Dim policies As New ExplorerBarLinkItem
        '  Dim pagibig As New ExplorerBarLinkItem
        '  Dim philhealth As New ExplorerBarLinkItem
        '  Dim SSS As New ExplorerBarLinkItem
        Dim withholdingtax As New ExplorerBarLinkItem
        Dim approver As New ExplorerBarLinkItem
        'Dim TaxCode As New ExplorerBarLinkItem
        'Dim applicantReqt As New ExplorerBarLinkItem

        'With applicantReqt
        '    .Bold = False
        '    .Tag = "ApplicantReqt"
        '    .Text = "Applicant Reqts"
        '    .ForeColor = Color.White
        '    '.IconIndex = 6
        'End With

        With PositionMaster
            .Bold = False
            .Tag = "PositionMaster"
            .Text = "Position"
            .ForeColor = Color.White
            '.IconIndex = 6
        End With

        With PayrollCodeMaster
            .Bold = False
            .Tag = "PayrollCodeMaster"
            .Text = "Payroll Code"
            .ForeColor = Color.White
            '.IconIndex = 5
        End With

        With paycode
            .Bold = False
            .Tag = "paycode"
            .Text = "Pay Code"
            .ForeColor = Color.White
            '.IconIndex = 5
        End With

        With PayCalendar
            .Bold = False
            .Tag = "PayCalendar"
            .Text = "Pay Calendar"
            .ForeColor = Color.White
            '.IconIndex = 5
        End With

        With payrollintegrator
            .Bold = False
            .Tag = "payrollintegrator"
            .Text = "Payroll Integrator"
            .ForeColor = Color.White
            '.IconIndex = 5
        End With


        With LoanType
            .Bold = False
            .Tag = "LoanType"
            .Text = "Loan Type"
            .ForeColor = Color.White
            '.IconIndex = 5
        End With

        With policies
            .Bold = False
            .Tag = "Policies"
            .Text = "Policy"
            .ForeColor = Color.White
            '.IconIndex = 5
        End With

        'With pagibig
        '    .Bold = False
        '    .Tag = "Pagibig"
        '    .Text = "Pag-ibig"
        '    .ForeColor = Color.White
        '    '.IconIndex = 5
        'End With
        'With philhealth
        '    .Bold = False
        '    .Tag = "philhealth"
        '    .Text = "Philhealth"
        '    .ForeColor = Color.White
        '    '.IconIndex = 5
        'End With
        'With SSS
        '    .Bold = False
        '    .Tag = "SSS"
        '    .Text = "SSS"
        '    .ForeColor = Color.White
        '    '.IconIndex = 5
        'End With

        With withholdingtax
            .Bold = False
            .Tag = "Withholdingtax"
            .Text = "Withholding Tax"
            .ForeColor = Color.White
            '.IconIndex = 5
        End With
        With MemberStatus
            .Bold = False
            .Tag = "MemberStatus"
            .Text = "Status"
            .ForeColor = Color.White
            '.IconIndex = 5
        End With

        With approver
            .Bold = False
            .Tag = "Approver"
            .Text = "Approver"
            .ForeColor = Color.White
            '.IconIndex = 5
        End With

        With Group
            .Bold = False
            .Tag = "Group"
            .Text = "Group"
            .ForeColor = Color.White
        End With

        With subgroup
            .Bold = False
            .Tag = "subgroup"
            .Text = "Sub-Group"
            .ForeColor = Color.White
        End With

        With Category
            .Bold = False
            .Tag = "Category"
            .Text = "Category"
            .ForeColor = Color.White
        End With

        With Type
            .Bold = False
            .Tag = "Type"
            .Text = "Type"
            .ForeColor = Color.White
        End With

        With rank
            .Bold = False
            .Tag = "Rank"
            .Text = "Rank"
            .ForeColor = Color.White
        End With
        'With TaxCode
        '    .Bold = False
        '    .Tag = "TaxCode"
        '    .Text = "TaxCode"
        '    .ForeColor = Color.White
        'End With

        AcclExplorerBar1.Bars(0).Items.Add(Group)
        AcclExplorerBar1.Bars(0).Items.Add(subgroup)
        AcclExplorerBar1.Bars(0).Items.Add(Category)
        AcclExplorerBar1.Bars(0).Items.Add(Type)
        AcclExplorerBar1.Bars(0).Items.Add(rank)
        'AcclExplorerBar1.Bars(0).Items.Add(TaxCode)

        If isAllowed_M_POSITION() = True Then
            AcclExplorerBar1.Bars(0).Items.Add(PositionMaster) 'jobrequisition
        End If
        If isAllowed_M_PAYROLLC() = True Then
            AcclExplorerBar1.Bars(0).Items.Add(PayrollCodeMaster)
        End If
        If isAllowed_M_PAYC() = True Then
            AcclExplorerBar1.Bars(0).Items.Add(paycode)
        End If
        If isAllowed_M_PCALENDAR() = True Then
            AcclExplorerBar1.Bars(0).Items.Add(PayCalendar)
        End If
        If isAllowed_M_PINTEGRATOR() = True Then
            AcclExplorerBar1.Bars(0).Items.Add(payrollintegrator)
        End If
        If isAllowed_M_LOANTYPE() = True Then
            AcclExplorerBar1.Bars(0).Items.Add(LoanType)
        End If
        If isAllowed_M_POLICY() = True Then
            AcclExplorerBar1.Bars(0).Items.Add(policies)
        End If
        'If isAllowed_M_PAGIBIG() = True Then
        '    AcclExplorerBar1.Bars(0).Items.Add(pagibig)
        'End If
        'If isAllowed_M_PHIL() = True Then
        '    AcclExplorerBar1.Bars(0).Items.Add(philhealth)
        'End If
        'If isAllowed_M_SSS() = True Then
        '    AcclExplorerBar1.Bars(0).Items.Add(SSS)
        'End If
        If isAllowed_M_HOLDINGTAX() = True Then
            AcclExplorerBar1.Bars(0).Items.Add(withholdingtax)
        End If
        If isAllowed_M_APPROVER() = True Then
            AcclExplorerBar1.Bars(0).Items.Add(approver)
            'AcclExplorerBar1.Bars(0).Items.Add(applicantReqt)
        End If
    End Sub

    Public Sub ListPaymentSubmenus()
        Dim ReceivePayment As New ExplorerBarLinkItem
        With ReceivePayment
            .Bold = False
            .Tag = "ReceivePayment"
            .Text = "Cash Collection"
        End With
        If isAllowed_PAYMENT() = True Then
            AcclExplorerBar1.Bars(3).Items.Add(ReceivePayment)
        End If
    End Sub

    Public Sub ListMemberManagerSubmenus()
        Dim employeemasterfile As New ExplorerBarLinkItem
        Dim Clientmasterfile As New ExplorerBarLinkItem
        Dim VendorMasterfile As New ExplorerBarLinkItem
        'Dim membersWithdrawal As New ExplorerBarLinkItem
        'Dim newMembersApproval As New ExplorerBarLinkItem

        With employeemasterfile
            .Bold = False
            .Tag = "Employeemasterfile"
            .Text = "Employee Master"
        End With

        With Clientmasterfile
            .Bold = False
            .Tag = "Clientmasterfile"
            .Text = "Client Master"
        End With

        With VendorMasterfile
            .Bold = False
            .Tag = "VendorMasterfile"
            .Text = "Vendor Master"
        End With

        'With newMembersApproval
        '    .Bold = False
        '    .Tag = "NewMembersApproval"
        '    .Text = "New Members Approval"
        'End With

        'With membersWithdrawal
        '    .Bold = False
        '    .Tag = "membersTermination"
        '    .Text = "Withdrawal"
        'End With

        If isAllowed_MM_MEMBER() = True Then
            AcclExplorerBar1.Bars(1).Items.Add(employeemasterfile)
        End If

        If isAllowed_MM_MEMBER() = True Then
            AcclExplorerBar1.Bars(1).Items.Add(Clientmasterfile)
        End If

        If isAllowed_MM_MEMBER() = True Then
            AcclExplorerBar1.Bars(1).Items.Add(VendorMasterfile)
        End If

        'If isAllowed_MM_WITHDRAWAL() = True Then
        '    AcclExplorerBar1.Bars(1).Items.Add(membersWithdrawal)
        'End If
        'AcclExplorerBar1.Bars(1).Items.Add(newMembersApproval)
    End Sub
    'THIRD SIDE MENU
    Public Sub ListOrganizationalStructureSubmenus()
        Dim orglevel As ExplorerBarLinkItem
        Dim orgchart As ExplorerBarLinkItem

        'items(properties) '
        orglevel = New ExplorerBarLinkItem
        orgchart = New ExplorerBarLinkItem

        With orglevel
            .Bold = False
            .Tag = "orglevel"
            .Text = "Level Definition"
            '.IconIndex = 12
        End With
        With orgchart
            .Bold = False
            .Tag = "orgchart"
            .Text = "Organization"
            '.IconIndex = 13
        End With
        If isAllowed_ORG_LEVEL() = True Then
            AcclExplorerBar1.Bars(4).Items.Add(orglevel)
        End If
        If isAllowed_ORG_ORG() = True Then
            AcclExplorerBar1.Bars(4).Items.Add(orgchart)
        End If
    End Sub
    'FOURTH SIDE MENU
    Public Sub ListOtherInfoSubmenus()
        Dim info1 As ExplorerBarLinkItem
        info1 = New ExplorerBarLinkItem
        With info1
            .Bold = False
            .Tag = "About"
            .Text = "Client System"
        End With
        AcclExplorerBar1.Bars(5).Items.Add(info1)
    End Sub
    'SECOND SIDE MENU
    Public Sub ListLoanManagerSubmenus()
        Dim loans As ExplorerBarLinkItem
        Dim existingloans As ExplorerBarLinkItem
        Dim approveloans As ExplorerBarLinkItem
        Dim printloandetails As ExplorerBarLinkItem

        loans = New ExplorerBarLinkItem
        existingloans = New ExplorerBarLinkItem
        approveloans = New ExplorerBarLinkItem
        printloandetails = New ExplorerBarLinkItem

        With loans
            .Bold = False
            .Tag = "NewLoans"
            .Text = "Loan Application"
        End With

        With existingloans
            .Bold = False
            .Tag = "ExistingLoans"
            .Text = "Loan Processing"
        End With

        With approveloans
            .Bold = False
            .Tag = "ApproveLoans"
            .Text = "Loan Approval"
        End With

        With printloandetails
            .Bold = False
            .Tag = "PrintLoanDetails"
            .Text = "Print Loan Details"
        End With
        If isAllowed_LM_LOANAPP() = True Then
            AcclExplorerBar1.Bars(2).Items.Add(loans)
        End If
        If isAllowed_LM_LOANPRO() = True Then
            AcclExplorerBar1.Bars(2).Items.Add(existingloans)
        End If
        If isAllowed_LM_LOANAPPROVAL() = True Then
            AcclExplorerBar1.Bars(2).Items.Add(approveloans)
        End If
        ' AcclExplorerBar1.Bars(2).Items.Add(printloandetails)

    End Sub

    Public Sub addfunctions()
        Dim viewresigned As ExplorerBarLinkItem
        Dim massupdateCert As ExplorerBarLinkItem
        viewresigned = New ExplorerBarLinkItem
        massupdateCert = New ExplorerBarLinkItem
        With massupdateCert
            .Bold = False
            .Tag = "DateCertified"
            .Text = "Date Certified"
        End With
        With viewresigned
            .Bold = False
            .Tag = "ViewResigned"
            .Text = "Rehire"
            '.IconIndex = 6
        End With
        AcclExplorerBar1.Bars(4).Items.Add(massupdateCert)
        AcclExplorerBar1.Bars(4).Items.Add(viewresigned) 'jobrequisition

    End Sub

    Public Sub transactions()
        Dim transactions As ExplorerBarLinkItem
        Dim applicantmaster As ExplorerBarLinkItem
        Dim uploadexcel As ExplorerBarLinkItem
        transactions = New ExplorerBarLinkItem
        uploadexcel = New ExplorerBarLinkItem
        applicantmaster = New ExplorerBarLinkItem
        With transactions
            .Bold = False
            .Tag = "Uploading"
            .Text = "Upload Emp.Master"
            '.IconIndex = 9
        End With
        With applicantmaster
            .Bold = False
            .Tag = "UploadApplicant"
            .Text = "Upload App.Master"
        End With
        With uploadexcel
            .Bold = False
            .Tag = "Uploadingexcel"
            .Text = "Upload Others"
        End With

        AcclExplorerBar1.Bars(5).Items.Add(transactions)
        AcclExplorerBar1.Bars(5).Items.Add(applicantmaster)
        AcclExplorerBar1.Bars(5).Items.Add(uploadexcel)
    End Sub

    Public Sub reports1()
        Dim demographic As ExplorerBarLinkItem
        Dim applicantdemo As ExplorerBarLinkItem
        Dim certification As ExplorerBarLinkItem

        'Items Properties
        demographic = New ExplorerBarLinkItem
        applicantdemo = New ExplorerBarLinkItem
        certification = New ExplorerBarLinkItem
        With demographic
            .Bold = False
            .Tag = "demographic"
            .Text = "Employee"
            '.IconIndex = 6
        End With
        With applicantdemo
            .Bold = False
            .Tag = "Applicantreport"
            .Text = "Applicant"
        End With

        With certification
            .Bold = False
            .Tag = "certification"
            .Text = "Certification"
            '.IconIndex = 6
        End With

        AcclExplorerBar1.Bars(6).Items.Add(demographic)
        'AcclExplorerBar1.Bars(5).Items.Add(applicantdemo)
        'AcclExplorerBar1.Bars(5).Items.Add(certification)
    End Sub

    Public Sub AboutProj()
        Dim changepass As ExplorerBarLinkItem
        Dim audit As ExplorerBarLinkItem
        Dim auditreport As ExplorerBarLinkItem
        'Items Properties
        changepass = New ExplorerBarLinkItem
        audit = New ExplorerBarLinkItem
        auditreport = New ExplorerBarLinkItem

        With changepass
            .Bold = False
            .Tag = "changepass"
            .Text = "Change Password"
            '.IconIndex = 1
        End With
        With audit
            .Bold = False
            .Tag = "auditTrail"
            .Text = "Audit Trail"
        End With
        With auditreport
            .Bold = False
            .Tag = "ReportTrail"
            .Text = "Report Audit"
        End With

        'AcclExplorerBar1.Bars(6).Items.Add(time)
        AcclExplorerBar1.Bars(7).Items.Add(changepass)
        AcclExplorerBar1.Bars(7).Items.Add(audit)
        AcclExplorerBar1.Bars(7).Items.Add(auditreport)
    End Sub

    Public Sub load_configurationfile()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_company_information_select", myconnection.sqlconn)
        cmd.Connection = myconnection.sqlconn
        myconnection.sqlconn.Open()

        Dim myreader As SqlDataReader = cmd.ExecuteReader()
        Try
            While myreader.Read()
                Me.lblCompanyTitle.Text = myreader.Item(0)
            End While
        Finally
            myreader.Close()
            myconnection.sqlconn.Close()
        End Try
    End Sub
    Private Sub StLoginName_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles StLoginName.TextChanged
        getusername() = frmLogin.Username.Text
        StLoginName.Text = "Login Name : " + frmLogin.Username.Text + "    " + "  |  "
        '"Computer Name : " + " " + My.Computer.Name + "  " + "  |  " + "System Date : " + Date.Now

        frmDownloadUploadExcel.GetUserName() = Me.getusername()

    End Sub

    Private Sub PictureBox3_MouseHover(ByVal sender As Object, ByVal e As System.EventArgs) Handles PictureBox3.MouseHover
        Me.AcclExplorerBar1.Show()
        Me.PictureBox3.Hide()
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Dim h As String = My.Computer.Clock.LocalTime.Hour
        Dim m As String = My.Computer.Clock.LocalTime.Minute
        Dim s As String = My.Computer.Clock.LocalTime.Second
        If m < 10 Then m = "0" + m
        If s < 10 Then s = "0" + s
        If h = 0 Then h = "12"
        StTime.Text = "Time : " + h + ":" + m + ":" + s + " | "
    End Sub
#End Region

#Region "Menu and Sub Menus"
    Private Sub EmployeeInfoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        If Me.ActiveMdiChild Is Nothing Then
            frmMember_Master.MdiParent = Me
            frmMember_Master.Show()

        Else
            MessageBox.Show("Employee Master File is already Open", "...")
        End If
    End Sub

    Private Sub ToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim xemployeesearch As New Employee_Search
        xemployeesearch.Show()
    End Sub

    Private Sub EmployeeSearchToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Employee_Search.MdiParent = Me
        Employee_Search.Show()
    End Sub


    Private Sub AcclExplorerBar1_ItemClick(ByVal sender As Object, ByVal args As vbAccelerator.Components.Controls.ExplorerBarItemClickEventArgs) Handles AcclExplorerBar1.ItemClick
        Try
            Select Case args.Item.Tag
                'Master file
                Case "Group"
                    frmMasterfile_Group.MdiParent = Me
                    frmMasterfile_Group.Show()

                Case "subgroup"
                    frmMasterfile_SubGroup.MdiParent = Me
                    frmMasterfile_SubGroup.Show()

                Case "Category"
                    frmMasterfile_Category.MdiParent = Me
                    frmMasterfile_Category.Show()

                Case "Type"
                    frmMasterfile_Type.MdiParent = Me
                    frmMasterfile_Type.Show()

                Case "Rank"
                    frmMasterfile_Rank.MdiParent = Me
                    frmMasterfile_Rank.StartPosition = FormStartPosition.CenterScreen
                    frmMasterfile_Rank.Show()

                Case "PositionMaster"
                    frmMember_Position.MdiParent = Me
                    frmMember_Position.Show()

                Case "employeetype"
                    Employee_Position.MdiParent = Me
                    Employee_Position.Show()

                Case "PayrollCodeMaster"
                    frmMasterfile_PayrollCode.MdiParent = Me
                    frmMasterfile_PayrollCode.Show()

                Case "paycode"
                    frmMasterfile_Paycode.MdiParent = Me
                    frmMasterfile_Paycode.Show()

                Case "ApproveLoans"
                    frmLoan_Approval.MdiParent = Me
                    frmLoan_Approval.Show()

                Case "PayCalendar"
                    frmMasterfile_PayrollCalendar.MdiParent = Me
                    frmMasterfile_PayrollCalendar.Show()

                Case "payrollintegrator"
                    frmMasterfile_PayIntegratorMaster.MdiParent = Me
                    frmMasterfile_PayIntegratorMaster.Show()

                Case "LoanType"
                    frmMasterfile_LoanType.MdiParent = Me
                    frmMasterfile_LoanType.Show()

                Case "Policies"
                    frmLoan_Policy_Master.MdiParent = Me
                    frmLoan_Policy_Master.Show()

                Case "Pagibig"
                    frmMasterfile_Pagibig.MdiParent = Me
                    frmMasterfile_Pagibig.Show()

                Case "philhealth"
                    frmMasterfile_PhilHealth.MdiParent = Me
                    frmMasterfile_PhilHealth.Show()

                Case "SSS"
                    frmMasterfile_SSS.MdiParent = Me
                    frmMasterfile_SSS.Show()

                Case "Withholdingtax"
                    frmMasterfile_WitholdingTax.MdiParent = Me
                    frmMasterfile_WitholdingTax.Show()

                Case "Approver"
                    frmLoan_Policy_Approvers_Master.MdiParent = Me
                    frmLoan_Policy_Approvers_Master.Show()

                Case "ratetype"
                    Rate_Type.MdiParent = Me
                    Rate_Type.Show()
                    Me.PictureBox3.Visible = False

                Case "MemberStatus"
                    frmMember_Status.MdiParent = Me
                    frmMember_Status.Show()
                    Me.PictureBox3.Visible = False

                Case "benefitmaster"
                    frmEmployee_BenefitMaster.Show()
                    frmEmployee_BenefitMaster.btnaddselected.Enabled = False
                    frmEmployee_BenefitMaster.btninsertall.Enabled = False
                    Me.PictureBox3.Visible = False

                    'Applicant Management
                Case "compensation"
                    Salary_Grade.MdiParent = Me
                    Salary_Grade.Show()
                    Me.PictureBox3.Visible = False

                    'Case "Employeemasterfile"
                    '    frmMember_Master.MdiParent = Me
                    '    frmMember_Master.Show()
                    '    Me.PictureBox3.Visible = False

                Case "Employeemasterfile"
                    frmMember_Master.TabPageforEmployee()
                    frmMember_Master.rbEmployee.Checked = True
                    frmMember_Master.txtContractPrice.Text = ""
                    frmMember_Master.txtContractrate.Text = ""
                    frmMember_Master.MdiParent = Me
                    frmMember_Master.Show()
                    Me.PictureBox3.Visible = False

                Case "Clientmasterfile"
                    'frmMember_Client.TabPageforClient()
                    'frmMember_Client.rbClient.Checked = True
                    frmMember_Client.MdiParent = Me
                    frmMember_Client.Show()
                    'Me.PictureBox3.Visible = False

                Case "VendorMasterfile"
                    frmMember_Vendors.MdiParent = Me
                    frmMember_Vendors.Show()

                Case "orglevel"
                    frmOrgChart_LevelDefinition.MdiParent = Me
                    frmOrgChart_LevelDefinition.Show()
                    Me.PictureBox3.Visible = False

                Case "orgchart"
                    Dim orgchart_main As New frmOrgChart_Master(WindowsApplication2.frmOrgChart_Master.selection.ORg_Main)
                    orgchart_main.MdiParent = Me
                    orgchart_main.Show()
                    Me.PictureBox3.Visible = False
                    'upload excel
                Case "Uploadingexcel"
                    FrmAdmin_FileUploader.MdiParent = Me
                    FrmAdmin_FileUploader.Show()
                    Me.PictureBox3.Visible = False
                Case "changepass"
                    frmAdmin_ChangePassword.Show()
                    Me.PictureBox3.Visible = False
                Case "About"
                    frmAboutSystem.ShowDialog()
                    Me.PictureBox3.Visible = False
                    'functions
                Case "NewLoans"
                    frmLoan_ApplicationMain.MdiParent = Me
                    frmLoan_ApplicationMain.Show()
                Case "ExistingLoans"
                    frmLoan_Processing.MdiParent = Me
                    frmLoan_Processing.Show()
                Case "Deposits"
                    frmVerification.MdiParent = Me
                    frmVerification.WindowState = FormWindowState.Maximized
                    frmVerification.Show()
                Case "Contribution"
                    frmCapitalShare.MdiParent = Me
                    frmCapitalShare.Show()
                Case "membersTermination"
                    frmMembersWithdrawal.MdiParent = Me
                    frmMembersWithdrawal.Show()
                    'Case "ApplicantReqt"
                    '    frmMembershipRequirements.MdiParent = Me
                    '    frmMembershipRequirements.Show()
                    'Case "NewMembersApproval"
                    '    frmNewMembersProcessing.MdiParent = Me
                    '    frmNewMembersProcessing.Show()
                Case "ReceivePayment"
                    Dim xForm As New frmCashier
                    xForm.StartPosition = FormStartPosition.CenterScreen
                    xForm.MdiParent = Me
                    xForm.Show()

            End Select
        Catch ex As Exception

        End Try
    End Sub

    Private Sub EmployeeMasterToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        frmMember_Master.MdiParent = Me
        frmMember_Master.Show()
        Me.PictureBox3.SendToBack()
    End Sub

    Private Sub EmployeePositionToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        frmMember_Position.MdiParent = Me
        frmMember_Position.Show()
    End Sub

    Private Sub EmployeeTypeToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Employee_Position.MdiParent = Me
        Employee_Position.Show()
    End Sub

    Private Sub PayCategoryToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Payroll_Schedule.MdiParent = Me
        Payroll_Schedule.Show()
    End Sub

    Private Sub TaxCodeToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Tax_Code.MdiParent = Me
        Tax_Code.Show()
    End Sub

    Private Sub RateTypeToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Rate_Type.MdiParent = Me
        Rate_Type.Show()
    End Sub

    Private Sub JobRankToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        frmMember_Status.MdiParent = Me
        frmMember_Status.Show()
    End Sub

    Private Sub SalaryGradeToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Salary_Grade.MdiParent = Me
        Salary_Grade.Show()
    End Sub

    Private Sub BenefitMasterToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        frmEmployee_BenefitMaster.Show()
        frmEmployee_BenefitMaster.btnaddselected.Enabled = False
        frmEmployee_BenefitMaster.btninsertall.Enabled = False
    End Sub

    Private Sub CertificationToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'Certification.MdiParent = Me
        'Certification.Show()
    End Sub

    Private Sub PrinterToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.PrintDialog1.ShowDialog()
    End Sub


    Private Sub ExitToolStripMenuItem1_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitToolStripMenuItem1.Click
        Me.Close()
    End Sub

    Private Sub Activeemployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Dim con As New clsPersonnel
            Dim applyinfo As New applyCRlogin
            Dim appRdr As New System.Configuration.AppSettingsReader
            Dim myconnection As New Clsappconfiguration
            Dim objreport As New CrystalDecisions.CrystalReports.Engine.ReportDocument
            objreport.Load(Application.StartupPath & "\reports\Print_list_of_active_employees.rpt")
            Logon(objreport, con.servername, con.databasename, con.username1, con.password1)
            objreport.Refresh()
            CooperativeReports.appreports.ReportSource = objreport
            CooperativeReports.MdiParent = Me
            CooperativeReports.Show()
        Catch ex As Exception
        End Try
    End Sub

    Private Sub Resignedemployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Dim con As New clsPersonnel
            Dim appRdr As New System.Configuration.AppSettingsReader
            Dim myconnection As New Clsappconfiguration
            Dim objreport As New CrystalDecisions.CrystalReports.Engine.ReportDocument
            objreport.Load(Application.StartupPath & "\reports\Print_list_of_resigned_employees.rpt")
            'DoCRLogin(objreport)
            'objreport.SetDatabaseLogon(appRdr.GetValue("Username", GetType(String)), appRdr.GetValue("Password", GetType(String)), appRdr.GetValue("Server", GetType(String)), appRdr.GetValue("Database", GetType(String)))
            'objreport.SetDatabaseLogon(con.username1, con.password1, con.servername, con.databasename)
            Logon(objreport, con.servername, con.databasename, con.username1, con.password1)
            objreport.Refresh()
            CooperativeReports.appreports.ReportSource = objreport
            CooperativeReports.MdiParent = Me
            CooperativeReports.Show()
        Catch ex As Exception
        End Try
    End Sub

    Private Sub Demography_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Dim con As New clsPersonnel
            Dim appRdr As New System.Configuration.AppSettingsReader
            Dim myconnection As New Clsappconfiguration
            Dim objreport As New CrystalDecisions.CrystalReports.Engine.ReportDocument
            objreport.Load(Application.StartupPath & "\reports\Employee_According_to_Demography.rpt")
            'DoCRLogin(objreport)
            'objreport.SetDatabaseLogon(appRdr.GetValue("Username", GetType(String)), appRdr.GetValue("Password", GetType(String)), appRdr.GetValue("Server", GetType(String)), appRdr.GetValue("Database", GetType(String)))
            'objreport.SetDatabaseLogon(con.username1, con.password1, con.servername, con.databasename)
            Logon(objreport, con.servername, con.databasename, con.username1, con.password1)
            objreport.Refresh()
            CooperativeReports.appreports.ReportSource = objreport
            CooperativeReports.MdiParent = Me
            CooperativeReports.Show()
        Catch ex As Exception
        End Try
    End Sub

    Private Sub headcount_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Dim con As New clsPersonnel
            Dim appRdr As New System.Configuration.AppSettingsReader
            Dim myconnection As New Clsappconfiguration
            Dim objreport As New CrystalDecisions.CrystalReports.Engine.ReportDocument
            objreport.Load(Application.StartupPath & "\reports\Employee_according_to_HeadCount.rpt")
            'DoCRLogin(objreport)
            'objreport.SetDatabaseLogon(appRdr.GetValue("Username", GetType(String)), appRdr.GetValue("Password", GetType(String)), appRdr.GetValue("Server", GetType(String)), appRdr.GetValue("Database", GetType(String)))
            'objreport.SetDatabaseLogon(con.username1, con.password1, con.servername, con.databasename)
            Logon(objreport, con.servername, con.databasename, con.username1, con.password1)
            objreport.Refresh()
            CooperativeReports.appreports.ReportSource = objreport
            CooperativeReports.MdiParent = Me
            CooperativeReports.Show()
        Catch ex As Exception
        End Try
    End Sub

    Private Sub HelpToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles HelpToolStripMenuItem.Click
        frmAboutSystem.ShowDialog()
        Me.PictureBox3.Visible = False
    End Sub

    Private Sub EmployeeMasterToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        frmMember_Master.Show()
    End Sub

    Private Sub UserToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UserToolStripMenuItem.Click
        frmAdmin_CreateUser.ShowDialog()
    End Sub

    Private Sub ExportToExcel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        frmDatabasetoExcel.MdiParent = Me
        frmDatabasetoExcel.Show()
    End Sub

    Private Sub ExistingMember_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExistingMember.Click
        'frmMember_Master.WindowState = FormWindowState.Maximized
        'frmMember_Master.MdiParent = Me
        'frmMember_Master.Show()
    End Sub

    Private Sub MemberContributionToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MemberContributionToolStripMenuItem.Click
        frmCapitalShare.MdiParent = Me
        frmCapitalShare.Show()
    End Sub

    Private Sub Deposits_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Saving_and_Deposit_New.MdiParent = Me
        Saving_and_Deposit_New.Show()
    End Sub

    Private Sub Newloans_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Newloans.Click
        frmLoan_ApplicationMain.MdiParent = Me
        frmLoan_ApplicationMain.Show()
    End Sub

    Private Sub ExistingLoans_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExistingLoans.Click
        frmLoan_Processing.MdiParent = Me
        frmLoan_Processing.Show()
    End Sub

    Private Sub DownloadingUploadingPayrollScheduToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        frmDownloadUploadExcel.MdiParent = Me
        frmDownloadUploadExcel.Show()
    End Sub

    Private Sub ChangePasswordToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ChangePasswordToolStripMenuItem.Click
        frmAdmin_ChangePassword.MdiParent = Me
        frmAdmin_ChangePassword.Show()
    End Sub

    Private Sub BackupToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If MessageBox.Show("Do you want to Backup your database?", "Database Backup", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            gFilePath = GetFilePath()
            If gFilePath <> "" Then
                DisplayMarqueeProgressBar("Backing up Database... Please wait, this may take several minutes.")
                bgwDatabaseFunctions.RunWorkerAsync()
            End If
        End If
    End Sub

    Private Sub PayrollSettlementReportToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        frmRpt_PayrollSettlement.Show()
    End Sub

    Private Sub ContributionBalanceMasterToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        frmRpt_ContributionBalanceMaster.MdiParent = Me
        frmRpt_ContributionBalanceMaster.Show()
    End Sub
    Private Sub InterestIncomeToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        frmInterest_Main.MdiParent = Me
        frmInterest_Main.Show()
    End Sub
    Private Sub CashPaymentMasterToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'Dim xForm As New frmCashPayment_Main
        Dim xForm As New frmCashier
        xForm.WindowState = FormWindowState.Maximized
        xForm.MdiParent = Me
        xForm.Show()
    End Sub
#End Region

    Private Sub BackupDatabase()
        Try
            Dim database As New clsDatabaseFunctions()
            database.BackupDatabase("Coop_Accounting", gFilePath, Me.username)
            database.BackupDatabase("Coop_Membership", gFilePath, Me.username)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function GetFilePath() As String
        Try
            Dim directory As String = ""
            Dim FolderBrowserDialog As New FolderBrowserDialog()
            With FolderBrowserDialog
                .RootFolder = Environment.SpecialFolder.Desktop
                .Description = "Select the folder where you will store your backup."

                If .ShowDialog() = Windows.Forms.DialogResult.OK Then
                    directory = .SelectedPath() + "/"
                End If
            End With

            Return directory
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Shared Sub DisplayMarqueeProgressBar(ByVal title As String)
        With frmStartup
            .MdiParent = frmMain
            .Show()
            .processWait.Style = ProgressBarStyle.Marquee
            .processWait.MarqueeAnimationSpeed = 5
            .lblTitle.Text = title
            .lblProcessStatus.Text = ""
            .lblProgress.Text = ""
            '.picLoadingSmall.Visible = False
        End With
    End Sub

#Region "Background Tasks"
    Private Sub bgwDatabaseFunctions_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgwDatabaseFunctions.DoWork
        Try
            BackupDatabase()
            MessageBox.Show("Backup of your Database is complete!", "Database Backup", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Database Backup", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub
    Private Sub bgwDatabaseFunctions_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwDatabaseFunctions.RunWorkerCompleted
        frmStartup.Close()
    End Sub
#End Region


    Private Sub SummaryOfAgingReportsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SummaryOfAgingReportsToolStripMenuItem.Click
        frmRpt_AgingOfLoans_v2.MdiParent = Me
        frmRpt_AgingOfLoans_v2.Show()
    End Sub

    Private Sub CapitalShareToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CapitalShareToolStripMenuItem.Click
        frmRpt_CapitalShare.ShowDialog()
    End Sub

    Private Sub StatementOfAccountsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        frmRpt_StatementofAccountReport.ShowDialog()
    End Sub

    Private Sub LoanReceivableAgingReportToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LoanReceivableAgingReportToolStripMenuItem.Click
        frmRpt_AgingOfLoans_v2.MdiParent = Me
        frmRpt_AgingOfLoans_v2.Show()
    End Sub

    Private Sub ScheduleOfLoanReceivaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ScheduleOfLoanReceivaToolStripMenuItem.Click
        frmRpt_LoanBalanceMasterfile.MdiParent = Me
        frmRpt_LoanBalanceMasterfile.Show()
    End Sub

    Private Sub PaidUpCapitalShareToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PaidUpCapitalShareToolStripMenuItem.Click
        frmRpt_CapitalContributionRpt.MdiParent = Me
        frmRpt_CapitalContributionRpt.Show()
    End Sub

    Private Sub LoansToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LoansToolStripMenuItem.Click
        frmRpt_StatementofAccountReport.ShowDialog()
    End Sub

    Private Sub AverageCapitalShareAndDividendsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AverageCapitalShareAndDividendsToolStripMenuItem.Click
        Dim xForm As New frmRpt_DividendsReport
        xForm.MdiParent = Me
        xForm.Show()
    End Sub

    Private Sub BalanceSheetToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BalanceSheetToolStripMenuItem.Click
        MessageBox.Show("You can access this report on the Accounting System", "Accounting System", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub

    Private Sub StatementOfNetSurplusToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles StatementOfNetSurplusToolStripMenuItem.Click
        MessageBox.Show("You can access this report on the Accounting System", "Accounting System", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub

    Private Sub StatementOfUtilizationOfStatutoryFundsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles StatementOfUtilizationOfStatutoryFundsToolStripMenuItem.Click
        MessageBox.Show("You can access this report on the Accounting System", "Accounting System", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub

    Private Sub AccountsPayableToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AccountsPayableToolStripMenuItem.Click
        MessageBox.Show("You can access this report on the Accounting System", "Accounting System", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub

    Private Sub LoansPayableToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LoansPayableToolStripMenuItem.Click
        MessageBox.Show("You can access this report on the Accounting System", "Accounting System", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub

    Private Sub GeneralLedgerToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GeneralLedgerToolStripMenuItem.Click
        MessageBox.Show("You can access this report on the Accounting System", "Accounting System", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub

    Private Sub CompanyInformationToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CompanyInformationToolStripMenuItem.Click
        'frmAdmin_Company_Master.Show()
        frmCompanyInfoMaster.MdiParent = Me
        frmCompanyInfoMaster.StartPosition = FormStartPosition.CenterScreen
        frmCompanyInfoMaster.Show()
    End Sub

    Private Sub btnShowHideSubmenu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnShowHideSubmenu.Click
        If btnShowHideSubmenu.Text = "Show" Then
            AcclExplorerBar1.Show()
            btnShowHideSubmenu.Text = "Hide"
        Else
            AcclExplorerBar1.Hide()
            btnShowHideSubmenu.Text = "Show"
        End If
    End Sub

    Private Sub DividendDistributionToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        frmUploader_DividendDistribution.MdiParent = Me
        frmUploader_DividendDistribution.Show()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        frmLoan_Policy_PositionType.Show()
    End Sub

    Private Sub CAPRToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CAPRToolStripMenuItem.Click
        LoadGeneralInfo()
        NewCAPRReport()
        xFormSec1.MdiParent = Me
        xFormSec2.MdiParent = Me
        xFormSec3.MdiParent = Me
        xFormSec3b.MdiParent = Me
        xFormSec4.MdiParent = Me
        frmCAPRViewer.MdiParent = Me

        xFormItro = New FrmCAPRIntro
        xFormItro.MdiParent = Me
        xFormItro.Show()
    End Sub

    Private Sub AccountsConfigurationToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim config As New frmAccountConfiguration
        config.MdiParent = Me
        config.Show()
    End Sub

    Private Sub MembershipTerminationToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim xForm As New frmMembersWithdrawal
        xForm.MdiParent = Me
        xForm.Show()
    End Sub

    Private Sub LoanMasterToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim xForm As New frmLoanMasterUploader
        xForm.MdiParent = Me
        xForm.xMode = "Loan Master"
        xForm.Show()
    End Sub

    Private Sub LoanPaymentsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim xForm As New frmLoanMasterUploader
        xForm.MdiParent = Me
        xForm.xMode = "Loan Payment"
        xForm.Show()
    End Sub

    Private Sub ContributionToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim xForm As New frmLoanMasterUploader
        xForm.MdiParent = Me
        xForm.xMode = "Member Contribution"
        xForm.Show()
    End Sub

    Private Sub SavingsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim xForm As New frmLoanMasterUploader
        xForm.MdiParent = Me
        xForm.xMode = "Savings"
        xForm.Show()
    End Sub

    Private Sub RightsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RightsToolStripMenuItem.Click
        frmAdmin_UpdateRights.MdiParent = Me
        frmAdmin_UpdateRights.Show()
    End Sub

    Private Sub LoanApproval_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LoanApproval.Click
        frmLoan_Approval.MdiParent = Me
        frmLoan_Approval.Show()
    End Sub

    Private Sub MemberMasterToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim xForm As New frmMemberMasterUplodaer
        xForm.MdiParent = Me
        xForm.Show()
    End Sub

    Private Sub DebitCreditToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DebitCreditToolStripMenuItem.Click
        frmMasterfile_DebitCredit.MdiParent = Me
        frmMasterfile_DebitCredit.StartPosition = FormStartPosition.CenterScreen
        frmMasterfile_DebitCredit.Show()
    End Sub

    Private Sub AccountRegisterToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AccountRegisterToolStripMenuItem.Click
        frmMasterfile_AccountRegister.MdiParent = Me
        frmMasterfile_AccountRegister.StartPosition = FormStartPosition.CenterScreen
        frmMasterfile_AccountRegister.Show()
    End Sub

    Private Sub CapitalShareDepositAndWithdrawalToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CapitalShareDepositAndWithdrawalToolStripMenuItem.Click
        Dim xForm As New Employee_Search
        xForm.xcase = "Dep and Withd"
        xForm.MdiParent = Me
        xForm.Show()
    End Sub

    Private Sub LoanOverpaymentToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        frmOverpayment.MdiParent = Me
        frmOverpayment.StartPosition = FormStartPosition.CenterScreen
        frmOverpayment.Show()
    End Sub

    Private Sub btnEnteng_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEnteng.Click
        If btnEnteng.Text = "HIDE SIDE BAR" Then
            AcclExplorerBar1.Hide()
            btnEnteng.Text = "SHOW SIDE BAR"
            Exit Sub
        End If
        If btnEnteng.Text = "SHOW SIDE BAR" Then
            AcclExplorerBar1.Show()
            btnEnteng.Text = "HIDE SIDE BAR"
            Exit Sub
        End If
    End Sub

    Public Sub LoadCooperativeName()
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gcon.cnstring, "_Select_CooperativeName")
        While rd.Read
            lblCoopName.Text = rd("fcCoopName")
        End While
    End Sub

    Private Sub ScheduleOfLoansRToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ScheduleOfLoansRToolStripMenuItem.Click
        frmReport_ScheduleofLoansReleased.MdiParent = Me
        frmReport_ScheduleofLoansReleased.StartPosition = FormStartPosition.CenterScreen
        frmReport_ScheduleofLoansReleased.Show()
    End Sub

    Private Sub LoanStatusReportToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LoanStatusReportToolStripMenuItem.Click
        frmRpt_LoanStatus.MdiParent = Me
        frmRpt_LoanStatus.StartPosition = FormStartPosition.CenterScreen
        frmRpt_LoanStatus.Show()
    End Sub

    Private Sub ScheduleOfLoansReceivableToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ScheduleOfLoansReceivableToolStripMenuItem.Click
        frmRpt_ScheduleofLoans_Receivable.MdiParent = Me
        frmRpt_ScheduleofLoans_Receivable.StartPosition = FormStartPosition.CenterScreen
        frmRpt_ScheduleofLoans_Receivable.Show()
    End Sub

    Private Sub LoanLedgerToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LoanLedgerToolStripMenuItem.Click
        frmLoanLedger.MdiParent = Me
        frmLoanLedger.StartPosition = FormStartPosition.CenterScreen
        frmLoanLedger.Show()
    End Sub

    Private Sub ClientListReportToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClientListReportToolStripMenuItem.Click
        Try
            Dim xForm As New frmRpt_MemberList
            xForm.MdiParent = Me
            xForm.Show()
        Catch ex As Exception
            frmMsgBox.txtMessage.Text = "Please install Report runtime requirements!"
            frmMsgBox.ShowDialog()
            Exit Sub
        End Try
    End Sub

    Private Sub AccountVerificationToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AccountVerificationToolStripMenuItem.Click
        frmVerification.MdiParent = Me
        frmVerification.WindowState = FormWindowState.Maximized
        frmVerification.StartPosition = FormStartPosition.CenterScreen
        frmVerification.Show()
    End Sub

    Private Sub ClientUploadToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClientUploadToolStripMenuItem.Click
        frmClientUploader.MdiParent = Me
        frmClientUploader.WindowState = FormWindowState.Maximized
        frmClientUploader.StartPosition = FormStartPosition.CenterScreen
        frmClientUploader.Show()
    End Sub

    Private Sub LoanForwardBalanceUploaderToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LoanForwardBalanceUploaderToolStripMenuItem.Click
        frmLoanForwardBalanceUploader.MdiParent = Me
        'frmLoanForwardBalanceUploader.WindowState = FormWindowState.Maximized
        frmLoanForwardBalanceUploader.StartPosition = FormStartPosition.CenterScreen
        frmLoanForwardBalanceUploader.Show()
    End Sub

    Private Sub LoanAmortizationUploaderToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LoanAmortizationUploaderToolStripMenuItem.Click
        frmAmortizationUploader.MdiParent = Me
        frmAmortizationUploader.WindowState = FormWindowState.Maximized
        frmAmortizationUploader.StartPosition = FormStartPosition.CenterScreen
        frmAmortizationUploader.Show()
    End Sub

    Private Sub ClientsList2ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClientsList2ToolStripMenuItem.Click
        Try
            frmReport_Client_v2.MdiParent = Me
            frmReport_Client_v2.WindowState = FormWindowState.Maximized
            frmReport_Client_v2.Show()
        Catch ex As Exception
            frmMsgBox.txtMessage.Text = "Please install Report runtime requirements!"
            frmMsgBox.ShowDialog()
            Exit Sub
        End Try
    End Sub

    Private Sub ClientsIntegritorToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClientsIntegritorToolStripMenuItem.Click
        frmClientMigrator.MdiParent = Me
        frmClientMigrator.WindowState = FormWindowState.Maximized
        frmClientMigrator.Show()
    End Sub

    Private Sub LoanUploader3ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LoanUploader3ToolStripMenuItem.Click
        frmLoanFinalUploader.MdiParent = Me
        frmLoanFinalUploader.WindowState = FormWindowState.Maximized
        frmLoanFinalUploader.Show()
    End Sub

    Private Sub InvestmentTransactionToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles InvestmentTransactionToolStripMenuItem.Click
        frmInvestmentApplication.MdiParent = Me
        frmInvestmentApplication.StartPosition = FormStartPosition.CenterScreen
        'frmInvestmentApplication.WindowState = FormWindowState.Maximized
        frmInvestmentApplication.Show()
    End Sub

    Private Sub InvestmentApprovalToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles InvestmentApprovalToolStripMenuItem.Click
        frmInvestmentApproval.MdiParent = Me
        frmInvestmentApproval.StartPosition = FormStartPosition.CenterScreen
        'frmInvestmentApproval.WindowState = FormWindowState.Maximized
        frmInvestmentApproval.Show()
    End Sub

    Private Sub InvestmentSetupToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles InvestmentSetupToolStripMenuItem.Click
        frmInvestmentSetup.StartPosition = FormStartPosition.CenterScreen
        frmInvestmentSetup.MdiParent = Me
        frmInvestmentSetup.Show()
    End Sub

    Private Sub LoanAmortizationEditorToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LoanAmortizationEditorToolStripMenuItem.Click
        frmEditor_Amortization.StartPosition = FormStartPosition.CenterScreen
        frmEditor_Amortization.MdiParent = Me
        frmEditor_Amortization.Show()
    End Sub

    Private Sub EmployeeMasterToolStripMenuItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EmployeeMasterToolStripMenuItem.Click
        With frmMember_Master
            '.TabPageforEmployee()
            '.rbEmployee.Checked = True
            .txtContractPrice.Text = ""
            .txtContractrate.Text = ""
            .WindowState = FormWindowState.Maximized
            .MdiParent = Me
            .Show()
        End With
    End Sub

    Private Sub ClientMasterToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClientMasterToolStripMenuItem.Click
        With frmMember_Client
            '.TabPageforClient()
            '.rbClient.Checked = True
            .WindowState = FormWindowState.Maximized
            .MdiParent = Me
            .Show()
        End With
    End Sub

    Private Sub EmployeeInformationReportToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EmployeeInformationReportToolStripMenuItem.Click
        frmEmp_Information.StartPosition = FormStartPosition.CenterScreen
        frmEmp_Information.MdiParent = Me
        frmEmp_Information.Show()
    End Sub

    Private Sub HistoryReportToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles HistoryReportToolStripMenuItem.Click
        frmMember_History.StartPosition = FormStartPosition.CenterScreen
        frmMember_History.MdiParent = Me
        frmMember_History.Show()
    End Sub

    Private Sub ContractSummaryPerCompanyToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ContractSummaryPerCompanyToolStripMenuItem.Click
        frmContract_Report.StartPosition = FormStartPosition.CenterScreen
        frmContract_Report.MdiParent = Me
        frmContract_Report.Show()
    End Sub

    Private Sub VendorMasterToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles VendorMasterToolStripMenuItem.Click
        frmMember_Vendors.StartPosition = FormStartPosition.CenterScreen
        frmMember_Vendors.MdiParent = Me
        frmMember_Vendors.Show()
    End Sub

    Private Sub DownloaderToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles DownloaderToolStripMenuItem.Click
        frmDownloader.StartPosition = FormStartPosition.CenterScreen
        frmDownloader.MdiParent = Me
        frmDownloader.Show()
    End Sub
End Class
