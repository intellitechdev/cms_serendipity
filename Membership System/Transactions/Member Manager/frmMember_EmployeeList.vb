﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmMember_EmployeeList

    Private Sub ClientEmployeeInfo_List(ByVal Desc As String)
        Dim myconnection As New Clsappconfiguration
        Try
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(myconnection.cnstring, "MSS_MembersInfo_GetClientMasterList",
                              New SqlParameter("@ClientName", Desc))
            With gridExistingMember
                .DataSource = ds.Tables(0)
                .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns(0).Visible = 150
                .Columns(1).Width = 400
                .Columns(2).Width = 200
                .Columns(3).Visible = False
            End With
        Catch ex As Exception
            frmMsgBox.txtMessage.Text = "Error! " + ex.ToString
            frmMsgBox.ShowDialog()
        End Try
    End Sub

    Private Sub frmMember_EmployeeList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ClientEmployeeInfo_List(frmMember_Client.txtClientNo.Text)
    End Sub

    Private Sub btnemp_close_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnemp_close.Click
        Me.Close()
    End Sub

    Private Sub btnfind_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnfind.Click
        GetClientInfo(frmMember_Client.txtClientName.Text, txtid.Text)
    End Sub

    Private Sub GetClientInfo(ByVal Desc As String, ByVal ClientID As String)
        Dim gcon As New Clsappconfiguration()
        Dim ds As New DataSet

        Try
            gridExistingMember.Columns.Clear()
            ds = SqlHelper.ExecuteDataset(gcon.cnstring, CommandType.StoredProcedure, "MSS_MembersInfo_GetClientMasterList_Seach", _
                                          New SqlParameter("@ClientID", Desc), _
                                          New SqlParameter("@employeeNo", ClientID))
            With gridExistingMember
                .DataSource = ds.Tables(0)
                .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                .Columns(0).Visible = 150
                .Columns(1).Width = 400
                .Columns(2).Width = 200
                .Columns(3).Visible = False
            End With
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub txtid_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtid.TextChanged
        GetClientInfo(frmMember_Client.txtClientNo.Text, Me.txtid.Text)
    End Sub
End Class