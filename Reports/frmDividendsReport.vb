'                   ########################################################
'                   ### Created by: Charl Magne "Ionflux" Onod San Pedro ###
'                   ### Date Created: November 11, 2010                  ###
'                   ### Website: http://www.ionflux.site50.net           ###
'                   ########################################################

Imports system.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmDividendsReport

    Private gCon As New Clsappconfiguration
    Private rptsummary As New ReportDocument
    Private xDate As Date
    Private xCompanyName As String = ""
    Private xAddress As String = ""

    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If

        Dim obj As ReportObject

        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function
    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        ' for each table apply connection info
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            ' check if logon was successful
            ' if TestConnectivity returns false,
            ' check logon credentials
            If (tbl.TestConnectivity()) Then
                'drop fully qualified table location
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next

        Return True
    End Function
    Private Sub GetCompanyDetail()
        Dim sSqlCmd As String = "SELECT CompanyName, CompanyAdd1 FROM Per_Company_Information"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                While rd.Read
                    xCompanyName = rd.Item("CompanyName").ToString
                    xAddress = rd.Item("CompanyAdd1").ToString
                End While
            End Using
        Catch ex As Exception
            MsgBox("Error at GetCompanyDetail in frmDividendsReport module." & vbCr & vbCr & ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error occur!")
        End Try

    End Sub
    Private Sub LoadReport()
        Try
            rptsummary.Load(Application.StartupPath & "\LoanReport\Dividends.rpt")
            Logon(rptsummary, gCon.Server, gCon.Database, gCon.Username, gCon.Password)
            rptsummary.Refresh()
            rptsummary.SetParameterValue("@Date", xDate)
            rptsummary.SetParameterValue("@CompanyName", xCompanyName)
            rptsummary.SetParameterValue("@Address", xAddress)
        Catch ex As Exception
            MsgBox("Error at LoadReport in frmDividendsReport module." & vbCr & vbCr & ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error occur!")
        End Try
    End Sub

    Private Sub BGWorker_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BGWorker.DoWork
        GetCompanyDetail()
        LoadReport()
    End Sub

    Private Sub BGWorker_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BGWorker.RunWorkerCompleted
        CrvRpt.Visible = True
        CrvRpt.ReportSource = rptsummary
        lblLoading.Visible = False
        DtDate.Enabled = True
        BtnGenerate.Enabled = True
    End Sub

    Private Sub BtnGenerate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnGenerate.Click
        xDate = DtDate.Value.Date
        DtDate.Enabled = False
        BtnGenerate.Enabled = False
        lblLoading.Visible = True
        BGWorker.RunWorkerAsync()
    End Sub
End Class