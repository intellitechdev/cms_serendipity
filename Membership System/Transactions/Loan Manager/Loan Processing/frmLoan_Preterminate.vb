Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class frmLoan_PreTerminate

#Region "Public Variables"
    Public Empinfo As String
    Public loanStatus As String
    Public Exnumber As String
#End Region
#Region "Public Property"
    Public Property getEmpinfo() As String
        Get
            Return Empinfo
        End Get
        Set(ByVal value As String)
            Empinfo = value
        End Set
    End Property
    Public Property getLoanStatus() As String
        Get
            Return loanStatus
        End Get
        Set(ByVal value As String)
            loanStatus = value
        End Set
    End Property
    Public Property getExloanNumber() As String
        Get
            Return Exnumber
        End Get
        Set(ByVal value As String)
            Exnumber = value
        End Set
    End Property
#End Region
    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Dispose()
        frmLoan_ApplicationMain.chkPreTerminate.Checked = False
    End Sub
    Private Sub LoadLoansToGrid(ByVal empno As String, ByVal status As String, ByVal Exnumber As String)

        Dim gcon As New Clsappconfiguration
        Dim sSqlCmd2 As String = "[MSS_LoanTracking_GetLoans] '" & empno & "', '" & status & "','" & Exnumber & "'"
        Try
            Dim dt As New DataTable
            dt = SqlHelper.ExecuteDataset(gcon.cnstring, CommandType.Text, sSqlCmd2).Tables(0)
            gridPreterminate.DataSource = dt
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        gridPreterminate.Columns("Member Name").Visible = False
        gridPreterminate.Columns("Percent Interest").Visible = False
        gridPreterminate.Columns("Receivable").Visible = False
        gridPreterminate.Columns("Percent Service Fee").Visible = False
        gridPreterminate.Columns("Percent Capital Share").Visible = False
        gridPreterminate.Columns("Amortization Date From").Visible = False
        gridPreterminate.Columns("Amortization Date To").Visible = False
        gridPreterminate.Columns("Percent Penalty").Visible = False
        gridPreterminate.Columns("Interest Refund").Visible = False
        gridPreterminate.Columns("Net Proceeds").Visible = False
        gridPreterminate.Columns("Salary {Monthly}").Visible = False
        gridPreterminate.Columns("SSS Loans").Visible = False
        gridPreterminate.Columns("Pagibig Loans").Visible = False
        gridPreterminate.Columns("PhilHealth Loans").Visible = False
        gridPreterminate.Columns("Company Loans").Visible = False
        gridPreterminate.Columns("Others").Visible = False
        gridPreterminate.Columns("fk_Member_Loan").Visible = False
        gridPreterminate.Columns("EmpNo").Visible = False

        With gridPreterminate
            .Columns("Select").DisplayIndex = 0
            .Columns("Loan No").DisplayIndex = 1
            .Columns("Loan Type").DisplayIndex = 2
            .Columns("Terms").DisplayIndex = 3
            .Columns("C").DisplayIndex = 4
            .Columns("Principal").DisplayIndex = 5
            .Columns("Interest Amount").DisplayIndex = 6
            .Columns("Service Fee Amount").DisplayIndex = 7
            .Columns("Capital Share Amount").DisplayIndex = 8
            .Columns("Penalty Amount").DisplayIndex = 9
            .Columns("Amount Paid").DisplayIndex = 10
            .Columns("Balance").DisplayIndex = 11
        End With

    End Sub
    Private Sub frmPreTerminate_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call SetupPreterminateGrid()
        Call LoadLoansToGrid(Empinfo, "Approved", Exnumber)
    End Sub

    Private Sub SetupPreterminateGrid()
        Dim colSelect As New DataGridViewCheckBoxColumn
        Dim colLoanNo As New DataGridViewTextBoxColumn
        Dim colLoanType As New DataGridViewTextBoxColumn
        Dim colTerms As New DataGridViewTextBoxColumn
        Dim colClassification As New DataGridViewTextBoxColumn
        Dim colPrincipal As New DataGridViewTextBoxColumn
        Dim colInterest As New DataGridViewTextBoxColumn
        Dim colServiceFee As New DataGridViewTextBoxColumn
        Dim colCapitalShare As New DataGridViewTextBoxColumn
        Dim colPenalty As New DataGridViewTextBoxColumn
        Dim colPayment As New DataGridViewTextBoxColumn
        Dim colBalance As New DataGridViewTextBoxColumn

        With colSelect
            .HeaderText = "Select"
            .Name = "Select"
        End With

        With colLoanNo
            .HeaderText = "Loan No"
            .Name = "Loan No"
            .DataPropertyName = "Loan No"
        End With

        With colLoanType
            .HeaderText = "Loan Type"
            .Name = "Loan Type"
            .DataPropertyName = "Loan Type"
        End With

        With colTerms
            .HeaderText = "Terms"
            .Name = "Terms"
            .DataPropertyName = "Terms"
        End With

        With colClassification
            .HeaderText = "Classification"
            .Name = "C"
            .DataPropertyName = "C"
        End With

        With colPrincipal
            .HeaderText = "Principal"
            .Name = "Principal"
            .DataPropertyName = "Principal"
        End With

        With colInterest
            .HeaderText = "Interest"
            .Name = "Interest Amount"
            .DataPropertyName = "Interest Amount"
        End With

        With colServiceFee
            .HeaderText = "Service Fee"
            .Name = "Service Fee Amount"
            .DataPropertyName = "Service Fee Amount"
        End With

        With colCapitalShare
            .HeaderText = "Capital Share"
            .Name = "Capital Share Amount"
            .DataPropertyName = "Capital Share Amount"
        End With

        With colPenalty
            .HeaderText = "Penalty"
            .Name = "Penalty Amount"
            .DataPropertyName = "Penalty Amount"
        End With

        With colPayment
            .HeaderText = "Amount Paid"
            .Name = "Amount Paid"
            .DataPropertyName = "Amount Paid"
        End With

        With colBalance
            .HeaderText = "Balance"
            .Name = "Balance"
            .DataPropertyName = "Balance"
        End With

        With gridPreterminate.Columns
            .Add(colSelect)
            .Add(colLoanNo)
            .Add(colLoanType)
            .Add(colTerms)
            .Add(colClassification)
            .Add(colPrincipal)
            .Add(colInterest)
            .Add(colServiceFee)
            .Add(colCapitalShare)
            .Add(colPenalty)
            .Add(colPayment)
            .Add(colBalance)
        End With

    End Sub

    Private Sub cmdPreTerminate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdPreTerminate.Click
        Dim xRow As Integer = 0
        Dim xValue As String
        For Each SelectedItem As DataGridViewRow In gridPreterminate.Rows
            xRow = SelectedItem.Index
            If gridPreterminate.Item("Select", xRow).Value = True Then
                If xValue Is Nothing Then
                    xValue = gridPreterminate.Item("Loan No", xRow).Value
                Else
                    xValue &= "," & gridPreterminate.Item("Loan No", xRow).Value
                End If
            End If
        Next
        frmLoan_ApplicationMain.TxtCoopLoan.Text = xValue
        Me.Close()
    End Sub
End Class