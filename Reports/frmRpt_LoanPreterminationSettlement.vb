Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class frmRpt_LoanPreterminationSettlement

    Private gCon As New Clsappconfiguration
    Private rptsummary As New ReportDocument

    Private employeeNo As String
    Public Property GetEmployeeNo() As String
        Get
            Return employeeNo
        End Get
        Set(ByVal value As String)
            employeeNo = value
        End Set
    End Property

    Private asOfDate As Date
    Public Property GetDate() As Date
        Get
            Return asOfDate
        End Get
        Set(ByVal value As Date)
            asOfDate = value
        End Set
    End Property

    Private preparedBy As String
    Public Property GetUser() As String
        Get
            Return preparedBy
        End Get
        Set(ByVal value As String)
            preparedBy = value
        End Set
    End Property

    Private loanID As String
    Public Property GetLoanID() As String
        Get
            Return loanID
        End Get
        Set(ByVal value As String)
            loanID = value
        End Set
    End Property

    Private adjustedInterestRefund As Decimal
    Public Property GetAdjustedInterestRefund() As Decimal
        Get
            Return adjustedInterestRefund
        End Get
        Set(ByVal value As Decimal)
            adjustedInterestRefund = value
        End Set
    End Property

    Private Sub frmLoanPreterminationSettlement_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        rptsummary.Close()
    End Sub

    Private Sub frmLoanPreterminationSettlement_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        picLoading.Visible = True
        bgwLoadReport.RunWorkerAsync()
    End Sub

    Private Sub bgwLoadReport_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgwLoadReport.DoWork
        LoadReport()
    End Sub

    Private Sub bgwLoadReport_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwLoadReport.RunWorkerCompleted
        Me.crvLoanPretermination.Visible = True
        Me.crvLoanPretermination.ReportSource = rptsummary

        picLoading.Visible = False
    End Sub

    Private Sub LoadReport()
        Try
            rptsummary.Load(Application.StartupPath & "\LoanReport\Loan_Pretermination_Statement.rpt")
            Logon(rptsummary, gCon.Server, gCon.Database, gCon.Username, gCon.Password)
            rptsummary.Refresh()
            rptsummary.SetParameterValue("@employeeNo", GetEmployeeNo())
            rptsummary.SetParameterValue("@asOfDate", GetDate())
            rptsummary.SetParameterValue("@preparedBy", GetUser())
            rptsummary.SetParameterValue("@pk_Members_Loan", GetLoanID())
            rptsummary.SetParameterValue("@adjustedInterestRefund", GetAdjustedInterestRefund())
        Catch ex As Exception
        End Try
    End Sub

    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If
        Dim obj As ReportObject
        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function

    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            If (tbl.TestConnectivity()) Then
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next
        Return True
    End Function


End Class