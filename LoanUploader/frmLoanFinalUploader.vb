﻿Imports System.Data.SqlClient
Imports Microsoft.VisualBasic.FileSystem
Imports System.Data
Imports System.IO
Imports Microsoft.ApplicationBlocks.Data

Public Class frmLoanFinalUploader
    Dim filenym As String
    Dim fpath As String
    Dim itemCount As Long
    Dim con As New Clsappconfiguration

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        If MsgBox("Are you sure?", vbYesNo, "Exit") = vbYes Then
            Me.Close()
        Else
            Exit Sub
        End If
    End Sub

    Private Sub Import()
        Dim MyConnection As System.Data.OleDb.OleDbConnection
        Dim DtSet As System.Data.DataSet
        Dim MyCommand As System.Data.OleDb.OleDbDataAdapter
        MyConnection = New System.Data.OleDb.OleDbConnection("provider=Microsoft.Jet.OLEDB.4.0;Data Source='" + txtPath.Text.ToString + "';Extended Properties=Excel 8.0;")
        MyCommand = New System.Data.OleDb.OleDbDataAdapter("select * from [Sheet1$]", MyConnection)
        MyCommand.TableMappings.Add("Table", "Vincent00x21")
        DtSet = New System.Data.DataSet
        MyCommand.Fill(DtSet)
        dgvList.DataSource = DtSet.Tables(0)
        MyConnection.Close()
    End Sub

    Private Sub btnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse.Click
        Try
            OpenFileDialog1.ShowDialog()
            filenym = OpenFileDialog1.FileName
            fpath = Path.GetFullPath(filenym)
            txtPath.Text = fpath

            LoadBtnMark()
            Import()
        Catch ex As Exception
            frmMsgBox.txtMessage.Text = "User Cancelled!"
            frmMsgBox.ShowDialog()
        End Try
    End Sub

    Private Sub LoadBtnMark()
        Dim btnmark As New DataGridViewCheckBoxColumn
        With btnmark
            .Name = "btnmark"
            .HeaderText = "Mark"
            .Width = 50
        End With
        dgvList.Columns.Add(btnmark)
    End Sub

    Private Sub btnClearRows_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearRows.Click
        dgvList.Columns.Clear()
        dgvAmortization.Columns.Clear()
        txtNoofItems.Text = "00"
        txtTotalMarked.Text = "00"
    End Sub

    Private Sub dgvList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvList.Click
        Try
            If dgvList.SelectedRows(0).Cells(0).Value = True Then
                dgvList.SelectedRows(0).Cells(0).Value = False
                dgvList.SelectedRows(0).DefaultCellStyle.BackColor = Color.White
            Else
                dgvList.SelectedRows(0).Cells(0).Value = True
                dgvList.SelectedRows(0).DefaultCellStyle.BackColor = Color.SkyBlue
            End If
        Catch ex As Exception
            frmMsgBox.Text = "Error - No data listed!"
            frmMsgBox.txtMessage.Text = ex.Message
            frmMsgBox.ShowDialog()
        End Try
    End Sub

    Private Sub chkMarkAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkMarkAll.Click
        If chkMarkAll.Checked = True Then
            For i As Integer = 0 To dgvList.RowCount - 1
                dgvList.Rows(i).Cells(0).Value = True
                dgvList.Rows(i).DefaultCellStyle.BackColor = Color.SkyBlue
            Next
        Else
            For i As Integer = 0 To dgvList.RowCount - 1
                dgvList.Rows(i).Cells(0).Value = False
                dgvList.Rows(i).DefaultCellStyle.BackColor = Color.White
            Next
        End If
    End Sub

    Private Sub _totalItems() Handles btnBrowse.Click
        Dim tot As Integer = 0
        Try
            For i As Integer = 0 To dgvList.RowCount - 1
                tot += 1
            Next
            txtNoofItems.Text = tot
        Catch ex As Exception
            frmMsgBox.txtMessage.Text = ex.Message
            frmMsgBox.ShowDialog()
        End Try
    End Sub

    Private Sub _totalMarkItems() Handles chkMarkAll.Click
        If chkMarkAll.Checked = True Then
            Dim tot As Integer = 0
            Try
                For i As Integer = 0 To dgvList.RowCount - 1
                    If dgvList.Rows(i).Cells(0).Value = True Then
                        tot += 1
                    End If
                Next
                txtTotalMarked.Text = tot
            Catch ex As Exception
                frmMsgBox.Text = "Error - Mark all items"
                frmMsgBox.txtMessage.Text = ex.Message
                frmMsgBox.ShowDialog()
            End Try
        Else
            txtTotalMarked.Text = "00"
        End If
    End Sub

    Private Sub _totalMarkItems_byGrid() Handles dgvList.Click
        Dim tot As Integer = 0
        Try
            For i As Integer = 0 To dgvList.RowCount - 1
                If dgvList.Rows(i).Cells(0).Value = True Then
                    tot += 1
                End If
            Next
            txtTotalMarked.Text = tot
        Catch ex As Exception
            frmMsgBox.Text = "Error - Marking Items"
            frmMsgBox.txtMessage.Text = ex.Message
            frmMsgBox.ShowDialog()
        End Try
    End Sub

    Private Sub frmLoanFinalUploader_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub _GenerateAmortization() Handles btnGenerateAmortization.Click
        Dim rowctr As Integer = 0
        Try

            If MsgBox("Generate Amortization?", vbYesNo, "...") = vbYes Then
                dgvAmortization.Columns.Clear()
                Call GenerateAmortizationColumns()

                For i As Integer = 0 To dgvList.RowCount - 1
                    With dgvList
                        If .Item("btnmark", i).Value = True Then
                            Dim principal As Decimal = CDec(.Item("Principal", i).Value)

                            Dim terms As Decimal = 0
                            Select Case cboModeofPayment.Text
                                Case "DAILY"
                                    terms = .Item("Terms", i).Value * 30
                                Case "WEEKLY"
                                    terms = .Item("Terms", i).Value * 4
                                Case "SEMI-MONTHLY"
                                    terms = .Item("Terms", i).Value * 2
                                Case "MONTHLY"
                                    terms = .Item("Terms", i).Value
                                Case "QUARTERLY"
                                    terms = .Item("Terms", i).Value / 4
                                Case "ANNUALLY"
                                    terms = .Item("Terms", i).Value / 12
                                Case "LUMP SUM"
                                    terms = 1
                            End Select

                            Dim DateGranted As Date = .Item("DateGranted", i).Value
                            Dim FirstPayment As Date = DateAdd(DateInterval.Month, 1, DateGranted)
                            Dim mode As String = cboModeofPayment.Text
                            Dim loanref As String = .Item("LoanNo", i).Value.ToString

                            Dim interestrate As Decimal = 0
                            Select Case mode
                                Case "DAILY"
                                    interestrate = CDec(.Item("InterestRate", i).Value / 360)
                                Case "WEEKLY"
                                    interestrate = CDec(.Item("InterestRate", i).Value / 48)
                                Case "SEMI-MONTHLY"
                                    interestrate = CDec(.Item("InterestRate", i).Value / 24)
                                Case "MONTHLY"
                                    interestrate = CDec(.Item("InterestRate", i).Value / 12)
                                Case "QUARTERLY"
                                    interestrate = CDec(.Item("InterestRate", i).Value / 3)
                                Case "ANNUALLY"
                                    interestrate = CDec(.Item("InterestRate", i).Value / 1)
                                Case "LUMP SUM"
                                    interestrate = CDec(.Item("InterestRate", i).Value)
                            End Select

                            Select Case .Item("InterestMethod", i).Value
                                Case "Straight Add-On"
                                    Dim rd As SqlDataReader = SqlHelper.ExecuteReader(con.cnstring, "Calculate_Amortization_Schedule_Straight_AddOn",
                                       New SqlParameter("@PrincipalAmount", principal),
                                       New SqlParameter("@InterestRate", interestrate),
                                       New SqlParameter("@Terms", terms),
                                       New SqlParameter("@DateGranted", DateGranted),
                                       New SqlParameter("@FirstPayment", FirstPayment),
                                       New SqlParameter("@xmode", mode))
                                    While rd.Read
                                        With dgvAmortization
                                            .Rows.Add()
                                            .Item("payno", rowctr).Value = rd(0)
                                            .Item("paydate", rowctr).Value = rd(1)
                                            .Item("loanref", rowctr).Value = loanref
                                            .Item("principal", rowctr).Value = rd(2)
                                            .Item("interest", rowctr).Value = rd(3)
                                            .Item("servicefee", rowctr).Value = rd(4)
                                            .Item("amortization", rowctr).Value = rd(5)
                                            .Item("balance", rowctr).Value = rd(6)
                                            .Item("xmethod", rowctr).Value = "Straight Add-On"
                                        End With
                                        rowctr += 1
                                    End While
                                Case "Straight Deducted"
                                    Dim rd As SqlDataReader = SqlHelper.ExecuteReader(con.cnstring, "Calculate_Amortization_Schedule_Straight_Deducted",
                                       New SqlParameter("@PrincipalAmount", principal),
                                       New SqlParameter("@InterestRate", interestrate),
                                       New SqlParameter("@Terms", terms),
                                       New SqlParameter("@DateGranted", DateGranted),
                                       New SqlParameter("@FirstPayment", FirstPayment),
                                       New SqlParameter("@xmode", mode))
                                    While rd.Read
                                        With dgvAmortization
                                            .Rows.Add()
                                            .Item("payno", rowctr).Value = rd(0)
                                            .Item("paydate", rowctr).Value = rd(1)
                                            .Item("loanref", rowctr).Value = loanref
                                            .Item("principal", rowctr).Value = rd(2)
                                            .Item("interest", rowctr).Value = rd(3)
                                            .Item("servicefee", rowctr).Value = rd(4)
                                            .Item("amortization", rowctr).Value = rd(5)
                                            .Item("balance", rowctr).Value = rd(6)
                                            .Item("xmethod", rowctr).Value = "Straight Deducted"
                                        End With
                                        rowctr += 1
                                    End While
                                Case "Diminishing"
                                    Dim rd As SqlDataReader = SqlHelper.ExecuteReader(con.cnstring, "Calculate_Amortization_Schedule_Diminishing",
                                       New SqlParameter("@PrincipalAmount", principal),
                                       New SqlParameter("@InterestRate", interestrate),
                                       New SqlParameter("@Terms", terms),
                                       New SqlParameter("@DateGranted", DateGranted),
                                       New SqlParameter("@FirstPayment", FirstPayment),
                                       New SqlParameter("@xmode", mode))
                                    While rd.Read
                                        With dgvAmortization
                                            .Rows.Add()
                                            .Item("payno", rowctr).Value = rd(0)
                                            .Item("paydate", rowctr).Value = rd(1)
                                            .Item("loanref", rowctr).Value = loanref
                                            .Item("principal", rowctr).Value = rd(2)
                                            .Item("interest", rowctr).Value = rd(3)
                                            .Item("servicefee", rowctr).Value = rd(4)
                                            .Item("amortization", rowctr).Value = rd(5)
                                            .Item("balance", rowctr).Value = rd(6)
                                            .Item("xmethod", rowctr).Value = "Diminishing"
                                        End With
                                        rowctr += 1
                                    End While
                            End Select

                        End If
                    End With
                Next

                _GenerateColorDivision()
            Else
                MsgBox("User Cancelled!", vbInformation, "...")
            End If
        Catch ex As Exception
            frmMsgBox.Text = "Error - Generating Amortization"
            frmMsgBox.txtMessage.Text = ex.Message
            frmMsgBox.ShowDialog()
        End Try
    End Sub

    Private Sub _GenerateColorDivision()
        '#### Generate Color as division #####
        For x As Integer = 0 To dgvAmortization.RowCount - 1
            With dgvAmortization
                If .Item("payno", x).Value = "0" Then
                    dgvAmortization.Rows(x).DefaultCellStyle.BackColor = Color.SkyBlue
                Else
                    dgvAmortization.Rows(x).DefaultCellStyle.BackColor = Color.LightYellow
                End If
            End With
        Next
    End Sub

    Private Sub GenerateAmortizationColumns()
        Dim payno As New DataGridViewTextBoxColumn
        Dim paydate As New DataGridViewTextBoxColumn
        Dim loanref As New DataGridViewTextBoxColumn
        Dim principal As New DataGridViewTextBoxColumn
        Dim interest As New DataGridViewTextBoxColumn
        Dim servicefee As New DataGridViewTextBoxColumn
        Dim amortization As New DataGridViewTextBoxColumn
        Dim balance As New DataGridViewTextBoxColumn
        Dim xmethod As New DataGridViewTextBoxColumn
        With payno
            .Name = "payno"
            .HeaderText = "No."
        End With
        With paydate
            .Name = "paydate"
            .HeaderText = "Date"
        End With
        With loanref
            .Name = "loanref"
            .HeaderText = "Loan Ref."
        End With
        With principal
            .Name = "principal"
            .HeaderText = "Principal"
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
        End With
        With interest
            .Name = "interest"
            .HeaderText = "Interest"
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
        End With
        With servicefee
            .Name = "servicefee"
            .HeaderText = "Service Fee"
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
        End With
        With amortization
            .Name = "amortization"
            .HeaderText = "Amortization"
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
        End With
        With balance
            .Name = "balance"
            .HeaderText = "Balance"
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight
        End With
        With xmethod
            .Name = "xmethod"
            .HeaderText = "Method"
        End With
        With dgvAmortization
            .Columns.Add(payno)
            .Columns.Add(paydate)
            .Columns.Add(loanref)
            .Columns.Add(principal)
            .Columns.Add(interest)
            .Columns.Add(servicefee)
            .Columns.Add(amortization)
            .Columns.Add(balance)
            .Columns.Add(xmethod)
        End With
    End Sub

    Private Sub Upload_Loan() Handles btnUpload.Click
        Try
            If MsgBox("Are you sure?", vbYesNo, "Upload") = vbYes Then

                For i As Integer = 0 To dgvList.RowCount - 1
                    With dgvList
                        If .Item("btnmark", i).Value = True Then
                            Dim empid As String = .Item("ClientID", i).Value
                            Dim loanno As String = .Item("LoanNo", i).Value
                            Dim Principal As Decimal = CDec(.Item("Principal", i).Value)
                            Dim DateGranted As Date = .Item("DateGranted", i).Value
                            Dim EndingBalance As Decimal = CDec(.Item("EndingBalance", i).Value)
                            Dim Terms As Decimal = CDec(.Item("Terms", i).Value)
                            Dim InterestRate As Decimal = CDec(.Item("InterestRate", i).Value)
                            Dim InterestMethod As String = .Item("InterestMethod", i).Value
                            Dim LoanType As String = .Item("LoanType", i).Value
                            Dim maturity As Date = DateAdd(DateInterval.Month, Terms, DateGranted)

                            SqlHelper.ExecuteNonQuery(con.cnstring, "_Upload_Loan_AllDetails",
                               New SqlParameter("@clientid", empid),
                               New SqlParameter("@Loanno", loanno),
                               New SqlParameter("@principal", Principal),
                               New SqlParameter("@Applicationdate", DateGranted),
                               New SqlParameter("@dategranted", DateAdd(DateInterval.Month, 1, DateGranted)),
                               New SqlParameter("@maturity", maturity),
                               New SqlParameter("@balance", EndingBalance),
                               New SqlParameter("@term", Terms),
                               New SqlParameter("@interestrate", InterestRate),
                               New SqlParameter("@loantype", LoanType))

                            DeleteAmortization_ThenUpdate(loanno)
                            SaveAmortSchedule()
                            UpdateDocNumber(loanno)
                        End If
                    End With
                Next

                frmMsgBox.Text = "Uploading..."
                frmMsgBox.txtMessage.Text = "Success..."
                frmMsgBox.ShowDialog()
            Else
                MsgBox("User Cancelled!", vbInformation, "Message...")
            End If
        Catch ex As Exception
            frmMsgBox.Text = "Error - Upload Loan"
            frmMsgBox.txtMessage.Text = ex.Message
            frmMsgBox.ShowDialog()
        End Try
    End Sub

    Private Sub DeleteAmortization_ThenUpdate(ByVal loanno As String)
        Try
            SqlHelper.ExecuteNonQuery(con.cnstring, "_DeleteAmortization_Schedule_ForUpdating",
                                       New SqlParameter("@LoanNo", loanno))
        Catch ex As Exception
            frmMsgBox.Text = "Error - Delete Amortization"
            frmMsgBox.txtMessage.Text = ex.Message
            frmMsgBox.ShowDialog()
        End Try
    End Sub

    Private Sub SaveAmortSchedule() 'by Vincent Nacar
        Try
            For i As Integer = 0 To dgvAmortization.RowCount - 1
                With dgvAmortization
                    Dim loanNo As String = .Item("loanref", i).Value
                    Dim payNo As String = .Item("payno", i).Value
                    Dim fcDate As String = .Item("paydate", i).Value
                    Dim xprincipal As String = .Item("principal", i).Value
                    Dim xinterest As String = .Item("interest", i).Value
                    Dim xservicefee As String = .Item("servicefee", i).Value
                    Dim xamortization As String = .Item("amortization", i).Value
                    Dim balance As String = .Item("balance", i).Value
                    Dim method As String = .Item("xmethod", i).Value
                    Dim status As String = "Unpaid"

                    SqlHelper.ExecuteNonQuery(con.cnstring, "_Insert_AmortizationSchedule",
                                               New SqlParameter("@pk_KeyID", System.Guid.NewGuid),
                                               New SqlParameter("@fcLoanNo", loanNo),
                                               New SqlParameter("@fdPayNo", payNo),
                                               New SqlParameter("@fcDate", fcDate),
                                               New SqlParameter("@fdPrincipal", xprincipal),
                                               New SqlParameter("@fdInterest", xinterest),
                                               New SqlParameter("@fdServiceFee", xservicefee),
                                               New SqlParameter("@fdAmortization", xamortization),
                                               New SqlParameter("@fdBalance", balance),
                                               New SqlParameter("@fcMethod", method),
                                               New SqlParameter("@fcStatus", status))
                End With
            Next
        Catch ex As Exception
            frmMsgBox.Text = "Error - Saving Amortization"
            frmMsgBox.txtMessage.Text = ex.Message
            frmMsgBox.ShowDialog()
        End Try
    End Sub

    Private Sub UpdateDocNumber(ByVal loanno As String)
        Try
            SqlHelper.ExecuteNonQuery(con.cnstring, "_Update_DocNumber_Used",
                           New SqlParameter("@DocNumber", loanno),
                           New SqlParameter("@DateUsed", Date.Now))
        Catch ex As Exception
            frmMsgBox.Text = "Error - Update Doc. Number"
            frmMsgBox.txtMessage.Text = ex.Message
            frmMsgBox.ShowDialog()
        End Try
    End Sub

    Private Sub btnTool_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTool.Click
        frmDocIssuer.StartPosition = FormStartPosition.CenterScreen
        frmDocIssuer.ShowDialog()
    End Sub

    Private Sub btnAmortTool_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAmortTool.Click
        frmAmortGenerator.StartPosition = FormStartPosition.CenterScreen
        frmAmortGenerator.ShowDialog()
    End Sub

End Class