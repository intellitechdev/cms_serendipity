Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmBankSettings
    Private gcon As New Clsappconfiguration()
#Region "Private value"

    Private savacc As String
    Private intacc As String
    Private cashacc As String
    Private tdsavacc As String
    Private tdintacc As String
    Private tdcashacc As String
    Private mbal As String
    Private mode As Boolean
    Private int As String
    Private period As String
    Private modetd As Boolean
    Private inttd As String
    Private periodtd As String
    Private Bank As String
    Private ID As Integer

#End Region
    Private Sub frmBankSettings_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            SavingsAccount()
            InterestAccount()
            ViewCashAccount()
            LoadBankSetting()
            LoadBankSettingTimeDep()

            BankList()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Savings and Time Deposit", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
#Region "Accounts"
    Private Sub LoadBankSetting()
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.StoredProcedure, "CIMS_m_Policies_BankSettings_Savings_Load")
        txtMaintainingBal.Clear()
        txtint.Clear()
        Try
            If rd.Read() Then
                txtMaintainingBal.SelectedText = rd.Item(0)
                cboSavingAccount.SelectedValue = rd.Item(1)
                cboIntAcc.SelectedValue = rd.Item(2)
                cboCashAcc.SelectedValue = rd.Item(3)
                rdbAuto.Checked = rd.Item(4)
                txtint.SelectedText = rd.Item(5)
                cboperiod.SelectedText = rd.Item(6)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub LoadBankSettingTimeDep()
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.StoredProcedure, "CIMS_m_Policies_BankSettings_TimeDeposit_Load")
        txtinttd.Clear()
        Try
            If rd.Read() Then
                Me.cbotdsav.SelectedValue = rd.Item(0)
                Me.cbotdint.SelectedValue = rd.Item(1)
                Me.cbotdcash.SelectedValue = rd.Item(2)
                Me.rdbtdauto.Checked = rd.Item(3)
                Me.txtinttd.SelectedText = rd.Item(4)
                Me.txtperiodtd.Text = rd.Item(5)

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub SavingsAccount()
        Dim sSqlCmd As String = "CIMS_Masterfiles_Accounts_Load "
        Try
            cboSavingAccount.DataSource = SqlHelper.ExecuteDataset(gcon.cnstring, CommandType.Text, sSqlCmd).Tables(0).DefaultView
            cboSavingAccount.DisplayMember = "Account Name"
            cboSavingAccount.ValueMember = "Account ID"
            cbotdsav.DataSource = SqlHelper.ExecuteDataset(gcon.cnstring, CommandType.Text, sSqlCmd).Tables(0).DefaultView
            cbotdsav.DisplayMember = "Account Name"
            cbotdsav.ValueMember = "Account ID"
        Catch ex As Exception
            MsgBox("Error at loading Savings to combobox." & vbCr & ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Access denied")
        End Try
    End Sub
    Private Sub InterestAccount()
        Dim sSqlCmd As String = "CIMS_Masterfiles_Accounts_Load "
        Try
            cboIntAcc.DataSource = SqlHelper.ExecuteDataset(gcon.cnstring, CommandType.Text, sSqlCmd).Tables(0).DefaultView
            cboIntAcc.DisplayMember = "Account Name"
            cboIntAcc.ValueMember = "Account ID"
            cbotdint.DataSource = SqlHelper.ExecuteDataset(gcon.cnstring, CommandType.Text, sSqlCmd).Tables(0).DefaultView
            cbotdint.DisplayMember = "Account Name"
            cbotdint.ValueMember = "Account ID"
        Catch ex As Exception
            MsgBox("Error at loading Interest to combobox." & vbCr & ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Access denied")
        End Try
    End Sub
    Private Sub ViewCashAccount()
        Dim sSqlCmd As String = "CIMS_Masterfiles_Accounts_Load "
        Try
            cboCashAcc.DataSource = SqlHelper.ExecuteDataset(gcon.cnstring, CommandType.Text, sSqlCmd).Tables(0).DefaultView
            cboCashAcc.DisplayMember = "Account Name"
            cboCashAcc.ValueMember = "Account ID"
            cbotdcash.DataSource = SqlHelper.ExecuteDataset(gcon.cnstring, CommandType.Text, sSqlCmd).Tables(0).DefaultView
            cbotdcash.DisplayMember = "Account Name"
            cbotdcash.ValueMember = "Account ID"
        Catch ex As Exception
            MsgBox("Error at loading Cash to combobox." & vbCr & ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Access denied")
        End Try
    End Sub

#End Region
#Region "EDIT"
    Private Sub SavingsEdit(ByVal mbal As String, ByVal savacc As String, ByVal intacc As String, _
                            ByVal cashacc As String, ByVal mode As Boolean, _
                             ByVal int As String, ByVal period As String)
        Dim mycon As New Clsappconfiguration
        mycon.sqlconn.Open()
        Dim trans As SqlTransaction = mycon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_m_Policies_BankSettings_Savings_AddEdit", _
                                      New SqlParameter("@MaintainingBal", mbal), _
                                      New SqlParameter("@AccountSav", savacc), _
                                      New SqlParameter("@AccountInt", intacc), _
                                      New SqlParameter("@AccountCash", cashacc), _
                                      New SqlParameter("@mode", mode), _
                                      New SqlParameter("@Interest", int), _
                                      New SqlParameter("@Period", period))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "EDITSAVINGS")
        Finally
            mycon.sqlconn.Close()
        End Try
    End Sub
    Private Sub TimeDepositEdit(ByVal savacc As String, ByVal intacc As String, _
                            ByVal cashacc As String, ByVal mode As Boolean, _
                             ByVal int As String, ByVal period As String)
        Dim mycon As New Clsappconfiguration
        mycon.sqlconn.Open()
        Dim trans As SqlTransaction = mycon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_m_Policies_BankSettings_Timedeposit_AddEdit", _
                                      New SqlParameter("@AccountSav", savacc), _
                                      New SqlParameter("@AccountInt", intacc), _
                                      New SqlParameter("@AccountCash", cashacc), _
                                      New SqlParameter("@mode", mode), _
                                      New SqlParameter("@Interest", int), _
                                      New SqlParameter("@period", period))

            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "EDITTIMEDEPOSIT")
        Finally
            mycon.sqlconn.Close()
        End Try
    End Sub
#End Region
#Region "getvalue"

    Private Sub cboSavingAccount_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboSavingAccount.KeyPress
        e.KeyChar = Microsoft.VisualBasic.ChrW(0)
    End Sub
    Private Sub cboIntAcc_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboIntAcc.KeyPress
        e.KeyChar = Microsoft.VisualBasic.ChrW(0)
    End Sub
    Private Sub cboCashAcc_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboCashAcc.KeyPress
        e.KeyChar = Microsoft.VisualBasic.ChrW(0)
    End Sub
    Private Sub txtMaintainingBal_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtMaintainingBal.KeyPress
        If e.KeyChar <> Chr(46) Then
            If Not Char.IsDigit(e.KeyChar) Then e.Handled = True
            If e.KeyChar = Chr(8) Then e.Handled = False
            If e.KeyChar = Chr(13) Then txtMaintainingBal.Focus()
        End If
    End Sub
    Private Sub txtint_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtint.KeyPress
        If e.KeyChar <> Chr(46) Then
            If Not Char.IsDigit(e.KeyChar) Then e.Handled = True
            If e.KeyChar = Chr(8) Then e.Handled = False
            If e.KeyChar = Chr(13) Then txtint.Focus()
        End If
    End Sub
    Private Sub cboperiod_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboperiod.KeyPress
        e.KeyChar = Microsoft.VisualBasic.ChrW(0)
    End Sub

    Private Sub cbotdsav_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbotdsav.Click
        SavingsAccount()
    End Sub
    Private Sub cbotdint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbotdint.Click
        InterestAccount()
    End Sub
    Private Sub cbotdcash_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbotdcash.Click
        ViewCashAccount()
    End Sub

    Private Sub cbotdsav_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cbotdsav.KeyPress
        e.KeyChar = Microsoft.VisualBasic.ChrW(0)
    End Sub
    Private Sub cbotdint_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cbotdint.KeyPress
        e.KeyChar = Microsoft.VisualBasic.ChrW(0)
    End Sub
    Private Sub cbotdcash_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cbotdcash.KeyPress
        e.KeyChar = Microsoft.VisualBasic.ChrW(0)
    End Sub
    Private Sub txtinttd_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtinttd.KeyPress
        If e.KeyChar <> Chr(46) Then
            If Not Char.IsDigit(e.KeyChar) Then e.Handled = True
            If e.KeyChar = Chr(8) Then e.Handled = False
            If e.KeyChar = Chr(13) Then txtinttd.Focus()
        End If
    End Sub
    Private Sub txtperiodtd_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtperiodtd.KeyPress
        If e.KeyChar <> Chr(46) Then
            If Not Char.IsDigit(e.KeyChar) Then e.Handled = True
            If e.KeyChar = Chr(8) Then e.Handled = False
            If e.KeyChar = Chr(13) Then txtperiodtd.Focus()
        End If
    End Sub




    Private Sub cboSavingAccount_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSavingAccount.SelectedValueChanged
        savacc = Me.cboSavingAccount.SelectedValue.ToString
    End Sub
    Private Sub cboIntAcc_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboIntAcc.SelectedValueChanged
        intacc = Me.cboIntAcc.SelectedValue.ToString
    End Sub
    Private Sub cboCashAcc_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCashAcc.SelectedValueChanged
        cashacc = Me.cboCashAcc.SelectedValue.ToString
    End Sub
    Private Sub txtMaintainingBal_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtMaintainingBal.TextChanged
        mbal = txtMaintainingBal.Text
    End Sub
    Private Sub txtint_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtint.TextChanged
        int = txtint.Text
    End Sub
    Private Sub rdbAuto_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbAuto.CheckedChanged

        If rdbAuto.Checked = "True" Then
            mode = rdbAuto.Checked = True
        Else
            mode = False
        End If

    End Sub
    Private Sub cboperiod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboperiod.SelectedIndexChanged
        period = cboperiod.SelectedItem.ToString
    End Sub

    Private Sub cbotdint_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbotdint.SelectedValueChanged
        tdintacc = cbotdint.SelectedValue.ToString
    End Sub
    Private Sub cbotdcash_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbotdcash.SelectedValueChanged
        tdcashacc = cbotdcash.SelectedValue.ToString
    End Sub
    Private Sub cbotdsav_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbotdsav.SelectedValueChanged
        tdsavacc = cbotdsav.SelectedValue.ToString
    End Sub
    Private Sub rdbtdauto_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbtdauto.CheckedChanged
        If rdbtdauto.Checked = "True" Then
            modetd = rdbtdauto.Checked = True
        Else
            modetd = False
        End If
    End Sub

    Private Sub txtinttd_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtinttd.TextChanged
        inttd = txtinttd.Text
    End Sub
    Private Sub txtperiodtd_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtperiodtd.TextChanged
        periodtd = txtperiodtd.Text
    End Sub
#End Region
    Private Sub btnapply_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnapply.Click
        If MessageBox.Show("Apply Changes?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            If tabbankset.SelectedTab.Text = "Savings" Then
                SavingsEdit(mbal, savacc, intacc, cashacc, mode, int, period)
            ElseIf tabbankset.SelectedTab.Text = "Time Deposit" Then
                TimeDepositEdit(tdsavacc, tdintacc, tdcashacc, modetd, inttd, periodtd)
            Else

            End If

            MessageBox.Show("Changes has been Applied", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Else

            MessageBox.Show("Changes Canceled", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)

        End If
    End Sub
    Private Sub tabbankset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles tabbankset.Click
        If tabbankset.SelectedTab.Text = "Banks" Then
            btnapply.Visible = False
            btnCancel.Visible = False
            btnsave.Visible = True
            btnupdate.Visible = True
            btnClose.Visible = True
            btndelete.Visible = True
        Else
            btnapply.Visible = True
            btnCancel.Visible = True
            btnsave.Visible = False
            btnupdate.Visible = False
            btnClose.Visible = False
            btndelete.Visible = False
        End If
    End Sub
    Private Sub btnCancelB_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Close()
    End Sub
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Close()
    End Sub
    Private Sub BankListEdit(ByVal ID As Integer, ByVal Bank As String)
        Dim mycon As New Clsappconfiguration
        mycon.sqlconn.Open()
        Dim trans As SqlTransaction = mycon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_m_Policies_BankSettings_BankList_AddEdit", _
                                     New SqlParameter("@pk", ID), _
                                     New SqlParameter("@BankName", Bank))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "EDITBankList")
        Finally
            mycon.sqlconn.Close()
        End Try
    End Sub
    Private Sub BankList()
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(gcon.cnstring, CommandType.StoredProcedure, "CIMS_m_Policies_BankSettings_BankList_Load")
        Try
            With Me.grdBank
                .DataSource = ds.Tables(0)
                .ReadOnly = True
                .Columns(0).Visible = False
                .Columns(1).Width = 515
                .Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            End With
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub BankListDelete(ByVal ID As String)
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_m_Policies_BankSettings_BankList_Delete", _
                                      New SqlParameter("@ID", ID))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub


    Private Sub btnupdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupdate.Click
        Try
            If btnupdate.Text = "Edit" Then
                Label1.Text = "Edit Bank Name"
                txtBank.Text = Me.grdBank.CurrentRow.Cells(1).Value.ToString
                'ID = Me.grdBank.CurrentRow.Cells(0).Value.ToString
                btnsave.Enabled = False
                btnupdate.Text = "Update"
                btnupdate.Image = My.Resources.save2
                btnClose.Text = "Cancel"
                btndelete.Visible = False

            ElseIf btnupdate.Text = "Update" Then
                'If Me.cboLoantype.Text <> "" And Me.txtdescription.Text <> "" Then
                Label1.Text = "Bank"
                BankListEdit(Me.grdBank.CurrentRow.Cells(0).Value.ToString, Me.txtBank.Text)
                BankList()
                'btndelete.Visible = True
                btnupdate.Text = "Edit"
                btnupdate.Image = My.Resources.edit1
                btnClose.Text = "Cancel"
                btnClose.Text = "Close"
                btnsave.Enabled = True
                btndelete.Visible = True

            Else
                MessageBox.Show("Empty field found! ", "Edit", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If


        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub btndelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndelete.Click
        Try
            ID = grdBank.CurrentRow.Cells(0).Value.ToString
            Dim x As New DialogResult
            x = MessageBox.Show("Are you sure you want to permanently delete this record?", "Confirm Deletion", MessageBoxButtons.OKCancel, MessageBoxIcon.Information)
            If x = System.Windows.Forms.DialogResult.OK Then
                Call BankListDelete(ID)
                BankList()
            ElseIf x = System.Windows.Forms.DialogResult.Cancel Then
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub btnsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsave.Click
        Try
            If btnsave.Text = "New" Then

                Label1.Text = "Add Bank"
                txtBank.Text = ""
                btndelete.Visible = False
                btnupdate.Visible = False
                btnsave.Text = "Save"
                btnsave.Image = My.Resources.save2
                btnClose.Text = "Cancel"
                'btnClose.Location = New System.Drawing.Point(74, 6)

            ElseIf btnsave.Text = "Save" Then
                'If Me.cboLoantype.Text <> "" And Me.txtdescription.Text <> "" Then
                Label1.Text = "Bank"
                Call BankListEdit("0", txtBank.Text)
                BankList()
                btnsave.Text = "New"
                btnsave.Image = My.Resources.new3
                btnClose.Text = "Close"
                'btnClose.Location = New System.Drawing.Point(218, 6)
                btndelete.Visible = True
                btnupdate.Visible = True
            Else
                MessageBox.Show("Empty field found! ", "Save", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If

        Catch ex As Exception
        End Try
    End Sub

    'Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton1.CheckedChanged
    '    If RadioButton1.Checked = True Then
    '        frmManual_Interest.ShowDialog()
    '    Else
    '        frmManual_Interest.Close()
    '    End If
    'End Sub

    'Private Sub RadioButton4_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton4.CheckedChanged
    '    If RadioButton4.Checked = True Then
    '        frmManual_Interest.ShowDialog()
    '    Else
    '        frmManual_Interest.Close()
    '    End If
    'End Sub
End Class