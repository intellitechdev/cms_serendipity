Public Class frmflashscreen
    Inherits System.Windows.Forms.Form
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick

        If Me.Opacity < 1 Then
            Me.Opacity = Me.Opacity + 0.02
            Me.Refresh()
        End If

        If Me.Opacity = 1.0 Then
            Timer1.Enabled = False
            frmLogin.Show()
            Me.Close()
        End If

    End Sub

    Private Sub mbaflashscreen_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Opacity = 0.1
    End Sub

End Class