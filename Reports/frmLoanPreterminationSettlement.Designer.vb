<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLoanPreterminationSettlement
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLoanPreterminationSettlement))
        Me.crvLoanPretermination = New CrystalDecisions.Windows.Forms.CrystalReportViewer
        Me.picLoading = New System.Windows.Forms.PictureBox
        Me.bgwLoadReport = New System.ComponentModel.BackgroundWorker
        CType(Me.picLoading, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'crvLoanPretermination
        '
        Me.crvLoanPretermination.ActiveViewIndex = -1
        Me.crvLoanPretermination.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.crvLoanPretermination.DisplayGroupTree = False
        Me.crvLoanPretermination.Dock = System.Windows.Forms.DockStyle.Fill
        Me.crvLoanPretermination.Location = New System.Drawing.Point(0, 0)
        Me.crvLoanPretermination.Name = "crvLoanPretermination"
        Me.crvLoanPretermination.SelectionFormula = ""
        Me.crvLoanPretermination.ShowGroupTreeButton = False
        Me.crvLoanPretermination.ShowRefreshButton = False
        Me.crvLoanPretermination.Size = New System.Drawing.Size(645, 544)
        Me.crvLoanPretermination.TabIndex = 0
        Me.crvLoanPretermination.ViewTimeSelectionFormula = ""
        '
        'picLoading
        '
        Me.picLoading.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.picLoading.Image = Global.WindowsApplication2.My.Resources.Resources.LoadingData
        Me.picLoading.Location = New System.Drawing.Point(247, 203)
        Me.picLoading.Name = "picLoading"
        Me.picLoading.Size = New System.Drawing.Size(159, 106)
        Me.picLoading.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picLoading.TabIndex = 1
        Me.picLoading.TabStop = False
        Me.picLoading.Visible = False
        '
        'bgwLoadReport
        '
        Me.bgwLoadReport.WorkerSupportsCancellation = True
        '
        'frmLoanPreterminationSettlement
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(645, 544)
        Me.Controls.Add(Me.picLoading)
        Me.Controls.Add(Me.crvLoanPretermination)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(453, 427)
        Me.Name = "frmLoanPreterminationSettlement"
        Me.Text = "Loan Pretermination Settlement Report"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.picLoading, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents crvLoanPretermination As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents picLoading As System.Windows.Forms.PictureBox
    Friend WithEvents bgwLoadReport As System.ComponentModel.BackgroundWorker
End Class
