﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmExistingInvestment

    Private con As New Clsappconfiguration
    Dim ds As DataSet

    Private Sub frmExistingInvestment_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If isLoaded("") Then
            txtSearch.Text = ""
            ActiveControl = txtSearch
        End If
    End Sub

    Private Function isLoaded(ByVal key As String) As Boolean
        Try
            ds = SqlHelper.ExecuteDataset(con.cnstring, "_Select_Existing_Investment",
                                           New SqlParameter("@search", key))
            dgvList.DataSource = ds.Tables(0)
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub txtSearch_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSearch.KeyDown
        If e.KeyCode = Keys.Enter Then
            If isSelected() Then
                If frmInvestmentApplication.isLoad_Amortization() Then
                    If frmInvestmentApplication.isLoad_InvestmentDetails Then
                        Me.Close()
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        If isLoaded(txtSearch.Text) Then

        End If
    End Sub

    Private Sub btnSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelect.Click
        If isSelected() Then
            If frmInvestmentApplication.isLoad_Amortization() Then
                If frmInvestmentApplication.isLoad_InvestmentDetails Then
                    Me.Close()
                End If
            End If
        End If
    End Sub

    Private Function isSelected() As Boolean
        Try
            frmInvestmentApplication.investmentNo = dgvList.SelectedRows(0).Cells(2).Value.ToString
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

End Class