Imports System.Configuration
Imports System.Xml
Imports System.Data.SqlClient
Public Class frmAdmin_ChangeDBConnection
#Region "Variables"
    Public getserver As String
    Public getdatabase As String
    Private getusername As String
    Private getpassword As String
#End Region

#Region "Get Property"
    Public ReadOnly Property getservername() As String
        Get
            Return getserver
        End Get
    End Property

    Public ReadOnly Property getdatabasename() As String
        Get
            Return getdatabase
        End Get
    End Property

    Public ReadOnly Property getusernametxt() As String
        Get
            Return getusername
        End Get
    End Property
    Public ReadOnly Property getpasswordtxt() As String
        Get
            Return getpassword
        End Get
    End Property
#End Region

    Private Sub ChkWinAuthin_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ChkWinAuthin.CheckedChanged
        If Me.ChkWinAuthin.Checked = True Then
            Me.txtusername.Text = ""
            Me.txtpassword.Text = ""
        End If
    End Sub
#Region "save to app.config"
    Private Sub SaveAppconfig()
        Save_Config_Parameter("Server", Me.txtServer.Text)
        Save_Config_Parameter("Database", Me.txtDatabase.Text)
        Save_Config_Parameter("Username", Me.txtusername.Text)
        Save_Config_Parameter("Password", Me.txtpassword.Text)


        getpassword = txtpassword.Text.Trim
        getusername = txtusername.Text.Trim
        getdatabase = txtDatabase.Text.Trim
        getserver = txtServer.Text.Trim
    End Sub
#End Region
#Region "funtion for saving app.config"
    Public Function Save_Config_Parameter(ByVal MyKey As String, ByVal MyValue As String) As String
        Dim XmlDocument As New XmlDocument
        Dim XmlNode As XmlNode
        Dim XmlRoot As XmlNode
        Dim XmlKey As XmlNode
        Dim XmlValue As XmlNode

        Save_Config_Parameter = ""
        XmlDocument.Load(Application.ExecutablePath & ".config")
        XmlNode = XmlDocument.DocumentElement.SelectSingleNode("/configuration/appSettings/add[@key=""" & MyKey & """]")
        If XmlNode Is Nothing Then

            ' The node does not exist, let's create it 

            XmlNode = XmlDocument.CreateNode(XmlNodeType.Element, "add", "")

            ' Adding the Key attribute to the node, keep in mind, Xml tokens are case sensitive 
            ' We should use 'key' instead of 'Key' 

            XmlKey = XmlDocument.CreateNode(XmlNodeType.Attribute, "key", "")
            XmlKey.Value = MyKey
            XmlNode.Attributes.SetNamedItem(XmlKey)

            ' Adding the key value, once again, remember that Xml tokens are case sensitive 

            XmlValue = XmlDocument.CreateNode(XmlNodeType.Attribute, "value", "")
            XmlValue.Value = MyValue
            XmlNode.Attributes.SetNamedItem(XmlValue)

            ' Add the new node to the root 

            XmlRoot = XmlDocument.DocumentElement.SelectSingleNode("/configuration/appSettings")
            If Not XmlRoot Is Nothing Then
                XmlRoot.AppendChild(XmlNode)
            Else
                Save_Config_Parameter = "ERROR"
            End If
        Else
            XmlNode.Attributes.GetNamedItem("value").Value = MyValue

        End If

        XmlDocument.Save(Application.ExecutablePath & ".config")


        XmlDocument = Nothing

    End Function
#End Region

    Private Sub OK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK.Click

        'Try
        '    With Me
        '        gUpdateSettingValue(.txtsqlservername.Text, .txtDbasename.Text)
        '        gUpdateSettingValue(.txtusername.Text, .txtpassword.Text)
        '        MessageBox.Show("updated with success.", "", MessageBoxButtons.OK, MessageBoxIcon.Information)
        '    End With

        'Catch ex As Exception
        '    MessageBox.Show(ex.Message & " - " & ex.Source)
        'End Try
        Call SaveAppconfig()
        Application.Restart()
        'Call Connection_Change()
        Me.Close()

    End Sub

    Private Sub Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel.Click
        Me.Close()
    End Sub

    Private Function Connection_Validate(ByVal sServer As String, ByVal sDatabase As String, ByVal sUserName As String, ByVal sPassword As String) As Boolean
        Dim cnct As SqlConnection
        Dim pword As New Clsappconfiguration

        Try
            cnct = New SqlConnection
            cnct.ConnectionString = "Data Source=" & sServer & "; " & "Initial Catalog=" & sDatabase & "; " & "User ID=" & sUserName & "; " & "password=" & sPassword & "; "
            cnct.Open()

            If ConnectionState.Open Then
                cnct.Close()
                Return True
            Else
                MessageBox.Show("Invalid Connection!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return False
            End If
        Catch ex As Exception
            'MessageBox.Show("Connection_Validate", Me.Name, Err.Description & " " & Err.Number)
        Finally
            cnct = Nothing
        End Try
    End Function
    Private Sub Connection_Change()
        Dim xmldoc As New XmlDocument
        Dim xmldoc1 As New XmlDocument
        'Dim node1, node2 As XmlElement
        'Dim nodes As XmlNodeList
        'Dim attrib As XmlAttributeCollection
        Dim pword As New Clsappconfiguration
        'Dim eProject_Conn As New eProject.CONNECTION_PARAMETER

        Me.UseWaitCursor = True



        If Connection_Validate(txtServer.Text, txtDatabase.Text, txtusername.Text, txtpassword.Text) Then
            Dim gcon As New Clsappconfiguration

            With gcon
                .Server = txtServer.Text
                .Database = txtDatabase.Text
                .Username = txtusername.Text
                .Password = txtpassword.Text
            End With

            My.MySettings.Default.Save()
            My.MySettings.Default.Reload()


            If MessageBox.Show("Connection was changed!", "Connect", MessageBoxButtons.OK) = MessageBoxButtons.OK Then
                frmLogin.txtServer.Text = txtServer.Text 'txtDbasename.Text
                frmLogin.txtDatabase.Text = txtDatabase.Text
                Me.Close()

            Else
                Me.Close()
            End If
        Else

            Me.Close()
        End If
    End Sub

    Private Sub frmDatabaseParameter_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.txtServer.Text = frmLogin.txtServer.Text
        Me.txtDatabase.Text = frmLogin.txtDatabase.Text
    End Sub

End Class