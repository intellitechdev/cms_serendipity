Public Class frmCAPRSecIIIb

    Public xParent As frmCAPRViewer
    Private Sub txt71Amt_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt71Amt.TextChanged, txt73Amt.TextChanged, txt72Amt.TextChanged
        If txt71Amt.Text = "" Then
            txt71Amt.Text = "0.00"
        End If
        If txt72Amt.Text = "" Then
            txt72Amt.Text = "0.00"
        End If
        If txt73Amt.Text = "" Then
            txt73Amt.Text = "0.00"
        End If
        txt74Amt.Text = Format((CDec(txt71Amt.Text) + CDec(txt72Amt.Text)) - CDec(txt73Amt.Text), "##,##0.00")
    End Sub

    Private Sub txt75Amt_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt75Amt.TextChanged, txt76Amt.TextChanged
        If txt75Amt.Text = "" Then
            txt75Amt.Text = "0.00"
        End If
        If txt76Amt.Text = "" Then
            txt76Amt.Text = "0.00"
        End If
        txt77Amt.Text = Format(CDec(txt75Amt.Text) - CDec(txt76Amt.Text), "##,##0.00")
    End Sub

    Private Sub btnPrevious_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrevious.Click
        Select Case TabControl1.SelectedIndex
            Case 3
                TabControl1.SelectTab(2)

            Case 2
                TabControl1.SelectTab(1)

            Case 1
                TabControl1.SelectTab(0)

            Case 0
                Me.Hide()
                xFormSec3.Show()
        End Select
    End Sub

    Private Sub txt41No_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt41No.TextChanged, txt42No.TextChanged, txt43No.TextChanged, txt44No.TextChanged, txt45No.TextChanged
        If txt41No.Text = "" Then
            txt41No.Text = 0
        End If
        If txt42No.Text = "" Then
            txt42No.Text = 0
        End If
        If txt43No.Text = "" Then
            txt43No.Text = 0
        End If
        If txt44No.Text = "" Then
            txt44No.Text = 0
        End If
        If txt45No.Text = "" Then
            txt45No.Text = 0
        End If

        txt46No.Text = Format(CDec(txt41No.Text) + CDec(txt42No.Text) + CDec(txt43No.Text) + _
                                CDec(txt44No.Text) + CDec(txt45No.Text), "##,##0")
    End Sub

    Private Sub txt41Amt_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt41Amt.TextChanged, txt42Amt.TextChanged, txt43Amt.TextChanged, txt44Amt.TextChanged, txt45Amt.TextChanged
        If txt41Amt.Text = "" Then
            txt41Amt.Text = "0.00"
        End If
        If txt42Amt.Text = "" Then
            txt42Amt.Text = "0.00"
        End If
        If txt43Amt.Text = "" Then
            txt43Amt.Text = "0.00"
        End If
        If txt44Amt.Text = "" Then
            txt44Amt.Text = "0.00"
        End If
        If txt45Amt.Text = "" Then
            txt45Amt.Text = "0.00"
        End If
        txt46Amt.Text = Format(CDec(txt41Amt.Text) + CDec(txt42Amt.Text) + CDec(txt43Amt.Text) + _
                                CDec(txt45Amt.Text) + CDec(txt44Amt.Text), "##,##0.00")
    End Sub

    Private Sub txt81Percentage_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt81Percentage.TextChanged, txt82Percentage.TextChanged, txt83Percentage.TextChanged, txt84Percentage.TextChanged, txt85Percentage.TextChanged
        If txt81Percentage.Text = "" Then
            txt81Percentage.Text = "0.00"
        End If
        If txt82Percentage.Text = "" Then
            txt82Percentage.Text = "0.00"
        End If
        If txt83Percentage.Text = "" Then
            txt83Percentage.Text = "0.00"
        End If
        If txt84Percentage.Text = "" Then
            txt84Percentage.Text = "0.00"
        End If
        If txt85Percentage.Text = "" Then
            txt85Percentage.Text = "0.00"
        End If

        txt86Percentage.Text = Format(CDec(txt81Percentage.Text) + CDec(txt82Percentage.Text) + _
                                        CDec(txt83Percentage.Text) + CDec(txt84Percentage.Text) + CDec(txt85Percentage.Text), "##,##0.00")

    End Sub

    Private Sub txt81Amt_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt81Amt.TextChanged, txt82Amt.TextChanged, txt83Amt.TextChanged, txt84Amt.TextChanged, txt85Amt.TextChanged
        If txt81Amt.Text = "" Then
            txt81Amt.Text = "0.00"
        End If
        If txt82Amt.Text = "" Then
            txt82Amt.Text = "0.00"
        End If
        If txt83Amt.Text = "" Then
            txt83Amt.Text = "0.00"
        End If
        If txt84Amt.Text = "" Then
            txt84Amt.Text = "0.00"
        End If
        If txt85Amt.Text = "" Then
            txt85Amt.Text = "0.00"
        End If
        txt86Amt.Text = Format(CDec(txt81Amt.Text) + CDec(txt82Amt.Text) + CDec(txt83Amt.Text) + CDec(txt84Amt.Text) + CDec(txt85Amt.Text), "##,##0.00")

    End Sub

    Private Sub txt81Aloc_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt81Aloc.TextChanged, txt82Aloc.TextChanged, txt83Aloc.TextChanged, txt84Aloc.TextChanged, txt85Aloc.TextChanged
        If txt81Aloc.Text = "" Then
            txt81Aloc.Text = "0.00"
        End If
        If txt82Aloc.Text = "" Then
            txt82Aloc.Text = "0.00"
        End If
        If txt83Aloc.Text = "" Then
            txt83Aloc.Text = "0.00"
        End If
        If txt84Aloc.Text = "" Then
            txt84Aloc.Text = "0.00"
        End If
        If txt85Aloc.Text = "" Then
            txt85Aloc.Text = "0.00"
        End If
        txt86Aloc.Text = Format(CDec(txt81Aloc.Text) + CDec(txt82Aloc.Text) + CDec(txt83Aloc.Text) + CDec(txt84Aloc.Text) + CDec(txt85Aloc.Text), "##,##0.00")
    End Sub

    Private Sub txt81Distrb_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt81Distrb.TextChanged, txt82Distrb.TextChanged, txt83Distrb.TextChanged, txt84Distrb.TextChanged, txt85Distrb.TextChanged
        If txt81Distrb.Text = "" Then
            txt81Distrb.Text = "0.00"
        End If
        If txt82Distrb.Text = "" Then
            txt82Distrb.Text = "0.00"
        End If
        If txt83Distrb.Text = "" Then
            txt83Distrb.Text = "0.00"
        End If
        If txt84Distrb.Text = "" Then
            txt84Distrb.Text = "0.00"
        End If
        If txt85Distrb.Text = "" Then
            txt85Distrb.Text = "0.00"
        End If
        txt86Distrb.Text = Format(CDec(txt81Distrb.Text) + CDec(txt82Distrb.Text) + CDec(txt83Distrb.Text) + CDec(txt84Distrb.Text) + CDec(txt85Distrb.Text), "##,##0.00")
    End Sub

    Private Sub txt81Discrp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt81Discrp.TextChanged, txt82Discrp.TextChanged, txt83Discrp.TextChanged, txt84Discrp.TextChanged, txt85Discrp.TextChanged
        If txt81Discrp.Text = "" Then
            txt81Discrp.Text = "0.00"
        End If
        If txt82Discrp.Text = "" Then
            txt82Discrp.Text = "0.00"
        End If
        If txt83Discrp.Text = "" Then
            txt83Discrp.Text = "0.00"
        End If
        If txt84Discrp.Text = "" Then
            txt84Discrp.Text = "0.00"
        End If
        If txt85Discrp.Text = "" Then
            txt85Discrp.Text = "0.00"
        End If
        txt86Discrp.Text = Format(CDec(txt81Discrp.Text) + CDec(txt82Discrp.Text) + CDec(txt83Discrp.Text) + CDec(txt84Discrp.Text) + CDec(txt85Discrp.Text), "##,##0.00")
    End Sub

    Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNext.Click
        Select Case TabControl1.SelectedIndex
            Case 0
                TabControl1.SelectTab(1)

            Case 1
                TabControl1.SelectTab(2)

            Case 2
                TabControl1.SelectTab(3)

            Case 3
                VI41No = CDec(txt41No.Text)
                VI42No = CDec(txt42No.Text)
                VI43No = CDec(txt43No.Text)
                VI44No = CDec(txt44No.Text)
                VI45No = CDec(txt45No.Text)
                VI41Amt = CDec(txt41Amt.Text)
                VI42Amt = CDec(txt42Amt.Text)
                VI43Amt = CDec(txt43Amt.Text)
                VI44Amt = CDec(txt44Amt.Text)
                VI45Amt = CDec(txt45Amt.Text)
                VI51No = CDec(txt51No.Text)
                VI52No = CDec(txt52No.Text)
                VI51Amt = CDec(txt51Amt.Text)
                VI52Amt = CDec(txt52Amt.Text)
                VI611No = CDec(txt611No.Text)
                VI612No = CDec(txt612No.Text)
                VI613No = CDec(txt613No.Text)
                VI611Amt = CDec(txt611Amt.Text)
                VI612Amt = CDec(txt612Amt.Text)
                VI613Amt = CDec(txt612Amt.Text)
                VI621No = CDec(txt621No.Text)
                VI622No = CDec(txt622No.Text)
                VI621Amt = CDec(txt621Amt.Text)
                VI622Amt = CDec(txt622Amt.Text)
                VII1 = CDec(txt71Amt.Text)
                VII2 = CDec(txt72Amt.Text)
                VII3 = CDec(txt73Amt.Text)
                VII5 = CDec(txt75Amt.Text)
                VII6 = CDec(txt76Amt.Text)

                VIII1Per = CDec(txt81Percentage.Text)
                VIII2Per = CDec(txt82Percentage.Text)
                VIII3Per = CDec(txt83Percentage.Text)
                VIII4Per = CDec(txt84Percentage.Text)
                VIII5Per = CDec(txt85Percentage.Text)
                VIII1Amt = CDec(txt81Amt.Text)
                VIII2Amt = CDec(txt82Amt.Text)
                VIII3Amt = CDec(txt83Amt.Text)
                VIII4Amt = CDec(txt84Amt.Text)
                VIII5Amt = CDec(txt85Amt.Text)
                VIII1Alloc = CDec(txt81Aloc.Text)
                VIII2Alloc = CDec(txt82Aloc.Text)
                VIII3Alloc = CDec(txt83Aloc.Text)
                VIII4Alloc = CDec(txt84Aloc.Text)
                VIII5Alloc = CDec(txt85Aloc.Text)
                VIII1Distrb = CDec(txt81Distrb.Text)
                VIII2Distrb = CDec(txt82Distrb.Text)
                VIII3Distrb = CDec(txt83Distrb.Text)
                VIII4Distrb = CDec(txt84Distrb.Text)
                VIII5Distrb = CDec(txt85Distrb.Text)
                VIII1Discrp = CDec(txt81Discrp.Text)
                VIII2Discrp = CDec(txt82Discrp.Text)
                VIII3Discrp = CDec(txt83Discrp.Text)
                VIII4Discrp = CDec(txt84Discrp.Text)
                VIII5Discrp = CDec(txt85Discrp.Text)
                Me.Hide()
                xFormSec4.Show()
        End Select
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        frmCAPRViewer.Close()
        xFormSec4.Close()
        xFormSec3b.Close()
        xFormSec3.Close()
        xFormSec2.Close()
        xFormItro.Close()
        Me.Close()
    End Sub
End Class