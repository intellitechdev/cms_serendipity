﻿Imports System.Data.Sql
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmMember_GovExam
    Dim pk_Exam As String

#Region "Property"
    Public Property getpkGovExam() As String
        Get
            Return pk_Exam
        End Get
        Set(ByVal value As String)
            pk_Exam = value
        End Set
    End Property
#End Region

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        Me.Close()
    End Sub

    Private Sub AddEditExam(ByVal EmpNo As String, ByVal dtExamDate As Date, ByVal Exam As String, ByVal Rating As String, ByVal pkExam As String)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Member_GovExam_AddEdit", _
                                     New SqlParameter("@employeeNo", EmpNo), _
                                     New SqlParameter("@dtExamDate", dtExamDate), _
                                     New SqlParameter("@fcExam", Exam), _
                                     New SqlParameter("@fcRating", Rating), _
                                     New SqlParameter("@pk_Exam", pkExam))
            trans.Commit()
            MessageBox.Show("Record has been Saved", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "Save Government Exam")
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If txtExam.Text = "" Or txtRatings.Text = "" Then
            MessageBox.Show("Complete Information is Needed", "Government Exam Information", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        Else
            Call AddEditExam(frmMember_Master.txtEmployeeNo.Text.Trim, Me.dtDate.Value, Me.txtExam.Text.Trim, Me.txtRatings.Text.Trim, "")
            Call frmMember_Master.GetmemberGovExam(frmMember_Master.txtEmployeeNo.Text)
            ClearText()
            'Me.Close()
        End If
    End Sub

    Private Sub btnupdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupdate.Click
        Call AddEditExam(frmMember_Master.txtEmployeeNo.Text.Trim, Me.dtDate.Value, Me.txtExam.Text.Trim, Me.txtRatings.Text.Trim, pk_Exam)
        Call frmMember_Master.GetmemberGovExam(frmMember_Master.txtEmployeeNo.Text)
        Me.Close()
    End Sub

    Private Sub ClearText()
        txtExam.Text = ""
        txtRatings.Text = ""
    End Sub
End Class