﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmLoanComputationShow001

    Private con As New Clsappconfiguration
    Public empno As String
    Public ltype As String

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub _Compute_BasicPay(ByVal sss As Decimal, ByVal ph As Decimal, ByVal love As Decimal)
        Try
            txtBasic.Text = (CDec(txtSalary.Text) - (sss + ph + love)).ToString
            txtBasic.Text = Format(CDec(txtBasic.Text), "##,##0.00")
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub txtSalary_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSalary.KeyDown
        Try
            If e.KeyCode = Keys.Enter Then
                _Compute_BasicPay(CDec(txtSSS.Text), CDec(txtPhilHealth.Text), CDec(txtPagibig.Text))
                RComputeLoanableAmt(empno, ltype, CDec(txtBasic.Text))
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub txtPhilHealth_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtPhilHealth.KeyDown
        Try
            If e.KeyCode = Keys.Enter Then
                _Compute_BasicPay(CDec(txtSSS.Text), CDec(txtPhilHealth.Text), CDec(txtPagibig.Text))
                RComputeLoanableAmt(empno, ltype, CDec(txtBasic.Text))
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub txtPagibig_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtPagibig.KeyDown
        Try
            If e.KeyCode = Keys.Enter Then
                _Compute_BasicPay(CDec(txtSSS.Text), CDec(txtPhilHealth.Text), CDec(txtPagibig.Text))
                RComputeLoanableAmt(empno, ltype, CDec(txtBasic.Text))
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub txtSSS_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtSSS.KeyDown
        Try
            If e.KeyCode = Keys.Enter Then
                _Compute_BasicPay(CDec(txtSSS.Text), CDec(txtPhilHealth.Text), CDec(txtPagibig.Text))
                RComputeLoanableAmt(empno, ltype, CDec(txtBasic.Text))
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Public Sub RComputeLoanableAmt(ByVal empNo As String, ByVal loantype As String, ByVal basic As Decimal)
        Try
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(con.cnstring, "_Computation_ComputeLoanable_Amt001",
                                                              New SqlParameter("@EmpNo", empNo),
                                                              New SqlParameter("@loanType", loantype),
                                                              New SqlParameter("@basicpay", basic))
            If rd.HasRows Then
                While rd.Read
                    txtCtpPercent.Text = rd(0).ToString
                    txtTakeHomePay.Text = Format(CDec(rd(1).ToString), "##,##0.00")
                    txtCapitalShare.Text = Format(CDec(rd(2).ToString), "##,##0.00")
                    txtRate.Text = Format(CDec(rd(3).ToString), "##,##0.00")
                    txtLoanableAmt.Text = Format(CDec(rd(4).ToString), "##,##0.00")

                    txtCapacity.Text = Format(CDec(txtBasic.Text) - CDec(txtTakeHomePay.Text), "##,##0.00")

                    _ApplyComp_LowerIsGood()
                End While
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub _ApplyComp_LowerIsGood()
        Try
            frmLoan_ApplicationMain.txtContribution.Text = Format(CDec(txtLoanableAmt.Text), "##,##0.00")
            frmLoan_ApplicationMain.txtCapacityToPay.Text = Format(CDec(txtCapacity.Text), "##,##0.00")

            If CDec(txtLoanableAmt.Text) > CDec(txtCapacity.Text) Then
                frmLoan_ApplicationMain.txtMaxLoanAmount.Text = Format(CDec(txtCapacity.Text), "##,##0.00")
            End If
            If CDec(txtCapacity.Text) > CDec(txtLoanableAmt.Text) Then
                frmLoan_ApplicationMain.txtMaxLoanAmount.Text = Format(CDec(txtLoanableAmt.Text), "##,##0.00")
            End If

            frmLoan_ApplicationMain.txtPrincipalAmount.Text = frmLoan_ApplicationMain.txtMaxLoanAmount.Text
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub frmLoanComputationShow001_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtSSS.Text = "0.00"
        txtPhilHealth.Text = "0.00"
        txtPagibig.Text = "0.00"
        txtBasic.Text = txtSalary.Text
        txtCapacity.Text = Format(CDec(txtBasic.Text) - CDec(txtTakeHomePay.Text), "##,##0.00")
    End Sub

End Class