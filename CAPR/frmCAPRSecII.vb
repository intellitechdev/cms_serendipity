Public Class frmCAPRSecII
    Private Sub BtnPrevious_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnPrevious.Click
        Me.Hide()
        xFormSec1.Show()
    End Sub

    Private Sub BtnNextOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnNextOK.Click
        Me.Hide()
        xFormSec4.Show()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        frmCAPRViewer.Close()
        xFormSec4.Close()
        xFormSec3b.Close()
        xFormSec3.Close()
        xFormSec2.Close()
        xFormItro.Close()
        Me.Close()
    End Sub
End Class