﻿Imports System.Data.SqlClient.SqlConnection
Imports System.Data.SqlClient
Imports System.Drawing
Imports Microsoft.VisualBasic.FileSystem
Imports System.Data
Imports System.IO
Imports Microsoft.VisualBasic
Imports System.Security
Imports System.Security.Principal.WindowsIdentity
Imports Microsoft.ApplicationBlocks.Data
Imports System.Text.RegularExpressions
Imports System.Web
Imports System.Web.Security
Imports System.Configuration

Public Class frmMember_Client

    Public fbMembership As Integer
    Dim img As Image
    Dim fpath As String
    Dim filenym As String
    Dim Picdata As Byte()
    Dim PicData2 As Byte()
    Dim Attachment As Byte()
    Dim AttachName As String
    Dim AttachFilename As String
    Dim AttachPath As String
    Dim ex As String
    Dim pkAttach As String

    Public Property getfbMembership() As Integer
        Get
            Return fbMembership
        End Get
        Set(ByVal value As Integer)
            fbMembership = value
        End Set
    End Property

#Region "Button"
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        If btnClose.Text = "Cancel" Then
            If btnAdd.Text = "Save" Then
                btnAdd.Text = "New"
                btnAdd.Image = My.Resources.new3
                btnEdit.Visible = True
                btnDelete.Visible = True
                btnClose.Text = "Close"
            ElseIf btnEdit.Text = "Update" Then
                btnEdit.Text = "Edit"
                btnEdit.Image = My.Resources.edit1
                btnAdd.Visible = True
                btnDelete.Visible = True
                btnClose.Text = "Close"
            End If
            Call EnableButton()
            Call DisableText()
        ElseIf btnClose.Text = "Close" Then
            Me.Close()
        End If
    End Sub

    Private Sub btnEmployeeList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEmployeeList.Click
        frmMember_EmployeeList.ShowDialog()
    End Sub
#End Region

#Region "Add/Edit"
    Private Sub AddClient(ByVal ClientID As String, ByVal ClientName As String, ByVal ContacPerson As String, ByVal TelNo As String,
                          ByVal TIN As String, ByVal Address As String, ByVal Desc As String, ByVal PayrollPeriod As Date,
                          ByVal PayPeriod As Date, ByVal Billing As Date, ByVal Payroll As Date, ByVal Collection As Date,
                          ByVal MonthPay As Date, ByVal ProjectSpecialist As String, ByVal Status As String, ByVal Pic As Byte())
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()
        Try
            'Dim MyGuid As Guid = New Guid(txtClientNo.Text)
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Member_Client_AddEdit", _
                                      New SqlParameter("@fcClientId", ClientID), _
                                      New SqlParameter("@fcClientName", ClientName), _
                                      New SqlParameter("@fcContactPerson", ContacPerson), _
                                      New SqlParameter("@fcTelNo", TelNo), _
                                      New SqlParameter("@fcTIN", TIN), _
                                      New SqlParameter("@fcAddress", Address), _
                                      New SqlParameter("@fcDescription", Desc), _
                                      New SqlParameter("@fcPayrollPeriod", PayrollPeriod), _
                                      New SqlParameter("@fcPayPeriod", PayPeriod), _
                                      New SqlParameter("@fcBilling", Billing), _
                                      New SqlParameter("@fcPayroll", Payroll), _
                                      New SqlParameter("@fcCollection", Collection), _
                                      New SqlParameter("@fc13PayRelease", MonthPay), _
                                      New SqlParameter("@fcProjectSpecialist", ProjectSpecialist), _
                                      New SqlParameter("@membershipStatus", Status), _
                                      New SqlParameter("@fbPicture", Pic))
            trans.Commit()
            MessageBox.Show("Records successfully saved!", "Saving Record...", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Add/Edit Client", MessageBoxButtons.OK)
        End Try
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        If btnAdd.Text = "New" Then
            Call DisableButton()
            Call EnableText()
            Call ClearText()
            btnEdit.Visible = False
            btnDelete.Visible = False
            btnClose.Text = "Cancel"
            btnAdd.Text = "Save"
            btnAdd.Image = My.Resources.save2
            picClientPhoto.Image = My.Resources.noLogo
        ElseIf btnAdd.Text = "Save" Then
            Call CheckDuplicate()
            Call EnableButton()
            Call DisableText()
            'Call ClearText()
            btnEdit.Visible = True
            btnDelete.Visible = True
            btnAdd.Text = "New"
            btnAdd.Image = My.Resources.new3
            'picClientPhoto.Image = My.Resources.noLogo
            btnClose.Text = "Close"
        End If
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If txtClientNo.Text = "" Then
            MessageBox.Show("Nothing to Edit", "Invalid Editing", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        Else
            If btnEdit.Text = "Edit" Then
                Call DisableButton()
                Call EnableText()
                btnAdd.Visible = False
                btnDelete.Visible = False
                btnClose.Text = "Cancel"
                btnEdit.Text = "Update"
                btnEdit.Image = My.Resources.save2
                btnAttach.Enabled = True
            ElseIf btnEdit.Text = "Update" Then

                Dim oStream As New MemoryStream
                Dim bmp1 As New Bitmap(picClientPhoto.Image)
                bmp1.Save(oStream, Imaging.ImageFormat.Jpeg)

                If Me.Picdata Is Nothing Then
                    Me.Picdata = oStream.GetBuffer
                End If

                Call AddClient(txtClientNo.Text.Trim, txtClientName.Text.Trim, txtContactPerson.Text.Trim, txtTelNo.Text.Trim, txtTIN.Text.Trim, txtAddress.Text.Trim,
                           txtDescription.Text.Trim, dtPayrollPeriod.Value, dtPayPeriod.Value, dtBilling.Value, dtPayroll.Value,
                           dtCollection.Value, dtPayRelease.Value, cboProjectSpecialist.Text.Trim, cboMemStatus.Text.Trim, Picdata)
                Call EnableButton()
                Call DisableText()
                btnAdd.Visible = True
                btnDelete.Visible = True
                btnEdit.Text = "Edit"
                btnEdit.Image = My.Resources.edit1
                btnClose.Text = "Close"
                btnAttach.Enabled = False
            End If
        End If
    End Sub
#End Region

#Region "Enable/Disable"
    Private Sub EnableText()
        txtClientNo.ReadOnly = False
        cboProjectSpecialist.Enabled = True
        txtClientName.Enabled = True
        txtContactPerson.Enabled = True
        txtTIN.Enabled = True
        txtTelNo.Enabled = True
        txtAddress.Enabled = True
        txtDescription.Enabled = True
        dtBilling.Enabled = True
        dtCollection.Enabled = True
        dtPayPeriod.Enabled = True
        dtPayroll.Enabled = True
        dtPayrollPeriod.Enabled = True
        dtPayRelease.Enabled = True
        cboMemStatus.Enabled = True
        btnBrowsePicture.Enabled = True
        cboMemStatus.Enabled = True
        btnBrowsePicture.Visible = True
    End Sub

    Private Sub EnableButton()
        btnContract.Enabled = True
        btnEmployeeList.Enabled = True
        btnSearchEmp.Enabled = True
        btnContract.Enabled = True
    End Sub

    Private Sub DisableText()
        txtClientNo.ReadOnly = True
        txtClientName.Enabled = False
        txtContactPerson.Enabled = False
        cboProjectSpecialist.Enabled = False
        txtTIN.Enabled = False
        txtTelNo.Enabled = False
        txtAddress.Enabled = False
        txtDescription.Enabled = False
        dtBilling.Enabled = False
        dtCollection.Enabled = False
        dtPayPeriod.Enabled = False
        dtPayroll.Enabled = False
        dtPayrollPeriod.Enabled = False
        dtPayRelease.Enabled = False
        cboMemStatus.Enabled = False
        btnBrowsePicture.Enabled = False
        cboMemStatus.Enabled = False
        btnBrowsePicture.Visible = False
    End Sub

    Private Sub DisableButton()
        btnContract.Enabled = False
        btnEmployeeList.Enabled = False
        btnSearchEmp.Enabled = False
        btnContract.Enabled = False
    End Sub
#End Region

#Region "Clear Text"
    Private Sub ClearText()
        txtClientNo.Text = ""
        cboProjectSpecialist.Text = ""
        txtClientName.Text = ""
        txtContactPerson.Text = ""
        txtTIN.Text = ""
        txtTelNo.Text = ""
        txtAddress.Text = ""
        txtDescription.Text = ""
        dtPayRelease.Text = ""
        cboMemStatus.Text = ""
        picClientPhoto.Image = My.Resources.noLogo
    End Sub
#End Region

    Private Sub frmMember_Client_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        EmployeeStatus()
        GetProjectSpecialist()
        txtPosHeadcount.ReadOnly = True
        txtProjHeadcount.ReadOnly = True
        Call DisableText()
    End Sub

#Region "Delete"
    Private Sub DeleteClient(ByVal empID As String, ByVal Users As String)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Member_Client_Delete", _
                                      New SqlParameter("@fcClientID", empID), _
                                      New SqlParameter("@user", Users))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "CIMS_Member_Client_Delete")
            frmMsgBox.txtMessage.Text = "Unable to Delete, Client already haved a transaction!"
            MessageBox.Show(ex.Message)
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim x As New DialogResult
        x = MessageBox.Show("Are you sure you want to permanently delete this record?", "Confirm Deletion", MessageBoxButtons.OKCancel, MessageBoxIcon.Information)
        If x = System.Windows.Forms.DialogResult.OK Then
            Call DeleteClient(Me.txtClientNo.Text.Trim, frmLogin.admin)
            picClientPhoto.Image = My.Resources.noLogo
            Call ClearText()
            lvlAttach.Clear()
        ElseIf x = System.Windows.Forms.DialogResult.Cancel Then
        End If
    End Sub
#End Region

    Private Sub btnSearchEmp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearchEmp.Click
        frmMember_SearchClient.ShowDialog()
        btnEmployeeList.Visible = True
        btnUpload.Enabled = True
    End Sub

    Private Sub GetProjectSpecialist()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("MSS_MembersInfo_GetProjectSpecialist", myconnection.sqlconn)
        myconnection.sqlconn.Open()
        Dim mydatareader As SqlDataReader = cmd.ExecuteReader()
        Try
            While mydatareader.Read()
                Me.cboProjectSpecialist.Items.Add(mydatareader(0))
            End While
        Finally
            mydatareader.Close()
            myconnection.sqlconn.Close()
        End Try
    End Sub

#Region "Employee Status"
    Public Sub EmployeeStatus()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("CIMS_m_Membership_Status_view", myconnection.sqlconn)
        myconnection.sqlconn.Open()
        Dim mydatareader As SqlDataReader = cmd.ExecuteReader()
        Try
            While mydatareader.Read()
                Me.cboMemStatus.Items.Add(mydatareader(0))
            End While
        Finally
            mydatareader.Close()
            myconnection.sqlconn.Close()
        End Try
    End Sub
#End Region

    Private Sub cboMemStatus_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboMemStatus.SelectedIndexChanged
        If Me.cboMemStatus.Text = "Active" Then
            getfbMembership() = 1
        ElseIf Me.cboMemStatus.Text = "Inactive" Then
            getfbMembership() = 0
        End If
    End Sub

    Private Sub btnBrowsePicture_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowsePicture.Click
        On Error GoTo e
        Dim ex As String
        OpenFileDialog1.ShowDialog()
        filenym = OpenFileDialog1.FileName
        fpath = Path.GetExtension(filenym)

        Picdata = File.ReadAllBytes(filenym)
        ex = Path.GetExtension(LTrim(RTrim(filenym)))


        Dim ms As New MemoryStream(Picdata, 0, Picdata.Length)

        ms.Write(Picdata, 0, Picdata.Length)

        img = Image.FromStream(ms, True)
        picClientPhoto.Image = img
        picClientPhoto.SizeMode = PictureBoxSizeMode.StretchImage
e:
        Exit Sub
    End Sub

    Public Sub LoadPicture(ByVal empno As String)
        Try
            Dim rd As SqlDataReader
            Dim myconnection As New Clsappconfiguration

            rd = SqlHelper.ExecuteReader(myconnection.cnstring, "CIMS_Member_ShowPictureSignature",
                               New SqlParameter("@employeeno", empno))
            While rd.Read
                If rd.Item("Fb_Picture") Is DBNull.Value Then
                    picClientPhoto.Image = My.Resources.noLogo
                Else
                    PicData2 = rd.Item("Fb_Picture")
                End If
            End While

            Picdata = PicData2

            Dim ms As New MemoryStream(Picdata, 0, Picdata.Length)
            ms.Write(Picdata, 0, Picdata.Length)
            img = Image.FromStream(ms, True)
            picClientPhoto.Image = img
            picClientPhoto.SizeMode = PictureBoxSizeMode.StretchImage
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub CheckDuplicate()
        Dim oStream As New MemoryStream
        Dim bmp1 As New Bitmap(picClientPhoto.Image)
        bmp1.Save(oStream, Imaging.ImageFormat.Jpeg)

        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("CIMS_m_Member_checkduplicateID", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        cmd.Parameters.Add("@fcEmployeeNo", SqlDbType.VarChar, 50).Value = Me.txtClientNo.Text
        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader
        Try
            If myreader.HasRows Then
                MessageBox.Show("IDnumber already exist please try another one", "IDnumber Exists..", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                If Me.Picdata Is Nothing Then
                    Me.Picdata = oStream.GetBuffer
                End If
                Call AddClient(txtClientNo.Text.Trim, txtClientName.Text.Trim, txtContactPerson.Text.Trim, txtTelNo.Text.Trim, txtTIN.Text.Trim, txtAddress.Text.Trim,
                               txtDescription.Text.Trim, dtPayrollPeriod.Value, dtPayPeriod.Value, dtBilling.Value, dtPayroll.Value,
                               dtCollection.Value, dtPayRelease.Value, cboProjectSpecialist.Text.Trim, cboMemStatus.Text.Trim, Picdata)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub btnContract_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnContract.Click
        frmContract_toExpire.ShowDialog()
    End Sub

    Private Sub btnAttach_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAttach.Click
        Dim openFileDialog2 As New OpenFileDialog()
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()

        Try
            If btnAttach.Text = "Attach File" Then
                openFileDialog2.Filter = "Text Files (*.txt)|*.txt|PDF Files (*.pdf)|*.pdf|Word Documents (*.docx)|*.docx|Excel Worksheets (*.xlsx)|*.xlsx|PowerPoint Presentations (*.pptx)|*.pptx|Word Documents 97-2003 (*.doc)|*.docx|Excel Worksheets 93-2003 (*.xls)|*.xls|PowerPoint Presentations (*.ppt)|*.ppt" & "|Office Files|*.docx;*.xlsx;*.pptx;*.doc;*.xls;*.ppt" & "|Images|*.jpg;*.png;*.gif"
                openFileDialog2.Title = "Select File"
                If openFileDialog2.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
                    btnAttach.Text = "Save File"
                    btnemp_add.Image = My.Resources.save2
                    AttachFilename = openFileDialog2.FileName
                    AttachPath = Path.GetExtension(AttachFilename)
                    AttachName = Path.GetFileName(AttachFilename)

                    Attachment = File.ReadAllBytes(AttachFilename)
                    ex = Path.GetExtension(LTrim(RTrim(AttachFilename)))

                    Dim ms As New MemoryStream(Attachment, 0, Attachment.Length)

                    ms.Write(Attachment, 0, Attachment.Length)
                    txtAttachment.Text = AttachName
                ElseIf DialogResult.Cancel Then
                    MessageBox.Show("User Cancelled!", "Cancelled", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
            ElseIf btnAttach.Text = "Save File" Then

                SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Member_ContractAttachment_AddEdit", _
                                         New SqlParameter("@ClientNo", txtClientNo.Text), _
                                         New SqlParameter("@fcAttachment", Attachment), _
                                         New SqlParameter("@fcAttachmentName", AttachName), _
                                         New SqlParameter("@fdDateAttach", Date.Now), _
                                         New SqlParameter("@pk_ContractAttach", pkAttach))
                trans.Commit()
                btnAttach.Text = "Attach File"
                txtAttachment.Text = ""
                MessageBox.Show("Requirement has been Saved", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Call GetmemberAttachReq(txtClientNo.Text)

                gcon.sqlconn.Close()
            End If
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "Save Requirements")
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub

#Region "Get member Attach Contract"
    Public Sub GetmemberAttachReq(ByVal empinfo As String)
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("MSS_MembersInfo_GetContractAttachment", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        cmd.Parameters.Add("@ClientNo", SqlDbType.VarChar, 256).Value = empinfo
        With Me.lvlAttach
            .Clear()
            .View = View.Details
            .GridLines = True
            .FullRowSelect = True
            .Columns.Add("Attachment Name", 500, HorizontalAlignment.Left)
            .Columns.Add("Date Attached", 300, HorizontalAlignment.Left)

            Dim myreader As SqlDataReader
            myreader = cmd.ExecuteReader
            Try
                While myreader.Read
                    With .Items.Add(myreader.Item(0))
                        .SubItems.Add(myreader.Item(1))
                        .SubItems.Add(myreader.Item(2))
                    End With
                End While
            Finally
                myreader.Close()
                myconnection.sqlconn.Close()
            End Try
        End With
    End Sub
#End Region

    Private Sub txtClientNo_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtClientNo.TextChanged
        'ClearText()
        Call GetmemberAttachReq(txtClientNo.Text)
    End Sub

    Private Sub btnUpload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        Try
            If lvlAttach.SelectedItems.Count = Nothing Then
                MessageBox.Show("Select Files to Download", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                btnUpload.Enabled = False
                Exit Sub
            Else
                Dim MyGuid As Guid = New Guid(Me.lvlAttach.SelectedItems(0).SubItems(2).Text)
                Dim afile As Byte()
                Dim myconnection As New Clsappconfiguration
                Dim cmd As New SqlCommand("MSS_MembersInfo_GetContractAttach_AttachedFiles", myconnection.sqlconn)
                cmd.CommandType = CommandType.StoredProcedure
                myconnection.sqlconn.Open()
                cmd.Parameters.Add("@EmployeeNo", SqlDbType.VarChar, 256).Value = txtClientNo.Text
                cmd.Parameters.Add("@FileName", SqlDbType.VarChar, 100).Value = Me.lvlAttach.SelectedItems(0).SubItems(0).Text
                cmd.Parameters.Add("@pk_ContractAttach", SqlDbType.UniqueIdentifier).Value = MyGuid
                SaveFileDialog1.Filter = "Text Files (*.txt)|*.txt|PDF Files (*.pdf)|*.pdf|Word Documents (*.docx)|*.docx|Excel Worksheets (*.xlsx)|*.xlsx|PowerPoint Presentations (*.pptx)|*.pptx|Word Documents 97-2003 (*.doc)|*.docx|Excel Worksheets 93-2003 (*.xls)|*.xls|PowerPoint Presentations (*.ppt)|*.ppt" & "|Office Files|*.docx;*.xlsx;*.pptx;*.doc;*.xls;*.ppt" & "|Images|*.jpg;*.png;*.gif"
                SaveFileDialog1.Title = "Upload Files"
                Using myreader As SqlDataReader = cmd.ExecuteReader
                    myreader.Read()
                    afile = DirectCast(myreader("fcAttacment"), Byte())
                End Using
                If SaveFileDialog1.ShowDialog() = DialogResult.OK Then
                    Dim FiletoSave As String = SaveFileDialog1.FileName
                    Dim Stream As FileStream = New FileStream(FiletoSave, FileMode.Create, FileAccess.Write)
                    Stream.Write(afile, 0, afile.Length)
                    Stream.Close()
                    btnUpload.Enabled = False
                    MessageBox.Show("File Successfully Uploaded!", "Uploaded", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    SaveFileDialog1.FileName = ""
                ElseIf DialogResult.Cancel Then
                    btnUpload.Enabled = False
                    MessageBox.Show("User Cancelled!", "Cancelled", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
                myconnection.sqlconn.Close()
                btnUpload.Enabled = False
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Contract Copy Download File", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub lvlAttach_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvlAttach.Click
        If btnemp_edit.Text = "Update" Then
            btnUpload.Enabled = False
        Else
            If lvlAttach.SelectedItems.Count = Nothing Then
                MessageBox.Show("Select Files to Download", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                btnUpload.Enabled = False
                Exit Sub
            Else
                btnUpload.Enabled = True
            End If
        End If
    End Sub
End Class