'                   ########################################################
'                   ### Created by: Charl Magne "Ionflux" Onod San Pedro ###
'                   ### Date Created: August 10, 2010                    ###
'                   ### Website: http://www.ionflux.site50.net           ###
'                   ########################################################

Public Class frmInterest_ReportOption

    Private Sub BtnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        Me.Close()
    End Sub

    Private Sub frmReportOption_ResizeBegin(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ResizeBegin
        Me.Opacity = 0.5
    End Sub

    Private Sub frmReportOption_ResizeEnd(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ResizeEnd
        Me.Opacity = 1
    End Sub

    Private Sub BtnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnOk.Click
        Dim xReportForm As New FrmRpt_AccruedInterestIncome
        If RdoDetail.Checked = True Then
            xReportForm.ReportMode = "Detail"
        Else
            xReportForm.ReportMode = "Summary"
        End If
        xReportForm.ShowDialog()
    End Sub

    Private Sub frmReportOption_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub
End Class