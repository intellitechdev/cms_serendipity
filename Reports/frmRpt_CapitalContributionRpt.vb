Imports system.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmRpt_CapitalContributionRpt

    Private rptsummary As New ReportDocument
    Private gcon As New Clsappconfiguration
    Private XDate As Date
    Private xMember As String = ""
    Private xMemberName As String = ""
    Private xCompanyName As String = ""

    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If

        Dim obj As ReportObject

        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function
    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        ' for each table apply connection info
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            ' check if logon was successful
            ' if TestConnectivity returns false,
            ' check logon credentials
            If (tbl.TestConnectivity()) Then
                'drop fully qualified table location
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next

        Return True
    End Function
    Private Sub LoadNames()
        Dim DTNames As New DataTable("Names")
        Dim Dr As DataRow
        Dim sSqlCmd As String = "CIMS_LoanManager_GetMemberList"

        With DTNames.Columns
            .Add("Name")
            .Add("EmpNo")
            .Add("PKey")
        End With

        Dr = DTNames.NewRow()
        Dr("Name") = "All Members"
        Dr("EmpNo") = ""
        Dr("PKey") = ""
        DTNames.Rows.Add(Dr)

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSqlCmd)
                While rd.Read
                    Dr = DTNames.NewRow()
                    Dr("Name") = rd.Item("Full Name").ToString
                    Dr("EmpNo") = rd.Item("Employee No").ToString
                    Dr("PKey") = rd.Item("pk_Employee").ToString
                    DTNames.Rows.Add(Dr)
                End While
            End Using
        Catch ex As Exception
            MsgBox("Error at LoadNames in frmCapitalContributionRpt module." & vbCr & vbCr & ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error occur!")
        End Try

        MtcboMembers.Items.Clear()
        MtcboMembers.LoadingType = MTGCComboBox.CaricamentoCombo.DataTable
        MtcboMembers.SourceDataString = New String(2) {"Name", "EmpNo", "PKey"}
        MtcboMembers.SourceDataTable = DTNames
        MtcboMembers.SelectedIndex = 0
    End Sub
    Private Function GetCompanyName() As String
        Dim sSqlCmd As String = "usp_per_company_information_select"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSqlCmd)
                While rd.Read
                    Return rd.Item("CompanyName").ToString
                End While
            End Using
        Catch ex As Exception
            MsgBox("Error at GetCompanyName in frmCapitalContributionRpt module." & vbCr & vbCr & ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error occur!")
            Return ""
        End Try
    End Function
    Private Sub BGWorker_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BGWorker.DoWork
        Try
            rptsummary.Load(Application.StartupPath & "\LoanReport\SchedOfCapitalContributionRpt.rpt")
            Logon(rptsummary, gcon.Server, gcon.Database, gcon.Username, gcon.Password)
            rptsummary.Refresh()
            rptsummary.SetParameterValue("@Date", XDate)
            rptsummary.SetParameterValue("@fxKeyMembers", xMember)
            rptsummary.SetParameterValue("@CompanyName", xCompanyName)
            rptsummary.SetParameterValue("@MemberName", xMemberName)
        Catch ex As Exception
            MsgBox("Error at BGWorker_DoWork in frmCapitalContributionRpt module." & vbCr & vbCr & ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error occur!")
        End Try
    End Sub

    Private Sub btnGenerate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerate.Click
        If MtcboMembers.SelectedItem.Col1 = "All Members" Then
            xMember = Nothing
        Else
            xMember = MtcboMembers.SelectedItem.Col3
        End If
        xMemberName = MtcboMembers.SelectedItem.Col1
        xCompanyName = GetCompanyName()
        XDate = DtDate.Value.Date
        lblLoading.Visible = True
        Panel1.Enabled = False
        BGWorker.RunWorkerAsync()
    End Sub

    Private Sub BGWorker_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BGWorker.RunWorkerCompleted
        CrvRpt.Visible = True
        CrvRpt.ReportSource = rptsummary
        lblLoading.Visible = False
        Panel1.Enabled = True
    End Sub

    Private Sub frmCapitalContributionRpt_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LoadNames()
    End Sub
End Class