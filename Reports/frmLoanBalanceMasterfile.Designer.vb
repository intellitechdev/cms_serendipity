<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLoanBalanceMasterfile
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLoanBalanceMasterfile))
        Me.crvLoanBalanceMaster = New CrystalDecisions.Windows.Forms.CrystalReportViewer
        Me.bgwReportProcess = New System.ComponentModel.BackgroundWorker
        Me.lblLoading = New System.Windows.Forms.Label
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.lblTo = New System.Windows.Forms.Label
        Me.DTTo = New System.Windows.Forms.DateTimePicker
        Me.DTFrom = New System.Windows.Forms.DateTimePicker
        Me.txtFilter = New System.Windows.Forms.TextBox
        Me.MtcboFilter = New MTGCComboBox
        Me.btnMerge = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.CboFilter = New System.Windows.Forms.ComboBox
        Me.LblFilter = New System.Windows.Forms.Label
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'crvLoanBalanceMaster
        '
        Me.crvLoanBalanceMaster.ActiveViewIndex = -1
        Me.crvLoanBalanceMaster.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.crvLoanBalanceMaster.DisplayGroupTree = False
        Me.crvLoanBalanceMaster.Dock = System.Windows.Forms.DockStyle.Fill
        Me.crvLoanBalanceMaster.EnableDrillDown = False
        Me.crvLoanBalanceMaster.EnableToolTips = False
        Me.crvLoanBalanceMaster.Location = New System.Drawing.Point(0, 34)
        Me.crvLoanBalanceMaster.Name = "crvLoanBalanceMaster"
        Me.crvLoanBalanceMaster.SelectionFormula = ""
        Me.crvLoanBalanceMaster.ShowGroupTreeButton = False
        Me.crvLoanBalanceMaster.ShowRefreshButton = False
        Me.crvLoanBalanceMaster.Size = New System.Drawing.Size(671, 332)
        Me.crvLoanBalanceMaster.TabIndex = 0
        Me.crvLoanBalanceMaster.ViewTimeSelectionFormula = ""
        '
        'bgwReportProcess
        '
        '
        'lblLoading
        '
        Me.lblLoading.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lblLoading.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoading.Image = Global.WindowsApplication2.My.Resources.Resources.LoadingData
        Me.lblLoading.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblLoading.Location = New System.Drawing.Point(274, 107)
        Me.lblLoading.Name = "lblLoading"
        Me.lblLoading.Size = New System.Drawing.Size(115, 116)
        Me.lblLoading.TabIndex = 1
        Me.lblLoading.Text = "Loading . . ."
        Me.lblLoading.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.lblLoading.Visible = False
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.lblTo)
        Me.Panel1.Controls.Add(Me.DTTo)
        Me.Panel1.Controls.Add(Me.DTFrom)
        Me.Panel1.Controls.Add(Me.txtFilter)
        Me.Panel1.Controls.Add(Me.MtcboFilter)
        Me.Panel1.Controls.Add(Me.btnMerge)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.CboFilter)
        Me.Panel1.Controls.Add(Me.LblFilter)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(671, 34)
        Me.Panel1.TabIndex = 2
        '
        'lblTo
        '
        Me.lblTo.AutoSize = True
        Me.lblTo.Location = New System.Drawing.Point(403, 10)
        Me.lblTo.Name = "lblTo"
        Me.lblTo.Size = New System.Drawing.Size(23, 13)
        Me.lblTo.TabIndex = 7
        Me.lblTo.Text = "To:"
        Me.lblTo.Visible = False
        '
        'DTTo
        '
        Me.DTTo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DTTo.Location = New System.Drawing.Point(432, 6)
        Me.DTTo.Name = "DTTo"
        Me.DTTo.Size = New System.Drawing.Size(91, 20)
        Me.DTTo.TabIndex = 6
        Me.DTTo.Visible = False
        '
        'DTFrom
        '
        Me.DTFrom.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DTFrom.Location = New System.Drawing.Point(297, 6)
        Me.DTFrom.Name = "DTFrom"
        Me.DTFrom.Size = New System.Drawing.Size(91, 20)
        Me.DTFrom.TabIndex = 6
        Me.DTFrom.Visible = False
        '
        'txtFilter
        '
        Me.txtFilter.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFilter.Location = New System.Drawing.Point(297, 6)
        Me.txtFilter.Name = "txtFilter"
        Me.txtFilter.Size = New System.Drawing.Size(226, 23)
        Me.txtFilter.TabIndex = 4
        Me.txtFilter.Visible = False
        '
        'MtcboFilter
        '
        Me.MtcboFilter.ArrowBoxColor = System.Drawing.SystemColors.Control
        Me.MtcboFilter.ArrowColor = System.Drawing.Color.Black
        Me.MtcboFilter.BindedControl = CType(resources.GetObject("MtcboFilter.BindedControl"), MTGCComboBox.ControlloAssociato)
        Me.MtcboFilter.BorderStyle = MTGCComboBox.TipiBordi.Fixed3D
        Me.MtcboFilter.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.MtcboFilter.ColumnNum = 2
        Me.MtcboFilter.ColumnWidth = "121; 0"
        Me.MtcboFilter.DisabledArrowBoxColor = System.Drawing.SystemColors.Control
        Me.MtcboFilter.DisabledArrowColor = System.Drawing.Color.LightGray
        Me.MtcboFilter.DisabledBackColor = System.Drawing.SystemColors.Control
        Me.MtcboFilter.DisabledBorderColor = System.Drawing.SystemColors.InactiveBorder
        Me.MtcboFilter.DisabledForeColor = System.Drawing.SystemColors.GrayText
        Me.MtcboFilter.DisplayMember = "Text"
        Me.MtcboFilter.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.MtcboFilter.DropDownBackColor = System.Drawing.Color.FromArgb(CType(CType(193, Byte), Integer), CType(CType(210, Byte), Integer), CType(CType(238, Byte), Integer))
        Me.MtcboFilter.DropDownForeColor = System.Drawing.Color.Black
        Me.MtcboFilter.DropDownStyle = MTGCComboBox.CustomDropDownStyle.DropDown
        Me.MtcboFilter.DropDownWidth = 141
        Me.MtcboFilter.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MtcboFilter.GridLineColor = System.Drawing.Color.LightGray
        Me.MtcboFilter.GridLineHorizontal = False
        Me.MtcboFilter.GridLineVertical = False
        Me.MtcboFilter.LoadingType = MTGCComboBox.CaricamentoCombo.ComboBoxItem
        Me.MtcboFilter.Location = New System.Drawing.Point(297, 6)
        Me.MtcboFilter.ManagingFastMouseMoving = True
        Me.MtcboFilter.ManagingFastMouseMovingInterval = 30
        Me.MtcboFilter.Name = "MtcboFilter"
        Me.MtcboFilter.SelectedItem = Nothing
        Me.MtcboFilter.SelectedValue = Nothing
        Me.MtcboFilter.Size = New System.Drawing.Size(226, 24)
        Me.MtcboFilter.TabIndex = 3
        Me.MtcboFilter.Visible = False
        '
        'btnMerge
        '
        Me.btnMerge.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMerge.Image = Global.WindowsApplication2.My.Resources.Resources.apply1
        Me.btnMerge.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnMerge.Location = New System.Drawing.Point(529, 3)
        Me.btnMerge.Name = "btnMerge"
        Me.btnMerge.Size = New System.Drawing.Size(91, 26)
        Me.btnMerge.TabIndex = 2
        Me.btnMerge.Text = "Generate"
        Me.btnMerge.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnMerge.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(3, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(55, 15)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Filter by:"
        '
        'CboFilter
        '
        Me.CboFilter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CboFilter.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CboFilter.FormattingEnabled = True
        Me.CboFilter.Items.AddRange(New Object() {"Customer Name", "Loan Number", "Loan Type", "Date Range"})
        Me.CboFilter.Location = New System.Drawing.Point(64, 6)
        Me.CboFilter.Name = "CboFilter"
        Me.CboFilter.Size = New System.Drawing.Size(150, 23)
        Me.CboFilter.TabIndex = 0
        '
        'LblFilter
        '
        Me.LblFilter.Location = New System.Drawing.Point(231, 5)
        Me.LblFilter.Name = "LblFilter"
        Me.LblFilter.Size = New System.Drawing.Size(65, 23)
        Me.LblFilter.TabIndex = 5
        Me.LblFilter.Text = "[Filter]"
        Me.LblFilter.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'frmLoanBalanceMasterfile
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(671, 366)
        Me.Controls.Add(Me.lblLoading)
        Me.Controls.Add(Me.crvLoanBalanceMaster)
        Me.Controls.Add(Me.Panel1)
        Me.MinimumSize = New System.Drawing.Size(679, 400)
        Me.Name = "frmLoanBalanceMasterfile"
        Me.ShowIcon = False
        Me.Text = "Loan Balance Master"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents crvLoanBalanceMaster As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents bgwReportProcess As System.ComponentModel.BackgroundWorker
    Friend WithEvents lblLoading As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnMerge As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents CboFilter As System.Windows.Forms.ComboBox
    Friend WithEvents MtcboFilter As MTGCComboBox
    Friend WithEvents lblTo As System.Windows.Forms.Label
    Friend WithEvents DTTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents DTFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents LblFilter As System.Windows.Forms.Label
    Friend WithEvents txtFilter As System.Windows.Forms.TextBox
End Class
