Imports System.Data.SqlClient
Imports System.Data.Sql
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Public Class frmLoan_Policy_Approvers_Master
#Region "Variables"
    Public boardmember As Boolean
    Public active As Boolean
    Public GUID As String
    Private loanid As String
    'Private empno As String
    Private LoanTypeCode As Integer
#End Region
#Region "Propert"
    Public Property getboardmember() As Boolean
        Get
            Return boardmember
        End Get
        Set(ByVal value As Boolean)
            boardmember = value
        End Set
    End Property
    Public Property getactive() As Boolean
        Get
            Return active
        End Get
        Set(ByVal value As Boolean)
            active = value
        End Set
    End Property
    Public Property getGUID() As String
        Get
            Return GUID
        End Get
        Set(ByVal value As String)
            GUID = value
        End Set
    End Property
    Private Property getloanid() As String
        Get
            Return loanid
        End Get
        Set(ByVal value As String)
            loanid = value
        End Set
    End Property
#End Region
#Region "Loan type"
    Private Sub loadType()
        Dim mycon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "CIMS_t_Loans_LoanType_Select",
                                     New SqlParameter("@co_name", cboProjectName.Text))
        Try
            cboLoantype.Items.Clear()
            While rd.Read
                Me.cboLoantype.Items.Add(rd.Item(0))
            End While
            rd.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "loadType")
        End Try
    End Sub
#End Region
    Private Sub frmLoanApprovers_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call _LoadAcctg_Proj()
        getLoanTypeCode()
        Me.cboProjectName.Text = frmMasterfile_LoanType.cboProject.Text
        If Me.cboProjectName.Text = "" Then
            Call loadType()
            Call DisabledText()
        Else
            Me.cboProjectName.Text = frmMasterfile_LoanType.cboProject.Text
            Me.cboLoantype.Text = frmMasterfile_LoanType.txtName.Text
            Call ViewApproverPerLoanType(cboLoantype.Text)
            Call loadType()
        End If
    End Sub
    Private Sub btnselect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnselect.Click
        frmSelectApprover.ShowDialog()
    End Sub
    Private Sub txtmaxloan_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtmaxloan.KeyPress
        e.Handled = Not Validates(e.KeyChar)
    End Sub
    Public Function Validates(ByVal C As Char) As Boolean
        If Not (Char.IsDigit(C)) And Not (C = ".") And Not (Microsoft.VisualBasic.AscW(C) = 8) And Not (Microsoft.VisualBasic.AscW(C) = 13) Then
            MsgBox("Invalid Input! This field allows 0-9 and '.' only.", MsgBoxStyle.Exclamation, "Invalid")
            Return False
        Else
            Return True
        End If
    End Function
    Public Function Appcode(ByVal C As Char) As Boolean
        If Not (Char.IsDigit(C)) And Not (Microsoft.VisualBasic.AscW(C) = 8) And Not (Microsoft.VisualBasic.AscW(C) = 13) Then
            MsgBox("Invalid Input! This field allows 0-9 only.", MsgBoxStyle.Exclamation, "Invalid")
            Return False
        Else
            Return True
        End If
    End Function
    Private Sub txtmaxloan_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtmaxloan.TextChanged
        If Me.txtmaxloan.Text = "" Then
            Me.txtmaxloan.Text = "0.00"
        End If
    End Sub
    Private Sub btnsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsave.Click

        Try
            If btnsave.Text = "New" Then
                Label1.Text = "Add New Approver"
                'Call GetSteps(Me.cboLoantype.Text)
                Call EnabledText()
                Call ClearText()
                btndelete.Visible = False
                btnupdate.Visible = False
                btnsave.Text = "Save"
                btnsave.Image = My.Resources.save2
                btnclose.Text = "Cancel"
                'btnclose.Location = New System.Drawing.Point(88, 7)
            ElseIf btnsave.Text = "Save" Then
                If Me.cboLoantype.Text <> "" Then
                    Label1.Text = "Position"
                    Call AddEditApprovers(Me.cboLoantype.Text, Me.txtgeneratedID.Text, Me.txtmaxloan.Text, boardmember, active, 1, Me.cboProjectName.Text)
                    Call ViewApproverPerLoanType(Me.cboLoantype.Text)
                    Call DisabledText()
                    Call ClearText()
                    btnsave.Text = "New"
                    btnsave.Image = My.Resources.new3
                    btnclose.Text = "Close"
                    'btnclose.Location = New System.Drawing.Point(218, 6)
                    btndelete.Visible = True
                    btnupdate.Visible = True
                Else
                    MessageBox.Show("Loan Type is Required.", "Unable to save", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                End If
            Else
                MessageBox.Show("Empty field found! ", "Save", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If

        Catch ex As Exception
        End Try
    End Sub
    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        If btnclose.Text = "Cancel" Then
            Label1.Text = "Approver"
            btnsave.Text = "New"
            btnsave.Image = My.Resources.new3
            btnupdate.Text = "Edit"
            btnupdate.Image = My.Resources.edit1
            btnclose.Text = "Close"
            'btnclose.Location = New System.Drawing.Point(242, 7)
            btndelete.Visible = True
            btnupdate.Visible = True
            btnsave.Enabled = True
        ElseIf btnclose.Text = "Close" Then
            Me.Close()
        End If
    End Sub
    Private Sub btnupdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupdate.Click
        Try
            If btnupdate.Text = "Edit" Then
                Call EnabledText()
                Me.cboLoantype.Text = Me.gridApprover.CurrentRow.Cells(0).Value
                Me.txtname.Text = Me.gridApprover.CurrentRow.Cells(3).Value
                Me.txtmaxloan.Text = Me.gridApprover.CurrentRow.Cells(5).Value
                Me.txtgeneratedID.Text = Me.gridApprover.CurrentRow.Cells(2).Value
                Me.chkboardmember.Checked = Me.gridApprover.CurrentRow.Cells(4).Value
                Me.chkstatus.Checked = Me.gridApprover.CurrentRow.Cells(6).Value
                Me.txtApprovingcode.Text = Me.gridApprover.CurrentRow.Cells(1).Value
                Me.btnsave.Enabled = False
                btnupdate.Text = "Update"
                btnupdate.Image = My.Resources.save2
                btnclose.Text = "Cancel"
                'btnclose.Location = New System.Drawing.Point(164, 7)
                btndelete.Visible = False

            ElseIf btnupdate.Text = "Update" Then
                Label1.Text = "Position"
                Call AddEditApprovers(Me.cboLoantype.Text, Me.txtgeneratedID.Text, Me.txtmaxloan.Text, boardmember, active, 2, cboProjectName.Text)
                Call ViewApproverPerLoanType(Me.cboLoantype.Text)
                Call DisabledText()
                Call ClearText()
                btndelete.Visible = True
                btnupdate.Text = "Edit"
                btnupdate.Image = My.Resources.edit1
                btnclose.Text = "Cancel"
                'btnclose.Location = New System.Drawing.Point(218, 6)
                btnclose.Text = "Close"
                Me.btnsave.Enabled = True
                Me.btndelete.Visible = True

            Else
                MessageBox.Show("Empty field found! ", "Edit", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
        Catch ex As Exception
        End Try
    End Sub
    Private Sub btndelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndelete.Click
        Try
            getGUID() = Me.gridApprover.CurrentRow.Cells(7).Value
            Dim x As New DialogResult
            x = MessageBox.Show("Are you sure you want to permanently delete this record?", "Confirm Deletion", MessageBoxButtons.OKCancel, MessageBoxIcon.Information)
            If x = System.Windows.Forms.DialogResult.OK Then
                Call DeleteApp(GUID)
                Call ViewApproverPerLoanType(Me.cboLoantype.Text)
            ElseIf x = System.Windows.Forms.DialogResult.Cancel Then
            End If
        Catch ex As Exception
        End Try
    End Sub
#Region "Get Steps"
    Private Sub GetSteps(ByVal loanType As String)
        Dim mycon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "CIMS_m_LoanType_Approvers_GetSteps", _
                                     New SqlParameter("@LoanType", loanType))
        Try
            While rd.Read
                Me.txtApprovingcode.Text = rd.Item(0).ToString.Trim
            End While
            rd.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "GetSteps")
        End Try
    End Sub

#End Region
#Region "Add/Edit New Approvers"
    Private Sub AddEditApprovers(ByVal loantype As String, ByVal empkey As String, ByVal maxprincipal As Decimal, ByVal boardmember As Boolean, ByVal active As Boolean, ByVal click As Integer, ByVal CompanyID As String)
        Dim mycon As New Clsappconfiguration
        mycon.sqlconn.Open()
        Dim trans As SqlTransaction = mycon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Masterfiles_Approvers_AddEdit", _
                                      New SqlParameter("@loanType", loantype), _
                                      New SqlParameter("@employeeNo", empkey), _
                                      New SqlParameter("@fbHavMaxPrincipal", boardmember), _
                                      New SqlParameter("@fdMaximumPrincipal", maxprincipal), _
                                      New SqlParameter("@fbActive", active), _
                                      New SqlParameter("@co_name", CompanyID))
            trans.Commit()
            If click = 1 Then
                MessageBox.Show("The Record has been successfully save.", "Approver List", MessageBoxButtons.OK, MessageBoxIcon.Information)
            ElseIf click = 2 Then
                MessageBox.Show("The Record has been successfully updated.", "Approver List", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "AddEditApprovers")
        Finally
            mycon.sqlconn.Close()
        End Try
    End Sub
#End Region
#Region "Delete Approvers"
    Private Sub DeleteApp(ByVal guid As String)
        Dim mycon As New Clsappconfiguration
        mycon.sqlconn.Open()
        Dim trans As SqlTransaction = mycon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Masterfiles_Approvers_Delete", _
                                      New SqlParameter("@fk_employee", guid))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "DeleteApp")
        Finally
            mycon.sqlconn.Close()
        End Try
    End Sub
#End Region
#Region "View Approvers Per Loan Type"
    Private Sub ViewApproverPerLoanType(ByVal loanType As String)
        'ds = SqlHelper.ExecuteDataset(mycon.sqlconn, CommandType.StoredProcedure, "CIMS_Masterfiles_Approvers_LoadPerLoanType", _
        '                             New SqlParameter("@loanType", loanType))
        Dim mycon As New Clsappconfiguration
        Dim ds As New DataSet
        Dim ad As New SqlDataAdapter
        Dim cmd As New SqlCommand("CIMS_Masterfiles_Approvers_LoadPerLoanType", mycon.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        mycon.sqlconn.Open()
        With cmd.Parameters
            '.Add("@loanType", SqlDbType.VarChar, 50).Value = loanType
            '.Add("@co_name", SqlDbType.VarChar, 100).Value = cboProjectName.Text
            '.Add("@fcLoanTypeCode", SqlDbType.VarChar, 100).Value = LoanTypeCode
            .Add("@loanType", SqlDbType.VarChar, 50, ParameterDirection.Input).Value = loanType
            .Add("@co_name", SqlDbType.VarChar, 100, ParameterDirection.Input).Value = cboProjectName.Text
        End With

        ad.SelectCommand = cmd
        ad.Fill(ds, "CIMS_m_LoanType_Approvers")
        Try
            Me.gridApprover.DataSource = ds
            Me.gridApprover.DataMember = "CIMS_m_LoanType_Approvers"
            Me.gridApprover.Columns(0).Visible = False
            Me.gridApprover.Columns(1).Visible = False
            Me.gridApprover.Columns(2).Width = 70
            Me.gridApprover.Columns(3).Width = 230
            Me.gridApprover.Columns(4).Width = 285
            Me.gridApprover.Columns(5).Width = 90
            Me.gridApprover.Columns(7).Visible = False
            'Me.gridApprover.Columns(8).Visible = False
            mycon.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "ViewApproverPerLoanType")
        End Try
    End Sub
#End Region
#Region "Enabled and Disabled and Clear txt"
    Private Sub DisabledText()

        Me.txtApprovingcode.Enabled = False
        Me.txtmaxloan.Enabled = False
        Me.chkboardmember.Enabled = False
        Me.chkstatus.Enabled = False
        Me.txtApprovingcode.Enabled = False
    End Sub
    Private Sub EnabledText()

        Me.txtApprovingcode.Enabled = True
        Me.txtmaxloan.Enabled = True
        Me.chkboardmember.Enabled = True
        Me.chkstatus.Enabled = True
        Me.txtApprovingcode.Enabled = True
    End Sub
    Private Sub ClearText()
        Me.txtname.Text = ""
        Me.txtApprovingcode.Text = ""
        Me.txtmaxloan.Text = "0.00"
        Me.chkboardmember.Checked = False
        Me.chkstatus.Checked = False
        Me.txtApprovingcode.Text = ""
        Me.txtgeneratedID.Text = ""
    End Sub
#End Region
    Private Sub chkboardmember_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkboardmember.CheckedChanged
        If Me.chkboardmember.Checked = True Then
            getboardmember() = True
        Else
            getboardmember() = False
        End If
    End Sub

    Private Sub chkstatus_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkstatus.CheckedChanged
        If Me.chkstatus.Checked = True Then
            getactive() = True
        Else
            getactive() = False
        End If
    End Sub

    Private Sub cboLoantype_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboLoantype.KeyPress
        e.KeyChar = Microsoft.VisualBasic.ChrW(0)
    End Sub

    Private Sub cboLoantype_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLoantype.TextChanged
        Call ViewApproverPerLoanType(Me.cboLoantype.Text)
    End Sub

    Private Sub txtApprovingcode_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtApprovingcode.KeyPress
        e.Handled = Not Appcode(e.KeyChar)
    End Sub

    Private Sub txtApprovingcode_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtApprovingcode.TextChanged
        If Me.txtApprovingcode.Text = "" Then
            Me.txtApprovingcode.Text = "0"
        End If
    End Sub

#Region "Loan type Code"
    Private Sub getLoanTypeCode()
        Dim mycon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "CIMS_Masterfiles_Approvers_getLoanTypeCode",
                                     New SqlParameter("@loanType", cboLoantype.Text),
                                     New SqlParameter("@co_name", cboProjectName.Text))
        Try
            cboLoantype.Items.Clear()
            While rd.Read
                Me.LoanTypeCode = rd.Item(0)
            End While
            rd.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "loadTypeCode")
        End Try
    End Sub
#End Region

#Region "View Approvers Per Loan Type"
    Private Sub ViewApproverPerLoan(ByVal loanType As String)
        Dim mycon As New Clsappconfiguration
        Dim ds As New DataSet

        ds = SqlHelper.ExecuteDataset(mycon.cnstring, "CIMS_Masterfiles_Approvers_LoadPerLoanType",
                                       New SqlParameter("@loanType", loanType),
                                       New SqlParameter("@co_name", cboProjectName.Text))

        gridApprover.DataSource = ds.Tables(0)
        Me.gridApprover.Columns(0).Visible = False
        Me.gridApprover.Columns(1).Visible = False
        Me.gridApprover.Columns(2).Width = 70
        Me.gridApprover.Columns(3).Width = 230
        Me.gridApprover.Columns(4).Width = 285
        Me.gridApprover.Columns(5).Width = 90
        Me.gridApprover.Columns(7).Visible = False
    End Sub
#End Region

#Region "Load Project"
    Private Sub _LoadAcctg_Proj()
        Dim mycon As New Clsappconfiguration
        Try
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(mycon.cnstring, "_Load_Project")
            cboProjectName.Items.Clear()
            While rd.Read
                With cboProjectName
                    .Items.Add(rd(0).ToString)
                End With
            End While
            cboProjectName.Text = "Select Project"
        Catch ex As Exception
            Throw
        End Try
    End Sub
#End Region

    Private Sub cboProjectName_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboProjectName.SelectedValueChanged
        cboLoantype.Text = ""
        Call ViewApproverPerLoanType(cboLoantype.Text)
        Call loadType()
    End Sub
End Class