Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class frmAgingOfLoans_v2

    Private asOfDate As Date
    Public Property GetDate() As Date
        Get
            Return asOfDate
        End Get
        Set(ByVal value As Date)
            asOfDate = value
        End Set
    End Property

    Private rptSummary As New ReportDocument
    Private gCon As New Clsappconfiguration


    Private Sub btnShowPanel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnShowPanel.Click
        If btnShowPanel.Text = "Hide" Then
            btnShowPanel.Text = "Show"
            btnShowPanel.Top = 3
            panelTop.Visible = False
        Else
            btnShowPanel.Text = "Hide"
            btnShowPanel.Top = 43
            panelTop.Visible = True
        End If
    End Sub

    Private Sub frmAgingOfLoans_v2_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        GetDate() = dteAsOf.Value.Date

    End Sub

    Private Sub btnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        picLoading.Visible = True

        If bgwLoadReport.IsBusy = False Then
            bgwLoadReport.RunWorkerAsync()
        End If
    End Sub

    Private Sub bgwLoadReport_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgwLoadReport.DoWork
        LoadReport()
    End Sub

    Private Sub bgwLoadReport_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwLoadReport.RunWorkerCompleted
        crvReport.Visible = True
        crvReport.ReportSource = rptSummary

        picLoading.Visible = False
    End Sub


    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If

        Dim obj As ReportObject

        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function
    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        ' for each table apply connection info
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            ' check if logon was successful
            ' if TestConnectivity returns false,
            ' check logon credentials
            If (tbl.TestConnectivity()) Then
                'drop fully qualified table location
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next

        Return True
    End Function

    Private Sub LoadReport()
        Try
            rptsummary.Load(Application.StartupPath & "\LoanReport\AgingOfLoans.rpt")
            Logon(rptsummary, gCon.Server, gCon.Database, gCon.Username, gCon.Password)
            rptsummary.Refresh()
            rptSummary.SetParameterValue("@asOfDate", GetDate())

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub dteAsOf_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dteAsOf.ValueChanged
        GetDate() = dteAsOf.Value.Date
    End Sub


    Private Sub frmAgingOfLoans_v2_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        rptSummary.Close()
    End Sub
End Class