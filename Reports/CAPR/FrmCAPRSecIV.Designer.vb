<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCAPRSecIV
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TabControl1 = New System.Windows.Forms.TabControl
        Me.TabPage1 = New System.Windows.Forms.TabPage
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.txt34 = New System.Windows.Forms.TextBox
        Me.txt33 = New System.Windows.Forms.TextBox
        Me.txt32 = New System.Windows.Forms.TextBox
        Me.txt31 = New System.Windows.Forms.TextBox
        Me.txt24 = New System.Windows.Forms.TextBox
        Me.txtCoopRating = New System.Windows.Forms.TextBox
        Me.txtTotalCoopPoints = New System.Windows.Forms.TextBox
        Me.txt14 = New System.Windows.Forms.TextBox
        Me.txt23 = New System.Windows.Forms.TextBox
        Me.txt13 = New System.Windows.Forms.TextBox
        Me.txt22 = New System.Windows.Forms.TextBox
        Me.txt12 = New System.Windows.Forms.TextBox
        Me.txt21 = New System.Windows.Forms.TextBox
        Me.txt11 = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.TabPage2 = New System.Windows.Forms.TabPage
        Me.txt321 = New System.Windows.Forms.TextBox
        Me.txt320 = New System.Windows.Forms.TextBox
        Me.txt319 = New System.Windows.Forms.TextBox
        Me.txt318 = New System.Windows.Forms.TextBox
        Me.txt317 = New System.Windows.Forms.TextBox
        Me.txt316 = New System.Windows.Forms.TextBox
        Me.txt315 = New System.Windows.Forms.TextBox
        Me.txt314 = New System.Windows.Forms.TextBox
        Me.txt313 = New System.Windows.Forms.TextBox
        Me.txt312 = New System.Windows.Forms.TextBox
        Me.txt311 = New System.Windows.Forms.TextBox
        Me.txt310 = New System.Windows.Forms.TextBox
        Me.txt39 = New System.Windows.Forms.TextBox
        Me.txt38 = New System.Windows.Forms.TextBox
        Me.txt37 = New System.Windows.Forms.TextBox
        Me.txt36 = New System.Windows.Forms.TextBox
        Me.txt35 = New System.Windows.Forms.TextBox
        Me.txt221 = New System.Windows.Forms.TextBox
        Me.txt220 = New System.Windows.Forms.TextBox
        Me.txt219 = New System.Windows.Forms.TextBox
        Me.txt218 = New System.Windows.Forms.TextBox
        Me.txt217 = New System.Windows.Forms.TextBox
        Me.txt216 = New System.Windows.Forms.TextBox
        Me.txt215 = New System.Windows.Forms.TextBox
        Me.txt214 = New System.Windows.Forms.TextBox
        Me.txt213 = New System.Windows.Forms.TextBox
        Me.txt212 = New System.Windows.Forms.TextBox
        Me.txt211 = New System.Windows.Forms.TextBox
        Me.txt210 = New System.Windows.Forms.TextBox
        Me.txt29 = New System.Windows.Forms.TextBox
        Me.txt28 = New System.Windows.Forms.TextBox
        Me.txt27 = New System.Windows.Forms.TextBox
        Me.txt26 = New System.Windows.Forms.TextBox
        Me.txt25 = New System.Windows.Forms.TextBox
        Me.txtPesosRating = New System.Windows.Forms.TextBox
        Me.txtTotalPesosPt = New System.Windows.Forms.TextBox
        Me.txt121 = New System.Windows.Forms.TextBox
        Me.txt120 = New System.Windows.Forms.TextBox
        Me.txt119 = New System.Windows.Forms.TextBox
        Me.txt118 = New System.Windows.Forms.TextBox
        Me.txt117 = New System.Windows.Forms.TextBox
        Me.txt116 = New System.Windows.Forms.TextBox
        Me.txt115 = New System.Windows.Forms.TextBox
        Me.txt114 = New System.Windows.Forms.TextBox
        Me.txt113 = New System.Windows.Forms.TextBox
        Me.txt112 = New System.Windows.Forms.TextBox
        Me.txt111 = New System.Windows.Forms.TextBox
        Me.txt110 = New System.Windows.Forms.TextBox
        Me.txt19 = New System.Windows.Forms.TextBox
        Me.txt18 = New System.Windows.Forms.TextBox
        Me.txt17 = New System.Windows.Forms.TextBox
        Me.txt16 = New System.Windows.Forms.TextBox
        Me.txt15 = New System.Windows.Forms.TextBox
        Me.Label35 = New System.Windows.Forms.Label
        Me.Label36 = New System.Windows.Forms.Label
        Me.Label37 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.Label33 = New System.Windows.Forms.Label
        Me.Label34 = New System.Windows.Forms.Label
        Me.Label32 = New System.Windows.Forms.Label
        Me.Label31 = New System.Windows.Forms.Label
        Me.Label30 = New System.Windows.Forms.Label
        Me.Label29 = New System.Windows.Forms.Label
        Me.Label28 = New System.Windows.Forms.Label
        Me.Label26 = New System.Windows.Forms.Label
        Me.Label24 = New System.Windows.Forms.Label
        Me.Label23 = New System.Windows.Forms.Label
        Me.Label22 = New System.Windows.Forms.Label
        Me.Label21 = New System.Windows.Forms.Label
        Me.Label19 = New System.Windows.Forms.Label
        Me.Label18 = New System.Windows.Forms.Label
        Me.Label17 = New System.Windows.Forms.Label
        Me.Label16 = New System.Windows.Forms.Label
        Me.Label15 = New System.Windows.Forms.Label
        Me.Label14 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label27 = New System.Windows.Forms.Label
        Me.Label25 = New System.Windows.Forms.Label
        Me.Label20 = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.TabPage3 = New System.Windows.Forms.TabPage
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.RdoBOD = New System.Windows.Forms.RadioButton
        Me.RdoGenManager = New System.Windows.Forms.RadioButton
        Me.RdoCompliance = New System.Windows.Forms.RadioButton
        Me.RdoAccountant = New System.Windows.Forms.RadioButton
        Me.txtCertifiedTrue = New System.Windows.Forms.TextBox
        Me.txtPreparedBy = New System.Windows.Forms.TextBox
        Me.Label40 = New System.Windows.Forms.Label
        Me.Label39 = New System.Windows.Forms.Label
        Me.btnNext = New System.Windows.Forms.Button
        Me.btnPrevious = New System.Windows.Forms.Button
        Me.Label38 = New System.Windows.Forms.Label
        Me.txtCoopPesosRating = New System.Windows.Forms.TextBox
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Location = New System.Drawing.Point(12, 12)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(772, 413)
        Me.TabControl1.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.White
        Me.TabPage1.Controls.Add(Me.Label9)
        Me.TabPage1.Controls.Add(Me.Label8)
        Me.TabPage1.Controls.Add(Me.txt34)
        Me.TabPage1.Controls.Add(Me.txt33)
        Me.TabPage1.Controls.Add(Me.txt32)
        Me.TabPage1.Controls.Add(Me.txt31)
        Me.TabPage1.Controls.Add(Me.txt24)
        Me.TabPage1.Controls.Add(Me.txtCoopRating)
        Me.TabPage1.Controls.Add(Me.txtTotalCoopPoints)
        Me.TabPage1.Controls.Add(Me.txt14)
        Me.TabPage1.Controls.Add(Me.txt23)
        Me.TabPage1.Controls.Add(Me.txt13)
        Me.TabPage1.Controls.Add(Me.txt22)
        Me.TabPage1.Controls.Add(Me.txt12)
        Me.TabPage1.Controls.Add(Me.txt21)
        Me.TabPage1.Controls.Add(Me.txt11)
        Me.TabPage1.Controls.Add(Me.Label4)
        Me.TabPage1.Controls.Add(Me.Label3)
        Me.TabPage1.Controls.Add(Me.Label2)
        Me.TabPage1.Controls.Add(Me.Label7)
        Me.TabPage1.Controls.Add(Me.Label6)
        Me.TabPage1.Controls.Add(Me.Label5)
        Me.TabPage1.Controls.Add(Me.Label1)
        Me.TabPage1.Location = New System.Drawing.Point(4, 24)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(764, 385)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Coop Indicators"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(109, 189)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(82, 15)
        Me.Label9.TabIndex = 4
        Me.Label9.Text = "COOP RATING"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(105, 165)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(113, 15)
        Me.Label8.TabIndex = 4
        Me.Label8.Text = "TOTAL COOP Points"
        '
        'txt34
        '
        Me.txt34.Location = New System.Drawing.Point(613, 129)
        Me.txt34.Name = "txt34"
        Me.txt34.Size = New System.Drawing.Size(127, 23)
        Me.txt34.TabIndex = 3
        Me.txt34.Text = "0.00"
        Me.txt34.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt33
        '
        Me.txt33.Location = New System.Drawing.Point(613, 100)
        Me.txt33.Name = "txt33"
        Me.txt33.Size = New System.Drawing.Size(127, 23)
        Me.txt33.TabIndex = 3
        Me.txt33.Text = "0.00"
        Me.txt33.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt32
        '
        Me.txt32.Location = New System.Drawing.Point(613, 71)
        Me.txt32.Name = "txt32"
        Me.txt32.Size = New System.Drawing.Size(127, 23)
        Me.txt32.TabIndex = 3
        Me.txt32.Text = "0.00"
        Me.txt32.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt31
        '
        Me.txt31.AcceptsReturn = True
        Me.txt31.Location = New System.Drawing.Point(613, 42)
        Me.txt31.Name = "txt31"
        Me.txt31.Size = New System.Drawing.Size(127, 23)
        Me.txt31.TabIndex = 3
        Me.txt31.Text = "0.00"
        Me.txt31.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt24
        '
        Me.txt24.Location = New System.Drawing.Point(480, 129)
        Me.txt24.Name = "txt24"
        Me.txt24.Size = New System.Drawing.Size(127, 23)
        Me.txt24.TabIndex = 3
        Me.txt24.Text = "0.00"
        Me.txt24.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtCoopRating
        '
        Me.txtCoopRating.Location = New System.Drawing.Point(613, 186)
        Me.txtCoopRating.Name = "txtCoopRating"
        Me.txtCoopRating.ReadOnly = True
        Me.txtCoopRating.Size = New System.Drawing.Size(127, 23)
        Me.txtCoopRating.TabIndex = 3
        Me.txtCoopRating.Text = "0.00"
        Me.txtCoopRating.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotalCoopPoints
        '
        Me.txtTotalCoopPoints.Location = New System.Drawing.Point(480, 157)
        Me.txtTotalCoopPoints.Name = "txtTotalCoopPoints"
        Me.txtTotalCoopPoints.ReadOnly = True
        Me.txtTotalCoopPoints.Size = New System.Drawing.Size(127, 23)
        Me.txtTotalCoopPoints.TabIndex = 3
        Me.txtTotalCoopPoints.Text = "0.00"
        Me.txtTotalCoopPoints.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt14
        '
        Me.txt14.Location = New System.Drawing.Point(347, 129)
        Me.txt14.Name = "txt14"
        Me.txt14.Size = New System.Drawing.Size(127, 23)
        Me.txt14.TabIndex = 3
        '
        'txt23
        '
        Me.txt23.Location = New System.Drawing.Point(480, 100)
        Me.txt23.Name = "txt23"
        Me.txt23.Size = New System.Drawing.Size(127, 23)
        Me.txt23.TabIndex = 3
        Me.txt23.Text = "0.00"
        Me.txt23.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt13
        '
        Me.txt13.Location = New System.Drawing.Point(347, 100)
        Me.txt13.Name = "txt13"
        Me.txt13.Size = New System.Drawing.Size(127, 23)
        Me.txt13.TabIndex = 3
        '
        'txt22
        '
        Me.txt22.Location = New System.Drawing.Point(480, 71)
        Me.txt22.Name = "txt22"
        Me.txt22.Size = New System.Drawing.Size(127, 23)
        Me.txt22.TabIndex = 3
        Me.txt22.Text = "0.00"
        Me.txt22.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt12
        '
        Me.txt12.Location = New System.Drawing.Point(347, 71)
        Me.txt12.Name = "txt12"
        Me.txt12.Size = New System.Drawing.Size(127, 23)
        Me.txt12.TabIndex = 3
        '
        'txt21
        '
        Me.txt21.Location = New System.Drawing.Point(480, 42)
        Me.txt21.Name = "txt21"
        Me.txt21.Size = New System.Drawing.Size(127, 23)
        Me.txt21.TabIndex = 3
        Me.txt21.Text = "0.00"
        Me.txt21.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt11
        '
        Me.txt11.Location = New System.Drawing.Point(347, 42)
        Me.txt11.Name = "txt11"
        Me.txt11.Size = New System.Drawing.Size(127, 23)
        Me.txt11.TabIndex = 3
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(24, 132)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(194, 15)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "Plans, Programs and Performance"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(24, 103)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(167, 15)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Operations and Management"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(24, 74)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(79, 15)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Organization"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(653, 24)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(47, 15)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "RATING"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(494, 24)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(93, 15)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "SCORE / POINTS"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(358, 24)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(109, 15)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "RESULTING RATION"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(24, 45)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(316, 15)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Compliance with Administrative and Legal Requirements"
        '
        'TabPage2
        '
        Me.TabPage2.AutoScroll = True
        Me.TabPage2.BackColor = System.Drawing.Color.White
        Me.TabPage2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.TabPage2.Controls.Add(Me.txt321)
        Me.TabPage2.Controls.Add(Me.txt320)
        Me.TabPage2.Controls.Add(Me.txt319)
        Me.TabPage2.Controls.Add(Me.txt318)
        Me.TabPage2.Controls.Add(Me.txt317)
        Me.TabPage2.Controls.Add(Me.txt316)
        Me.TabPage2.Controls.Add(Me.txt315)
        Me.TabPage2.Controls.Add(Me.txt314)
        Me.TabPage2.Controls.Add(Me.txt313)
        Me.TabPage2.Controls.Add(Me.txt312)
        Me.TabPage2.Controls.Add(Me.txt311)
        Me.TabPage2.Controls.Add(Me.txt310)
        Me.TabPage2.Controls.Add(Me.txt39)
        Me.TabPage2.Controls.Add(Me.txt38)
        Me.TabPage2.Controls.Add(Me.txt37)
        Me.TabPage2.Controls.Add(Me.txt36)
        Me.TabPage2.Controls.Add(Me.txt35)
        Me.TabPage2.Controls.Add(Me.txt221)
        Me.TabPage2.Controls.Add(Me.txt220)
        Me.TabPage2.Controls.Add(Me.txt219)
        Me.TabPage2.Controls.Add(Me.txt218)
        Me.TabPage2.Controls.Add(Me.txt217)
        Me.TabPage2.Controls.Add(Me.txt216)
        Me.TabPage2.Controls.Add(Me.txt215)
        Me.TabPage2.Controls.Add(Me.txt214)
        Me.TabPage2.Controls.Add(Me.txt213)
        Me.TabPage2.Controls.Add(Me.txt212)
        Me.TabPage2.Controls.Add(Me.txt211)
        Me.TabPage2.Controls.Add(Me.txt210)
        Me.TabPage2.Controls.Add(Me.txt29)
        Me.TabPage2.Controls.Add(Me.txt28)
        Me.TabPage2.Controls.Add(Me.txt27)
        Me.TabPage2.Controls.Add(Me.txt26)
        Me.TabPage2.Controls.Add(Me.txt25)
        Me.TabPage2.Controls.Add(Me.txtPesosRating)
        Me.TabPage2.Controls.Add(Me.txtTotalPesosPt)
        Me.TabPage2.Controls.Add(Me.txt121)
        Me.TabPage2.Controls.Add(Me.txt120)
        Me.TabPage2.Controls.Add(Me.txt119)
        Me.TabPage2.Controls.Add(Me.txt118)
        Me.TabPage2.Controls.Add(Me.txt117)
        Me.TabPage2.Controls.Add(Me.txt116)
        Me.TabPage2.Controls.Add(Me.txt115)
        Me.TabPage2.Controls.Add(Me.txt114)
        Me.TabPage2.Controls.Add(Me.txt113)
        Me.TabPage2.Controls.Add(Me.txt112)
        Me.TabPage2.Controls.Add(Me.txt111)
        Me.TabPage2.Controls.Add(Me.txt110)
        Me.TabPage2.Controls.Add(Me.txt19)
        Me.TabPage2.Controls.Add(Me.txt18)
        Me.TabPage2.Controls.Add(Me.txt17)
        Me.TabPage2.Controls.Add(Me.txt16)
        Me.TabPage2.Controls.Add(Me.txt15)
        Me.TabPage2.Controls.Add(Me.Label35)
        Me.TabPage2.Controls.Add(Me.Label36)
        Me.TabPage2.Controls.Add(Me.Label37)
        Me.TabPage2.Controls.Add(Me.Label12)
        Me.TabPage2.Controls.Add(Me.Label33)
        Me.TabPage2.Controls.Add(Me.Label34)
        Me.TabPage2.Controls.Add(Me.Label32)
        Me.TabPage2.Controls.Add(Me.Label31)
        Me.TabPage2.Controls.Add(Me.Label30)
        Me.TabPage2.Controls.Add(Me.Label29)
        Me.TabPage2.Controls.Add(Me.Label28)
        Me.TabPage2.Controls.Add(Me.Label26)
        Me.TabPage2.Controls.Add(Me.Label24)
        Me.TabPage2.Controls.Add(Me.Label23)
        Me.TabPage2.Controls.Add(Me.Label22)
        Me.TabPage2.Controls.Add(Me.Label21)
        Me.TabPage2.Controls.Add(Me.Label19)
        Me.TabPage2.Controls.Add(Me.Label18)
        Me.TabPage2.Controls.Add(Me.Label17)
        Me.TabPage2.Controls.Add(Me.Label16)
        Me.TabPage2.Controls.Add(Me.Label15)
        Me.TabPage2.Controls.Add(Me.Label14)
        Me.TabPage2.Controls.Add(Me.Label11)
        Me.TabPage2.Controls.Add(Me.Label27)
        Me.TabPage2.Controls.Add(Me.Label25)
        Me.TabPage2.Controls.Add(Me.Label20)
        Me.TabPage2.Controls.Add(Me.Label13)
        Me.TabPage2.Controls.Add(Me.Label10)
        Me.TabPage2.Location = New System.Drawing.Point(4, 24)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(764, 385)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Pesos Indicators"
        '
        'txt321
        '
        Me.txt321.Location = New System.Drawing.Point(549, 591)
        Me.txt321.Name = "txt321"
        Me.txt321.Size = New System.Drawing.Size(127, 23)
        Me.txt321.TabIndex = 8
        Me.txt321.Text = "0.00"
        Me.txt321.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt320
        '
        Me.txt320.Location = New System.Drawing.Point(549, 562)
        Me.txt320.Name = "txt320"
        Me.txt320.Size = New System.Drawing.Size(127, 23)
        Me.txt320.TabIndex = 8
        Me.txt320.Text = "0.00"
        Me.txt320.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt319
        '
        Me.txt319.Location = New System.Drawing.Point(549, 533)
        Me.txt319.Name = "txt319"
        Me.txt319.Size = New System.Drawing.Size(127, 23)
        Me.txt319.TabIndex = 8
        Me.txt319.Text = "0.00"
        Me.txt319.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt318
        '
        Me.txt318.Location = New System.Drawing.Point(549, 504)
        Me.txt318.Name = "txt318"
        Me.txt318.Size = New System.Drawing.Size(127, 23)
        Me.txt318.TabIndex = 8
        Me.txt318.Text = "0.00"
        Me.txt318.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt317
        '
        Me.txt317.Location = New System.Drawing.Point(549, 461)
        Me.txt317.Name = "txt317"
        Me.txt317.Size = New System.Drawing.Size(127, 23)
        Me.txt317.TabIndex = 8
        Me.txt317.Text = "0.00"
        Me.txt317.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt316
        '
        Me.txt316.Location = New System.Drawing.Point(549, 432)
        Me.txt316.Name = "txt316"
        Me.txt316.Size = New System.Drawing.Size(127, 23)
        Me.txt316.TabIndex = 8
        Me.txt316.Text = "0.00"
        Me.txt316.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt315
        '
        Me.txt315.Location = New System.Drawing.Point(549, 385)
        Me.txt315.Name = "txt315"
        Me.txt315.Size = New System.Drawing.Size(127, 23)
        Me.txt315.TabIndex = 8
        Me.txt315.Text = "0.00"
        Me.txt315.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt314
        '
        Me.txt314.Location = New System.Drawing.Point(549, 356)
        Me.txt314.Name = "txt314"
        Me.txt314.Size = New System.Drawing.Size(127, 23)
        Me.txt314.TabIndex = 8
        Me.txt314.Text = "0.00"
        Me.txt314.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt313
        '
        Me.txt313.Location = New System.Drawing.Point(549, 327)
        Me.txt313.Name = "txt313"
        Me.txt313.Size = New System.Drawing.Size(127, 23)
        Me.txt313.TabIndex = 8
        Me.txt313.Text = "0.00"
        Me.txt313.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt312
        '
        Me.txt312.Location = New System.Drawing.Point(549, 280)
        Me.txt312.Name = "txt312"
        Me.txt312.Size = New System.Drawing.Size(127, 23)
        Me.txt312.TabIndex = 8
        Me.txt312.Text = "0.00"
        Me.txt312.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt311
        '
        Me.txt311.Location = New System.Drawing.Point(549, 251)
        Me.txt311.Name = "txt311"
        Me.txt311.Size = New System.Drawing.Size(127, 23)
        Me.txt311.TabIndex = 8
        Me.txt311.Text = "0.00"
        Me.txt311.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt310
        '
        Me.txt310.Location = New System.Drawing.Point(549, 222)
        Me.txt310.Name = "txt310"
        Me.txt310.Size = New System.Drawing.Size(127, 23)
        Me.txt310.TabIndex = 8
        Me.txt310.Text = "0.00"
        Me.txt310.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt39
        '
        Me.txt39.Location = New System.Drawing.Point(549, 193)
        Me.txt39.Name = "txt39"
        Me.txt39.Size = New System.Drawing.Size(127, 23)
        Me.txt39.TabIndex = 8
        Me.txt39.Text = "0.00"
        Me.txt39.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt38
        '
        Me.txt38.Location = New System.Drawing.Point(549, 164)
        Me.txt38.Name = "txt38"
        Me.txt38.Size = New System.Drawing.Size(127, 23)
        Me.txt38.TabIndex = 8
        Me.txt38.Text = "0.00"
        Me.txt38.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt37
        '
        Me.txt37.Location = New System.Drawing.Point(549, 135)
        Me.txt37.Name = "txt37"
        Me.txt37.Size = New System.Drawing.Size(127, 23)
        Me.txt37.TabIndex = 8
        Me.txt37.Text = "0.00"
        Me.txt37.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt36
        '
        Me.txt36.Location = New System.Drawing.Point(549, 87)
        Me.txt36.Name = "txt36"
        Me.txt36.Size = New System.Drawing.Size(127, 23)
        Me.txt36.TabIndex = 8
        Me.txt36.Text = "0.00"
        Me.txt36.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt35
        '
        Me.txt35.Location = New System.Drawing.Point(549, 58)
        Me.txt35.Name = "txt35"
        Me.txt35.Size = New System.Drawing.Size(127, 23)
        Me.txt35.TabIndex = 8
        Me.txt35.Text = "0.00"
        Me.txt35.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt221
        '
        Me.txt221.Location = New System.Drawing.Point(416, 591)
        Me.txt221.Name = "txt221"
        Me.txt221.Size = New System.Drawing.Size(127, 23)
        Me.txt221.TabIndex = 7
        Me.txt221.Text = "0.00"
        Me.txt221.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt220
        '
        Me.txt220.Location = New System.Drawing.Point(416, 562)
        Me.txt220.Name = "txt220"
        Me.txt220.Size = New System.Drawing.Size(127, 23)
        Me.txt220.TabIndex = 7
        Me.txt220.Text = "0.00"
        Me.txt220.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt219
        '
        Me.txt219.Location = New System.Drawing.Point(416, 533)
        Me.txt219.Name = "txt219"
        Me.txt219.Size = New System.Drawing.Size(127, 23)
        Me.txt219.TabIndex = 7
        Me.txt219.Text = "0.00"
        Me.txt219.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt218
        '
        Me.txt218.Location = New System.Drawing.Point(416, 504)
        Me.txt218.Name = "txt218"
        Me.txt218.Size = New System.Drawing.Size(127, 23)
        Me.txt218.TabIndex = 7
        Me.txt218.Text = "0.00"
        Me.txt218.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt217
        '
        Me.txt217.Location = New System.Drawing.Point(416, 461)
        Me.txt217.Name = "txt217"
        Me.txt217.Size = New System.Drawing.Size(127, 23)
        Me.txt217.TabIndex = 7
        Me.txt217.Text = "0.00"
        Me.txt217.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt216
        '
        Me.txt216.Location = New System.Drawing.Point(416, 432)
        Me.txt216.Name = "txt216"
        Me.txt216.Size = New System.Drawing.Size(127, 23)
        Me.txt216.TabIndex = 7
        Me.txt216.Text = "0.00"
        Me.txt216.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt215
        '
        Me.txt215.Location = New System.Drawing.Point(416, 385)
        Me.txt215.Name = "txt215"
        Me.txt215.Size = New System.Drawing.Size(127, 23)
        Me.txt215.TabIndex = 7
        Me.txt215.Text = "0.00"
        Me.txt215.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt214
        '
        Me.txt214.Location = New System.Drawing.Point(416, 356)
        Me.txt214.Name = "txt214"
        Me.txt214.Size = New System.Drawing.Size(127, 23)
        Me.txt214.TabIndex = 7
        Me.txt214.Text = "0.00"
        Me.txt214.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt213
        '
        Me.txt213.Location = New System.Drawing.Point(416, 327)
        Me.txt213.Name = "txt213"
        Me.txt213.Size = New System.Drawing.Size(127, 23)
        Me.txt213.TabIndex = 7
        Me.txt213.Text = "0.00"
        Me.txt213.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt212
        '
        Me.txt212.Location = New System.Drawing.Point(416, 280)
        Me.txt212.Name = "txt212"
        Me.txt212.Size = New System.Drawing.Size(127, 23)
        Me.txt212.TabIndex = 7
        Me.txt212.Text = "0.00"
        Me.txt212.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt211
        '
        Me.txt211.Location = New System.Drawing.Point(416, 251)
        Me.txt211.Name = "txt211"
        Me.txt211.Size = New System.Drawing.Size(127, 23)
        Me.txt211.TabIndex = 7
        Me.txt211.Text = "0.00"
        Me.txt211.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt210
        '
        Me.txt210.Location = New System.Drawing.Point(416, 222)
        Me.txt210.Name = "txt210"
        Me.txt210.Size = New System.Drawing.Size(127, 23)
        Me.txt210.TabIndex = 7
        Me.txt210.Text = "0.00"
        Me.txt210.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt29
        '
        Me.txt29.Location = New System.Drawing.Point(416, 193)
        Me.txt29.Name = "txt29"
        Me.txt29.Size = New System.Drawing.Size(127, 23)
        Me.txt29.TabIndex = 7
        Me.txt29.Text = "0.00"
        Me.txt29.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt28
        '
        Me.txt28.Location = New System.Drawing.Point(416, 164)
        Me.txt28.Name = "txt28"
        Me.txt28.Size = New System.Drawing.Size(127, 23)
        Me.txt28.TabIndex = 7
        Me.txt28.Text = "0.00"
        Me.txt28.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt27
        '
        Me.txt27.Location = New System.Drawing.Point(416, 135)
        Me.txt27.Name = "txt27"
        Me.txt27.Size = New System.Drawing.Size(127, 23)
        Me.txt27.TabIndex = 7
        Me.txt27.Text = "0.00"
        Me.txt27.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt26
        '
        Me.txt26.Location = New System.Drawing.Point(416, 87)
        Me.txt26.Name = "txt26"
        Me.txt26.Size = New System.Drawing.Size(127, 23)
        Me.txt26.TabIndex = 7
        Me.txt26.Text = "0.00"
        Me.txt26.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt25
        '
        Me.txt25.Location = New System.Drawing.Point(416, 58)
        Me.txt25.Name = "txt25"
        Me.txt25.Size = New System.Drawing.Size(127, 23)
        Me.txt25.TabIndex = 7
        Me.txt25.Text = "0.00"
        Me.txt25.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtPesosRating
        '
        Me.txtPesosRating.Location = New System.Drawing.Point(549, 649)
        Me.txtPesosRating.Name = "txtPesosRating"
        Me.txtPesosRating.ReadOnly = True
        Me.txtPesosRating.Size = New System.Drawing.Size(127, 23)
        Me.txtPesosRating.TabIndex = 6
        Me.txtPesosRating.Text = "0.00"
        Me.txtPesosRating.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotalPesosPt
        '
        Me.txtTotalPesosPt.Location = New System.Drawing.Point(416, 620)
        Me.txtTotalPesosPt.Name = "txtTotalPesosPt"
        Me.txtTotalPesosPt.ReadOnly = True
        Me.txtTotalPesosPt.Size = New System.Drawing.Size(127, 23)
        Me.txtTotalPesosPt.TabIndex = 6
        Me.txtTotalPesosPt.Text = "0.00"
        Me.txtTotalPesosPt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt121
        '
        Me.txt121.Location = New System.Drawing.Point(283, 591)
        Me.txt121.Name = "txt121"
        Me.txt121.Size = New System.Drawing.Size(127, 23)
        Me.txt121.TabIndex = 6
        '
        'txt120
        '
        Me.txt120.Location = New System.Drawing.Point(283, 562)
        Me.txt120.Name = "txt120"
        Me.txt120.Size = New System.Drawing.Size(127, 23)
        Me.txt120.TabIndex = 6
        '
        'txt119
        '
        Me.txt119.Location = New System.Drawing.Point(283, 533)
        Me.txt119.Name = "txt119"
        Me.txt119.Size = New System.Drawing.Size(127, 23)
        Me.txt119.TabIndex = 6
        '
        'txt118
        '
        Me.txt118.Location = New System.Drawing.Point(283, 504)
        Me.txt118.Name = "txt118"
        Me.txt118.Size = New System.Drawing.Size(127, 23)
        Me.txt118.TabIndex = 6
        '
        'txt117
        '
        Me.txt117.Location = New System.Drawing.Point(283, 461)
        Me.txt117.Name = "txt117"
        Me.txt117.Size = New System.Drawing.Size(127, 23)
        Me.txt117.TabIndex = 6
        '
        'txt116
        '
        Me.txt116.Location = New System.Drawing.Point(283, 432)
        Me.txt116.Name = "txt116"
        Me.txt116.Size = New System.Drawing.Size(127, 23)
        Me.txt116.TabIndex = 6
        '
        'txt115
        '
        Me.txt115.Location = New System.Drawing.Point(283, 385)
        Me.txt115.Name = "txt115"
        Me.txt115.Size = New System.Drawing.Size(127, 23)
        Me.txt115.TabIndex = 6
        '
        'txt114
        '
        Me.txt114.Location = New System.Drawing.Point(283, 356)
        Me.txt114.Name = "txt114"
        Me.txt114.Size = New System.Drawing.Size(127, 23)
        Me.txt114.TabIndex = 6
        '
        'txt113
        '
        Me.txt113.Location = New System.Drawing.Point(283, 327)
        Me.txt113.Name = "txt113"
        Me.txt113.Size = New System.Drawing.Size(127, 23)
        Me.txt113.TabIndex = 6
        '
        'txt112
        '
        Me.txt112.Location = New System.Drawing.Point(283, 280)
        Me.txt112.Name = "txt112"
        Me.txt112.Size = New System.Drawing.Size(127, 23)
        Me.txt112.TabIndex = 6
        '
        'txt111
        '
        Me.txt111.Location = New System.Drawing.Point(283, 251)
        Me.txt111.Name = "txt111"
        Me.txt111.Size = New System.Drawing.Size(127, 23)
        Me.txt111.TabIndex = 6
        '
        'txt110
        '
        Me.txt110.Location = New System.Drawing.Point(283, 222)
        Me.txt110.Name = "txt110"
        Me.txt110.Size = New System.Drawing.Size(127, 23)
        Me.txt110.TabIndex = 6
        '
        'txt19
        '
        Me.txt19.Location = New System.Drawing.Point(283, 193)
        Me.txt19.Name = "txt19"
        Me.txt19.Size = New System.Drawing.Size(127, 23)
        Me.txt19.TabIndex = 6
        '
        'txt18
        '
        Me.txt18.Location = New System.Drawing.Point(283, 164)
        Me.txt18.Name = "txt18"
        Me.txt18.Size = New System.Drawing.Size(127, 23)
        Me.txt18.TabIndex = 6
        '
        'txt17
        '
        Me.txt17.Location = New System.Drawing.Point(283, 135)
        Me.txt17.Name = "txt17"
        Me.txt17.Size = New System.Drawing.Size(127, 23)
        Me.txt17.TabIndex = 6
        '
        'txt16
        '
        Me.txt16.Location = New System.Drawing.Point(283, 87)
        Me.txt16.Name = "txt16"
        Me.txt16.Size = New System.Drawing.Size(127, 23)
        Me.txt16.TabIndex = 6
        '
        'txt15
        '
        Me.txt15.Location = New System.Drawing.Point(283, 58)
        Me.txt15.Name = "txt15"
        Me.txt15.Size = New System.Drawing.Size(127, 23)
        Me.txt15.TabIndex = 6
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Location = New System.Drawing.Point(588, 18)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(47, 15)
        Me.Label35.TabIndex = 5
        Me.Label35.Text = "RATING"
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Location = New System.Drawing.Point(435, 18)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(93, 15)
        Me.Label36.TabIndex = 4
        Me.Label36.Text = "SCORE / POINTS"
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Location = New System.Drawing.Point(296, 18)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(109, 15)
        Me.Label37.TabIndex = 3
        Me.Label37.Text = "RESULTING RATION"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(25, 90)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(227, 15)
        Me.Label12.TabIndex = 1
        Me.Label12.Text = "Allowance for Probable Losses on Loans"
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Location = New System.Drawing.Point(410, 668)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(0, 15)
        Me.Label33.TabIndex = 1
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Location = New System.Drawing.Point(25, 652)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(84, 15)
        Me.Label34.TabIndex = 1
        Me.Label34.Text = "PESOS RATING"
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Location = New System.Drawing.Point(13, 623)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(110, 15)
        Me.Label32.TabIndex = 1
        Me.Label32.Text = "Total PESOS points"
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(25, 594)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(246, 15)
        Me.Label31.TabIndex = 1
        Me.Label31.Text = "Total Members' Share Capital / Total Assets"
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(25, 565)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(205, 15)
        Me.Label30.TabIndex = 1
        Me.Label30.Text = "Net Loans Receivables / Total Assets"
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(25, 536)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(163, 15)
        Me.Label29.TabIndex = 1
        Me.Label29.Text = "Total Deposits / Total Assets"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(25, 507)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(190, 15)
        Me.Label28.TabIndex = 1
        Me.Label28.Text = "Non-earning Assets / Total Assets"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(25, 464)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(166, 15)
        Me.Label26.TabIndex = 1
        Me.Label26.Text = "Trend in External Borrowings"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(25, 435)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(133, 15)
        Me.Label24.TabIndex = 1
        Me.Label24.Text = "Growth in Membership"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(25, 388)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(140, 15)
        Me.Label23.TabIndex = 1
        Me.Label23.Text = "Net Institutional Capital"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(25, 359)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(55, 15)
        Me.Label22.TabIndex = 1
        Me.Label22.Text = "Liquidity"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(25, 330)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(55, 15)
        Me.Label21.TabIndex = 1
        Me.Label21.Text = "Solvency"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(25, 283)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(144, 15)
        Me.Label19.TabIndex = 1
        Me.Label19.Text = "Administrative Efficiency"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(25, 254)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(124, 15)
        Me.Label18.TabIndex = 1
        Me.Label18.Text = "Cost per Peso of Loan"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(25, 225)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(155, 15)
        Me.Label17.TabIndex = 1
        Me.Label17.Text = "Loan Portfolio Profitability"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(25, 196)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(192, 15)
        Me.Label16.TabIndex = 1
        Me.Label16.Text = "Rate of Return on Member's Share"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(25, 167)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(161, 15)
        Me.Label15.TabIndex = 1
        Me.Label15.Text = "Operational Self-sufficiency"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(25, 138)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(66, 15)
        Me.Label14.TabIndex = 1
        Me.Label14.Text = "Asset Yield"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(25, 61)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(96, 15)
        Me.Label11.TabIndex = 1
        Me.Label11.Text = "Portfolio at Risk"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label27.Location = New System.Drawing.Point(13, 489)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(122, 18)
        Me.Label27.TabIndex = 0
        Me.Label27.Text = "Structure of Assets"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.Location = New System.Drawing.Point(13, 417)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(65, 18)
        Me.Label25.TabIndex = 0
        Me.Label25.Text = "Outreach"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(13, 312)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(57, 18)
        Me.Label20.TabIndex = 0
        Me.Label20.Text = "Stability"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(13, 120)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(65, 18)
        Me.Label13.TabIndex = 0
        Me.Label13.Text = "Efficiency"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(13, 43)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(108, 18)
        Me.Label10.TabIndex = 0
        Me.Label10.Text = "Portfolio Quality"
        '
        'TabPage3
        '
        Me.TabPage3.BackColor = System.Drawing.Color.White
        Me.TabPage3.Controls.Add(Me.Panel1)
        Me.TabPage3.Controls.Add(Me.RdoCompliance)
        Me.TabPage3.Controls.Add(Me.RdoAccountant)
        Me.TabPage3.Controls.Add(Me.txtCertifiedTrue)
        Me.TabPage3.Controls.Add(Me.txtPreparedBy)
        Me.TabPage3.Controls.Add(Me.Label40)
        Me.TabPage3.Controls.Add(Me.Label39)
        Me.TabPage3.Location = New System.Drawing.Point(4, 24)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(764, 385)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Signatures"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.RdoBOD)
        Me.Panel1.Controls.Add(Me.RdoGenManager)
        Me.Panel1.Location = New System.Drawing.Point(164, 114)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(292, 29)
        Me.Panel1.TabIndex = 4
        '
        'RdoBOD
        '
        Me.RdoBOD.AutoSize = True
        Me.RdoBOD.Checked = True
        Me.RdoBOD.Location = New System.Drawing.Point(3, 7)
        Me.RdoBOD.Name = "RdoBOD"
        Me.RdoBOD.Size = New System.Drawing.Size(111, 19)
        Me.RdoBOD.TabIndex = 3
        Me.RdoBOD.TabStop = True
        Me.RdoBOD.Text = "BOD CHAIRMAN"
        Me.RdoBOD.UseVisualStyleBackColor = True
        '
        'RdoGenManager
        '
        Me.RdoGenManager.AutoSize = True
        Me.RdoGenManager.Location = New System.Drawing.Point(122, 7)
        Me.RdoGenManager.Name = "RdoGenManager"
        Me.RdoGenManager.Size = New System.Drawing.Size(120, 19)
        Me.RdoGenManager.TabIndex = 3
        Me.RdoGenManager.Text = "General Manager"
        Me.RdoGenManager.UseVisualStyleBackColor = True
        '
        'RdoCompliance
        '
        Me.RdoCompliance.AutoSize = True
        Me.RdoCompliance.Location = New System.Drawing.Point(272, 44)
        Me.RdoCompliance.Name = "RdoCompliance"
        Me.RdoCompliance.Size = New System.Drawing.Size(131, 19)
        Me.RdoCompliance.TabIndex = 3
        Me.RdoCompliance.Text = "Compliance Officer"
        Me.RdoCompliance.UseVisualStyleBackColor = True
        '
        'RdoAccountant
        '
        Me.RdoAccountant.AutoSize = True
        Me.RdoAccountant.Checked = True
        Me.RdoAccountant.Location = New System.Drawing.Point(164, 44)
        Me.RdoAccountant.Name = "RdoAccountant"
        Me.RdoAccountant.Size = New System.Drawing.Size(87, 19)
        Me.RdoAccountant.TabIndex = 3
        Me.RdoAccountant.TabStop = True
        Me.RdoAccountant.Text = "Accountant"
        Me.RdoAccountant.UseVisualStyleBackColor = True
        '
        'txtCertifiedTrue
        '
        Me.txtCertifiedTrue.Location = New System.Drawing.Point(164, 85)
        Me.txtCertifiedTrue.Name = "txtCertifiedTrue"
        Me.txtCertifiedTrue.Size = New System.Drawing.Size(239, 23)
        Me.txtCertifiedTrue.TabIndex = 2
        '
        'txtPreparedBy
        '
        Me.txtPreparedBy.Location = New System.Drawing.Point(164, 15)
        Me.txtPreparedBy.Name = "txtPreparedBy"
        Me.txtPreparedBy.Size = New System.Drawing.Size(239, 23)
        Me.txtPreparedBy.TabIndex = 2
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Location = New System.Drawing.Point(10, 88)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(148, 15)
        Me.Label40.TabIndex = 1
        Me.Label40.Text = "Certified True and Correct"
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Location = New System.Drawing.Point(10, 18)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(73, 15)
        Me.Label39.TabIndex = 0
        Me.Label39.Text = "Prepared by"
        '
        'btnNext
        '
        Me.btnNext.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNext.Location = New System.Drawing.Point(694, 431)
        Me.btnNext.Name = "btnNext"
        Me.btnNext.Size = New System.Drawing.Size(90, 29)
        Me.btnNext.TabIndex = 5
        Me.btnNext.Text = "View Report"
        Me.btnNext.UseVisualStyleBackColor = True
        '
        'btnPrevious
        '
        Me.btnPrevious.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPrevious.Location = New System.Drawing.Point(610, 431)
        Me.btnPrevious.Name = "btnPrevious"
        Me.btnPrevious.Size = New System.Drawing.Size(78, 29)
        Me.btnPrevious.TabIndex = 4
        Me.btnPrevious.Text = "Previous"
        Me.btnPrevious.UseVisualStyleBackColor = True
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Location = New System.Drawing.Point(13, 435)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(120, 15)
        Me.Label38.TabIndex = 6
        Me.Label38.Text = "COOP-PESOS RATING"
        '
        'txtCoopPesosRating
        '
        Me.txtCoopPesosRating.Location = New System.Drawing.Point(139, 432)
        Me.txtCoopPesosRating.Name = "txtCoopPesosRating"
        Me.txtCoopPesosRating.Size = New System.Drawing.Size(127, 23)
        Me.txtCoopPesosRating.TabIndex = 7
        '
        'FrmCAPRSecIV
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(796, 466)
        Me.ControlBox = False
        Me.Controls.Add(Me.txtCoopPesosRating)
        Me.Controls.Add(Me.Label38)
        Me.Controls.Add(Me.btnNext)
        Me.Controls.Add(Me.btnPrevious)
        Me.Controls.Add(Me.TabControl1)
        Me.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "FrmCAPRSecIV"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cooperative Annual Performance Report - [Section IV]"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents btnNext As System.Windows.Forms.Button
    Friend WithEvents btnPrevious As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txt34 As System.Windows.Forms.TextBox
    Friend WithEvents txt33 As System.Windows.Forms.TextBox
    Friend WithEvents txt32 As System.Windows.Forms.TextBox
    Friend WithEvents txt31 As System.Windows.Forms.TextBox
    Friend WithEvents txt24 As System.Windows.Forms.TextBox
    Friend WithEvents txt14 As System.Windows.Forms.TextBox
    Friend WithEvents txt23 As System.Windows.Forms.TextBox
    Friend WithEvents txt13 As System.Windows.Forms.TextBox
    Friend WithEvents txt22 As System.Windows.Forms.TextBox
    Friend WithEvents txt12 As System.Windows.Forms.TextBox
    Friend WithEvents txt21 As System.Windows.Forms.TextBox
    Friend WithEvents txt11 As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtCoopRating As System.Windows.Forms.TextBox
    Friend WithEvents txtTotalCoopPoints As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents txt313 As System.Windows.Forms.TextBox
    Friend WithEvents txt312 As System.Windows.Forms.TextBox
    Friend WithEvents txt311 As System.Windows.Forms.TextBox
    Friend WithEvents txt310 As System.Windows.Forms.TextBox
    Friend WithEvents txt39 As System.Windows.Forms.TextBox
    Friend WithEvents txt38 As System.Windows.Forms.TextBox
    Friend WithEvents txt37 As System.Windows.Forms.TextBox
    Friend WithEvents txt36 As System.Windows.Forms.TextBox
    Friend WithEvents txt35 As System.Windows.Forms.TextBox
    Friend WithEvents txt213 As System.Windows.Forms.TextBox
    Friend WithEvents txt212 As System.Windows.Forms.TextBox
    Friend WithEvents txt211 As System.Windows.Forms.TextBox
    Friend WithEvents txt210 As System.Windows.Forms.TextBox
    Friend WithEvents txt29 As System.Windows.Forms.TextBox
    Friend WithEvents txt28 As System.Windows.Forms.TextBox
    Friend WithEvents txt27 As System.Windows.Forms.TextBox
    Friend WithEvents txt26 As System.Windows.Forms.TextBox
    Friend WithEvents txt25 As System.Windows.Forms.TextBox
    Friend WithEvents txt113 As System.Windows.Forms.TextBox
    Friend WithEvents txt112 As System.Windows.Forms.TextBox
    Friend WithEvents txt111 As System.Windows.Forms.TextBox
    Friend WithEvents txt110 As System.Windows.Forms.TextBox
    Friend WithEvents txt19 As System.Windows.Forms.TextBox
    Friend WithEvents txt18 As System.Windows.Forms.TextBox
    Friend WithEvents txt17 As System.Windows.Forms.TextBox
    Friend WithEvents txt16 As System.Windows.Forms.TextBox
    Friend WithEvents txt15 As System.Windows.Forms.TextBox
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents txt317 As System.Windows.Forms.TextBox
    Friend WithEvents txt316 As System.Windows.Forms.TextBox
    Friend WithEvents txt315 As System.Windows.Forms.TextBox
    Friend WithEvents txt314 As System.Windows.Forms.TextBox
    Friend WithEvents txt217 As System.Windows.Forms.TextBox
    Friend WithEvents txt216 As System.Windows.Forms.TextBox
    Friend WithEvents txt215 As System.Windows.Forms.TextBox
    Friend WithEvents txt214 As System.Windows.Forms.TextBox
    Friend WithEvents txt117 As System.Windows.Forms.TextBox
    Friend WithEvents txt116 As System.Windows.Forms.TextBox
    Friend WithEvents txt115 As System.Windows.Forms.TextBox
    Friend WithEvents txt114 As System.Windows.Forms.TextBox
    Friend WithEvents txt318 As System.Windows.Forms.TextBox
    Friend WithEvents txt218 As System.Windows.Forms.TextBox
    Friend WithEvents txt118 As System.Windows.Forms.TextBox
    Friend WithEvents txt321 As System.Windows.Forms.TextBox
    Friend WithEvents txt320 As System.Windows.Forms.TextBox
    Friend WithEvents txt319 As System.Windows.Forms.TextBox
    Friend WithEvents txt221 As System.Windows.Forms.TextBox
    Friend WithEvents txt220 As System.Windows.Forms.TextBox
    Friend WithEvents txt219 As System.Windows.Forms.TextBox
    Friend WithEvents txtPesosRating As System.Windows.Forms.TextBox
    Friend WithEvents txtTotalPesosPt As System.Windows.Forms.TextBox
    Friend WithEvents txt121 As System.Windows.Forms.TextBox
    Friend WithEvents txt120 As System.Windows.Forms.TextBox
    Friend WithEvents txt119 As System.Windows.Forms.TextBox
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents txtCoopPesosRating As System.Windows.Forms.TextBox
    Friend WithEvents RdoGenManager As System.Windows.Forms.RadioButton
    Friend WithEvents RdoBOD As System.Windows.Forms.RadioButton
    Friend WithEvents RdoCompliance As System.Windows.Forms.RadioButton
    Friend WithEvents RdoAccountant As System.Windows.Forms.RadioButton
    Friend WithEvents txtCertifiedTrue As System.Windows.Forms.TextBox
    Friend WithEvents txtPreparedBy As System.Windows.Forms.TextBox
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
End Class
