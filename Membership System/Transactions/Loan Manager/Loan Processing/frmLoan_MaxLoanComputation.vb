Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class frmFinStep

    Private govtLoans As Decimal
    Public Property GetGovtLoans() As Decimal
        Get
            Return govtLoans
        End Get
        Set(ByVal value As Decimal)
            govtLoans = value
        End Set
    End Property

#Region "Events"
    Private Sub cmdSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim msg As String
        msg = MsgBox("Your Loan Application will now be forwarded to the Credit Committee for approval", MsgBoxStyle.OkOnly)
        Me.Close()
        click_submit()

    End Sub
    Private Function click_submit() As Boolean
        'step 1 objects
        frmLoan_ApplicationMain.picStep1.Visible = False
        frmLoan_ApplicationMain.lblStepOne.Visible = False
        frmLoan_ApplicationMain.grpClassification.Visible = False
        frmLoan_ApplicationMain.optRestructure.Visible = False
        frmLoan_ApplicationMain.optNew.Visible = False
        frmLoan_ApplicationMain.lblChooseLoanType.Visible = False
        frmLoan_ApplicationMain.cboLoanType.Visible = False
        frmLoan_ApplicationMain.lblTerms.Visible = False
        frmLoan_ApplicationMain.cboTerms.Visible = False
        frmLoan_ApplicationMain.chkPreTerminate.Visible = False
        frmLoan_ApplicationMain.cmdDone.Visible = False

        'step2 objects
        frmLoan_ApplicationMain.picStep2.Visible = False
        frmLoan_ApplicationMain.lblSteptwo.Visible = False
        frmLoan_ApplicationMain.lblRate.Visible = False
        frmLoan_ApplicationMain.lblSSSLoan.Visible = False
        frmLoan_ApplicationMain.lblPagibigLoan.Visible = False
        frmLoan_ApplicationMain.lblPhilHealth.Visible = False
        frmLoan_ApplicationMain.txtPhilHealth.Visible = False
        frmLoan_ApplicationMain.lblCompanyLoan.Visible = False
        frmLoan_ApplicationMain.lblOtherNonRecurring.Visible = False
        frmLoan_ApplicationMain.txtRateMonthly.Visible = False
        frmLoan_ApplicationMain.txtSSSLoan.Visible = False
        frmLoan_ApplicationMain.txtPagibigLoan.Visible = False
        frmLoan_ApplicationMain.lblPhilHealth.Visible = False
        frmLoan_ApplicationMain.txtPhilHealth.Visible = False
        frmLoan_ApplicationMain.txtCompanyLoan.Visible = False
        frmLoan_ApplicationMain.txtOtherNonRecurring.Visible = False
        frmLoan_ApplicationMain.btnComputeMaxLoan.Visible = False
        frmLoan_ApplicationMain.grpMaxLoanableAmount.Visible = False
        frmLoan_ApplicationMain.lblContribution.Visible = False
        frmLoan_ApplicationMain.lblCapacityToPay.Visible = False
        frmLoan_ApplicationMain.txtContribution.Visible = False
        frmLoan_ApplicationMain.txtCapacityToPay.Visible = False
        frmLoan_ApplicationMain.lblNote.Visible = False
        frmLoan_ApplicationMain.txtMaxLoanAmount.Visible = False
        frmLoan_ApplicationMain.cmdBack.Visible = False
        'step3 objects
        frmLoan_ApplicationMain.picStep3.Visible = False
        frmLoan_ApplicationMain.lblStepThree.Visible = False
        frmLoan_ApplicationMain.grpNecessaryInfo.Visible = False
        frmLoan_ApplicationMain.lblLoanApplicationDate.Visible = False
        frmLoan_ApplicationMain.lblPrincipalAmount.Visible = False
        frmLoan_ApplicationMain.dtpLoanApplicationDate.Visible = False
        frmLoan_ApplicationMain.txtPrincipalAmount.Visible = False
        'what do u want to do objects
        'frmloan_type.picWhatdouwant.Visible = False
        'frmloan_type.lblWhatdouwant.Visible = False
        'frmloan_type.lblClickYes.Visible = False
        'frmloan_type.cmdYes.Visible = False
        'frmloan_type.cmdNo.Visible = False
    End Function
    Private Function click_cancel() As Boolean
        'step 1 objects
        'frmloan_type.picStep1.Enabled = True
        frmLoan_ApplicationMain.lblStepOne.Enabled = True
        frmLoan_ApplicationMain.grpClassification.Enabled = True
        frmLoan_ApplicationMain.optRestructure.Enabled = True
        frmLoan_ApplicationMain.optNew.Enabled = True
        frmLoan_ApplicationMain.lblChooseLoanType.Enabled = True
        frmLoan_ApplicationMain.cboLoanType.Enabled = True
        frmLoan_ApplicationMain.lblTerms.Enabled = True
        frmLoan_ApplicationMain.cboTerms.Enabled = True
        frmLoan_ApplicationMain.chkPreTerminate.Enabled = True
        frmLoan_ApplicationMain.cmdDone.Enabled = True

        'step2 objects
        frmLoan_ApplicationMain.picStep2.Enabled = True
        frmLoan_ApplicationMain.lblSteptwo.Enabled = True
        frmLoan_ApplicationMain.lblRate.Enabled = True
        frmLoan_ApplicationMain.lblSSSLoan.Enabled = True
        frmLoan_ApplicationMain.lblPagibigLoan.Enabled = True
        frmLoan_ApplicationMain.lblPhilHealth.Enabled = True
        frmLoan_ApplicationMain.txtPhilHealth.Enabled = True
        frmLoan_ApplicationMain.lblCompanyLoan.Enabled = True
        frmLoan_ApplicationMain.lblOtherNonRecurring.Enabled = True
        frmLoan_ApplicationMain.txtRateMonthly.Enabled = True
        frmLoan_ApplicationMain.txtSSSLoan.Enabled = True
        frmLoan_ApplicationMain.txtPagibigLoan.Enabled = True
        frmLoan_ApplicationMain.lblPhilHealth.Enabled = True
        frmLoan_ApplicationMain.txtPhilHealth.Enabled = True
        frmLoan_ApplicationMain.txtCompanyLoan.Enabled = True
        frmLoan_ApplicationMain.txtOtherNonRecurring.Enabled = True
        frmLoan_ApplicationMain.btnComputeMaxLoan.Enabled = True
        frmLoan_ApplicationMain.grpMaxLoanableAmount.Enabled = True
        frmLoan_ApplicationMain.lblContribution.Enabled = True
        frmLoan_ApplicationMain.lblCapacityToPay.Enabled = True
        frmLoan_ApplicationMain.txtContribution.Enabled = True
        frmLoan_ApplicationMain.txtCapacityToPay.Enabled = True
        frmLoan_ApplicationMain.lblNote.Enabled = True
        frmLoan_ApplicationMain.txtMaxLoanAmount.Enabled = True
        frmLoan_ApplicationMain.cmdBack.Enabled = True
        'step3 objects
        frmLoan_ApplicationMain.picStep3.Enabled = True
        frmLoan_ApplicationMain.lblStepThree.Enabled = True
        frmLoan_ApplicationMain.grpNecessaryInfo.Enabled = True
        frmLoan_ApplicationMain.lblLoanApplicationDate.Enabled = True
        frmLoan_ApplicationMain.lblPrincipalAmount.Enabled = True
        frmLoan_ApplicationMain.dtpLoanApplicationDate.Enabled = True
        frmLoan_ApplicationMain.txtPrincipalAmount.Enabled = True
        'what do u want to do objects
        'frmloan_type.picWhatdouwant.Enabled = True
        'frmloan_type.lblWhatdouwant.Enabled = True
        'frmloan_type.lblClickYes.Enabled = True
        'frmloan_type.cmdYes.Enabled = True
        'frmloan_type.cmdNo.Enabled = True
    End Function
    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Close()
        click_cancel()
    End Sub
#End Region

#Region "ShowComputation "
    Public Sub Show_Computation(ByVal empno As String, ByVal basicpay As Decimal, _
                ByVal loantype As String, ByVal terms As String, _
                ByVal cooploans As Decimal, ByVal otherloans As Decimal, _
                ByVal misc As Decimal, ByVal restruct As Decimal)
        'txtContribution.Clear()MSS_Loans_ComputeMax ceLoanAmount ''ByVal otherloans As Decimal''
        Dim gCon As New Clsappconfiguration

        Dim sSqlCmd As String = "MSS_Loans_ComputeMaxLoanAmount2 '" & empno & "', '" & _
                                        basicpay & "', '" & loantype & "', '" & terms & _
                                        "', '" & cooploans & "', '" & otherloans & "', '" & misc & "','" & restruct & "'"

        '--original code
        '"MSS_Loans_ComputeMaxLoanAmount1 '" & empno & "', '" & _
        'basicpay & "', '" & loantype & "', '" & terms & _
        '"', '" & cooploans & "', '" & otherloans & "', '" & misc & "'"

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                While rd.Read
                    txtloantype.Text = rd("Type of Loan")
                    txtterms.Text = rd("Terms")
                    txtInterest.Text = Format(rd("Interest"), "N2")
                    txtSalary.Text = rd("Basic Pay").ToString()
                    txtRemainingNetPay.Text = Format(rd("Remaining Net Pay"), "N2")
                    txtMaxLoanAmount.Text = "PHP " + frmLoan_ApplicationMain.txtMaxLoanAmount.Text
                    txtCapitalContribution.Text = Format(rd("Capital Contribution"), "N2")
                    txtPolicyRate.Text = rd("Policy Rate").ToString
                    txtMaxLoanAmount2.Text = "PHP " + Format(rd("Max Loan Capital Cont"), "N2")
                    txtCapToPay.Text = "PHP " + Format(rd("Capacity to Pay"), "N2")

                    'Added by Vincent Nacar :D ====================================
                    frmLoan_ApplicationComputation.lblInterest.Text = Format(rd("Interest"), "N2")
                    '==============================================================
                End While
            End Using
        Catch ex As Exception
            'MsgBox(ex.ToString, MsgBoxStyle.OkOnly, "error at Show_Computation")
            MessageBox.Show("Compute Failed!, Please Check your Employment Information and Supply the Required fields. ", "Error!")
        End Try

    End Sub



    Private Function SalaryTypeRate(ByVal empNo As String) As Integer
        Dim rate As Integer
        Dim gCon As New Clsappconfiguration

        Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "CIMS_m_WholdTax_SalaryRate_AmortizationRate", _
                        New SqlParameter("@employeeNo", empNo))
            If rd.Read Then
                rate = rd.Item(0)
            End If
        End Using

        Return rate
    End Function
#End Region

    Private Sub frmFinStep_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        Close()
    End Sub
End Class