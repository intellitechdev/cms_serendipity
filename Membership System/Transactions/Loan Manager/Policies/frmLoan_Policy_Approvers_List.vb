Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmSelectApprover
    Private Sub btnok_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnok.Click
        Try
            frmLoan_Policy_Approvers_Master.txtname.Text = Me.lvlApprovers.SelectedItems(0).Text
            frmLoan_Policy_Approvers_Master.txtgeneratedID.Text = Me.lvlApprovers.SelectedItems(0).SubItems(1).Text
            Me.Close()
        Catch ex As Exception
            MessageBox.Show("Select one member", "Select", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub


    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        Me.Close()
    End Sub

    Private Sub frmSelectApprover_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call ListofApprovers()
    End Sub
#Region "list of approvers"
    Private Sub ListofApprovers()
        Dim mycon As New Clsappconfiguration
        Dim cmd As New SqlCommand("CIMS_m_Member_SelectApprover1", mycon.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Connection = mycon.sqlconn
        mycon.sqlconn.Open()
        With Me.lvlApprovers
            .Clear()
            .View = View.Details
            .FullRowSelect = True
            .GridLines = True
            .Columns.Add("Member Name", 400, HorizontalAlignment.Left)
            Dim rd As SqlDataReader
            rd = cmd.ExecuteReader
            Try
                While rd.Read
                    With .Items.Add(rd.Item(0))
                        .subitems.add(rd.Item(1))
                    End With
                End While
                rd.Close()
            Catch ex As Exception
                MessageBox.Show(ex.Message, "ListofApprovers")
            End Try
        End With

    End Sub
#End Region
#Region "List with parameters"
    Private Sub ListWithParam(ByVal search As String)
        Dim mycon As New Clsappconfiguration
        Dim cmd As New SqlCommand("CIMS_m_Member_SelectApprover", mycon.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Connection = mycon.sqlconn
        mycon.sqlconn.Open()
        cmd.Parameters.Add("@searchname", SqlDbType.VarChar, 256).Value = search
        With Me.lvlApprovers
            .Clear()
            .View = View.Details
            .FullRowSelect = True
            .GridLines = True
            .Columns.Add("Member Name", 400, HorizontalAlignment.Left)
            Dim rd As SqlDataReader
            rd = cmd.ExecuteReader
            Try
                While rd.Read
                    With .Items.Add(rd.Item(0))
                        .subitems.add(rd.Item(1))
                    End With
                End While
                rd.Close()
            Catch ex As Exception
                MessageBox.Show(ex.Message, "ListofApprovers")
            End Try
        End With
    End Sub
#End Region

    Private Sub btnfind_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnfind.Click
        Call ListWithParam(Me.txtsearch.Text)
    End Sub

    Private Sub txtsearch_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtsearch.KeyPress
        If e.KeyChar = Chr(13) Then
            Call ListWithParam(Me.txtsearch.Text)
        End If
    End Sub

    Private Sub txtsearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtsearch.TextChanged
        Call ListWithParam(Me.txtsearch.Text)
    End Sub
End Class