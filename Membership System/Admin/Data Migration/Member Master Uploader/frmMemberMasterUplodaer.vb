﻿Imports System.Configuration
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmMemberMasterUplodaer
    Private xFilePath As String = ""

    Private Sub DlgOpenUploader_FileOk_1(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles DlgOpenUploader.FileOk
        txtUploader.Text = DlgOpenUploader.FileName
        xFilePath = txtUploader.Text
    End Sub

    Private Sub DlgSaveUploader_FileOk_1(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles DlgSaveUploader.FileOk
        xFilePath = DlgOpenUploader.FileName
    End Sub

    Private Sub btnBrowse_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse.Click
        DlgOpenUploader.ShowDialog()
    End Sub

    Private Sub frmMemberMasterUplodaer_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub btnUpload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        If txtUploader.Text <> "" And txtCreator.Text <> "" Then
            Create_Linked_Servers()
            Upload_employees()
            Delete_Linked_Servers()
        End If
    End Sub

    Private Sub Create_Linked_Servers()
        Dim myconnection As New Clsappconfiguration
        Try
            SqlHelper.ExecuteNonQuery(myconnection.cnstring, CommandType.StoredProcedure, "CIMS_Upload_MemberMaster", _
                        New SqlParameter("@datasrc", txtUploader.Text))
        Catch ex As Exception
            MessageBox.Show(ex.ToString)
        End Try
    End Sub

    Private Sub Delete_Linked_Servers()
        Dim myconnection As New Clsappconfiguration
        Try
            SqlHelper.ExecuteNonQuery(myconnection.cnstring, CommandType.StoredProcedure, "CIMS_Delete_LinkedServer")
        Catch ex As Exception
            MessageBox.Show(ex.ToString)
        End Try
    End Sub

    Private Sub Upload_employees()
        Dim myconnection As New Clsappconfiguration
        Try
            SqlHelper.ExecuteNonQuery(myconnection.cnstring, CommandType.StoredProcedure, "MSS_Admin_UploadMemberMaster", _
                                            New SqlParameter("@coopName", frmMain.lblCompanyTitle.Text), _
                                            New SqlParameter("@user", txtCreator.Text))
            MessageBox.Show("Member Information Succesfully Uploaded!")
        Catch ex As Exception
            MessageBox.Show(ex.ToString)
        End Try
    End Sub
End Class